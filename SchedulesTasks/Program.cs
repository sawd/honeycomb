﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using Honeycomb.Custom;
using System.Net.Mail;
using System.Configuration;

namespace SchedulesTasks {
    class Program {
        static void Main(string[] args) {

            try {
                //send out lease reminders and agent notifications
				AutomatedNotifications.SendLeaseExpiryReminders();
                //Console.ReadLine();
                                
            } catch(Exception ex) {
				Utility.LogException(ex, "Console app calling SendLeaseExpiryReminders");
            }

			try {
				//terminate expired leases
				Security.TerminateExpiredLeaseAccess();

			} catch(Exception ex) {

				Utility.LogException(ex, "Console app calling TerminateExpiredLeaseAccess");
			}

        }

    }
}
