﻿#region Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using System.Data.Entity;
using System.Data.Linq;
using System.Data.Objects;
using Honeycomb.Custom;
using System.Collections;
using System.ComponentModel;
using Honeycomb.Custom.Data;
#endregion

namespace HoneyComb.Maintenance
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in  both code and config file together.HoneyComb.Maintenance.WCFMaintenance
    [ServiceContract(Namespace = "MaintenanceService")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class WCFMaintenance
    {
		private Honeycomb_Entities dbContext = new Honeycomb_Entities();

        #region lOOKoptoptions
        #region common methods
        /// <summary>
        /// A single function which utilises the unified lookup structure, accepts an id of the specific lookup list to retrieve
        /// </summary>
        /// <param name="LookUpListID"></param>
        /// <returns></returns>
        [OperationContract]
        public EntityTransport<CustomLookUpOptions> GetLookUpOptions(PaginationContainer pagination, string filter = null)
        {
            var LookUpOptions = new List<CustomLookUpOptions>();
            int count;

            try
            {

                using (var context = new Honeycomb_Entities())
                {

                    LookUpOptions = (from options in context.LookUp
                                     where options.DeletedOn == null
                                     && options.DeletedBy == null
                                     select new CustomLookUpOptions
                                     {
                                         lookUpTypeID = options.LookUpOption.FirstOrDefault().ID
                                         ,
                                         lookUpTypeName = options.Name
                                         ,
                                         lookUpName = options.LookUpOption.FirstOrDefault().Name
                                         ,
                                         lookUpDescription = options.Description
                                         ,
                                         lookUpID = options.ID

                                     }
                                     ).GroupBy(p => p.lookUpID).Select(w => w.FirstOrDefault()).ToList();
                    count = LookUpOptions.Count;
                    LookUpOptions = LookUpOptions.AsQueryable().OptionalWhere(!string.IsNullOrEmpty(filter), lk => (lk.lookUpTypeName.ToLower().Contains(filter.ToLower()) || lk.lookUpName.ToLower().Contains(filter.ToLower()))).ToList();
                    LookUpOptions = LookUpOptions.AsQueryable().Paginate(pagination.CurrentPage, pagination.PageSize, pagination.OrderBy).ToList();
                }

                return Utility.ServiceResponse(ref LookUpOptions,count);
            }
            catch (Exception ex)
            {
                return Utility.ServiceResponse(ref LookUpOptions, ex);
            }
        }
        /// <summary>
        /// Update lookup Options
        /// </summary>
        /// <param name="lookupOptions">lookup Object to be created or updated</param>
        /// <returns></returns>
        [OperationContract]
        public EntityTransport<long> UpdateLookUpOptions(LookUpOption lookupOptions)
        {
            long id = 0;
            using (var context = new Honeycomb_Entities())
            {
                //create new lookup
                try
                {

                    if (lookupOptions.ID == 0)
                    {
                        context.LookUpOption.Add(lookupOptions);
                        lookupOptions.InsertedOn = DateTime.Now;
                        lookupOptions.InsertedBy = SessionManager.UserName;
                        lookupOptions.DeletedBy = null;
                    }
                    else
                    {
                        var dbLookups = context.LookUpOption.Where(l => l.ID == lookupOptions.ID).FirstOrDefault();

                        lookupOptions.MergeInto(dbLookups, new string[] 
                        {                        
                        "ID",
                        "DeletedOn",
                        "DeletedBy",
                        "InsertedOn",
                        "InsertedBy",
                        "LookUpID"
                        }, true);

                    }

                    context.SaveChanges();
                    id = lookupOptions.ID;
                    return Utility.ServiceResponse(ref id);
                }
                catch (Exception ex)
                {

                    return Utility.ServiceResponse(ref id, ex);
                }

            }


        }
        /// <summary>
        /// Updates or creates a new lookup object
        /// </summary>
        /// <param name="lookUpEntity">lookup object to be created or updated </param>
        /// <returns></returns>
        [OperationContract]
        public EntityTransport<ProcessResponse> UpdateLookups(LookUp lookUpEntity)
        {
            ProcessResponse response = new ProcessResponse();

            using (var context = new Honeycomb_Entities())
            {
                try
                {
                    if (lookUpEntity.ID == 0)
                    {
                        context.LookUp.Add(lookUpEntity);
                        lookUpEntity.InsertedBy = SessionManager.UserName;
                        lookUpEntity.InsertedOn = DateTime.Now;
                        lookUpEntity.InsertedByID = SessionManager.UserID;
                        lookUpEntity.DeletedBy = null;

                    }
                    else
                    {

                        var dbLookUps = context.LookUp.Where(lk => lk.DeletedOn == null && lk.DeletedBy == null && lk.ID == lookUpEntity.ID).FirstOrDefault();
                        lookUpEntity.MergeInto(dbLookUps, new string[] 
                        {
                            "ID",
                            "DeletedOn",
                            "DeletedBy",
                            "InsertedOn",
                            "InsertedBy",
                            "LookUpID"
                        }, true);


                    }
                    context.SaveChanges();
                    response.success = true;
                    response.message = "Successfully created look up option";
                    return Utility.ServiceResponse(ref response);
                }
                catch (Exception ex)
                {
                    response.success = false;
                    response.message = "Failed to create look up option.";
                    return Utility.ServiceResponse(ref response, ex);
                }


            }
        }
        /// <summary>
        /// Get lookUps
        /// </summary>
        /// <param name="pagination"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        [OperationContract]
        public EntityTransport<LookUp> GetLookUps(PaginationContainer pagination, string filter = null)
        {
            var lookups = new List<LookUp>();
            try
            {


                using (var context = new Honeycomb_Entities())
                {
                    lookups = context.LookUp.Where(lk => lk.DeletedBy == null && lk.DeletedOn == null).Select(all => all).ToList();
                    lookups = lookups.AsQueryable().OptionalWhere(!string.IsNullOrEmpty(filter), l => l.Name.ToLower().Contains(filter.ToLower())).ToList();
                    lookups = lookups.AsQueryable().Paginate(pagination.CurrentPage, pagination.PageSize, pagination.OrderBy).ToList();
                    return Utility.ServiceResponse(ref lookups);
                }

            }
            catch (Exception ex)
            {

                return Utility.ServiceResponse(ref lookups, ex);
            }

        }

        [OperationContract]
        public EntityTransport<CustomLookUpOptions> GetLookUpOptionsBylookUptypeID(long LookUptypeID)
        {
            var LookUpOptions = new List<CustomLookUpOptions>();

            try
            {

                using (var context = new Honeycomb_Entities())
                {
                    LookUpOptions = (from lo in context.LookUpOption
                                     join lk in context.LookUp on lo.LookUpID equals lk.ID
                                     where lo.LookUpID == LookUptypeID
                                     && lo.DeletedByID == null
                                     && lo.DeletedOn == null
                                     orderby lo.Name
                                     select new CustomLookUpOptions
                                     {
                                         lookUpTypeName = lo.Name,
                                         lookUpName = lk.Name,
                                         lookUpDescription = lk.Description,
                                         lookUpID = lk.ID,
                                         lookUpTypeID = lo.ID
                                     }).ToList();

                }

                return Utility.ServiceResponse(ref LookUpOptions);
            }
            catch (Exception ex)
            {
                return Utility.ServiceResponse(ref LookUpOptions, ex);
            }
        }

        /// <summary>
        /// A single function which utilises the unified lookup structure, accepts an id of the specific lookup list to retrieve, do not edit this function! It retrieves a list of options for a specific lookup list, It's a util function which is used in multiple places, changing this will break lots of lookup list displays.
        /// </summary>
        /// <param name="LookUpListID"></param>
        /// <returns></returns>
        [OperationContract]
        public EntityTransport<LookUpOption> GetLookUpOptionsByID(long LookUpListID)
        {
            var LookUpOptions = new List<LookUpOption>();

            try
            {

                using (var context = new Honeycomb_Entities())
                {
                    //do not edit this function! It retrieves a list of options for a specific lookup list, It's a util function which is used in multiple places, changing this will break lots of lookup list displays.
                    LookUpOptions = context.LookUpOption.Where(lo => lo.LookUpID == LookUpListID).OrderBy(o => o.Name).ToList();

                }

                return Utility.ServiceResponse(ref LookUpOptions);
            }
            catch (Exception ex)
            {
                return Utility.ServiceResponse(ref LookUpOptions, ex);
            }
        }
        /// <summary>
        /// A single function which utilises the unified lookup structure, accepts an id of the specific lookup list to retrieve
        /// </summary>
        /// <param name="LookUpListID"></param>
        /// <returns></returns>
        [OperationContract]
        public EntityTransport<CustomLookUpOptions> GetAvailableLookUpOptions()
        {

            var LookUps = new List<CustomLookUpOptions>();

            try
            {

                using (var context = new Honeycomb_Entities())
                {
                    LookUps = (from lko in context.LookUpOption
                               join lk in context.LookUp on lko.LookUpID equals lk.ID
                               where lko.DeletedOn == null
                               && lko.DeletedBy == null
                               select new CustomLookUpOptions
                              {
                                  lookUpName = lk.Name,
                                  lookUpID = lk.ID
                              }).Distinct().AsQueryable().ToList(); ;



                }

                return Utility.ServiceResponse(ref LookUps);
            }
            catch (Exception ex)
            {
                return Utility.ServiceResponse(ref LookUps, ex);
            }
        }
        /// <summary>
        /// Delete a selected look up option 
        /// </summary>
        /// <param name="lookUpID">Id of the lookup optopn to be deleted </param>
        /// <returns></returns>
        [OperationContract]
        public EntityTransport<LookUpOption> DeleteLookUpOption(long lookUpID)
        {
            LookUpOption LookUpOptionEntity = new LookUpOption();
            try
            {
                using (var context = new Honeycomb.Custom.Data.Honeycomb_Entities())
                {
                    LookUpOptionEntity = context.LookUpOption.Where(l => l.ID == lookUpID).SingleOrDefault();

                    if (LookUpOptionEntity != null)
                    {
                        LookUpOptionEntity.DeletedOn = DateTime.Now;
                        LookUpOptionEntity.DeletedBy = SessionManager.UserName;
                        context.Entry(LookUpOptionEntity).State = System.Data.EntityState.Modified;
                        context.SaveChanges();
                    }
                    else
                    {
                        return Utility.ServiceResponse(ref LookUpOptionEntity);
                    }
                }
                return Utility.ServiceResponse(ref LookUpOptionEntity);
            }
            catch (Exception ex)
            {
                return Utility.ServiceResponse(ref LookUpOptionEntity, ex);
            }



        }


        #endregion

        #endregion
        #region NotificationsCRUD
        /// <summary>
        /// Get All NotificatioTypes
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        public EntityTransport<NotificationTypes> GetAllNotificationLists()
        {
            var NotificationTypesList = new List<NotificationTypes>();
            using (var context = new Honeycomb_Entities())
            {
                try
                {
                    NotificationTypesList = (from notiTypes in context.NotificationTypes
                                             where notiTypes.DeletedOn == null
                                             && notiTypes.DeletedBy == null
                                             select notiTypes).ToList();

                    return Utility.ServiceResponse(ref NotificationTypesList);
                }
                catch (Exception ex)
                {

                    return Utility.ServiceResponse(ref NotificationTypesList, ex);
                }

            }




        }
        /// <summary>
        /// Get notification by notiionid
        /// </summary>
        /// <param name="notificationListID"></param>
        /// <returns></returns>
        [OperationContract]
        public EntityTransport<NotificationTypes> GetAllNotificationListsByID(long notificationListID)
        {
            var NotificationTypesList = new List<NotificationTypes>();
            using (var context = new Honeycomb_Entities())
            {
                try
                {
                    NotificationTypesList = (from notiTypes in context.NotificationTypes
                                             where notiTypes.DeletedOn == null
                                             && notiTypes.ID == notificationListID
                                             && notiTypes.DeletedBy == null
                                             select notiTypes).ToList();

                    return Utility.ServiceResponse(ref NotificationTypesList);
                }
                catch (Exception ex)
                {

                    return Utility.ServiceResponse(ref NotificationTypesList, ex);
                }

            }




        }

        [OperationContract(Name = "UpdateNotificationList")]
        public EntityTransport<long> UpdateNotificationList(NotificationTypes notificationTypes)
        {
            long notificatioID = 0;

            using (var context = new Honeycomb_Entities())
            {
                try
                {
                    if (notificationTypes.ID == 0)
                    {
                        context.NotificationTypes.Add(notificationTypes);
                        notificationTypes.InsertedBy = SessionManager.UserID;
                        notificationTypes.InsertedOn = DateTime.Now;
                        notificationTypes.DeletedOn = null;
                        notificationTypes.DeletedBy = null;

                    }
                    else
                    {
                        var notiTypes = context.NotificationTypes.Where(n => n.DeletedBy == null && n.DeletedOn == null).FirstOrDefault();
                        notificationTypes.MergeInto(notiTypes, new string[]
                    {
                    "ID",
                    "Name",
                        "DeletedOn",
                        "DeletedBy",
                        "InsertedOn",
                        "InsertedBy"
                    
                    
                    }, true);

                    }
                    context.SaveChanges();
                    notificatioID = notificationTypes.ID;
                    return Utility.ServiceResponse(ref notificatioID);
                }

                catch (Exception ex)
                {

                    return Utility.ServiceResponse(ref  notificatioID, ex);
                }

            }

        }
        /// <summary>
        /// Updating a notification person
        /// </summary>
        /// <param name="notificationPerson"></param>
        /// <returns></returns>
        [OperationContract(Name = "UpdateNotificationPerson")]
        public EntityTransport<long> UpdateNotificationPerson(NotificationPerson notificationPerson)
        {
            long notificatioID = 0;

            using (var context = new Honeycomb_Entities())
            {
                try
                {
                    if (notificationPerson.ID == 0)
                    {
                        context.NotificationPerson.Add(notificationPerson);
                        notificationPerson.InsertedByID = SessionManager.UserID;
                        notificationPerson.InsertedOn = DateTime.Now;
                        notificationPerson.DeletedOn = null;
                        notificationPerson.DeletedBy = null;
                        context.SaveChanges();
                        notificatioID = notificationPerson.ID;
                    }
                    else
                    {
                        var notiTypes = context.NotificationPerson.Where(n => n.DeletedBy == null && n.DeletedOn == null ).FirstOrDefault();
                        notificationPerson.MergeInto(notiTypes, new string[]
                    {
                        "ID",                   
                        "DeletedOn",
                        "DeletedBy",
                        "InsertedOn",
                        "InsertedBy",
                        "NotificationTypeID"
                    }, true);
                        context.SaveChanges();
                        notificatioID = notificationPerson.ID;
                    }

                    return Utility.ServiceResponse(ref notificatioID);
                }

                catch (Exception ex)
                {

                    return Utility.ServiceResponse(ref  notificatioID, ex);
                }

            }

        }
        #endregion

		#region Agents & Sales Agents
		/// <summary>
		/// Gets sales agencies
		/// </summary>
		/// <returns></returns>
		[OperationContract]
		[Security("Admin,AgentsManage")]
		public EntityTransport<SalesAgent> GetSalesAgencies() {
			Utility.WCFCheckSecurityAttribute();

			List<SalesAgent> salesAgents = new List<SalesAgent>();

			try {

				salesAgents = dbContext.SalesAgent.OrderBy(o => o.Name).ToList();

				return Utility.ServiceResponse(ref salesAgents);

			}catch(Exception ex){
				return Utility.ServiceResponse(ref salesAgents, ex);
			}
		}

		/// <summary>
		/// Returns a list Agents
		/// </summary>
		/// <param name="agencyID">if null returns all agents, if long it will return the agents for the agency</param>
		/// <returns></returns>
		[OperationContract]
		[Security("Admin,AgentsManage")]
		public EntityTransport<Agent> GetSalesAgents(long? agencyID) {
			Utility.WCFCheckSecurityAttribute();

			List<Agent> salesAgents = new List<Agent>();

			try {

				if(agencyID != null) {
					salesAgents = dbContext.Agent.Where(a => a.AgencyID == agencyID && a.DeletedOn == null).OrderBy(o => o.AgentFirstName).ThenBy(o => o.AgentLastName).ToList();
				} else {
					salesAgents = dbContext.Agent.Where(a => a.DeletedOn == null).OrderBy(o => o.AgentFirstName).ThenBy(o => o.AgentLastName).ToList();
				}

				return Utility.ServiceResponse(ref salesAgents);
			}catch(Exception ex){
				return Utility.ServiceResponse(ref salesAgents, ex);
			}
		}

		/// <summary>
		/// returns an agent
		/// </summary>
		/// <param name="agentID"></param>
		/// <returns></returns>
		[OperationContract]
		[Security("Admin,AgentsManage")]
		public EntityTransport<Agent> GetSalesAgent(long agentID) {
			Utility.WCFCheckSecurityAttribute();

			Agent salesAgent = new Agent();

			try {

				salesAgent = dbContext.Agent.Where(a => a.AgentID == agentID).FirstOrDefault();

				return Utility.ServiceResponse(ref salesAgent);

			}catch(Exception ex){
				return Utility.ServiceResponse(ref salesAgent, ex);
			}
		}

// *** EDITS *** //

        /// <summary>
        /// returns a phase
        /// </summary>
        /// <param name="agentID"></param>
        /// <returns></returns>
        [OperationContract]
        [Security("Admin,AgentsManage")]
        public EntityTransport<Phase> GetPhase(long agentID)
        {
            Utility.WCFCheckSecurityAttribute();

            Phase phase = new Phase();

            try
            {

                phase = dbContext.Phase.Where(w => w.ID == agentID).FirstOrDefault();

                return Utility.ServiceResponse(ref phase);

            }
            catch (Exception ex)
            {
                return Utility.ServiceResponse(ref phase, ex);
            }
        }

        /// <summary>
        /// returns property with phases
        /// </summary>
        /// <param name="agentID"></param>
        /// <returns></returns>
        [OperationContract]
        [Security("Admin,AgentsManage")]
        public EntityTransport<Phase> GetPropertyPhase()
        {
            Utility.WCFCheckSecurityAttribute();

            List<Phase> propertyPhase = new List<Phase>();

            try
            {
                propertyPhase = dbContext.Phase.OrderBy(o => o.ID).ToList();
                return Utility.ServiceResponse(ref propertyPhase);

            }
            catch (Exception ex)
            {
                return Utility.ServiceResponse(ref propertyPhase, ex);
            }
        }

        /// <summary>
        /// Will update an agent in the system or add a new one if the id passed up is 0
        /// </summary>
        /// <param name="phase">An agent entity</param>
        /// <returns></returns>
        [OperationContract]
        [Security("Admin,AgentsManage")]
        public EntityTransport<ProcessResponse> UpdatePhase(Phase phase)
        {
            Utility.WCFCheckSecurityAttribute();
            ProcessResponse pr = new ProcessResponse();

            try
            {

                if (phase.ID == 0)
                {
                    //add new
                    phase.SystemID = CacheManager.SystemID;
                    dbContext.Phase.Add(phase);

                }
                else
                {
                    //update
                    var phaseDB = dbContext.Phase.Where(w => w.ID == phase.ID).FirstOrDefault();

                    if (phaseDB != null)
                    {
                        phase.MergeInto(phaseDB, new string[] { "ID", "SystemID" }, true);
                    }

                }

                dbContext.SaveChanges();

                pr.success = true;
                return Utility.ServiceResponse(ref pr);
            }
            catch (Exception ex)
            {
                pr.success = false;
                return Utility.ServiceResponse(ref pr, ex);
            }

        }

        

// *** EDITS END *** //

		/// <summary>
		/// returns sales agency
		/// </summary>
		/// <param name="agencyID"></param>
		/// <returns></returns>
		[OperationContract]
		[Security("Admin,AgentsManage")]
		public EntityTransport<SalesAgent> GetSalesAgency(long agencyID) {
			Utility.WCFCheckSecurityAttribute();

			SalesAgent salesAgency = new SalesAgent();

			try {
				salesAgency = dbContext.SalesAgent.Where(sa => sa.ID == agencyID).FirstOrDefault();

				return Utility.ServiceResponse(ref salesAgency);
			}catch(Exception ex){
				return Utility.ServiceResponse(ref salesAgency, ex);
			}
		}

		/// <summary>
		/// Will update a sales agency in the database or add a new one if the id passed up is 0
		/// </summary>
		/// <param name="salesAgency"></param>
		/// <returns></returns>
		[OperationContract]
		[Security("Admin,AgentsManage")]
		public EntityTransport<ProcessResponse> UpdateSalesAgency(SalesAgent salesAgency) {
			Utility.WCFCheckSecurityAttribute();
			ProcessResponse pr = new ProcessResponse();

			try {

				if(salesAgency.ID == 0) {
					//add new
					//check if there is already one with the provided name
					var salesAgencyDB = dbContext.SalesAgent.Where(sa => sa.Name == salesAgency.Name.Trim()).FirstOrDefault();

					if(salesAgencyDB == null) {
						salesAgency.SystemID = CacheManager.SystemID;
						salesAgency.Name = salesAgency.Name.Trim();
						dbContext.SalesAgent.Add(salesAgency);
						pr.success = true;
					} else {
						pr.success = false;
						pr.message = "There is already an agency with the name specified.";
					}

				} else { 
					//update
					var salesAgencyDB = dbContext.SalesAgent.Where(sa => sa.ID == salesAgency.ID).FirstOrDefault();

					if(salesAgencyDB != null) {
						salesAgency.MergeInto(salesAgencyDB, new string[] { "ID,SystemID" }, true);
						pr.success = true;
					} else {
						pr.success = false;
						pr.message = "Agency does not exist in database.";
					}
				}

				dbContext.SaveChanges();

				return Utility.ServiceResponse(ref pr);
			}catch(Exception ex){
				pr.success = false;
				pr.message = "An error occured while attmpting to save the agency.";
				return Utility.ServiceResponse(ref pr, ex);
			}

		}

		/// <summary>
		/// Will update an agent in the system or add a new one if the id passed up is 0
		/// </summary>
		/// <param name="salesAgent">An agent entity</param>
		/// <returns></returns>
		[OperationContract]
		[Security("Admin,AgentsManage")]
		public EntityTransport<ProcessResponse> UpdateSalesAgent(Agent salesAgent) {
			Utility.WCFCheckSecurityAttribute();
			ProcessResponse pr = new ProcessResponse();

			try {

				if(salesAgent.AgentID == 0) {
					//add new
					salesAgent.SystemID = CacheManager.SystemID;
					salesAgent.InsertedOn = DateTime.Now;
					salesAgent.InsertedBy = SessionManager.UserID;
					dbContext.Agent.Add(salesAgent);

				} else {
					//update
					var salesAgentDB = dbContext.Agent.Where(sa => sa.AgentID == salesAgent.AgentID).FirstOrDefault();

					if(salesAgentDB != null) {
						salesAgent.MergeInto(salesAgentDB, new string[] { "AgentID,InsertedOn,InsertedBy,SystemID" }, true);
					}

				}

				dbContext.SaveChanges();

				pr.success = true;
				return Utility.ServiceResponse(ref pr);
			} catch(Exception ex) {
				pr.success = false;
				return Utility.ServiceResponse(ref pr, ex);
			}

		}

		/// <summary>
		/// Will soft delete an agency
		/// </summary>
		/// <param name="agencyID"></param>
		/// <returns></returns>
		[OperationContract]
		[Security("Admin,AgentsManage")]
		public EntityTransport<bool> DeleteSalesAgency(long agencyID) {
			Utility.WCFCheckSecurityAttribute();
			bool success = true;

			try {
				var agency = dbContext.SalesAgent.Where(sa => sa.ID == agencyID).FirstOrDefault();

				return Utility.ServiceResponse(ref success);
			}catch(Exception ex){
				success = false;
				return Utility.ServiceResponse(ref success, ex);
			}
		}

		/// <summary>
		/// Will soft delete a sales agent
		/// </summary>
		/// <param name="agentID"></param>
		/// <returns></returns>
		[OperationContract]
		[Security("Admin,AgentsManage")]
		public EntityTransport<bool> DeleteSalesAgent(long agentID) {
			Utility.WCFCheckSecurityAttribute();

			bool success = true;

			try {
				var agent = dbContext.Agent.Where(a => a.AgentID == agentID).FirstOrDefault();

				if(agent != null) {
					agent.DeletedOn = DateTime.Now;
					agent.DeletedBy = SessionManager.UserID;

					dbContext.SaveChanges();
				}

				return Utility.ServiceResponse(ref success);

			}catch(Exception ex){
				success = false;
				return Utility.ServiceResponse(ref success, ex);
			}
		}

		#endregion	

		#region Message Templates
		/// <summary>
		/// This method will retrieve all the message templates in the system. 
		/// </summary>
		/// <returns>A list of type MessageTemplate</returns>
		[OperationContract]
		[SecurityAttribute("Admin,SystemSettingsManage")]
		public EntityTransport<MessageTemplate> GetMessageTemplates(PaginationContainer pagination) {
			Utility.WCFCheckSecurityAttribute();
			List<MessageTemplate> messageTemplates = new List<MessageTemplate>();

			try {

				using(var context = new Honeycomb_Entities()){
					messageTemplates = context.MessageTemplate.OrderBy(o => o.Name).ToList();
				}

				var paginatedResult = messageTemplates.AsQueryable().Paginate(pagination.CurrentPage, pagination.PageSize, pagination.OrderBy).ToList();
				return Utility.ServiceResponse(ref paginatedResult, messageTemplates.Count);
			}catch(Exception ex){
				return Utility.ServiceResponse(ref messageTemplates, ex);
			}
		}

		/// <summary>
		/// This will retreive a message template by it's id
		/// </summary>
		/// <param name="messageTemplateID"></param>
		/// <returns>Entity of type MessageTemplate</returns>
		[OperationContract]
		[SecurityAttribute("Admin,SystemSettingsManage")]
		public EntityTransport<MessageTemplate> GetMessageTemplate(long messageTemplateID) {
			Utility.WCFCheckSecurityAttribute();
			MessageTemplate messageTemplate = new MessageTemplate();

			try {
				using(var context = new Honeycomb_Entities()){
					messageTemplate = context.MessageTemplate.Where(m => m.ID == messageTemplateID).FirstOrDefault();
				}

				return Utility.ServiceResponse(ref messageTemplate);
			}catch(Exception ex){
				return Utility.ServiceResponse(ref messageTemplate, ex);
			}

		}

		/// <summary>
		/// This method will update a provided MessageTemplate, it does not behave like standard honeycomb update methods in that it will not add a new MessageTemplate if the ID = 0
		/// </summary>
		/// <param name="messageTemplate">An Object of type MessageTemplate</param>
		/// <returns></returns>
		[OperationContract]
		[Security("Admin,SystemSettingsManage")]
		public EntityTransport<ProcessResponse> UpdateMessageTemplate(MessageTemplate messageTemplate) {
			Utility.WCFCheckSecurityAttribute();
			ProcessResponse pRes = new ProcessResponse();

			try {
				
				using(var context = new Honeycomb_Entities()){
					//get the message out of the DB
					var messageTemplateDB = context.MessageTemplate.Where(mt => mt.ID == messageTemplate.ID).FirstOrDefault();

					if(messageTemplateDB != null) {

						if(messageTemplate.Message.Trim() != "") {

							messageTemplate.MergeInto(messageTemplateDB, new string[] {
							"Type",							
							"ID",
							"Placeholders",
							"Name"
						}, true);

							context.SaveChanges();

							pRes.success = true;
						} else {
							pRes.success = false;
							pRes.message = "You cannot supply a blank message! Please provide a message.";
						}

					} else {
						pRes.success = false;
						pRes.message = "The Message Template could not be found in the database.";
					}
				}

				return Utility.ServiceResponse(ref pRes);
			}catch(Exception ex){
				pRes.success = false;
				pRes.message = "An error occured while attempting to update the Message Template.";
				return Utility.ServiceResponse(ref pRes, ex);
			}
		}

		#endregion
	}
}