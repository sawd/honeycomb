﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Honeycomb.Custom;

namespace Honeycomb.Public {
    public partial class C3FCD3D76192E4007DFB496CCA67E13B : System.Web.UI.Page {
        //set to true to stop bulksms from repeating the request
        string success = "false";

        protected void Page_Load(object sender, EventArgs e) {

            string MobileNumber = Request.QueryString ["mobile"];
            string VisitorNameQS = Request.QueryString ["message"];
            string securityToken = Request.QueryString ["bobo"];

            if(!string.IsNullOrEmpty(MobileNumber) && !string.IsNullOrEmpty(VisitorNameQS) && !string.IsNullOrEmpty(securityToken)) {

                Custom.Data.Occupant occupant = null;
                Custom.Security security = new Custom.Security();
                List<Custom.Data.AccessVisitor> visitorsList = new List<Custom.Data.AccessVisitor>();

                MobileNumber = "0" + MobileNumber.Remove(0, 2);

                if(VisitorNameQS.Contains("simbithi ")) {
                    VisitorNameQS = VisitorNameQS.Replace("simbithi ", "");
                } else if(VisitorNameQS.Contains("Simbithi ")) {
                    VisitorNameQS = VisitorNameQS.Replace("Simbithi ", "");
                }

                if(securityToken == null) {
                    securityToken = "no";
                    success = "true";
                }

                using(var context = new Honeycomb.Custom.Data.Honeycomb_Entities()) {
                    occupant = context.Occupant.Where(o => o.Cellphone.ToLower() == MobileNumber && o.DeletedOn == null && o.PropertyOccupant.Any(po => po.ArchivedOn == null && po.Property.DeletedOn == null)).FirstOrDefault();

                    if(occupant != null && securityToken == "XDNLILHDL784954dgfd98d") {
                        //occupant exisits, send through the requested codes

                        if(occupant.CanSMS) {

                            visitorsList.Add(new Custom.Data.AccessVisitor {
                                ID = Guid.NewGuid(),
                                VisitorName = VisitorNameQS
                            });

                            var visitorAccessCodes = security.CreateAccesCodes(visitorsList, "OccupantSMS", occupant.ID, DateTime.Now, CacheManager.SystemSetting.SendCodeToOccupant, false);

                            if(visitorAccessCodes.Count() > 0) {
                                success = "true";
                            }

                        } else {
                            success = "true";
                        }
                    } else {
                        success = "true";
                    }
                }
                Response.Write(success);
                Response.End();
            }
        }
    }
}