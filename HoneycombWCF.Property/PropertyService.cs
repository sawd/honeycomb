﻿#region Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using System.Data.Entity;
using System.Data.Linq;
using System.Data.Objects;
using Honeycomb.Custom;
using System.Collections;
using System.ComponentModel;
using Honeycomb.Custom.Data;
using System.Data.Entity.Infrastructure;
using System.ComponentModel.DataAnnotations;

#endregion

namespace Honeycomb.Property {
	//[ServiceOutputBehavior]
	[ServiceContract(Namespace = "PropertyService")]
	[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
	public class WCFProperty {

		#region common methods
		/// <summary>
		/// A single function which utilises the unified lookup structure, accepts an id of the specific lookup list to retrieve
		/// </summary>
		/// <param name="LookUpListID"></param>
		/// <returns></returns>
		[OperationContract]
		public EntityTransport<LookUpOption> GetLookUpOptions(long LookUpListID) {
			var LookUpOptions = new List<LookUpOption>();

			try {

				using(var context = new Honeycomb_Entities()) {
					LookUpOptions = context.LookUpOption.Where(lo => lo.LookUpID == LookUpListID).OrderBy(o => o.Name).ToList();
				}

				return Utility.ServiceResponse(ref LookUpOptions);
			} catch(Exception ex) {
				return Utility.ServiceResponse(ref LookUpOptions, ex);
			}
		}


		[OperationContract]
		public EntityTransport<bool> PropertyHasTenants(long PropertyID) {
			bool ActivateLease = false;

			try {

				using(var context = new Honeycomb_Entities()) {
					ActivateLease = (context.Occupant.Where(o => o.PropertyOccupant.Any(po => po.PropertyID == PropertyID && po.ArchivedOn == null) && o.IsTenant && o.DeletedOn == null).Count() > 0);
				}

				return Utility.ServiceResponse(ref ActivateLease);
			} catch(Exception ex) {
				ActivateLease = false;
				return Utility.ServiceResponse(ref ActivateLease, ex);
			}
		}

		/// <summary>
		/// Gets the current settings for the system
		/// </summary>
		/// <param name="systemID"></param>
		/// <returns></returns>
		[OperationContract]
		public EntityTransport<Custom.Data.SystemSetting> GetSystemSettings() {

			var systemSettings = new Custom.Data.SystemSetting();

			try {
				systemSettings = CacheManager.SystemSetting;

				return Utility.ServiceResponse(ref systemSettings);
			} catch(Exception ex) {
				return Utility.ServiceResponse(ref systemSettings, ex);
			}
		}

		/// <summary>
		/// Gets a list of Sales Agents from a lookup table
		/// </summary>
		/// <returns>A list of SalesAgents</returns>
		[OperationContract]
		public EntityTransport<Custom.Data.SalesAgent> GetSalesAgents() {
			var salesAgents = new List<Custom.Data.SalesAgent>();

			try {
				using(var context = new Custom.Data.Honeycomb_Entities()) {
					salesAgents = context.SalesAgent.Where(sa => sa.SystemID == SessionManager.SystemID).ToList();
				}

				return Utility.ServiceResponse(ref salesAgents);

			} catch(Exception ex) {
				return Utility.ServiceResponse(ref salesAgents, ex);
			}
		}

		/// <summary>
		/// Gets a list of Sales Agents from a lookup table
		/// </summary>
		/// <returns>A list of SalesAgents</returns>
		[OperationContract]
		public EntityTransport<Custom.Data.Agent> GetAgents() {
			var salesAgents = new List<Custom.Data.Agent>();

			try {
				using(var context = new Custom.Data.Honeycomb_Entities()) {
					salesAgents = context.Agent.ToList();
				}
				return Utility.ServiceResponse(ref salesAgents);
			} catch(Exception ex) {
				return Utility.ServiceResponse(ref salesAgents, ex);
			}
		}

		/// <summary>
		/// Gets a list of agents for an agency
		/// </summary>
		/// <returns>A list of SalesAgents</returns>
		[OperationContract]
		public EntityTransport<Custom.Data.Agent> GetAgencyAgents(long AgencyID) {
			var salesAgents = new List<Custom.Data.Agent>();

			try {
				using(var context = new Custom.Data.Honeycomb_Entities()) {
					salesAgents = context.Agent.Where(a => a.AgencyID == AgencyID).ToList();
				}
				return Utility.ServiceResponse(ref salesAgents);
			} catch(Exception ex) {
				return Utility.ServiceResponse(ref salesAgents, ex);
			}
		}
		#endregion

		#region Erven
		[OperationContract]
		[Security("Admin,ManageErven,PropertyView")]
		public EntityTransport<Custom.Data.Erf> GetErven() {

			Utility.WCFCheckSecurityAttribute();

			var erven = new List<Custom.Data.Erf>();
			try {
				using(var context = new Honeycomb.Custom.Data.Honeycomb_Entities()) {
					erven = context.Erf.Where(w => w.DeletedOn == null && w.SystemID == CacheManager.SystemID).OrderBy(o => o.ErfNumber).ToList();
				}
				return Utility.ServiceResponse(ref erven, erven.Count);
			} catch(Exception ex) {
				return Utility.ServiceResponse(ref erven, ex);
			}

		}

		/// <summary>
		/// Gets a DDO containing erf info including whether or not it contains properties
		/// </summary>
		/// <param name="erfID"></param>
		/// <returns></returns>
		[OperationContract]
		[Security("Admin,ManageErven")]
		public EntityTransport<Custom.Data.ErfDDO> GetErf(long erfID) {

			Utility.WCFCheckSecurityAttribute();

			var erf = new Custom.Data.ErfDDO();
			try {

				using(var context = new Custom.Data.Honeycomb_Entities()) {
					erf = (from er in context.Erf.Include("Property")
						   where er.ID == erfID
						   select new Custom.Data.ErfDDO {
							   ID = er.ID,
							   Name = er.Name,
							   Description = er.Description,
							   ErfTypeID = er.ErfTypeID,
							   ErfNumber = er.ErfNumber,
							   HasProperties = er.Property.Count() > 0 ? true : false
						   }).SingleOrDefault();
				}

				return Utility.ServiceResponse(ref erf);
			} catch(Exception ex) {
				return Utility.ServiceResponse(ref erf, ex);
			}
		}

		/// <summary>
		/// takes the erf and saves it
		/// </summary>
		/// <param name="erf"></param>
		/// <returns></returns>
		[OperationContract]
		[Security("Admin,ManageErven")]
		public EntityTransport<long> UpdateErf(Custom.Data.Erf erf) {

			Utility.WCFCheckSecurityAttribute();

			long retID = erf.ID;

			try {

				using(var context = new Custom.Data.Honeycomb_Entities()) {

					if(erf.ID == 0) {
						//it's a new erf, add it
						context.Erf.Add(erf);
						erf.InsertedBy = SessionManager.UserName;
						erf.InsertedByID = SessionManager.UserID;
						erf.InsertedOn = DateTime.Now;
						erf.SystemID = CacheManager.SystemID;
					} else {
						//it's an existing erf, update it
						var erfDB = context.Erf.Where(er => er.ID == erf.ID).FirstOrDefault();
						erf.MergeInto(erfDB, new string[]
                        {"ID",
                        "DeletedBy",
                        "DeletedOn",
                        "DeletedByID",
                        "InsertedOn",
                        "InsertedBy",
                        "InsertedByID",
                        "SystemID"}, true);
					}

					context.SaveChanges();
				}
				retID = erf.ID;

				return Utility.ServiceResponse(ref retID);
			} catch(Exception ex) {
				return Utility.ServiceResponse(ref retID, ex);
			}
		}


		[OperationContract]
		[Security("Admin,PropertyView")]
		public EntityTransport<Custom.Data.PropertyDescription> GetErfScheme(Int64 erfID) {

			Utility.WCFCheckSecurityAttribute();

			var erf = new List<Custom.Data.PropertyDescription>();
			try {
				using(var context = new Honeycomb.Custom.Data.Honeycomb_Entities()) {
					erf = context.GetErfScheme(erfID).ToList();
				}
				return Utility.ServiceResponse(ref erf, erf.Count);
			} catch(Exception ex) {
				return Utility.ServiceResponse(ref erf, ex);
			}

		}

		/// <summary>
		/// Gets a custom object containing Erven joined on Erf Type
		/// </summary>
		/// <param name="pagination"></param>
		/// <returns></returns>
		[OperationContract]
		[Security("Admin,ManageErven")]
		public EntityTransport<Custom.Data.ErvenAndTypes> GetErvenAndTypes(PaginationContainer pagination) {

			Utility.WCFCheckSecurityAttribute();

			var erven = new List<Custom.Data.ErvenAndTypes>();
			try {
				using(var context = new Honeycomb.Custom.Data.Honeycomb_Entities()) {
					erven = (from e in context.Erf
							 join et in context.ErfType on e.ErfTypeID equals et.ID
							 where e.DeletedOn == null
							 && e.SystemID == CacheManager.SystemID
							 select new Custom.Data.ErvenAndTypes {
								 Name = e.Name,
								 ID = e.ID,
								 ErfNumber = e.ErfNumber,
								 ErfType = et.Name
							 }).ToList();
				}

				var paginatedResult = erven.AsQueryable().Paginate(pagination.CurrentPage, pagination.PageSize, pagination.OrderBy).ToList();

				return Utility.ServiceResponse(ref paginatedResult, erven.Count);
			} catch(Exception ex) {
				return Utility.ServiceResponse(ref erven, ex);
			}

		}

		/// <summary>
		/// Get all the erf types
		/// </summary>
		/// <returns></returns>
		[OperationContract]
		[Security("Admin,ManageErven")]
		public EntityTransport<Custom.Data.ErfType> GetErfTypes() {

			Utility.WCFCheckSecurityAttribute();

			var erfTypes = new List<Custom.Data.ErfType>();
			try {

				using(var context = new Custom.Data.Honeycomb_Entities()) {
					erfTypes = context.ErfType.ToList();
				}

				return Utility.ServiceResponse(ref erfTypes);

			} catch(Exception ex) {
				return Utility.ServiceResponse(ref erfTypes, ex);
			}
		}

		/// <summary>
		/// Delete an erf if there are no properties assigned to it
		/// </summary>
		/// <param name="erfID"></param>
		/// <returns></returns>
		[OperationContract]
		[Security("Admin,ManageErven")]
		public EntityTransport<bool> DeleteErf(long erfID) {

			Utility.WCFCheckSecurityAttribute();

			var erf = new Custom.Data.Erf();
			var erfProperties = new List<Custom.Data.Property>();
			int propertiesCount = 0;
			bool deleted = false;
			try {

				Utility.CheckSession();

				using(var context = new Custom.Data.Honeycomb_Entities()) {
					erf = context.Erf.Where(e => e.ID == erfID).FirstOrDefault();
					propertiesCount = (from prop in context.Property
									   join er in context.Erf on prop.ERFID equals er.ID
									   where er.ID == erfID
									   && prop.DeletedOn == null
									   select prop).Count();

					if(!(propertiesCount > 0)) {
						erf.DeletedOn = DateTime.Now;
						erf.DeletedByID = SessionManager.UserID;
						erf.DeletedBy = SessionManager.UserName;

						context.SaveChanges();

						deleted = true;
					}
				}

				return Utility.ServiceResponse(ref deleted);
			} catch(Exception ex) {
				return Utility.ServiceResponse(ref deleted, ex);
			}
		}

		#endregion

		#region Property
		/// <summary>
		/// Returns a list of properties single residential properties
		/// </summary>
		/// <returns>a paginated entitytransport object</returns>
		/// <roles><role>Admin = admin</role></roles>
		[OperationContract]
		[Security("Admin,PropertyView")]
		public EntityTransport<Custom.Data.CustomPropertyOwner> GetSingleResidentProperties(PaginationContainer pagination, string filter = null, string occupantFilter = null) {

			Utility.WCFCheckSecurityAttribute();
			filter = filter.Trim();

			var properties = new List<Custom.Data.CustomPropertyOwner>();
			try {

				using(var context = new Honeycomb.Custom.Data.Honeycomb_Entities()) {
					properties = (from owners in context.Ownership
								  join property in context.Property on owners.PropertyID equals property.ID
								  join erven in context.Erf on property.ERFID equals erven.ID
								  where property.PropertyTypeID == 10000
								  && property.DeletedOn == null
								  && property.DeletedBy == null
								  && owners.ArchivedOn == null
								  select new Custom.Data.CustomPropertyOwner {
									  ContactNumber = owners.Cell,
									  ERFNumber = erven.ErfNumber,
									  Email = owners.Email,
									  OwnerID = owners.ID,
									  PropertyID = property.ID,
									  StreetName = property.StreetName,
									  StreetNumber = property.StreetNumber,
									  Phase = property.PhaseID,
									  PrincipalOwnerName = owners.PrincipalOwnerName
								  }
								 ).ToList();

					properties = properties.AsQueryable().OptionalWhere(!string.IsNullOrEmpty(filter), p => (p.PrincipalOwnerName.Trim().ToLower().Contains(filter.ToLower()) || p.Email.Trim().ToLower().Contains(filter.ToLower()) || p.ERFNumber.Trim() == filter)).ToList();

					//check for matches on occupant name
					if(!string.IsNullOrEmpty(occupantFilter)) {
						List<long> propertyOccupants = context.Property.Where(p => p.PropertyOccupant.Any(po => po.ArchivedOn == null && ((po.Occupant.FirstName.ToLower().Trim() + " " + po.Occupant.LastName.ToLower().Trim()).Contains(occupantFilter.ToLower().Trim()) || po.Occupant.Email.ToLower().Contains(occupantFilter)))).Select(s => s.ID).ToList();
						properties = properties.AsQueryable().Where(p => propertyOccupants.Contains(p.PropertyID)).ToList();
					}

				}
				var paginatedResult = properties.AsQueryable().Paginate(pagination.CurrentPage, pagination.PageSize, pagination.OrderBy).ToList();
				return Utility.ServiceResponse(ref paginatedResult, properties.Count);
			} catch(Exception ex) {
				return Utility.ServiceResponse(ref properties, ex);
			}

		}

		/// <summary>
		/// Returns a list of sectional scheme properties
		/// </summary>
		/// <returns>a paginated entitytransport object</returns>
		/// <roles><role>Admin = admin</role></roles>
		[OperationContract]
		[Security("Admin,PropertyView")]
		public EntityTransport<Custom.Data.CustomPropertyOwner> GetSectionalSchemeProperties(PaginationContainer pagination, string filter = null, string occupantFilter = null) {

			Utility.WCFCheckSecurityAttribute();
			filter = filter.Trim();
			occupantFilter = occupantFilter.Trim();

			var properties = new List<Custom.Data.CustomPropertyOwner>();
			try {
				using(var context = new Honeycomb.Custom.Data.Honeycomb_Entities()) {
					properties = (from owners in context.Ownership
								  join property in context.Property on owners.PropertyID equals property.ID
								  join erven in context.Erf on property.ERFID equals erven.ID
								  where property.PropertyTypeID == 10002
								  && property.DeletedOn == null
								  && property.DeletedBy == null
								  && owners.ArchivedOn == null
								  select new Custom.Data.CustomPropertyOwner {
									  ContactNumber = owners.Cell,
									  ERFNumber = erven.ErfNumber,
									  OwnerID = owners.ID,
									  Email = owners.Email,
									  PropertyID = property.ID,
									  StreetName = property.StreetName,
									  StreetNumber = property.StreetNumber,
									  Phase = property.PhaseID,
									  PrincipalOwnerName = owners.PrincipalOwnerName
								  }
								 ).ToList();

					properties = properties.AsQueryable().OptionalWhere(!string.IsNullOrEmpty(filter), p => (p.PrincipalOwnerName.Trim().ToLower().Contains(filter.ToLower()) || p.ERFNumber.Trim() == filter)).ToList();

					//check for matches on occupant name
					if(!string.IsNullOrEmpty(occupantFilter)) {
						List<long> propertyOccupants = context.Property.Where(p => p.PropertyOccupant.Any(po => po.ArchivedOn == null && ((po.Occupant.FirstName.ToLower().Trim() + " " + po.Occupant.LastName.ToLower().Trim()).Contains(occupantFilter.ToLower().Trim())))).Select(s => s.ID).ToList();
						properties = properties.AsQueryable().Where(p => propertyOccupants.Contains(p.PropertyID)).ToList();
					}

				}
				var paginatedResult = properties.AsQueryable().Paginate(pagination.CurrentPage, pagination.PageSize, pagination.OrderBy).ToList();
				return Utility.ServiceResponse(ref paginatedResult, properties.Count);
			} catch(Exception ex) {
				return Utility.ServiceResponse(ref properties, ex);
			}

		}

		/// <summary>
		/// Returns a list of sectional title properties
		/// </summary>
		/// <returns>a paginated entitytransport object</returns>
		/// <roles><role>Admin = admin</role></roles>
		[OperationContract]
		[Security("Admin,PropertyView")]
		public EntityTransport<Custom.Data.CustomPropertyOwner> GetSectionTitleProperties(PaginationContainer pagination, string filter = null, string occupantFilter = null, string complexIDList = null) {

			Utility.WCFCheckSecurityAttribute();

			List<long> complexIDIntList = null;
			filter = filter.Trim();
			occupantFilter = occupantFilter.Trim();

			if(!string.IsNullOrEmpty(complexIDList))
				complexIDIntList = new List<long>(complexIDList.Split(',').Select(long.Parse));

			var properties = new List<Custom.Data.CustomPropertyOwner>();

			try {
				using(var context = new Honeycomb.Custom.Data.Honeycomb_Entities()) {
					properties = (from owners in context.Ownership
								  join property in context.Property on owners.PropertyID equals property.ID
								  join erven in context.Erf on property.ERFID equals erven.ID
								  from scheme in context.Ownership.Where(s => s.PropertyID == property.SchemeID).DefaultIfEmpty()
								  where property.PropertyTypeID == 10001
								  && property.DeletedOn == null
								  && property.DeletedBy == null
								  && owners.ArchivedOn == null
								  select new Custom.Data.CustomPropertyOwner {
									  ContactNumber = owners.Cell,
									  ERFNumber = (property.SectionNumber == null) ? erven.ErfNumber : erven.ErfNumber + "/" + property.SectionNumber,
									  OwnerID = owners.ID,
									  Email = owners.Email,
									  PropertyID = property.ID,
									  StreetName = property.StreetName,
									  StreetNumber = property.StreetNumber,
									  Phase = property.PhaseID,
									  SchemeID = property.SchemeID,
									  Complex = ((scheme == null) ? owners.PropertyName : scheme.PropertyName),
									  DoorNumber = property.DoorNumber,
									  SectionNumber = property.SectionNumber,
									  PrincipalOwnerName = owners.PrincipalOwnerName
								  }
								 ).ToList();

					properties = properties.AsQueryable().OptionalWhere(!string.IsNullOrEmpty(filter), p => (p.PrincipalOwnerName.Trim().ToLower().Contains(filter.ToLower()) || p.Email.Trim().ToLower().Contains(filter.ToLower()) || p.ERFNumber.Trim() == filter)).ToList();
					properties = properties.AsQueryable().OptionalWhere(complexIDIntList != null, p => complexIDIntList.Contains(Utility.ConvertNull(p.SchemeID, 0))).ToList();

					//check for matches on occupant name
					if(!string.IsNullOrEmpty(occupantFilter)) {
						List<long> propertyOccupants = context.Property.Where(p => p.PropertyOccupant.Any(po => po.ArchivedOn == null && ((po.Occupant.FirstName.ToLower().Trim() + " " + po.Occupant.LastName.ToLower().Trim()).Contains(occupantFilter.ToLower().Trim()) || po.Occupant.Email.ToLower().Trim().Contains(occupantFilter.ToLower())))).Select(s => s.ID).ToList();
						properties = properties.AsQueryable().Where(p => propertyOccupants.Contains(p.PropertyID)).ToList();
					}

				}
				var paginatedResult = properties.AsQueryable().Paginate(pagination.CurrentPage, pagination.PageSize, pagination.OrderBy).ToList();
				return Utility.ServiceResponse(ref paginatedResult, properties.Count);
			} catch(Exception ex) {
				return Utility.ServiceResponse(ref properties, ex);
			}

		}

		[OperationContract]
		[Security("Admin,PropertyView")]
		public EntityTransport<Custom.Data.CustomPropertyOwner> GetAllPropertyOwners() {

			Utility.WCFCheckSecurityAttribute();

			var propertyOwners = new List<Custom.Data.CustomPropertyOwner>();
			try {
				using(var context = new Custom.Data.Honeycomb_Entities()) {
					propertyOwners = (from owners in context.Ownership
									  join property in context.Property on owners.PropertyID equals property.ID
									  join erven in context.Erf on property.ERFID equals erven.ID
									  where owners.DeletedOn == null && owners.ArchivedOn == null
									  select new Custom.Data.CustomPropertyOwner {
										  ContactNumber = owners.Cell,
										  ERFNumber = erven.ErfNumber,
										  Email = owners.Email,
										  OwnerID = owners.ID,
										  PropertyID = property.ID,
										  StreetName = property.StreetName,
										  StreetNumber = property.StreetNumber,
										  Phase = property.PhaseID,
										  PrincipalOwnerName = owners.PrincipalOwnerName
									  }
									 ).ToList();
				}

				return Utility.ServiceResponse(ref propertyOwners);
			} catch(Exception ex) {
				return Utility.ServiceResponse(ref propertyOwners, ex);
			}
		}

		/// <summary>
		/// Updates the property or adds a new one. Saves the owerships details as well
		/// </summary>
		/// <param name="propertyEntity"></param>
		/// <param name="owner"></param>
		/// <returns></returns>
		[OperationContract]
		[Security("Admin,PropertyEdit,PropertyAdd")]
		public EntityTransport<long []> UpdateProperties(Custom.Data.Property propertyEntity, Ownership owner, Lease lease) {

			Utility.WCFCheckSecurityAttribute();

			long[] idList;

			try {

				using(var context = new Honeycomb.Custom.Data.Honeycomb_Entities()) {

					//save the new property
					if(propertyEntity.ID == 0) {
						context.Property.Add(propertyEntity);
						propertyEntity.SystemID = CacheManager.SystemID;
						propertyEntity.InsertedOn = DateTime.Now;
						propertyEntity.InsertedBy = SessionManager.UserID;
						List<Ownership> owners = new List<Ownership> { owner };
						propertyEntity.Ownership = owners;
					} else {

						//update the property
						var dbProperty = context.Property.Include(i => i.Ownership).Include("Lease").Where(w => w.ID == propertyEntity.ID).FirstOrDefault();
						propertyEntity.MergeInto(dbProperty, new string[] 
                            {"ID",
								"Ownership",
                            "PropertyTypeID",
							"Lease",
                            "SystemID",
                            "ERFID",
                            "DeletedOn",
                            "DeletedBy",
                            "InsertedOn",
                            "InsertedBy"}, true);

						#region ownership save
						if(owner.ID == 0) {
							//List<Ownership> owners = new List<Ownership> { owner };
							//dbProperty.Ownership = owners;
							owner.InsertedOn = DateTime.Now;
							owner.InsertedBy = SessionManager.UserID;
							dbProperty.Ownership.Add(owner);
							
						} else {
							//save the ownership details, join on property to maintain the relationship
							var dbOwnershipAndProperty = (from ow in context.Ownership
														  join prop in context.Property
														  on ow.PropertyID equals prop.ID
														  where ow.ID == owner.ID
														  select ow).FirstOrDefault();

							//merge into the db entity but make sure to ignore the Property entity in the object otherwise it will overite it and not associate the owner with the property.
							owner.MergeInto(dbOwnershipAndProperty, new string[]
                            {"ID",
                             "Property",
                             "PropertyID",
                             "SystemID",
                             "InsertedOn",
                             "InsertedBy",
                             "DeletedOn",
                             "DeletedBy",
                             "ArchivedOn",
                             "ArchivedBy"}, true);
						}
						#endregion

						#region lease save
						//check if the property can have a lease assigned to it
						if(PropertyHasTenants(dbProperty.ID).EntityList[0]) {
							//it can, set it.
							//check if the property already has a lease assigned
							bool PropertyHasLease = (context.Property.Where(p => p.Lease.PropertyID == dbProperty.ID).Count() > 0);

							if(PropertyHasLease) {
								//yes it already has a lease, merge and update ignoring certain fields, join on property to maintain relationship
								var dbLease = (from l in context.Lease
											   join p in context.Property on l.PropertyID equals p.ID
											   where p.ID == dbProperty.ID
											   select l).FirstOrDefault();

								lease.MergeInto(dbLease, new string[] { "NotificationLastSendOn","Property","TerminationDate","TerminationReason","PropertyID" }, true);

								dbLease.Property = dbProperty;
								dbProperty.Lease = dbLease;

							} else {
								//there is no current lease, set it
								dbProperty.Lease = lease;
							}
						}

						#endregion
						//TODO: call the method to check the date of transfer, if there is a change then send an email to 

					}
					context.SaveChanges();
				}
				
				idList = new long[2];
				idList[0] = propertyEntity.ID;
				idList[1] = owner.ID;

				return Utility.ServiceResponse(ref idList);
			} catch(Exception ex) {
				idList = new long[1];
				idList[0] = 0;
				return Utility.ServiceResponse(ref idList, ex);
			}
		}

		/// <summary>
		/// Gets the property date through a stored procedure
		/// </summary>
		/// <param name="propertyID"></param>
		/// <returns></returns>
		[OperationContract]
		[Security("Admin,PropertyEdit,PropertyView")]
		public EntityTransport<Custom.Data.PropertyDescription> GetProperty(long? propertyID) {

			Utility.WCFCheckSecurityAttribute();

            Utility.WCFCheckSecurityAttribute();

			EntityTransport<Custom.Data.PropertyDescription> dataResponse;
			Custom.Data.PropertyDescription propertyEntity = new Custom.Data.PropertyDescription();

			try {
				Utility.CheckSession();
				using(var context = new Honeycomb_Entities()) {
					propertyEntity = context.GetPropertyDescription(propertyID).FirstOrDefault();
				}
				dataResponse = Utility.ServiceResponse(ref propertyEntity);
			} catch(Exception ex) {
				dataResponse = Utility.ServiceResponse(ref propertyEntity, ex);
			}


			return dataResponse;

		}


		/// <summary>
		/// Marks a property as deleted
		/// </summary>
		/// <returns></returns>
		[OperationContract]
		[Security("Admin,PropertyEdit")]
		public EntityTransport<ProcessResponse> DeleteProperty(long propertyID) {

			Utility.WCFCheckSecurityAttribute();

			var pRes = new ProcessResponse();

			try {
				using(var context = new Honeycomb.Custom.Data.Honeycomb_Entities()) {
					var propertyEntity = context.Property
												.Include(po => po.PropertyOccupant)
												.Include(i => i.Pet)
												.Include(i => i.Vehicle)
												.Include(i => i.GolfCart)
												.Include(i => i.Staff)
												.Include("Staff.Property")
												.Where(p => p.ID == propertyID).SingleOrDefault();
					var allowDelete = true;

					if(propertyEntity != null) {

						//if sectional scheme do a check to see if any properties are linked to this property
						if(propertyEntity.PropertyTypeID == 10002 && context.Property.Where(p => p.PropertyTypeID == 10001 && p.SchemeID == propertyEntity.ID && p.DeletedOn == null && p.Ownership.Any(own => own.ArchivedOn == null)).Count() > 0) {
							allowDelete = false;
							pRes.message = "The sectional scheme has properties associated with it. You cannot delete this scheme until you have removed the associated properties.";
						}

						if(allowDelete) {
							propertyEntity.DeletedOn = DateTime.Now;
							propertyEntity.DeletedBy = SessionManager.UserID;
							context.Entry(propertyEntity).State = System.Data.EntityState.Modified;

							foreach(var pet in propertyEntity.Pet) {
								if(pet.DeletedOn == null) {
									pet.DeletedOn = DateTime.Now;
									pet.DeletedByID = SessionManager.UserID;
									pet.DeletedBy = SessionManager.UserName;
								}
							}

							foreach(var vechicle in propertyEntity.Vehicle) {
								if(vechicle.DeletedOn == null) {
									vechicle.DeletedOn = DateTime.Now;
									vechicle.DeletedBy = SessionManager.UserName;
									vechicle.DeletedByID = SessionManager.UserID;
								}
							}

							foreach(var cart in propertyEntity.GolfCart) {
								if(cart.DeletedOn == null) {
									cart.DeletedOn = DateTime.Now;
									cart.DeletedBy = SessionManager.UserName;
									cart.DeletedByID = SessionManager.UserID;
								}
							}

							foreach(var occupant in propertyEntity.PropertyOccupant) {
								if(occupant.ArchivedOn == null) {
									occupant.ArchivedOn = DateTime.Now;
								}
							}

							//TODO: test this before uncommenting and activating it.
							//Custom.Security.TerminateAccess(propertyEntity.Staff.ToList());

							foreach(var staff in propertyEntity.Staff) {
								if(staff.Property.Count() == 1) {
									staff.DeletedOn = DateTime.Now;
									staff.DeletedBy = SessionManager.UserName;
									staff.DeleteByID = SessionManager.UserID;
								}
							}

							context.SaveChanges();

							pRes.success = true;
							pRes.message = "The specified property has beeen deleted.";
						}

					} else {
						pRes.success = false;
						pRes.message = "The specified property does not exist";
						return Utility.ServiceResponse(ref pRes);
					}

				}
				return Utility.ServiceResponse(ref pRes);
			} catch(Exception ex) {
				pRes.success = false;
				pRes.message = "There was an error when attempting to delete the property.";
				return Utility.ServiceResponse(ref pRes, ex);
			}
		}

		/// <summary>
		/// List of all property types
		/// </summary>
		/// <returns></returns>
		[OperationContract]
		[Security("Admin,PropertyView")]
		public EntityTransport<Custom.Data.PropertyType> GetPropertyTypes() {

			Utility.WCFCheckSecurityAttribute();

			var propertTypes = new List<Custom.Data.PropertyType>();
			try {
				using(var context = new Custom.Data.Honeycomb_Entities()) {
					propertTypes = context.PropertyType.Where(pt => pt.SystemID == SessionManager.SystemID).ToList();
				}
				return Utility.ServiceResponse(ref propertTypes);
			} catch(Exception ex) {
				return Utility.ServiceResponse(ref propertTypes, ex);
			}
		}

		/// <summary>
		/// List of all property groups
		/// </summary>
		/// <returns></returns>
		[OperationContract]
		[Security("Admin,PropertyView")]
		public EntityTransport<DropDownOption> GetPropertyGroups() {
			Utility.WCFCheckSecurityAttribute();

			var propertGroups = new List<DropDownOption>();
			try {
				using(var context = new Custom.Data.Honeycomb_Entities()) {
					propertGroups = (from owners in context.Ownership
									 join props in context.Property on owners.PropertyID equals props.ID
									 where props.DeletedOn == null &&
											owners.ArchivedOn == null &&
											owners.DeletedOn == null &&
											props.PropertyTypeID == 10002 &&
											props.SchemeID == null
									 select new DropDownOption {
										 ID = props.ID,
										 Name = owners.PropertyName
									 }).ToList();
				}
				return Utility.ServiceResponse(ref propertGroups);
			} catch(Exception ex) {
				return Utility.ServiceResponse(ref propertGroups, ex);
			}
		}

		/// <summary>
		/// Gets management associations
		/// </summary>
		/// <returns></returns>
		[OperationContract]
		[Security("Admin,PropertyView")]
		public EntityTransport<Custom.Data.ManagementAssociation> GetManagementAssociations() {

			Utility.WCFCheckSecurityAttribute();

			var associations = new List<Custom.Data.ManagementAssociation>();
			try {
				using(var context = new Custom.Data.Honeycomb_Entities()) {
					associations = context.ManagementAssociation.Where(mass => mass.SystemID == SessionManager.SystemID).ToList();
					return Utility.ServiceResponse(ref associations);
				}
			} catch(Exception ex) {
				return Utility.ServiceResponse(ref associations, ex);
			}
		}
		#endregion

		#region Owners
		/// <summary>
		/// Gets an owner from the database
		/// </summary>
		/// <param name="PropertyID"></param>
		/// <returns></returns>
		[OperationContract]
		[Security("Admin,PropertyAdd,PropertyEdit,PropertyView")]
		public EntityTransport<Custom.Data.Ownership> GetOwner(long PropertyID) {

			Utility.WCFCheckSecurityAttribute();

			var Owner = new Custom.Data.Ownership();
			try {

				using(var context = new Custom.Data.Honeycomb_Entities()) {
					Owner = context.Ownership.Where(ow => ow.PropertyID == PropertyID && ow.ArchivedOn == null).FirstOrDefault();
				}

				return Utility.ServiceResponse(ref Owner);
			} catch(Exception ex) {
				return Utility.ServiceResponse(ref Owner, ex);
			}
		}

		/// <summary>
		/// Gets a list of all owners within the system
		/// </summary>
		/// <returns></returns>
		[OperationContract]
		[Security("Admin,PropertyAdd,PropertyEdit,PropertyView")]
		public EntityTransport<Custom.Data.Ownership> GetOwners() {

			Utility.WCFCheckSecurityAttribute();

			var owners = new List<Custom.Data.Ownership>();
			try {
				using(var context = new Custom.Data.Honeycomb_Entities()) {
					owners = context.Ownership.Where(own => own.SystemID == SessionManager.SystemID).ToList();
				}
				return Utility.ServiceResponse(ref owners);
			} catch(Exception ex) {
				return Utility.ServiceResponse(ref owners, ex);
			}
		}

		/// <summary>
		/// Will mark an owner as archived.
		/// </summary>
		/// <returns></returns>
		[OperationContract]
		[Security("Admin,PropertyAdd,PropertyEdit")]
		public EntityTransport<long> ArchiveOwner(Ownership ownerToArchive, Ownership newOwner) {

			Utility.WCFCheckSecurityAttribute();

			long newID = 0;
			try {

				using(var context = new Honeycomb_Entities()) {
					var OwnerDB = context.Ownership.Where(ow => ow.ID == ownerToArchive.ID).FirstOrDefault();

					if(OwnerDB != null) {
						OwnerDB.ArchivedOn = DateTime.Now;
						OwnerDB.ArchivedBy = SessionManager.UserID;
						OwnerDB.DateOfTransfer = ownerToArchive.DateOfTransfer;

						//now add the new owner record to the property
						var propertyDB = context.Property.Where(p => p.ID == ownerToArchive.PropertyID && p.DeletedOn == null).FirstOrDefault();

						if(propertyDB != null) {
							propertyDB.Ownership.Add(newOwner);
						}

						context.SaveChanges();

						newID = newOwner.ID;
					} else {
						newID = 0;
					}
				}

				return Utility.ServiceResponse(ref newID);
			} catch(Exception ex) {
				return Utility.ServiceResponse(ref newID, ex);
			}
		}

		/// <summary>
		/// Gets a list of Person types for owners
		/// </summary>
		/// <returns></returns>
		[OperationContract]
		[Security("Admin,PropertyAdd,PropertyEdit,PropertyView")]
		public EntityTransport<Custom.Data.PersonType> GetPersonTypes() {

			Utility.WCFCheckSecurityAttribute();

			var personTypes = new List<Custom.Data.PersonType>();
			try {
				using(var context = new Custom.Data.Honeycomb_Entities()) {
					personTypes = context.PersonType.Where(pt => pt.SystemID == SessionManager.SystemID).ToList();
				}
				return Utility.ServiceResponse(ref personTypes);
			} catch(Exception ex) {
				return Utility.ServiceResponse(ref personTypes, ex);
			}
		}

		/// <summary>
		/// Gets a list of Property statuses
		/// </summary>
		/// <returns></returns>
		[OperationContract]
		[Security("Admin,PropertyAdd,PropertyEdit,PropertyView")]
		public EntityTransport<Custom.Data.PropertyStatus> GetPropertyStatus() {

            Utility.WCFCheckSecurityAttribute();

            var propertyStatuses = new List<Custom.Data.PropertyStatus>();
			try {
				using(var context = new Custom.Data.Honeycomb_Entities()) {
                    propertyStatuses = context.PropertyStatus.Where(ps => ps.SystemID == SessionManager.SystemID).OrderByDescending(z => z.ID).ToList();
                    /* ownership.propertystatusID */                    
				}
				return Utility.ServiceResponse(ref propertyStatuses);
			} catch(Exception ex) {
				return Utility.ServiceResponse(ref propertyStatuses, ex);
			}
		}

        // *** EDITS ***

        /// <summary>
        /// Gets a list of Property phases
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [Security("Admin,PropertyAdd,PropertyEdit,PropertyView")]
        public EntityTransport<Custom.Data.Phase> GetPhase()
        {

            Utility.WCFCheckSecurityAttribute();

            var phaseref = new List<Custom.Data.Phase>();
            try
            {
                using (var context = new Custom.Data.Honeycomb_Entities())
                {
                    phaseref = context.Phase.Where(p => p.SystemID == SessionManager.SystemID).OrderBy/*Descending*/(z => z.ID).ToList();
                }
                return Utility.ServiceResponse(ref phaseref);
            }
            catch (Exception ex)
            {
                return Utility.ServiceResponse(ref phaseref, ex);
            }
        }

        // *** EDITS END ***

		/// <summary>
		/// Gets the history of ownership for a given property
		/// </summary>
		/// <param name="PropertyID">The id of the property to get a history for</param>
		/// <returns></returns>
		[OperationContract]
		[Security("Admin,PropertyAdd,PropertyEdit,PropertyView")]
		public EntityTransport<Custom.Data.OwnershipHistory> GetPropertyOwnershipHistory(long PropertyID) {

			Utility.WCFCheckSecurityAttribute();

			var OwnershipHistory = new List<Custom.Data.OwnershipHistory>();

			try {

				using(var context = new Custom.Data.Honeycomb_Entities()) {
					OwnershipHistory = (from o in context.Ownership
										join pt in context.PersonType on o.PersonTypeID equals pt.ID
										where o.PropertyID == PropertyID
										&& o.ArchivedOn != null
										&& o.DeletedOn == null
										orderby o.DateOfTransfer descending
										select new Custom.Data.OwnershipHistory() {
											PrincipalOwnerName = o.PrincipalOwnerName,
											Cell = o.Cell,
											DateOfTransfer = o.DateOfTransfer.Value,
											Email = o.Email,
											GovID = o.GovID,
											OwnerType = pt.Name,
											SaleAgreementFile = o.SaleDocFileID
										})
										.Take(10).ToList();
				}

				return Utility.ServiceResponse(ref OwnershipHistory);

			} catch(Exception ex) {
				return Utility.ServiceResponse(ref OwnershipHistory, ex);
			}
		}
		#endregion

		#region Occupants
		/// <summary>
		/// Gets a list of the occupants which are assigned to a property, it will ignore archived occupants
		/// </summary>
		/// <param name="PropertyID">The id of the property</param>
		/// <returns>A list of the occupants which are assigned to a property</returns>
		[OperationContract]
		[Security("Admin,PropertyAdd,PropertyEdit,PropertyView")]
		public EntityTransport<Custom.Data.Occupant> GetOccupants(long PropertyID) {

			Utility.WCFCheckSecurityAttribute();

			var PropertyOccupants = new List<Custom.Data.Occupant>();
			try {

				using(var context = new Custom.Data.Honeycomb_Entities()) {
					PropertyOccupants = (from o in context.Occupant
										 join po in context.PropertyOccupant on o.ID equals po.OccupantID
										 where po.ArchivedOn == null
										 && po.PropertyID == PropertyID
										 orderby o.FirstName
										 select o).ToList();
				}

				//decrypt the occupant intranet passwords
				foreach(var occupant in PropertyOccupants) {
					if(!string.IsNullOrEmpty(occupant.IntranetPassword)) {
						occupant.IntranetPassword = Security.Encryption.Decrypt(occupant.IntranetPassword);
					}
				}

				return Utility.ServiceResponse(ref PropertyOccupants);
			} catch(Exception ex) {
				return Utility.ServiceResponse(ref PropertyOccupants, ex);
			}
		}


		/// <summary>
		/// This method archives the occupant so that they are hidden and can then be found under the occupant history view. It will not archive a principal occupant.
		/// </summary>
		/// <param name="OccupantID">The id of the occupant to archive</param>
		/// <param name="PropertyID">The property that the occupant has been assigned to.</param>
		/// <returns>The id of the occupant</returns>
		[OperationContract]
		[Security("Admin,PropertyAdd,PropertyEdit")]
		public EntityTransport<ProcessResponse> ArchiveOccupant(long OccupantID, long PropertyID) {

			Utility.WCFCheckSecurityAttribute();
			ProcessResponse pRes = new ProcessResponse();

			try {

				using(var context = new Custom.Data.Honeycomb_Entities()) {
					//include the occupant in the entity so that it retains the relationship and we can access the actual occupant entity
					var PropertyOccupant = context.PropertyOccupant.Include("Occupant").Where(po => po.OccupantID == OccupantID && po.PropertyID == PropertyID).FirstOrDefault();

					bool isOccupantLeaseHolder = context.Lease.Where(l => l.PropertyID == PropertyID && l.LeaseOccupantID == OccupantID && (l.Terminated == false || l.Terminated == null)).Count() > 0;

					if(isOccupantLeaseHolder) {
						pRes.success = false;
						pRes.message = "You cannot archive this occupant because they are set as the lease holder in the lease tab.";
						return Utility.ServiceResponse(ref pRes);
					}

					//check if the occupant is the principal occupant, if so do not allow archive request
					if(!PropertyOccupant.Occupant.IsPrincipalOccupant) {
						var improSecurity = new Custom.ImproSecurity();
						PropertyOccupant.ArchivedOn = DateTime.Now;
						context.SaveChanges();
						improSecurity.DisableMasterRecord(PropertyOccupant.Occupant.IDOrPassportNo);

						pRes.success = true;
					} else {
						pRes.success = false;
						pRes.message = "You cannot archive a princle occupant.";
					}
				}

				return Utility.ServiceResponse(ref pRes);
			} catch(Exception ex) {
				pRes.success = false;
				pRes.message = "There was error when attempting to save the occupant.";
				return Utility.ServiceResponse(ref pRes, ex);
			}
		}

		/// <summary>
		/// This method restores an archived occupant If you restore a principle occupant their principle status is removed.
		/// </summary>
		/// <param name="OccupantID">The id of the occupant to restore</param>
		/// <param name="PropertyID">The property that the occupant has been assigned to.</param>
		/// <returns>The id of the occupant</returns>
		[OperationContract]
		[Security("Admin,PropertyAdd,PropertyEdit")]
		public EntityTransport<long> RestoreOccupant(long OccupantID, long PropertyID) {

			Utility.WCFCheckSecurityAttribute();

			try {

				using (var context = new Custom.Data.Honeycomb_Entities()) {
					//include the occupant in the entity so that it retains the relationship and we can access the actual occupant entity
					var PropertyOccupant = context.PropertyOccupant.Include("Occupant").Where(po => po.OccupantID == OccupantID && po.PropertyID == PropertyID).FirstOrDefault();
					PropertyOccupant.ArchivedOn = null;
					//check if the occupant is a principal occupant, if so remove principle occupant status.
					if (PropertyOccupant.Occupant.IsPrincipalOccupant) {
						PropertyOccupant.Occupant.IsPrincipalOccupant = false;
					}
					context.SaveChanges();
					var improSecurity = new Custom.ImproSecurity();
					improSecurity.RestoreMasterRecord(PropertyOccupant.Occupant.IDOrPassportNo);
				}

				return Utility.ServiceResponse(ref OccupantID);
			} catch (Exception ex) {
				return Utility.ServiceResponse(ref OccupantID, ex);
			}
		}

		/// <summary>
		/// Retrieves a list of archived occupants for a property
		/// </summary>
		/// <param name="PropertyID">The id of the property</param>
		/// <returns></returns>
		[OperationContract]
		[Security("Admin,PropertyAdd,PropertyEdit,PropertyView")]
		public EntityTransport<Custom.Data.Occupant> GetArchivedOccupants(long PropertyID) {

			Utility.WCFCheckSecurityAttribute();

			var ArchivedOccupants = new List<Custom.Data.Occupant>();
			try {

				using(var context = new Custom.Data.Honeycomb_Entities()) {
					//Added the orderby clause to order the take(20) to show the most recently archived. Note: this will fail if for some reason
					//there is no propertyoccupant record for an occupant. This should not be possible within the system according to Gerard. 
					ArchivedOccupants = context.Occupant.Where(o => o.PropertyOccupant.Any(po => po.ArchivedOn != null && po.PropertyID == PropertyID)).OrderByDescending(o => o.PropertyOccupant.FirstOrDefault().ArchivedOn).Take(20).ToList();
				}

				return Utility.ServiceResponse(ref ArchivedOccupants);
			} catch(Exception ex) {
				return Utility.ServiceResponse(ref ArchivedOccupants, ex);
			}
		}

		/// <summary>
		/// Gets the list of relationship types to the Principal Owner
		/// </summary>
		/// <returns></returns>
		[OperationContract]
		[Security("Admin,PropertyAdd,PropertyEdit,PropertyView")]
		public EntityTransport<Custom.Data.PrincipalOwnerRelationship> GetPrincipalRelationships() {

			Utility.WCFCheckSecurityAttribute();

			var PrincipalRelationshps = new List<Custom.Data.PrincipalOwnerRelationship>();
			try {

				using(var context = new Custom.Data.Honeycomb_Entities()) {
					PrincipalRelationshps = context.PrincipalOwnerRelationship.Where(x => x.SystemID == CacheManager.SystemID).ToList();
				}

				return Utility.ServiceResponse(ref PrincipalRelationshps);
			} catch(Exception ex) {
				return Utility.ServiceResponse(ref PrincipalRelationshps, ex);
			}
		}

		/// <summary>
		/// A method to do a lookup on the available relationships to a key occupant
		/// </summary>
		/// <returns>A list of relationships</returns>
		[OperationContract]
		[Security("Admin,PropertyAdd,PropertyEdit,PropertyView")]
		public EntityTransport<Custom.Data.KeyOccupantRelationship> GetKeyOccupantRelationships() {

			Utility.WCFCheckSecurityAttribute();

			var KeyOccupantRelationships = new List<Custom.Data.KeyOccupantRelationship>();
			try {

				using(var context = new Custom.Data.Honeycomb_Entities()) {
					KeyOccupantRelationships = context.KeyOccupantRelationship.Where(x => x.SystemID == CacheManager.SystemID).ToList();
				}

				return Utility.ServiceResponse(ref KeyOccupantRelationships);
			} catch(Exception ex) {
				return Utility.ServiceResponse(ref KeyOccupantRelationships, ex);
			}
		}

		/// <summary>
		/// Adds a new occupant or Updates an existing occupant on the system
		/// </summary>
		/// <param name="Occupant">The occupant entity</param>
		/// <param name="PropertyID">The id of the property</param>
		/// <returns>Boolean indicating success</returns>
		[OperationContract]
		[Security("Admin,PropertyAdd,PropertyEdit")]
		public EntityTransport<string> UpdateOccupant(Occupant Occupant, long PropertyID, bool sendIntranetNotification) {

			Utility.WCFCheckSecurityAttribute();

			string message = "";
			var ImpSecurity = new ImproSecurity();

			try {
				using(var context = new Honeycomb_Entities()) {

					if(!string.IsNullOrEmpty(Occupant.IntranetUsername)) {
						//check if the username that is being requested is available
						if(!OccupantUsernameAvailable(Occupant.IntranetUsername, Occupant.ID)) {
							message = "The requested username is not available.";
							return Utility.ServiceResponse(ref message);
						} else { 
							//the username is available, check if there is a password and if so encrypt it
							if(!string.IsNullOrEmpty(Occupant.IntranetPassword)) {
								Occupant.IntranetPassword = Security.Encryption.Encrypt(Occupant.IntranetPassword);
							}
						}
					}

					if(Occupant.ID == 0) {

						if(CanAddNewOccupant(PropertyID)) {
							//add new record
							Occupant.InsertedBy = SessionManager.UserName;
							Occupant.InsertedByID = SessionManager.UserID;
							Occupant.InsertedOn = DateTime.Now;
							Occupant.SystemID = CacheManager.SystemID;
							//add the entity onto the context
							context.Occupant.Add(Occupant);

							//save so that we can get the occupant id from the database
							context.SaveChanges();

							//if the new occupant has been marked as the priciple occupant then reset the previous one
							if(Occupant.IsPrincipalOccupant) {
								ResetPrincipleOccupant(PropertyID, Occupant.ID);
							}

							#region notification stuff
							if(Occupant.HasAccessCard) {
								Communications comm = new Communications();
								//send sms
								if(!string.IsNullOrEmpty(Occupant.Cellphone)) {
									comm.SendSMS(Utility.GetMessageContent(Enumerators.MessageType.SMSAccessCardActivation).ReplaceObjectTokens(Occupant, new string[] { "FirstName" }), Occupant.Cellphone);
								}
								//send email
								if(!string.IsNullOrEmpty(Occupant.Email)) {
									comm.SendEmail(Occupant.Email, "Access Card Activation", Utility.GetMessageContent(Enumerators.MessageType.EmailAccessCardActivation).ReplaceObjectTokens(Occupant, new string[] { "FirstName" }));
								}
							}
							#endregion

							message = "";

							//add the link entry for the property and the occupant
							context.PropertyOccupant.Add(new PropertyOccupant() { OccupantID = Occupant.ID, PropertyID = PropertyID, ArchivedOn = null });

							context.SaveChanges();

							//impro integration
							if(Occupant.HasAccessCard) {
								ImpSecurity.SynchroniseImpro(Occupant);
							}
						} else {
							message = "You cannot add another occupant as you have reached the maximum allowed for this property.";
						}

					} else {

						//update the record
						var OccupantDB = context.Occupant.Where(o => o.ID == Occupant.ID).FirstOrDefault();

						#region notification stuff
						if((!OccupantDB.HasAccessCard && Occupant.HasAccessCard)) {
							Communications comm = new Communications();
							//send sms
							if(!string.IsNullOrEmpty(Occupant.Cellphone)) {
								comm.SendSMS(Utility.GetMessageContent(Enumerators.MessageType.SMSAccessCardActivation).ReplaceObjectTokens(Occupant, new string[] { "FirstName" }), Occupant.Cellphone);
							}
							//send email
							if(!string.IsNullOrEmpty(Occupant.Email)) {
								comm.SendEmail(Occupant.Email, "Access Card Activation", Utility.GetMessageContent(Enumerators.MessageType.EmailAccessCardActivation).ReplaceObjectTokens(Occupant, new string[] { "FirstName" }));
							}
						}
						#endregion

						Occupant.MergeInto(OccupantDB, new string[]{
                            "PropertyOccupant",
                            "InsertedOn",
                            "InsertedBy",
                            "InsertedByID",
                            "DeletedOn",
                            "DeletedBy",
                            "DeleteByID",
                            "SystemID",
                            "ID",
                        }, true);

						message = "";

						//if the occupant has been marked as the priciple occupant then reset the previous one
						if(Occupant.IsPrincipalOccupant) {
							ResetPrincipleOccupant(PropertyID, Occupant.ID);
						}

                        if ((!OccupantDB.GolfMember && OccupantDB.IsOwner) || (!OccupantDB.GolfMember && OccupantDB.IsPrincipalOccupant)) {

                            var PropID = context.PropertyOccupant.Where(o => o.OccupantID == Occupant.ID).FirstOrDefault().PropertyID;
                            long? PropertyPhase = context.Property.Where(w => w.ID == PropID).Select(s => s.PhaseID).FirstOrDefault();
                            var PhaseRequired = CacheManager.Phases.Where(w => w.ID == PropertyPhase && w.MembershipCompulsory == true).FirstOrDefault();

                            if (PhaseRequired != null) {
                                message = "A golf membership is required for a minimum of one occupant. This is a warning and the occupant was updated.";
                            }

                            var OccupantListGolfMembership = context.PropertyOccupant.Where(w => w.PropertyID == PropID && w.Occupant.GolfMember == true && w.ArchivedOn == null).ToList().Count;
                            if (OccupantListGolfMembership >= 1) {
                                message = "";
                            }

                        }

						context.SaveChanges();

						//impro integration
						if(OccupantDB.HasAccessCard) {
							ImpSecurity.SynchroniseImpro(OccupantDB);
						}

                        
					}

					//notification for intranet logn details
					if(sendIntranetNotification && Occupant.ID != 0) {
						Communications comm = new Communications();
						//send email
						if(!string.IsNullOrEmpty(Occupant.Email) && !string.IsNullOrEmpty(Occupant.IntranetPassword) && !string.IsNullOrEmpty(Occupant.IntranetUsername)) {
							//decrypt the password
							Occupant.IntranetPassword = Security.Encryption.Decrypt(Occupant.IntranetPassword);
							comm.SendEmail(Occupant.Email, "Simbithi Intranet Details", Utility.GetMessageContent(Enumerators.MessageType.EmailOccupantIntranetDetails).ReplaceObjectTokens(Occupant));
						}
					}


				}
				return Utility.ServiceResponse(ref message);
			} catch(Exception ex) {
				message = ex.InnerException.Message;
				return Utility.ServiceResponse(ref message, ex);
			}
		}

		private bool CanAddNewOccupant(long propertyID) {
			bool canAdd = false;
			var settings = CacheManager.SystemSetting;

			if(settings.OccupantsPerRoomLimit == 0) {
				canAdd = true;
			} else if(settings.OccupantsPerRoomLimit > 0) { 
				using(var context = new Custom.Data.Honeycomb_Entities()){
					var property = context.Property.Include(i => i.PropertyOccupant).Where(p => p.ID == propertyID).FirstOrDefault();

					if(property != null) {
						if(property.NoOfRooms.HasValue) {
							if(property.NoOfRooms.Value > 0) {
								int noOfRooms = property.NoOfRooms.Value;
								int noOfOccupants = property.PropertyOccupant.Count(i => i.ArchivedOn == null);

								int propertyOccupantLimit = (noOfRooms * settings.OccupantsPerRoomLimit) + 1;

								if(noOfOccupants < propertyOccupantLimit) {
									canAdd = true;
								}
							}
						}
					}
				}
			}

			return canAdd;
		}

		/// <summary>
		/// Util function to reset the previous principle occupant to false for a given property
		/// </summary>
		/// <param name="PropertyID">The current PropertyID</param>
		public void ResetPrincipleOccupant(long PropertyID, long IgnoreID) {
			using(var context = new Honeycomb_Entities()) {
				var PreviousPrincipleOccupant = (from o in context.Occupant
												 join po in context.PropertyOccupant on o.ID equals po.OccupantID
												 where po.PropertyID == PropertyID
												 && o.IsPrincipalOccupant == true
												 && po.ArchivedOn == null
												 && o.ID != IgnoreID
												 select o).FirstOrDefault();

				if(PreviousPrincipleOccupant != null) {
					PreviousPrincipleOccupant.IsPrincipalOccupant = false;
					context.SaveChanges();
				}
			}
		}

		/// <summary>
		/// Checks if the requested username has been used before
		/// </summary>
		/// <param name="username">The username string</param>
		/// <param name="OccupantID">The id of the occupant</param>
		/// <returns>True if the username is available</returns>
		public bool OccupantUsernameAvailable(string username, long OccupantID) {
			var avail = true;

			try {

				using(var context = new Honeycomb_Entities()) {
					var OccupantDBChk = (from o in context.Occupant
										 join po in context.PropertyOccupant on o.ID equals po.OccupantID
										 join p in context.Property on po.PropertyID equals p.ID
										 where o.IntranetUsername == username
										 && po.ArchivedOn == null
										 && o.DeletedOn == null
										 && p.DeletedOn == null
										 select o).OptionalWhere(OccupantID != 0, ow => ow.ID != OccupantID).FirstOrDefault();

					//if there are no records then that username does not exist
					if(OccupantDBChk == null) {
						avail = true;
					} else {
						avail = false;
					}
				}

			} catch(Exception ex) {
				avail = false;
			}

			return avail;
		}

		#endregion

		#region provinces
		/// <summary>
		/// Returns a list of provinces on the system
		/// </summary>
		/// <returns>a paginated entitytransport object</returns>
		/// <roles><role>Admin = admin</role></roles>

		[OperationContract]
		[Security("Admin")]
		public EntityTransport<Custom.Data.Province> GetProvinces() {

			Utility.WCFCheckSecurityAttribute();

			var provinces = new List<Custom.Data.Province>();
			try {
				using(var context = new Custom.Data.Honeycomb_Entities()) {
					provinces = context.Province.ToList();

					return Utility.ServiceResponse(ref provinces);
				}
			} catch(Exception ex) {
				return Utility.ServiceResponse(ref provinces, ex);
			}
		}
		#endregion

		#region Transfers
		[OperationContract]
		[Security("Admin")]
		public EntityTransport<Custom.Data.PropertyTransfer> GetTranfersforProperty(long propertyID) {

			Utility.WCFCheckSecurityAttribute();

			var transfers = new List<Custom.Data.PropertyTransfer>();
			try {
				using(var context = new Custom.Data.Honeycomb_Entities()) {
					transfers = context.PropertyTransfer.Where(pt => pt.PropertyID == propertyID).ToList();
					return Utility.ServiceResponse(ref transfers);
				}
			} catch(Exception ex) {

				return Utility.ServiceResponse(ref transfers, ex);
			}
		}
		#endregion

		#region security
		/// <summary>
		/// Gets the incident history for a given property
		/// </summary>
		/// <param name="PropertyID"></param>
		/// <returns></returns>
		[OperationContract]
		[Security("PropertyAdd,PropertyEdit,PropertyView,Admin")]
		public EntityTransport<PropertyIncidentHistory> GetPropertyIncidentHistory(long PropertyID) {

			Utility.WCFCheckSecurityAttribute();

			var PropertyIncidents = new List<PropertyIncidentHistory>();

			try {

				using(var context = new Honeycomb_Entities()) {
					PropertyIncidents = (from i in context.Incident
										 join it in context.IncidentType on i.IncidentTypeID equals it.ID
										 join status in context.IncidentStatus on i.IncidentStatusID equals status.ID
										 where i.PropertyID == PropertyID
										 && i.ArchivedOn == null
										 orderby i.IncidentDate descending
										 select new PropertyIncidentHistory() {
											 IncidentDate = i.IncidentDate,
											 IncidentTime = i.IncidentTime,
											 IncidentType = it.Name,
											 Status = status.Name,
											 ID = i.ID
										 }).Take(10).ToList();
				}

				return Utility.ServiceResponse(ref PropertyIncidents);
			} catch(Exception ex) {
				return Utility.ServiceResponse(ref PropertyIncidents, ex);
			}
		}

		/// <summary>
		/// Returns a data transport object for IncidentDetails
		/// </summary>
		/// <param name="IncidentID"></param>
		/// <returns></returns>
		[OperationContract]
		[Security("PropertyAdd,PropertyEdit,PropertyView,Admin")]
		public EntityTransport<IncidentDetail> GetIncidentDetails(long IncidentID) {

			Utility.WCFCheckSecurityAttribute();

			var Incident = new IncidentDetail();

			try {

				using(var context = new Honeycomb_Entities()) {
					Incident = (from i in context.Incident
								join it in context.LookUpOption on i.IncidentTypeID equals it.ID
								join ins in context.LookUpOption on i.IncidentSourceID equals ins.ID
								join ip in context.LookUpOption on i.IncidentPriorityID equals ip.ID
								join status in context.IncidentStatus on i.IncidentStatusID equals status.ID
								join usr in context.User on i.AssignedUserID equals usr.ID
								where i.ID == IncidentID
								select new IncidentDetail() {
									ID = i.ID,
									AssignedTo = usr.FirstName + " " + usr.LastName,
									Source = ins.Name,
									IncidentType = it.Name,
									Priority = ip.Name,
									IncidentDate = i.IncidentDate,
									IncidentTime = i.IncidentTime,
									FileID = i.FileID,
									Status = status.Name,
									Description = i.Description,
									ResolutionDescription = i.ResolutionDescription,
									ResolutionDate = i.ResolutionDate.Value,
									PartiesInvolved = (List<string>)i.Occupant.Select(s => s.FirstName + " " + s.LastName)
								}).FirstOrDefault();
				}

				return Utility.ServiceResponse(ref Incident);
			} catch(Exception ex) {
				return Utility.ServiceResponse(ref Incident, ex);
			}
		}
		#endregion

		#region staff
		/// <summary>
		/// returns a list of staff members assigned to a property
		/// </summary>
		/// <param name="PropertyID"></param>
		/// <returns></returns>
		[OperationContract]
		[Security("PropertyAdd,PropertyEdit,PropertyView,Admin")]
		public EntityTransport<Staff> GetPropertyStaff(long PropertyID) {

			Utility.WCFCheckSecurityAttribute();

			var PropertyStaff = new List<Staff>();

			try {
				using(var context = new Honeycomb_Entities()) {
					PropertyStaff = context.Staff.Where(s => s.Property.Any(p => p.ID == PropertyID) && s.DeletedOn == null && s.Active == true).ToList();
				}

				return Utility.ServiceResponse(ref PropertyStaff);
			} catch(Exception ex) {
				return Utility.ServiceResponse(ref PropertyStaff, ex);
			}
		}

		/// <summary>
		/// Gets a list of erven that the staff member is active on
		/// </summary>
		/// <param name="StaffID"></param>
		/// <returns></returns>
		[OperationContract]
		[Security("PropertyAdd,PropertyEdit,PropertyView,Admin")]
		public EntityTransport<Erf> GetStaffErven(long StaffID) {

			Utility.WCFCheckSecurityAttribute();

			var StaffErven = new List<Erf>();

			try {

				using(var context = new Honeycomb_Entities()) {
					StaffErven = context.Erf.Where(e => e.Property.Any(p => p.Staff.Any(s => s.ID == StaffID))).ToList();
				}

				return Utility.ServiceResponse(ref StaffErven);
			} catch(Exception ex) {
				return Utility.ServiceResponse(ref StaffErven, ex);
			}
		}

		/// <summary>
		/// Will search the database for a staff member with the same id and return that staff member.
		/// </summary>
		/// <param name="StaffIDNo"></param>
		/// <returns></returns>
		[OperationContract]
		[Security("Admin,PropertyView,PropertyEdit,PropertyAdd")]
		public EntityTransport<Staff> GetStaffByID(string StaffIDNo, long PropertyID) {

			Utility.WCFCheckSecurityAttribute();

			var StaffDB = new Staff();
			try {

				using(var context = new Honeycomb_Entities()) {
					if(!string.IsNullOrEmpty(StaffIDNo)) {
						StaffDB = context.Staff.Where(s => s.GOVID == StaffIDNo.Trim() && !(s.Property.Any(p => p.ID == PropertyID))).FirstOrDefault();

                        //if a staff member was not found on the honeycomb system look for the staff member in impro
                        if (StaffDB == null) {
                            var impro = new ImproSecurity();
                            var improStaff = impro.GetMasterByID(StaffIDNo);

                            if (improStaff != null) {
                                StaffDB = new Staff();
                                StaffDB.FirstName = improStaff.MST_FirstName;
                                StaffDB.Lastname = improStaff.MST_LastName;
                                StaffDB.Gender = improStaff.MST_Gender == "M" ? "Male" : "Female";
                                StaffDB.GOVID = StaffIDNo;
                                StaffDB.Active = true;

                                byte[] imageBA = impro.GetMasterImage(improStaff.MST_SQ);

                                /// an image exists, add it to the document repository and associate it with the staff entity to send down
                                if (imageBA.Length > 0) {
                                    var newDocumentID = Custom.Model.Document.SaveDocument(imageBA, "staffimage.jpg", "", SessionManager.UserName);
                                    if (newDocumentID != null) {
                                        StaffDB.FileID = newDocumentID;
                                    }
                                }
                            }
                        }
					}
				}

				return Utility.ServiceResponse(ref StaffDB);
			} catch(Exception ex) {
				return Utility.ServiceResponse(ref StaffDB, ex);
			}
		}

		/// <summary>
		/// Updates or adds a staff memeber
		/// </summary>
		/// <param name="StaffMember"></param>
		/// <param name="PropertyID"></param>
		/// <returns></returns>
		[OperationContract]
		[Security("Admin,PropertyEdit,PropertyAdd")]
		public EntityTransport<string> UpdateStaff(Staff StaffMember, long PropertyID) {

			Utility.WCFCheckSecurityAttribute();

			var Message = "";
			var ImproSecurity = new ImproSecurity();

			try {

				using(var context = new Honeycomb_Entities()) {

					if(StaffMember.ID == 0) {
						//add
						StaffMember.InsertedBy = SessionManager.UserName;
						StaffMember.InsertedOn = DateTime.Now;
						StaffMember.InsertedByID = SessionManager.UserID;
						//associate the staff member with a property
						StaffMember.Property = context.Property.Where(p => p.ID == PropertyID).ToList();
						//add the staff memeber to the context
						context.Staff.Add(StaffMember);
						//synchronise impro
						ImproSecurity.SynchroniseImpro(StaffMember);
						//save changes
						context.SaveChanges();
					} else {
						//do an update
						var StaffMemberDB = context.Staff.Include("Property").Where(s => s.ID == StaffMember.ID).FirstOrDefault();

						if(StaffMemberDB != null) {

							//check if this staff member has been assigned to the property
							if(!StaffMemberDB.Property.Any(p => p.ID == PropertyID)) {
								//add the property to the staff member
								StaffMemberDB.Property.Add(context.Property.Where(p => p.ID == PropertyID).FirstOrDefault());
							}

							StaffMember.MergeInto(StaffMemberDB, new string[] { 
                                "Property",
                                "InsertedBy",
                                "InsertedOn",
                                "InsertedByID",
                                "DeletedOn",
                                "DeletedBy",
                                "DeleteByID"
                            }, true);

							//Update the context
							context.SaveChanges();
							//synchronise impro
							ImproSecurity.SynchroniseImpro(StaffMemberDB);
						} else {
							Message = "There was an erorr retrieving the staff member record.";
						}
					}
				}

				return Utility.ServiceResponse(ref Message);

			} catch(Exception ex) {
				Message = "There was an error updating the staff member";
				return Utility.ServiceResponse(ref Message, ex);
			}
		}
		#endregion

		#region staff notes
		/// <summary>
		/// Gets all staff notes
		/// </summary>
		/// <param name="StaffID">The id of the staff member</param>
		/// <returns>A list of type StaffNote</returns>
		[OperationContract]
		[Security("PropertyAdd,PropertyEdit,PropertyView,Admin")]
		public EntityTransport<StaffNote> GetStaffNotes(long StaffID) {

			Utility.WCFCheckSecurityAttribute();

			var StaffNotes = new List<StaffNote>();

			try {

				using(var context = new Honeycomb_Entities()) {
					StaffNotes = context.StaffNote.Where(sn => sn.StaffID == StaffID && sn.DeletedOn == null).OrderByDescending(o => o.InsertedOn).Take(20).ToList();
				}

				return Utility.ServiceResponse(ref StaffNotes);
			} catch(Exception ex) {
				return Utility.ServiceResponse(ref StaffNotes, ex);
			}
		}

		/// <summary>
		/// Retrieves a single staff note record when provided with an ID.
		/// </summary>
		/// <param name="NoteID"></param>
		/// <returns></returns>
		[OperationContract]
		[Security("PropertyAdd,PropertyEdit,PropertyView,Admin")]
		public EntityTransport<StaffNote> GetStaffNote(long NoteID) {

			Utility.WCFCheckSecurityAttribute();

			var StaffNote = new StaffNote();

			try {
				using(var context = new Honeycomb_Entities()) {
					StaffNote = context.StaffNote.Where(sn => sn.ID == NoteID).FirstOrDefault();
				}

				return Utility.ServiceResponse(ref StaffNote);
			} catch(Exception ex) {
				return Utility.ServiceResponse(ref StaffNote, ex);
			}
		}

		/// <summary>
		/// Adds or updates a staff note
		/// </summary>
		/// <param name="Note"></param>
		/// <returns></returns>
		[OperationContract]
		[Security("PropertyAdd,PropertyEdit,Admin")]
		public EntityTransport<bool> UpdateStaffNote(StaffNote Note) {

			Utility.WCFCheckSecurityAttribute();

			var success = true;

			try {

				using(var context = new Honeycomb_Entities()) {

					if(Note.ID == 0) {
						//insert
						Note.InsertedOn = DateTime.Now;
						Note.InsertedBy = SessionManager.UserName;
						Note.InsertedByID = SessionManager.UserID;

						context.StaffNote.Add(Note);
					} else {
						//update
						var NoteDB = context.StaffNote.Where(sn => sn.ID == Note.ID).FirstOrDefault();

						if(NoteDB != null) {
							Note.MergeInto(NoteDB, new string[] { 
                                "InsertedOn",
                                "InsertedBy",
                                "InsertedByID",
                                "DeletedOn",
                                "DeletedBy",
                                "DeletedByID"
                            }, true);
						}
					}

					context.SaveChanges();
				}

				return Utility.ServiceResponse(ref success);
			} catch(Exception ex) {
				success = false;
				return Utility.ServiceResponse(ref success, ex);
			}
		}

		/// <summary>
		/// Soft deletes the note
		/// </summary>
		/// <param name="NoteID"></param>
		/// <returns></returns>
		[OperationContract]
		[Security("PropertyAdd,PropertyEdit,Admin")]
		public EntityTransport<bool> DeleteStaffNote(long NoteID) {

			Utility.WCFCheckSecurityAttribute();

			bool success = true;

			try {

				using(var context = new Honeycomb_Entities()) {
					var StaffNoteDB = context.StaffNote.Where(sn => sn.ID == NoteID).FirstOrDefault();

					if(StaffNoteDB != null) {
						StaffNoteDB.DeletedOn = DateTime.Now;
						StaffNoteDB.DeletedBy = SessionManager.UserName;
						StaffNoteDB.DeletedByID = SessionManager.UserID;

						context.SaveChanges();
					}
				}

				return Utility.ServiceResponse(ref success);
			} catch(Exception ex) {
				success = false;
				return Utility.ServiceResponse(ref success, ex);
			}
		}
		#endregion

		#region leases
		[OperationContract]
		[Security("PropertyAdd,PropertyEdit,PropertyView,Admin")]
		public EntityTransport<Lease> GetPropertyLease(long PropertyID) {

			Utility.WCFCheckSecurityAttribute();

			var PropertyLease = new Lease();

			try {

				using(var context = new Honeycomb_Entities()) {
					PropertyLease = context.Lease.Where(l => l.PropertyID == PropertyID).FirstOrDefault();
				}

				return Utility.ServiceResponse(ref PropertyLease);
			} catch(Exception ex) {
				return Utility.ServiceResponse(ref PropertyLease, ex);
			}
		}

		/// <summary>
		/// Inserts or updates a lease extention record in the database.
		/// </summary>
		/// <param name="leaseExtention">A LeaseExtention entity</param>
		/// <returns>bool indicating success</returns>
		[OperationContract]
		[Security("PropertyAdd,PropertyEdit,Admin")]
		public EntityTransport<bool> ExtendLease(LeaseExtension leaseExtention) {

			Utility.WCFCheckSecurityAttribute();

			var success = true;

			try {

				using(var context = new Honeycomb_Entities()) {
					if(leaseExtention.ID == 0) {
						//add new lease extention
						leaseExtention.InsertedOn = DateTime.Now;
						leaseExtention.InsertedBy = SessionManager.UserName;
						leaseExtention.InsertedByID = SessionManager.UserID;
						//add onto the context in order to save
						context.LeaseExtension.Add(leaseExtention);
					} else {
						//update current record
						var dbLeaseEntity = context.LeaseExtension.Where(l => l.ID == leaseExtention.ID).FirstOrDefault();

						if(dbLeaseEntity != null) {
							leaseExtention.MergeInto(dbLeaseEntity, new string[] {
                                "InsertedOn",
                                "InsertedBy",
                                "InsertedByID",
                                "DeletedOn",
                                "DeletedBy",
                                "DeletedByID"
                            }, true);
						}
					}

					//save changes to db
					context.SaveChanges();
				}

				return Utility.ServiceResponse(ref success);
			} catch(Exception ex) {
				success = false;
				return Utility.ServiceResponse(ref success, ex);
			}
		}

		/// <summary>
		/// Gets a lease extention by it's id
		/// </summary>
		/// <param name="extentionID"></param>
		/// <returns></returns>
		[OperationContract]
		[Security("PropertyAdd,PropertyEdit,PropertyView,Admin")]
		public EntityTransport<LeaseExtension> GetLeaseExtention(long extentionID) {

			Utility.WCFCheckSecurityAttribute();

			var LeaseExtention = new LeaseExtension();

			try {

				using(var context = new Honeycomb_Entities()) {
					LeaseExtention = context.LeaseExtension.Where(le => le.ID == extentionID).FirstOrDefault();
				}

				return Utility.ServiceResponse(ref LeaseExtention);
			} catch(Exception ex) {
				return Utility.ServiceResponse(ref LeaseExtention, ex);
			}
		}

		/// <summary>
		/// Gets all the lease extentions for a property
		/// </summary>
		/// <param name="propertyID"></param>
		/// <returns></returns>
		[OperationContract]
		[Security("PropertyAdd,PropertyEdit,PropertyView,Admin")]
		public EntityTransport<LeaseExtension> GetPropertyLeaseExtentions(long propertyID) {

			Utility.WCFCheckSecurityAttribute();

			var propertyLeaseExtentions = new List<LeaseExtension>();

			try {

				using(var context = new Honeycomb_Entities()) {
					propertyLeaseExtentions = context.LeaseExtension.Where(le => le.PropertyID == propertyID && le.DeletedOn == null).OrderBy(o => o.EndDate).ToList();
				}

				return Utility.ServiceResponse(ref propertyLeaseExtentions);
			} catch(Exception ex) {
				return Utility.ServiceResponse(ref propertyLeaseExtentions, ex);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="propertyID"></param>
		/// <param name="terminationReason"></param>
		/// <returns></returns>
		[OperationContract]
		[Security("PropertyAdd,PropertyEdit,Admin")]
		public EntityTransport<string> TerminateLease(long propertyID, string terminationReason) {

			Utility.WCFCheckSecurityAttribute();

			string returnMessage = "";

			try {

				Honeycomb.Custom.Security.TerminateLease(propertyID, terminationReason);

				return Utility.ServiceResponse(ref returnMessage);
			} catch(Exception ex) {
				return Utility.ServiceResponse(ref returnMessage, ex);
			}
		}

		/// <summary>
		/// This function returns all the occupants marked as tenants for the supplied propertyID.
		/// </summary>
		/// <param name="propertyID">The id of the property to retrieve occupant tenants for.</param>
		/// <returns>A list of occupants</returns>
		[OperationContract]
		[Security("PropertyAdd,PropertyEdit,Admin")]
		public EntityTransport<Occupant> GetPropertyTenants(long propertyID) {
			List<Occupant> propertyTenants = new List<Occupant>();

			Utility.WCFCheckSecurityAttribute();

			try {

				using(var context = new Honeycomb_Entities()){
					propertyTenants = context.Occupant.Where(o => o.PropertyOccupant.Any(po => po.PropertyID == propertyID && po.ArchivedOn == null) &&
																	o.DeletedOn == null &&
																	o.IsTenant == true).ToList();
				}

				return Utility.ServiceResponse(ref propertyTenants);
			}catch(Exception ex){
				return Utility.ServiceResponse(ref propertyTenants, ex);
			}
		}

		#endregion

		#region personal

		#region pets
		[OperationContract]
		[Security("Admin,PropertyPersonalMaintenance,PropertyView")]
		public EntityTransport<Custom.Data.Pet> GetPropertyPets(long propertyID) {

			Utility.WCFCheckSecurityAttribute();

			var petsList = new List<Custom.Data.Pet>();

			try {
				using(var context = new Honeycomb_Entities()) {
					petsList = context.Pet.Where(p => p.PropertyID == propertyID && p.DeletedOn == null).ToList();
				}

				return Utility.ServiceResponse(ref petsList);
			} catch(Exception ex) {
				return Utility.ServiceResponse(ref petsList, ex);
			}
		}

        [OperationContract]
        [Security("Admin,PropertyPersonalMaintenance")]
        public EntityTransport<Custom.Data.ProcessResponse> UpdatePet(Custom.Data.Pet petEntity, long propertyID)
        {

            Utility.WCFCheckSecurityAttribute();

            // *** EDITS
            int PetCount = 0;
            var pr = new Custom.Data.ProcessResponse();

            using (var petListContext1 = new Honeycomb_Entities()) {
                PetCount = petListContext1.Pet.Where(p => p.PropertyID == propertyID && p.DeletedOn == null && p.Type == petEntity.Type).ToList().Count();
            }
            int PetLimit = (petEntity.Type == "Dog") ? CacheManager.SystemSetting.DogsLimit : CacheManager.SystemSetting.CatsLimit;

            if (PetCount >= PetLimit && petEntity.PetID == 0) {
                pr.message = String.Format("You cannot have more than the specified number of {0}s", petEntity.Type.ToLower());
                pr.success = false;
                return Utility.ServiceResponse(ref pr, new Exception(pr.message), false);
            }
            else
            {
                try
                {

                    using (var context = new Honeycomb_Entities())
                    {
                        if (petEntity.PetID == 0)
                        {

                            petEntity.InsertedOn = DateTime.Now;
                            petEntity.InsertedBy = SessionManager.UserName;
                            petEntity.InsertedByID = SessionManager.UserID;

                            context.Pet.Add(petEntity);

                        }
                        else
                        {
                            var petEntityDB = context.Pet.Where(p => p.PetID == petEntity.PetID).FirstOrDefault();

                            if (petEntityDB != null)
                            {
                                petEntity.MergeInto(petEntityDB, new string[] {
                                    "InsertedOn",
                                    "InsertedBy",
                                    "InsertedByID",
                                    "Property",
                                    "PropertyID"
                                }, true);
                            }
                        }
                        pr.success = true;

                        context.SaveChanges();
                    }

                    return Utility.ServiceResponse(ref pr);
                }
                catch (Exception Exception)
                {
                    pr.success = false;
                    return Utility.ServiceResponse(ref pr, Exception);
                }
            }


        }

		[OperationContract]
		[Security("Admin,PropertyPersonalMaintenance")]
		public EntityTransport<bool> DeletePet(long petID) {

			Utility.WCFCheckSecurityAttribute();

			bool success = true;

			try {
				using(var context = new Honeycomb_Entities()) {
					var petDB = context.Pet.Where(p => p.PetID == petID).FirstOrDefault();

					if(petDB != null) {
						petDB.DeletedOn = DateTime.Now;
						petDB.DeletedBy = SessionManager.UserName;
						petDB.DeletedByID = SessionManager.UserID;

						context.SaveChanges();
					}
				}

				return Utility.ServiceResponse(ref success);
			} catch(Exception ex) {
				success = false;
				return Utility.ServiceResponse(ref success, ex);
			}
		}
   
        #endregion

// *** EDITS ***

        #region Files
        /// <summary>
        /// Returns a list of the files to the user
        /// </summary>
        /// <param name="entityID">The id of the entity that is linked to the file</param>
        /// <param name="fileType">A string representing the type of file to be returned</param>
        /// <returns>A list of type</returns>
        [OperationContract]
        [Security("Admin,ManageOrders")]
        public EntityTransport<FileDetail> GetFiles(int entityID, string fileType)
        {
            Utility.CheckSession();
            Utility.WCFCheckSecurityAttribute();

            List<FileDetail> fileList = new List<FileDetail>();

            try
            {
                //check if the file type passed up exists
                if (!Utility.IsCustomFileType(fileType))
                {
                    throw new Exception("Requested file type specified does not exist for the system.");
                }

                using (var context = new Honeycomb_Entities())
                {

                    switch (fileType)
                    {
                        case "VetCertificate":
                            {
                                fileList = context.File.Where(f => f.Type == fileType && f.DeletedOn == null && f.Pet.PetID == entityID).OrderByDescending(o => o.InsertedOn).Select(s => new FileDetail()
                                {
                                    FileID = s.FileID,
                                    FileName = s.FileName,
                                    InsertedOn = s.InsertedOn.Value,
                                    Label = s.Label,
                                    Type = s.Type
                                }).ToList();
                                break;
                            }
                        case "ChipCertificate":
                            {
                                /*fileList = context.File.Where(f => f.Type == fileType && f.DeletedOn == null && f.InsertedByID == entityID).OrderByDescending(o => o.InsertedOn).Select(s => new FileDetail()*/
                                fileList = context.File.Where(f => f.Type == fileType && f.DeletedOn == null && f.Pet.PetID == entityID).OrderByDescending(o => o.InsertedOn).Select(s => new FileDetail()
                                {
                                    FileID = s.FileID,
                                    FileName = s.FileName,
                                    InsertedOn = s.InsertedOn.Value,
                                    Label = s.Label,
                                    Type = s.Type
                                }).ToList();
                                break;
                            }
                    }
                }

                return Utility.ServiceResponse(ref fileList);
            }
            catch (Exception ex)
            {
                return Utility.ServiceResponse(ref fileList, ex);
            }
        }

        /// <summary>
        /// Will add the file to the File table and link the relevant entity
        /// </summary>
        /// <param name="uploadedFile"></param>
        /// <param name="entityID"></param>
        /// <returns></returns>
        [Security("Admin,ManageOrders")]
        public ProcessResponse UpdateFile(File uploadedFile, int entityID)
        {
            Utility.CheckSession();
            Utility.WCFCheckSecurityAttribute();
            ProcessResponse pRes = new ProcessResponse();

            try
            {

                if (Utility.IsCustomFileType(uploadedFile.Type))
                {

                    using (var context = new Honeycomb_Entities())
                    {

                        //var me = new PetDocument(); *** Declared but never used ***

                        uploadedFile.InsertedBy = SessionManager.UserName;
                        uploadedFile.InsertedOn = DateTime.Now;
                        uploadedFile.InsertedByID = SessionManager.UserID;

                        switch (uploadedFile.Type)
                        {
                            case "VetCertificate":
                            case "ChipCertificate":
                                {
                                    //this is an order file, link it to the provided order
                                    var Pet = context.Pet.Where(o => o.PetID == entityID && o.DeletedOn == null).FirstOrDefault();

                                    if (Pet != null)
                                    {
                                       Pet.File.Add(uploadedFile);
                                    }
                                    else
                                    {
                                        throw new Exception("File cannot be attached to this Pet as it is not created yet or is deleted. "
                                        + "Please create the Pet first ");
                                    }

                                    break;
                                }
                        }

                        //finally we need to add the file to the database
                        context.File.Add(uploadedFile);

                        context.SaveChanges();
                    }

                }
                else
                {
                    throw new Exception("Requested file type specified does not exist for the system");
                }

                pRes.success = true;
                pRes.message = "Successfully uploaded file";

                return pRes;
            }
            catch (Exception ex)
            {
                pRes.success = false;
                pRes.message = ex.Message;
                return pRes;
            }
        }

        /// <summary>
        /// Will soft delete a file
        /// </summary>
        /// <param name="fileID">The unique identity of the file</param>
        /// <returns></returns>
        [OperationContract]
        [Security("Admin,ManageOrders")]
        public EntityTransport<ProcessResponse> DeleteFile(Guid fileID)
        {
            Utility.CheckSession();
            Utility.WCFCheckSecurityAttribute();
            ProcessResponse pRes = new ProcessResponse();

            try
            {
                using (var context = new Honeycomb_Entities())
                {
                    File PetEnt = context.File.Where(f => f.FileID == fileID && f.DeletedOn == null).FirstOrDefault();

                    if (PetEnt != null)
                    {

                        switch (PetEnt.Type)
                        {
                            case "VetCertificate":
                            case "ChipCertificate":
                                {
                                    //this is an order file, link it to the provided order
                                    PetEnt = context.File.Include(i => i.Pet).Where(f => f.FileID == fileID).FirstOrDefault();

                                    if (PetEnt != null)
                                    {
                                        PetEnt.Pet.File.Remove(PetEnt);
                                    }
                                    else
                                    {
                                        throw new Exception("The file could not be located or does not exist, please refresh and try again.");
                                    }

                                    break;
                                }
                        }

                        context.Entry(PetEnt).State = System.Data.EntityState.Deleted;
                        context.SaveChanges();

                        pRes.success = true;
                        pRes.message = "Successfully deleted file";
                    }
                    else
                    {
                        pRes.success = false;
                        pRes.message = "The file could not be located or does not exist, please refresh and try again.";
                    }

                }

                return Utility.ServiceResponse(ref pRes);
            }
            catch (Exception ex)
            {
                pRes.success = false;
                pRes.message = "There was an error when attempting to delete the ";
                return Utility.ServiceResponse(ref pRes, ex);
            }
        }
        #endregion

// *** EDITS - END ***

		#region golf carts
		[OperationContract]
		[Security("Admin,PropertyPersonalMaintenance,PropertyView")]
		public EntityTransport<Custom.Data.GolfCart> GetPropertyGolfCarts(long propertyID) {

			Utility.WCFCheckSecurityAttribute();

			var golfCarts = new List<Custom.Data.GolfCart>();

			try {
				using(var context = new Honeycomb_Entities()) {
					golfCarts = context.GolfCart.Where(g => g.PropertyID == propertyID && g.DeletedOn == null).ToList();
				}

				return Utility.ServiceResponse(ref golfCarts);
			} catch(Exception ex) {
				return Utility.ServiceResponse(ref golfCarts, ex);
			}
		}

		[OperationContract]
		[Security("Admin,PropertyPersonalMaintenance")]
		public EntityTransport<Custom.Data.ProcessResponse> UpdateGolfCart(Custom.Data.GolfCart golfCartEntity) {

			Utility.WCFCheckSecurityAttribute();

			var pr = new Custom.Data.ProcessResponse();

			try {
				using(var context = new Honeycomb_Entities()) {
					if(golfCartEntity.GolfCartID == 0) {
						golfCartEntity.InsertedOn = DateTime.Now;
						golfCartEntity.InsertedBy = SessionManager.UserName;
						golfCartEntity.InsertedByID = SessionManager.UserID;

						context.GolfCart.Add(golfCartEntity);
					} else {
						var golfCartDB = context.GolfCart.Where(g => g.GolfCartID == golfCartEntity.GolfCartID).FirstOrDefault();

						if(golfCartEntity != null) {

							golfCartEntity.MergeInto(golfCartDB, new string[] { 
                                "InsertedOn",
                                "InsertedBy",
                                "InsertedByID",
                                "Property",
                                "PropertyID"
                            }, true);
						}
					}

					context.SaveChanges();
					pr.success = true;
				}

				return Utility.ServiceResponse(ref pr);
			} catch(Exception ex) {
				pr.success = false;
				return Utility.ServiceResponse(ref pr, ex);
			}
		}

		[OperationContract]
		[Security("Admin,PropertyPersonalMaintenance")]
		public EntityTransport<bool> DeleteGolfCart(long golfCartID) {

			Utility.WCFCheckSecurityAttribute();

			bool success = true;

			try {
				using(var context = new Honeycomb_Entities()) {
					var golfCartDB = context.GolfCart.Where(g => g.GolfCartID == golfCartID).FirstOrDefault();

					if(golfCartDB != null) {
						golfCartDB.DeletedOn = DateTime.Now;
						golfCartDB.DeletedBy = SessionManager.UserName;
						golfCartDB.DeletedByID = SessionManager.UserID;

						context.SaveChanges();
					}
				}

				return Utility.ServiceResponse(ref success);
			} catch(Exception ex) {
				success = false;
				return Utility.ServiceResponse(ref success, ex);
			}
		}
		#endregion

		#region vehicles
		[OperationContract]
		[Security("Admin,PropertyPersonalMaintenance,PropertyView")]
		public EntityTransport<Custom.Data.Vehicle> GetPropertyVehicles(long propertyID) {

			Utility.WCFCheckSecurityAttribute();

			var propertyVehicles = new List<Custom.Data.Vehicle>();

			try {
				using(var context = new Honeycomb_Entities()) {
					propertyVehicles = context.Vehicle.Where(v => v.PropertyID == propertyID && v.DeletedOn == null).ToList();
				}

				return Utility.ServiceResponse(ref propertyVehicles);
			} catch(Exception ex) {
				return Utility.ServiceResponse(ref propertyVehicles, ex);
			}
		}

		[OperationContract]
		[Security("Admin,PropertyPersonalMaintenance")]
		public EntityTransport<Custom.Data.ProcessResponse> UpdateVehicle(Custom.Data.Vehicle vehicleEntity) {

			Utility.WCFCheckSecurityAttribute();

			var pr = new Custom.Data.ProcessResponse();

			try {
				using(var context = new Honeycomb_Entities()) {

					if(vehicleEntity.VehicleID == 0) {

						vehicleEntity.InsertedOn = DateTime.Now;
						vehicleEntity.InsertedBy = SessionManager.UserName;
						vehicleEntity.InsertedByID = SessionManager.UserID;

						context.Vehicle.Add(vehicleEntity);
					} else {
						var vehicleDB = context.Vehicle.Where(v => v.VehicleID == vehicleEntity.VehicleID).FirstOrDefault();

						if(vehicleDB != null) {
							vehicleEntity.MergeInto(vehicleDB, new string[] { 
                                "InsertedOn",
                                "InsertedBy",
                                "InsertedByID",
                                "Property",
                                "PropertyID"
                            }, true);
						}
					}

					context.SaveChanges();
					pr.success = true;
				}

				return Utility.ServiceResponse(ref pr);
			} catch(Exception ex) {
				pr.success = false;
				return Utility.ServiceResponse(ref pr, ex);
			}
		}

		[OperationContract]
		[Security("Admin,PropertyPersonalMaintenance")]
		public EntityTransport<bool> DeleteVehicle(long vehicleID) {

			Utility.WCFCheckSecurityAttribute();

			bool success = true;

			try {
				using(var context = new Honeycomb_Entities()) {
					var vehicleDB = context.Vehicle.Where(v => v.VehicleID == vehicleID).FirstOrDefault();

					if(vehicleDB != null) {
						vehicleDB.DeletedOn = DateTime.Now;
						vehicleDB.DeletedBy = SessionManager.UserName;
						vehicleDB.DeletedByID = SessionManager.UserID;

						context.SaveChanges();
					}
				}

				return Utility.ServiceResponse(ref success);
			} catch(Exception ex) {
				success = false;
				return Utility.ServiceResponse(ref success, ex);
			}
		}
		#endregion

		#endregion
	}
}
