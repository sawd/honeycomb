﻿#region Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using Honeycomb.Custom;
using Custom = Honeycomb.Custom;
using Honeycomb.Security;
using System.ComponentModel;
using System.Web;
using System.Web.Security;
using Honeycomb.Custom.Data;
using System.Xml.Linq;

#endregion

namespace Honeycomb.Security {

    [ServiceContract(Namespace = "SecurityService")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
	[ServiceOutputBehavior]
    public class WCFSecurity {

        #region Users
        /// <summary>
        /// Gets all users within the system for the current client
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [Security("Admin,UsersAdd,UsersEdit")]
        public EntityTransport<Custom.Data.User> GetSiteUsers(PaginationContainer pagination) {

            Utility.WCFCheckSecurityAttribute();

            var users = new List<Custom.Data.User>();
            
            try {
                Utility.CheckSession();
                using(var context = new Custom.Data.Honeycomb_Entities()) {
                    users = context.User.Where(usr => usr.SystemID == SessionManager.SystemID && usr.DeletedOn == null).ToList();
                }

                var paginatedResult = users.AsQueryable().Paginate(pagination.CurrentPage, pagination.PageSize, pagination.OrderBy).ToList();
                return Utility.ServiceResponse(ref paginatedResult, users.Count);

            } catch(Exception ex) {

                return Utility.ServiceResponse(ref users, ex);
            }
            //return entityTransport;
        }

        /// <summary>
        /// Creates & Edits a user, as well as saving the roles that the user has access to
        /// </summary>
        /// <param name="userEntity"></param>
        /// <returns></returns>
        [OperationContract]
        [Security("Admin,UsersEdit")]
        public EntityTransport<Custom.Data.User> UpdateUser(Custom.Data.User userEntity, long [] userRoles) {

            Utility.WCFCheckSecurityAttribute();

            var entityTransport = new EntityTransport<Custom.Data.User>();
            List<Custom.Data.Role> userRoleSel = new List<Custom.Data.Role>();
            entityTransport.EntityList = new List<Custom.Data.User>();
            try {
                Utility.CheckSession();

                using(var context = new Custom.Data.Honeycomb_Entities()) {
                    if(userEntity.ID == -1) {
                        userEntity.SystemID = SessionManager.SystemID;
                        userEntity.Password = Encryption.Encrypt(userEntity.Password);
                        userEntity.InsertedByID = SessionManager.UserID;
                        userEntity.InsertedBy = SessionManager.UserName;
                        context.User.Add(userEntity);
                        //context.Entry(userEntity).State = System.Data.EntityState.Added;
                    } else {

                        userEntity.Password = Encryption.Encrypt(userEntity.Password);

                        context.User.Add(userEntity);
                        context.Entry(userEntity).State = System.Data.EntityState.Modified;
                    }

                    context.SaveChanges();

                    //user is saved, now add the permissions to the user
                    if(userEntity.ID > 0) {

                        //select the user entity out and include the linked roles using the include function
                        var userEntityDB = (from u in context.User.Include("Role")
                                            where u.ID == userEntity.ID
                                            select u).SingleOrDefault<Custom.Data.User>();

                        //get the roles from the database using the list of id's
                        userRoleSel = context.Role.Where(r => userRoles.Contains(r.ID)).ToList();
                        //assign the new roles, this will replace any existing roles
                        userEntityDB.Role = userRoleSel;

                        context.SaveChanges();
                    }

                    entityTransport.EntityList.Add(new Custom.Data.User() { ID = userEntity.ID });
                    entityTransport.Message = "";
                    entityTransport.Count = 1;
                }
            } catch(Exception ex) {

                entityTransport.Message = ex.Message;
            }

            return entityTransport;
        }

        /// <summary>
        /// Soft deletes a user from the database
        /// </summary>
        /// <param name="userID">The users id in  the system</param>
        /// <returns>a user entity with the updated values</returns>
        [OperationContract]
        [Security("Admin,UsersEdit")]
        public EntityTransport<Custom.Data.User> DeleteUser(long userID) {

            Utility.WCFCheckSecurityAttribute();

            Custom.Data.User userEntity = new Custom.Data.User();

            try {

                Utility.CheckSession();

                using(var context = new Custom.Data.Honeycomb_Entities()) {
                    userEntity = context.User.Where(u => u.ID == userID).FirstOrDefault();

                    if(userEntity != null) {
                        userEntity.DeletedByID = SessionManager.UserID;
                        userEntity.DeletedBy = SessionManager.UserName;
                        userEntity.DeletedOn = DateTime.Now;
                        context.Entry(userEntity).State = System.Data.EntityState.Modified;
                        context.SaveChanges();
                    }
                }

                return Utility.ServiceResponse(ref userEntity);
            } catch(Exception ex) {
                return Utility.ServiceResponse(ref userEntity, ex);
            }

        }

        /// <summary>
        /// Gets a user from the database
        /// </summary>
        /// <param name="userID">The users id in  the system</param>
        /// <returns>a custom user entity with the users roles and the roles that the user is not currently assigned to.</returns>
        [OperationContract]
        [Security("Admin,UsersEdit")]
        public EntityTransport<Custom.Data.UserAndRoles> GetUser(long userID) {

            Utility.WCFCheckSecurityAttribute();

            var userEntity = new Custom.Data.User();
            List<Custom.Data.Role> userRoles = new List<Custom.Data.Role>();
            List<Custom.Data.Role> allRoles = new List<Custom.Data.Role>();
            var userAndRoles = new Custom.Data.UserAndRoles();

            try {

                using(var context = new Custom.Data.Honeycomb_Entities()) {
                    userEntity = context.User.Where(u => u.ID == userID).SingleOrDefault();
                    userRoles = context.Role.Where(r => r.User.Any(u => u.ID == userID)).OrderBy(o => o.RoleName).ToList();
                    allRoles = context.Role.ToList().Where(r => !userRoles.Select(ur => ur.ID).Contains(r.ID)).OrderBy(o => o.RoleName).ToList();
                }

                userEntity.Password = Encryption.Decrypt(userEntity.Password);

                userAndRoles.User = userEntity;
                userAndRoles.UserRoles = userRoles;
                userAndRoles.Roles = allRoles;

                return Utility.ServiceResponse(ref userAndRoles);

            } catch(Exception ex) {
                return Utility.ServiceResponse(ref userAndRoles, ex);
            }
        }

        /// <summary>
        /// Gets a list of users who are assigned any of the roles in the provided array.
        /// </summary>
        /// <param name="roles"></param>
        /// <returns></returns>
        [OperationContract]
        [Security("Admin,UsersAdd,UsersEdit,SecurityIncidentsView,SecurityIncidentsAdd,SecurityIncidentsEdit")]
        public EntityTransport<Custom.Data.User> GetIncidentUsers() {

            Utility.WCFCheckSecurityAttribute();

            var users = new List<Custom.Data.User>();

            try {

                users = Custom.Security.GetUsersWithRoles(new long [] { 10009, 10010, 10015, 10023 });

                return Utility.ServiceResponse(ref users);

            } catch(Exception ex) {

                return Utility.ServiceResponse(ref users, ex);
            }
        }


        public string LoginAPI(string UserName, string Password, string APIKey) {
            string retVal = null;
            if (SessionManager.UserName == null) {
                Honeycomb.Security.Membership ms = new Honeycomb.Security.Membership();
                if (ms.ValidateUser(UserName, Password)) {
                    retVal = SessionManager.SessionID;
                    using (var context = new Custom.Data.Honeycomb_Entities()) {
                        Guid key = Guid.Parse(APIKey);
                        var user = context.User.Where(u => u.UserName == UserName && u.APIToken == key).SingleOrDefault();
                        if (user == null) {
                            throw new System.Exception("Invalid APIKey.");
                        }
                    }
                } else {
                    throw new System.Exception("Invalid API username or password.");
                }
            } else {
                retVal = SessionManager.SessionID;
            }
            return retVal;
        }



        #endregion

        #region guard gate allocation
        /// <summary>
        /// Gets a list of users who are currently assigned to a gate
        /// </summary>
        /// <param name="gateID">If provided function will return all users assigned to that gate, if null it will return all users who are assigned to any gate</param>
        /// <returns>A custom entity list of type CustomGateUser</returns>
        [OperationContract]
        [Security("Admin,GuardAllocation")]
        public EntityTransport<CustomGateUser> GetGateUsers(PaginationContainer pagination, long? gateID = null) {

            Utility.WCFCheckSecurityAttribute();

            var gateUsers = new List<CustomGateUser>();

            try {
                using(var context = new Honeycomb_Entities()){

                    if(gateID != null) {
                        gateUsers = context.User.Where(u => u.Gate.ID == gateID && u.DeletedOn == null && u.ID != 10039)
                                    .Select(s => new CustomGateUser(){ Gate = s.Gate.Name, GateID = s.GateID, Name = s.FirstName + " " + s.LastName, UserID = s.ID, Username = s.UserName})
                                    .ToList();
                    } else {
                        gateUsers = context.User.Where(u => u.GateID != null && u.DeletedOn == null && u.ID != 10039)
                                    .Select(s => new CustomGateUser() { Gate = s.Gate.Name, GateID = s.GateID, Name = s.FirstName + " " + s.LastName, UserID = s.ID, Username = s.UserName })
                                    .ToList();
                    }

                }

                var paginatedResult = gateUsers.AsQueryable().Paginate(pagination.CurrentPage, pagination.PageSize, pagination.OrderBy).ToList();
                return Utility.ServiceResponse(ref paginatedResult,gateUsers.Count());
            }catch(Exception ex){
                return Utility.ServiceResponse(ref gateUsers, ex);
            }
        }

        /// <summary>
        /// Gets all the gates for the system
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [Security("Admin,GuardAllocation")]
        public EntityTransport<Gate> GetGates() {

            Utility.WCFCheckSecurityAttribute();

            var gates = new List<Gate>();

            try {
                using(var context = new Honeycomb_Entities()){
                    gates = context.Gate.ToList();
                }

                return Utility.ServiceResponse(ref gates);
            }catch(Exception ex){
                return Utility.ServiceResponse(ref gates, ex);
            }
        }

        /// <summary>
        /// assigns a user to the gate, if gateid is null it will clear the users gate
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="gateID"></param>
        /// <returns></returns>
        [OperationContract]
        [Security("Admin,GuardAllocation")]
        public EntityTransport<bool> AssignUserTogate(long userID, long? gateID) {

            Utility.WCFCheckSecurityAttribute();

            bool success = false;

            try {
                using(var context = new Honeycomb_Entities()){
                    var userDB = context.User.Where(u => u.ID == userID).FirstOrDefault();

                    if(userDB != null) {
                        userDB.GateID = gateID;
                        context.SaveChanges();
                        success = true;
                    }
                }

                return Utility.ServiceResponse(ref success);
            }catch(Exception ex){
                return Utility.ServiceResponse(ref success, ex);
            }
        }

        /// <summary>
        /// gets all the users who have been assigned the gate control role
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [Security("Admin,GuardAllocation")]
        public EntityTransport<Custom.Data.User> GetGateControlUsers() {

            Utility.WCFCheckSecurityAttribute();

            var users = new List<Custom.Data.User>();

            try {

                users = Custom.Security.GetUsersWithRoles(new long [] { 10012 });

                return Utility.ServiceResponse(ref users);

            } catch(Exception ex) {

                return Utility.ServiceResponse(ref users, ex);
            }
        }

		/// <summary>
		/// Checks if a guard is currently assigned a gate, if not returns false.
		/// </summary>
		/// <param name="userID">The UserID of the guard</param>
		/// <returns></returns>
		[OperationContract]
		[Security("Admin,SecurityGateControl")]
		public static void CheckGuardHasGate(long userID) {
			Utility.WCFCheckSecurityAttribute();

			using(var context = new Honeycomb_Entities()){
				var gateGuard = context.User.Where(u => u.ID == userID && u.GateID != null).FirstOrDefault();

				if(gateGuard == null) {
					throw new Exception("Guard is not assigned a gate");
				}
			}

		}
        #endregion

        #region System
        [OperationContract]
        public EntityTransport<bool> KeepAlive() {
            var success = true;
            try {
                Utility.CheckSession();
                return Utility.ServiceResponse(ref success);
            } catch(Exception ex) {
                success = false;
                return Utility.ServiceResponse(ref success,ex);
            }
        }

        #endregion

        #region guest list scheduling methods
        /// <summary>
        /// returns a list of guest lists
        /// </summary>
        /// <param name="OccupantID"></param>
        /// <returns></returns>
        [OperationContract]
        [Security("Admin,SecurityAccessCodeManage")]
        public EntityTransport<Custom.Data.GuestList> GetOccupantGuestLists(long OccupantID) {

            Utility.WCFCheckSecurityAttribute();

            var occupantGuestLists = new List<Custom.Data.GuestList>();

            try {

                using(var context = new Custom.Data.Honeycomb_Entities()){
                    occupantGuestLists = context.GuestList.Where(ogl => ogl.OccupantID == OccupantID && ogl.DeletedOn == null).ToList();
                }

                return Utility.ServiceResponse(ref occupantGuestLists);
            }catch(Exception ex){
                return Utility.ServiceResponse(ref occupantGuestLists, ex);
            }
        }

        /// <summary>
        /// Sets the guest list as deleted, this is a soft delete
        /// </summary>
        /// <param name="GuestListID"></param>
        /// <returns></returns>
        [OperationContract]
        [Security("Admin,SecurityAccessCodeManage")]
        public EntityTransport<bool> DeleteGuestList(long GuestListID) {

            Utility.WCFCheckSecurityAttribute();

            bool DeleteSuccess = true;
            try {
                using(var context = new Custom.Data.Honeycomb_Entities()){
                    var GuestListDB = context.GuestList.Where(gl => gl.ID == GuestListID).FirstOrDefault();
                    GuestListDB.DeletedOn = DateTime.Now;
                    GuestListDB.DeletedByID = SessionManager.UserID;
                    GuestListDB.DeletedBy = SessionManager.UserName;
                    context.SaveChanges();
                }

                return Utility.ServiceResponse(ref DeleteSuccess);
            }catch(Exception ex){
                DeleteSuccess = false;
                return Utility.ServiceResponse(ref DeleteSuccess, ex);
            }
        }

        /// <summary>
        /// Updates or Adds a guest list record, adds and deletes the visitor list for the Guest list
        /// </summary>
        /// <param name="GuestList"></param>
        /// <param name="VisitorsList"></param>
        /// <returns></returns>
		[OperationContract]
		[Security("Admin,SecurityAccessCodeManage")]
		public EntityTransport<long> SaveGuestList(Custom.Data.GuestList GuestList, List<Custom.Data.GuestListVisitor> VisitorsList) {

			Utility.WCFCheckSecurityAttribute();

			long GuestListID = 0;

			try {

				using(var context = new Custom.Data.Honeycomb_Entities()) {

					//check if this is an update or an add
					if(GuestList.ID == 0) {
						//it's an add so add a record
						context.GuestList.Add(GuestList);
						GuestList.InsertedOn = DateTime.Now;
						GuestList.InsertedBy = SessionManager.UserName;
						GuestList.InsertedByID = SessionManager.UserID;
						GuestList.SystemID = CacheManager.SystemID;

						//push the Visitors list to the Guest List
						GuestList.GuestListVisitor = VisitorsList;

					} else {
						//it's an update get the guest list from the DB and include the current visitors so that when we add a new list it 
						//will delete the existing visitors and replace them with the new list we provide
						var GuestListDB = context.GuestList.Include("GuestListVisitor").Where(gl => gl.ID == GuestList.ID).FirstOrDefault();
						GuestList.MergeInto(GuestListDB, new string[] {"ID",
                        "InsertedOn",
                        "InsertedBy",
                        "InsertedByID",
                        "DeletedOn",
                        "DeleteBy",
                        "DeletedByID"}, true);

						//overwrite the old Guest list with the new one
						GuestListDB.GuestListVisitor = VisitorsList;
					}

					//save after assigning the visitors to the list
					context.SaveChanges();

					GuestListID = GuestList.ID;
				}

				return Utility.ServiceResponse(ref GuestListID);
			} catch(Exception ex) {
				return Utility.ServiceResponse(ref GuestListID, ex);
			}
		}

        /// <summary>
        /// gets the current visitors for a list
        /// </summary>
        /// <param name="GuestListID"></param>
        /// <returns></returns>
        [OperationContract]
        [Security("Admin,SecurityAccessCodeManage")]
        public EntityTransport<Custom.Data.GuestListVisitor> GetVisitorsByList(long GuestListID) {

            Utility.WCFCheckSecurityAttribute();

            var VisitorList = new List<Custom.Data.GuestListVisitor>();

            try {

                using(var context = new Custom.Data.Honeycomb_Entities()){
                    VisitorList = context.GuestListVisitor.Where(gl=> gl.GuestListID == GuestListID).ToList();
                }

                return Utility.ServiceResponse(ref VisitorList);
            }catch(Exception ex){
                return Utility.ServiceResponse(ref VisitorList, ex);
            }
        }
        #endregion

        #region Access Code Creation Methods
        /// <summary>
        /// Gets a list of all the opccupants on the system
        /// </summary>
        /// <returns>a list of occupant entities.</returns>
        [OperationContract]
        [Security("Admin,SecurityAccessCodeManage,SecurityGateControl,SecurityIncidentsAdd,SecurityIncidentsEdit,SecurityIncidentsView")]
        public EntityTransport<Custom.Data.Occupant> GetOccupants() {

            Utility.WCFCheckSecurityAttribute();

            var occupantsList = new List<Custom.Data.Occupant>();

            try {
                Utility.CheckSession();

                using(var context = new Honeycomb.Custom.Data.Honeycomb_Entities()) {
                    occupantsList = context.Occupant.Where(o => o.SystemID == SessionManager.SystemID && o.DeletedOn == null && o.PropertyOccupant.Any(po => po.ArchivedOn == null && po.Property.DeletedOn == null && po.Property.Ownership.Any(own => own.DeletedOn == null && own.ArchivedOn == null))).OrderBy(o => o.FirstName).ThenBy(o => o.LastName).ToList();
                }

                return Utility.ServiceResponse(ref occupantsList);

            } catch(Exception ex) {
                return Utility.ServiceResponse(ref occupantsList, ex);
            }
        }

        /// <summary>
        /// Gets a list of the opccupants on the system whoose name matches the provided search term.
        /// </summary>
        /// <returns>a list of occupant entities.</returns>
        [OperationContract]
        [Security("Admin,SecurityAccessCodeManage,SecurityGateControl")]
        public EntityTransport<Custom.Data.AutoCompleteObject> GetOccupantsSearch(string SearchTerm) {

            Utility.WCFCheckSecurityAttribute();

            var occupantsList = new List<Custom.Data.AutoCompleteObject>();

            try {
                Utility.CheckSession();

                using(var context = new Honeycomb.Custom.Data.Honeycomb_Entities()) {
                    occupantsList = (from occ in context.Occupant
                                     where occ.SystemID == SessionManager.SystemID
                                     && occ.DeletedOn == null
                                     && (occ.FirstName.ToLower().Contains(SearchTerm) || occ.LastName.ToLower().Contains(SearchTerm))
                                     select new Custom.Data.AutoCompleteObject { 
                                         label = occ.FirstName + " " + occ.LastName,
                                         value = occ.Cellphone
                                     }).Take(10).ToList();
                }

                return Utility.ServiceResponse(ref occupantsList);

            } catch(Exception ex) {
                return Utility.ServiceResponse(ref occupantsList, ex);
            }
        }

        /// <summary>
        /// Takes a list of visitors and creates access codes for them. It then sends off the access codes to the occupants cellphone number.
        /// </summary>
        /// <param name="visitors">list of visitors that need codes</param>
        /// <param name="occupantID">the id of the occupant id there is one</param>
        /// <param name="sendToOccupant">a boolean indicating whether the code must be sent to the occupant</param>
        /// <param name="sendToVisitor">a boolean indicating whether the code must be sent to the visitor</param>
        /// <returns></returns>
        [OperationContract]
        [Security("Admin,SecurityAccessCodeManage")]
        public EntityTransport<Custom.Data.VisitorAccessCodes> CreateAccesCodes(List<Custom.Data.AccessVisitor> visitors, long occupantID, DateTime? dateValid = null, bool sendToOccupant = false, bool sendToVisitor = false, bool useIntranetSettings = false) {

            Utility.WCFCheckSecurityAttribute();

            var visitorAccessCodesList = new List<Custom.Data.VisitorAccessCodes>();
            Custom.Security security = new Custom.Security();
			DateTime today = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);

            try {

				string origin = useIntranetSettings ? "OccupantIntranet" : "ControlRoom";

                Utility.CheckSession();
				visitorAccessCodesList = security.CreateAccesCodes(visitors, origin, occupantID, dateValid, sendToOccupant, sendToVisitor, useIntranetSettings);

				if(dateValid != null) {
					//code is valid in the future and we can schedule an X day reminder sms because it is far enough in the future to do so
					if(dateValid.Value.Subtract(today.AddDays((double)CacheManager.SystemSetting.SendCodeXDaysBefore)).Days > 0) { 
						//schedule the X Day reminder sms for delivery for each visitor X days before the valid date
						foreach(var visitor in visitorAccessCodesList) {
							Custom.Security.SendAccessCodeSMS(visitor.AccessCode, sendToVisitor, sendToOccupant, dateValid.Value.Subtract(new TimeSpan(CacheManager.SystemSetting.SendCodeXDaysBefore,0,0,0)).Date);
						}
					}
				}

                return Utility.ServiceResponse(ref visitorAccessCodesList);
            } catch(Exception ex) {
                return Utility.ServiceResponse(ref visitorAccessCodesList, ex);
            }
        }
        #endregion

        #region Gate Control Methods

        /// <summary>
        /// Gets the active access code for todays date
        /// </summary>
        /// <param name="VisitorAccessCode"></param>
        /// <returns></returns>
        [OperationContract]
        [Security("Admin,SecurityGateControl")]
        public EntityTransport<Custom.Data.VisitorAndAcccessCode> GetAccessCodeForToday(string VisitorAccessCode) {

            Utility.WCFCheckSecurityAttribute();

            Custom.Data.VisitorAndAcccessCode theAccessCode = new Custom.Data.VisitorAndAcccessCode();
            Custom.Data.AccessCode AccessCode = new Custom.Data.AccessCode();
            Custom.Data.AccessVisitor Visitor = new Custom.Data.AccessVisitor();
            Custom.Data.Occupant Occupant = new Custom.Data.Occupant();
            var currentDate = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day);

            try {

                using(var context = new Custom.Data.Honeycomb_Entities()) {
                    AccessCode = (from ac in context.AccessCode
                                  where ac.DeletedOn == null
                                  && ac.DisabledByID == null
                                  && ac.DisabledOn == null
                                  && ac.VisitorAccessCode == VisitorAccessCode
                                  && ac.ExitedOn == null
                                  select ac).FirstOrDefault();


				#region code validility checks
					if(AccessCode != null) {

                        if(!AccessCode.Used.Value) {
							//if code is not valid for today return empty poco to indicate failure
                            if(AccessCode.DateValidFor != currentDate) {
                                return Utility.ServiceResponse(ref theAccessCode);
                            }
                        } else {

							//system setting indicates that codes expire
							if(CacheManager.SystemSetting.AccessCodeExpire) {

								//if the code is for exit and has exceeded the allowed validility period return empty poco to indicate failure
								if(!(AccessCode.EnteredOn.Value.AddHours(CacheManager.SystemSetting.AccessCodeVaildPeriod) >= DateTime.Now)) {
									return Utility.ServiceResponse(ref theAccessCode);
								}
							} else { 
								//codes do not expire
								//if the validility period is greater than 0 it means the system must check if the validility period has lapsed
								if(CacheManager.SystemSetting.AccessCodeVaildPeriod > 0 && ((AccessCode.EnteredOn.Value.AddHours(CacheManager.SystemSetting.AccessCodeVaildPeriod) < DateTime.Now))) {
									//indicate that the system must capture a reason for why the person has exited after the validility period
									theAccessCode.CaptureExpiredReason = true;
								}
							}
                        }

                    } else {
						//code cannot be found return empty poco to indicate failure
                        return Utility.ServiceResponse(ref theAccessCode);
					}
					#endregion		

					//the code is valid for either entry or exit, proceed with populating poco for return to front end
                    Visitor = context.AccessVisitor.Where(v => v.ID == AccessCode.VisitorID).FirstOrDefault();
                    Occupant = context.Occupant.Where(o => o.ID == AccessCode.OccupantOriginatorID).FirstOrDefault();

                    theAccessCode.AccessCode = AccessCode.VisitorAccessCode;
                    if(Occupant != null) {
                        theAccessCode.OccupantName = Occupant.FirstName + " " + Occupant.LastName;
                        theAccessCode.OccupantCell = Occupant.Cellphone;

                        Property prop = context.Property.Where(p => p.PropertyOccupant.Any(po => po.OccupantID == Occupant.ID)).FirstOrDefault();

                        theAccessCode.OccupantAddress = !string.IsNullOrEmpty(prop.DoorNumber) ? prop.StreetNumber + " " + prop.StreetName + " Door: " + prop.DoorNumber : prop.StreetNumber + " " + prop.StreetName;
                    } else {
                        theAccessCode.OccupantName = "N/A";
                        theAccessCode.OccupantCell = "N/A";
                    }

                    theAccessCode.VisitorName = Visitor.VisitorName;
                    theAccessCode.AccessCodeID = AccessCode.ID;
                    theAccessCode.Used = AccessCode.Used;
                    theAccessCode.VechicleOccupantIn = AccessCode.VehicleOccupantIn;
                    theAccessCode.VechicleRegIn = AccessCode.VehicleRegIn;
					theAccessCode.CodeValidilityPeriod = CacheManager.SystemSetting.AccessCodeVaildPeriod;
                }

                return Utility.ServiceResponse(ref theAccessCode);

            } catch(Exception ex) {
                return Utility.ServiceResponse(ref theAccessCode, ex);
            }
        }

        /// <summary>
        /// Captures the entry of an access code in the system.
        /// </summary>
        /// <param name="AccessCode"></param>
        /// <param name="VechicleRegIn"></param>
        /// <param name="VechicleOccupantIn"></param>
        /// <returns>the modified access code record</returns>
        [OperationContract]
        [Security("Admin,SecurityGateControl")]
        public EntityTransport<bool> CaptureEntryForAccessCode(Guid AccessCode, string VechicleRegIn, int VechicleOccupantIn, long GuardID, string entryXML) {

            Utility.WCFCheckSecurityAttribute();
			Honeycomb.Security.WCFSecurity.CheckGuardHasGate(GuardID);

			CheckCodeValidForEntry(AccessCode);
            Custom.Data.AccessCode TheAccessCode = new Custom.Data.AccessCode();
			bool success = true;

            try {

                Utility.CheckSession();

                using(var context = new Custom.Data.Honeycomb_Entities()) {
                    TheAccessCode = context.AccessCode.Where(ac => ac.ID == AccessCode).FirstOrDefault();
                    
                    User gateGaurd = context.User.Where(u => u.ID == GuardID).FirstOrDefault();

                    TheAccessCode.VehicleRegIn = VechicleRegIn;
                    TheAccessCode.VehicleOccupantIn = VechicleOccupantIn;
                    TheAccessCode.Used = true;
                    TheAccessCode.EnteredOn = DateTime.Now;

					//capture gate id
					TheAccessCode.EnteredGateID = gateGaurd.GateID;
                    //audit stuff for capturing guard who made this record
                    TheAccessCode.GuardAuditEnteredBy = gateGaurd.UserName;
                    TheAccessCode.GuardAuditEnteredID = gateGaurd.ID;

					if(!string.IsNullOrEmpty(entryXML)) {
						try {

							var tmpParseCheck = XDocument.Parse(entryXML);
							TheAccessCode.EntryXML = tmpParseCheck.ToString();
						}catch(Exception ex){
							throw new Exception("Error 04: Entry XML passed does not validate. Message: " + ex.Message);
						}
					}

                    context.SaveChanges();
                }

				return Utility.ServiceResponse(ref success);
            } catch(Exception ex) {
				success = false;
				return Utility.ServiceResponse(ref success, ex);
            }
        }

        /// <summary>
        /// Puts the access code into exited state
        /// </summary>
        /// <param name="AccessCodeID"></param>
        /// <param name="VehicleRegOut"></param>
        /// <param name="VehicleOccupantOut"></param>
        /// <param name="ReasonForDiscrepancy"></param>
        /// <returns></returns>
        [OperationContract]
        [Security("Admin,SecurityGateControl")]
        public EntityTransport<bool> CaptureExitForAccessCode(Guid AccessCodeID, string VehicleRegOut, int VehicleOccupantOut, long GuardID, string ReasonForDiscrepancy = "", string lateExitReason = "", string exitXML = "") {

            Utility.WCFCheckSecurityAttribute();
			Honeycomb.Security.WCFSecurity.CheckGuardHasGate(GuardID);

            Custom.Data.AccessCode AccessCodeEnt = new Custom.Data.AccessCode();
            bool success = true;

            try {
                
                using(var context = new Custom.Data.Honeycomb_Entities()) {
                    AccessCodeEnt = context.AccessCode.Include("AccessVisitor").Where(ac => ac.ID == AccessCodeID).FirstOrDefault();

					if(AccessCodeEnt != null) {

						User gateGaurd = context.User.Where(u => u.ID == GuardID).FirstOrDefault();

						AccessCodeEnt.VehicleRegOut = VehicleRegOut;
						AccessCodeEnt.VehicleOccupantOut = VehicleOccupantOut;
						AccessCodeEnt.ExitedOn = DateTime.Now;

						//capture gate id
						AccessCodeEnt.ExitedGateID = gateGaurd.GateID;
						//audit stuff for capturing guard who made this record
						AccessCodeEnt.GuardAuditExitedBy = gateGaurd.UserName;
						AccessCodeEnt.GuardAuditExitedID = gateGaurd.ID;

						//check if a late exit reason needs to be captured
						if(!CacheManager.SystemSetting.AccessCodeExpire && (CacheManager.SystemSetting.AccessCodeVaildPeriod > 0 && ((AccessCodeEnt.EnteredOn.Value.AddHours(CacheManager.SystemSetting.AccessCodeVaildPeriod) < DateTime.Now)))) {
							if(!string.IsNullOrEmpty(lateExitReason)) {
								AccessCodeEnt.LateExitReason = lateExitReason;
							} else {								
								throw new Exception("Error 03: No late exit reason provided.");
							}
						}

						if(AccessCodeEnt.VehicleOccupantIn != AccessCodeEnt.VehicleOccupantOut) {
							if(ReasonForDiscrepancy == "") {
								throw new Exception("Error 02: No occupant in vs occupant out discrepency reason.");
							}

							//store the reason for the discrepancy
							AccessCodeEnt.DiscrepancyReason = ReasonForDiscrepancy;
							//send the occupant who requested this code a message.
							if(AccessCodeEnt.OccupantOriginatorID != null) {
								Custom.Data.Occupant Occupant = context.Occupant.Where(oc => oc.ID == AccessCodeEnt.OccupantOriginatorID).FirstOrDefault();

								if(!string.IsNullOrEmpty(Occupant.Cellphone)) {
									Communications comUtil = new Communications();
									comUtil.SendSMS(AccessCodeEnt.VehicleOccupantIn.ToString() + " guests arrived with visitor " + AccessCodeEnt.AccessVisitor.VisitorName + " and " + AccessCodeEnt.VehicleOccupantOut.ToString() + " left. Please contact your visitor if you have concerns.", Occupant.Cellphone);
								}
							}
						}

						//setting the XML
						if(!string.IsNullOrEmpty(exitXML)) {
							try {
								var tmpExitXMLParsed = XDocument.Parse(exitXML);
								AccessCodeEnt.ExitXML = tmpExitXMLParsed.ToString();
							}catch(Exception ex){
								throw new Exception("Error 04: Exit XML passed does not validate. Message: " + ex.Message);
							}
						}

						context.SaveChanges();

					} else {
						throw new Exception("Error 01: Access code is not valid.");
					}
                }

                return Utility.ServiceResponse(ref success);
            } catch(Exception ex) {
                success = false;
                return Utility.ServiceResponse(ref success, ex, false);
            }
        }

		/// <summary>
		/// checks if a code is valid for entry, this is a security check used by the CaptureEntryForAccessCode method
		/// </summary>
		/// <param name="AccessCodeID"></param>
		/// <returns>boolean indicating code is ready for entry processing</returns>
		private static void CheckCodeValidForEntry(Guid AccessCodeID) {

			bool codeIsValid = false;
			var currentDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);

			using(var context = new Custom.Data.Honeycomb_Entities()) {
				var AccessCodeDB = (from ac in context.AccessCode
							  where ac.DeletedOn == null
							  && ac.DisabledByID == null
							  && ac.DisabledOn == null
							  && ac.ID == AccessCodeID
							  && ac.ExitedOn == null
							  select ac).FirstOrDefault();

				//check code exists
				if(AccessCodeDB != null) {

					//check code has not been used already
					if(!AccessCodeDB.Used.Value) {
						//if code is not valid for today return empty poco to indicate failure
						if(AccessCodeDB.DateValidFor != currentDate) {
							codeIsValid = false;
						} else {
							//code is valid 
							codeIsValid = true;
						}
					} else {
						//code already used, code is not valid for entry
						codeIsValid = false;
					}

				} else {
					//code does no exist in database
					codeIsValid = false;
				}
			}

			//if the code is not valid throw an exception
			if(!codeIsValid) {
				throw new Exception("Code not valid for entry!");
			}

		}


        #endregion

        #region Security Log Incidents
        /// <summary>
        /// Retrieves a paginated set of incidents which can be filtered by erfnumber or ownername
        /// </summary>
        /// <param name="pagination">Pagination object</param>
        /// <param name="filterBY">A string which will be matched on either erfnumber or ownername. If null or empty no comparism is made.</param>
        /// <returns></returns>
        [OperationContract]
        [Security("Admin,SecurityIncidentsAdd,SecurityIncidentsEdit,SecurityIncidentsView")]
        public EntityTransport<Custom.Data.IncidentsDetail> GetIncidents(PaginationContainer pagination, DateTime? fromDate, DateTime? toDate, long? incidentID, long? incidentTypeID, string ownerName, string erfNumber, string occupantName) {

            Utility.WCFCheckSecurityAttribute();

            var incidents = new List<Custom.Data.IncidentsDetail>();
            try {
                
                //execute stored proc which retrieves all incidents matching the search criteria
                using(var context = new Honeycomb.Custom.Data.Honeycomb_Entities()) {
					incidents = context.GetIncidents(fromDate, toDate, incidentID, incidentTypeID, ownerName, erfNumber, occupantName).ToList();
                }

                var paginatedResult = incidents.AsQueryable().Paginate(pagination.CurrentPage, pagination.PageSize, pagination.OrderBy).ToList();
                return Utility.ServiceResponse(ref paginatedResult, incidents.Count);
            } catch(Exception ex) {
                return Utility.ServiceResponse(ref incidents, ex);
            }

        }

        /// <summary>
        /// Gets an incident out of the database
        /// </summary>
        /// <param name="incidentID"></param>
        /// <returns></returns>
        [OperationContract]
        [Security("Admin,SecurityIncidentsAdd,SecurityIncidentsEdit,SecurityIncidentsView")]
        public EntityTransport<Incident> GetIncident(long incidentID) {

            Utility.WCFCheckSecurityAttribute();

            var incident = new Incident();

            try {
                using(var context = new Honeycomb_Entities()){
                    incident = context.Incident.Where(i => i.ID == incidentID).FirstOrDefault();
                }

                return Utility.ServiceResponse(ref incident);
            }catch(Exception ex){
                return Utility.ServiceResponse(ref incident, ex);
            }
        }

        [OperationContract]
        [Security("Admin,SecurityIncidentsAdd,SecurityIncidentsEdit,SecurityIncidentsView")]
        public EntityTransport<string> GetIncidentPropertyDesc(long incidentID) {

            Utility.WCFCheckSecurityAttribute();

            string description = "";

            try {

                using(var context = new Honeycomb_Entities()){
                    description = (from incident in context.Incident
                                   join own in context.Ownership on incident.PropertyID equals own.PropertyID into ownGroup
                                   from propOwner in ownGroup.DefaultIfEmpty()
                                   join erf in context.Erf on incident.ErfID equals erf.ID into erfGroup
                                   from propErf in erfGroup.DefaultIfEmpty()
                                   where incident.ID == incidentID
                                   && propOwner.ArchivedOn == null
                                   select (propOwner == null) ? propErf.Name : propOwner.PropertyName).FirstOrDefault();
                }

                return Utility.ServiceResponse(ref description);
            }catch(Exception ex){
                return Utility.ServiceResponse(ref description, ex);
            }
        }

        /// <summary>
        /// Gets a list of occupants who have been assigned to this incident
        /// </summary>
        /// <param name="incidentID"></param>
        /// <returns></returns>
        [OperationContract]
        [Security("Admin,SecurityIncidentsAdd,SecurityIncidentsEdit,SecurityIncidentsView")]
        public EntityTransport<AutoCompleteObject> GetIncidentOccupants(long incidentID) {

            Utility.WCFCheckSecurityAttribute();

            var occupantList = new List<AutoCompleteObject>();

            try {
                using(var context = new Honeycomb_Entities()) {
                    occupantList = context.Occupant.Where(o => o.Incident.Any(i => i.ID == incidentID)).AsEnumerable().Select(s => new AutoCompleteObject {
                        label = s.FirstName + " " + s.LastName,
                        value = s.ID.ToString()
                    }).ToList();
                }

                return Utility.ServiceResponse(ref occupantList);
            } catch(Exception ex) {
                return Utility.ServiceResponse(ref occupantList, ex);
            }
        }

        /// <summary>
        /// Gets a list of all the opccupants on the system
        /// </summary>
        /// <returns>a list of occupant entities.</returns>
        [OperationContract]
        [Security("Admin,SecurityIncidentsAdd,SecurityIncidentsEdit,SecurityIncidentsView")]
        public EntityTransport<Custom.Data.Occupant> GetOccupantsLookUp() {

            Utility.WCFCheckSecurityAttribute();

            var occupantsList = new List<Custom.Data.Occupant>();

            try {
                Utility.CheckSession();

                using(var context = new Honeycomb.Custom.Data.Honeycomb_Entities()) {
                    occupantsList = context.Occupant.Where(o => o.SystemID == SessionManager.SystemID && o.DeletedOn == null).OrderBy(o => o.FirstName).ThenBy(o => o.LastName).ToList();
                }

                return Utility.ServiceResponse(ref occupantsList);

            } catch(Exception ex) {
                return Utility.ServiceResponse(ref occupantsList, ex);
            }
        }

        /// <summary>
        /// Gets a list of all the opccupants on the system
        /// </summary>
        /// <returns>a list of occupant entities.</returns>
        [OperationContract]
        [Security("Admin,SecurityIncidentsAdd,SecurityIncidentsEdit,SecurityIncidentsView")]
        public EntityTransport<AutoCompleteObject> GetOccupantsAutoCompleteLookUp(string searchString) {

            Utility.WCFCheckSecurityAttribute();

            var occupantsList = new List<AutoCompleteObject>();

            try {
                Utility.CheckSession();

                using (var context = new Honeycomb.Custom.Data.Honeycomb_Entities()) {
                    occupantsList = context.Occupant.Where(o =>
                        o.SystemID == SessionManager.SystemID &&
                        o.DeletedOn == null && (
                            o.FirstName.ToLower().Contains(searchString.ToLower()) ||
                            o.LastName.ToLower().Contains(searchString.ToLower())
                        )).OrderBy(o => o.FirstName).ThenBy(o => o.LastName).AsEnumerable().Select(s => new AutoCompleteObject {
                            label = s.FirstName + " " + s.LastName,
                            value = s.ID.ToString()
                        }).ToList();
                }

                return Utility.ServiceResponse(ref occupantsList);

            } catch (Exception ex) {
                return Utility.ServiceResponse(ref occupantsList, ex);
            }
        }

        [OperationContract]
        [Security("Admin,SecurityIncidentsAdd,SecurityIncidentsEdit,SecurityIncidentsView,SecurityGateControl")]
        public EntityTransport<AutoCompleteObject> GetActiveOccupants(string searchString) {

            Utility.WCFCheckSecurityAttribute();

            var occupantsList = new List<AutoCompleteObject>();

            try {
                Utility.CheckSession();

                using(var context = new Honeycomb.Custom.Data.Honeycomb_Entities()) {
                    occupantsList = context.Occupant.Where(o =>
                        o.SystemID == SessionManager.SystemID &&
                        o.PropertyOccupant.Any(po => po.ArchivedOn == null && po.Property.DeletedOn == null) &&
                        o.DeletedOn == null &&
						((o.FirstName.ToLower() + " " + o.LastName.ToLower()).Contains(searchString.ToLower()) || o.Cellphone.Contains(searchString))
                        ).OrderBy(o => o.FirstName).ThenBy(o => o.LastName).AsEnumerable().Select(s => new AutoCompleteObject {
                            label = s.FirstName + " " + s.LastName + " (" + s.Cellphone + ")",
                            value = s.ID.ToString()
                        }).Take(10).ToList();
                }

                return Utility.ServiceResponse(ref occupantsList);

            } catch(Exception ex) {
                return Utility.ServiceResponse(ref occupantsList, ex);
            }
        }

        [OperationContract]
        [Security("Admin,SecurityAccessCodeManage,SecurityGateControl")]
        public EntityTransport<CustomOccupant> GetOccupant(long occupantID) {

            Utility.WCFCheckSecurityAttribute();

            var occupant = new CustomOccupant();

            try {
                using(var context = new Honeycomb_Entities()){
                    occupant = (from o in context.Occupant
                                join po in context.PropertyOccupant on o.ID equals po.OccupantID
                                join p in context.Property on po.PropertyID equals p.ID
                                join e in context.Erf on p.ERFID equals e.ID
                                where o.ID == occupantID
                                && o.DeletedOn == null
                                && po.ArchivedOn == null
                                select new CustomOccupant() { 
									FirstName = o.FirstName,
									LastName = o.LastName,
									IDNo = o.IDOrPassportNo,
									CanSMS = o.CanSMS,
									Address = p.StreetNumber + " " + p.StreetName + " Door No: " + p.DoorNumber,
									ID = o.ID,
									Cellphone = o.Cellphone,
									Email = o.Email,
									IsOwner = o.IsOwner,
									IsPrincipleOccupant = o.IsPrincipalOccupant,
									IsTenant = o.IsTenant,
									HasAccessCard = o.HasAccessCard,
									CanReceiveCommunication = o.RecieveComms,
									RegisteredOnClubMaster = o.ClubmasterReg,
									DateRegisteredOnClubMaster = o.ClubMasterRegDate,
									IsGolfMember = o.GolfMember,
									IsSocialMember = o.SocialMember
                                }).FirstOrDefault();
                }

                return Utility.ServiceResponse(ref occupant);
            }catch(Exception ex){
                return Utility.ServiceResponse(ref occupant, ex);
            }
        }
        
		[OperationContract]
        [Security("Admin,APIIntranet")]
		public EntityTransport<Byte[]> GetOccupantImage(long occupantID) {

			Utility.WCFCheckSecurityAttribute();

			Byte[] occupantImage = null;

			try {
				string occupantIDNoOrPassport;
				using (var context = new Custom.Data.Honeycomb_Entities()) {

					occupantIDNoOrPassport = context.Occupant.Where(w => w.ID == occupantID).FirstOrDefault().IDOrPassportNo;

					if(!string.IsNullOrEmpty(occupantIDNoOrPassport)) {

						var improSecurity = new Custom.ImproSecurity();
						var occupantImageOriginal = improSecurity.GetMasterImageByID(occupantIDNoOrPassport);

						if(occupantImageOriginal != null) {
							occupantImage = Utility.Resize(occupantImageOriginal, 128, 128);
						}
					}
				}
				
				return Utility.ServiceResponse(ref occupantImage);
			} catch (Exception ex) {
				return Utility.ServiceResponse(ref occupantImage, ex);
			}
		}
        

        /// <summary>
        /// gets the available incident types
        /// </summary>
        /// <returns>a list of type IncidentType</returns>
        [OperationContract]
        [Security("Admin,SecurityIncidentsAdd,SecurityIncidentsEdit,SecurityIncidentsView")]
        public EntityTransport<Custom.Data.IncidentType> GetIncidentTypes() {

            Utility.WCFCheckSecurityAttribute();

            var incidentTypes = new List<Custom.Data.IncidentType>();

            try {

                using(var context = new Custom.Data.Honeycomb_Entities()){
                    incidentTypes = context.IncidentType.OrderBy(i => i.Name).ToList();
                }

                return Utility.ServiceResponse(ref incidentTypes);
            }catch(Exception ex){
                return Utility.ServiceResponse(ref incidentTypes,ex);
            }
        }

        /// <summary>
        /// Used to retrieve a set of properties/erfs that match the search string, this function was specifically written for the autocomplete search function
        /// </summary>
        /// <param name="filter">A string to match the property name/Erf name on</param>
        /// <returns>A list of type AutoCompleteObject</returns>
        [OperationContract]
        [Security("Admin,SecurityIncidentsAdd,SecurityIncidentsEdit,SecurityIncidentsView")]
        public EntityTransport<AutoCompleteObject> GetProperties(string filter) {

            Utility.WCFCheckSecurityAttribute();

            var propertyOptions = new List<AutoCompleteObject>();

            try {

                using(var context = new Honeycomb_Entities()){

                    if(!string.IsNullOrEmpty(filter)) {
                        var ownershipMatches = context.Ownership.Where(o => o.DeletedOn == null && o.ArchivedOn == null && o.PropertyName.Contains(filter) && o.Property.DeletedOn == null).Take(20);
                        var erfMatches = context.Erf.Where(e => e.DeletedOn == null && e.Name.Contains(filter)).Take(20);

                        foreach(var owner in ownershipMatches) {
                            propertyOptions.Add(new AutoCompleteObject() { 
                                label = owner.PropertyName,
                                type = "property",
                                value = owner.PropertyID.ToString()
                            });
                        }

                        foreach(var erf in erfMatches) {
                            propertyOptions.Add(new AutoCompleteObject() {
                                label = erf.Name,
                                type = "erf",
                                value = erf.ID.ToString()
                            });
                        }

                        if(propertyOptions.Count() > 0) {
                            propertyOptions.OrderBy(o => o.label).Take(20);
                        }
                    }
                    
                }

                return Utility.ServiceResponse(ref propertyOptions);
            }catch(Exception ex){
                return Utility.ServiceResponse(ref propertyOptions, ex);
            }
        }

		/// <summary>
		/// adds an incident
		/// </summary>
		/// <param name="incident"></param>
		/// <param name="involvedPersons"></param>
		/// <returns></returns>
		[OperationContract]
		[Security("Admin,SecurityIncidentsAdd")]
		public EntityTransport<IncidentProcessResponse> AddIncident(Incident incident, long[] involvedPersons) {

			Utility.WCFCheckSecurityAttribute();

			var processResponse = new IncidentProcessResponse();
			var involvedPersonsList = new List<Occupant>();

			processResponse.success = true;
			processResponse.message = "Successfully saved incident";

			try {
				using(var context = new Honeycomb_Entities()) {

					//get the involved occupants out of the database
					if(involvedPersons != null) {
						involvedPersonsList = context.Occupant.Where(o => involvedPersons.Contains(o.ID)).ToList();
					}

					if(incident.ID == 0) {
						//add new incident
						incident.InsertedOn = DateTime.Now;
						incident.InsertedBy = SessionManager.UserName;
						incident.InsertedByID = SessionManager.UserID;

						//assign the involved persons to the incident
						if(involvedPersonsList.Count() > 0) {
							incident.Occupant = involvedPersonsList;
						}
						//save the changes to the database so that we can get the new incidents id
						context.Incident.Add(incident);
						context.SaveChanges();

						processResponse.IncidentID = incident.ID;

						//insert a new record which is marked as archived for storing the history of the incident
						var incidentHistory = new Incident();

						incident.MergeInto(incidentHistory, new string[] { }, true);

						incidentHistory.ArchivedOn = DateTime.Now;
						incidentHistory.IncidentID = incident.ID;

						context.Incident.Add(incidentHistory);
						context.SaveChanges();
					}
				}

				return Utility.ServiceResponse(ref processResponse);
			} catch(Exception ex) {
				processResponse.success = false;
				return Utility.ServiceResponse(ref processResponse, ex);
			}
		}

        /// <summary>
        /// Performs the save to the database for incidents, updates the incident
        /// </summary>
        /// <param name="incident">An Incident Entity</param>
        /// <param name="involvedPersons">An array of type long which represent occupant id's</param>
        /// <returns>A ProcessResponse object which contains a boolean indicating success and a string containing a message relating to the save.</returns>
        [OperationContract]
        [Security("Admin,SecurityIncidentsModify")]
        public EntityTransport<ProcessResponse> UpdateIncident(Incident incident, long[] involvedPersons) {

            Utility.WCFCheckSecurityAttribute();

            var processResponse = new ProcessResponse();
            var involvedPersonsList = new List<Occupant>();

            processResponse.success = true;
            processResponse.message = "Successfully saved incident";

            try {
                using(var context = new Honeycomb_Entities()){

                    //get the involved occupants out of the database
                    if(involvedPersons != null) {
                        involvedPersonsList = context.Occupant.Where(o => involvedPersons.Contains(o.ID)).ToList();
                    }

                    if(incident.ID != 0) { 
                        //do update
                        //get the incident out of the database
                        var incidentDB = context.Incident.Include("Occupant").Where(i => i.ID == incident.ID).FirstOrDefault();

                        if(incidentDB != null) {
                            //merge into the database entity
                            incident.MergeInto(incidentDB, new string [] { 
                                "DeletedOn",
                                "DeletedBy",
                                "DeletedByID",                                
                                "InsertedBy",
                                "InsertedByID",
                                "ArchivedOn",
                                "IncidentID",
								"InsertedOn",
								"ResolutionDescription"
                            }, true);

                            //assign the involved occupants to the incident
                            incidentDB.Occupant = involvedPersonsList;

                            //save the changes to db
                            context.SaveChanges();
                        } else {
                            processResponse.success = false;
                            processResponse.message = "Could not locate incident to perform update.";
                        }
                    }
                }

                return Utility.ServiceResponse(ref processResponse);
            }catch(Exception ex){
                processResponse.success = false;
                return Utility.ServiceResponse(ref processResponse, ex);
            }
        }

        [OperationContract]
        [Security("Admin,SecurityIncidentsEdit")]
        public EntityTransport<ProcessResponse> UpdateIncidentProgress(IncidentUpdate incidentUpdate) {

            Utility.WCFCheckSecurityAttribute();

            var processResponse = new ProcessResponse();

            try {

                using(var context = new Honeycomb_Entities()){
                    incidentUpdate.InsertedOn = DateTime.Now;
                    incidentUpdate.InsertedBy = SessionManager.UserName;
                    incidentUpdate.InsertedByID = SessionManager.UserID;

                    context.IncidentUpdate.Add(incidentUpdate);
                    context.SaveChanges();
                }

                processResponse.success = true;
                return Utility.ServiceResponse(ref processResponse);
            }catch(Exception ex){
                processResponse.success = false;
                return Utility.ServiceResponse(ref processResponse, ex);
            }
        }

        [OperationContract]
        [Security("Admin,SecurityIncidentsFinalise")]
        public EntityTransport<ProcessResponse> MarkIncidentForClosure(long incidentID, string resolutionDescription) {

            Utility.WCFCheckSecurityAttribute();

            var processResponse = new ProcessResponse();

            try {
                using(var context = new Honeycomb_Entities()){
                    var incidentDB = context.Incident.Where(i => i.ID == incidentID).FirstOrDefault();
                    incidentDB.IncidentStatusID = 10001;
                    incidentDB.ResolutionDate = DateTime.Now;
                    incidentDB.ResolutionDescription = resolutionDescription;

					//now check if there is a supplier access record associated with this incident
					var supplierAccess = context.SupplierAccess.Where(sa => sa.IncidentID == incidentID && sa.ExitedOn == null).FirstOrDefault();
					//if there is a record set the required exit fields
					if(supplierAccess != null) {
						supplierAccess.ExitedOn = DateTime.Now;
						supplierAccess.ExitedGuardID = SessionManager.UserID;
						supplierAccess.ExitedGuardName = SessionManager.FirstName + " " + SessionManager.LastName;
						supplierAccess.ExitedGateID = supplierAccess.EnteredGateID;
					}

                    context.SaveChanges();
                }

                processResponse.success = true;
                return Utility.ServiceResponse(ref processResponse);
            }catch(Exception ex){
                processResponse.success = false;
                return Utility.ServiceResponse(ref processResponse, ex);
            }
        }

        [OperationContract]
        [Security("Admin,SecurityIncidentsClose")]
        public EntityTransport<ProcessResponse> CloseIncident(long incidentID) {

            Utility.WCFCheckSecurityAttribute();

            var processResponse = new ProcessResponse();

            try {
                using(var context = new Honeycomb_Entities()) {
                    var incidentDB = context.Incident.Where(i => i.ID == incidentID).FirstOrDefault();
                    incidentDB.IncidentStatusID = 10002;

					//now check if there is a supplier access record associated with this incident
					var supplierAccess = context.SupplierAccess.Where(sa => sa.IncidentID == incidentID && sa.ExitedOn == null).FirstOrDefault();
					//if there is a record set the required exit fields
					if(supplierAccess != null) {
						supplierAccess.ExitedOn = DateTime.Now;
						supplierAccess.ExitedGuardID = SessionManager.UserID;
						supplierAccess.ExitedGuardName = SessionManager.FirstName + " " + SessionManager.LastName;
						supplierAccess.ExitedGateID = supplierAccess.EnteredGateID;
					}

                    context.SaveChanges();
                }

                processResponse.success = true;
                return Utility.ServiceResponse(ref processResponse);
            } catch(Exception ex) {
                processResponse.success = false;
                return Utility.ServiceResponse(ref processResponse, ex);
            }
        }

        [OperationContract]
        [Security("Admin,SecurityIncidentsClose")]
        public EntityTransport<ProcessResponse> OpenIncident(long incidentID) {

            Utility.WCFCheckSecurityAttribute();

            var processResponse = new ProcessResponse();

            try {
                using(var context = new Honeycomb_Entities()) {
                    var incidentDB = context.Incident.Where(i => i.ID == incidentID).FirstOrDefault();
                    incidentDB.IncidentStatusID = 10000;
                    context.SaveChanges();
                }

                processResponse.success = true;
                return Utility.ServiceResponse(ref processResponse);
            } catch(Exception ex) {
                processResponse.success = false;
                return Utility.ServiceResponse(ref processResponse, ex);
            }
        }

        #endregion

        #region Tag Holder Access Group
        [OperationContract]
        [Security("Admin,AccessGroupView,AccessGroupEdit")]
        public EntityTransport<TAGHOLDER_ACCESS_GROUP> GetTagAccessGroups() {
            var accessGroups = new List<TAGHOLDER_ACCESS_GROUP>();

            Utility.WCFCheckSecurityAttribute();

            try {

                accessGroups = ImproSecurity.GetAccessGroups();

                return Utility.ServiceResponse(ref accessGroups);
            }catch(Exception ex){
                return Utility.ServiceResponse(ref accessGroups, ex);
            }
        }

        [OperationContract]
        [Security("Admin,AccessGroupEdit")]
        public EntityTransport<string> SetTagHolderAccessGroup(string idNo, int tagGroupNo, DateTime? startDate, DateTime? endDate) {
            string message = "";

            Utility.WCFCheckSecurityAttribute();

            try {
                ImproSecurity securityHelper = new ImproSecurity();
                message = securityHelper.SetMasterTagAccessGroup(idNo, tagGroupNo, startDate, endDate);
				//now update the honeycomb tag holder cache
				UpdateTagHolderCache(new TagHolderCache() { TagHolderID = idNo, StartDate = startDate, ExpiryDate = endDate, AccessGroupID = tagGroupNo });

                return Utility.ServiceResponse(ref message);
            }catch(Exception ex){
                message = "There was an error when updating the access group, please try again.";
                return Utility.ServiceResponse(ref message, ex);
            }
        }

		/// <summary>
		/// Will get the stored tag holder access according to the honeycomb system.
		/// </summary>
		/// <param name="tagHolderID"></param>
		/// <returns></returns>
		[OperationContract]
		[Security("Admin,AccessGroupEdit")]
		public EntityTransport<TagHolderCache> GetTagHolderAccess(string tagHolderID) {
			Utility.WCFCheckSecurityAttribute();

			TagHolderCache tagCache = null;

			try {
				tagCache = GetTagHolderCache(tagHolderID);

				return Utility.ServiceResponse(ref tagCache);
			}catch(Exception ex){
				return Utility.ServiceResponse(ref tagCache, ex);
			}
		}

		/// <summary>
		/// Stores tag holder access details that were last specified by the honeycomb system.
		/// </summary>
		/// <param name="tagCache">An entity of type TagholderCache</param>
		private void UpdateTagHolderCache(TagHolderCache tagCache) {

			using(var context = new Honeycomb_Entities()){

				if(GetTagHolderCache(tagCache.TagHolderID) == null) {
					//add new
					context.TagHolderCache.Add(tagCache);
					context.SaveChanges();
				} else {
					//update
					TagHolderCache tagCacheDB = context.TagHolderCache.Where(th => th.TagHolderID == tagCache.TagHolderID).FirstOrDefault();

					if(tagCacheDB != null) {
						tagCacheDB.AccessGroupID = tagCache.AccessGroupID;
						tagCacheDB.StartDate = tagCache.StartDate;
						tagCacheDB.ExpiryDate = tagCache.ExpiryDate;

						context.SaveChanges();
					}
				}
			}

		}

		private TagHolderCache GetTagHolderCache(string tagHolderID) {
			TagHolderCache tagCache = null;

			using(var context = new Honeycomb_Entities()){
				tagCache = context.TagHolderCache.Where(th => th.TagHolderID == tagHolderID).FirstOrDefault();
			}

			return tagCache;
		}

        #endregion

		#region Supplier Access
		/// <summary>
		/// Will save a supplier access entry into the database with the correct required fields
		/// </summary>
		/// <param name="supplierAccessEntry"></param>
		/// <returns></returns>
		[OperationContract]
		[Security("Admin,SupplierEntryExit")]
		public EntityTransport<ProcessResponse> CaptureSupplierEntry(SupplierAccess supplierAccessEntry, long guardID) {
			Utility.WCFCheckSecurityAttribute();
			Honeycomb.Security.WCFSecurity.CheckGuardHasGate(guardID);

			ProcessResponse pRes = new ProcessResponse();

			try {
				using(var context = new Honeycomb_Entities()){

					User gateGaurd = context.User.Where(u => u.ID == guardID).FirstOrDefault();

					if(gateGaurd != null) {
						//validation
						if(supplierAccessEntry == null) {
							pRes.success = false;
							pRes.message = "You cannot supply a blank supplier access entry";
							return Utility.ServiceResponse(ref pRes);
						} else {
							//validation of required input
							if(string.IsNullOrEmpty(supplierAccessEntry.CompanyName) || string.IsNullOrEmpty(supplierAccessEntry.DeliveryNoteReference) || string.IsNullOrEmpty(supplierAccessEntry.VehicleRegistration) || string.IsNullOrEmpty(supplierAccessEntry.DriverIDNumber) || supplierAccessEntry.VehicleOccupantsIn == null) {
								pRes.success = false;
								pRes.message = "You have supplied an invalid supplier access entry. Required fields are missing.";
								return Utility.ServiceResponse(ref pRes);
							}

							//validate the gate that the gaurd is assigned to
							if(!gateGaurd.GateID.HasValue) {
								pRes.success = false;
								pRes.message = "You have not been assigned to a gate. Please contact the control room to have yourself assigned to a gate.";
								return Utility.ServiceResponse(ref pRes);
							}
						}
					} else {
						pRes.success = false;
						pRes.message = "A valid gaurd entry could not be located, please log out and log in again.";
						return Utility.ServiceResponse(ref pRes);
					}

					//we have passed validation we can now process the entry
					//one last check to see whether there is an existing record which has not been completed
					var supplierAccessCheck = context.SupplierAccess.Where(sa => sa.VehicleRegistration.Trim() == supplierAccessEntry.VehicleRegistration.Trim() && sa.ExitedOn == null).FirstOrDefault();

					if(supplierAccessCheck == null) {

						supplierAccessEntry.SupplierAccessID = 0;
						supplierAccessEntry.EnteredGateID = gateGaurd.GateID.Value;
						supplierAccessEntry.EnteredGuardID = gateGaurd.ID;
						supplierAccessEntry.EnteredGuardName = gateGaurd.FirstName + " " + gateGaurd.LastName;
						supplierAccessEntry.EnteredOn = DateTime.Now;
						supplierAccessEntry.IsLocked = false;
						supplierAccessEntry.VehicleRegistration = supplierAccessEntry.VehicleRegistration.Trim();

						context.SupplierAccess.Add(supplierAccessEntry);
						context.SaveChanges();

						pRes.success = true;
					} else {
						pRes.success = false;
						pRes.message = "There is an existing record for this contractor. Our records indicate this vechicle's exit was not completed correctly. Contact the control room before allowing this vehicle onto the premises.";
					}
				}
				return Utility.ServiceResponse(ref pRes);
			}catch(Exception ex){
				pRes.success = false;
				pRes.message = "There was an error when attempting to capture an entry for the supplier.";
				return Utility.ServiceResponse(ref pRes, ex);
			}

		}

		/// <summary>
		/// This function retrieves the current access record for the vechicle on the estate. It will return messages back to the user regarding anything the should/would prevent the vehicle from leaving. This function will be used in conjunction with the actual exit processing function to achieve the desired outcome.
		/// </summary>
		/// <param name="vehicleRegistrationNumber">The vehicles registration number</param>
		/// <returns></returns>
		[OperationContract]
		[Security("Admin,SupplierEntryExit")]
		public EntityTransport<SupplierAccessExit> GetSupplierAccessForExit(string vehicleRegistrationNumber, long guardID) {
			Utility.WCFCheckSecurityAttribute();
			Honeycomb.Security.WCFSecurity.CheckGuardHasGate(guardID);

			SupplierAccessExit retSupplierAccessExit = new SupplierAccessExit();

			try {
				using(var context = new Honeycomb_Entities()){
					//retrieve the supplier access record
					var supplierAccessDB = context.SupplierAccess.Include("SupplierLockedExitAttempts").Where(sa => sa.VehicleRegistration == vehicleRegistrationNumber.Trim() && sa.EnteredOn != null && sa.ExitedOn == null).FirstOrDefault();
					User gateGaurd = context.User.Where(u => u.ID == guardID).FirstOrDefault();

					if(supplierAccessDB != null) {

						//check if the supplier has already attempted to leave and has moved to another gate to try and exit
						if(supplierAccessDB.IsLocked) {
							retSupplierAccessExit.Success = false;
							retSupplierAccessExit.Message = "This supplier has been prevented from leaving the premises. Please notify security and ensure the supplier does not exit. Your name will be recorded against this action, for reference.";

							//add a exit attempt and record the guards info
							supplierAccessDB.SupplierLockedExitAttempts.Add(new SupplierLockedExitAttempts {
								AttemptDate = DateTime.Now,
								GuardID = gateGaurd.ID,
								GuardName = gateGaurd.FirstName + " " + gateGaurd.LastName,
								SupplierAccessID = retSupplierAccessExit.SupplierAccessID,
								GateID = gateGaurd.GateID.Value
							});

							context.SaveChanges();

							return Utility.ServiceResponse(ref retSupplierAccessExit);
						}

						//does the vehicle have to leave by the same gate it entered on
						if(!CacheManager.SystemSetting.IsSupplierGateRestricted || (CacheManager.SystemSetting.IsSupplierGateRestricted && (supplierAccessDB.EnteredGateID == gateGaurd.GateID))) {
							//no more checks to do and the supplier is ready to be processed for exit, populate the return object with the values needed to continue
							retSupplierAccessExit.Success = true;
							retSupplierAccessExit.IsSupplierGateRestricted = CacheManager.SystemSetting.IsSupplierGateRestricted;
							retSupplierAccessExit.SupplierAccessID = supplierAccessDB.SupplierAccessID;
							retSupplierAccessExit.VehicleRegistratioNumber = supplierAccessDB.VehicleRegistration;

						} else {
							//gate does not match, get the gate they entered at and instruct the guard to send them to that gate
							var gateDB = context.Gate.Where(g => g.ID == supplierAccessDB.EnteredGateID).FirstOrDefault();

							retSupplierAccessExit.Success = false;
							retSupplierAccessExit.Message = "This vehicle cannot exit the premises at this gate.";

							if(gateDB != null) {
								retSupplierAccessExit.Message += "Please direct the vehicle to return to " + gateDB.Name + " in order to exit.";
							} else {
								retSupplierAccessExit.Message += "Stop the vehicle from leaving. The recorded gate entry appears to be incorrect please contact the control room.";
							}
						}

					} else {
						retSupplierAccessExit.Success = false;
						retSupplierAccessExit.Message = "Warning! This vehicle must not leave premises. Either the vehilce registration number you entered is incorrect or the Access Control system has no record of this vehilce entering the premises.";
					}
				}

				return Utility.ServiceResponse(ref retSupplierAccessExit);
			}catch(Exception ex){
				retSupplierAccessExit.Success = false;
				retSupplierAccessExit.Message = "There was an unexpected error when attempting to retrieve details regarding this vehicle.";
				return Utility.ServiceResponse(ref retSupplierAccessExit, ex);
			}
		}

		/// <summary>
		/// This will perform the actual capture of the supplier exit, if it returns a success of false in the process reponse object then the ui will indicate that the vechicle cannot leave the estate
		/// </summary>
		/// <param name="supplierAccessID">The id of the supplier access record</param>
		/// <param name="numberOfOccupants">The number of occupants in the vehicle as it is leaving</param>
		/// <returns></returns>
		[OperationContract]
		[Security("Admin,SupplierEntryExit")]
		public EntityTransport<ProcessResponse> CaptureSupplierExit(long supplierAccessID, int numberOfOccupants, long guardID) {
			Utility.WCFCheckSecurityAttribute();
			Honeycomb.Security.WCFSecurity.CheckGuardHasGate(guardID);

			ProcessResponse pRes = new ProcessResponse();

			try { 
				using(var context = new Honeycomb_Entities()){
					var supplierAccessDB = context.SupplierAccess.Include("SupplierLockedExitAttempts").Where(sa => sa.SupplierAccessID == supplierAccessID && sa.IsLocked == false).FirstOrDefault();
					User gateGaurd = context.User.Include("Gate").Where(u => u.ID == guardID).FirstOrDefault();

					if(supplierAccessDB != null) {
						//check if the system is gate restriced, if not continue, if so check that the gate is the same otherwhise process the else.
						if(!CacheManager.SystemSetting.IsSupplierGateRestricted || (CacheManager.SystemSetting.IsSupplierGateRestricted && (supplierAccessDB.EnteredGateID == gateGaurd.GateID))) {

							supplierAccessDB.VehicleOccupantsOut = numberOfOccupants;

							if(supplierAccessDB.VehicleOccupantsOut != supplierAccessDB.VehicleOccupantsIn) {

								supplierAccessDB.IsLocked = true;

								pRes.success = false;
								pRes.message = "Warning! This vehicle must not leave the premises. There are contractors still on the estate! Contact the Control Room for assistance.";

								//add an exit attempt and record the guards info
								supplierAccessDB.SupplierLockedExitAttempts.Add(new SupplierLockedExitAttempts {
									AttemptDate = DateTime.Now,
									GuardID = gateGaurd.ID,
									GuardName = gateGaurd.FirstName + " " + gateGaurd.LastName,
									SupplierAccessID = supplierAccessDB.SupplierAccessID,
									GateID = gateGaurd.GateID.Value
								});

								var systemSettings = context.SystemSetting.FirstOrDefault();

								//insert a new incident for this discrepency
								Incident contractorDiscrepencyIncident = new Incident();
								//set to open status and to the correct type
								contractorDiscrepencyIncident.ID = 0;
								contractorDiscrepencyIncident.IncidentStatusID = 10000;
								contractorDiscrepencyIncident.IncidentTypeID = 10023;
								contractorDiscrepencyIncident.IncidentSourceID = 10014;
								contractorDiscrepencyIncident.IncidentPriorityID = 10009;
								contractorDiscrepencyIncident.IncidentDate = DateTime.Now;
								contractorDiscrepencyIncident.IncidentTime = DateTime.Now.ToString("HH:mm");
								contractorDiscrepencyIncident.Title = supplierAccessDB.CompanyName + " Exit Discrepency";
								contractorDiscrepencyIncident.Description = supplierAccessDB.CompanyName + " with vehicle registration " + supplierAccessDB.VehicleRegistration + " and driver id of " + supplierAccessDB.DriverIDNumber + " had a discrepency between the number of occupants in (" + supplierAccessDB.VehicleOccupantsIn.ToString() + ") and occupants out (" + supplierAccessDB.VehicleOccupantsOut.ToString() + ").";
								contractorDiscrepencyIncident.AssignedUserID = systemSettings.ContractorAuthorityID;

								var incidentAddResponse = AddIncident(contractorDiscrepencyIncident, null);

								supplierAccessDB.IncidentID = contractorDiscrepencyIncident.ID;

								//send a notification email to the control room
								Communications com = new Communications();
								com.SendEmail((long)Enumerators.NotificationType.ControlRoom, "URGENT: Supplier vehicle blocked from exiting", Utility.GetMessageContent(Enumerators.MessageType.EmailContractorDiscrepency).ReplaceObjectTokens(new {
									SupplierName = supplierAccessDB.CompanyName,
									GateName = gateGaurd.Gate.Name,
									GuardName = gateGaurd.FirstName + " " + gateGaurd.LastName,
									VehicleRegistration = supplierAccessDB.VehicleRegistration
								}));


							} else {
								supplierAccessDB.ExitedOn = DateTime.Now;
								supplierAccessDB.ExitedGateID = gateGaurd.GateID;
								supplierAccessDB.ExitedGuardID = gateGaurd.ID;
								supplierAccessDB.ExitedGuardName = gateGaurd.FirstName + " " + gateGaurd.LastName;

								pRes.success = true;
								pRes.message = "The contractors's exit has been processed successfully. You may allow the vehicle to exit the premises.";
							}
						} else {
							//gate does not match, get the gate they entered at and instruct the guard to send them to that gate
							var gateDB = context.Gate.Where(g => g.ID == supplierAccessDB.EnteredGateID).FirstOrDefault();

							pRes.success = false;
							pRes.message = "This vehicle cannot exit the premises at this gate. ";

							if(gateDB != null) {
								pRes.message += "Please direct the vehicle to return to " + gateDB.Name + " in order to exit.";
							} else {
								pRes.message += "Stop the vehicle from leaving. The recorded gate entry appears to be incorrect please contact the control room.";
							}
						}

						context.SaveChanges();

					} else {
						pRes.success = false;
						pRes.message = "This contractor cannot be processed, do not allow the vehicle to exit. This may be due to a pending locked exit. Contact the control room.";
					}
				}

				return Utility.ServiceResponse(ref pRes);
			}catch(Exception ex){
				pRes.success = false;
				pRes.message = "There was an error when attempting to capture the exit for this vehicle. Please try again.";
				return Utility.ServiceResponse(ref pRes, ex);
			}
		}
		#endregion
	}
}
