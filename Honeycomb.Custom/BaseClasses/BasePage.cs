﻿#region Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

#endregion

namespace Honeycomb.Custom.BaseClasses {
    public class BasePage : System.Web.UI.Page {
        #region Properties
        public List<long> PageRoles = new List<long>();
        public List<long> AllowedRoles {
            get;
            set;
        }
        #endregion

        protected override void OnPreLoad(EventArgs e) {
            base.OnPreLoad(e);
        }

        protected override void OnLoad(EventArgs e) {
            if (SessionManager.SystemID == Guid.Empty) {
                Session.Clear();
                Session.Abandon();
                FormsAuthentication.SignOut();
                Response.Redirect("~/Public/Login.aspx");
            }
            if (!Request.IsAuthenticated) {
                FormsAuthentication.RedirectToLoginPage();
            }
            //if (SessionManager.UserRoles != null) {
            //    //check user access permission for this page.
            //    AllowedRoles = SessionManager.UserRoles.Intersect(PageRoles).ToList();
            //    //Check user permissions for controls on page.
            //    foreach (Control item in Page.Controls) {
            //        if (item is HtmlControl) {
            //            var x = ((HtmlControl)item).Attributes["Roles"];
            //        } else if (item is WebControl) {
            //            var y = ((WebControl)item).Attributes["Roles"];
            //        }
            //    }   
            //}
            base.OnLoad(e);
        }
        protected override void OnLoadComplete(EventArgs e) {
          
            base.OnLoadComplete(e);
        }


    }
}
