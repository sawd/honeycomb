﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Honeycomb.Custom {
    public class SessionManager : SAWD.WebManagers.SessionManagerBase {

        
        /// <summary>
        /// The number of failed login attempts 
        /// </summary>        
        public static int LoginAttemptCounter {
            get { return (int)(GetSession("LoginAttemptCounter", 0)); }
            set { SetSession("LoginAttemptCounter", value); }
        }
                

        /// <summary>
        /// Check to if the user is logged in
        /// </summary>        
        public static bool isLoggedIn {
            get { return (bool)(GetSession("isLogedIn",false)); }
            set { SetSession("isLogedIn", value); }
        }

        /// <summary>
        /// Check to if the user is logged in
        /// </summary>        
        public static string sessionID {
            get { return (string)(GetSession("sessionID", false)); }
            set { SetSession("sessionID", value); }
        }

        /// <summary>
        /// The username of the current logged in user
        /// </summary>        
        public static string UserName {
            get { return (string)(GetSession("UserName")); }
            set { SetSession("UserName", value); }
        }

        /// <summary>
        /// The username of the current logged in user
        /// </summary>        
        public static string UserEmail {
            get { return (string)(GetSession("UserEmail")); }
            set { SetSession("UserEmail", value); }
        }

        /// <summary>
        /// The ID of the current logged in user
        /// </summary>        
        public static long UserID {
            get { return (long)(GetSession("UserID")); }
            set { SetSession("UserID", value); }
        }
                
        /// <summary>
        /// The FirstName of the current logged in user
        /// </summary>        
        public static string FirstName {
            get { return (string)(GetSession("FirstName")); }
            set { SetSession("FirstName", value); }
        }

        /// <summary>
        /// The LastName of the current logged in user
        /// </summary>        
        public static string LastName {
            get { return (string)(GetSession("LastName")); }
            set { SetSession("LastName", value); }
        }
                
        /// <summary>
        /// The roles of the current logged in user
        /// </summary>        
        public static List<string> UserRoles {
            get { return (List<string>)(GetSession("UserRoles")); }
            set { SetSession("UserRoles", value); }
        }

        /// <summary>
        /// The ID of the gate the current user is assigned to
        /// </summary>        
        public static long UserGateID {
            get { return (long)(GetSession("UserGateID")); }
            set { SetSession("UserGateID", value); }
        }

        /// <summary>
        /// Sets the current horizontal menu id for the vertical menu to generate from
        /// </summary>
        public static long HorizontalPageID {
            get { return Utility.ConvertNull(GetSession("HorizontalPageID"),0); }
            set { SetSession("HorizontalPageID", value); }
        }

        public static Guid SystemID {
            get { return Utility.ConvertNull(GetSession("SystemID"), Guid.Empty); }
            set { SetSession("SystemID", value); }
        }

		public static string LoginMessage {
			get { return Utility.ConvertNull(GetSession("LoginMessage"), String.Empty); }
			set { SetSession("LoginMessage", value); }
		}
    }
}
