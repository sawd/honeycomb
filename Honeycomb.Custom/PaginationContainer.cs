﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Honeycomb.Custom {
    public sealed class PaginationContainer {
        public int CurrentPage { get; set; }
        public string OrderBy { get; set; }
        public int PageSize { get; set; }
    }
}
