﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Honeycomb.Custom.Data
{

    public class SMSSendReturn
    {
        public bool Sent { get; set; }
        public Guid? CommEngineID { get; set; }
        public long EventID { get; set; }
        public string Message { get; set; }
    }


    public class CustomPropertyOwner
    {
        public long PropertyID { get; set; }
        public long OwnerID { get; set; }
        public string Email { get; set; }
        public string PrincipalOwnerName { get; set; }
        public string ContactNumber { get; set; }
        public string ERFNumber { get; set; }
        public string StreetNumber { get; set; }
        public string StreetName { get; set; }
        public long? Phase { get; set; }
        public string Complex { get; set; }
        public string SectionNumber { get; set; }
        public string DoorNumber { get; set; }
        public string PropertyName { get; set; }
        public long? SchemeID { get; set; }
    }

    public class UserAndRoles
    {
        public Custom.Data.User User { get; set; }
        public List<Custom.Data.Role> UserRoles { get; set; }
        public List<Custom.Data.Role> Roles { get; set; }
    }

    public class VisitorAccessCodes
    {
        public string FullName { get; set; }
        public string Cellphone { get; set; }
        public string AccessCode { get; set; }
        public string Message { get; set; }
    }

    public class VisitorAndAcccessCode
    {
        public Guid AccessCodeID { get; set; }
        public string AccessCode { get; set; }
        public string OccupantName { get; set; }
        public string OccupantCell { get; set; }
        public string OccupantAddress { get; set; }
        public string OccupantPhone { get; set; }
        public string VisitorName { get; set; }
        public bool? Used { get; set; }
        public int? VechicleOccupantIn { get; set; }
        public string VechicleRegIn { get; set; }
		public bool CaptureExpiredReason { get; set; }
		public int CodeValidilityPeriod { get; set; }
    }

    public class AutoCompleteObject
    {
        public string value { get; set; }
        public string label { get; set; }
        public string type { get; set; }
    }
    public class DropDownOption
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public string otherValue { get; set; }
    }

    public class ErvenAndTypes
    {
        public string Name { get; set; }
        public string ErfNumber { get; set; }
        public string ErfType { get; set; }
        public long ID { get; set; }
    }

    public class ErfDDO
    {
        public long ID { get; set; }
        public long ErfTypeID { get; set; }
        public string Name { get; set; }
        public string ErfNumber { get; set; }
        public string Description { get; set; }
        public bool HasProperties { get; set; }
    }

    public class OwnershipHistory
    {
        public string PrincipalOwnerName { get; set; }
        public string GovID { get; set; }
        public string OwnerType { get; set; }
        public string Cell { get; set; }
        public DateTime DateOfTransfer { get; set; }
        public string Email { get; set; }
        public Guid? SaleAgreementFile { get; set; }
    }

    public class PropertyIncidentHistory
    {
        public DateTime IncidentDate { get; set; }
        public string IncidentType { get; set; }
        public string IncidentTime { get; set; }
        public bool Closed { get; set; }
        public string Status { get; set; }
        public long ID { get; set; }
    }

    public class IncidentDetail
    {
        public long ID { get; set; }
        public DateTime IncidentDate { get; set; }
        public string IncidentType { get; set; }
        public string IncidentTime { get; set; }
        public bool Closed { get; set; }
        public string AssignedTo { get; set; }
        public string Source { get; set; }
        public string Priority { get; set; }
        public Guid? FileID { get; set; }
        public List<string> PartiesInvolved { get; set; }
        public string Status { get; set; }
        public string Description { get; set; }
        public string ResolutionDescription { get; set; }
        public DateTime? ResolutionDate { get; set; }
        public string ErfNumber { get; set; }
        public string OwnerName { get; set; }
    }
    public class CustomLookUpOptions
    {

        public string lookUpTypeName { get; set; }
        public string lookUpName { get; set; }
        public string lookUpDescription { get; set; }
        public long? lookUpID { get; set; }
        public long? lookUpTypeID { get; set; }
    }
    public class CustomLookUp
    {
        public string lookUpName { get; set; }
        public long lookUpID { get; set; }
    }

    public class ProcessResponse
    {
        public bool success { get; set; }
        public string message { get; set; }
    }

	public class IncidentProcessResponse {
		public bool success { get; set; }
		public string message { get; set; }
		public long IncidentID { get; set; }
	}

    public class CustomContractor
    {
        public long ContractorID { get; set; }
        public string TradingName { get; set; }
        public string Contact { get; set; }
        public string Email { get; set; }
        public string MobileNo { get; set; }
    }

    public class CustomGateUser
    {
        public long UserID { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
        public string Gate { get; set; }
        public long? GateID { get; set; }
    }
    public class CustomBulkSMSEntity
    {
        public string FromNumber { get; set; }
        public string toNumber { get; set; }
        public string Message { get; set; }
        public string Name { get; set; }
    }

    public class CustomContractorDetail {
        public Contractor Contractor { get; set; }
        public List<ContactPerson> ContactPerson { get; set; }
        public List<long> SubCategory { get; set; }
    }
    public class PropertySaleStatus
    {
        public string erfName { get; set; }
        public string address { get; set; }
        public string OwnerName { get; set; }

    }

    public class CustomOccupant {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool CanSMS { get; set; }
        public string IDNo { get; set; }
        public string Address { get; set; }
        public string Cellphone { get; set; }
        public long ID { get; set; }
		public string Email { get; set; }
		public bool IsOwner { get; set; }
		public bool IsPrincipleOccupant { get; set; }
		public bool IsTenant { get; set; }
		public bool HasAccessCard { get; set; }
		public bool CanReceiveCommunication { get; set; }
		public bool RegisteredOnClubMaster { get; set; }
		public DateTime? DateRegisteredOnClubMaster { get; set; }
		public bool IsGolfMember { get; set; }
		public bool IsSocialMember { get; set; }
    }

    public class CustomCalendarEvent {
        public long ID { get; set; }
        public long VenueID { get; set; }
        public long InsertedByID { get; set; }
        public long? OccupantID { get; set; }
		public decimal TotalBookingCost { get; set; }
        public string title { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Requirements { get; set; }
        public string InsertedBy { get; set; }
		public bool allDay { get; set; }
        public DateTime start { get; set; }
        public DateTime end { get; set; }
        public DateTime InsertedOn { get; set; }
        public string className { get; set; }
        public bool editable { get; set; }
		public long VenueBookingStatusID { get; set; }
    }

    public class CustomIntanetBooking {
        public long ID { get; set; }
        public long VenueID { get; set; }
        public long InsertedByID { get; set; }
        public long? OccupantID { get; set; }
		public decimal TotalBookingCost { get; set; }
        public string VenueName { get; set; }
        public string title { get; set; }
        public DateTime start { get; set; }
        public DateTime end { get; set; }
        public DateTime InsertedOn { get; set; }
		public long VenueBookingStatusID { get; set; }
    }

    public class CustomTime {
		public DateTime DateObj { get; set; }
        public string Time { get; set; }
        public string DisplayTime { get; set; }
        public string hrs { get; set; }
    }

    public class CustomImproTag {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int GroupID { get; set; }
    }

    public class CustomBulkSmsEntityComparer : IEqualityComparer<CustomBulkSMSEntity> {

        public bool Equals(CustomBulkSMSEntity x, CustomBulkSMSEntity y) {
            if(Object.ReferenceEquals(x, y)) {
                return true;
            }

            if(Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null)) {
                return false;
            }

            return x.FromNumber == y.FromNumber && x.Name == y.Name && x.toNumber == y.toNumber;
        }

        public int GetHashCode(CustomBulkSMSEntity cbse) {

            if(Object.ReferenceEquals(cbse, null)) {
                return 0;
            }

            //get hash code for name
            int hashCBSEName = cbse.Name == null ? 0 : cbse.Name.GetHashCode();

            //get from number hash code
            int hashCBSEFromNumber = cbse.FromNumber == null ? 0 : cbse.FromNumber.GetHashCode();

            //get to number hash code
            int hashCBSEToNumber = cbse.toNumber == null ? 0 : cbse.toNumber.GetHashCode();

            return hashCBSEName ^ hashCBSEFromNumber ^ hashCBSEToNumber;
        }
    }

	/// <summary>
	/// This custom entity is used as a container for the notifications code that notifies Occupants, Owners and Agents of lease expiry
	/// </summary>
	public class PropertyLeaseEntity {
		public Property Property { get; set; }
		public Ownership Owner { get; set; }
		public Occupant Occupant { get; set; }
		public Ownership Scheme { get; set; }
		public Agent Agent { get; set; }
		public DateTime? LeaseExpiryDate { get; set; }
	}

	/// <summary>
	/// A custom object used as a container for supplier access entry settings and values
	/// </summary>
	public class SupplierAccessExit {
		public long SupplierAccessID { get; set; }
		public long EnteredGateID { get; set; }
		public int OccupantIn { get; set; }
		public bool IsSupplierGateRestricted { get; set; }
		public DateTime? EnteredOn { get; set; }
		public string VehicleRegistratioNumber { get; set; }
		public string CompanyName { get; set; }
		public bool Success { get; set; }
		public string Message { get; set; }
	}

// *** EDITS ***

    public class FileDetail
    {
        public Guid FileID { get; set; }
        public string Type { get; set; }
        public string Label { get; set; }
        public string FileName { get; set; }
        public DateTime InsertedOn { get; set; }
        
    }

// *** EDITS - END ***

}
