﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Honeycomb.Custom.Data;
using System.Data;
using System.Xml.Linq;
using System.Xml;
using System.ServiceModel;

namespace Honeycomb.Custom
{
    public class Communications
    {
        private string userName = ConfigurationManager.AppSettings["PortalUserName"];
        private string password = ConfigurationManager.AppSettings["PortalPassword"];
        #region Methods
        /// <summary>
        /// Sends an SMS to a mobile number
        /// </summary>
        /// <param name="message">mobile message</param>
        /// <param name="mobileNumber">recipient cell number</param>
        /// <returns></returns>
        public SMSSendReturn SendSMS(string message, string mobileNumber)
        {
            SMSSendReturn smsReturn = new SMSSendReturn();
            DataSet dsSend = new DataSet();
            using (Honeycomb.Custom.MobileAPI.APISoapClient mobileAPI = new Honeycomb.Custom.MobileAPI.APISoapClient("APISoap"))
            {
                using (var context = new Custom.Data.Honeycomb_Entities())
                {

                    if (true)
                    {

                        try
                        {

                            mobileAPI.Open();
                            #region XML node declaration
                            XmlDocument messageEnvelope = new XmlDocument();
                            XmlElement root = messageEnvelope.CreateElement("senddata");
                            XmlElement isLive = messageEnvelope.CreateElement("live");
                            XmlElement settings = messageEnvelope.CreateElement("settings");
                            XmlElement entries = messageEnvelope.CreateElement("entries");
                            XmlElement defaultDate = messageEnvelope.CreateElement("default_date");
                            XmlElement defaultTime = messageEnvelope.CreateElement("default_time");
                            XmlElement numTo = messageEnvelope.CreateElement("numto");
                            XmlElement customerID = messageEnvelope.CreateElement("customerid");
                            XmlElement messageData = messageEnvelope.CreateElement("data1");
                            #endregion

                            #region Populate Nodes
                            isLive.InnerText = ConfigurationManager.AppSettings["SendSMS"];
                            defaultDate.InnerText = DateTime.Now.ToString("dd/MMM/yyyy");
                            defaultTime.InnerText = DateTime.Now.ToString("HH:mm");
                            numTo.InnerText = mobileNumber;
                            customerID.InnerText = CacheManager.SystemID.ToString();
                            messageData.InnerText = message;
                            #endregion

                            #region Append Nodes
                            settings.AppendChild(isLive);
                            settings.AppendChild(defaultDate);
                            settings.AppendChild(defaultTime);

                            entries.AppendChild(numTo);
                            entries.AppendChild(customerID);
                            entries.AppendChild(messageData);

                            root.AppendChild(settings);
                            root.AppendChild(entries);

                            messageEnvelope.AppendChild(root);
                            #endregion

                            dsSend = mobileAPI.Send_STR_DS(userName, password, messageEnvelope.OuterXml);

                            if (dsSend.Tables.Contains("call_result"))
                            {
                                if (dsSend.Tables["call_result"].Rows[0].ItemArray[0].ToString().ToLower() == "true")
                                {
                                    //the result is true therefore the sms was sent
                                    smsReturn.Sent = true;

                                    if (dsSend.Tables.Contains("send_info"))
                                    {
                                        smsReturn.EventID = Int32.Parse((dsSend.Tables["send_info"].Rows[0].ItemArray[0]).ToString());
                                    }

                                }
                                else
                                {
                                    smsReturn.Sent = false;
                                    smsReturn.Message = dsSend.Tables["call_result"].Rows[0].ItemArray[1].ToString();
                                    if (smsReturn.Message.Contains("No data to send"))
                                    {
                                        smsReturn.Message = "Cellphone number not valid.";
                                    }
                                }
                            }

                            mobileAPI.Close();

                        }
                        catch (TimeoutException ex)
                        {
                            //return that the method failed and send the sms to the comm engine for sending
                            string XMLData = "<xmldata><users><user mobile='" + mobileNumber + "' /></users></xmldata>";
                            smsReturn.CommEngineID = context.CreateCommEngineSMS(message, "0825058592", XMLData, CacheManager.CommEngineID, null).FirstOrDefault();
                            smsReturn.Sent = false;
                            smsReturn.Message = "A timeout occured when attempting to send the sms. The message was queued for sending.";
                            //abort the api connection
                            mobileAPI.Abort();
                        }
                        catch (CommunicationException ex)
                        {
                            //return that the method failed and send the sms to the comm engine for sending
                            string XMLData = "<xmldata><users><user mobile='" + mobileNumber + "' /></users></xmldata>";
                            smsReturn.CommEngineID = context.CreateCommEngineSMS(message, "0825058592", XMLData, CacheManager.CommEngineID, null).FirstOrDefault();
                            smsReturn.Sent = false;
                            smsReturn.Message = "A connection error occurred while attempting to send the sms. The message was queued for sending.";
                            //abort the api connection
                            mobileAPI.Abort();
                        }

                        //call sent sms table
                        //long lastChangeID = Utility.ConvertNull<long>(context.SMSSent.Select(sent => sent.ChangeID).Max(), 0);

                        //GetAllSentSMS(lastChangeID);
                    }

                }
            }
            return smsReturn;
        }
        /// <summary>
        /// Sends an SMS to a users belonging to a particular notificationTypeID
        /// </summary>
        /// <param name="message">mobile message</param>
        /// <param name="notificationTypeID">ID  representing the group of users</param>
        /// <returns></returns>
        public SMSSendReturn SendSMS(string message, long? notificationTypeID)
        {


            SMSSendReturn smsReturn = new SMSSendReturn();
            DataSet dsSend = new DataSet();
            using (Honeycomb.Custom.MobileAPI.APISoapClient mobileAPI = new Honeycomb.Custom.MobileAPI.APISoapClient("APISoap"))
            {
                using (var context = new Custom.Data.Honeycomb_Entities())
                {

                    var NotificationCelllList = from person in context.NotificationPerson
                                                where person.NotificationTypeID == notificationTypeID
                                                select new { person.Cellphone };
                    if (true)
                    {

                        try
                        {

                            mobileAPI.Open();
                            #region XML node declaration
                            XmlDocument messageEnvelope = new XmlDocument();
                            XmlElement root = messageEnvelope.CreateElement("senddata");
                            XmlElement isLive = messageEnvelope.CreateElement("live");
                            XmlElement settings = messageEnvelope.CreateElement("settings");

                            XmlElement defaultDate = messageEnvelope.CreateElement("default_date");
                            XmlElement defaultTime = messageEnvelope.CreateElement("default_time");

                            #endregion

                            #region Populate Nodes
                            isLive.InnerText = ConfigurationManager.AppSettings["SendSMS"];
                            defaultDate.InnerText = DateTime.Now.ToString("dd/MMM/yyyy");
                            defaultTime.InnerText = DateTime.Now.ToString("HH:mm");
                            XmlElement entries;
                            XmlElement numTo, customerID, messageData;

                            foreach (var mobileNumber in NotificationCelllList)
                            {
                                if (mobileNumber.Cellphone != "" || mobileNumber.Cellphone != string.Empty)
                                {
                                    entries = messageEnvelope.CreateElement("entries");
                                    numTo = messageEnvelope.CreateElement("numto");
                                    customerID = messageEnvelope.CreateElement("customerid");
                                    messageData = messageEnvelope.CreateElement("data1");
                                    numTo.InnerText = mobileNumber.Cellphone;
                                    customerID.InnerText = CacheManager.SystemID.ToString();
                                    messageData.InnerText = message;

                                    entries.AppendChild(numTo);
                                    entries.AppendChild(customerID);
                                    entries.AppendChild(messageData);
                                    root.AppendChild(entries);

                                    messageEnvelope.AppendChild(root);
                                }

                            }
                            #endregion

                            #region Append Nodes
                            settings.AppendChild(isLive);
                            settings.AppendChild(defaultDate);
                            settings.AppendChild(defaultTime);



                            root.AppendChild(settings);


                            messageEnvelope.AppendChild(root);
                            #endregion

                            dsSend = mobileAPI.Send_STR_DS(userName, password, messageEnvelope.OuterXml);

                            if (dsSend.Tables.Contains("call_result"))
                            {
                                if (dsSend.Tables["call_result"].Rows[0].ItemArray[0].ToString().ToLower() == "true")
                                {
                                    //the result is true therefore the sms was sent
                                    smsReturn.Sent = true;

                                    if (dsSend.Tables.Contains("send_info"))
                                    {
                                        smsReturn.EventID = Int32.Parse((dsSend.Tables["send_info"].Rows[0].ItemArray[0]).ToString());
                                    }

                                }
                                else
                                {
                                    smsReturn.Sent = false;
                                    smsReturn.Message = dsSend.Tables["call_result"].Rows[0].ItemArray[1].ToString();
                                    if (smsReturn.Message.Contains("No data to send"))
                                    {
                                        smsReturn.Message = "Cellphone number not valid.";
                                    }
                                }
                            }

                            mobileAPI.Close();

                        }
                        catch (TimeoutException ex)
                        {
                            //return that the method failed and send the sms to the comm engine for sending
                            StringBuilder XMLData = new StringBuilder();
                            XMLData.Append("<xmldata><users>");
                            foreach (var cellNumber in NotificationCelllList)
                            {
                                XMLData.Append("<user mobile='" + cellNumber.Cellphone + "' />");
                            }
                            XMLData.Append("</users></xmldata>");
                            smsReturn.CommEngineID = context.CreateCommEngineSMS(message, "0825058592", XMLData.ToString(), CacheManager.CommEngineID, null).FirstOrDefault();
                            smsReturn.Sent = false;
                            smsReturn.Message = "A timeout occured when attempting to send the sms. The message was queued for sending.";
                            //abort the api connection
                            mobileAPI.Abort();
                        }
                        catch (CommunicationException ex)
                        {
                            //return that the method failed and send the sms to the comm engine for sending
                            StringBuilder XMLData = new StringBuilder();
                            XMLData.Append("<xmldata><users>");

                            foreach (var cellNumber in NotificationCelllList)
                            {
                                XMLData.Append("<user mobile='" + cellNumber.Cellphone + "' />");
                            }
                            XMLData.Append("</users></xmldata>");
							smsReturn.CommEngineID = context.CreateCommEngineSMS(message, "0825058592", XMLData.ToString(), CacheManager.CommEngineID, null).FirstOrDefault();
                            smsReturn.Sent = false;
                            smsReturn.Message = "A connection error occured while attempting to send the sms. The message was queued for sending.";
                            //abort the api connection
                            mobileAPI.Abort();
                        }

                        //call sent sms table
                        //long lastChangeID = Utility.ConvertNull<long>(context.SMSSent.Select(sent => sent.ChangeID).Max(), 0);

                        //GetAllSentSMS(lastChangeID);
                    }

                }
            }
            return smsReturn;
        }
        /// <summary>
        ///  Sends an SMSes using the details in the CustomBulkSMSEntity xml 
        /// </summary>
        /// <param name="customBulkWntity"></param>
        /// <returns></returns>
        public SMSSendReturn SendSMS(List<CustomBulkSMSEntity> customBulkEntity)
        {
            Guid commEngineID = Guid.Parse(ConfigurationManager.AppSettings["CommEngineID"].ToString());
            SMSSendReturn smsReturn = new SMSSendReturn();
            DataSet dsSend = new DataSet();
            string Textmesage = string.Empty;
            XmlDocument xdoc = new XmlDocument();
            using (var context = new Custom.Data.Honeycomb_Entities())
            {

                if (true)
                {

                    try
                    {
                        #region XML node declaration
                        XmlDocument messageEnvelope = new XmlDocument();
                        XmlElement users = messageEnvelope.CreateElement("users");

                        XmlElement XMLDATA = messageEnvelope.CreateElement("xmldata");

                        #endregion

                        #region Populate Nodes

                        foreach (var item in customBulkEntity)
                        {
                            XmlElement user = messageEnvelope.CreateElement("user");
                            XmlElement fromNumber = messageEnvelope.CreateElement("FromNumber");
                            XmlElement message = messageEnvelope.CreateElement("Message");
                            XmlAttribute toNumber = messageEnvelope.CreateAttribute("toNumber");
                            XmlAttribute name = messageEnvelope.CreateAttribute("name");

                            fromNumber.InnerText = item.FromNumber;
                            message.InnerText = item.Message;
                            Textmesage = item.Message;
                        #endregion
                            #region Append Nodes
                            user.AppendChild(fromNumber);
                            user.AppendChild(message);
                            user.SetAttribute("mobile", item.toNumber);
                            user.SetAttribute("name", item.Name);
                            users.AppendChild(user);
                            #endregion
                        }

                        XMLDATA.AppendChild(users);
                        messageEnvelope.AppendChild(XMLDATA);


                        XmlNodeList userNodeList = messageEnvelope.SelectNodes("//user");

                        smsReturn.CommEngineID = context.CreateCommEngineSMS(Textmesage, "", messageEnvelope.OuterXml, commEngineID, null).FirstOrDefault();




                    }
                    catch (Exception ex)
                    {
                        SendEmail("errors@sawebdesign.co.za", "error sending bulk sms", ex.StackTrace);
                    }



                }
            }
            return smsReturn;


        }
        /// <summary>
        ///  Gets a report of all messages sent based on a sentID, use 0 to get all records or specify an ID 
        ///  to seed from
        /// </summary>
        /// <param name="ChangeID"></param>
        /// <returns></returns>
        public DataSet GetAllSentSMS(long? ChangeID = null)
        {
			if(ChangeID == null) {
				using(var context = new Honeycomb_Entities()) {

					ChangeID = Utility.ConvertNull<long>(context.SMSSent.Select(sent => sent.ChangeID).Max(), 0);
				}
			}

            using (Honeycomb.Custom.MobileAPI.APISoapClient mobileAPI = new Honeycomb.Custom.MobileAPI.APISoapClient("APISoap"))
            {
                mobileAPI.Open();
                #region XML node declaration
                XmlDocument messageEnvelope = new XmlDocument();
                XmlElement root = messageEnvelope.CreateElement("sent");
                XmlElement settings = messageEnvelope.CreateElement("settings");
                XmlElement id = messageEnvelope.CreateElement("id");
                XmlElement maxItems = messageEnvelope.CreateElement("max_recs");
                XmlElement columns = messageEnvelope.CreateElement("cols_returned");
                XmlElement dateFormat = messageEnvelope.CreateElement("date_format");
                #endregion

                #region Populate Nodes
                id.InnerText = ChangeID.ToString();
                columns.InnerText = "sentid,eventid,smstype,numto,data,flash,customerid,status,statusdate";
                dateFormat.InnerText = "dd-MMM-yyyy HH:ss";
                maxItems.InnerText = "500";
                #endregion

                settings.AppendChild(id);
                settings.AppendChild(columns);
                settings.AppendChild(dateFormat);
                //settings.AppendChild(maxItems);

                root.AppendChild(settings);
                messageEnvelope.AppendChild(root);

                DataSet dsResult = mobileAPI.Sent_STR_DS(userName, password, messageEnvelope.OuterXml);
                mobileAPI.Close();

                using (var context = new Custom.Data.Honeycomb_Entities())
                {
                    //write a record to honeycomb
                    foreach (DataRow record in dsResult.Tables[0].Rows)
                    {
						if(record["customerid"].ToString() == CacheManager.SystemID.ToString())
                        {
							var currentSentID = long.Parse(record["sentid"].ToString());

							if(context.SMSSent.Where(sms => sms.SentID == currentSentID).FirstOrDefault() == null) {
								context.SMSSent.Add(new Custom.Data.SMSSent {
									ChangeID = long.Parse(record["changeid"].ToString()),
									CustomerID = CacheManager.SystemID,
									Data = Utility.ConvertNull(record["data"], string.Empty),
									EventID = long.Parse(record["eventid"].ToString()),
									Flash = Utility.ConvertNull(record["flash"], false),
									NumberTo = long.Parse(record["numto"].ToString()),
									SentID = long.Parse(record["sentid"].ToString()),
									SMSType = Utility.ConvertNull(record["smstype"], string.Empty),
									Status = Utility.ConvertNull(record["status"], string.Empty),
									StatusDate = Utility.ConvertNull(record["statusdate"], DateTime.Now),
									SystemID = Utility.ConvertNull(Guid.Parse(record["customerID"].ToString()), Guid.Empty)
								});
							}
                        }

                    }
                    context.SaveChanges();
                }
                return dsResult;

            }
        }

        /// <summary>
        /// Gets a all replies from recipients based on a replyID, use 0 to get all records or specify an ID 
        /// to seed from
        /// </summary>
        /// <param name="replyID"></param>
        /// <returns></returns>
        public DataSet GetAllReplySMS(int replyID)
        {
            using (Honeycomb.Custom.MobileAPI.APISoapClient mobileAPI = new Honeycomb.Custom.MobileAPI.APISoapClient("APISoap"))
            {
                mobileAPI.Open();
                #region XML node declaration
                XmlDocument messageEnvelope = new XmlDocument();
                XmlElement root = messageEnvelope.CreateElement("reply");
                XmlElement settings = messageEnvelope.CreateElement("settings");
                XmlElement id = messageEnvelope.CreateElement("id");
                XmlElement maxItems = messageEnvelope.CreateElement("max_recs");
                XmlElement columns = messageEnvelope.CreateElement("cols_returned");
                XmlElement dateFormat = messageEnvelope.CreateElement("date_format");
                #endregion

                #region Populate Nodes
                id.InnerText = replyID.ToString();
                columns.InnerText = "eventid,numfrom,receiveddata,received,sentid,sentdata,sentdatetime,sentcustomerid";
                dateFormat.InnerText = "dd-MMM-yyyy HH:ss";
                maxItems.InnerText = string.Empty;
                #endregion

                settings.AppendChild(id);
                settings.AppendChild(columns);
                settings.AppendChild(dateFormat);
                //settings.AppendChild(maxItems);

                root.AppendChild(settings);
                messageEnvelope.AppendChild(root);

                DataSet dsResult = mobileAPI.Reply_STR_DS(userName, password, messageEnvelope.OuterXml);
                mobileAPI.Close();

                return dsResult;
            }
        }
        /// <summary>
        /// Sends an email from the systems settings from email address
        /// </summary>
        /// <param name="toEmail">Email Recipient</param>
        /// <param name="subject">Subject of the email</param>
        /// <param name="message">Email message</param>
        public void SendEmail(string toEmail, string subject, string message)
        {
            Guid commEngineID = Guid.Parse(ConfigurationManager.AppSettings["CommEngineID"].ToString());
            using (var context = new Custom.Data.Honeycomb_Entities())
            {
                var systemID = new Guid();

                systemID = Guid.Parse(ConfigurationManager.AppSettings["SystemID"].ToString());

                var fromEmailAddress = context.SystemSetting.Where(ss => ss.SystemID == systemID).Select(fmail => fmail.FromEmailAddress).FirstOrDefault();

                string XMLData = "<xmldata><users><user email='" + toEmail + "'></user></users></xmldata>";
				context.CreateCommEngineEmail(message, XMLData, commEngineID, fromEmailAddress, subject);
            }
        }
        public void SendEmail(string toEmail, string subject, string message,string attachment)
        {
            Guid commEngineID = Guid.Parse(ConfigurationManager.AppSettings["CommEngineID"].ToString());
            using (var context = new Custom.Data.Honeycomb_Entities())
            {
                var systemID = new Guid();

                systemID = Guid.Parse(ConfigurationManager.AppSettings["SystemID"].ToString());

                var fromEmailAddress = context.SystemSetting.Where(ss => ss.SystemID == systemID).Select(fmail => fmail.FromEmailAddress).FirstOrDefault();

                {

                    string XMLData = "<xmldata><users><user email='" + toEmail + "' subject='" + subject + "'></user></users></xmldata>";
                    context.CreateCommEngineEmailAtt(message, XMLData, commEngineID, fromEmailAddress,attachment);
                }
            }
        
        }
        /// <summary>
        /// Sends an email from the systems settings from email address to users who belong to a particular notificationTypeID
        /// </summary>
        /// <param name="notificationTypeID">ID  representing the group of users</param>
        /// <param name="subject">Subject of the email</param>
        /// <param name="message">Email message</param>
        public void SendEmail(long? notificationTypeID, string subject, string message)
        {
            Guid commEngineID = Guid.Parse(ConfigurationManager.AppSettings["CommEngineID"].ToString());
            using (var context = new Custom.Data.Honeycomb_Entities())
            {
                var systemID = new Guid();

                systemID = Guid.Parse(ConfigurationManager.AppSettings["SystemID"].ToString());

                var fromEmailAddress = context.SystemSetting.Where(ss => ss.SystemID == systemID).Select(fmail => fmail.FromEmailAddress).FirstOrDefault();

                {
                    var NotificationEmailList = from person in context.NotificationPerson
                                                where person.NotificationTypeID == notificationTypeID
                                                select new { person.Email };

                    StringBuilder xmlData = new StringBuilder();
                    xmlData.Append("<xmldata><users>");
                    foreach (var emailaddress in NotificationEmailList)
                    {
                        xmlData.Append("<user email ='" + emailaddress.Email + "'/>");
                    }
                    xmlData.Append("</users></xmldata>");
                    context.CreateCommEngineEmail(message, xmlData.ToString(), commEngineID, fromEmailAddress, subject);

                }
            }

        }
    
        }
        #endregion

    }
