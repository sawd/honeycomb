﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Reporting.WebForms;
using Honeycomb.Custom.Data;
using System.IO;
//using WebSupergoo.ABCpdf8;
//using WebSupergoo.ABCpdf8.Objects;
//using WebSupergoo.ABCpdf8.Atoms;
//using WebSupergoo.ABCpdf8.Operations;

namespace Honeycomb.Custom {
    /// <summary>
    /// Handles all automation methods to be called by console apps
    /// </summary>
    public class AutomatedNotifications {

		/// <summary>
		/// Will get a list of property lease entity objects which will expire in the provided number of days.
		/// </summary>
		/// <param name="expiresIn">An integer indicating in how many days the method must check ahead for expiring leases.</param>
		/// <returns></returns>
		public static List<PropertyLeaseEntity> GetPropertyLeaseExpiries(int expiresIn) {
			List<PropertyLeaseEntity> propertyLeaseEntityList = new List<PropertyLeaseEntity>();
			DateTime compareExpiryDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).AddDays(expiresIn);

			using(var context = new Honeycomb_Entities()){

				//get all leases with no extentions, include property details
				var propertyLeaseList = context.Property.Where(p => p.Lease.EndDate == compareExpiryDate &&
																					(p.Lease.Terminated == null || p.Lease.Terminated == false) &&
																					(p.LeaseExtension.Count() == 0 || p.LeaseExtension.All(le => le.DeletedOn != null)) &&
																					p.Ownership.Any(o => o.ArchivedOn == null) &&
																					p.DeletedOn == null)
																					.Select(s => new PropertyLeaseEntity {
																							Occupant = s.Lease.Occupant ?? null,
																							Agent = s.Lease.Agent,
																							LeaseExpiryDate = s.Lease.EndDate,
																							Property = s,
																							Scheme = s.SchemeID != null ? context.Ownership.Where(ow => ow.PropertyID == s.SchemeID).FirstOrDefault() : null,
																							Owner = s.Ownership.Where(o => o.ArchivedOn == null).FirstOrDefault()}).ToList();
				//get all the properties with lease extentions
				var propertyLeaseExtentionList = context.Property.Where(p => p.LeaseExtension.Max(lem => lem.EndDate) == compareExpiryDate &&
																			(p.Lease.Terminated == null || p.Lease.Terminated == false) &&
																			p.LeaseExtension.Any(le => le.DeletedOn == null) &&
																			p.Ownership.Any(o => o.ArchivedOn == null) &&
																			p.DeletedOn == null)
																			.Select(s => new PropertyLeaseEntity {
																				Occupant = s.Lease.Occupant ?? null,
																				Agent = s.Lease.Agent,
																				LeaseExpiryDate = s.LeaseExtension.Count() > 0 ? s.LeaseExtension.Max(lem => lem.EndDate) : null,
																				Property = s,
																				Scheme = s.SchemeID != null ? context.Ownership.Where(ow => ow.PropertyID == s.SchemeID).FirstOrDefault() : null,
																				Owner = s.Ownership.Where(o => o.ArchivedOn == null).FirstOrDefault()
																			}).ToList();
				
				propertyLeaseEntityList = propertyLeaseList.Union(propertyLeaseExtentionList).ToList();

				var propertyIDList = propertyLeaseEntityList.Select(s => s.Property.ID).ToList();

				//now get all the principal occupants for the properties with expired leases
				var principalOccupants = context.Occupant.Where(o => o.PropertyOccupant.Any(po => propertyIDList.Contains(po.PropertyID) && po.ArchivedOn == null) &&
																		o.IsPrincipalOccupant == true &&
																		o.IsTenant == true).Select(s => new {
																			Occupant = s,
																			PropertyID = s.PropertyOccupant.FirstOrDefault() != null ? s.PropertyOccupant.FirstOrDefault().PropertyID : 0
																		}).ToList();

				//get the occupant for each property
				foreach(var propertyLeaseEntity in propertyLeaseEntityList) {

					if(propertyLeaseEntity.Occupant == null) {
						//get the principle occupant
						var principleOccupant = principalOccupants.Where(o => o.PropertyID == propertyLeaseEntity.Property.ID).Select(s => s.Occupant).FirstOrDefault();

						if(principleOccupant != null) {
							propertyLeaseEntity.Occupant = principleOccupant;
						}
					}

				}
			}

			return propertyLeaseEntityList;
		}

		//handles sending of lease expiry reminder emails to tenants
		public static void SendOccupantLeaseExpiryReminder(ref List<PropertyLeaseEntity> propertyLeaseEntityList) {

			Communications com = new Communications();
			string emailMessage = "";
			//send off emails to each principal occupant in the collection
			foreach(var propertyLeaseEntity in propertyLeaseEntityList) {

				try {

					if(propertyLeaseEntity.Occupant != null && propertyLeaseEntity.Property != null && propertyLeaseEntity.Owner != null && propertyLeaseEntity.LeaseExpiryDate != null) {
						//build message and send
						emailMessage = Utility.GetMessageContent(Enumerators.MessageType.EmailTenantLeaseExpiryReminder).ReplaceObjectTokens(new {
							occupantFirstName = propertyLeaseEntity.Occupant.FirstName,
							ownerName = propertyLeaseEntity.Owner.PrincipalOwnerName,
							PropertyAddress = propertyLeaseEntity.Scheme == null ? propertyLeaseEntity.Property.StreetNumber + " " + propertyLeaseEntity.Property.StreetName : propertyLeaseEntity.Property.SectionNumber + " " + propertyLeaseEntity.Scheme.PropertyName + " " + propertyLeaseEntity.Property.StreetNumber + " " + propertyLeaseEntity.Property.StreetName,
							leaseExpiryDate = propertyLeaseEntity.LeaseExpiryDate.Value.ToString("yyyy-MM-dd")
						});

						if(!string.IsNullOrEmpty(propertyLeaseEntity.Occupant.Email)) {
							com.SendEmail(propertyLeaseEntity.Occupant.Email, "Lease expiry notification", emailMessage);
						}
					}
				}catch(Exception ex){
					Utility.SendExceptionMail(ex,"Failed to send reminder email to Occupant.");
				}
			}
		}

		/// <summary>
		/// Sends owner lease reminder emails to the provided list
		/// </summary>
		/// <param name="propertyLeaseEntityList"></param>
		public static void SendOwnerLeaseExpiryReminder(ref List<PropertyLeaseEntity> propertyLeaseEntityList) {
			Communications com = new Communications();
			string emailMessage = "";
			//send off emails to each owner
			foreach(var propertyLeaseEntity in propertyLeaseEntityList) {

				try {

					if(propertyLeaseEntity.Occupant != null && propertyLeaseEntity.Property != null && propertyLeaseEntity.Owner != null && propertyLeaseEntity.LeaseExpiryDate != null) {
						//build message and send
						emailMessage = Utility.GetMessageContent(Enumerators.MessageType.EmailOwnerLeaseExpiryReminder).ReplaceObjectTokens(new {
							occupantFirstName = propertyLeaseEntity.Occupant.FirstName,
							occupantLastName = propertyLeaseEntity.Occupant.LastName,
							ownerName = propertyLeaseEntity.Owner.PrincipalOwnerName,
							PropertyAddress = propertyLeaseEntity.Scheme == null ? propertyLeaseEntity.Property.StreetNumber + " " + propertyLeaseEntity.Property.StreetName : propertyLeaseEntity.Property.SectionNumber + " " + propertyLeaseEntity.Scheme.PropertyName + " " + propertyLeaseEntity.Property.StreetNumber + " " + propertyLeaseEntity.Property.StreetName,
							leaseExpiryDate = propertyLeaseEntity.LeaseExpiryDate.Value.ToString("yyyy-MM-dd")
						});

						if(!string.IsNullOrEmpty(propertyLeaseEntity.Owner.Email)) {
							com.SendEmail(propertyLeaseEntity.Owner.Email, "Lease expiry notification", emailMessage);
						}
					}
				}catch(Exception ex){
					Utility.SendExceptionMail(ex, "Failed to send lease expiry reminder to Owner.");
				}
			}
		}

		/// <summary>
		/// Sends agent lease reminder emails to the provided list
		/// </summary>
		/// <param name="propertyLeaseEntityList"></param>
		public static void SendAgentLeaseExpiryReminder(ref List<PropertyLeaseEntity> propertyLeaseEntityList, string numDays = "30") {
			Communications com = new Communications();
			string emailMessage = "";
			//send off emails to each owner
			foreach(var propertyLeaseEntity in propertyLeaseEntityList) {

				try {

					if(propertyLeaseEntity.Occupant != null && propertyLeaseEntity.Property != null && propertyLeaseEntity.Owner != null && propertyLeaseEntity.LeaseExpiryDate != null && propertyLeaseEntity.Agent != null) {
						//build message and send
						emailMessage = Utility.GetMessageContent(Enumerators.MessageType.EmailAgentLeaseExpiryReminder).ReplaceObjectTokens(new {
							agentFirstName = propertyLeaseEntity.Agent.AgentFirstName,
							occupantFirstName = propertyLeaseEntity.Occupant.FirstName,
							occupantLastName = propertyLeaseEntity.Occupant.LastName,
							ownerName = propertyLeaseEntity.Owner.PrincipalOwnerName,
							PropertyAddress = propertyLeaseEntity.Scheme == null ? propertyLeaseEntity.Property.StreetNumber + " " + propertyLeaseEntity.Property.StreetName : propertyLeaseEntity.Property.SectionNumber + " " + propertyLeaseEntity.Scheme.PropertyName + " " + propertyLeaseEntity.Property.StreetNumber + " " + propertyLeaseEntity.Property.StreetName,
							leaseExpiryDate = propertyLeaseEntity.LeaseExpiryDate.Value.ToString("yyyy-MM-dd"),
							NumberOfDays = numDays
						});

						if(!string.IsNullOrEmpty(propertyLeaseEntity.Agent.Email)) {
							com.SendEmail(propertyLeaseEntity.Agent.Email, "Lease expiry notification", emailMessage);
						}
					}
				}catch(Exception ex){
					Utility.SendExceptionMail(ex, "Failed to send lease expiry reminder to agent.");
				}
			}
		}

		/// <summary>
		/// Sends out lease expiry reminders
		/// </summary>
		public static void SendLeaseExpiryReminders(){
			var propertyLeaseExpiries30Day = GetPropertyLeaseExpiries(30);
			var propertyLeaseExpiries15Day = GetPropertyLeaseExpiries(15);

			//send out 30 day reminders
			SendAgentLeaseExpiryReminder(ref propertyLeaseExpiries30Day);
			SendOccupantLeaseExpiryReminder(ref propertyLeaseExpiries30Day);
			SendOwnerLeaseExpiryReminder(ref propertyLeaseExpiries30Day);
			//send out 15 day reminders
			SendAgentLeaseExpiryReminder(ref propertyLeaseExpiries15Day,"15");
		}

        public static void SendInductionNotificationToTenant() {

            using(var context = new Custom.Data.Honeycomb_Entities()) {
                var lease = from leases in context.Lease
                            where leases.PropertyID != null
                            && leases.DateOfInduction == null
                            && leases.InductionBy == null
                            && leases.StartDate != null
                            join po in context.PropertyOccupant on leases.PropertyID equals po.PropertyID
                            select leases;

                //gets occupied properties
                var properties = from property in context.Property
                                 join propertyOccupants in context.PropertyOccupant on property.ID equals propertyOccupants.PropertyID
                                 select property;

                //Get tenants for the properties               
                var tenants = (from occ in context.Occupant
                               join le in context.PropertyOccupant on occ.ID equals le.OccupantID
                               join prop in context.Property on le.PropertyID equals prop.ID
                               join l in context.Lease on prop.ID equals l.PropertyID
                               join owners in context.Ownership on prop.ID equals owners.PropertyID
                               join extensons in context.LeaseExtension on prop.ID equals extensons.PropertyID into tempExtensions
                               from exten in tempExtensions.OrderByDescending(w => w.EndDate).Take(1).DefaultIfEmpty()
                               join agents in context.Agent on l.AgentID equals agents.AgentID into tempTable
                               from ag in tempTable.DefaultIfEmpty()
                               where le.ArchivedOn == null
                            && occ.IsPrincipalOccupant == true
                            && owners.ArchivedOn == null
                               && occ.IsTenant == true
                               select new { occupant = occ, lease = l, owners = owners, agent = ag, leaseExtension = exten, property = prop }).ToList();

                // Send Email to Tenant 
                Communications communication = new Communications();
                DateTime endDateCompare = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                //Induction Email Text to tenant
                #region Email Text
                StringBuilder expiryEmailToTenant = new StringBuilder();
                StringBuilder expiryEmailToOwner = new StringBuilder();
                StringBuilder expiryEmailToAgent = new StringBuilder();
                StringBuilder inductionEmailText = new StringBuilder();
                StringBuilder copyOfLeaseEmailTextOwner = new StringBuilder();
                StringBuilder copyOfLeaseEmailTextAdmin = new StringBuilder();


                string[] myMessage = { "occupantFirstName", "occupantLastName", "tenantFirstName", "tenantLastName", "PropertyAddress", "leaseExpiryDate", "ownerFirstName", "ownerCellNumber" };
                string[] placeHolders = { "{PropertyAddress}", "{leaseExpiryDate}", "{occupantFirstName}", "{agentContactNumber}", "{occupantCellNumber}", "{agentFirstName}", "{agentCellNumber}" };

                #endregion
                foreach(var detail in tenants) {

						if(detail.property != null && detail.lease != null && detail.occupant != null){

						List<string> fieldValues = new List<string>();

						fieldValues.Add(detail.property.StreetNumber + " " + detail.property.StreetName);
						fieldValues.Add(detail.lease.EndDate.ToString());
						fieldValues.Add(detail.occupant.FirstName + detail.occupant.LastName);

						if(detail.agent != null) {
							fieldValues.Add(Utility.ConvertNull(detail.agent.CelNumber, ""));
							fieldValues.Add(detail.agent.AgentFirstName + detail.agent.AgentLastName);
						} else {
							//insert blanks inder order to retain placeholder - value integrity
							fieldValues.Add("");
							fieldValues.Add("");
						}

						var ownerDetails = from ow in tenants.Select(w => w.owners)
										   where ow.ID == detail.owners.ID
										   select new { ownerFirstName = ow.PrincipalOwnerName, ownerLastName = ow.PropertyID, ownerCellNumber = ow.Cell };

						var	agentDetails = from ag in tenants.Select(w => w.agent)
										   where ag.AgentID == detail.agent.AgentID
										   select new { agentFirstName = ag.AgentFirstName, agentLastName = ag.AgentLastName, agentCellNumber = Utility.ConvertNull(ag.CelNumber, ""), agentEmail = ag.Email };
						

						var propertyDeatails = from prop in tenants.Select(w => w.property)
											   where prop.ID == detail.property.ID
											   select new { doorNumber = prop.DoorNumber, streetName = prop.StreetNumber, streetNumber = prop.StreetNumber };

						var leaseDetails = from le in tenants.Select(w => w.lease)
										   where le.PropertyID == detail.property.ID
										   select new { doorNumber = le.EndDate, fileName = le.FileID, dateOfInduction = le.DateOfInduction };

						var occupantsDetails = from oc in tenants.Select(w => w.occupant)
											   where oc.ID == detail.occupant.ID
											   select new { occupantFirstName = oc.FirstName, occupantLastName = oc.LastName, occupantCellNumber = oc.Cellphone };
							
						
						if(detail.lease.DateOfInduction == null) {
							//communication.SendEmail(detail.occupant.Email, "Tenant induction", Utility.GetMessageContent(Enumerators.MessageType.EmailTenantInduction).ReplaceObjectTokens(occupantsDetails.Select(e => e).FirstOrDefault(), myMessage));
						}

						if(detail.leaseExtension != null) {
							fieldValues[1] = detail.lease.EndDate.ToString();
							if(detail.leaseExtension.EndDate == DateTime.Now.AddDays(30)) {
								//Email to tenant

								communication.SendEmail(detail.occupant.Email, "Lease Renewal reminder – Tenant", Utility.GetMessageContent(Enumerators.MessageType.EmailTenantLeaseExpiryReminder).ReplaceObjectTokens(ownerDetails.Select(e => e).FirstOrDefault(), placeHolders, fieldValues.ToArray(), myMessage));
								//Email to owner 

								communication.SendEmail(detail.agent.Email, "Lease Renewal reminder – Owner", Utility.GetMessageContent(Enumerators.MessageType.EmailOwnerLeaseExpiryReminder).ReplaceObjectTokens(ownerDetails.Select(e => e).FirstOrDefault(), placeHolders, fieldValues.ToArray(), myMessage));
								//Email to Agent
								if(detail.agent != null) {

									communication.SendEmail(detail.agent.Email, "Lease Renewal reminder – Agent", Utility.GetMessageContent(Enumerators.MessageType.EmailAgentLeaseExpiryReminder).ReplaceObjectTokens(agentDetails.Select(e => e).FirstOrDefault(), placeHolders, fieldValues.ToArray(), myMessage));
								}
							}

						} else {
							if(detail.lease.EndDate == endDateCompare.AddDays(30)) {
								fieldValues[1] = detail.lease.EndDate.ToString();
								//Email to tenant

							    communication.SendEmail(detail.occupant.Email, "Lease Renewal reminder – Tenant", Utility.GetMessageContent(Enumerators.MessageType.EmailTenantLeaseExpiryReminder).ReplaceObjectTokens(occupantsDetails.Select(e => e).FirstOrDefault(), placeHolders, fieldValues.ToArray(), myMessage));
								//Email to owner 

							    communication.SendEmail(detail.owners.Email, "Lease Renewal reminder – Owner", Utility.GetMessageContent(Enumerators.MessageType.EmailOwnerLeaseExpiryReminder).ReplaceObjectTokens(ownerDetails.Select(e => e).FirstOrDefault(), placeHolders, fieldValues.ToArray(), myMessage));
								//Email to Agent
								if(detail.agent != null) {
									communication.SendEmail(detail.agent.Email, "Lease Renewal reminder – Agent", Utility.GetMessageContent(Enumerators.MessageType.EmailAgentLeaseExpiryReminder).ReplaceObjectTokens(agentDetails.Select(e => e).FirstOrDefault(), placeHolders, fieldValues.ToArray(), myMessage));
								}
							}

						}
						if(detail.lease.FileID == null) {
							DateTime startDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);

							if(detail.lease.StartDate != null && detail.lease.EndDate != null) {

								if(detail.lease.NotificationLastSendOn == null) {
									startDate = (DateTime)detail.lease.StartDate;
								} else {
									startDate = (DateTime)detail.lease.NotificationLastSendOn;
								}

								DateTime sendDate = startDate.AddDays(30);
								DateTime CopyLeaseDateCompare = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
								if(sendDate == CopyLeaseDateCompare || startDate == CopyLeaseDateCompare) {
									detail.lease.NotificationLastSendOn = CopyLeaseDateCompare;
									context.SaveChanges();
									//send to reminder to owner
									communication.SendEmail(detail.owners.Email, "Copy of Lease reminder – Owner", Utility.GetMessageContent(Enumerators.MessageType.EmailCopyOfLeaseEmailOwner).ReplaceObjectTokens(ownerDetails.Select(e => e).FirstOrDefault(), placeHolders, fieldValues.ToArray(), myMessage));
									//send to admin notification list
									communication.SendEmail((long)Enumerators.NotificationType.Admin, "Copy of Lease reminder – Admin", Utility.GetMessageContent(Enumerators.MessageType.EmailCopyOfLeaseEmailAdmin).ReplaceObjectTokens(ownerDetails.Select(e => e).FirstOrDefault(), placeHolders, fieldValues.ToArray(), myMessage));
								}

							}
						}
					}
                }
            }

        }


        #region ErfAvailabilitySchedule


        //public static void generatePDF(int propertyStatusID, string path) {
        //    Communications comm = new Communications();
        //    string fileName = string.Empty;
        //    var salePropertyList = new List<PropertySaleStatus>();

        //    StringBuilder html = new StringBuilder();
        //    using(var context = new Honeycomb.Custom.Data.Honeycomb_Entities()) {

        //        salePropertyList = (from details in context.PropertyStatus
        //                            join own in context.Ownership on details.ID equals own.PropertyStatusID
        //                            join prop in context.Property on own.PropertyID equals prop.ID
        //                            join erf in context.Erf on prop.ERFID equals erf.ID
        //                            where own.PropertyStatusID == propertyStatusID
        //                            select new PropertySaleStatus {
        //                                erfName = erf.Name,
        //                                address = prop.StreetNumber + prop.StreetName,
        //                                OwnerName = own.PrincipalOwnerName
        //                            }).ToList();


        //        foreach(var propRecord in salePropertyList) {
        //            html.Append(string.Format("<tr><td>{0}</td><td>{1}</td><td>{2}</td></tr>", propRecord.erfName, propRecord.address, propRecord.OwnerName));

        //        }




        //        if(salePropertyList.Count() < 0) {
        //            if(propertyStatusID == 10000) {

        //                html.Append("<tr><td colspan='8'>No properties for first Sale</td></tr>");

        //            } else if(propertyStatusID == 10001) {

        //                html.Append("<tr><td colspan='8'>No properties for Sale</td></tr>");

        //            }
        //        } else {
        //            if(propertyStatusID == 10000) {
        //                fileName = "Properties for first sale";

        //            } else if(propertyStatusID == 10001) {
        //                fileName = "Properties for for sale";
        //            }
        //        }

        //        string Reporthtml = File.ReadAllText(path + @"\FirstSale.html");
        //        Reporthtml = Reporthtml.ToString().Replace("{content}", html.ToString());
        //        Doc doc = new Doc();
        //        // adjust the default rotation and save
        //        double w = doc.MediaBox.Width;
        //        double h = doc.MediaBox.Height;
        //        double l = doc.MediaBox.Left;
        //        double b = doc.MediaBox.Bottom;
        //        doc.Transform.Rotate(90, l, b);
        //        doc.Transform.Translate(w, 0);
        //        doc.Rect.Width = h;
        //        doc.Rect.Height = w;
        //        doc.Rect.Inset(50, 50);
        //        int pageID = doc.AddImageHtml(Reporthtml, true, 800, true);


        //        int theID = doc.GetInfoInt(doc.Root, "Pages");
        //        doc.SetInfo(theID, "/Rotate", "90");

        //        string pdfFile = path + @"\PDFReports" + fileName + ".pdf";
        //        doc.Save(pdfFile);

        //        {
        //            var approvedAgents = context.Agent.Where(d => d.DeletedOn == null).Select(s => s.Email).ToList();
        //            foreach(var agentEmailAddress in approvedAgents) {
        //                comm.SendEmail(agentEmailAddress.ToString(), fileName, "Find an attached list of  " + fileName, pdfFile);
        //            }

        //        }


        //    }

        //
        //}
        #endregion
    }
}