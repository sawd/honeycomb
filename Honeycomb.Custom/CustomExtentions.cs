﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Linq.Dynamic;
using System.Data;
using System.Dynamic;
using System.Data.Entity;

namespace Honeycomb.Custom
{


    public static class StringExtention
    {
        /// <summary>
        /// Replaces all occuraences of object properties that are encased in curly braces with that object property's value. EG: {FirstName}
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="str">The string that is invoking the method</param>
        /// <param name="SourceObject">The object/entity that contains the properties you would like to populate in the string.</param>
        /// <param name="fields">A string array of property names to populate in the string.</param>
        /// <returns>The modified string</returns>
        public static string ReplaceObjectTokens<T>(this string str, T SourceObject, string[] fields = null) {
            var propertyNameList = SourceObject.GetType().GetProperties().Select(s => s.Name);

            //change the list to include only the fields that are in the provided array of strings
            if (fields != null) {
                propertyNameList = propertyNameList.Where(p => fields.Contains(p));
            }

            //use string builder for performance
            StringBuilder sb = new StringBuilder(str);
            //loop through all the properties of the object and replace the tokens with values
            foreach (var prop in propertyNameList) {
                PropertyInfo sourceProp = SourceObject.GetType().GetProperty(prop);
                //if the value is null leave it alone
                if(sourceProp.GetValue(SourceObject) != null && str.Contains("{" + prop + "}")) {
                    sb.Replace("{" + prop + "}", sourceProp.GetValue(SourceObject).ToString());
                }
            }

            return sb.ToString();
        }

        public static string ReplaceObjectTokens<T>(this string str, T SourceObject, string[] placeHolder, string[] FieldValue, string[] fields = null)
        {
            var propertyNameList = SourceObject.GetType().GetProperties().Select(s => s.Name);
            string[] placeHolders = placeHolder;
            string[] fieldValues = FieldValue;

           

            //change the list to include only the fields that are in the provided array of strings
            if (fields != null)
            {
                propertyNameList = propertyNameList.Where(p => fields.Contains(p));
            }

            //use string builder for performance
            StringBuilder sb = new StringBuilder(str);
            //loop through all the properties of the object and replace the tokens with values
            bool hasParentesis = false;
            int i = 0;
            foreach (var prop in propertyNameList)
            {
           
                string temp = prop;
                //Checks if prop has  { }
                if (prop.Substring(0, 1) != "{")
                {
                    hasParentesis = false;
                    temp = "{" + prop + "}";

                }
                else
                {
                    hasParentesis = true;
                }
                PropertyInfo sourceProp = SourceObject.GetType().GetProperty(prop);
                //if the value is null leave it alone
                if (sourceProp.GetValue(SourceObject) != null && str.Contains(temp) || str.Contains(prop))
                {
                    if (hasParentesis == false)
                    {
                        sb.Replace(prop, sourceProp.GetValue(SourceObject).ToString());
                        for (int j = 0; j <= FieldValue.Count()-1; j++)
			{
                sb.Replace(placeHolders[j], fieldValues[j]);
			}
                    }
                    else
                    {
                        sb.Replace(temp, sourceProp.GetValue(SourceObject).ToString().Replace(placeHolder[i], FieldValue[i]));
                    }
                }
                i = i + 1;
            }

            return sb.ToString();
        }

    }
}