﻿#region Directives
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Configuration;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Web;
using System.Xml;
#endregion

namespace Honeycomb.Custom {
    class MessageInspector : IDispatchMessageInspector, IClientMessageInspector {

        #region IDispatchMessageInspector Members
        public object AfterReceiveRequest(ref Message request, IClientChannel channel, InstanceContext instanceContext) {
			MessageBuffer buffer = request.CreateBufferedCopy(Int32.MaxValue);
			request = buffer.CreateMessage();
			string msg = buffer.CreateMessage().ToString();

            return null;
        }

        public void BeforeSendReply(ref Message reply, object correlationState) {
        
        }
       
        #endregion


        #region IClientMessageInspector Members

        public void AfterReceiveReply(ref Message reply, object correlationState) {
            throw new NotImplementedException();
        }

        public object BeforeSendRequest(ref Message request, IClientChannel channel) {
            throw new NotImplementedException();
        } 
        #endregion
    }

	[AttributeUsage(AttributeTargets.Class)]
	public class ServiceOutputBehavior : Attribute, IServiceBehavior, IEndpointBehavior {
		public void AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase, Collection<ServiceEndpoint> endpoints, BindingParameterCollection bindingParameters) {
		}

		public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase) {
			for(int i = 0; i < serviceHostBase.ChannelDispatchers.Count; i++) {
				ChannelDispatcher channelDispatcher = serviceHostBase.ChannelDispatchers[i] as ChannelDispatcher;
				if(channelDispatcher != null) {
					foreach(EndpointDispatcher endpointDispatcher in channelDispatcher.Endpoints) {
						MessageInspector inspector = new MessageInspector();
						endpointDispatcher.DispatchRuntime.MessageInspectors.Add(inspector);
					}
				}
			}
		}

		public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase) {
		}

		public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters) { 

		}

		public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime) {
			clientRuntime.MessageInspectors.Add(new MessageInspector());
		}

		public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher) { 

		}

		public void Validate(ServiceEndpoint endpoint) { 

		}
	}
}

