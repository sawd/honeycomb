﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data.Objects.DataClasses;
using Honeycomb.Custom;
using Microsoft.Reporting.WebForms;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;

namespace Honeycomb.Custom.Model
{
    public class ReportsController
    {
        static string reportPath;
        

        public static void LoadReports(string reportName, bool isUsername, ReportViewer reportViewer, HttpContext pageContext, string path, string storedProc)
        {
            Communications comm = new Communications();
            try
            {
                try
                {

                    pageContext = HttpContext.Current;
                    reportViewer.Visible = true;
                    reportViewer.ShowPrintButton = false;
                    reportViewer.Reset();
                    Microsoft.Reporting.WebForms.LocalReport r = reportViewer.LocalReport;
                    r.ReportPath = path + string.Format(@"Reports\{0}.rdlc", reportName);


                    Database db = new SqlDatabase(ConfigurationManager.ConnectionStrings["RptConnectionString"].ConnectionString);
                    DbCommand command = db.GetStoredProcCommand(storedProc);
                    DataSet dataset = db.ExecuteDataSet(command);
                    ReportDataSource datasource = new ReportDataSource("DataSet1", dataset.Tables[0]);
                    reportViewer.LocalReport.DataSources.Clear();

                    List<Microsoft.Reporting.WebForms.ReportParameter> list = new List<Microsoft.Reporting.WebForms.ReportParameter>();
                    var paramr = r.GetParameters();
                    foreach (ReportParameterInfo p in r.GetParameters())
                    {

                        //Console.WriteLine(r.Values.ToString());
                        if (p.Name.ToLower() != "username" && p.Name.ToLower() != "password")
                        {
                            if (!string.IsNullOrEmpty(HttpContext.Current.Request.Form[p.Name]))
                            {
                                list.Add(new Microsoft.Reporting.WebForms.ReportParameter(p.Name, HttpContext.Current.Request.Form[p.Name], false));
                                list.Add(new Microsoft.Reporting.WebForms.ReportParameter(p.Name, pageContext.Request.Form[p.Name], false));
                            }
                            else
                            {
                                if (!p.Nullable && !p.AllowBlank)
                                {
                                    throw new Exception("Report (" + reportName + ") parameter " + p.Name + " cannot be null or empty");
                                }
                                string Ctlvalue = null;
                                if (!p.Nullable && p.AllowBlank)
                                {
                                    Ctlvalue = "";
                                }
                                list.Add(new Microsoft.Reporting.WebForms.ReportParameter(p.Name, Ctlvalue, false));
                            }
                        }
                        else
                        {
                            //check the report and make sure that it has a username and password input parameters for security
                            if (p.Name.ToLower() == "username")
                            {
                                isUsername = true;
                                list.Add(new Microsoft.Reporting.WebForms.ReportParameter(p.Name, SessionManager.UserName, false));
                            }
                        }
                        if (p.Name.ToLower() == "reportname")
                        {
                            string str = string.Join(" ", BreakPhraseIntoWords(reportName));


                            list.Add(new Microsoft.Reporting.WebForms.ReportParameter(p.Name, str, false));
                        }
                    }

                    if (isUsername)
                    {
                        r.SetParameters(list);
                    }
                    else
                    {
                        //lblError.Text = "Report (" + reportName + ") has missing parameter UserName or Password";
                    }
                    reportViewer.LocalReport.DataSources.Add(datasource);
                    reportViewer.LocalReport.Refresh();
                }
                catch (SqlException ex)
                {

                    comm.SendEmail("errors@sawebdesign.co.za", "HoneyComb Errors", ex.StackTrace);
                }
            }
            catch (Exception ex)
            {

                comm.SendEmail("errors@sawebdesign.co.za", "HoneyComb Errors", ex.StackTrace);
            }

        }
        /// <summary>
        /// Breaks a phrase into an array of words
        /// </summary>
        /// <param name="sInput"></param>
        /// <returns></returns>
        public static string[] BreakPhraseIntoWords(string sInput)
        {


            Communications comm = new Communications();


            StringBuilder[] sReturn = new StringBuilder[1];
            sReturn[0] = new StringBuilder(sInput.Length);
            const string CUPPER = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            int iArrayCount = 0;
            for (int iIndex = 0; iIndex < sInput.Length; iIndex++)
            {
                string sChar = sInput.Substring(iIndex, 1); // get a char
                if ((CUPPER.Contains(sChar)) && (iIndex > 0))
                {
                    iArrayCount++;
                    System.Text.StringBuilder[] sTemp = new System.Text.StringBuilder[iArrayCount + 1];
                    Array.Copy(sReturn, 0, sTemp, 0, iArrayCount);
                    sTemp[iArrayCount] = new StringBuilder(sInput.Length);
                    sReturn = sTemp;
                }
                sReturn[iArrayCount].Append(sChar);
            }
            string[] sReturnString = new string[iArrayCount + 1];
            for (int iIndex = 0; iIndex < sReturn.Length; iIndex++)
            {
                sReturnString[iIndex] = sReturn[iIndex].ToString();
            }
            return sReturnString;






        }

        /// <summary>
        /// Stores a report as a pdf
        /// </summary>
        /// <param name="reportName"></param>
        /// <param name="reportViewer"></param>
        /// <returns></returns>
        public static string ExportReport(string reportName,  ReportViewer reportViewer)
        {
            Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string filenameExtension;

            byte[] bytes = reportViewer.LocalReport.Render(
                "PDF", null, out mimeType, out encoding, out filenameExtension,
                 out streamids, out warnings);

            string filename = Path.Combine(Path.GetTempPath(), reportName + ".pdf");
            using (FileStream fs = new FileStream(filename, FileMode.Create))
            {
                fs.Write(bytes, 0, bytes.Length);
            }

            return filename;




        }
    }
}
