﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Honeycomb.Custom.Data;

namespace Honeycomb.Custom.Model {
    public class Document {

        public static Guid? SaveDocument(byte [] fileBytes, string fileName, string fileDescription, string uploadedBy) {
            var doc = new DocumentRepository();

            try {

                if(fileBytes != null) {

                    doc.Id = Guid.NewGuid();
                    doc.FileName = fileName.Split('.').First<string>();
                    doc.Extension = fileName.Split('.').Last<string>();
                    doc.FileData = fileBytes;
                    doc.FileSize = fileBytes.Length;
                    doc.DisplayName = fileDescription ?? doc.FileName;
                    doc.DateUploaded = DateTime.Now;
                    doc.UploadedBy = uploadedBy;

                    using(var context = new Honeycomb_Entities()) {
                        context.DocumentRepository.Add(doc);
                        context.SaveChanges();
                    }

                }
                return doc.Id;
            } catch(Exception ex) {
                return null;
            }
        }
    }
}
