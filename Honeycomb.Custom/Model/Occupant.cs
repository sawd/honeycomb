﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Honeycomb.Custom.Data;

namespace Honeycomb.Custom.Model {
    class Occupant {
        /// <summary>
        /// Util function to reset the previous principle occupant to false for a given property
        /// </summary>
        /// <param name="PropertyID">The current PropertyID</param>
        public void ResetPrincipleOccupant(long PropertyID, long IgnoreID) {
            using(var context = new Honeycomb_Entities()) {
                var PreviousPrincipleOccupant = (from o in context.Occupant
                                                 join po in context.PropertyOccupant on o.ID equals po.OccupantID
                                                 where po.PropertyID == PropertyID
                                                 && o.IsPrincipalOccupant == true
                                                 && po.ArchivedOn == null
                                                 && o.ID != IgnoreID
                                                 select o).FirstOrDefault();

                if(PreviousPrincipleOccupant != null) {
                    PreviousPrincipleOccupant.IsPrincipalOccupant = false;
                    context.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Checks if the requested username has been used before
        /// </summary>
        /// <param name="username">The username string</param>
        /// <param name="OccupantID">The id of the occupant</param>
        /// <returns>True if the username is available</returns>
        public bool OccupantUsernameAvailable(string username, long OccupantID) {
            var avail = true;

            try {

                using(var context = new Honeycomb_Entities()) {
                    var OccupantDBChk = (from o in context.Occupant
                                         join po in context.PropertyOccupant on o.ID equals po.OccupantID
                                         where o.IntranetUsername == username
                                         && po.ArchivedOn == null
                                         && o.DeletedOn == null
                                         select o).OptionalWhere(OccupantID != 0, ow => ow.ID != OccupantID).FirstOrDefault();

                    //if there are no records then that username does not exist
                    if(OccupantDBChk == null) {
                        avail = true;
                    } else {
                        avail = false;
                    }
                }

            } catch(Exception ex) {
                avail = false;
            }

            return avail;
        }
    }
}
