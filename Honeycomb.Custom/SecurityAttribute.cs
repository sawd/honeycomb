﻿using System;

/// <summary>
/// Class used for tagging properties in the WCF for security
/// </summary>
[AttributeUsage(AttributeTargets.Method)]
public class SecurityAttribute : System.Attribute {

    public string Roles;

    public SecurityAttribute(string roles) {
        this.Roles = roles;
    }

}