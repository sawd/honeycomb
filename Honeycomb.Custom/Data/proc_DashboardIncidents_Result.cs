//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Runtime.Serialization; 
namespace Honeycomb.Custom.Data
{
    public partial class proc_DashboardIncidents_Result
    {
    	[DataMember]
        public string Status { get; set; }
    	[DataMember]
        public string dayMonth { get; set; }
    	[DataMember]
        public Nullable<int> numberOfIncidents { get; set; }
    }
    
}
