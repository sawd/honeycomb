//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Honeycomb.Custom.Data
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    [DataContract]
    public partial class USER_FIELD_NAME
    {
        public USER_FIELD_NAME()
        {
            this.EMP_USER_FIELD = new HashSet<EMP_USER_FIELD>();
            this.VST_USER_FIELD = new HashSet<VST_USER_FIELD>();
        }
    
        [DataMember] public short UFN_No { get; set; }
        [DataMember] public string UFN_Name { get; set; }
        [DataMember] public short UFN_Type { get; set; }
        [DataMember] public short UFN_Required { get; set; }
        [DataMember] public string UFN_Desc { get; set; }
    
        [DataMember] public ICollection<EMP_USER_FIELD> EMP_USER_FIELD { get; set; }
        [DataMember] public ICollection<VST_USER_FIELD> VST_USER_FIELD { get; set; }
    }
}
