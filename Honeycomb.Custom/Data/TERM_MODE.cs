//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Honeycomb.Custom.Data
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    [DataContract]
    public partial class TERM_MODE
    {
        [DataMember] public short TM_No { get; set; }
        [DataMember] public short TP_No { get; set; }
        [DataMember] public string SITE_SLA { get; set; }
        [DataMember] public short MOD_No { get; set; }
        [DataMember] public string TERM_SLA { get; set; }
        [DataMember] public short TM_Report { get; set; }
        [DataMember] public short TM_Modified { get; set; }
    
        [DataMember] public MODE_TYPE MODE_TYPE { get; set; }
        [DataMember] public TERMINAL TERMINAL { get; set; }
        [DataMember] public TIME_PATTERN TIME_PATTERN { get; set; }
    }
}
