//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Runtime.Serialization; 
namespace Honeycomb.Custom.Data
{
    [DataContract]
    public partial class SMSSent
    {
    	[DataMember]
        public long SentID { get; set; }
    	[DataMember]
        public Nullable<long> EventID { get; set; }
    	[DataMember]
        public string SMSType { get; set; }
    	[DataMember]
        public Nullable<long> NumberTo { get; set; }
    	[DataMember]
        public string Data { get; set; }
    	[DataMember]
        public Nullable<bool> Flash { get; set; }
    	[DataMember]
        public Nullable<System.Guid> CustomerID { get; set; }
    	[DataMember]
        public string Status { get; set; }
    	[DataMember]
        public Nullable<System.DateTime> StatusDate { get; set; }
    	[DataMember]
        public Nullable<System.Guid> SystemID { get; set; }
    	[DataMember]
        public Nullable<long> ChangeID { get; set; }
    }
    
}
