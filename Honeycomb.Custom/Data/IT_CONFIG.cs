//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Honeycomb.Custom.Data
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    [DataContract]
    public partial class IT_CONFIG
    {
        [DataMember] public int ITC_NO { get; set; }
        [DataMember] public string ITC_PARAM_1 { get; set; }
        [DataMember] public string ITC_PARAM_2 { get; set; }
    }
}
