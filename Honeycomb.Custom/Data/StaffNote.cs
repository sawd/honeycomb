//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Runtime.Serialization; 
namespace Honeycomb.Custom.Data
{
    [DataContract]
    public partial class StaffNote
    {
    	[DataMember]
        public long ID { get; set; }
    	[DataMember]
        public long StaffID { get; set; }
    	[DataMember]
        public string Title { get; set; }
    	[DataMember]
        public string Note { get; set; }
    	[DataMember]
        public Nullable<System.DateTime> InsertedOn { get; set; }
    	[DataMember]
        public string InsertedBy { get; set; }
    	[DataMember]
        public Nullable<long> InsertedByID { get; set; }
    	[DataMember]
        public Nullable<System.DateTime> DeletedOn { get; set; }
    	[DataMember]
        public string DeletedBy { get; set; }
    	[DataMember]
        public Nullable<long> DeletedByID { get; set; }
    
    	[DataMember]
        public Staff Staff { get; set; }
    }
    
}
