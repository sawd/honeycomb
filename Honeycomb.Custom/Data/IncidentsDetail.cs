//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Runtime.Serialization; 
namespace Honeycomb.Custom.Data
{
    public partial class IncidentsDetail
    {
    	[DataMember]
        public long ID { get; set; }
    	[DataMember]
        public System.DateTime IncidentDate { get; set; }
    	[DataMember]
        public string IncidentType { get; set; }
    	[DataMember]
        public string Priority { get; set; }
    	[DataMember]
        public string Status { get; set; }
    	[DataMember]
        public string ErfNumber { get; set; }
    	[DataMember]
        public string AssignedTo { get; set; }
    }
    
}
