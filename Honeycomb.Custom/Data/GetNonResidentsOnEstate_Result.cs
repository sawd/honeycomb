//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Runtime.Serialization; 
namespace Honeycomb.Custom.Data
{
    public partial class GetNonResidentsOnEstate_Result
    {
    	[DataMember]
        public string Label { get; set; }
    	[DataMember]
        public decimal Data { get; set; }
    }
    
}
