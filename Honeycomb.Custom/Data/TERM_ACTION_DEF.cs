//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Honeycomb.Custom.Data
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    [DataContract]
    public partial class TERM_ACTION_DEF
    {
        [DataMember] public short AT_TypeNo { get; set; }
        [DataMember] public short UT_TypeNo { get; set; }
        [DataMember] public short TACD_Inside { get; set; }
        [DataMember] public string TACD_Name { get; set; }
        [DataMember] public string TACD_P1 { get; set; }
        [DataMember] public string TACD_P2 { get; set; }
        [DataMember] public string TACD_P3 { get; set; }
        [DataMember] public string TACD_P4 { get; set; }
        [DataMember] public string TACD_P5 { get; set; }
        [DataMember] public string TACD_P6 { get; set; }
        [DataMember] public string TACD_P7 { get; set; }
        [DataMember] public string TACD_P8 { get; set; }
        [DataMember] public string TACD_SYSNAME { get; set; }
    
        [DataMember] public ACTION_TYPE ACTION_TYPE { get; set; }
        [DataMember] public UNIT_TYPE UNIT_TYPE { get; set; }
    }
}
