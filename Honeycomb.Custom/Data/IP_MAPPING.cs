//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Honeycomb.Custom.Data
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    [DataContract]
    public partial class IP_MAPPING
    {
        [DataMember] public string SITE_SLA { get; set; }
        [DataMember] public string TERM_SLA { get; set; }
        [DataMember] public string IP { get; set; }
        [DataMember] public int PORT_FOR_PC { get; set; }
        [DataMember] public int PORT_FOR_HW { get; set; }
        [DataMember] public short IP_MODIFIED { get; set; }
    }
}
