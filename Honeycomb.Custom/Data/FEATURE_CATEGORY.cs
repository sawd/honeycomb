//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Honeycomb.Custom.Data
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    [DataContract]
    public partial class FEATURE_CATEGORY
    {
        public FEATURE_CATEGORY()
        {
            this.FEATURE = new HashSet<FEATURE>();
        }
    
        [DataMember] public short FEAT_CAT_ID { get; set; }
        [DataMember] public string FEAT_CAT_NAME { get; set; }
        [DataMember] public Nullable<short> DISPLAY_TYPE { get; set; }
    
        [DataMember] public ICollection<FEATURE> FEATURE { get; set; }
    }
}
