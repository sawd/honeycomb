//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Honeycomb.Custom.Data
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    [DataContract]
    public partial class CONTROLLER
    {
        public CONTROLLER()
        {
            this.LOCATION = new HashSet<LOCATION>();
            this.TERMINAL = new HashSet<TERMINAL>();
            this.ZONE = new HashSet<ZONE>();
        }
    
        [DataMember] public string CTRL_SLA { get; set; }
        [DataMember] public string HOST_SLA { get; set; }
        [DataMember] public string CTRL_FixedAddr { get; set; }
        [DataMember] public string CTRL_Name { get; set; }
        [DataMember] public short CTRL_AllowCardValidation { get; set; }
        [DataMember] public short UT_TypeNo { get; set; }
        [DataMember] public short CTRL_Modified { get; set; }
        [DataMember] public string SITE_SLA { get; set; }
    
        [DataMember] public ICollection<LOCATION> LOCATION { get; set; }
        [DataMember] public ICollection<TERMINAL> TERMINAL { get; set; }
        [DataMember] public ICollection<ZONE> ZONE { get; set; }
        [DataMember] public HOST HOST { get; set; }
        [DataMember] public UNIT_TYPE UNIT_TYPE { get; set; }
    }
}
