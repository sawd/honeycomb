//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Runtime.Serialization; 
namespace Honeycomb.Custom.Data
{
    [DataContract]
    public partial class Suburb
    {
    	[DataMember]
        public int Sub_TownID { get; set; }
    	[DataMember]
        public int Sub_ID { get; set; }
    	[DataMember]
        public string Sub_Name { get; set; }
    	[DataMember]
        public string Sub_Abv_Name { get; set; }
    	[DataMember]
        public string Sub_post_code { get; set; }
    	[DataMember]
        public Nullable<int> Sub_Box_post_code { get; set; }
    	[DataMember]
        public System.Guid msrepl_tran_version { get; set; }
    }
    
}
