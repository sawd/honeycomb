//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Honeycomb.Custom.Data
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    [DataContract]
    public partial class ACTION_TYPE
    {
        public ACTION_TYPE()
        {
            this.TERM_ACTION = new HashSet<TERM_ACTION>();
            this.TERM_ACTION_DEF = new HashSet<TERM_ACTION_DEF>();
            this.UNIT_TYPE = new HashSet<UNIT_TYPE>();
        }
    
        [DataMember] public short AT_TypeNo { get; set; }
        [DataMember] public string AT_Name { get; set; }
    
        [DataMember] public ICollection<TERM_ACTION> TERM_ACTION { get; set; }
        [DataMember] public ICollection<TERM_ACTION_DEF> TERM_ACTION_DEF { get; set; }
        [DataMember] public ICollection<UNIT_TYPE> UNIT_TYPE { get; set; }
    }
}
