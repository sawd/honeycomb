//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Runtime.Serialization; 
namespace Honeycomb.Custom.Data
{
    [DataContract]
    public partial class MessageTemplate
    {
    	[DataMember]
        public long ID { get; set; }
    	[DataMember]
        public string Name { get; set; }
    	[DataMember]
        public string Type { get; set; }
    	[DataMember]
        public string Message { get; set; }
    	[DataMember]
        public string Placeholders { get; set; }
    }
    
}
