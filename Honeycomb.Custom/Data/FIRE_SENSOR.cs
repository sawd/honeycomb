//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Honeycomb.Custom.Data
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    [DataContract]
    public partial class FIRE_SENSOR
    {
        [DataMember] public short FS_ID { get; set; }
        [DataMember] public short FP_ID { get; set; }
        [DataMember] public short FS_LOOP { get; set; }
        [DataMember] public short FS_NO { get; set; }
        [DataMember] public short TYPE_ID { get; set; }
        [DataMember] public string FS_NAME { get; set; }
        [DataMember] public string FS_GUIDANCE_TEXT { get; set; }
        [DataMember] public short FS_ZONE { get; set; }
        [DataMember] public short FS_SUPER_ZONE { get; set; }
    
        [DataMember] public FIRE_PANEL FIRE_PANEL { get; set; }
        [DataMember] public FIRE_TYPE FIRE_TYPE { get; set; }
    }
}
