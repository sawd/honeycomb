//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Honeycomb.Custom.Data
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    [DataContract]
    public partial class T_A_G_ZONECOUNT
    {
        public T_A_G_ZONECOUNT()
        {
            this.T_A_G_ZONECOUNT_ACTIONS = new HashSet<T_A_G_ZONECOUNT_ACTIONS>();
        }
    
        [DataMember] public int TAGRP_NO { get; set; }
        [DataMember] public string SITE_SLA { get; set; }
        [DataMember] public string CTRL_SLA { get; set; }
        [DataMember] public short ZONE_NO { get; set; }
        [DataMember] public int T_A_G_MAX_CNT { get; set; }
        [DataMember] public int T_A_G_CNT { get; set; }
    
        [DataMember] public ICollection<T_A_G_ZONECOUNT_ACTIONS> T_A_G_ZONECOUNT_ACTIONS { get; set; }
        [DataMember] public TAGHOLDER_ACCESS_GROUP TAGHOLDER_ACCESS_GROUP { get; set; }
        [DataMember] public ZONE ZONE { get; set; }
    }
}
