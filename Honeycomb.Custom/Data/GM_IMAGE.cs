//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Honeycomb.Custom.Data
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    [DataContract]
    public partial class GM_IMAGE
    {
        public GM_IMAGE()
        {
            this.GM_CADDX_PANEL = new HashSet<GM_CADDX_PANEL>();
            this.GM_CADDX_ZONE = new HashSet<GM_CADDX_ZONE>();
            this.GM_CAMERA = new HashSet<GM_CAMERA>();
            this.GM_CUSTOM_APP = new HashSet<GM_CUSTOM_APP>();
            this.GM_CUSTOM_MESSAGE = new HashSet<GM_CUSTOM_MESSAGE>();
            this.GM_DVR = new HashSet<GM_DVR>();
            this.GM_FIRE_PANEL = new HashSet<GM_FIRE_PANEL>();
            this.GM_FIRE_SENSOR = new HashSet<GM_FIRE_SENSOR>();
            this.GM_NORMAL_ICON = new HashSet<GM_NORMAL_ICON>();
        }
    
        [DataMember] public int IMAGE_ID { get; set; }
        [DataMember] public string SITE_SLA { get; set; }
        [DataMember] public short IMAGE_TYPE { get; set; }
        [DataMember] public string IMAGE_NAME { get; set; }
        [DataMember] public byte[] IMAGE { get; set; }
    
        [DataMember] public ICollection<GM_CADDX_PANEL> GM_CADDX_PANEL { get; set; }
        [DataMember] public ICollection<GM_CADDX_ZONE> GM_CADDX_ZONE { get; set; }
        [DataMember] public ICollection<GM_CAMERA> GM_CAMERA { get; set; }
        [DataMember] public ICollection<GM_CUSTOM_APP> GM_CUSTOM_APP { get; set; }
        [DataMember] public ICollection<GM_CUSTOM_MESSAGE> GM_CUSTOM_MESSAGE { get; set; }
        [DataMember] public ICollection<GM_DVR> GM_DVR { get; set; }
        [DataMember] public ICollection<GM_FIRE_PANEL> GM_FIRE_PANEL { get; set; }
        [DataMember] public ICollection<GM_FIRE_SENSOR> GM_FIRE_SENSOR { get; set; }
        [DataMember] public GM_FLOOR_PLAN GM_FLOOR_PLAN { get; set; }
        [DataMember] public GM_GROUND_PLAN GM_GROUND_PLAN { get; set; }
        [DataMember] public GM_MASTER_PLAN GM_MASTER_PLAN { get; set; }
        [DataMember] public ICollection<GM_NORMAL_ICON> GM_NORMAL_ICON { get; set; }
        [DataMember] public GM_SITES_PLAN GM_SITES_PLAN { get; set; }
    }
}
