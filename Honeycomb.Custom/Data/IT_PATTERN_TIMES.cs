//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Honeycomb.Custom.Data
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    [DataContract]
    public partial class IT_PATTERN_TIMES
    {
        [DataMember] public int PATTERN_ID { get; set; }
        [DataMember] public int DAYTYPE_ID { get; set; }
        [DataMember] public int TIME_ID { get; set; }
        [DataMember] public int TIME_ { get; set; }
    
        [DataMember] public IT_PATTERN IT_PATTERN { get; set; }
        [DataMember] public IT_PATTERN_DAYTYPE IT_PATTERN_DAYTYPE { get; set; }
    }
}
