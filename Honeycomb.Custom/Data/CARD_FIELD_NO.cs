//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Honeycomb.Custom.Data
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    [DataContract]
    public partial class CARD_FIELD_NO
    {
        [DataMember] public int FIELD_ID { get; set; }
        [DataMember] public int EV_NO { get; set; }
        [DataMember] public int CARD_ID { get; set; }
        [DataMember] public short FIELD_DELETED { get; set; }
        [DataMember] public short FIELD_LAYER { get; set; }
        [DataMember] public int FIELD_TYPE { get; set; }
    
        [DataMember] public CARD CARD { get; set; }
        [DataMember] public CARD_EVENT CARD_EVENT { get; set; }
        [DataMember] public CARD_FIELD_LOC CARD_FIELD_LOC { get; set; }
        [DataMember] public CARD_FIELD_TYPE CARD_FIELD_TYPE { get; set; }
    }
}
