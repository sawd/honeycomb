//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Honeycomb.Custom.Data
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    [DataContract]
    public partial class TAG_TYPE
    {
        public TAG_TYPE()
        {
            this.TAG = new HashSet<TAG>();
        }
    
        [DataMember] public short TT_TypeNo { get; set; }
        [DataMember] public string TT_Name { get; set; }
        [DataMember] public string TT_Desc { get; set; }
    
        [DataMember] public ICollection<TAG> TAG { get; set; }
    }
}
