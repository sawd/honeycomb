//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Honeycomb.Custom.Data
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    [DataContract]
    public partial class IF_PARAMETER
    {
        [DataMember] public short IPARM_No { get; set; }
        [DataMember] public short ID_No { get; set; }
        [DataMember] public short IDT_TypeNo { get; set; }
        [DataMember] public short IF_TypeNo { get; set; }
        [DataMember] public string TERM_SLA { get; set; }
        [DataMember] public string IPARM_Name { get; set; }
        [DataMember] public string IPARM_Data { get; set; }
    
        [DataMember] public INPUT_DEVICE INPUT_DEVICE { get; set; }
    }
}
