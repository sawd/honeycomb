//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Runtime.Serialization; 
namespace Honeycomb.Custom.Data
{
    public partial class rpt_ListOfApprovedAgents_Result
    {
    	[DataMember]
        public string AgentFirstName { get; set; }
    	[DataMember]
        public string AgentLastName { get; set; }
    	[DataMember]
        public string Name { get; set; }
    }
    
}
