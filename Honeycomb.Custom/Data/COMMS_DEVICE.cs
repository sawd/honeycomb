//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Honeycomb.Custom.Data
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    [DataContract]
    public partial class COMMS_DEVICE
    {
        public COMMS_DEVICE()
        {
            this.COMMS_QUEUE = new HashSet<COMMS_QUEUE>();
        }
    
        [DataMember] public string SITE_SLA { get; set; }
        [DataMember] public int COMMS_ID { get; set; }
        [DataMember] public string COMMS_TYPE { get; set; }
        [DataMember] public string COMMS_LOCATION { get; set; }
    
        [DataMember] public ICollection<COMMS_QUEUE> COMMS_QUEUE { get; set; }
    }
}
