//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Honeycomb.Custom.Data
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    [DataContract]
    public partial class IT_PATTERN
    {
        public IT_PATTERN()
        {
            this.IT_PATTERN_TIMES = new HashSet<IT_PATTERN_TIMES>();
        }
    
        [DataMember] public int PATTERN_ID { get; set; }
        [DataMember] public string PATTERN_NAME { get; set; }
    
        [DataMember] public ICollection<IT_PATTERN_TIMES> IT_PATTERN_TIMES { get; set; }
    }
}
