//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Honeycomb.Custom.Data
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    [DataContract]
    public partial class UT_MT
    {
        [DataMember] public short UT_TypeNo { get; set; }
        [DataMember] public short MOD_No { get; set; }
        [DataMember] public string UT_MT_DESC { get; set; }
    
        [DataMember] public MODE_TYPE MODE_TYPE { get; set; }
        [DataMember] public UNIT_TYPE UNIT_TYPE { get; set; }
    }
}
