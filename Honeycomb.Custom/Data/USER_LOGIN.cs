//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Honeycomb.Custom.Data
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    [DataContract]
    public partial class USER_LOGIN
    {
        [DataMember] public short LOGIN_ID { get; set; }
        [DataMember] public string USER_NAME { get; set; }
        [DataMember] public Nullable<int> OPGRP_NO { get; set; }
    
        [DataMember] public OPERATOR_GROUP OPERATOR_GROUP { get; set; }
    }
}
