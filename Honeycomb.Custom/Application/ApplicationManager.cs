﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Honeycomb.Custom.Application
{
    /// <summary>
    /// A class used to represent the Application state in a web application. All calls to objects in application state should be made through this class.
    /// This helps control the number of Application variables created. Before a property is added to this class permission must be granted by the 
    /// "standards police".
    /// 
    /// </summary>
    public partial class ApplicationManager
    {
        /// <summary>
        /// An instance of the JobScheduler
        /// </summary>
        public static object JobScheduler {
            get{
                if (System.Web.HttpContext.Current!=null)
                    return System.Web.HttpContext.Current.Application["JobScheduler"];
                else {
                    return null;
                }
            }
            set {
                if (System.Web.HttpContext.Current != null) {
                    System.Web.HttpContext.Current.Application["JobScheduler"] = value;
                }
            }
        }

        /// <summary>
        /// An instance of the TimeMonitor that checks server times
        /// </summary>
        public static object TimeMonitor {
            get {
                if (System.Web.HttpContext.Current != null)
                    return System.Web.HttpContext.Current.Application["TimeMonitor"];
                else {
                    return null;
                }
            }
            set {
                if (System.Web.HttpContext.Current != null) {
                    System.Web.HttpContext.Current.Application["TimeMonitor"] = value;
                }
            }
        }

        /// <summary>
        /// A property that describes if there was an error when the application started. If it has, nothing will be allowed through and this message will always be displayed
        /// </summary>
        public static string StartUpError {
            get {
                if (System.Web.HttpContext.Current != null)
                    if (System.Web.HttpContext.Current.Application["StartUpError"] != null) {
                        return System.Web.HttpContext.Current.Application["StartUpError"].ToString();
                    }
                    else {
                        return "";
                    }

                else {
                    return "";
                }
            }
            set {
                if (System.Web.HttpContext.Current != null) {
                    System.Web.HttpContext.Current.Application["StartUpError"] = value;
                }
            }
        }
    }
}
