﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;

namespace Honeycomb.Custom
{
    public class CacheManager : SAWD.WebManagers.CacheManagerBase
    {

        /// <summary>
        /// Stores the current menu string
        /// </summary>        
        public static string HorizontalMenu
        {
            get
            {
                string retval = (string)(GetCache(" HorizontalMenu"));
                return retval;
            }
            set
            {
                SetCache(" HorizontalMenu", value, new TimeSpan(0, 30, 0));
            }
        }

        public static string VerticalMenu {
            get {
                string retval = (string)(GetCache(" VerticalMenu"));
                return retval;
            }
            set {
                SetCache(" VerticalMenu", value, new TimeSpan(0, 30, 0));
            }
        }

        /// <summary>
        /// returns the system id from the appsettings in the configuration
        /// </summary>
        public static Guid SystemID {
            get {
                Guid retval;

                if(GetCache("SystemID") != null) {
                    retval = Guid.Parse((GetCache("SystemID")).ToString());
                } else {
                    retval = Guid.Parse(ConfigurationManager.AppSettings ["SystemID"]);
                }

                return retval;
            }
            set {
                SetCache("SystemID", value, new TimeSpan(0, 30, 0));
            }
        }

        /// <summary>
        /// property that represents the systems CommEngineID, gets it from the database if it is not already in the cache
        /// </summary>
        public static Guid? CommEngineID {
            get {

                Guid? retval;

                if(GetCache("CommEngineID") != null) {
                    retval = Guid.Parse((GetCache("CommEngineID")).ToString());
                } else {
                    retval = null;
                }

                if(retval == null) { 
                    using(var context = new Custom.Data.Honeycomb_Entities()){
                        var systemID = new Guid();
                        systemID = Guid.Parse(ConfigurationManager.AppSettings ["SystemID"]);

                        retval = context.HoldingCompany.Where(hc => hc.SystemID == systemID).Select(hcs => hcs.CommEngineSiteID).FirstOrDefault();
                        SetCache("CommEngineID", retval, new TimeSpan(0, 30, 0));
                    }
                }

                return retval;
            }
            set {
                SetCache("CommEngineID", value, new TimeSpan(0, 30, 0));
            }
        }

        public static List<Custom.Data.Phase> Phases
        {
            get
            {
                List<Custom.Data.Phase> retVal = new List<Custom.Data.Phase>();

                using (var context = new Custom.Data.Honeycomb_Entities())
                {
                    var systemID = new Guid();
                    systemID = Guid.Parse(ConfigurationManager.AppSettings["SystemID"]);

                    retVal = context.Phase.Where(w => w.SystemID == systemID).ToList();
                    SetCache("Phases", retVal, new TimeSpan(0, 30, 0));
                }


                return retVal;
            }
            set
            {
                SetCache("Phases", value, new TimeSpan(0, 30, 0));
            }
        }

        public static Custom.Data.SystemSetting SystemSetting {
            get {
                Custom.Data.SystemSetting retVal = new Custom.Data.SystemSetting();

                    using(var context = new Custom.Data.Honeycomb_Entities()) {
                        var systemID = new Guid();
                        systemID = Guid.Parse(ConfigurationManager.AppSettings ["SystemID"]);

                        retVal = context.SystemSetting.Where(ss => ss.SystemID == systemID).FirstOrDefault();
                        SetCache("SystemSetting", retVal, new TimeSpan(0, 30, 0));
                    }
                

                return retVal;
            }
            set {
                SetCache("SystemSetting",value, new TimeSpan(0,30,0));
            }
        }

		public static string APISecurityToken {
			get {
				string retVal = (string)(GetCache(" APISecurityToken", string.Empty));
				return retVal;
			}
			set {
				SetCache(" APISecurityToken", value, new TimeSpan(0, 30, 0));
			}
		}
      
    }
}
