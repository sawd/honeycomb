﻿#region imports
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Honeycomb.Custom.Data;
using System.Data.EntityClient;
#endregion

namespace Honeycomb.Custom {
    public class ImproSecurity {

        /// <summary>
        /// This function will return the first master record that has a matching SA Identity Number.
        /// </summary>
        /// <param name="IdentityNumber">The id number to match in the impro database.</param>
        /// <returns>A Master record, if no match then it will return an empty master record.</returns>
        public MASTER GetMasterByID(string IdentityNumber) {
            MASTER MasterDB = null;

            if(!ImproDBConnected()) {
                return MasterDB;
            }

            using(var context = new ImproEntities()){
                MasterDB = context.MASTER.Where(m => m.MST_ID == IdentityNumber).FirstOrDefault();
            }

            return MasterDB;
        }


        public int UpdateMaster(MASTER mst) {
            int MST_SQ = 0;

            if(!ImproDBConnected()) {
                return 0;
            }

            try { 
                using(var context = new ImproEntities()){

                    if(mst.MST_SQ == 0) {
                        //add the master record
                        mst.MST_SQ = GetNextMST_SQ();
                        mst.MST_Current = 1;
                        mst.SITE_SLA = "01000000";
                        mst.MT_NO = 1;
                        mst.USRPRF_NUM = null;
                        mst.MST_Title = "";
                        mst.MST_MiddleName = "";
                        mst.MST_Suffix = "";

                        context.MASTER.Add(mst);

                        context.SaveChanges();

                        MST_SQ = mst.MST_SQ;

                    } else { 
                        //update the master record
                        var MST_DB = context.MASTER.Where(m => m.MST_SQ == mst.MST_SQ && m.MST_Current == 1).FirstOrDefault();
                        mst.MergeInto(MST_DB,new string[]{
                        "MST_SQ",
                        "MST_Current",
                        "MST_Type",
                        "SITE_SLA",
                        "MST_CDATE",
                        "MST_ID",
                        "USRPRF_NUM"
                        }, true);

                        context.SaveChanges();

                        MST_SQ = MST_DB.MST_SQ;
                    }

                }


            }catch(Exception ex){
                MST_SQ = 0;
            }

            return MST_SQ;
        }


        /// <summary>
        /// Util function to get the next id to use for the master record
        /// </summary>
        /// <returns>The MAX MST_SQ incremented once.</returns>
        private int GetNextMST_SQ() {
            int NextID = 0;

            if(!ImproDBConnected()) {
                return 0;
            }

            try { 

                using(var context = new ImproEntities()){
                    NextID = context.MASTER.Max(m => m.MST_SQ);
                    NextID++;
                }

            }catch(Exception ex){
                NextID = 0;
            }

            return NextID;
        }


        /// <summary>
        /// Takes an occupant argument and finds if there is a master record matching the id. If there is it updates certain fields otherwise it will insert a new one.
        /// </summary>
        /// <param name="Occupant">An object of type Occupant</param>
        /// <returns>An empty string if successful, otherwise a message describing the error.</returns>
        public string SynchroniseImpro(Occupant Occupant) {
            var message = "";
            int MST_SQ = 0;

            if(!ImproDBConnected()) {
                return "";
            }

            try {

                if(!string.IsNullOrEmpty(Occupant.IDOrPassportNo)) {
                    var MasterDBChk = GetMasterByID(Occupant.IDOrPassportNo);

                    //check if there is matching record in impro 
                    if(MasterDBChk == null) {
                        //if not insert a new record
                        var newMaster = new MASTER();
                        newMaster.MST_FirstName = Occupant.FirstName;
                        newMaster.MST_LastName = Occupant.LastName;
                        newMaster.MST_Gender = Occupant.Gender.Substring(0,1);
                        newMaster.MST_ID = Occupant.IDOrPassportNo;

                        MST_SQ = UpdateMaster(newMaster);
                        
                    } else { 
                        //if there is update
                        MasterDBChk.MST_FirstName = Occupant.FirstName;
                        MasterDBChk.MST_LastName = Occupant.LastName;
                        MasterDBChk.MST_Gender = Occupant.Gender.Substring(0, 1);

                        MST_SQ = UpdateMaster(MasterDBChk);
                    }

                    if(MST_SQ != 0) {
                        updateMasterEmployee(MST_SQ, 10);
                    }
                }

            }catch(Exception ex){

            }

            return message;
        }

        /// <summary>
        /// Takes an occupant argument and finds if there is a master record matching the id. If there is it updates certain fields otherwise it will insert a new one.
        /// </summary>
        /// <param name="Staff">An object of type Occupant</param>
        /// <returns>An empty string if successful, otherwise a message describing the error.</returns>
        public string SynchroniseImpro(Staff Staff) {
            var message = "";
            int MST_SQ = 0;

            if(!ImproDBConnected()) {
                return "";
            }

            try {

                if(!string.IsNullOrEmpty(Staff.GOVID)) {
                    var MasterDBChk = GetMasterByID(Staff.GOVID);

                    //check if there is matching record in impro 
                    if(MasterDBChk == null) {
                        //if not insert
                        var newMaster = new MASTER();
                        newMaster.MST_FirstName = Staff.FirstName;
                        newMaster.MST_LastName = Staff.Lastname;
                        newMaster.MST_Gender = Staff.Gender.Substring(0, 1);
                        newMaster.MST_ID = Staff.GOVID;

                        MST_SQ = UpdateMaster(newMaster);

                    } else {
                        //if there is update
                        MasterDBChk.MST_FirstName = Staff.FirstName;
                        MasterDBChk.MST_LastName = Staff.Lastname;
                        MasterDBChk.MST_Gender = Staff.Gender.Substring(0, 1);

                        MST_SQ = UpdateMaster(MasterDBChk);
                    }

                    if(MST_SQ != 0) {
                        updateMasterEmployee(MST_SQ, 2);
                    }

                }

            } catch(Exception ex) {

            }

            return message;
        }

        /// <summary>
        /// inserts an employee record for the master record as this is required for the master record to appear in the Impro Tag Holder app.
        /// </summary>
        /// <param name="MST_SQ"></param>
        /// <param name="departmentNo"></param>
        /// <returns></returns>
        public bool updateMasterEmployee(int MST_SQ, int departmentNo) {

            if(!ImproDBConnected()) {
                return false;
            }

            try {

                using(var context = new ImproEntities()){
                    var mstEmployeeDB = context.EMPLOYEE.Where(e => e.MST_SQ == MST_SQ).FirstOrDefault();

                    //check if it does not exist, if not insert
                    if(mstEmployeeDB == null) {
                        var insertEmployee = new Custom.Data.EMPLOYEE();
                        insertEmployee.MST_SQ = MST_SQ;
                        insertEmployee.EMP_EmployeeNo = MST_SQ.ToString();
                        insertEmployee.DEPT_No = departmentNo;
                        insertEmployee.SITE_SLA = "01000000";
                        insertEmployee.EMP_Employer = "";
                        insertEmployee.EMP_Position = "";

                        context.EMPLOYEE.Add(insertEmployee);
                        context.SaveChanges();
                    } 
                }

                return true;
            } catch(Exception) {
                return false;
            }
        }

        public string SetMasterTagAccessGroup(string idNo, int tagGroupNo, DateTime? startDate, DateTime? endDate) {
            string returnMessage = "";

            var master = GetMasterByID(idNo);
			int tagStart = 0;
			int tagExpire = 0;

			if(startDate != null) {
				tagStart = Int32.Parse(startDate.Value.ToString("yyyyMMdd"));
			}

			if(endDate != null) {
				tagExpire = Int32.Parse(endDate.Value.ToString("yyyyMMdd"));
			}

            if(master != null) {

                using(var context = new ImproEntities()) {
                    var tags = context.TAG.Include("TAG_T_A_G").Where(t => t.MST_SQ == master.MST_SQ).ToList();

                    if(tags.Count > 0) {
                        //there are tags update them

                        foreach(var tag in tags) {
                            tag.TAG_Start = tagStart;
                            tag.TAG_Expiry = tagExpire;
                            tag.TAG_Modified = 1;

                            tag.TAG_T_A_G = new List<TAG_T_A_G>();

                            tag.TAG_T_A_G.Add(new TAG_T_A_G() {
                                TAG_Code = tag.TAG_Code,
                                TAGRP_No = tagGroupNo,
                                SITE_SLA = "01000000",
                                TT_TypeNo = 1,
                                TTAG_Modified = 1
                            });
                        }

                        context.SaveChanges();

                        returnMessage = "Successfully updated tag holders' access group.";

                    } else {

                        //insert with tag type = 1 (Slim Tag), SITE_SLA = 01000000
                        var tag = new TAG();

                        tag.TAG_Code = master.MST_SQ.ToString();
                        tag.TT_TypeNo = 1;
                        tag.TAG_Suspend = 0;
                        tag.TAG_Report = 1;
                        tag.TAG_BlackList = 0;
                        tag.TAG_NoAPB = 0;
						tag.TAG_Start = tagStart;
                        tag.TAG_Expiry = tagExpire;
                        tag.TAG_Visitor = 0;
                        tag.TAG_Modified = 1;
                        tag.MST_SQ = master.MST_SQ;
                        tag.TAG_SPECIAL_EV = 0;
                        tag.tag_expirytime = 0;
                        tag.TAG_NO = 0;

                        tag.TAG_T_A_G.Add(new TAG_T_A_G() {
                            TAG_Code = master.MST_SQ.ToString(),
                            TAGRP_No = tagGroupNo,
                            SITE_SLA = "01000000",
                            TT_TypeNo = 1,
                            TTAG_Modified = 1
                        });

                        context.TAG.Add(tag);
                        context.SaveChanges();

                        //there are no tags, insert one with defaults
                        returnMessage = "There were no tags associated with the provided ID no. A tag has been added with default values. Please make sure to update the tag number once one is issued.";
                    }

                }

            } else {
                returnMessage = "There is no record of the provided tag holder, please add them first by clicking the update button and then set their access details.";
            }

            return returnMessage;
        }

        //public bool SetEMSTagHolder(string idNo, int tagGroupID, DateTime startDate, DateTime endDate) { 

        //}

        public static List<TAGHOLDER_ACCESS_GROUP> GetAccessGroups() {

            var accessGroups = new List<TAGHOLDER_ACCESS_GROUP>();

            if(!ImproDBConnected()) {
                return accessGroups;
            }

            using(var context = new ImproEntities()){
                accessGroups = context.TAGHOLDER_ACCESS_GROUP.Where(thag => thag.TAGRP_Suspended == 0).OrderBy(o => o.TAGRP_Name).ToList();
            }

            return accessGroups;
        }

        /// <summary>
        /// This function has not been completed.
        /// </summary>
        /// <param name="idNo"></param>
        /// <returns></returns>
        public static CustomImproTag GetMasterAccessGroup(string idNo) {
            var improSecurity = new Custom.ImproSecurity();
            var masterRecord = improSecurity.GetMasterByID(idNo);
            var customImprotag = new CustomImproTag();

            if(masterRecord != null) { 
                using(var context = new ImproEntities()){
                    var tag = context.TAG.Include("TAG_T_A_G").Where(t => t.MST_SQ == masterRecord.MST_SQ && t.TAG_Suspend == 0).OrderByDescending(o => o.TAG_Expiry).FirstOrDefault();

                    if(tag != null) { 
                        string startDate = tag.TAG_Start.ToString();
                        string endDate = tag.TAG_Expiry.ToString();

                        if(startDate.Length == 8 && endDate.Length == 8) {
                            //apologies... I have to massage date values into datetimes because of how impro stores dates
                            customImprotag.StartDate = new DateTime(Int32.Parse(startDate.Substring(0, 4)), Int32.Parse(startDate.Substring(5, 2)), Int32.Parse(startDate.Substring(7, 2)));
                            customImprotag.EndDate = new DateTime(Int32.Parse(endDate.Substring(0, 4)), Int32.Parse(endDate.Substring(5, 2)), Int32.Parse(endDate.Substring(7, 2)));
                        }
                    }
                }
            }

            return customImprotag;
        }



        /// <summary>
        /// this function will disable a master record
        /// </summary>
        /// <param name="identityNumber"></param>
        /// <returns></returns>
        public bool DisableMasterRecord(string identityNumber) {

            if(!ImproDBConnected()) {
                return false;
            }

            try {

                using(var context = new ImproEntities()) {
                    var masterRecord = GetMasterByID(identityNumber);

                    if(masterRecord != null) {

                        masterRecord.MST_Current = 0;

                        context.MASTER.Attach(masterRecord);
                        
                        var entry = context.Entry(masterRecord);
                        entry.State = System.Data.EntityState.Modified;

                        context.SaveChanges();

                        SuspendUserTags(masterRecord.MST_SQ);

                        return true;
                    } else {
                        return false;
                    }
                }
                
            }catch(Exception){
                return false;
            }
        }

        /// <summary>
        /// suspends all tags that the user has been assigned.
        /// </summary>
        /// <param name="MST_SQ"></param>
        /// <returns></returns>
        public bool SuspendUserTags(int MST_SQ) {

            if(!ImproDBConnected()) {
                return false;
            }

            try {
                using(var context = new ImproEntities()){
					var userTags = context.TAG.Include("TAG_T_A_G").Where(ut => ut.MST_SQ == MST_SQ).ToList();//Include("TAG_T_A_G")

                    foreach(var userTag in userTags) {
                        userTag.TAG_Suspend = 1;
                        userTag.TAG_Modified = 1;

						foreach(var tag_T_A_G in userTag.TAG_T_A_G) {
							tag_T_A_G.TTAG_Modified = 1;
						}

                    }

                    context.SaveChanges();
                }

                return true;
            }catch(Exception){
                return false;
            }
        }

		/// <summary>
		/// this function will restore a master record
		/// </summary>
		/// <param name="identityNumber"></param>
		/// <returns></returns>
		public bool RestoreMasterRecord(string identityNumber) {

			if (!ImproDBConnected()) {
				return false;
			}

			try {

				using (var context = new ImproEntities()) {
					var masterRecord = GetMasterByID(identityNumber);

					if (masterRecord != null) {

						masterRecord.MST_Current = 1;

						context.MASTER.Attach(masterRecord);

						var entry = context.Entry(masterRecord);
						entry.State = System.Data.EntityState.Modified;

						context.SaveChanges();

						RestoreUserTags(masterRecord.MST_SQ);

						return true;
					} else {
						return false;
					}
				}

			} catch (Exception) {
				return false;
			}
		}

		/// <summary>
		/// suspends all tags that the user has been assigned.
		/// </summary>
		/// <param name="MST_SQ"></param>
		/// <returns></returns>
		public bool RestoreUserTags(int MST_SQ) {

			if (!ImproDBConnected()) {
				return false;
			}

			try {
				using (var context = new ImproEntities()) {
					var userTags = context.TAG.Include("TAG_T_A_G").Where(ut => ut.MST_SQ == MST_SQ).ToList();//Include("TAG_T_A_G")

					foreach (var userTag in userTags) {
						userTag.TAG_Suspend = 0;
						userTag.TAG_Modified = 1;

						foreach (var tag_T_A_G in userTag.TAG_T_A_G) {
							tag_T_A_G.TTAG_Modified = 1;
						}

					}

					context.SaveChanges();
				}

				return true;
			} catch (Exception) {
				return false;
			}
		}

		/// <summary>
		/// Retrieves the image stream for a master record.
		/// </summary>
		/// <param name="identityNumber"></param>
		/// <returns></returns>
		public Byte [] GetMasterImageByID(string identityNumber) {
			if (!ImproDBConnected()) {
				return null;
			}
			try {
				using (var context = new ImproEntities()) {
					var masterRecord = GetMasterByID(identityNumber);
					if (masterRecord != null) {
						var imageEntry = (from images in context.IMAGE
										  where images.MST_SQ == masterRecord.MST_SQ
										  select images.IMG_Image).FirstOrDefault();
						return imageEntry;
					} else {
						return null;
					}
				}
			} catch (Exception) {
				return null;
			}
		}

        /// <summary>
        /// Will retrieve a byte array for the the image associated with the provided master record
        /// </summary>
        /// <param name="MST_SQ">An integer id of the master record</param>
        /// <returns>A byte arrary containing the data for the image</returns>
        public byte [] GetMasterImage(int MST_SQ) { 
            byte [] retImageBA = null;

            if(!ImproDBConnected()) {
                return retImageBA;
            }

            try {
                    using(var context = new ImproEntities()){
                        var imageDB = context.IMAGE.Where(i => i.MST_SQ == MST_SQ).FirstOrDefault();

                        if(imageDB != null) {
                            retImageBA = imageDB.IMG_Image;
                        }
                    }

                return retImageBA;
            }catch(Exception ex){
                return retImageBA;
            }

        }

        /// <summary>
        /// Validates that the system can connect to the impro database
        /// </summary>
        /// <returns>false if a successfull connection cannot be opened.</returns>
        private static bool ImproDBConnected() {
            bool success = false;

            EntityConnection improConnection = new EntityConnection("name=ImproEntities");

            try {

                improConnection.Open();

                if(improConnection.State == System.Data.ConnectionState.Open) {
                    success = true;
                } else {
                    success = false;
                }

                improConnection.Dispose();
                
            }catch(Exception){
                success = false;
                improConnection.Dispose();
            }

            return success;
        }

    }
}
