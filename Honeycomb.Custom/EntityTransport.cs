﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Honeycomb.Custom {
    public sealed class EntityTransport<T> {
        public List<T> EntityList { get; set; }
        public int Count { get; set; }
        public string Message { get; set; }
        public string EntityType { get; set; }
    }
}
