﻿#region Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Configuration;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Web;
using Honeycomb.Custom;
#endregion

namespace Honeycomb.Custom {
    class CustomBehaviour : BehaviorExtensionElement, IEndpointBehavior {
        public void AddBindingParameters(ServiceEndpoint serviceEndpoint, System.ServiceModel.Channels.BindingParameterCollection bindingParameters) {
        }

        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime) {
            clientRuntime.MessageInspectors.Add(new MessageInspector());
        }

        public void ApplyDispatchBehavior(ServiceEndpoint serviceEndpoint, System.ServiceModel.Dispatcher.EndpointDispatcher endpointDispatcher) {
            endpointDispatcher.DispatchRuntime.MessageInspectors.Add(new MessageInspector());
        }

        public void Validate(ServiceEndpoint serviceEndpoint) {
        }
        public override Type BehaviorType {
            get {
                return typeof(CustomBehaviour);
            }
        }

        protected override object CreateBehavior() {
            return new CustomBehaviour();
        }
    }
}
