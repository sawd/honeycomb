﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Linq.Dynamic;
using System.Data;
using System.Dynamic;
using System.Data.Entity;
using Honeycomb.Custom;

namespace System.Linq {
    public static class PageQuery {
        public static IQueryable<T> Paginate<T>(
            this IQueryable<T> query, int page, int pageSize) {
            int skip = Math.Max(pageSize * (page), 0);
            query.Count();
            return query.Skip(skip).Take(pageSize);
        }
        public static IQueryable<T> Paginate<T>(
            this IQueryable<T> query, int page, int pageSize, string orderBy) {
            int skip = Math.Max(pageSize * (page), 0);
            query.Count();
            return query.OrderBy(orderBy).Skip(skip).Take(pageSize);
        }
        public static IEnumerable<T> Paginate<T>(
        this IEnumerable<T> query, int page, int pageSize) {
            int skip = Math.Max(pageSize * (page), 0);
            query.Count();
            return query.Skip(skip).Take(pageSize);
        }
    }

    public static class Merge { 
        public static void MergeInto<T>(this T source, T destination, string[] fields, bool Exclude = false) {
            var propertyNameList = source.GetType().GetProperties().Select(s => s.Name);
            if (Exclude) {
                propertyNameList = propertyNameList.Where(w => !fields.Contains(w));
            } else {
                propertyNameList = propertyNameList.Where(w => fields.Contains(w));
            }
            foreach (var prop in propertyNameList ) {
                PropertyInfo destinationProp = destination.GetType().GetProperty(prop);
                PropertyInfo sourceProp = source.GetType().GetProperty(prop);
                destinationProp.SetValue(destination, sourceProp.GetValue(source));
            }
        }
    }

	public static class CSV {
		public static string ToCSV<T>(this IQueryable<T> source, bool OutputHeaders = true, string[] fields = null) {

			var propertyNameList = source.ElementType.GetProperties().Select(s => s.Name);
			if (fields != null) {
				propertyNameList = propertyNameList.Where(w => fields.Contains(w));
			}
			StringBuilder csv = new StringBuilder();
			// If OutputHeaders is set to true, Generate the Header Row
			string lastProp = propertyNameList.Last();
			if (OutputHeaders) {
				foreach (var prop in propertyNameList) {
					csv.Append(Utility.CSVCellSanitise(prop));
					if (prop != lastProp) {
						csv.Append(",");
					} else {
						csv.Append("\n");		
					}
				}
				
			}
			// Generate each line for the CSV
			foreach (T item in source) {
				foreach (var prop in propertyNameList) {
					object propValue = Utility.ConvertNull(item.GetType().GetProperty(prop).GetValue(item), "");
						
					csv.Append(Utility.CSVCellSanitise(propValue.ToString()));
					if (prop != lastProp) {
						csv.Append(",");
					} else {
						csv.Append("\n");
					}
				}
			}

			return csv.ToString();
		}
	}

    public static class SetModified {
        public static void SetAllModified<T>(this T entity, System.Data.Objects.ObjectContext context) where T : System.Data.Objects.DataClasses.IEntityWithKey {
            var stateEntry = context.ObjectStateManager.GetObjectStateEntry(entity.EntityKey);
            var propertyNameList = stateEntry.CurrentValues.DataRecordInfo.FieldMetadata.Select(pn => pn.FieldType.Name);
            foreach (var propName in propertyNameList) {
                stateEntry.SetModifiedProperty(propName);
            }
        }

        /// <summary>
        /// Sets properties in an entity object to modified
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity"></param>
        /// <param name="context">The entity Object</param>
        /// <param name="PropertyList">An array of property names to either include or exclude</param>
        /// <param name="Exclude">If true, excludes properties from the modification</param>
        public static void SetAllModified<T>(this T entity, System.Data.Objects.ObjectContext context, string[] PropertyList, bool Exclude = false) where T : System.Data.Objects.DataClasses.IEntityWithKey {
            var stateEntry = context.ObjectStateManager.GetObjectStateEntry(entity.EntityKey);
            var propertyNameList = stateEntry.CurrentValues.DataRecordInfo.FieldMetadata.Select(pn => pn.FieldType.Name);
            if (Exclude) {
                propertyNameList = propertyNameList.Where(w => !PropertyList.Contains(w));
            } else {
                propertyNameList = propertyNameList.Where(w => propertyNameList.Contains(w));
            }
            foreach (var propName in propertyNameList) {

                stateEntry.SetModifiedProperty(propName);
            }
        }

        public static void SetAllModified<T>(this T entity, T otherEntity, System.Data.Objects.ObjectContext context, string[] PropertyList, bool Exclude = false) where T : System.Data.Objects.DataClasses.IEntityWithKey {
            var stateEntry = context.ObjectStateManager.GetObjectStateEntry(entity.EntityKey);
            var propertyNameList = stateEntry.CurrentValues.DataRecordInfo.FieldMetadata.Select(pn => pn.FieldType.Name);
            if (Exclude) {
                propertyNameList = propertyNameList.Where(w => !PropertyList.Contains(w));
            } else {
                propertyNameList = propertyNameList.Where(w => propertyNameList.Contains(w));
            }
            foreach (var propName in propertyNameList) {
                PropertyInfo propInfo = entity.GetType().GetProperty(propName);
                if (propInfo.GetValue(entity, null) != propInfo.GetValue(otherEntity, null)) {
                    propInfo.SetValue(entity, propInfo.GetValue(otherEntity, null), null);
                }
                stateEntry.SetModifiedProperty(propName);
            }
        }


    }



}

namespace SAWD.ExtensionMethods {
    public static class AffectAllEntities {
        public static void AttachAll<T>(this System.Data.Objects.ObjectContext context, IQueryable<T> entityList) {
            foreach (T entity in entityList) {
                context.AttachTo(entity.GetType().Name, entity);
            }
        }
        public static void AddAll<T>(this System.Data.Objects.ObjectContext context, IQueryable<T> entityList) {
            foreach (T entity in entityList) {
                context.AddObject(entity.GetType().Name, entity);
            }
        }
        
    }

    public class DynamicDataRow : DynamicObject {
        /// <summary>
        /// Creates a dynamic wrapper for datarows, allowing convenient access to data members.
        /// </summary>
        private DataRow _dataRow;

        public DynamicDataRow(DataRow dataRow) {
            if (dataRow == null)
                throw new ArgumentNullException("dataRow");
            this._dataRow = dataRow;
        }

        public DataRow DataRow {
            get { return _dataRow; }
        }

        public override bool TryGetMember(GetMemberBinder binder, out object result) {
            result = null;
            if (_dataRow.Table.Columns.Contains(binder.Name)) {
                result = _dataRow[binder.Name];
                return true;
            }
            return false;
        }

        public override bool TrySetMember(SetMemberBinder binder, object value) {
            if (_dataRow.Table.Columns.Contains(binder.Name)) {
                _dataRow[binder.Name] = value;
                return true;
            }
            return false;
        }
    }

    public static class DynamicDataRowExtensions {
        public static dynamic AsDynamic(this DataRow dataRow) {
            return new DynamicDataRow(dataRow);
        }
    }

    public static class BindToDropDown {
        public static void Bind(this System.Web.UI.WebControls.DropDownList control, object dataSource, string dataTextField, string dataValueField) {
            control.DataSource = dataSource;
            control.DataTextField = dataTextField;
            control.DataValueField = dataValueField;
        }

    }

    
}

