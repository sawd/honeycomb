﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Honeycomb.Custom {
    public static class Enumerators {
        public enum ERFTypes { CommonProperty = 10001, Mixed, SectionalTitle, SingleResidential};

        public enum MessageType {
            SMSAccessCardActivation = 10000,
            EmailAccessCardActivation = 10001,
            EmailTenantInduction = 10002,
            EmailTenantLeaseExpiryReminder = 10003,
            EmailOwnerLeaseExpiryReminder=10004, 
            EmailAgentLeaseExpiryReminder=10005,            
            EmailCopyOfLeaseEmailAdmin=10006,
            EmailCopyOfLeaseEmailOwner = 10007,
            SMSAccessCode = 10008,
			EmailClientBookingNotification,
			EmailAdminBookingNotification,
			EmailClientBookingCancelled,
			EmailAdminBookingCancelled,
			EmailContractorDiscrepency,
			EmailOccupantIntranetDetails
        };

        public enum NotificationType { Security = 10000, Accounts, Admin=10002, ControlRoom=10003 };

        public enum LookupList { JobTitle = 10000, Saluation , IncidentType, IncidentSource, IncidentPriority};

		public enum VenueBookingStatus { Pending = 10000, Cancelled };

        //enumerator for file types
        public enum FileType
        {
            [DescriptionAttribute("VetCertificate")]
            VetCertificate,
            [DescriptionAttribute("ChipCertificate")]
            ChipCertificate
        }
    }

}
