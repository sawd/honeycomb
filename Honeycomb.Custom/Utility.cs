﻿using System;
using System.Data;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using Honeycomb.Custom.Data;
using System.Net.Mail;
using System.Web;
using System.Configuration;
using System.IO;
using System.Drawing.Imaging;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace Honeycomb.Custom
{
    public static class Utility
    {
        public static string[] GetEnumNames<T>()
        {
            return Enum.GetNames(typeof(T));
        }

        //checks if the file type specified exists in our enumerator, this is to protect against broken clientside code not sending up a valid option. This will prevent orphanining
        public static bool IsCustomFileType(string fileType)
        {
            var availableFileTypes = Utility.GetEnumNames<Enumerators.FileType>();

            return availableFileTypes.Contains(fileType);
        }

        /// <summary>
        /// Converts an object to the specified type or returns the specified 
        /// null replacement if it is null.
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <typeparam name="T">Type of object to be returned</typeparam>
        /// <param name="value">Value to be checked</param>
        /// <param name="replacement">Replacement if value is null</param>
        /// <returns>Value as T or replacement value if null</returns>
        public static T ConvertNull<T>(object value, T replacement)
        {
            if ((value == null) || (value == DBNull.Value))
                return replacement;
            else
                return (T)Convert.ChangeType(value, typeof(T));
        }

        /// <summary>
        /// Returns an entity collection with/without an error 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static EntityTransport<T> ServiceResponse<T>(ref List<T> data, Exception error = null)
        {

            if(error != null) {
                SendExceptionMail(error);
            }

            return new EntityTransport<T>
            {
                EntityList = data,
                Count = data.Count,
                Message = error == null ? string.Empty : error.Message,
                EntityType = (data.GetType().GenericTypeArguments[0]).FullName
            };
        }

        public static EntityTransport<T> ServiceResponse<T>(ref T data, Exception error = null)
        {
            var entityListItems = new List<T>();
            entityListItems.Add(data);

			if(error != null) {
                SendExceptionMail(error);
            }

            return ServiceResponse<T>(ref entityListItems);
        }

		public static EntityTransport<T> ServiceResponse<T>(ref T data, Exception error, bool sendExceptionEmail) {
			var entityListItems = new List<T>();
			entityListItems.Add(data);

			if(error != null && sendExceptionEmail) {
				SendExceptionMail(error);
			}

			 return new EntityTransport<T>
            {
				EntityList = entityListItems,
				Count = entityListItems.Count,
                Message = error == null ? string.Empty : error.Message,
				EntityType = (entityListItems.GetType().GenericTypeArguments[0]).FullName
            };
		}

        public static EntityTransport<T> ServiceResponse<T>(ref List<T> data, int totalCount, Exception error = null)
        {

            if(error != null) {
                SendExceptionMail(error);
            }

            return new EntityTransport<T>
            {
                EntityList = data,
                Count = totalCount,
                Message = error == null ? string.Empty : error.Message,
                EntityType = (data.GetType().GenericTypeArguments[0]).FullName
            };
        }

        /// <summary>
        /// Checks if a user has access to perform a function this uses the SecurityAttribute that must be defined on every WCF method.
        /// </summary>
        /// <returns></returns>
        public static bool WCFCheckSecurityAttribute() {
            //check the session and send error for API:Your session has timed out
            if (!SessionManager.isLoggedIn) {
                throw new Exception("Your session has timed out");
            }

            StackTrace stackTrace = new StackTrace();
            StackFrame stackFrame = stackTrace.GetFrame(1);
            MethodBase methodBase = stackFrame.GetMethod();
            var attributes = methodBase.GetCustomAttributes(typeof(SecurityAttribute));
            var roles = attributes.FirstOrDefault();

            if(roles != null) {
                var rolesList = ((SecurityAttribute [])(attributes)) [0].Roles.Split(new char [] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList().Select(t => t).ToList();
                if(!CheckUserAccess(rolesList)) {
                    throw new Exception("You do not have the required permissions to access this information");
                } else {
                    return true;
                }
            } else {
                throw new Exception("You do not have the required permissions to access this information");
            }
        }

        /// <summary>
        /// Checks if a user has access to perform a function.
        /// </summary>
        /// <param name="roles"></param>
        /// <returns></returns>
        public static bool CheckUserAccess(List<string> roles) {
            if(SessionManager.UserRoles.Intersect(roles).Count() > 0) {
                return true;
            }
            return false;
        }

		public static bool CheckUserAccess(string role) {
			List<string> roles = new List<string> {role};
			return CheckUserAccess(roles);
		}

        /// <summary>
        /// Checks if a user has access to perform a function.
        /// </summary>
        /// <param name="roles"></param>
        /// <returns></returns>
        //public static bool CheckUserAccess(List<long> roles)
        //{
        //    if (SessionManager.UserRoles.Intersect(roles).Count() > 0)
        //    {
        //        return true;
        //    }
        //    return false;
        //}

        /// <summary>
        /// Adds our custom http headers that send the Juice System Session ID for the current authenticated API user. 
        /// </summary>
        /// <param name="currentRequest"></param>
        public static void AddCustomHeadersToWCFRequest(ref Message currentRequest)
        {
            //create a new http request message to send to WCF Service System
            HttpRequestMessageProperty channelRequestMessageProperty = new HttpRequestMessageProperty();

        }
        #region security and session checks
        public static void CheckWCFSecurity()
        {

        }

        public static void CheckSession()
        {
            if (!SessionManager.isLoggedIn)
                throw new Exception("SessionExpired");
        }
        #endregion
        /// <summary>
        /// Extends IQueryable to allow an optional where to be applied
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="condition"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static IQueryable<T> OptionalWhere<T>(this IQueryable<T> source, bool condition, Expression<Func<T, bool>> predicate)
        {
            return condition ? source.Where(predicate) : source;
        }
        /// <summary>
        /// Extends IQueryable to iterate a collection
        /// </summary>
        /// <typeparam name="T"> </typeparam>
        /// <param name="enumeration">collecttion</param>
        /// <param name="action"></param>
        public static void ForEach<T>(this IEnumerable<T> enumeration, Action<T> action)
        {
            foreach (T item in enumeration)
            {
                action(item);
            }
        }
        /// <summary>
        /// Generates a dataset fro an entity Object
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumarableSource"></param>
        /// <returns></returns>
        public static DataSet ToDataset<T>(this IEnumerable<T> enumarableSource)
        {
            DataRow row;
            DataColumn cols1;
            DataSet ds = new DataSet();
            ds = ds.Copy();
            int i = 0;
            foreach (T item in enumarableSource)
            {
                var cols = item.GetType().GetProperties().Select(s => s.Name);

                       ds.Tables.Add(new DataTable());
                foreach (var columns in cols)
                {

                    ds.Tables[i].Columns.Add(new DataColumn(columns.ToString(), System.Type.GetType("System.String")));
                    row = ds.Tables[i].NewRow();

                    PropertyInfo colValues = item.GetType().GetProperty(columns);

                    if (colValues.GetValue(item) != null)
                    {
                        row[columns.ToString()] = colValues.GetValue(item).ToString();
                    }
                    else
                    {
                        row[columns.ToString()] =Utility.ConvertNull( colValues.GetValue(item),"");
                    
                    }
                    ds.Tables[i].Rows.Add(row);
              
                }
                i = i + 1;
            }
            return ds;
        }

        public static void SendExceptionMail(Exception ex, String message = "") {

            if(ex.Message != "SessionExpired") {

                try {
                    string userDetails = "";
                    string page = "";

                    if(SessionManager.isLoggedIn == true) {
                        userDetails = SessionManager.FirstName + " " + SessionManager.LastName + ", " + SessionManager.UserName + "<br/>";
                    } else {
                        userDetails = "Honeycomb System <br/>";
                    }

                    userDetails += string.Format("{0}<br/>{1}<br/><br/>",
                                                 HttpContext.Current.Request.UserAgent,
                                                 HttpContext.Current.Request.UserHostAddress);
                    var paramses = HttpContext.Current.Request.Params;
                    foreach(var item in paramses.AllKeys) {
                        userDetails += item + " : " + paramses[item] + "<br/>";
                    }

                    page = HttpContext.Current.Request.Url.OriginalString;
                    string request = string.Empty;
					
					if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["DebugLog"])) {
						LogException(ex, message);
					}
                    Communications comm = new Communications();

                    comm.SendEmail(ConfigurationManager.AppSettings["ErrorsEmail"],
                                    "Error occured on HoneyComb - " + System.Environment.MachineName,
                                    string.Format("An exception occurred on: {0}<br/>{8}<br/><br/><b>Message:</b><br/>{6}<br/>{1}<br/><br/><b>Page:</b><br/>{7}<br/><br/> <b>Stacktrace:</b><br/> {2} <br/><br/> <b>InnerException:<br/></b>{3} <br/><br/><b>Request Body:</b><br/>{4}<br/><br/><br/><b>User Details:</b><br/> {5}",
                                        System.Environment.MachineName,
                                        ex.Message,
                                        ex.StackTrace,
                                        ex.InnerException,
                                        HttpContext.Current.Server.HtmlEncode(request),
                                        userDetails,
                                        message,
                                        page,
                                        DateTime.Now.ToString()));

                } catch(Exception exception) {
					if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["DebugLog"])) {
						LogException(ex, message);
					}
                    return;
                }

                return;
            }
        }

		public static void LogException(Exception exc, string source) {
			// Include logic for logging exceptions
			// Get the absolute path to the log file
			string logFile = ConfigurationManager.AppSettings["logpath"] ?? "/ErrorLog.txt";
			logFile = HttpContext.Current.Server.MapPath(logFile);

			// Open the log file for append and write the log
			using (StreamWriter sw = System.IO.File.AppendText(logFile)) {
				sw.WriteLine("********** {0} **********", DateTime.Now);
				if (exc.InnerException != null) {
					sw.Write("Inner Exception Type: ");
					sw.WriteLine(exc.InnerException.GetType().ToString());
					sw.Write("Inner Exception: ");
					sw.WriteLine(exc.InnerException.Message);
					sw.Write("Inner Source: ");
					sw.WriteLine(exc.InnerException.Source);
					if (exc.InnerException.StackTrace != null) {
						sw.WriteLine("Inner Stack Trace: ");
						sw.WriteLine(exc.InnerException.StackTrace);
					}
				}
				sw.Write("Exception Type: ");
				sw.WriteLine(exc.GetType().ToString());
				sw.WriteLine("Exception: " + exc.Message);
				sw.WriteLine("Source: " + source);
				sw.WriteLine("Stack Trace: ");
				if (exc.StackTrace != null) {
					sw.WriteLine(exc.StackTrace);
					sw.WriteLine();
				}
				sw.Close();
			}
		}

        #region MessageType
        /// <summary>
        /// gets the template string for a message template
        /// </summary>
        /// <param name="NotificationType"></param>
        /// <returns></returns>
        public static string GetMessageContent(Enumerators.MessageType MessageType)
        {
            var returnStr = "";

            using (var context = new Honeycomb_Entities())
            {
                var MessageTemplate = context.MessageTemplate.Where(mt => mt.ID == (long)MessageType).FirstOrDefault();

                if (MessageTemplate != null)
                {
                    returnStr = MessageTemplate.Message;
                }
            }

            return returnStr;
        }

        #endregion

		#region Image Utilities
		public static Byte[] Resize(Byte[] input, int width, int height) {
			MemoryStream output = new MemoryStream();
			Resize(new MemoryStream(input), output, width, height);
			return output.ToArray();
		}

		public static void Resize(Stream input, Stream output, int width, int height) {
			ImageCodecInfo myImageCodecInfo;
			System.Drawing.Imaging.Encoder myEncoder;
			EncoderParameter myEncoderParameter;
			EncoderParameters myEncoderParameters;

			// Get an ImageCodecInfo object that represents the JPEG codec.
			myImageCodecInfo = GetEncoderInfo("image/jpeg");
			// Create an Encoder object based on the GUID
			myEncoder = System.Drawing.Imaging.Encoder.Quality;
			// Create an EncoderParameters object.
			myEncoderParameters = new EncoderParameters(1);
			// Save the bitmap as a JPEG file with quality level 75.
			myEncoderParameter = new EncoderParameter(myEncoder, 85L);
			myEncoderParameters.Param[0] = myEncoderParameter;

			using (var image = Image.FromStream(input)) {

				var orientationBA = image.PropertyItems.Where(pi => pi.Id == 274).Select(s => s.Value).FirstOrDefault();

				if (orientationBA != null) {
					if (orientationBA.Length > 0) {
						//check the orientation and rotate image if neccesary
						int orientationInd = (int)orientationBA[0];

						switch (orientationInd) {
							case 3:
							case 4:
								image.RotateFlip(RotateFlipType.Rotate180FlipNone);
								break;
							case 5:
							case 6:
								image.RotateFlip(RotateFlipType.Rotate90FlipNone);
								break;
							case 7:
							case 8:
								image.RotateFlip(RotateFlipType.Rotate270FlipNone);
								break;
							default:
								break;
						}
					}
				}

				// Figure out the ratio
				double ratioX = (double)width / (double)image.Width;
				double ratioY = (double)height / (double)image.Height;
				// use whichever multiplier is smaller
				double ratio = ratioX < ratioY ? ratioX : ratioY;

				// now we can get the new height and width
				int newHeight = Convert.ToInt32(image.Height * ratio);
				int newWidth = Convert.ToInt32(image.Width * ratio);

				var bmp = new Bitmap(newWidth, newHeight);
				var gr = Graphics.FromImage(bmp);

				gr.CompositingQuality = CompositingQuality.HighSpeed;
				gr.SmoothingMode = SmoothingMode.HighSpeed;
				gr.InterpolationMode = InterpolationMode.HighQualityBicubic;

				gr.DrawImage(image, 0, 0, newWidth, newHeight);

				//gr.DrawImage(image, new Rectangle(0, 0, width, height));
				bmp.Save(output, myImageCodecInfo, myEncoderParameters);

				bmp.Dispose();
				gr.Dispose();
			}
		}

		private static ImageCodecInfo GetEncoderInfo(String mimeType) {
			int j;
			ImageCodecInfo[] encoders;
			encoders = ImageCodecInfo.GetImageEncoders();
			for (j = 0; j < encoders.Length; ++j) {
				if (encoders[j].MimeType == mimeType)
					return encoders[j];
			}
			return null;
		}
		#endregion

		#region String Utilities
		public static string CSVCellSanitise(string csv) {
			return "\"" + csv.Replace("\"", "\"\"") + "\"";
		}
		#endregion
	}
}
