﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Honeycomb.Custom;

namespace Honeycomb.AccessControl {
    [ServiceContract(Namespace="AccessControl")]
    public interface Impro {

        [OperationContract]
        EntityTransport<Custom.Data.EMPLOYEE> GetUsers();

        [OperationContract]
        EntityTransport<Custom.Data.EMPLOYEE> AddUsers(Custom.Data.EMPLOYEE userData);

        [OperationContract]
        EntityTransport<Custom.Data.EMPLOYEE> CreateUser(Custom.Data.EMPLOYEE userData);

        [OperationContract]
        EntityTransport<Custom.Data.EMPLOYEE> UpdateUser(Custom.Data.EMPLOYEE userData);

        [OperationContract]
        EntityTransport<Custom.Data.EMPLOYEE> DeleteUser(Custom.Data.EMPLOYEE userData);
    }
}
