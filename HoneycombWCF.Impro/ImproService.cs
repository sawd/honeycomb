﻿#region Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Honeycomb.Custom;
using System.Data.Linq;
using System.Data.Objects.DataClasses;
#endregion

namespace Honeycomb.AccessControl {

    public class ImproWCF : Impro {


        public EntityTransport<Custom.Data.EMPLOYEE> GetUsers() {
            var users = new List<Custom.Data.EMPLOYEE>();
            try {
                using (var context = new Custom.Data.ImproEntities()) {

                    users = (from userData in context.EMPLOYEE
                             select userData).Take(1000).ToList();

                    return Utility.ServiceResponse(ref users);
                }
            } catch (Exception ex) {

                return Utility.ServiceResponse(ref users, ex);
            }
        }

        public EntityTransport<Custom.Data.EMPLOYEE> AddUsers(Custom.Data.EMPLOYEE userData) {

            try {
                using (var context = new Custom.Data.ImproEntities()) {
                    context.EMPLOYEE.Add(userData);
                    context.SaveChanges();
                    return Utility.ServiceResponse(ref userData);
                }
            } catch (Exception ex) {

                return Utility.ServiceResponse(ref userData, ex);
            }
        }

        public EntityTransport<Custom.Data.EMPLOYEE> CreateUser(Custom.Data.EMPLOYEE userData) {
            try {
                using (var context = new Custom.Data.ImproEntities()) {
                    context.EMPLOYEE.Add(userData);
                    context.SaveChanges();
                    return Utility.ServiceResponse(ref userData);
                }
            } catch (Exception ex) {

                return Utility.ServiceResponse(ref userData, ex);
            }
        }

        public EntityTransport<Custom.Data.EMPLOYEE> UpdateUser(Custom.Data.EMPLOYEE userData) {
            throw new NotImplementedException();
        }

        public EntityTransport<Custom.Data.EMPLOYEE> DeleteUser(Custom.Data.EMPLOYEE userData) {
            throw new NotImplementedException();
        }
    }
}
