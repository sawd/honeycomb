﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Honeycomb.API.Common {
    
    public class APIContainer {
        /// <summary>
        /// Key provided by Juice software administrator
        /// </summary>
        public string APIKey { get; set; }
        /// <summary>
        /// Requesting server IP
        /// </summary>
        public string ClientIP { get; set; }
        /// <summary>
        /// SessionID returned from the Juice System
        /// </summary>
        public string APISessionID { get; set; }

        public System.Web.SessionState.HttpSessionState APISession { get; set; }

        ///// <summary>
        ///// Class Constructor
        ///// </summary>
        //public void APIContainer() {

        //}

    }

}