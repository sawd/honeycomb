﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using Honeycomb.Custom.Data;
using Honeycomb.Custom;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace Honeycomb.API.Common {
    
    public static class Utils {

        public static string GetRequestIP() {
            string currentIP;
            if (HttpContext.Current != null) {
                currentIP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            } else {
                var req = OperationContext.Current.RequestContext;
                currentIP = ((RemoteEndpointMessageProperty)OperationContext.Current.IncomingMessageProperties[RemoteEndpointMessageProperty.Name]).Address;
            }
            return currentIP;
        }


        public static bool CheckRequest(string APIKey) {
            bool retval = true;

            //if (Cache.CacheManager.APIContainer.Count > 0) {
            //    List<APIContainer> currentCache = Cache.CacheManager.APIContainer;
            //    var currentData = currentCache.Where(jsc => jsc.APISessionID == SessionManager.SessionID).FirstOrDefault();

            //    if (currentData != null) {
            //        if (currentData.ClientIP == GetRequestIP() && currentData.APIKey == APIKey) {
            //            retval = true;
            //        } else {
            //            throw new Exception("Request does not match the API data.");
            //        }
            //    }
            //}

            return retval;
        }

    }

}