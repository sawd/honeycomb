﻿using System.Collections.Generic;
using System;
using System.Web;
using Honeycomb.API.Common;

using System.Linq;

namespace Honeycomb.API.Cache {

    public partial class CacheManager : SAWD.WebManagers.CacheManagerBase {
       
        public HttpContext Context { get; set; }

        /// <summary>
        /// A cached list of the system's accessing the API
        /// </summary>
        public static List<APIContainer> APIContainer {
            get {
                List<APIContainer> retval = (List<APIContainer>)(GetCacheApp("API", "APIContainer", null));
                if (retval == null) {
                    retval = new List<APIContainer>();
                    SetCacheApp("API", "APIContainer", retval);
                }
                return retval;
            }
            set {
                TimeSpan span = new TimeSpan(1, 0, 0, 0);
                SetCacheApp("API", "APIContainer", value, span);
            }
        }

        /// <summary>
        /// A cached list of users 
        /// </summary>
        public static List<Honeycomb.Custom.Data.User> APIUsers {
            get {
                List<Honeycomb.Custom.Data.User> retval = (List<Honeycomb.Custom.Data.User>)(GetCacheApp("API", "APIUsers", null));
                if (retval == null) {
                    var context = new Custom.Data.Honeycomb_Entities();
                    retval = context.User.Where(usr => usr.DeletedOn == null).ToList();
                    SetCacheApp("API", "APIUsers", retval);
                }
                return retval;
            }
            set {
                TimeSpan span = new TimeSpan(0, 1, 0, 0);
                SetCacheApp("API", "APIUsers", value, span);
            }
        }

        public static List<Honeycomb.Custom.Data.Occupant> APIOccupants {
            get {
                List<Honeycomb.Custom.Data.Occupant> retval = (List<Honeycomb.Custom.Data.Occupant>)(GetCacheApp("API", "APIOccupants", null));
                if (retval == null) {
                    var context = new Custom.Data.Honeycomb_Entities();
                    retval = (from occupant in context.Occupant
                         join propertyOccupant in context.PropertyOccupant on occupant.ID equals propertyOccupant.OccupantID
                         where propertyOccupant.ArchivedOn == null
						 && propertyOccupant.Occupant.IntranetUsername != null
                         select occupant).ToList();
                    SetCacheApp("API", "APIOccupants", retval);
                }
                return retval;
            }
            set {
                TimeSpan span = new TimeSpan(0, 1, 0, 0);
                SetCacheApp("API", "APIOccupants", value, span);
            }
        }

        //public static void SetCacheDymanic(string name, object value, TimeSpan cacheexpiry) {
        //    SetCacheApp("API", name, value, cacheexpiry);
        //}

        //public static object GetCacheDynamic(string name) {
        //    return GetCacheApp("API", name, null);
        //}
    }

}
