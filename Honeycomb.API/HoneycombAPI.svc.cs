﻿using Honeycomb.API.Common;
using Honeycomb.Custom;
using Honeycomb.Custom.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;

namespace Honeycomb.API {

	[ServiceContract(Namespace = "HoneycombAPI")]
	[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
	public class HoneycombAPI {

		private Custom.Data.Honeycomb_Entities context = new Custom.Data.Honeycomb_Entities();
		
		/// <summary>
		/// Authenticate a Honeycomb API user
		/// </summary>
		/// <param name="UserName"></param>
		/// <param name="Password"></param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		public async Task<string> Authenticate(string APIUserName, string APIPassword, string APIKey) {

			string temporaryToken = Guid.NewGuid().ToString();
			string currentIP = Honeycomb.API.Common.Utils.GetRequestIP();

			string retVal = null;
			List<APIContainer> currentCache = Cache.CacheManager.APIContainer;
			var currentData = currentCache.Where(jsc => jsc.ClientIP == currentIP && jsc.APIKey == APIKey).FirstOrDefault();

			Honeycomb.Security.WCFSecurity security = new Security.WCFSecurity();
			retVal = security.LoginAPI(APIUserName, APIPassword, APIKey);

			if(retVal != null) {
				//check if client has authenticated in the past
				if(currentData == null) {
					currentCache.Add(new APIContainer { APIKey = APIKey, ClientIP = currentIP, APISessionID = retVal });
				} else {
					if(currentData.APISessionID != retVal) {
						currentData.APISessionID = retVal;
					}
				}
				Cache.CacheManager.APIContainer = currentCache;
			} else {
				temporaryToken = null;
			}
			return retVal;
		}

		/// <summary>
		/// Authenticate a Honeycomb user
		/// </summary>
		/// <param name="UserName"></param>
		/// <param name="Password"></param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		public async Task<bool> AuthenticateUser(string SecurityToken, string IntranetUsername) {
			Utils.CheckRequest(SecurityToken);

			bool retVal = false;
			try {
				var user = Honeycomb.API.Cache.CacheManager.APIUsers.Where(usr => usr.UserName == IntranetUsername).FirstOrDefault();
				if(user == null) {
					throw new Exception("IntranetUsername incorrect or does not match.");
				} else {
					retVal = true;
				}
				retVal = true;
			} catch {
				retVal = false;
			}

			return retVal;
		}

		/// <summary>
		/// Authenticate a Honeycomb occupant
		/// </summary>
		/// <param name="UserName"></param>
		/// <param name="Password"></param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		public async Task<CustomOccupant> AuthenticateOccupant(string SecurityToken, string IntranetUsername, string IntranetPassword) {
			Utils.CheckRequest(SecurityToken);

			EntityTransport<CustomOccupant> retVal = null;
			var occupant = Honeycomb.API.Cache.CacheManager.APIOccupants.Where(occ => occ.IntranetUsername.ToLower() == IntranetUsername.ToLower() && occ.IntranetPassword == Honeycomb.Security.Encryption.Encrypt(IntranetPassword)).FirstOrDefault();
			if(occupant == null) {
				throw new Exception("IntranetUsername incorrect or does not match.");
			} else {
				//this will make sure that the built is security executes
				Honeycomb.Security.WCFSecurity security = new Honeycomb.Security.WCFSecurity();
				retVal = security.GetOccupant(occupant.ID);
			}

			return retVal.EntityList.FirstOrDefault();
		}

		/// <summary>
		/// Authenticate a Honeycomb occupant
		/// </summary>
		/// <param name="UserName"></param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		public async Task<CustomOccupant> ForgottenPasswordOccupant(string SecurityToken, string IntranetUsername) {
			Utils.CheckRequest(SecurityToken);

			EntityTransport<CustomOccupant> retVal = null;
			var occupant = Honeycomb.API.Cache.CacheManager.APIOccupants.Where(occ => occ.IntranetUsername.ToLower() == IntranetUsername.ToLower()).FirstOrDefault();
			if(occupant == null) {
				throw new Exception("IntranetUsername incorrect or does not match.");
			} else {
				//this will make sure that the built is security executes
				Honeycomb.Security.WCFSecurity security = new Honeycomb.Security.WCFSecurity();
				retVal = security.GetOccupant(occupant.ID);
				retVal.EntityList[0].Address = Honeycomb.Security.Encryption.Decrypt(occupant.IntranetPassword);
			}

			return retVal.EntityList.FirstOrDefault();
		}

		/// <summary>
		/// Retrieve the URL for a Honeycomb occupant's account
		/// </summary>
		/// <param name="OccupantID"></param>
		/// <returns>Returns a string containing a url for the image of a honeycomb occupant, or null if none can be found.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		public async Task<Byte[]> RetrieveOccupantImage(string SecurityToken, long OccupantID) {
			Utils.CheckRequest(SecurityToken);

			EntityTransport<Byte[]> retVal = null;
			var occupant = Honeycomb.API.Cache.CacheManager.APIOccupants.Where(occ => occ.ID == OccupantID).FirstOrDefault();
			if(occupant == null) {
				throw new Exception("IntranetUser incorrect or does not match.");
			} else {
				//this will make sure that the built is security executes
				Honeycomb.Security.WCFSecurity security = new Honeycomb.Security.WCFSecurity();
				retVal = security.GetOccupantImage(occupant.ID);
			}

			return (retVal.EntityList.FirstOrDefault());
		}

		#region GateControl / Cancom

		/// <summary>
		/// Authenticate a Honeycomb Guard
		/// </summary>
		/// <param name="UserName"></param>
		/// <param name="Password"></param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		public async Task<long> AuthenticateGuard(string SecurityToken, string Username, string Password) {
			Utils.CheckRequest(SecurityToken);
			long retVal = -1;
			try {

				var user = Honeycomb.API.Cache.CacheManager.APIUsers.Where(usr => usr.UserName == Username && usr.Password == Honeycomb.Security.Encryption.Encrypt(Password)).FirstOrDefault();
				if(user == null) {
					throw new Exception("Username or PAssword incorrect or does not match.");
				} else {
					//NOTE: This code is repeated in the Membership provider 
					var userRoles = context.Role.Where(r => r.User.Any(u => u.ID == user.ID)).Select(sel => sel.Name).ToList<string>();
					user = context.User.Where(u => u.ID == user.ID).FirstOrDefault();
					//if the user is a gate guard, make sure they have a gate assigned to them, otherwise do not allow login
					if(userRoles.Contains("SecurityGateControl") && user.GateID == null) {
						throw new Exception("No gate assigned, you cannot login until you have been assigned a gate. Please contact the control room.");
					}
					retVal = user.ID;
				}
				retVal = user.ID;
			} catch {
				retVal = -1;
			}
			return retVal;
		}

		/// <summary>
		/// Check an access code
		/// </summary>
		/// <param name="SecurityToken"></param>
		/// <param name="Code"></param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		public async Task<VisitorAndAcccessCode> GetAccessCodeForToday(string SecurityToken, string Code) {
			Utils.CheckRequest(SecurityToken);

			EntityTransport<VisitorAndAcccessCode> retVal = null;
			Honeycomb.Security.WCFSecurity sec = new Security.WCFSecurity();
			retVal = sec.GetAccessCodeForToday(Code);

			return retVal.EntityList.FirstOrDefault();
		}

		/// <summary>
		/// Capture Entry for Access Code
		/// </summary>
		/// <param name="SecurityToken"></param>
		/// <param name="AccessCodeID"></param>
		/// <param name="VechicleRegIn"></param>
		/// <param name="VechicleOccupantIn"></param>
		/// <param name="GuardID"></param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		public async Task<bool> CaptureEntryForAccessCode(string SecurityToken, Guid AccessCodeID, string VechicleRegIn, int VechicleOccupantIn, long GuardID, string entryXML = "") {
			Utils.CheckRequest(SecurityToken);
			
			EntityTransport<bool> retVal = null;
			Honeycomb.Security.WCFSecurity sec = new Security.WCFSecurity();
			retVal = sec.CaptureEntryForAccessCode(AccessCodeID, VechicleRegIn, VechicleOccupantIn, GuardID, entryXML);

			return retVal.EntityList.FirstOrDefault();
		}

		/// <summary>
		/// Capture Exit for Access Code
		/// </summary>
		/// <param name="SecurityToken"></param>
		/// <param name="AccessCodeID"></param>
		/// <param name="VehicleRegOut"></param>
		/// <param name="VehicleOccupantOut"></param>
		/// <param name="GuardID"></param>
		/// <param name="ReasonForDiscrepancy"></param>
		/// <param name="lateExitReason"></param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		public async Task<bool> CaptureExitForAccessCode(string SecurityToken, Guid AccessCodeID, string VehicleRegOut, int VehicleOccupantOut, long GuardID, string ReasonForDiscrepancy = "", string lateExitReason = "", string exitXML = "") {
			Utils.CheckRequest(SecurityToken);
			
			EntityTransport<bool> retVal = null;
			Honeycomb.Security.WCFSecurity sec = new Security.WCFSecurity();
			retVal = sec.CaptureExitForAccessCode(AccessCodeID, VehicleRegOut, VehicleOccupantOut, GuardID, ReasonForDiscrepancy, lateExitReason, exitXML);

			if(!string.IsNullOrEmpty(retVal.Message)) {
				throw new Exception(retVal.Message);
			}

			return retVal.EntityList.FirstOrDefault();
		}

		#region Supplier Access
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		public async Task<bool> GuardHasGate(string SecurityToken, long guardID) {
			Utils.CheckRequest(SecurityToken);
			var isValid = true;

			try {
				Honeycomb.Security.WCFSecurity.CheckGuardHasGate(guardID);
			}catch(Exception ex){
				isValid = false;
			}

			return isValid;
		}

		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		public async Task<ProcessResponse> CaptureSupplierEntry(string SecurityToken, SupplierAccess supplierAccessEntry, long guardID) {
			Utils.CheckRequest(SecurityToken);

			Honeycomb.Security.WCFSecurity sec = new Security.WCFSecurity();
			var retVal = sec.CaptureSupplierEntry(supplierAccessEntry, guardID).EntityList.First();

			return retVal;
		}

		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		public async Task<SupplierAccessExit> GetSupplierAccessForExit(string SecurityToken, string vehicleRegistrationNumber, long guardID) {
			Utils.CheckRequest(SecurityToken);
			Honeycomb.Security.WCFSecurity sec = new Security.WCFSecurity();
			var retVal = sec.GetSupplierAccessForExit(vehicleRegistrationNumber, guardID).EntityList.First();
			return retVal;
		}

		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		public async Task<ProcessResponse> CaptureSupplierExit(string SecurityToken, long supplierAccessID, int numberOfOccupants, long guardID) {
			Utils.CheckRequest(SecurityToken);
			Honeycomb.Security.WCFSecurity sec = new Security.WCFSecurity();
			var retVal = sec.CaptureSupplierExit(supplierAccessID, numberOfOccupants, guardID).EntityList.First();
			return retVal;
		}
		#endregion

		/// <summary>
		/// Get a list of Occupants matching on Name or Surname like SearchString
		/// </summary>
		/// <param name="SecurityToken"></param>
		/// <param name="SearchString"></param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		public async Task<List<AutoCompleteObject>> GetOccupants(string SecurityToken, string SearchString) {
			Utils.CheckRequest(SecurityToken);

			List<AutoCompleteObject> retVal = null;
			Honeycomb.Security.WCFSecurity sec = new Security.WCFSecurity();
			retVal = sec.GetActiveOccupants(SearchString).EntityList;

			return retVal;
		}

		/// <summary>
		/// Get the Occupants details
		/// </summary>
		/// <param name="SecurityToken"></param>
		/// <param name="OccupantID"></param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		public async Task<CustomOccupant> GetOccupant(string SecurityToken, long OccupantID) {
			Utils.CheckRequest(SecurityToken);

			EntityTransport<CustomOccupant> retVal = null;
			Honeycomb.Security.WCFSecurity sec = new Security.WCFSecurity();
			retVal = sec.GetOccupant(OccupantID);

			return retVal.EntityList.FirstOrDefault();
		}

		#endregion

		#region Access Codes
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		public async Task<List<Custom.Data.GuestList>> GetGuestList(string SecurityToken, long OccupentID) {
			Utils.CheckRequest(SecurityToken);
			Honeycomb.Security.WCFSecurity _wcf = new Honeycomb.Security.WCFSecurity();
			var occupantGuestLists = _wcf.GetOccupantGuestLists(OccupentID).EntityList;
			return occupantGuestLists;
		}

		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		public async Task<List<Custom.Data.GuestListVisitor>> GetVisitorsByList(string SecurityToken, long GuestListID) {
			Utils.CheckRequest(SecurityToken);
			Honeycomb.Security.WCFSecurity _wcf = new Honeycomb.Security.WCFSecurity();
			var VisitorList = _wcf.GetVisitorsByList(GuestListID).EntityList;
			return VisitorList;
		}

		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		public async Task<long> SaveGuestList(string SecurityToken, Custom.Data.GuestList GuestList, List<Custom.Data.GuestListVisitor> VisitorsList) {
			Utils.CheckRequest(SecurityToken);
			Honeycomb.Security.WCFSecurity _wcf = new Honeycomb.Security.WCFSecurity();
			var GuestListID = _wcf.SaveGuestList(GuestList, VisitorsList).EntityList[0];
			return GuestListID;
		}

		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		public async Task<List<bool>> DeleteGuestList(string SecurityToken, long GuestListID) {
			Utils.CheckRequest(SecurityToken);
			Honeycomb.Security.WCFSecurity _wcf = new Honeycomb.Security.WCFSecurity();
			var DeleteSuccess = _wcf.DeleteGuestList(GuestListID).EntityList;
			return DeleteSuccess;
		}

		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		public async Task<List<VisitorAccessCodes>> CreateAccessCodes(string SecurityToken, List<AccessVisitor> visitors, long occupantID, DateTime? dateValid = null, bool sendToOccupant = false, bool sendToVisitor = false) {
			Utils.CheckRequest(SecurityToken);
			Honeycomb.Security.WCFSecurity wcf = new Honeycomb.Security.WCFSecurity();
			var visitorAccessCodes = wcf.CreateAccesCodes(visitors, occupantID, dateValid, sendToOccupant, sendToVisitor, true).EntityList;
			return visitorAccessCodes;
		}

		/// <summary>
		/// Please note this function can bypass normal security roles and settings because there is no need to have a secure call for it
		/// </summary>
		/// <param name="securityToken"></param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		public async Task<SystemSetting> GetSystemSettings(string securityToken) {
			Utils.CheckRequest(securityToken);

			SystemSetting settings = new SystemSetting();

			using(var context = new Honeycomb_Entities()){
				settings = context.SystemSetting.Where(s => s.SystemID == CacheManager.SystemID).FirstOrDefault();
			}

			return settings;
		}
		#endregion

		#region Venue Booking
		/// <summary>
		/// Retrieves a list of Venues
		/// </summary>
		/// <param name="SecurityToken"></param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		public async Task<List<Venue>> GetVenueList(string SecurityToken) {
			Utils.CheckRequest(SecurityToken);
			EntityTransport<Venue> retVal = null;
			Honeycomb.Admin.WCFAdmin admin = new Honeycomb.Admin.WCFAdmin();
			retVal = admin.GetVenues(true);
			return retVal.EntityList;
		}

		/// <summary>
		/// Retrieves a list of bookings for a particular Venue and Date
		/// </summary>
		/// <param name="SecurityToken"></param>
		/// <param name="VenueID"></param>
		/// <param name="StartDate"></param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		public async Task<List<CustomCalendarEvent>> GetBookingListByDates(string SecurityToken, long VenueID, DateTime StartDate, DateTime EndDate) {
			Utils.CheckRequest(SecurityToken);

			EntityTransport<CustomCalendarEvent> retVal = null;
			Honeycomb.Admin.WCFAdmin admin = new Honeycomb.Admin.WCFAdmin();
			retVal = admin.GetBookingListByDates(VenueID, StartDate, EndDate);
			return retVal.EntityList;
		}


		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		public async Task<List<CustomIntanetBooking>> GetUpcomingBookings(string SecurityToken, long OccupantID) {
			Utils.CheckRequest(SecurityToken);

			EntityTransport<CustomIntanetBooking> retVal = null;
			Honeycomb.Admin.WCFAdmin admin = new Honeycomb.Admin.WCFAdmin();
			retVal = admin.GetUpcomingBookings(OccupantID);
			return retVal.EntityList;
		}

		/// <summary>
		/// Retrieves a list of bookings for a particular Venue and Date
		/// </summary>
		/// <param name="SecurityToken"></param>
		/// <param name="VenueID"></param>
		/// <param name="StartDate"></param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		public async Task<List<CustomCalendarEvent>> UpdateBooking(string SecurityToken, VenueBooking newBooking) {
			Utils.CheckRequest(SecurityToken);

			EntityTransport<CustomCalendarEvent> retVal = null;
			Honeycomb.Admin.WCFAdmin admin = new Honeycomb.Admin.WCFAdmin();
			retVal = admin.UpdateBooking(newBooking, false);
			return retVal.EntityList;
		}

		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		public async Task<List<CustomCalendarEvent>> CancelBooking(string SecurityToken, long BookingID) {
			Utils.CheckRequest(SecurityToken);

			//collect venuebooking
			VenueBooking booking = context.VenueBooking.Where(v => v.VenueBookingID == BookingID).FirstOrDefault();

			EntityTransport<CustomCalendarEvent> retVal = null;
			Honeycomb.Admin.WCFAdmin admin = new Honeycomb.Admin.WCFAdmin();
			retVal = admin.UpdateBooking(booking, true);
			return retVal.EntityList;
		}

		/// <summary>
		/// Retrieves a list of future bookings made in the name of the occupant specified by ID
		/// </summary>
		/// <param name="SecurityToken"></param>
		/// <param name="OccupantID"></param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		public async Task<List<VenueBooking>> GetBookingsByOccupantID(string SecurityToken, long OccupantID) {
			Utils.CheckRequest(SecurityToken);

			EntityTransport<VenueBooking> retVal = null;
			Honeycomb.Admin.WCFAdmin admin = new Honeycomb.Admin.WCFAdmin();
			retVal = admin.GetBookingsByOccupantID(OccupantID);
			return retVal.EntityList;
		}

		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		public async Task<VenueBooking> GetBooking(string SecurityToken, long BookingID) {
			Utils.CheckRequest(SecurityToken);

			EntityTransport<VenueBooking> retVal = null;
			Honeycomb.Admin.WCFAdmin admin = new Honeycomb.Admin.WCFAdmin();
			retVal = admin.GetBooking(BookingID);
			return retVal.EntityList.FirstOrDefault();
		}

		#endregion
	}
}
