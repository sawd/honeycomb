﻿#region Directives
using System;
using System.Collections.Generic;
using System.Data;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Configuration;
using Honeycomb.Custom;
using Honeycomb.Custom.Data;
#endregion

namespace Honeycomb.SMS
{
    [ServiceContract(Namespace = "SMS")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class SMSService
    {
        private string userName = ConfigurationManager.AppSettings["PortalUserName"];
        private string password = ConfigurationManager.AppSettings["PortalPassword"];

        /// <summary>
        /// Sends an sms to a valid user within the estate management system
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns>A custom poco which indicates whether the send was successful, in the poco will be two other properties, a commengine message id and a event id, if one is null it means that the other method was used.</returns>
        [OperationContract]
        public SMSSendReturn SendSMS(string message, string mobileNumber)
        {
            SMSSendReturn smsReturn = new SMSSendReturn();
            DataSet dsSend = new DataSet();
            using (Honeycomb.Custom.MobileAPI.APISoapClient mobileAPI = new Honeycomb.Custom.MobileAPI.APISoapClient("APISoap"))
            {
                using (var context = new Custom.Data.Honeycomb_Entities())
                {

                    if (true)
                    {

                        try
                        {

                            mobileAPI.Open();
                            #region XML node declaration
                            XmlDocument messageEnvelope = new XmlDocument();
                            XmlElement root = messageEnvelope.CreateElement("senddata");
                            XmlElement isLive = messageEnvelope.CreateElement("live");
                            XmlElement settings = messageEnvelope.CreateElement("settings");
                            XmlElement entries = messageEnvelope.CreateElement("entries");
                            XmlElement defaultDate = messageEnvelope.CreateElement("default_date");
                            XmlElement defaultTime = messageEnvelope.CreateElement("default_time");
                            XmlElement numTo = messageEnvelope.CreateElement("numto");
                            XmlElement customerID = messageEnvelope.CreateElement("customerid");
                            XmlElement messageData = messageEnvelope.CreateElement("data1");
                            #endregion

                            #region Populate Nodes
                            isLive.InnerText = ConfigurationManager.AppSettings["SendSMS"];
                            defaultDate.InnerText = DateTime.Now.ToString("dd/MMM/yyyy");
                            defaultTime.InnerText = DateTime.Now.ToString("HH:mm");
                            numTo.InnerText = mobileNumber;
                            customerID.InnerText = CacheManager.SystemID.ToString();
                            messageData.InnerText = message;
                            #endregion

                            #region Append Nodes
                            settings.AppendChild(isLive);
                            settings.AppendChild(defaultDate);
                            settings.AppendChild(defaultTime);

                            entries.AppendChild(numTo);
                            entries.AppendChild(customerID);
                            entries.AppendChild(messageData);

                            root.AppendChild(settings);
                            root.AppendChild(entries);

                            messageEnvelope.AppendChild(root);
                            #endregion

                            dsSend = mobileAPI.Send_STR_DS(userName, password, messageEnvelope.OuterXml);

                            if (dsSend.Tables.Contains("call_result"))
                            {
                                if (dsSend.Tables["call_result"].Rows[0].ItemArray[0].ToString().ToLower() == "true")
                                {
                                    //the result is true therefore the sms was sent
                                    smsReturn.Sent = true;

                                    if (dsSend.Tables.Contains("send_info"))
                                    {
                                        smsReturn.EventID = Int32.Parse((dsSend.Tables["send_info"].Rows[0].ItemArray[0]).ToString());
                                    }

                                }
                                else
                                {
                                    smsReturn.Sent = false;
                                    smsReturn.Message = dsSend.Tables["call_result"].Rows[0].ItemArray[1].ToString();
                                    if (smsReturn.Message.Contains("No data to send"))
                                    {
                                        smsReturn.Message = "Cellphone number not valid.";
                                    }
                                }
                            }

                            mobileAPI.Close();

                        }
                        catch (TimeoutException ex)
                        {
                            //return that the method failed and send the sms to the comm engine for sending
                            string XMLData = "<xmldata><users><user cell='" + mobileNumber + "' /></users></xmldata>";
                            smsReturn.CommEngineID = context.CreateCommEngineSMS(message, "0825058592", XMLData, CacheManager.CommEngineID, null).FirstOrDefault();
                            smsReturn.Sent = false;
                            smsReturn.Message = "A timeout occurred when attempting to send the sms. The message was queued for sending.";
                            //abort the api connection
                            mobileAPI.Abort();
                        }
                        catch (CommunicationException ex)
                        {
                            //return that the method failed and send the sms to the comm engine for sending
                            string XMLData = "<xmldata><users><user cell='" + mobileNumber + "' /></users></xmldata>";
							smsReturn.CommEngineID = context.CreateCommEngineSMS(message, "0825058592", XMLData, CacheManager.CommEngineID, null).FirstOrDefault();
                            smsReturn.Sent = false;
                            smsReturn.Message = "A connection error occurred while attempting to send the sms. The message was queued for sending.";
                            //abort the api connection
                            mobileAPI.Abort();
                        }

                        //call sent sms table
                        long lastChangeID = Utility.ConvertNull<long>(context.SMSSent.Select(sent => sent.ChangeID).Max(), 0);

                        GetAllSentSMS(lastChangeID);
                    }

                }
            }
            return smsReturn;
        }
        /// <summary>
        /// Sends SMS 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="sendToOwner"></param>

        /// <returns></returns>
        [OperationContract]
        public EntityTransport<ProcessResponse> SendBulkSMS(string SMSmessage, string sendTo)
        {

            ProcessResponse pRes = new ProcessResponse();
            bool sendToAll = false, sendToOwners = false, sendToTenants = false, sendToOccupants = false;

            if(sendTo.Contains("All")) {
                sendToAll = true;
            }

            if(sendTo.Contains("Owner")) {
                sendToOwners = true;
            }

            if(sendTo.Contains("Tenant")) {
                sendToTenants = true;
            }

            if(sendTo.Contains("Occupant")) {
                sendToOccupants = true;
            }

            using (var context = new Custom.Data.Honeycomb_Entities())
            {
                var fromCellNumber = context.SystemSetting.Where(y => y.SystemID == CacheManager.SystemID).Select(x => x.FromCellNumber).SingleOrDefault();

                try
                {
                    var messageID = context.SendOccupantBulkSMS(SMSmessage, CacheManager.CommEngineID,sendToAll,sendToOwners, sendToTenants,sendToOccupants);

                    return Utility.ServiceResponse(ref pRes);
                }
                catch (Exception ex)
                {

                    return Utility.ServiceResponse(ref pRes, ex);
                }
            }
        }

        /// <summary>
        ///  Gets a report of all messages sent based on a sentID, use 0 to get all records or specify an ID 
        ///  to seed from
        /// </summary>
        /// <param name="ChangeID"></param>
        /// <returns></returns>
        [OperationContract]
        public DataSet GetAllSentSMS(long ChangeID)
        {
            using (Honeycomb.Custom.MobileAPI.APISoapClient mobileAPI = new Honeycomb.Custom.MobileAPI.APISoapClient("APISoap"))
            {
                mobileAPI.Open();
                #region XML node declaration
                XmlDocument messageEnvelope = new XmlDocument();
                XmlElement root = messageEnvelope.CreateElement("sent");
                XmlElement settings = messageEnvelope.CreateElement("settings");
                XmlElement id = messageEnvelope.CreateElement("id");

                XmlElement maxItems = messageEnvelope.CreateElement("max_recs");
                XmlElement columns = messageEnvelope.CreateElement("cols_returned");
                XmlElement dateFormat = messageEnvelope.CreateElement("date_format");
                #endregion

                #region Populate Nodes
                id.InnerText = ChangeID.ToString();
                columns.InnerText = "sentid,eventid,smstype,numto,data,flash,customerid,status,statusdate";
                dateFormat.InnerText = "dd-MMM-yyyy HH:ss";
                maxItems.InnerText = "10";
                #endregion

                settings.AppendChild(id);
                settings.AppendChild(columns);
                settings.AppendChild(dateFormat);
                settings.AppendChild(maxItems);

                root.AppendChild(settings);
                messageEnvelope.AppendChild(root);

                DataSet dsResult = mobileAPI.Sent_STR_DS(userName, password, messageEnvelope.OuterXml);
                mobileAPI.Close();

                using (var context = new Custom.Data.Honeycomb_Entities())
                {
                    //write a record to honeycomb
                    DateTime statusDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                    foreach (DataRow record in dsResult.Tables[0].Rows)
                    {
                        if (record["customerid"].ToString() != "NA")
                        {
                            context.SMSSent.Add(new Custom.Data.SMSSent
                            {
                                ChangeID = long.Parse(record["changeid"].ToString()),
                                CustomerID = CacheManager.SystemID,
                                Data = Utility.ConvertNull(record["data"], string.Empty),
                                EventID = long.Parse(record["eventid"].ToString()),
                                Flash = Utility.ConvertNull(record["flash"], false),
                                NumberTo = long.Parse(record["numto"].ToString()),
                                SentID = long.Parse(record["sentid"].ToString()),
                                SMSType = Utility.ConvertNull(record["smstype"], string.Empty),
                                Status = Utility.ConvertNull(record["status"], string.Empty),
                                StatusDate = Utility.ConvertNull(record["statusdate"], statusDate),
                                SystemID = CacheManager.SystemID
                            });
                        }

                    }
                    context.SaveChanges();
                }
                return dsResult;

            }
        }

        /// <summary>
        /// Gets a all replies from recipients based on a replyID, use 0 to get all records or specify an ID 
        /// to seed from
        /// </summary>
        /// <param name="replyID"></param>
        /// <returns></returns>
        [OperationContract]
        public DataSet GetAllReplySMS(int replyID)
        {
            using (Honeycomb.Custom.MobileAPI.APISoapClient mobileAPI = new Honeycomb.Custom.MobileAPI.APISoapClient("APISoap"))
            {
                mobileAPI.Open();
                #region XML node declaration
                XmlDocument messageEnvelope = new XmlDocument();
                XmlElement root = messageEnvelope.CreateElement("reply");
                XmlElement settings = messageEnvelope.CreateElement("settings");
                XmlElement id = messageEnvelope.CreateElement("id");
                XmlElement maxItems = messageEnvelope.CreateElement("max_recs");
                XmlElement columns = messageEnvelope.CreateElement("cols_returned");
                XmlElement dateFormat = messageEnvelope.CreateElement("date_format");
                #endregion

                #region Populate Nodes
                id.InnerText = replyID.ToString();
                columns.InnerText = "eventid,numfrom,receiveddata,received,sentid,sentdata,sentdatetime,sentcustomerid";
                dateFormat.InnerText = "dd-MMM-yyyy HH:ss";
                maxItems.InnerText = string.Empty;
                #endregion

                settings.AppendChild(id);
                settings.AppendChild(columns);
                settings.AppendChild(dateFormat);
                //settings.AppendChild(maxItems);

                root.AppendChild(settings);
                messageEnvelope.AppendChild(root);

                DataSet dsResult = mobileAPI.Reply_STR_DS(userName, password, messageEnvelope.OuterXml);
                mobileAPI.Close();

                return dsResult;
            }
        }
        /// <summary>
        /// Get All Undelivered smses
        /// </summary>
        /// <param name="changeid"></param>
        /// <returns></returns>
        [OperationContract]
        public EntityTransport<SMSSent> GetUndeliveredSMSes(PaginationContainer pagination, string filter = null)
        {

            var undeliveredSMSes = new List<SMSSent>();
            DateTime TodayDateCompare = new DateTime(2012, 11, 21);
            //  new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            try
            {
                using (var context = new Honeycomb_Entities())
                {
                    undeliveredSMSes = (from smses in context.SMSSent
                                        where smses.Status == "UNDELIV"
                                       || smses.Status == "UNKNOWN"

                                        select smses).ToList();
                    undeliveredSMSes = undeliveredSMSes.AsQueryable().OptionalWhere(!string.IsNullOrEmpty(filter), l => l.NumberTo.ToString().ToLower().Contains(filter.ToLower())).ToList();
                    undeliveredSMSes = undeliveredSMSes.AsQueryable().Paginate(pagination.CurrentPage, pagination.PageSize, pagination.OrderBy).ToList();
                }
                return Utility.ServiceResponse(ref undeliveredSMSes);
            }
            catch (Exception ex)
            {
                return Utility.ServiceResponse(ref undeliveredSMSes, ex);
            }
        }
        /// <summary>
        /// Send SMSes by the notification Type 
        /// </summary>
        /// <param name="notificationTypeID">notificationTypeID</param>
        /// <param name="message">SMS message </param>
        /// <returns></returns>
        [OperationContract(Name = "SendSMSByNotificationTypeID")]
        public EntityTransport<CustomBulkSMSEntity> SendSMS(long? notificationTypeID, string SMSmessage)
        {
            var NotificationCelllList = new List<CustomBulkSMSEntity>();

            using (var context = new Custom.Data.Honeycomb_Entities())
            {
                Communications comms = new Communications();
                try
                {

                    NotificationCelllList = (from person in context.NotificationPerson
                                             where person.NotificationTypeID == notificationTypeID
                                             select new CustomBulkSMSEntity
                                             {
                                                 toNumber = person.Cellphone,
                                                 Name = person.Name,
                                                 Message = SMSmessage,
                                                 FromNumber = string.Empty

                                             }).ToList<CustomBulkSMSEntity>();
                    comms.SendSMS(NotificationCelllList);
                    NotificationCelllList.Clear();
                    return Utility.ServiceResponse(ref NotificationCelllList);

                }
                catch (Exception ex)
                {
                    return Utility.ServiceResponse(ref NotificationCelllList, ex);

                }



            }



        }
    }
}
