﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SAWD.WebManagers
{
    public class SAWDHttpContext : System.Web.HttpContextBase, IContext
    {

        private HttpSession session = new HttpSession();

        public ISession Session {
            get {
                return session;
            }
        }
    }
}
