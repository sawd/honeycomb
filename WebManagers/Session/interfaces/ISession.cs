﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SAWD.WebManagers
{
    public interface ISession
    {
        void Add(string name, object value);
        object this[string name] { get; set; }
        void Clear();
        void Abandon();
        string SessionID { get; }
    }
}
