﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SAWD.WebManagers
{
    public interface IContext
    {
        ISession Session { get; }
    }
}
