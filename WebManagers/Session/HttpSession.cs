﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SAWD.WebManagers
{
    /// <summary>
    /// A wrapper for an HttpContext Session. It implements ISession so that we have a common interface for working with sessions
    /// </summary>
    public class HttpSession : ISession 
    {
        private System.Web.SessionState.HttpSessionState localSession = System.Web.HttpContext.Current.Session;

        public string SessionID { get { return localSession.SessionID; } }

        public void Add(string name, object value) {
            localSession.Add(name, value);
        }




        public object this[string name] {
            get {
                if (localSession == null) {
                    return null;
                }
                return localSession[name]; }
            set {
                if (localSession != null) {
                    localSession[name] = value;
                }
            }
        }

        /// <summary>
        /// Clears all the values in the Session.
        /// </summary>
        public void Clear() {
            localSession.Clear();
        }

        /// <summary>
        /// Abandons the session including all its values and fires the Session_End event
        /// </summary>
        public void Abandon() {
            localSession.Abandon();
        }
    }
}
