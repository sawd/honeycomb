﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SAWD.WebManagers
{
    /// <summary>
    /// A class that simulates a HttpContext Session but stores the information in the cache
    /// </summary>
    public class CacheSession : ISession
    {
        System.Web.HttpContext context = System.Web.HttpContext.Current;
        string sessionid = System.Web.HttpContext.Current.Request["sessionid"].ToString();
        List<string> cacheditems;


        public string SessionID {
            get { return sessionid; }
        }

        public List<string> CachedItemNamesList {
            get {
                cacheditems = (List<string>)(context.Cache[sessionid + "_CachedItemsList"]);
                if (cacheditems == null)
                    cacheditems = new List<string>();
                context.Cache.Add(sessionid + "_CachedItemsList", cacheditems, null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(20), System.Web.Caching.CacheItemPriority.Normal, null);
                return cacheditems;
            }
            set { cacheditems = value;
            context.Cache[sessionid + "_CachedItemsList"] = cacheditems;
            }
        }

        public CacheSession() {
        }
        
        public void Add(string name, object value) {
            CachedItemNamesList.Add(name);
            context.Cache.Add(sessionid + "_" + name, value, null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(20), System.Web.Caching.CacheItemPriority.Normal, null);
        }


        public object this[string name] {
            get { return context.Cache[sessionid + "_" + name]; }
            set { context.Cache[sessionid + "_" + name] = value; }
        }

        /// <summary>
        /// Simulates a HttpContext Session's clear method by clearing an item representing a user's session, from the Cache
        /// </summary>
        public void Clear() {
            foreach (string item in CachedItemNamesList) {
                context.Cache.Remove(sessionid + "_" + item);
            }
            context.Cache.Remove(sessionid + "_CachedItemsList");
        }

        /// <summary>
        /// In the case of the "CacheSession" class the Abandon method does exactly the same as the Clear method
        /// </summary>
        public void Abandon() {
            Clear();
        }


    }
}
