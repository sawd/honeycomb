﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SAWD.WebManagers
{
    /// <summary>
    /// Simulates an HttpContext but instead directs its session methods to an item in the system cache. This contexts is only used when we pass a sessionid parameter in the Request's querystring
    /// </summary>
    public class CacheSessionContext : IContext
    {
        
        private CacheSession localsession = new CacheSession();
        public ISession Session {
            get { return localsession; }
        }
    }
}
