﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net;

namespace Honeycomb.KeepAlive {
    class Program {

        public static void Main(string [] args) {

            HttpRequestService httpRS = new HttpRequestService();
            TimerCallback timerDelegate = new TimerCallback(httpRS.DoHttpRequest);
            Timer stateTimer = new Timer(timerDelegate, null, 10000, 10000);

            Console.WriteLine("Timers set for web address call");
            Console.ReadLine();
        }
    }

    class HttpRequestService {

        public HttpRequestService() { }

        public void DoHttpRequest(Object stateInfo) {
            Console.WriteLine("Make HTTP request on website.");
            WebRequest webRequest = WebRequest.Create("http://honeycomb/");
            HttpWebResponse webResp = (HttpWebResponse)webRequest.GetResponse();

            if(webResp.StatusCode == HttpStatusCode.OK) {
                Console.WriteLine("Successfully received request from web application");
            } else {
                Console.WriteLine("Web Application responsed with a code other than 200 success.");
            }
            
        }
    }
}
