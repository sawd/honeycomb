﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using Honeycomb.Custom;
using Honeycomb.Custom.Data;
using System.ComponentModel;
using System.Data.Entity;

namespace Honeycomb.Contractor {
    [ServiceContract(Namespace = "ContractorService")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class WCFContractor {

        #region contractor listing, edit etc
        [OperationContract]
        [Security("Admin,ContractorsEdit,ContractorsAdd")]
        public EntityTransport<long> UpdateContractor(Custom.Data.Contractor contractorEntity, List<long> categoriesList , List<ContactPerson> contactPersons) {

            Utility.WCFCheckSecurityAttribute();

            long contractorID = 0;

            try {
                using(var context = new Honeycomb_Entities()){
					if(contactPersons.Count > 0) {
						if(contractorEntity.ContractorID == 0) {
							//create a new one
							contractorEntity.InsertedOn = DateTime.Now;
							contractorEntity.InsertedBy = SessionManager.UserName;
							contractorEntity.InsertedByID = SessionManager.UserID;
							contractorEntity.SystemID = CacheManager.SystemID;

							//add contact persons
							if(contactPersons.Count() > 0) {
								//add the contacts
								contractorEntity.ContactPerson = contactPersons;
							}

							if(categoriesList.Count() > 0) {
								contractorEntity.SubCategory = context.SubCategory.Where(s => categoriesList.Contains(s.SubCategoryID)).ToList();
							}

							//attach the list of contact persons
							context.Contractor.Add(contractorEntity);

							//save
							context.SaveChanges();

						} else {
							//update an existing one
							var contractorDB = context.Contractor.Include("ContactPerson").Where(c => c.ContractorID == contractorEntity.ContractorID).FirstOrDefault();
							//attach the list of contact persons                        
							contractorDB.ContactPerson = contactPersons;

							if(contractorDB != null) {
								contractorEntity.MergeInto(contractorDB, new string[] { 
                                "InsertedOn",
                                "InsertedBy",
                                "InsertedByID",
                                "SystemID",
                                "ContactPerson"
                            }, true);

								context.SaveChanges();
							}
						}
					}

                    contractorID = contractorEntity.ContractorID;
                }

                return Utility.ServiceResponse(ref contractorID);
            }catch(Exception ex){
                return Utility.ServiceResponse(ref contractorID, ex);
            }
        }

        /// <summary>
        /// Gets all the contractors 
        /// </summary>
        /// <param name="pagination"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        [OperationContract]
        [Security("Admin,ContractorsView")]
        public EntityTransport<CustomContractor> GetContractors(PaginationContainer pagination, string filter = null,long [] subcatArr = null) {

            Utility.WCFCheckSecurityAttribute();

            var contractorsList = new List<CustomContractor>();

            try {
                using(var context = new Honeycomb_Entities()){

                    if(subcatArr != null) {
                        if(subcatArr.Length > 0) {
                            var contractorsWithSubCategories = (from c in context.Contractor
                                                                join cp in context.ContactPerson.Where(o => o.Type == "O") on c.ContractorID equals cp.ContractorID into lcp
                                                                from leftcp in lcp.DefaultIfEmpty()
                                                                where c.DeletedOn == null
                                                                 && c.SubCategory.Any(sc => subcatArr.Contains(sc.SubCategoryID))
                                                                select new CustomContractor() {
																	Contact = (leftcp == null) ? "N/A" : leftcp.FirstName + " " + leftcp.LastName,
                                                                    ContractorID = c.ContractorID,
																	Email = (leftcp == null) ? "N/A" : leftcp.Email,
																	MobileNo = (leftcp == null) ? "N/A" : leftcp.MobileNo,
                                                                    TradingName = c.TradingName
                                                                })
                                                               .OptionalWhere(!string.IsNullOrEmpty(filter), op => op.TradingName.ToLower().Contains(filter.ToLower()))
                                                                .ToList();


                            contractorsList = contractorsWithSubCategories;
                           
                        }
                    } else {
                        contractorsList = (from c in context.Contractor
										   join cp in context.ContactPerson.Where(o => o.Type == "O") on c.ContractorID equals cp.ContractorID into lcp
										   from leftcp in lcp.DefaultIfEmpty()
                                           where c.DeletedOn == null
                                           select new CustomContractor() {
											   Contact = (leftcp == null) ? "N/A" : leftcp.FirstName + " " + leftcp.LastName,
											   ContractorID = c.ContractorID,
											   Email = (leftcp == null) ? "N/A" : leftcp.Email,
											   MobileNo = (leftcp == null) ? "N/A" : leftcp.MobileNo,
											   TradingName = c.TradingName
                                           })
                                       .OptionalWhere(!string.IsNullOrEmpty(filter), op => op.TradingName.ToLower().Contains(filter.ToLower()))
                                       .ToList();
                    }

                }

                var paginatedResult = contractorsList.AsQueryable().Paginate(pagination.CurrentPage, pagination.PageSize, pagination.OrderBy).ToList();
                return Utility.ServiceResponse(ref paginatedResult, contractorsList.Count);
            }catch(Exception ex){
                return Utility.ServiceResponse(ref contractorsList, ex);
            }
        }


        /// <summary>
        /// Returns a contractor entity object
        /// </summary>
        /// <param name="contractorID">The id of the contractor</param>
        /// <returns></returns>
        [OperationContract]
        [Security("Admin,ContractorsView")]
        public EntityTransport<Honeycomb.Custom.Data.Contractor> GetContractor(long contractorID) {

            Utility.WCFCheckSecurityAttribute();

            var contractorDB = new Custom.Data.Contractor();

            try {
                using(var context = new Honeycomb_Entities()){
                    contractorDB = context.Contractor.Where(c => c.ContractorID == contractorID).FirstOrDefault();
                }

                return Utility.ServiceResponse(ref contractorDB);
            }catch(Exception ex) {
                return Utility.ServiceResponse(ref contractorDB, ex);
            }
        }

        /// <summary>
        /// Gets the list of contact persons for the contractor
        /// </summary>
        /// <param name="contractorID">the contractors id</param>
        /// <returns></returns>
        [OperationContract]
        [Security("Admin,ContractorsView")]
        public EntityTransport<ContactPerson> GetContractorContactPersons(long contractorID) {

            Utility.WCFCheckSecurityAttribute();

            var contractorPerson = new List<ContactPerson>();

            try {
                using(var context = new Honeycomb_Entities()){
                    contractorPerson = context.ContactPerson.Where(cp => cp.ContractorID == contractorID).ToList();
                }

                return Utility.ServiceResponse(ref contractorPerson);
            }catch(Exception ex){
                return Utility.ServiceResponse(ref contractorPerson, ex);
            }
        }

        /// <summary>
        /// Gets a integer list of sub category ids that have been assigned to the contractor
        /// </summary>
        /// <param name="contractorID">The id of the contractor</param>
        /// <returns>an integer list/array of sub category id's</returns>
        [OperationContract]
        [Security("Admin,ContractorsView")]
        public EntityTransport<SubCategory> GetContractorSubCategories(long contractorID) {

            Utility.WCFCheckSecurityAttribute();

            var subCategories = new List<SubCategory>();

            try {

                using(var context = new Honeycomb_Entities()){
                    subCategories = context.SubCategory.Where(sc => sc.Contractor.Any(c => c.ContractorID == contractorID)).ToList();
                }

                return Utility.ServiceResponse(ref subCategories);
            }catch(Exception ex){
                return Utility.ServiceResponse(ref subCategories, ex);
            }
        }


        /// <summary>
        /// marks a contractor as deleted
        /// </summary>
        /// <param name="contractorID"></param>
        /// <returns></returns>
        [OperationContract]
        [Security("Admin,ContractorsEdit")]
        public EntityTransport<ProcessResponse> DeleteContractor(long contractorID){
            ProcessResponse pRes = new ProcessResponse();

            try {
                using(var context = new Honeycomb_Entities()){
                    var contractorDB = context.Contractor.Include(i => i.Staff).Where(c => c.ContractorID == contractorID).FirstOrDefault();

                    if(contractorDB != null) {

						var improSecurity = new Custom.ImproSecurity();

                        contractorDB.DeletedOn = DateTime.Now;
                        contractorDB.DeletedBy = SessionManager.UserName;
                        contractorDB.DeletedByID = SessionManager.UserID;

                        //get the staff that are currently assigned to the contractor
                        var contractorStaff = context.Staff.Include(i => i.Contractor).Include(i => i.Property).Where(s => s.DeletedOn == null && s.Contractor.Any(c => c.ContractorID == contractorID)).ToList();
                        //todo: remove the association of the staff for the contractor that has been deleted

						foreach(var staff in contractorStaff) {
							if(staff.Contractor.Count == 1 && staff.Property.Count == 0){
								//this staf member is only assiged to this contractor & no properties, remove their access from the estate
								if(!string.IsNullOrEmpty(staff.GOVID)) {
									improSecurity.DisableMasterRecord(staff.GOVID);
								}
							}
						}

						contractorDB.Staff = new List<Staff>();

                        context.SaveChanges();
                    }
                }

                pRes.success = true;
                pRes.message = "Successfuly deleted contractor and removed staff associations.";

                return Utility.ServiceResponse(ref pRes);
            }catch(Exception ex){
                pRes.success = true;
                pRes.message = "There was an error when attempting to remove the specified contractor.";

                return Utility.ServiceResponse(ref pRes, ex);
            }
        }

        #endregion

        #region lookups
        [OperationContract]
        [Security("Admin,ContractorsView")]
        public EntityTransport<Category> GetCategories() {

            Utility.WCFCheckSecurityAttribute();

            var categories = new List<Category>();

            try {
                using(var context = new Honeycomb_Entities()){
                    categories = context.Category.Where(c => c.DeletedOn == null && c.SystemID == CacheManager.SystemID).ToList();
                }

                return Utility.ServiceResponse(ref categories);
            }catch(Exception ex){
                return Utility.ServiceResponse(ref categories, ex);
            }
        }


        [OperationContract]
        [Security("Admin,ContractorsView")]
        public EntityTransport<SubCategory> GetSubCategories(long categoryID) {

            Utility.WCFCheckSecurityAttribute();

            var subCategories = new List<SubCategory>();

            try {
                using(var context = new Honeycomb_Entities()){
                    subCategories = context.SubCategory.Where(sc => sc.CategoryID == categoryID && sc.DeletedOn == null).ToList();
                }

                return Utility.ServiceResponse(ref subCategories);
            }catch(Exception ex){
                return Utility.ServiceResponse(ref subCategories, ex);
            }
        }
        #endregion

        #region staff
        /// <summary>
        /// returns a list of staff members assigned to a property
        /// </summary>
        /// <param name="ContractorID"></param>
        /// <returns></returns>
        [OperationContract]
        [Security("Admin,ContractorsView")]
        public EntityTransport<Staff> GetStaff(long ContractorID) {

            Utility.WCFCheckSecurityAttribute();

            var ContractorStaff = new List<Staff>();

            try {
                using(var context = new Honeycomb_Entities()) {
                    ContractorStaff = context.Staff.Where(s => s.Contractor.Any(c => c.ContractorID == ContractorID) && s.DeletedOn == null && s.Active == true).OrderBy(o => o.Lastname).ToList();
                }

                return Utility.ServiceResponse(ref ContractorStaff);
            } catch(Exception ex) {
                return Utility.ServiceResponse(ref ContractorStaff, ex);
            }
        }

        /// <summary>
        /// Gets a list of erven that the staff member is active on
        /// </summary>
        /// <param name="StaffID"></param>
        /// <returns></returns>
        [OperationContract]
        [Security("Admin,ContractorsView")]
        public EntityTransport<Erf> GetStaffErven(long StaffID) {

            Utility.WCFCheckSecurityAttribute();

            var StaffErven = new List<Erf>();

            try {

                using(var context = new Honeycomb_Entities()) {
                    StaffErven = context.Erf.Where(e => e.Property.Any(p => p.Staff.Any(s => s.ID == StaffID))).ToList();
                }

                return Utility.ServiceResponse(ref StaffErven);
            } catch(Exception ex) {
                return Utility.ServiceResponse(ref StaffErven, ex);
            }
        }

        /// <summary>
        /// Will search the database for a staff member with the same id and return that staff member.
        /// </summary>
        /// <param name="StaffIDNo"></param>
        /// <returns></returns>
        [OperationContract]
        [Security("Admin,ContractorsEdit,ContractorsAdd")]
        public EntityTransport<Staff> GetStaffByID(string StaffIDNo, long ContractorID) {

            Utility.WCFCheckSecurityAttribute();

            var StaffDB = new Staff();
            var impro = new ImproSecurity();
            try {

                using(var context = new Honeycomb_Entities()) {
                    if(!string.IsNullOrEmpty(StaffIDNo)) {
                        StaffDB = context.Staff.Where(s => s.GOVID == StaffIDNo.Trim() && !(s.Contractor.Any(c => c.ContractorID == ContractorID))).FirstOrDefault();

                        //if a staff member was not found on the honeycomb system look for the staff member in impro
                        if(StaffDB == null) {
                            var improStaff = impro.GetMasterByID(StaffIDNo);

                            if(improStaff != null) {
                                StaffDB = new Staff();
                                StaffDB.FirstName = improStaff.MST_FirstName;
                                StaffDB.Lastname = improStaff.MST_LastName;
                                StaffDB.Gender = improStaff.MST_Gender == "M" ? "Male" : "Female";
                                StaffDB.GOVID = StaffIDNo;
                                StaffDB.Active = true;

                                byte [] imageBA = impro.GetMasterImage(improStaff.MST_SQ);

                                /// an image exists, add it to the document repository and associate it with the staff entity to send down
                                if(imageBA.Length > 0) {
                                    var newDocumentID = Custom.Model.Document.SaveDocument(imageBA,"staffimage.jpg","",SessionManager.UserName);
                                    if(newDocumentID != null) {
                                        StaffDB.FileID = newDocumentID;
                                    }
                                }
                            }
                        }
                    }
                }

                return Utility.ServiceResponse(ref StaffDB);
            } catch(Exception ex) {
                return Utility.ServiceResponse(ref StaffDB, ex);
            }
        }

        /// <summary>
        /// Updates or adds a staff member
        /// </summary>
        /// <param name="StaffMember"></param>
        /// <param name="ContractorID"></param>
        /// <returns></returns>
        [OperationContract]
        [Security("Admin,ContractorsEdit,ContractorsAdd")]
        public EntityTransport<string> UpdateStaff(Staff StaffMember, long ContractorID) {

            Utility.WCFCheckSecurityAttribute();

            var Message = "";
            var ImproSecurity = new ImproSecurity();

            try {

                using(var context = new Honeycomb_Entities()) {

                    if(StaffMember.ID == 0) {
                        //add
                        StaffMember.InsertedBy = SessionManager.UserName;
                        StaffMember.InsertedOn = DateTime.Now;
                        StaffMember.InsertedByID = SessionManager.UserID;
                        //associate the staff member with a contractor
                        StaffMember.Contractor = context.Contractor.Where(c => c.ContractorID == ContractorID).ToList();
                        //add the staff memeber to the context
                        context.Staff.Add(StaffMember);
                        //synchronise impro
                        ImproSecurity.SynchroniseImpro(StaffMember);
                        //save changes
                        context.SaveChanges();
                    } else {
                        //do an update
                        var StaffMemberDB = context.Staff.Include("Contractor").Where(s => s.ID == StaffMember.ID).FirstOrDefault();

                        if(StaffMemberDB != null) {

                            //check if this staff member has been assigned to the contractor
                            if(!StaffMemberDB.Contractor.Any(c => c.ContractorID == ContractorID)) {
                                //add the contractor to the staff entity
                                StaffMemberDB.Contractor.Add(context.Contractor.Where(c => c.ContractorID == ContractorID).FirstOrDefault());
                            }

                            StaffMember.MergeInto(StaffMemberDB, new string [] { 
                                "Contractor",
                                "InsertedBy",
                                "InsertedOn",
                                "InsertedByID",
                                "DeletedOn",
                                "DeletedBy",
                                "DeleteByID"
                            }, true);

                            //Update the context
                            context.SaveChanges();
                            //synchronise impro
                            ImproSecurity.SynchroniseImpro(StaffMemberDB);
                        } else {
                            Message = "There was an erorr retrieving the staff member record.";
                        }
                    }
                }

                return Utility.ServiceResponse(ref Message);

            } catch(Exception ex) {
                Message = "There was an error updating the staff member";
                return Utility.ServiceResponse(ref Message, ex);
            }
        }

        [OperationContract]
        [Security("Admin,ContractorsEdit,ContractorsAdd")]
        public EntityTransport<string> GetStaffCompanies(long StaffID) {

            Utility.WCFCheckSecurityAttribute();

            string comanies = "";
            try
            {
                using (var context = new Honeycomb_Entities())
                {
                    comanies = String.Join(", ", context.Contractor.Include(s => s.Staff).Where(w => w.Staff.Any(s => s.ID == StaffID)).ToList().Select(sel => sel.TradingName));
                 
                }

                return Utility.ServiceResponse(ref comanies);
            }
            catch (Exception ex)
            {
                return Utility.ServiceResponse(ref comanies, ex);
            }
        }

        /// <summary>
        /// Will remove all staff members from the contractor, it does not delete them it simply removes the link to the contractor.
        /// </summary>
        /// <param name="contractorID"></param>
        /// <returns>boolean and message indicating success</returns>
        [OperationContract]
        [Security("Admin,ContractorsEdit")]
        public EntityTransport<ProcessResponse> RemoveAllContractorStaff(long contractorID) {
            ProcessResponse pRes = new ProcessResponse();

            try {
                using(var context = new Honeycomb_Entities()){
                    var contractorDB = context.Contractor.Include("Staff").Where(c => c.ContractorID == contractorID).FirstOrDefault();

                    if(contractorDB != null) {
                        //clear
                        contractorDB.Staff.Clear();
                        //save
                        context.SaveChanges();
                        pRes.success = true;
                        pRes.message = "Successfully cleared staff members for the provided contractor.";
                    }
                }

                return Utility.ServiceResponse(ref pRes);
            }catch(Exception ex){
                pRes.success = false;
                pRes.message = "There was an error while attempting to clear the staff member associations for the contractor.";
                return Utility.ServiceResponse(ref pRes, ex);
            }
        }
        #endregion

        #region staff notes
        /// <summary>
        /// Gets all staff notes
        /// </summary>
        /// <param name="StaffID">The id of the staff member</param>
        /// <returns>A list of type StaffNote</returns>
        [OperationContract]
        [Security("Admin,ContractorsView")]
        public EntityTransport<StaffNote> GetStaffNotes(long StaffID) {

            Utility.WCFCheckSecurityAttribute();

            var StaffNotes = new List<StaffNote>();

            try {

                using(var context = new Honeycomb_Entities()) {
                    StaffNotes = context.StaffNote.Where(sn => sn.StaffID == StaffID && sn.DeletedOn == null).OrderByDescending(o => o.InsertedOn).Take(20).ToList();
                }

                return Utility.ServiceResponse(ref StaffNotes);
            } catch(Exception ex) {
                return Utility.ServiceResponse(ref StaffNotes, ex);
            }
        }

        /// <summary>
        /// Retrieves a single staff note record when provided with an ID.
        /// </summary>
        /// <param name="NoteID"></param>
        /// <returns></returns>
        [OperationContract]
        [Security("Admin,ContractorsView")]
        public EntityTransport<StaffNote> GetStaffNote(long NoteID) {

            Utility.WCFCheckSecurityAttribute();

            var StaffNote = new StaffNote();

            try {
                using(var context = new Honeycomb_Entities()) {
                    StaffNote = context.StaffNote.Where(sn => sn.ID == NoteID).FirstOrDefault();
                }

                return Utility.ServiceResponse(ref StaffNote);
            } catch(Exception ex) {
                return Utility.ServiceResponse(ref StaffNote, ex);
            }
        }

        /// <summary>
        /// Adds or updates a staff note
        /// </summary>
        /// <param name="Note"></param>
        /// <returns></returns>
        [OperationContract]
        [Security("Admin,ContractorsEdit,ContractorsAdd")]
        public EntityTransport<bool> UpdateStaffNote(StaffNote Note) {

            Utility.WCFCheckSecurityAttribute();

            var success = true;

            try {

                using(var context = new Honeycomb_Entities()) {

                    if(Note.ID == 0) {
                        //insert
                        Note.InsertedOn = DateTime.Now;
                        Note.InsertedBy = SessionManager.UserName;
                        Note.InsertedByID = SessionManager.UserID;

                        context.StaffNote.Add(Note);
                    } else {
                        //update
                        var NoteDB = context.StaffNote.Where(sn => sn.ID == Note.ID).FirstOrDefault();

                        if(NoteDB != null) {
                            Note.MergeInto(NoteDB, new string [] { 
                                "InsertedOn",
                                "InsertedBy",
                                "InsertedByID",
                                "DeletedOn",
                                "DeletedBy",
                                "DeletedByID"
                            }, true);
                        }
                    }

                    context.SaveChanges();
                }

                return Utility.ServiceResponse(ref success);
            } catch(Exception ex) {
                success = false;
                return Utility.ServiceResponse(ref success, ex);
            }
        }

        /// <summary>
        /// Soft deletes the note
        /// </summary>
        /// <param name="NoteID"></param>
        /// <returns></returns>
        [OperationContract]
        [Security("Admin,ContractorsEdit,ContractorsAdd")]
        public EntityTransport<bool> DeleteStaffNote(long NoteID) {

            Utility.WCFCheckSecurityAttribute();

            bool success = true;

            try {

                using(var context = new Honeycomb_Entities()) {
                    var StaffNoteDB = context.StaffNote.Where(sn => sn.ID == NoteID).FirstOrDefault();

                    if(StaffNoteDB != null) {
                        StaffNoteDB.DeletedOn = DateTime.Now;
                        StaffNoteDB.DeletedBy = SessionManager.UserName;
                        StaffNoteDB.DeletedByID = SessionManager.UserID;

                        context.SaveChanges();
                    }
                }

                return Utility.ServiceResponse(ref success);
            } catch(Exception ex) {
                success = false;
                return Utility.ServiceResponse(ref success, ex);
            }
        }
        #endregion
    }
}
