
POST: http://api.honeycombcms.com/honeycombapi.svc/AuthenticateGuard

Request Headers:
Accept: */*
Content-Type: application/json; charset=utf-8
Accept-Language: en-za
Accept-Encoding: gzip, deflate
User-Agent: Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)
Host: api.honeycombcms.com
Content-Length: 100
Connection: Keep-Alive
Pragma: no-cache
Cookie: ASP.NET_SessionId=xxxxxxxxxxxxxxxxxxxxxxx; (Key returned from Authenticate method)

Request Body:
{"SecurityToken":"xxxxxx-xxxxx-xxxx-xxx-xxxxxxxxxx","Username":"xxxxx","Password":"xxxxxx"} (Don't forget to update Content-Length in header above)
