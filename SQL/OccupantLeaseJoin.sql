select o.FirstName,o.LastName, l.EndDate, p.ERFID/*Count(*) as num*/
from [Honeycomb-Simbithi].[system].Occupant o
inner join [Honeycomb-Simbithi].[system].PropertyOccupant po on po.OccupantID = o.ID AND po.ArchivedOn is null
inner join [Honeycomb-Simbithi].[system].Property p on po.PropertyID = p.ID
inner join [Honeycomb-Simbithi].[system].Lease l on l.PropertyID = p.ID AND l.EndDate < GETDATE()
where o.DeletedOn is null
AND o.IsTenant = 1
AND o.HasAccessCard = 1
AND (Select Count(*) FROM [Honeycomb-Simbithi].[system].LeaseExtension le where le.PropertyID = p.id) = 0