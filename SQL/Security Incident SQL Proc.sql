use [Honeycomb-simbithi]

DECLARE @searchString varchar(100) = ''

SET @searchString = '%'+ @searchString +'%'

;With MassiveCTE 
as (
	select distinct i.ID from security.Incident i
		left outer join system.Erf e on i.ErfID = e.ID
		-- Incident Property
		left outer join system.Property ip on i.PropertyID = ip.ID
		left outer join system.Ownership io on ip.ID = io.PropertyID
		left outer join security.IncidentInvolvedPerson ipo on ip.ID = ipo.IncidentID
		left outer join system.Occupant ioc on ip.ID = ipo.OccupantID and (ioc.FirstName + ' ' + ioc.LastName) like @searchString
		-- Erf Property
		left outer join system.Property ep on e.ID = ep.ERFID
		left outer join system.Ownership eo on ep.ID = eo.PropertyID
		left outer join security.IncidentInvolvedPerson epo on ep.ID = epo.IncidentID
		left outer join system.Occupant eoc on ep.ID = epo.OccupantID and(eoc.FirstName + ' ' + eoc.LastName) like @searchString
	where 
	(e.ErfNumber like @searchString ) 
	OR (io.PrincipalOwnerName like @searchString )
	OR (eo.PrincipalOwnerName like @searchString )
	OR ('inc' + cast(i.ID as varchar(50)) like @searchString)
)
select i.* from MassiveCTE mc
join security.Incident i on mc.ID = i.ID