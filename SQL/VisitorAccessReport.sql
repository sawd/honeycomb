select ac.VisitorAccessCode, o.FirstName + ' ' + o.LastName as Resident, av.VisitorName as Visitor , ac.Used, ac.VehicleRegIn, ac.VehicleRegOut, ac.EnteredOn, ac.VehicleOccupantIn, ac.ExitedOn, ac.VehicleOccupantOut
from [security].AccessCode ac
inner join [system].Occupant o on o.id = ac.OccupantOriginatorID
inner join [security].AccessVisitor av on av.ID = ac.VisitorID
where ac.VisitorAccessCode in ('13050731774','13050821528')