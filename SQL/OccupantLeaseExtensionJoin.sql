select o.FirstName,o.LastName, le.EndDate, p.ERFID /*Count(*) as Num*/
from [Honeycomb-Simbithi].[system].Occupant o
inner join [Honeycomb-Simbithi].[system].PropertyOccupant po on po.OccupantID = o.ID AND po.ArchivedOn is null
inner join [Honeycomb-Simbithi].[system].Property p on po.PropertyID = p.ID
inner join [Honeycomb-Simbithi].[system].LeaseExtension le on le.PropertyID = p.ID AND le.id = (Select Max(ID) from [Honeycomb-Simbithi].[system].LeaseExtension s where s.PropertyID = p.ID) AND le.EndDate < GETDATE()
where o.DeletedOn is null
AND o.IsTenant = 1
AND o.HasAccessCard = 1