DECLARE @schema NVARCHAR(100)
DECLARE @table NVARCHAR(100)
DECLARE @createtable BIT = 1 --whether to create the audit table or not

SET @schema = 'security' -- the schema name of the table to be audited
SET @table = 'Incident'  -- the name of the table to be audited

IF OBJECT_ID('tempdb..#PKS') IS NOT NULL 
	DROP TABLE #PKS
	
IF OBJECT_ID('tempdb..#COL') IS NOT NULL 	
	DROP TABLE #COL

/* This will be a list of Columns that you want to exclude from the trigger 
TrackSysUserID,TrackDateTime are defaults and then you add any additional ones */
DECLARE @casecolumnExclude NVARCHAR(MAX) = 'InsertedOn,InsertedBy,InsertedByID,ArchivedOn,IncidentID,DeletedOn,DeletedBy,DeletedByID'

/*
NOTES:
When adding a new table for audit go and add it to the $Juice\Juice.SQL\Data\DeleteData.sql clean up audit section

*/
------------------------------------------------------
-- template
DECLARE @CREATETRIGGER NVARCHAR(MAX)
DECLARE @DELETETRIGGER NVARCHAR(MAX)
DECLARE @AUDITXMLUPDATE NVARCHAR(MAX) = ''
DECLARE @AUDITXMLINSERT NVARCHAR(MAX) = ''
DECLARE @AUDITXMLDELETE NVARCHAR(MAX) = ''
DECLARE @primarykeylist NVARCHAR(MAX) = ''

/* e.g.
AccountTypeID
--or 
SysUserID, BranchID, SysRoleID
*/
DECLARE @primarykeyjoin NVARCHAR(MAX) = ''
/* e.g.
ON r.AccountTypeID = N.AccountTypeID
--or
ON r.SysUserID = N.SysUserID
AND r.BranchID = N.BranchID
AND r.SysRoleID = N.SysRoleID
*/
DECLARE @primarykeywhere NVARCHAR(MAX) = ''
/* e.g.
WHERE r.AccountTypeID = aud.AccountTypeID
--or
WHERE r.SysUserID = aud.SysUserID
AND r.BranchID = aud.BranchID
AND r.SysRoleID = aud.SysRoleID
*/
DECLARE @casecolumnlist NVARCHAR(MAX) = ''
/* e.g.
(CASE WHEN UPDATE(Code) THEN r.Code END) AS oCode,
(CASE WHEN UPDATE(Code) THEN N.Code END) AS nCode,
(CASE WHEN UPDATE(Name) THEN r.Name END) AS oName,
(CASE WHEN UPDATE(Name) THEN N.Name END) AS nName,
(CASE WHEN UPDATE(IsActive) THEN r.IsActive END) AS oIsActive,
(CASE WHEN UPDATE(IsActive) THEN N.IsActive END) AS nIsActive,
(CASE WHEN UPDATE(DisplayOrder) THEN r.DisplayOrder END) AS oDisplayOrder,
(CASE WHEN UPDATE(DisplayOrder) THEN N.DisplayOrder END) AS nDisplayOrder,
*/

DECLARE @newcolumnlist NVARCHAR(MAX) = ''
/* e.g.
r.Code AS nCode
,r.Name AS nName
,r.IsActive AS nIsActive
,r.DisplayOrder AS nDisplayOrder
*/
DECLARE @oldcolumnlist NVARCHAR(MAX) = ''
/* e.g.
r.Code AS oCode
,r.Name AS oName
,r.IsActive AS oIsActive
,r.DisplayOrder AS oDisplayOrder
,r.TrackSysUserID AS oTrackSysUserID
,r.TrackDateTime AS oTrackDateTime
*/

------------------------------------------------------
-- get the primary key/s of the table
CREATE TABLE #PKS ( COLUMN_NAME NVARCHAR(MAX), TheDef NVARCHAR(MAX) )

INSERT INTO #PKS 
SELECT C.COLUMN_NAME, C.COLUMN_NAME + ' ' +  
		CASE 
			WHEN C.DATA_TYPE LIKE 'CHAR' OR C.DATA_TYPE LIKE 'VARCHAR' 
				THEN '[' + C.DATA_TYPE + '](' + CAST(C.CHARACTER_MAXIMUM_LENGTH AS NVARCHAR(10)) + ')' 
			ELSE '[' + C.DATA_TYPE + ']' 
		END + ' ' + 
		CASE 
			WHEN C.IS_NULLABLE = 'YES' 
				THEN 'NULL' 
			ELSE 'NOT NULL' 
		END AS TheDef
FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS T 
	INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE K ON T.CONSTRAINT_NAME = K.CONSTRAINT_NAME  
	INNER JOIN INFORMATION_SCHEMA.COLUMNS AS c ON 
		T.TABLE_SCHEMA = C.TABLE_SCHEMA
		AND T.TABLE_NAME = C.TABLE_NAME
		AND K.COLUMN_NAME = C.COLUMN_NAME
		AND K.ORDINAL_POSITION = C.ORDINAL_POSITION	
WHERE 
    T.CONSTRAINT_TYPE = 'PRIMARY KEY'  
    AND T.TABLE_NAME = @table 
    AND T.TABLE_SCHEMA = @schema    
ORDER BY K.ORDINAL_POSITION ASC
------------------------------------------------------
-- construct the primary key list of the table
SELECT @primarykeylist = @primarykeylist + COLUMN_NAME + ','
FROM #PKS

SET @primarykeylist = LEFT(@primarykeylist, LEN(@primarykeylist) - 1)

--SELECT @primarykeylist
------------------------------------------------------

SET @DELETETRIGGER = 'IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N''[<@schema>].[TRG_<@table>]'')) BEGIN
DROP TRIGGER [<@schema>].[TRG_<@table>]
END'

SET @CREATETRIGGER = 'CREATE TRIGGER [<@schema>].[TRG_<@table>]
ON [<@schema>].[<@table>]
FOR INSERT,UPDATE,DELETE
AS

DECLARE @audit TABLE (
	AuditSysUserID INT NULL,
	AuditAction CHAR(1),
	AuditXML XML,
	<@primarykeycolumndef>
)

IF (SELECT COUNT(*) FROM INSERTED) > 0
BEGIN
	IF (SELECT COUNT(*) FROM DELETED) > 0
	BEGIN
		INSERT INTO @audit (AuditSysUserID, AuditAction, AuditXML, <@primarykeylist>)
		SELECT (SELECT TOP 1 TrackSysUserID FROM INSERTED),''U'',AuditXML =	<@AUDITXMLUPDATE>,<@primarykeylist> FROM DELETED aud
	END
	ELSE
	BEGIN
		INSERT INTO @audit (AuditSysUserID, AuditAction, AuditXML, <@primarykeylist>)
		SELECT (SELECT TOP 1 TrackSysUserID FROM INSERTED),''I'',AuditXML =	<@AUDITXMLINSERT>,<@primarykeylist> FROM INSERTED aud
	END
END
ELSE
BEGIN
	INSERT INTO @audit (AuditSysUserID, AuditAction, AuditXML, <@primarykeylist>)
	SELECT NULL,''D'',AuditXML = <@AUDITXMLDELETE>,<@primarykeylist> FROM DELETED aud
END

INSERT INTO audit.<@table>Audit (AuditSysUserID, AuditAction, AuditXML, <@primarykeylist>)
SELECT AuditSysUserID, AuditAction, AuditXML, <@primarykeylist>  
FROM @audit
WHERE CAST(AuditXML AS NVARCHAR(MAX)) NOT LIKE ''<r/>''
'
SET @AUDITXMLUPDATE = '(SELECT <@casecolumnlist> FROM DELETED AS r INNER JOIN INSERTED AS N <@primarykeyjoin> WHERE <@primarykeywhere> FOR XML AUTO)'
SET @AUDITXMLINSERT = '(SELECT <@newcolumnlist> FROM INSERTED AS r WHERE <@primarykeywhere> FOR XML AUTO)'
SET @AUDITXMLDELETE = '(SELECT <@oldcolumnlist> FROM DELETED AS r	WHERE <@primarykeywhere> FOR XML AUTO)'


SET @DELETETRIGGER = REPLACE(@DELETETRIGGER, '<@table>', @table)
SET @DELETETRIGGER = REPLACE(@DELETETRIGGER, '<@schema>', @schema)

SET @CREATETRIGGER = REPLACE(@CREATETRIGGER, '<@table>', @table)
SET @CREATETRIGGER = REPLACE(@CREATETRIGGER, '<@schema>', @schema)
SET @CREATETRIGGER = REPLACE(@CREATETRIGGER, '<@primarykeylist>', @primarykeylist)

------------------------------------------------------
-- construct the primary key join of the table
SELECT @primarykeyjoin = @primarykeyjoin + 'r.' + COLUMN_NAME + ' = N.' + COLUMN_NAME + ' AND '
FROM #PKS

SET @primarykeyjoin = 'ON ' + LEFT(@primarykeyjoin, LEN(@primarykeyjoin) - 4)
SET @AUDITXMLUPDATE = REPLACE(@AUDITXMLUPDATE, '<@primarykeyjoin>', @primarykeyjoin)

--SELECT @primarykeyjoin
------------------------------------------------------
-- construct the primary key where of the table
SELECT @primarykeywhere = @primarykeywhere + 'r.' + COLUMN_NAME + ' = aud.' + COLUMN_NAME + ' AND '
FROM #PKS

SET @primarykeywhere = LEFT(@primarykeywhere, LEN(@primarykeywhere) - 4)

SET @AUDITXMLUPDATE = REPLACE(@AUDITXMLUPDATE, '<@primarykeywhere>', @primarykeywhere)
SET @AUDITXMLINSERT = REPLACE(@AUDITXMLINSERT, '<@primarykeywhere>', @primarykeywhere)
SET @AUDITXMLDELETE = REPLACE(@AUDITXMLDELETE, '<@primarykeywhere>', @primarykeywhere)

--SELECT @primarykeywhere
------------------------------------------------------
-- get the other columns of the table
CREATE TABLE #COL ( COLUMN_NAME VARCHAR(MAX), IsXML bit )

INSERT INTO #COL 
SELECT COLUMN_NAME, CASE WHEN DATA_TYPE LIKE 'xml' THEN 1 ELSE 0 END
FROM INFORMATION_SCHEMA.COLUMNS AS c
WHERE TABLE_NAME LIKE @table 
    AND TABLE_SCHEMA LIKE @schema
    AND COLUMN_NAME NOT IN (SELECT COLUMN_NAME FROM #PKS)
    AND COLUMN_NAME NOT IN (SELECT Data FROM SYSTEM.Split(@casecolumnExclude, ','))
ORDER BY ORDINAL_POSITION ASC
------------------------------------------------------
-- construct the case column list where of the table
SELECT @casecolumnlist = @casecolumnlist + 
	'(CASE WHEN UPDATE(' + COLUMN_NAME + ') AND ((' + CASE WHEN IsXML = 1 THEN 'CAST(r.' + COLUMN_NAME + ' AS NVARCHAR(MAX)) <> CAST(N.' + COLUMN_NAME + ' AS NVARCHAR(MAX))' ELSE 'r.' + COLUMN_NAME + ' <> N.' + COLUMN_NAME END + ') OR (r.' + COLUMN_NAME + ' IS NULL AND N.' + COLUMN_NAME + ' IS NOT NULL) OR (r.' + COLUMN_NAME + ' IS NOT NULL AND N.' + COLUMN_NAME + ' IS NULL)) THEN ' + CASE WHEN IsXML = 1 THEN 'CAST(r.' + COLUMN_NAME + ' AS NVARCHAR(MAX))' ELSE 'r.' + COLUMN_NAME END + ' END) AS o' + COLUMN_NAME + ',' +
	'(CASE WHEN UPDATE(' + COLUMN_NAME + ') AND ((' + CASE WHEN IsXML = 1 THEN 'CAST(r.' + COLUMN_NAME + ' AS NVARCHAR(MAX)) <> CAST(N.' + COLUMN_NAME + ' AS NVARCHAR(MAX))' ELSE 'r.' + COLUMN_NAME + ' <> N.' + COLUMN_NAME END + ') OR (r.' + COLUMN_NAME + ' IS NULL AND N.' + COLUMN_NAME + ' IS NOT NULL) OR (r.' + COLUMN_NAME + ' IS NOT NULL AND N.' + COLUMN_NAME + ' IS NULL)) THEN ' + CASE WHEN IsXML = 1 THEN 'CAST(N.' + COLUMN_NAME + ' AS NVARCHAR(MAX))' ELSE 'N.' + COLUMN_NAME END + ' END) AS n' + COLUMN_NAME + ','
FROM #COL

IF LEN(@casecolumnlist) > 0
BEGIN
	SET @casecolumnlist = LEFT(@casecolumnlist, LEN(@casecolumnlist) - 1)
	SET @AUDITXMLUPDATE = REPLACE(@AUDITXMLUPDATE, '<@casecolumnlist>', @casecolumnlist)
END
ELSE
BEGIN
	SET @AUDITXMLUPDATE = 'NULL'
END
/* e.g.
(CASE WHEN UPDATE(Code) THEN r.Code END) AS oCode,
(CASE WHEN UPDATE(Code) THEN N.Code END) AS nCode,
(CASE WHEN UPDATE(Name) THEN r.Name END) AS oName,
(CASE WHEN UPDATE(Name) THEN N.Name END) AS nName,
(CASE WHEN UPDATE(IsActive) THEN r.IsActive END) AS oIsActive,
(CASE WHEN UPDATE(IsActive) THEN N.IsActive END) AS nIsActive,
(CASE WHEN UPDATE(DisplayOrder) THEN r.DisplayOrder END) AS oDisplayOrder,
(CASE WHEN UPDATE(DisplayOrder) THEN N.DisplayOrder END) AS nDisplayOrder,
*/
------------------------------------------------------
-- construct the new column list where of the table
SELECT @newcolumnlist = @newcolumnlist + 'r.' + COLUMN_NAME + ' AS n' + COLUMN_NAME + ','
FROM #COL

IF LEN(@newcolumnlist) > 0
BEGIN
SET @newcolumnlist = LEFT(@newcolumnlist, LEN(@newcolumnlist) - 1)
SET @AUDITXMLINSERT = REPLACE(@AUDITXMLINSERT, '<@newcolumnlist>', @newcolumnlist)
END
ELSE
BEGIN
SET @AUDITXMLINSERT = 'NULL'
END
/* e.g.
r.Code AS nCode,
r.Name AS nName,
r.IsActive AS nIsActive,
r.DisplayOrder AS nDisplayOrder
*/
------------------------------------------------------
-- construct the old column list where of the table
SELECT @oldcolumnlist = @oldcolumnlist + 'r.' + COLUMN_NAME + ' AS o' + COLUMN_NAME + ','
FROM #COL

IF LEN(@oldcolumnlist) > 0
BEGIN
SET @oldcolumnlist = LEFT(@oldcolumnlist, LEN(@oldcolumnlist) - 1)
SET @AUDITXMLDELETE = REPLACE(@AUDITXMLDELETE, '<@oldcolumnlist>', @oldcolumnlist)
END
ELSE
BEGIN
SET @AUDITXMLDELETE = 'NULL'
END
------------------------------------------------------
-- replace the audit xml constructors for update, insert and delete
SET @CREATETRIGGER = REPLACE(@CREATETRIGGER, '<@AUDITXMLUPDATE>', @AUDITXMLUPDATE)
SET @CREATETRIGGER = REPLACE(@CREATETRIGGER, '<@AUDITXMLINSERT>', @AUDITXMLINSERT)
SET @CREATETRIGGER = REPLACE(@CREATETRIGGER, '<@AUDITXMLDELETE>', @AUDITXMLDELETE)
------------------------------------------------------
-- primary key column definition
DECLARE @primarykeycolumndef NVARCHAR(MAX) = ''

SELECT @primarykeycolumndef = @primarykeycolumndef + TheDef + ', '
FROM #PKS
/* e.g.
[SysUserID] [int] NOT NULL,
[BranchID] [int] NOT NULL,
[SysRoleID] [int] NOT NULL,
*/
--SELECT @primarykeycolumndef

SET @CREATETRIGGER = REPLACE(@CREATETRIGGER, '<@primarykeycolumndef>', LEFT(@primarykeycolumndef, LEN(@primarykeycolumndef) - 1))

------------------------------------------------------
-- create audit table template
DECLARE @CREATEAUDITTABLE NVARCHAR(MAX) = ''

SET @CREATEAUDITTABLE = 'CREATE TABLE [audit].[<@table>Audit](
	[AuditID] [bigint] IDENTITY(1,1) NOT NULL,
	[AuditDateTime] [datetime2](2) NOT NULL,
	[AuditSysUserID] [int] NULL,
	[AuditAction] [char](1) NOT NULL,
	[AuditXML] [xml] NULL,
	<@primarykeycolumndef>
 CONSTRAINT [PK_<@table>Audit] PRIMARY KEY CLUSTERED 
([AuditID] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [AUDIT]
) ON [AUDIT]'

SET @CREATEAUDITTABLE = REPLACE(@CREATEAUDITTABLE, '<@table>', @table)
SET @CREATEAUDITTABLE = REPLACE(@CREATEAUDITTABLE, '<@primarykeycolumndef>', @primarykeycolumndef)
------------------------------------------------------
-- default getdate() on auditdatetime column
DECLARE @DEFAULTGETDATE NVARCHAR(MAX) = ''

SET @DEFAULTGETDATE = 'ALTER TABLE [audit].[<@table>Audit] ADD  CONSTRAINT [DF_<@table>Audit_AuditDateTime]  DEFAULT (getdate()) FOR [AuditDateTime]'

SET @DEFAULTGETDATE = REPLACE(@DEFAULTGETDATE, '<@table>', @table)
------------------------------------------------------
-- drop the temporary tables
DROP TABLE #PKS
DROP TABLE #COL
------------------------------------------------------
-- EXECUTE THE CREATE TRIGGER SCRIPT
IF @createtable = 1
BEGIN
	--SELECT @CREATEAUDITTABLE
	EXECUTE sp_executesql @CREATEAUDITTABLE

	--SELECT @DEFAULTGETDATE
	EXECUTE sp_executesql @DEFAULTGETDATE
END

--SELECT @CREATETRIGGER
EXECUTE sp_executesql @DELETETRIGGER
EXECUTE sp_executesql @CREATETRIGGER
