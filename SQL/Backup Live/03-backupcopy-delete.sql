USE MASTER;

DECLARE @filename NVARCHAR(250)
SET @filename = 'honeycomb_backup_for_local.bak'
DECLARE @location NVARCHAR(MAX)
SET @location = 'C:\MSSQL\Backup\tmp\' + @filename

BACKUP DATABASE [Honeycomb]
   TO DISK = @location WITH INIT, COMPRESSION ;
   
 ALTER DATABASE [Honeycomb] SET SINGLE_USER WITH ROLLBACK IMMEDIATE 
 
 DROP DATABASE [Honeycomb];