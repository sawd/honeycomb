USE master;
GO
--put database into simple recovery mode
ALTER DATABASE [Honeycomb-Simbithi]
SET RECOVERY SIMPLE;

USE [Honeycomb-Simbithi];

GO

UPDATE system.DocumentRepository
SET FileData = null

CHECKPOINT;

GO

EXEC sp_filestream_force_garbage_collection @dbname = [Honeycomb-Simbithi];

CHECKPOINT;

EXEC sp_filestream_force_garbage_collection @dbname = [Honeycomb-Simbithi];

GO