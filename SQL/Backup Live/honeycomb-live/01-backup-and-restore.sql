Use MASTER

DECLARE @filename NVARCHAR(250)
SET @filename = 'honeycomb_backup_for_live.bak'
DECLARE @location NVARCHAR(MAX)
SET @location = 'C:\MSSQL\Backup\tmp\' + @filename

BACKUP DATABASE [Honeycomb]
   TO DISK = @location WITH INIT ;

RESTORE FILELISTONLY 
   FROM DISK = @location;

RESTORE DATABASE [Honeycomb-Simbithi]
   FROM DISK = @location WITH REPLACE,
   MOVE 'Honeycomb' TO 'C:\MSSQL\Backup\tmp\Honeycomb-Simbithi.mdf',
   MOVE 'Honeycomb_Log' TO 'C:\MSSQL\Backup\tmp\Honeycomb-Simbithi.ldf',
   MOVE 'Honeycomb_Repository' TO 'C:\MSSQL\Backup\tmp\Honeycomb-Simbithi_Repository';
GO

PRINT @location