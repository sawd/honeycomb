USE master;
GO
--put database into simple recovery mode
ALTER DATABASE [Honeycomb]
SET RECOVERY SIMPLE;

USE [Honeycomb];

GO

UPDATE system.DocumentRepository
SET FileData = null

CHECKPOINT;

GO

EXEC sp_filestream_force_garbage_collection @dbname = [Honeycomb];

CHECKPOINT;

EXEC sp_filestream_force_garbage_collection @dbname = [Honeycomb];

GO