Use MASTER

DECLARE @filename NVARCHAR(250)
SET @filename = 'honeycomb_backup_for_live.bak'
DECLARE @location NVARCHAR(MAX)
SET @location = 'C:\MSSQL\Backup\tmp\' + @filename

BACKUP DATABASE [Honeycomb-Simbithi]
   TO DISK = @location WITH INIT ;

RESTORE FILELISTONLY 
   FROM DISK = @location;

RESTORE DATABASE [Honeycomb]
   FROM DISK = @location WITH REPLACE,
   MOVE 'Honeycomb' TO 'C:\MSSQL\Backup\tmp\Honeycomb01.mdf',
   MOVE 'Honeycomb_Log' TO 'C:\MSSQL\Backup\tmp\Honeycomb01.ldf',
   MOVE 'Honeycomb_Repository' TO 'C:\MSSQL\Backup\tmp\Honeycomb01_Repository';
GO

PRINT @location