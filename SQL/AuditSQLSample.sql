
IF (OBJECT_ID('tempdb..#tmp') IS NOT NULL)
DROP TABLE #tmp


SELECT 
	AuditID,
	AuditSysUserID,
	AuditDateTime,
	SUBSTRING(CAST(Attribute.Name.query('local-name(.)') AS VARCHAR(100)), 2, 100) [Group],
	CAST(Attribute.Name.query('local-name(.)') AS VARCHAR(100)) Attribute,
	Attribute.Name.value('.','VARCHAR(100)') Value
INTO #tmp
FROM 
    audit.IncidentAudit
    CROSS APPLY AuditXML.nodes('/r//@*') Attribute(Name)
WHERE AuditAction = 'U'


SELECT * FROM (
SELECT 
	tNew.AuditID, 
	tNew.AuditSysUserID, 
	tNew.AuditDateTime, 
	tNew.[Group], 
	tOld.Value AS Before, 
	tNew.Value AS [After], 
	ROW_NUMBER() OVER (PARTITION BY tNew.AuditID, tNew.[Group] ORDER BY tNew.AuditID) AS rCount
FROM #tmp tOld
	LEFT JOIN #tmp tNew ON tOld.[Group] = tNew.[Group] AND tOld.AuditID = tNew.AuditID AND tNew.Attribute = 'n' + tOld.[Group]
	) AS x
WHERE x.rCount = 1


DROP TABLE #tmp
