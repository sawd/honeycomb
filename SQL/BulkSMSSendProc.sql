DECLARE @sendTo varchar(500) = 'Owner,Tenant,All'

;With OwnersCTE
AS (
	SELECT DISTINCT o.ID FROM system.Occupant o
	INNER JOIN system.PropertyOccupant po on o.ID = po.OccupantID AND po.ArchivedOn is null
	WHERE
	(o.Cellphone is not null)
	AND (o.Cellphone <> '')
	AND o.RecieveComms = 1
	AND o.IsOwner = 1
	AND o.DeletedOn is null
),
TenantCTE 
AS (
	SELECT DISTINCT o.ID FROM system.Occupant o
	INNER JOIN system.PropertyOccupant po on o.ID = po.OccupantID AND po.ArchivedOn is null
	WHERE
	(o.Cellphone is not null)
	AND (o.Cellphone <> '')
	AND o.RecieveComms = 1
	AND o.IsTenant = 1
	AND o.DeletedOn is null
)


SELECT Count(*) from TenantCTE oCTE