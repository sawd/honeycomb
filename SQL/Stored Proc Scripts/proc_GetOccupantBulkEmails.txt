USE [Honeycomb-Simbithi]
GO

/****** Object:  StoredProcedure [system].[proc_GetOccupantBulkEmails]    Script Date: 2013-08-05 09:59:08 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Gerard Matthews
-- Create date: 26/07/2013
-- Description:	Will retirieve a list of distinct cellphone numbers to which smses can be sent
-- EXEC system.[proc_GetOccupantBulkEmails] 0,0,0,0
-- =============================================
CREATE PROCEDURE [system].[proc_GetOccupantBulkEmails]
	-- Add the parameters for the stored procedure here
	@SendToAll bit = 0, 
	@SendToOwners bit = 0,
	@SendToTenants bit = 0,
	@SendToOccupants bit = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	

	-- remove the tmp table
	--IF (OBJECT_ID('tempdb..#tmp') IS NOT NULL)
	--DROP TABLE #tmp
	CREATE TABLE #TEMPCELL(OccupantID bigint not null)

	IF @SendToAll = 1
		BEGIN
			--select and return all values
			INSERT INTO #TEMPCELL
			SELECT o.ID
			FROM [system].Occupant o
			INNER JOIN [system].PropertyOccupant po on o.ID = po.OccupantID AND po.ArchivedOn is null
			WHERE o.RecieveComms = 1
			AND o.DeletedOn is null
		END
		ELSE
		BEGIN
			IF @SendToOwners = 1
			BEGIN
				--select and return only owners
				INSERT INTO #TEMPCELL
				SELECT o.ID
				FROM [system].Occupant o
				INNER JOIN [system].PropertyOccupant po on o.ID = po.OccupantID AND po.ArchivedOn is null
				WHERE o.RecieveComms = 1
				AND o.DeletedOn is null
				AND o.IsOwner = 1
				AND o.IsTenant = 0
			END

			IF @SendToTenants = 1
			BEGIN
				--select and return only tenants
				INSERT INTO #TEMPCELL
				SELECT o.ID
				FROM [system].Occupant o
				INNER JOIN [system].PropertyOccupant po on o.ID = po.OccupantID AND po.ArchivedOn is null
				WHERE o.RecieveComms = 1
				AND o.DeletedOn is null
				AND o.IsOwner = 0
				AND o.IsTenant = 1
			END

			IF @SendToOccupants = 1
			BEGIN
				--select neither tenant or owner
				INSERT INTO #TEMPCELL
				SELECT o.ID
				FROM [system].Occupant o
				INNER JOIN [system].PropertyOccupant po on o.ID = po.OccupantID AND po.ArchivedOn is null
				WHERE o.RecieveComms = 1
				AND o.DeletedOn is null
				AND o.IsOwner = 0
				AND o.IsTenant = 0
			END
		END
	
	SELECT  o.FirstName,o.LastName,o.Email,o.Cellphone AS CellPhone,e.ErfNumber
	FROM #TEMPCELL
	INNER JOIN [system].Occupant o on o.ID = #TEMPCELL.OccupantID
	INNER JOIN [system].PropertyOccupant po ON po.OccupantID = o.ID
	INNER JOIN [system].Property p ON po.PropertyID = p.ID
	INNER JOIN [system].Erf e ON p.ERFID = e.ID
	ORDER BY o.FirstName, o.LastName


	DROP TABLE #TEMPCELL
END



GO


