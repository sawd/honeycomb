
//#region Globals
var Globals = function () {
    var globalObj = {
        cookieOptions: {
            expires: 60,
            path: '/'
        },
        paginationContainer: function (orderBy, pageSize, callBack) {
            /// <summary>
            /// An object that initialises various pagination required variables.
            /// </summary>
            /// <param name="orderBy" type="String">Optional: The field name used to order the returns set of data related to this object(defaults to '')</param>
            /// <param name="pageSize" type="int">Optional: The number of items per page to be returned. (defaults to Global.PageSize)</param>
            /// <param name="totalCount" type="int">Optional: The total number of items in the pagination (defaults to -1)</param>
            /// <param name="currentPage" type="int">Optional: The current page of pagination. (defaults to -0)</param>
            /// <returns type="String" />
            this.TotalCount = -1;
            this.CurrentPage = 0;
            this.OrderBy = orderBy || '';
            this.PageSize = pageSize || 10;
            this.PaginationID = undefined;
            this.TableID = undefined;
            this.Callback = callBack;

        },
        defaults: function () {
            $.datepicker.setDefaults({ "dateFormat": "dd/mm/yy" });
            $.blockUI.defaults.message = null;
        }
    };

    return globalObj;
}();

//#endregion

//#region Utils
var Utils = {
    masterLoadEvent: function () {
        //#region Load Defaults 
        Globals.defaults();
    	//#endregion

    	//#region HORIZONTAL NAVIGATION (AJAX/INLINE DATA)	
        $('.hornav a').click(function () {

        	//this is only applicable when window size below 450px
        	if ($(this).parents('.more').length === 0) {
        		$('.hornav li.more ul').hide();
        	}

        	//remove current menu
        	$('.hornav li').each(function () {
        		$(this).removeClass('current');
        	});

        	$(this).parent().addClass('current');	// set as current menu

        	var url = $(this).attr('href');
        	if ($(url).length > 0) {
        		$('.contentwrapper .subcontent').hide();
        		$(url).show();
        	} else {
        		$.post(url, function (data) {
        			$('#contentwrapper').html(data);
        			$('.stdtable input:checkbox').uniform();	//restyling checkbox
        		});
        	}
        	return false;
        });
    	//#endregion

        //Keep Session alive
        setInterval(Utils.keepAlive, 30000);
    },

    bindDataToGrid: function (templateContainer, template, data) {

        $(templateContainer).children().remove();

        $(template).tmpl(data).appendTo(templateContainer);

        //if ($(".loader").is(":visible"))
           // $(".loader").slideToggle();
    },
    populateEntityFromForm: function (formID, entity) {
        for (var entityProperty in entity) {
            if (entityProperty != "__type") {
                var value;
                var $currentElem = $(formID).find(String.format('[name="{0}"]',entityProperty));

                if ($currentElem.length > 0) {
                    value = $currentElem.val();

                    //detect if the element is a datpicker, if so then use the built in Jquery UI date picker to get the javascript date
                    if ($currentElem.hasClass("isDatePicker")) {
                        value = $currentElem.datepicker("getDate");
                    }
                    else if (entityProperty.toUpperCase().indexOf("INSERTEDON") > -1 || entityProperty.toUpperCase().indexOf("DELETEDON") > -1) {
                        value = null;
                    }
                    //Checkbox
                    if ($currentElem.is("input[type=checkbox]")) {
                        value = $currentElem.is(":checked");
                    }
                }
                else {
                    value = null;
                }

                if ($currentElem.val() !== "") {
                    entity[entityProperty] = value;
                }
                else {
                    entity[entityProperty] = null;
                }
            }
        }

        return entity;
    },
    responseFail: function (result) {
    	alert("An error has occured. If this continues please log out and log back in again. Otherwise report this to technical support.\n\n Error: " + result.get_message());
    	console.log("Error:" + result.get_message());
    },
    keepAlive: function () {
        if (document.location.toString().toUpperCase().indexOf("PUBLIC") === -1) {

        	var response = function (_result) {
                if (_result) {
                	//returned true, that means the session expired. Logout the user.
                	alert("Your sessions has expired or you no longer have a gate assigned to you. Log in again or contact the control room to get assigned to a gate.");
                    document.location = "/Public/Logout.aspx";
                }
            }

            Honeycomb.GateControl.KeepAlive(response, Utils.responseFail);
        }
    },
    validation: {
        validateForm: function (jqSelector, failureMessage, clearForm, isNotVisible, showErrorDialog) {
            /// <summary>Validates any DOM formindex against a 'valtype' attribute</summary>
            /// <param name="jqSelector" type="String">Jquery typed selector used to find the collection of formindexs</param>
            /// <param name="failureMessage" type="String">Message to display once validation has failed</param>
            /// <param name="clearForm" type="Boolean">When true clears all data within formindexs</param>
        	/// <param name="isNotVisible" type="Boolean">When true validates formindexs that are not visible</param>
        	if (typeof showErrorDialog != "undefined") {
        		showErrorDialog = showErrorDialog;
        	} else {
        		showErrorDialog = true;
        	}

            $(jqSelector).off("mouseover.validation");
            $(jqSelector).on("mouseover.validation", ".moreerrorinfo", null, function (e) {
                $.jGrowl("close");
                var target = $(e.currentTarget).siblings("[valmessage]");
                $.jGrowl(target.attr("valmessage"), { header: "Field Information" });
            });

            var form;
            if (isNotVisible) {
                form = $(jqSelector).find('select,input,textarea');
            }
            else {
                form = $(jqSelector).find('select,input,textarea');
            }
            formId = jqSelector;
            form = form.toArray();
            var hasErrors = false;

            if (form.length === 0 ) {
                return true;
            }

            for (var formindex = 0; formindex < form.length; formindex++) {
                if (form[formindex].getAttribute("valtype")) {

                    var toValidate = form[formindex].getAttribute("valtype").split(";");
                    var isRequired = false;
                    if (form[formindex].getAttribute("valtype").toUpperCase().indexOf("REQUIRED") > -1) {
                        isRequired = true;
                    }
                    for (var i = 0; i < toValidate.length; i++) {
                        var validate = toValidate[i].split(":");

                        switch (validate[0].trim().toUpperCase()) {
                            case "REQUIRED":
                                if (form[formindex].value === "" || form[formindex].selectedIndex == -1 || form[formindex].value == "-1" || (form[formindex].type == "radio" && $('input:radio[name="' + $(form[formindex]).attr('name') + '"]:checked').val() === undefined)) {
                                    hasErrors = true;
                                    Utils.validation.validateFail(form, formId, formindex);
                                }
                                else {
                                    Utils.validation.validatePass(form, formId, formindex);
                                }
                                break;
                            case "DATEREQ":
                                break;
                            case "REGEX":
                                if (!hasErrors) {
                                    if ((isRequired === true) || (form[formindex].value !== "")) {
                                        var pattern = Utils.validation.validateRegex(validate[1]);
                                        var val = form[formindex].value;
                                        var test = val.match(new RegExp(pattern, 'gi'));
                                        if (!test) {
                                            hasErrors = true;
                                            form[formindex].setAttribute("valmessage", Utils.validation.validateRegexMessage(validate[1]));
                                            Utils.validation.validateFail(form, formId, formindex);
                                        } else {
                                            Utils.validation.validatePass(form, formId, formindex);
                                        }
                                    }
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }
            }

            if (failureMessage === "" || failureMessage === undefined) {
                failureMessage = Utils.validation.vaildationDefaultMessage;
            }
            if (hasErrors && showErrorDialog) {
                $.jGrowl(failureMessage, { header: "<span style='color:red;font-size:13px;'>Validation Error</span>" });
            }
            else {
                $(".moreerrorinfo").off("mouseover.validation",".moreerrorinfo").remove();
            }
            
            return !hasErrors;
        },
        validatePass: function (form, formId, formindex) {
            /// <summary>Handles successful validation on an formindex</summary>
            var $currentElement = $('[name=' + form[formindex].name + ']');
            if ($currentElement.hasClass("datepicker") || $currentElement.hasClass("dpicker")) {
                $currentElement.parent(".input-type-text").removeClass("error");
            }
            else {
                $currentElement.removeClass("error");
            }

            if ($.browser.msie) {
                $currentElement.unbind("focusout");
            } else {
                $currentElement.unbind("blur");
            }
        },
        validateFail: function (form, formId, formindex) {
            /// <summary>Handles unsuccessful validation on an formindex</summary>
            var $currentElement = $('[name=' + form[formindex].name + ']');

            if ($currentElement.hasClass("datepicker") || $('#' + form[formindex].name + '').hasClass("dpicker")) {
                $currentElement.parent(".input-type-text").addClass("error");
            }
            else {
                $currentElement.addClass("error");
            }

            if (!$currentElement.attr("infoon") && $currentElement.attr("valmessage")) {
                $currentElement.attr("infoon", "true");
                $currentElement.parent().append("<a class='moreerrorinfo error'>(?)</a>");
            }
        },
        validateRegex: function (regType) {
            switch (regType.trim().toUpperCase()) {
                case "DATE":
                    return "^(((0[1-9]|[12]\\d|3[01])\\/(0[13578]|1[02])\\/((19|[2-9]\\d)\\d{2}))|((0[1-9]|[12]\\d|30)\\/(0[13456789]|1[012])\\/((19|[2-9]\\d)\\d{2}))|((0[1-9]|1\\d|2[0-8])\\/02\\/((19|[2-9]\\d)\\d{2}))|(29\\/02\\/((1[6-9]|[2-9]\\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$";
                case "STRING":
                    return "[a-z][A-Z]";
                case "INT":
                	return "^\\d+$";
                case "DOUBLE":
                    return "^(0|(-(((0|[1-9]\\d*)\\.\\d+)|([1-9]\\d*))))$";
                case "PHONE":
                    return "^[\\d]{10,12}$";
                case "PASSWORD":
                    return "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{4,12}$"; //Password matching expression. Password must be at least 4 characters, no more than 8 characters, and must include at least one upper case letter, one lower case letter, and one numeric digit
                case "EMAIL":
                    return "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$";
                default:
                    return regType;
            }
        },
        validateRegexMessage: function (regType) {
            switch (regType.trim().toUpperCase()) {
                case "STRING":
                    return "Only text characters are allowed for this field";
                case "INT":
                    return "Only numeric characters are allowed for this field";
                case "DOUBLE":
                    return "Only numeric values are allowed for this field";
                case "PHONE":
                    return "Only phone numbers are allowed for this field";
                case "EMAIL":
                    return "Only email addresses are allowed for this field";
                default:
                    return "Please enter a value for this field.";
            }
        },
        vaildationDefaultMessage:"Please review the form"
    },
    ClearForm: function () {
        for (var formindex = 0; formindex < form.length; formindex++) {
            if (form[formindex].type == "text") {
                form[formindex].value = "";
            }
        }
    },
    uploadFile: function (jqSelector) {
        $fileElement = $(jqSelector);
        if ($fileElement.length > 0) {
            var fileData = {
                fileName: $fileElement.fileName,
                file: $fileElement.attr("value")
            };
            var formData = $("<form action='/HttpHandlers/FileUpload.ashx' method='post' enctype='multipart/form-data'></form>");
            $fileElement.clone().appendTo(formData);
            formData.appendTo(document);
            formData.submit();
        }
    },
    initDataPicker: function () {
        $(".isDatePicker").datepicker({dateFormat: "dd/MM/yyyy"});
    }
};
//#endregion

//#region JQuery Extensions
//#region $.fn.mapInputsToObject - Maps form element values to an object by field.
$.fn.mapToObject = function (object, field) {
    /// <summary>
    /// Maps form element values to an object by field name.
    /// Currently only maps text, hidden, checkbox, selects but can and will be expanded
    /// </summary>
    /// <param name="object" type="object">(optional) The object to map values to / extend.</param>
    /// <param name="field" type="string">(optional) The field name to map values to.</param>
    /// <returns type="String" />
    object = object || {};
    field = field || "id";
    var inputs = this.find(':input');
    inputs.each(function () {
        var i = $(this);
        if (i.is('input[type="text"], input[type="hidden"], select')) {
            if (i.hasClass("isDatePicker")) {
                object[i.attr(field)] = i.datepicker("getDate");
            } else {
                object[i.attr(field)] = (i.val() !== "") ? i.val() : null;
            }
        }
        else if (i.is('input[type="checkbox"]')) {
            object[i.attr(field)] = i.is(":checked");
        }
        //TODO: Garth - Expand to cover more input types.
    });

    return object;
};
//#endregion
//#region $.fn.applyObjecttoInputs- Maps object values to form elements by field.
$.fn.mapObjectTo = function (object, field, overwrite) {
    /// <summary>
    /// Maps object values to form elements by field.
    /// Currently only maps text, hidden, checkbox but can and will be expanded
    /// </summary>
    /// <param name="object" type="object">The object to draw values from.</param>
    /// <param name="field" type="string">(optional) The field to use as key for value mapping.</param>
    /// <returns type="String" />
    object = object || {};
    overwrite = overwrite || false;
    field = field || "id";
    var inputs = this.find(':input, .field[' + field + ']');
    inputs.each(function () {
        var i = $(this);
        if (i.is('input[type="text"], input[type="hidden"], select')) {
            var value;

            if (object[i.attr(field)] instanceof Date) {
                value = object[i.attr(field)].format("dd/MM/yyyy");
            }
            else {
                value = object[i.attr(field)];
            }


            if (overwrite) {
                i.val(value || "");
            }
            else {
                i.val(value || i.val());
            }
        }
        else if (i.is('input[type="checkbox"]')) {
            if (object[i.attr(field)]) {
                i.attr('checked', 'checked');
            }
            else {
                i.removeAttr('checked');
            }
        }
        else if (i.is('.field')) {
            i.html(object[i.attr(field)] || "&nbsp;");
        }
        //TODO: Garth - Expand to cover more input types.
    });

    return object;
};
//#endregion
//#endregion

//#region Master Load
$(function () {
    Utils.masterLoadEvent();
});
//#endregion