/*
 * 	Additional function for forms.html
 *	Written by ThemePixels	
 *	http://themepixels.com/
 *
 *	Copyright (c) 2012 ThemePixels (http://themepixels.com)
 *	
 *	Built for Amanda Premium Responsive Admin Template
 *  http://themeforest.net/category/site-templates/admin-templates
 */

jQuery(document).ready(function(){
	
	///// FORM TRANSFORMATION /////
	jQuery('input:checkbox, input:radio, select.uniformselect, input:file').uniform();

    	
	///// TAG INPUT /////
	
	jQuery('#tags').tagsInput();
    	
	///// SELECT WITH SEARCH /////
	jQuery(".chzn-select").chosen();

    //Jquery tabs
	jQuery("#tabs").tabs();

    //Date pickers
	Utils.initDataPicker();
	
});