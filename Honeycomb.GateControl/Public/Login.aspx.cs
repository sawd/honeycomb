﻿#region Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Honeycomb.GateControl;
#endregion

namespace Honeycomb.Web.Public {
    public partial class Login : System.Web.UI.Page {
        protected void Page_Load(object sender, EventArgs e) {

            if (Request.IsAuthenticated) {
                Response.Redirect("~/Pages/GateControl.aspx");
            } else {
                ClientScript.RegisterStartupScript(GetType(), "Username Focus", "$(\"#LoginUser_UserName\").focus();", true);
            }

        }

		protected void LoginUser_LoginError(object sender, EventArgs e) { 

			if(!string.IsNullOrEmpty(SessionManager.LoginMessage)) {
				ClientScript.RegisterStartupScript(GetType(), "Invalid Login", "login.invalidLogin('" + SessionManager.LoginMessage + "');", true);
				SessionManager.LoginMessage = "";
			} else {
				ClientScript.RegisterStartupScript(GetType(), "Invalid Login", "login.invalidLogin('The username and password did not return a match, please try again.');", true);
			}
		}
    }
}