﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Honeycomb.GateControl.Public.Errors {
	public partial class Errors : System.Web.UI.Page {
		protected void Page_Load(object sender, EventArgs e) {
			string errorCode = Request["error"];
			ShowErrorControls(errorCode);

		}

		private void ShowErrorControls(string controlID) {
			switch(controlID) {
				case "404":
					Error404.Visible = true;
					break;
				case "500":
					Error500.Visible = true;
					break;
				case "403":
					Error403.Visible = true;
					break;
				default:
					Unknown.Visible = true;
					break;
			}
		}
	}
}