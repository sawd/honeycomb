﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Honeycomb.Master" AutoEventWireup="true" CodeBehind="GateControl.aspx.cs" Inherits="Honeycomb.GateControl.Pages.GateControl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<script type="text/javascript" src="/Scripts/plugins/jquery.alerts.js"></script>
	<script type="text/javascript" src="/Scripts/plugins/jquery.tmpl.min.js"></script>
	<script type="text/javascript" src="/Scripts/custom/elements.js"></script>
	<script type="text/javascript" src="/Scripts/plugins/chosen.jquery.js"></script>
	<script type="text/javascript" src="GateControl.aspx.js?date=20140825"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SiteContent" runat="server">

	<script id="entryFormTemplate" type="text/x-jquery-tmpl">
		<div id="basicform" class="subcontent">

			<div class="contenttitle2">
				<h3 style="margin-bottom: 10px;"><strong>Status: Valid for <u>Entry</u></strong></h3>
				<h3><strong>Code: ${AccessCode} - Visitor name - ${VisitorName}</strong></h3>
				<h3>Occupant: ${OccupantName} (${OccupantAddress})</h3>
			</div>
			<!--contenttitle-->

			<div class="stdform">
				<p>
					<label>Registration No</label>
					<span class="field">
						<input type="text" name="VechicleRegIn" id="VechicleRegIn" class="smallinput" /></span>
					<small class="desc">The vehicle registration no.</small>
				</p>
				<p>
					<label>No. of people</label>
					<span class="field">
						<input type="text" name="VechicleOccupantIn" id="VechicleOccupantIn" class="smallinput" /></span>
					<small class="desc">The number of people in the vehicle.</small>
				</p>
				<p class="stdformbutton">
					<button type="button" id="entrySubmit" class="submit radius2">Capture Entry</button>
				</p>
			</div>
		</div>
	</script>

	<script id="exitFormTemplate" type="text/x-jquery-tmpl">
		<div id="basicform" class="subcontent">

			<div class="contenttitle2">
				<h3 style="margin-bottom: 10px;"><strong>Status: Valid for <u>Exit</u>{{if CaptureExpiredReason}} : ${CodeValidilityPeriod} hour validility period has expired{{/if}}</strong></h3>
				<h3><strong>Code: ${AccessCode} - for Visitor name - ${VisitorName}</strong></h3>
				<h3>Occupant: ${OccupantName} (${OccupantAddress})</h3>
			</div>
			<!--contenttitle-->

			<div class="stdform">
				<p>
					<label>Registration No</label>
					<span class="field">
						<input type="text" name="VechicleRegOut" id="VechicleRegOut" class="smallinput" /></span>
					<small class="desc">The vehicle registration no.</small>
				</p>
				<p>
					<label>No. of people</label>
					<span class="field">
						<input type="text" name="VechicleOccupantOut" id="VechicleOccupantOut" class="smallinput" /></span>
					<small class="desc">The number of people in the vehicle.</small>
				</p>
				<p id="hiddenReason" style="display: none;">
					<label>Reason</label>
					<span class="field">
						<input type="text" name="Reason" id="Reason" class="smallinput" /></span>
					<small class="desc">The reason why there is a difference between the occupants exiting vs entering.</small>
				</p>
				<p id="expiredReasonDiv" style="display: none;">
					<label>Reason for late exit</label>
					<span class="field">
						<input type="text" name="ExpiredReason" id="ExpiredReason" class="smallinput" /></span>
					<small class="desc">The reason why the visitor is exiting after the ${CodeValidilityPeriod} hour validility period.</small>
				</p>
				<p class="stdformbutton">
					<button type="button" id="exitSubmit" class="submit radius2">Capture Exit</button>
				</p>
			</div>
		</div>
	</script>

	<script id="supplierEntryTemplate" type="text/x-jquery-tmpl">
		<div class="stdform">
			<p>
				<label>Company Name</label>
				<span class="field">
					<input type="text" name="CompanyName" id="CompanyName" class="smallinput" valtype="required;" /></span>
				<small class="desc">The name of the contractor company.</small>
			</p>
			<p>
				<label>Delivery Note Ref</label>
				<span class="field">
					<input type="text" name="DeliveryNoteReference" id="DeliveryNoteReference" class="smallinput" valtype="required;" /></span>
				<small class="desc">The reference number on the delivery note.</small>
			</p>
			<p>
				<label>Erf Destination</label>
				<span class="field">
					<input type="text" name="ErfDestinations" id="ErfDestinations" class="smallinput" valtype="required;" /></span>
				<small class="desc">The erf/address that the contractor is going to visit.</small>
			</p>
			<p>
				<label>Registration No</label>
				<span class="field">
					<input type="text" name="VehicleRegistration" id="VehicleRegistration" class="smallinput" valtype="required;" /></span>
				<small class="desc">The vehicle registration no.</small>
			</p>
			<p>
				<label>No. of people</label>
				<span class="field">
					<input type="text" name="VehicleOccupantsIn" id="VehicleOccupantsIn" class="smallinput" valtype="regex:INT;required;" /></span>
				<small class="desc">The number of people in the vehicle.</small>
			</p>
			<p>
				<label>Driver ID Number</label>
				<span class="field">
					<input type="text" name="DriverIDNumber" id="DriverIDNumber" class="smallinput" valtype="required;" /></span>
				<small class="desc">The driver's identity number.</small>
			</p>
			<p class="stdformbutton">
				<button type="button" id="supplierEntrySubmit" class="submit radius2">Capture Entry</button>
			</p>
		</div>
	</script>

	<script id="supplierExitRequestTemplate" type="text/x-jquery-tmpl">
		<table>
			<tr>
				<td><span class="field">
					<input type="text" id="RegistrationNumber" name="RegistrationNumber" class="smallinput" placeholder="Vehicle Registration Number" /></span></td>
				<td width="20"></td>
				<td style="vertical-align: middle;">
					<button id="RequestExitButton" type="button">Begin Exit</button>
				</td></tr>
		</table>
	</script>

	<script id="supplierExitTemplate" type="text/x-jquery-tmpl">
		<div class="stdform">
			<p>
				<label>No. of people</label>
				<span class="field">
					<input type="text" name="VehicleOccupantsOut" id="VehicleOccupantsOut" class="smallinput" valtype="regex:INT;required;" /></span>
				<small class="desc">The number of people in the vehicle.</small>
			</p>
			<p class="stdformbutton">
				<button type="button" id="supplierExitSubmit" class="submit radius2">Capture Exit</button>
			</p>
		</div>
	</script>

	<script type="text/x-jquery-template" id="occupantsTemplate">
        <option value="${ID}" cansms="${CanSMS}" cell="${Cellphone}">${FirstName} ${LastName} - ${Cellphone}</option>
	</script>

	<%-- navigation --%>
	<ul class="hornav">
		<li class="current"><a href="#visitorsIn">Visitors Access Codes</a></li>
		<li><a href="#supplierAccessEntry">Contractor Delivery/Collection</a></li>
		<li><a href="#visitorsOut">Search Occupant</a></li>
		<li><a href="#logout">Logout</a></li>
	</ul>

	<%-- contents --%>
	<div id="grid" class="contentwrapper">
		<div id="visitorsIn" class="subcontent">
			<table>
				<tr>
					<td><span class="field">
						<input type="text" id="AccessCode" name="AccessCode" class="smallinput" placeholder="Access Code" /></span></td>
					<td width="20"></td>
					<td style="vertical-align: middle;">
						<button id="GetCodeButton" type="button">Verify Code</button>
					</t></tr>
			</table>
			<div id="AccessForm"></div>
		</div>
		<div id="supplierAccessEntry" class="subcontent" style="display: none;">
			<button id="ShowEntryFormButton" type="button">Contractor Entry</button>&nbsp;&nbsp;<button id="ShowExitFormButton" type="button">Contractor Exit</button>
			<div id="SupplierAccessForm"></div>
		</div>
		<div id="supplierAccessExit" class="subcontent" style="display: none;">
		</div>
		<div id="visitorsOut" class="subcontent" style="display: none;">
			<input type="text" name="Occupants" class="smallinput" id="Occupants" />
			<div></div>
			<div id="userdet" class="contenttitle2" style="display: none; margin: 10px 0;">
				<br />
				<h3><strong>Details</strong></h3>
				<div style="width: 400px;" id="address"></div>
			</div>
		</div>
		<div id="logout" class="subcontent" style="display: none;">
			<button id="logoutbutton" onclick="location.href='/Public/logout.aspx'" type="button">Exit Gate Control</button>
		</div>
	</div>

</asp:Content>
