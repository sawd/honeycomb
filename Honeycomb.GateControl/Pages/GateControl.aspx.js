﻿/*global, jAlert,SecurityService, Utils,Honeycomb,Entities */

//#region utils
var Utility = {
	numericOnly: function (_elementID) {

		$(_elementID).keydown(function (event) {
			// Allow: backspace, delete, tab, escape, and enter
			if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
				// Allow: Ctrl+A
                (event.keyCode == 65 && event.ctrlKey === true) ||
				// Allow: home, end, left, right
                (event.keyCode >= 35 && event.keyCode <= 39)) {
				// let it happen, don't do anything
				return;
			}
			else {
				// Ensure that it is a number and stop the keypress
				if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
					event.preventDefault();
				}
			}
		});

	}
};
//#endregion

//#region Access Code Methods
var AccessCodes = {
	getCode: function () {
		var response = function (_result) {
			if (_result !== null) {

				if (_result.AccessCode !== null) {

					$("#AccessCode").val("");

					AccessCodes.CurrentAccessCode = _result;

					if (!AccessCodes.CurrentAccessCode.Used) {
						AccessCodes.ShowEntryForm();
					} else {
						AccessCodes.ShowExitForm();
					}

				} else {
					var entryForm = $("#AccessForm");
					entryForm.html("<div class=\"contenttitle2\"><h3>That code is not valid.</h3></div>");
					$("#grid").unblock();
				}

			} else {
				jAlert("The code you supplied is not valid for today.", "Code Not Valid");
			}
		};

		$("#grid").block();
		Honeycomb.GateControl.GetAccessCodeForToday($("#AccessCode").val(), response, Utils.responseFail);
	},
	CaptureEntry: function () {

		if (AccessCodes.ValidateEntry()) {

			var response = function (_result) {
				$("#grid").unblock();
				$("#AccessForm").html("<div class=\"contenttitle2\"><h3>Successfully captured entry for code " + AccessCodes.CurrentAccessCode.AccessCode + "</h3></div>");
			};


			$("#grid").block();
			Honeycomb.GateControl.CaptureEntryForAccessCode(AccessCodes.CurrentAccessCode.AccessCodeID, $("#VechicleRegIn").val(), $("#VechicleOccupantIn").val(), response, Utils.responseFail);
		}
	},
	CurrentAccessCode: null,
	ShowEntryForm: function () {
		var entryForm = $("#AccessForm");

		Utils.bindDataToGrid("#AccessForm", "#entryFormTemplate", AccessCodes.CurrentAccessCode);

		$("#entrySubmit").click(function () {
			AccessCodes.CaptureEntry();
		});

		Utility.numericOnly("#VechicleOccupantIn");

		$("#grid").unblock();

		entryForm.show();
	},
	ShowExitForm: function () {
		var entryForm = $("#AccessForm");

		Utils.bindDataToGrid("#AccessForm", "#exitFormTemplate", AccessCodes.CurrentAccessCode);

		$("#exitSubmit").click(function () {
			AccessCodes.CaptureExit();
		});

		if (AccessCodes.CurrentAccessCode.CaptureExpiredReason) {
			$("#expiredReasonDiv").show();
		}

		Utility.numericOnly("#VechicleOccupantOut");

		$("#grid").unblock();

		entryForm.show();
	},
	CaptureExit: function () {

		if (AccessCodes.validateExit()) {
			var response = function (_result) {
				if (_result) {
					$("#AccessForm").html("<div class=\"contenttitle2\"><h3>Successfully captured exit for code " + AccessCodes.CurrentAccessCode.AccessCode + "</h3></div>");
				} else {
					$("#AccessForm").html("<div class=\"contenttitle2\"><h3>There was an error when capturing exit for code " + AccessCodes.CurrentAccessCode.AccessCode + "</h3></div>");
				}
			};

			Honeycomb.GateControl.CaptureExitForAccessCode(AccessCodes.CurrentAccessCode.AccessCodeID, $("#VechicleRegOut").val(), $("#VechicleOccupantOut").val(), $("#Reason").val(), $("#ExpiredReason").val(), response, Utils.responseFail);
		}

	},
	validateExit: function () {
		var isValid = true;
		var vechicleOccupantsOut = $("#VechicleOccupantOut").val();
		var message = "";

		if ($("#VechicleRegOut").val() === "") {
			isValid = false;
			message += "You must enter the vehicle's registration number.\n";
		}


		if ($("#Reason").val() === "" || ($("#Reason").length === 0)) {

			if (parseInt(vechicleOccupantsOut, 10) != parseInt(AccessCodes.CurrentAccessCode.VechicleOccupantIn, 10)) {
				message += "Please provide a reason for the discrepancy between occupants in and occupants out.";
				$("#hiddenReason").show("slow");
				isValid = false;
			} else {
				$("#hiddenReason").hide("slow");
			}

		}

		//check the validility period expiry reason
		if (AccessCodes.CurrentAccessCode.CaptureExpiredReason) {

			if ($("#ExpiredReason").val().replace(" ","") === "") {
				message += "\n Please provide a reason for the late exit of the visitor.";
				isValid = false;
			}
		}

		if (!isValid) {
			alert(message);
		}

		return isValid;
	},
	ValidateEntry: function () {
		var isValid = true;
		var vechicleOccupantsIn = $("#VechicleOccupantIn").val();
		var message = "";

		if ($("#VechicleRegIn").val() === "") {
			isValid = false;
			message += "You must enter the vehicle's registration number.\n";
		}

		if (isNaN(vechicleOccupantsIn) || vechicleOccupantsIn === "") {
			isValid = false;
			message += "You must provide the number of occupants.";
		}

		if (!isValid) {
			alert(message);
		}

		return isValid;
	},
	getOccupants: function () {
		var response = function (result) {

			$("#occupantsTemplate").tmpl(result.EntityList).appendTo("#Occupants");
			$("#Occupants").trigger("liszt:updated");
			//$(".loader").toggle(false);
		};

		Honeycomb.GateControl.GetOccupants(response, Utils.responseFail);
	}
};
//#enregion

var SupplierAccess = {
	init: function () {
		//init
		$("#ShowEntryFormButton").click(SupplierAccess.showEntryForm);
		$("#ShowExitFormButton").click(SupplierAccess.showExitRequestForm);
	},
	showEntryForm: function () {
		Utils.bindDataToGrid("#SupplierAccessForm", "#supplierEntryTemplate", {});
		Utility.numericOnly("#SupplierAccessForm #VehicleOccupantsIn");
		$("#supplierEntrySubmit").click(SupplierAccess.processEntry);
	},
	showExitRequestForm: function () {
		Utils.bindDataToGrid("#SupplierAccessForm", "#supplierExitRequestTemplate", {});
		$("#RequestExitButton").click(SupplierAccess.requestExitInfo);
	},
	processEntry: function () {
		if (Utils.validation.validateForm("#SupplierAccessForm", "", false, false, false)) {
			var supplierAccessRequest = new Entities.SupplierAccess();
			$("#SupplierAccessForm").mapToObject(supplierAccessRequest, name);
			supplierAccessRequest.EnteredOn = new Date();
			$("#SupplierAccessForm").block();
			Honeycomb.GateControl.CaptureSupplierEntry(supplierAccessRequest, SupplierAccess.events.processEntry_SUCCESS, Utils.responseFail);
		} else {
			alert("There is a validation error, all fields are required and No. of people must be a number!");
		}
	},
	requestExitInfo: function () {

		var $form = $("#SupplierAccessForm");

		if ($form.find("#RegistrationNumber").val() !== "") {
			$("#SupplierAccessForm").block();
			Honeycomb.GateControl.GetSupplierAccessForExit($form.find("#RegistrationNumber").val(), SupplierAccess.events.requestExitInfo_SUCCESS, Utils.responseFail);
		}
	},
	processExit: function () {
		if (Utils.validation.validateForm("#SupplierAccessForm", "", false, false, false)) {

			var exitData = $("#SupplierAccessForm").data("ExitData");
			var occupantsOut = $("#SupplierAccessForm").find("#VehicleOccupantsOut").val();

			if (exitData !== null) {
				$("#SupplierAccessForm").block();
				Honeycomb.GateControl.CaptureSupplierExit(exitData.SupplierAccessID, occupantsOut, SupplierAccess.events.processExit_SUCCESS, Utils.responseFail);
			}

		} else {
			alert("Please enter the number of occupants and only use numbers.");
		}
	},
	showExitForm: function(){
		Utils.bindDataToGrid("#SupplierAccessForm", "#supplierExitTemplate", {});
		Utility.numericOnly("#SupplierAccessForm #VehicleOccupantsOut");
		$("#supplierExitSubmit").click(SupplierAccess.processExit);
	},
	events: {
		processEntry_SUCCESS: function(result){

			$("#SupplierAccessForm").unblock();

			if (result.success) {
				var supplierAccessRequest = new Entities.SupplierAccess();
				$("#SupplierAccessForm").mapObjectTo(supplierAccessRequest, "name", true);
				alert("Entry was successfully processed for the vehicle, you may allow them onto the premises.");
			} else {
				alert(result.message);
			}
		},
		requestExitInfo_SUCCESS: function(result){
			
			$("#SupplierAccessForm").unblock();

			if(result.Success){

				$("#SupplierAccessForm").data("ExitData", result);
				SupplierAccess.showExitForm();

			}else {
				alert(result.Message);
			}
		},
		processExit_SUCCESS: function(result){
			$("#SupplierAccessForm").unblock();
			$("#SupplierAccessForm").removeData("ExitData");
			SupplierAccess.showExitRequestForm();
			alert(result.message);
		}
	}
};

//#region page methods
var Page = {
	load: function () {
		Page.bindevents();
		//AccessCodes.getOccupants();
		///// SELECT WITH SEARCH /////
		$("#Occupants").autocomplete({
			source: function (request, response) {
				var requestTown = function (result) {
					if (result === null) { result = []; }
					response(result);
				};

				Honeycomb.GateControl.GetOccupants(request.term, requestTown, Utils.responseFail);
			}
            , minLength: 3
            , select: function (event, ui) {
				var occupantID = ui.item.value;
				//$("#Occupants").val(ui.item.label);

				var response = function (_result) {
					if (_result !== null) {
						var currentOccupant = _result;
						$("#userdet").show("slow");
						$("#address").html("<h3>" + currentOccupant.FirstName + " " + currentOccupant.LastName + " " + currentOccupant.Cellphone + "</h3><br/>" + "<h3><strong>Address</strong></h3><h3>" + currentOccupant.Address + "</h3>");
					}

					$("#visitorsOut").unblock();
				};

				$("#visitorsOut").block();
				Honeycomb.GateControl.GetOccupant(occupantID, response, Utils.responseFail);

				return false;

            }
		});


	},
	bindevents: function () {
		$("#GetCodeButton").click(function () {
			AccessCodes.getCode();
		});

		SupplierAccess.init();
	}
};
//#endregion 

//execute on document ready
$(function () {
	Page.load();
});