﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Honeycomb.GateControl;
using Honeycomb.GateControl.HoneycombAPI;

namespace Honeycomb.GateControl.Services {
	[ServiceContract(Namespace = "Honeycomb")]
	[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
	public class GateControl {
		// To use HTTP GET, add [WebGet] attribute. (Default ResponseFormat is WebMessageFormat.Json)
		// To create an operation that returns XML,
		//     add [WebGet(ResponseFormat=WebMessageFormat.Xml)],
		//     and include the following line in the operation body:
		//         WebOperationContext.Current.OutgoingResponse.ContentType = "text/xml";
		#region Authenticate
		private static string APIUserName = System.Configuration.ConfigurationManager.AppSettings["APIUserName"],
							  APIPassword = System.Configuration.ConfigurationManager.AppSettings["APIPassword"],
							  APIKey = System.Configuration.ConfigurationManager.AppSettings["APIKey"];

		[OperationContract]
		public static async Task<bool> AuthenticateAsync() {

			bool authenticated = false;
			var API = new HoneycombAPI.HoneycombAPIClient();
			try {
				string APIUserName = System.Configuration.ConfigurationManager.AppSettings["APIUserName"],
					   APIPassword = System.Configuration.ConfigurationManager.AppSettings["APIPassword"],
					   APIKey = System.Configuration.ConfigurationManager.AppSettings["APIKey"];
				API.Open();
				var response = await API.AuthenticateAsync(APIUserName, APIPassword, APIKey);
				API.Close();
				if(response != null) {
					authenticated = true;
					if(HttpContext.Current == null) {
						HttpContext.Current = new HttpContext(
						new HttpRequest("", "http://tempuri.org", ""),
						new HttpResponse(new System.IO.StringWriter())
						);
					}
					CacheManager.APISecurityToken = response;
				}
			} catch(CommunicationException ex) {
				Error.ServiceError(ex);
			} catch(TimeoutException) {
				API.Abort();
			} catch(Exception ex) {
				Error.ServiceError(ex);
			} finally {

			}
			return authenticated;
		}

		public static bool Authenticate() {
			bool authenticated = false;
			if(string.IsNullOrEmpty(CacheManager.APISecurityToken)) {

				var API = new HoneycombAPI.HoneycombAPIClient();
				try {
					API.Open();
					var response = API.Authenticate(APIUserName, APIPassword, APIKey);
					API.Close();
					if(response != null) {
						authenticated = true;
						CacheManager.APISecurityToken = response;
					}
				} catch(CommunicationException ex) {
					Error.ServiceError(ex);
				} catch(TimeoutException) {
					API.Abort();
				} catch(Exception ex) {
					API.Abort();
					Error.ServiceError(ex);
				} finally {
				}
			} else {
				authenticated = true;
			}
			return authenticated;
		}

		public async static Task<long> CheckHoneycombGateUser(string UserName, string Password) {
			long retval = -1;
			var API = new HoneycombAPI.HoneycombAPIClient();
			try {
				Authenticate();
				API.Open();
				retval = API.AuthenticateGuard(APIKey, UserName, Password);
				API.Close();
			} catch(CommunicationException ex) {
				API.Abort();
				Error.ServiceError(ex);
			} catch(TimeoutException ex) {
				API.Abort();
				throw (ex);
			} catch(Exception ex) {
				API.Abort();
				Error.ServiceError(ex);
			} finally {

			}
			return retval;
		}
		#endregion

		[OperationContract]
		public async Task<VisitorAndAcccessCode> GetAccessCodeForToday(string code) {
			VisitorAndAcccessCode retVisitorAndCode = null;
			Utility.CheckSession();

			var API = new HoneycombAPI.HoneycombAPIClient();
			try {
				Authenticate();
				API.Open();
				retVisitorAndCode = await API.GetAccessCodeForTodayAsync(APIKey, code);
				API.Close();
			} catch(CommunicationException ex) {
				API.Abort();
				Error.ServiceError(ex);
			} catch(TimeoutException ex) {
				API.Abort();
				throw (ex);
			} catch(Exception ex) {
				API.Abort();
				Error.ServiceError(ex);
			} finally {

			}
			return retVisitorAndCode;
		}

		/// <summary>
		/// This method implements the API and requires the current Guards ID to be passed in.
		/// </summary>
		/// <param name="SecurityToken"></param>
		/// <param name="AccessCodeID"></param>
		/// <param name="VechicleRegIn"></param>
		/// <param name="VechicleOccupantIn"></param>
		/// <param name="GuardID"></param>
		/// <returns></returns>
		[OperationContract]
		public async Task<bool> CaptureEntryForAccessCode(Guid AccessCodeID, string VechicleRegIn, int VechicleOccupantIn) {
			bool retAccessCode = false;
			Utility.CheckSession();

			var API = new HoneycombAPI.HoneycombAPIClient();

			try {
					Authenticate();
					API.Open();
					retAccessCode = API.CaptureEntryForAccessCode(APIKey, AccessCodeID, VechicleRegIn, VechicleOccupantIn, SessionManager.UserID, null);

			} catch(CommunicationException ex) {
				API.Abort();
				Error.ServiceError(ex);
			} catch(TimeoutException ex) {
				API.Abort();
				throw (ex);
			} catch(Exception ex) {
				API.Abort();
				Error.ServiceError(ex);
			} finally {

			}

			return retAccessCode;
		}

		[OperationContract]
		public async Task<bool> CaptureExitForAccessCode(Guid AccessCodeID, string VehicleRegOut, int VehicleOccupantOut, string ReasonForDiscrepancy = "", string lateExitReason = "") {
			bool retAccessCode = false;
			Utility.CheckSession();

			var API = new HoneycombAPI.HoneycombAPIClient();

			try {
				Authenticate();
				API.Open();
                retAccessCode = await API.CaptureExitForAccessCodeAsync(APIKey, AccessCodeID, VehicleRegOut, VehicleOccupantOut, SessionManager.UserID, ReasonForDiscrepancy, lateExitReason, null);

			} catch(CommunicationException ex) {
				API.Abort();
				Error.ServiceError(ex);
			} catch(TimeoutException ex) {
				API.Abort();
				throw (ex);
			} catch(Exception ex) {
				API.Abort();
				Error.ServiceError(ex);
			} finally {

			}

			return retAccessCode;
		}


		[OperationContract]
		public async Task<AutoCompleteObject[]> GetOccupants(string searchString) {
			AutoCompleteObject[] retAutoCompelteObject = null;
			Utility.CheckSession();

			var API = new HoneycombAPI.HoneycombAPIClient();

			try {
				Authenticate();
				API.Open();

				retAutoCompelteObject = await API.GetOccupantsAsync(APIKey, searchString);

			} catch(CommunicationException ex) {
				API.Abort();
				Error.ServiceError(ex);
			} catch(TimeoutException ex) {
				API.Abort();
				throw (ex);
			} catch(Exception ex) {
				API.Abort();
				Error.ServiceError(ex);
			} finally {

			}

			return retAutoCompelteObject;
		}


		[OperationContract]
		public async Task<CustomOccupant> GetOccupant(long OccupantID) {
			CustomOccupant retCustomOccupant = null;
			Utility.CheckSession();

			var API = new HoneycombAPI.HoneycombAPIClient();

			try {
				Authenticate();
				API.Open();

				retCustomOccupant = await API.GetOccupantAsync(APIKey, OccupantID);

			} catch(CommunicationException ex) {
				API.Abort();
				Error.ServiceError(ex);
			} catch(TimeoutException ex) {
				API.Abort();
				throw (ex);
			} catch(Exception ex) {
				API.Abort();
				Error.ServiceError(ex);
			} finally {

			}

			return retCustomOccupant;
		}

		#region Supplier Access Methods
		[OperationContract]
		public async Task<ProcessResponse> CaptureSupplierEntry(SupplierAccess supplierAccessEntry) {
			ProcessResponse pRes = null;
			Utility.CheckSession();

			var API = new HoneycombAPI.HoneycombAPIClient();

			try {
				Authenticate();
				API.Open();

				pRes = await API.CaptureSupplierEntryAsync(APIKey, supplierAccessEntry, SessionManager.UserID);

			} catch(CommunicationException ex) {
				API.Abort();
				Error.ServiceError(ex);
			} catch(TimeoutException ex) {
				API.Abort();
				throw (ex);
			} catch(Exception ex) {
				API.Abort();
				Error.ServiceError(ex);
			} finally {

			}

			return pRes;
		}

		[OperationContract]
		public async Task<SupplierAccessExit> GetSupplierAccessForExit(string vehicleRegistrationNumber) {
			SupplierAccessExit supplierAccessRet = null;
			Utility.CheckSession();

			var API = new HoneycombAPI.HoneycombAPIClient();

			try {
				Authenticate();
				API.Open();

				supplierAccessRet = await API.GetSupplierAccessForExitAsync(APIKey, vehicleRegistrationNumber, SessionManager.UserID);

			} catch(CommunicationException ex) {
				API.Abort();
				Error.ServiceError(ex);
			} catch(TimeoutException ex) {
				API.Abort();
				throw (ex);
			} catch(Exception ex) {
				API.Abort();
				Error.ServiceError(ex);
			} finally {

			}

			return supplierAccessRet;
		}

		[OperationContract]
		public async Task<ProcessResponse> CaptureSupplierExit(long supplierAccessID, int numberOfOccupants) {
			ProcessResponse pRes = null;
			Utility.CheckSession();

			var API = new HoneycombAPI.HoneycombAPIClient();

			try {
				Authenticate();
				API.Open();

				pRes = await API.CaptureSupplierExitAsync(APIKey, supplierAccessID, numberOfOccupants, SessionManager.UserID);

			} catch(CommunicationException ex) {
				API.Abort();
				Error.ServiceError(ex);
			} catch(TimeoutException ex) {
				API.Abort();
				throw (ex);
			} catch(Exception ex) {
				API.Abort();
				Error.ServiceError(ex);
			} finally {

			}

			return pRes;
		}

		[OperationContract]
		public bool KeepAlive() {
			bool logoutUser = false;

			var API = new HoneycombAPI.HoneycombAPIClient();

			try {
				Authenticate();
				API.Open();

				//check if the session has expired, if so logout the user through the correct logout procedure.
				try {
					Utility.CheckSession();
				}catch(Exception ex){
					logoutUser = true;
				}

				//only check if the guard has a gate if the session is intact
				if(!logoutUser) {
					//log out the user if the gaurd does not have a gate
					logoutUser = !API.GuardHasGate(APIKey, SessionManager.UserID);
				}

			} catch(CommunicationException ex) {
				API.Abort();
				Error.ServiceError(ex);
			} catch(TimeoutException ex) {
				API.Abort();
				throw (ex);
			} catch(Exception ex) {
				API.Abort();
				Error.ServiceError(ex);
			} finally {

			}

			return logoutUser;
		}

		#endregion
	}
}
