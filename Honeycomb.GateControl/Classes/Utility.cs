﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Mail;
using System.Web;

namespace Honeycomb.GateControl {

	public sealed class EntityTransport<T> {
		public List<T> EntityList { get; set; }
		public int Count { get; set; }
		public string Message { get; set; }
		public string EntityType { get; set; }
	}

	public static class Utility {

		/// <summary>
		/// Converts an object to the specified type or returns the specified 
		/// null replacement if it is null.
		/// </summary>
		/// <remarks>
		/// </remarks>
		/// <typeparam name="T">Type of object to be returned</typeparam>
		/// <param name="value">Value to be checked</param>
		/// <param name="replacement">Replacement if value is null</param>
		/// <returns>Value as T or replacement value if null</returns>
		public static T ConvertNull<T>(object value, T replacement) {
			if((value == null) || (value == DBNull.Value))
				return replacement;
			else
				return (T)Convert.ChangeType(value, typeof(T));
		}

		/// <summary>
		/// Returns an entity collection with/without an error 
		/// </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		public static EntityTransport<T> ServiceResponse<T>(ref List<T> data, Exception error = null) {

			if(error != null) {
				SendExceptionMail(error);
			}

			return new EntityTransport<T> {
				EntityList = data,
				Count = data.Count,
				Message = error == null ? string.Empty : error.Message,
				EntityType = (data.GetType().GenericTypeArguments[0]).FullName
			};
		}

		public static EntityTransport<T> ServiceResponse<T>(ref T data, Exception error = null) {
			var entityListItems = new List<T>();
			entityListItems.Add(data);

			if(error != null) {
				SendExceptionMail(error);
			}

			return ServiceResponse<T>(ref entityListItems);
		}

		public static EntityTransport<T> ServiceResponse<T>(ref List<T> data, int totalCount, Exception error = null) {

			if(error != null) {
				SendExceptionMail(error);
			}

			return new EntityTransport<T> {
				EntityList = data,
				Count = totalCount,
				Message = error == null ? string.Empty : error.Message,
				EntityType = (data.GetType().GenericTypeArguments[0]).FullName
			};
		}

		

		public static void CheckSession() {
			if(!SessionManager.isLoggedIn)
				throw new Exception("SessionExpired");
		}
		
		/// <summary>
		/// Extends IQueryable to allow an optional where to be applied
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="source"></param>
		/// <param name="condition"></param>
		/// <param name="predicate"></param>
		/// <returns></returns>
		public static IQueryable<T> OptionalWhere<T>(this IQueryable<T> source, bool condition, Expression<Func<T, bool>> predicate) {
			return condition ? source.Where(predicate) : source;
		}
		/// <summary>
		/// Extends IQueryable to iterate a collection
		/// </summary>
		/// <typeparam name="T"> </typeparam>
		/// <param name="enumeration">collecttion</param>
		/// <param name="action"></param>
		public static void ForEach<T>(this IEnumerable<T> enumeration, Action<T> action) {
			foreach(T item in enumeration) {
				action(item);
			}
		}

		/// <summary>
		/// Sends an email from the systems settings from email address
		/// </summary>
		/// <param name="toEmail">Email Recipient</param>
		/// <param name="subject">Subject of the email</param>
		/// <param name="message">Email message</param>
		public static void SendEmail(string toEmail, string subject, string message) {
			Guid commEngineID = Guid.Parse(ConfigurationManager.AppSettings["CommEngineID"].ToString());
			
				var systemID = new Guid();

				systemID = Guid.Parse(ConfigurationManager.AppSettings["SystemID"].ToString());

				var fromEmailAddress = ConfigurationManager.AppSettings["FromEmailAddress"].ToString();

				
					MailMessage MyMailMessage = new MailMessage();
					MyMailMessage.From = new MailAddress(fromEmailAddress);
					MyMailMessage.To.Add(toEmail);
					MyMailMessage.Subject = subject;
					MyMailMessage.IsBodyHtml = true;

					MyMailMessage.Body = message;

					SmtpClient SMTPServer = new SmtpClient(ConfigurationManager.AppSettings["SMTPServer"]);

					try {
						SMTPServer.Send(MyMailMessage);

					} catch(Exception) { }
				
			
		}

		public static void SendExceptionMail(Exception ex, String message = "") {

			if(ex.Message != "SessionExpired") {

				try {
					string userDetails = "";
					string page = "";

					if(SessionManager.isLoggedIn == true) {
						userDetails = SessionManager.UserName + "<br/>";
					} else {
						userDetails = "Honeycomb System <br/>";
					}

					userDetails += string.Format("{0}<br/>{1}<br/><br/>",
												 HttpContext.Current.Request.UserAgent,
												 HttpContext.Current.Request.UserHostAddress);
					var paramses = HttpContext.Current.Request.Params;
					foreach(var item in paramses.AllKeys) {
						userDetails += item + " : " + paramses[item] + "<br/>";
					}

					page = HttpContext.Current.Request.Url.OriginalString;
					string request = string.Empty;
					string jsonRequest = new StreamReader(HttpContext.Current.Request.InputStream).ReadToEnd();

					SendEmail(ConfigurationManager.AppSettings["ErrorsEmail"], "Error occured on HoneyComb - " + System.Environment.MachineName,
							string.Format("An exception occurred on: {0}<br/>{8}<br/><br/><b>Message:</b><br/>{6}<br/>{1}<br/><br/><b>Page:</b><br/>{7}<br/><br/> <b>Stacktrace:</b><br/> {2} <br/><br/> <b>InnerException:<br/></b>{3} <br/><br/><b>Request Body:</b><br/>{4}<br/><br/><br/><b>User Details:</b><br/> {5}",
										System.Environment.MachineName,
										ex.Message,
										ex.StackTrace,
										ex.InnerException,
										HttpContext.Current.Server.HtmlEncode(request),
										userDetails,
										message,
										page,
										DateTime.Now.ToString()));


				} catch(Exception exception) {
					return;
				}

				return;
			}
		}

	}
}