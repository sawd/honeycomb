﻿#region Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration.Provider;
using System.Web.Security;
using System.Web;
using System.Data.Entity;
#endregion

namespace Honeycomb.GateControl.Security {
    public class Membership : MembershipProvider {

        #region Fields
        private string _applicationName;
        private int _maxInvalidPasswordAttempts;
        private int _minRequiredNonAlphanumericCharacters;
        private int _minRequiredPasswordLength;
        private string _passwordStrengthRegularExpression;
        private bool _requiresQuestionAndAnswer;
        private bool _enablePasswordRetrieval;
        private bool _enablePasswordReset;
        private MembershipPasswordFormat _passwordFormat;
        #endregion

        /// <summary>
        /// Performs initialization of the membership provider based on customized requirements from the web.config membership settings
        /// </summary>
        /// <param name="name"></param>
        /// <param name="config"></param>
        public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config) {
            base.Initialize(name, config);
            _applicationName = GetValueFromConfigFile(config["applicationName"], "");
            _maxInvalidPasswordAttempts = Convert.ToInt32(GetValueFromConfigFile(config["maxInvalidPasswordAttempts"], "3"));
            _minRequiredNonAlphanumericCharacters = Convert.ToInt32(GetValueFromConfigFile(config["minRequiredNonAlphanumericCharacters"], "1"));
            _minRequiredPasswordLength = Convert.ToInt32(GetValueFromConfigFile(config["minRequiredPasswordLength"], "6"));
            _passwordStrengthRegularExpression = GetValueFromConfigFile(config["passwordStrengthRegularExpression"], "");
            _requiresQuestionAndAnswer = Convert.ToBoolean(GetValueFromConfigFile(config["requiresQuestionAndAnswer"], "true"));
            _enablePasswordRetrieval = Convert.ToBoolean(GetValueFromConfigFile(config["enablePasswordRetrieval"], "true"));
            _enablePasswordReset = Convert.ToBoolean(GetValueFromConfigFile(config["enablePasswordReset"], "true"));
            _passwordFormat = MembershipPasswordFormat.Encrypted;
        }

        /// <summary>
        /// Checks if configuration file value is null, if it is, then this method will return the default value
        /// </summary>
        /// <param name="configValue">Value from config file</param>
        /// <param name="defaultValue">Default value to return if config value is null</param>
        /// <returns>Returns string containing either config or default value</returns>
        private string GetValueFromConfigFile(string configValue, string defaultValue) {
            if (string.IsNullOrEmpty(configValue)) {
                return defaultValue;
            }
            return configValue;
        }
        public override string ApplicationName {
            get { return _applicationName; }
            set { _applicationName = value; }
        }

        public override bool ChangePassword(string username, string oldPassword, string newPassword) {
            throw new NotImplementedException();
        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer) {
            throw new NotImplementedException();
        }

        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status) {
            throw new NotImplementedException();
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData) {
            throw new NotImplementedException();
        }

        public override bool EnablePasswordReset {
            get { return _enablePasswordReset; }
        }

        public override bool EnablePasswordRetrieval {
            get { return _enablePasswordRetrieval; }
        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords) {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords) {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords) {
            throw new NotImplementedException();
        }

        public override int GetNumberOfUsersOnline() {
            throw new NotImplementedException();
        }

        public override string GetPassword(string username, string answer) {
            throw new NotImplementedException();
        }

        public override MembershipUser GetUser(string username, bool userIsOnline) {
            throw new NotImplementedException();
        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline) {
            throw new NotImplementedException();
        }

        public override string GetUserNameByEmail(string email) {
            throw new NotImplementedException();
        }

        public override int MaxInvalidPasswordAttempts {
            get { return _maxInvalidPasswordAttempts; }
        }

        public override int MinRequiredNonAlphanumericCharacters {
            get { return _minRequiredNonAlphanumericCharacters; }
        }

        public override int MinRequiredPasswordLength {
            get { return _minRequiredPasswordLength; }
        }

        public override int PasswordAttemptWindow {
            get { throw new NotImplementedException(); }
        }

        public override MembershipPasswordFormat PasswordFormat {
            get { return _passwordFormat; }
        }

        public override string PasswordStrengthRegularExpression {
            get { return _passwordStrengthRegularExpression; }
        }

        public override bool RequiresQuestionAndAnswer {
            get { return _requiresQuestionAndAnswer; }
        }

        public override bool RequiresUniqueEmail {
            get { throw new NotImplementedException(); }
        }

        public override string ResetPassword(string username, string answer) {
            throw new NotImplementedException();
        }

        public override bool UnlockUser(string userName) {
            throw new NotImplementedException();
        }

        public override void UpdateUser(MembershipUser user) {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Authenticate a user
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public override bool ValidateUser(string username, string password) {

				//password = Security.Encryption.Encrypt(password);
				long userID = Honeycomb.GateControl.Services.GateControl.CheckHoneycombGateUser(username, password).Result;

				if(userID != -1) {
					//user has validated, set up the session variables
					SessionManager.UserID = userID;
					SessionManager.UserName = username;
					SessionManager.isLoggedIn = true;
				} else {
					SessionManager.LoginMessage = "Either your username or password is incorrect or you have not been assigned a gate, please try again.";
					SessionManager.isLoggedIn = false;
				}

				return userID != -1 ? true : false;
            
        }
    }
}
