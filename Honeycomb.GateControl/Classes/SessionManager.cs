﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Honeycomb.GateControl;

namespace Honeycomb.GateControl {
	public class SessionManager : SAWD.WebManagers.SessionManagerBase {


		/// <summary>
		/// The number of failed login attempts 
		/// </summary>        
		public static int LoginAttemptCounter {
			get { return (int)(GetSession("LoginAttemptCounter", 0)); }
			set { SetSession("LoginAttemptCounter", value); }
		}


		/// <summary>
		/// Check to if the user is logged in
		/// </summary>        
		public static bool isLoggedIn {
			get { return (bool)(GetSession("isLogedIn", false)); }
			set { SetSession("isLogedIn", value); }
		}

		/// <summary>
		/// Check to if the user is logged in
		/// </summary>        
		public static string sessionID {
			get { return (string)(GetSession("sessionID", false)); }
			set { SetSession("sessionID", value); }
		}

		/// <summary>
		/// The username of the current logged in user
		/// </summary>        
		public static string UserName {
			get { return (string)(GetSession("UserName")); }
			set { SetSession("UserName", value); }
		}

		/// <summary>
		/// The ID of the current logged in user
		/// </summary>        
		public static long UserID {
			get { return (long)(GetSession("UserID")); }
			set { SetSession("UserID", value); }
		}


		public static string LoginMessage {
			get { return Utility.ConvertNull(GetSession("LoginMessage"), String.Empty); }
			set { SetSession("LoginMessage", value); }
		}
	}
}
