﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;

namespace Honeycomb.GateControl {
	public class CacheManager : SAWD.WebManagers.CacheManagerBase {

		public static string APISecurityToken {
			get {
				string retVal = (string)(GetCache(" APISecurityToken", string.Empty));
				return retVal;
			}
			set {
				SetCache(" APISecurityToken", value, new TimeSpan(0, 30, 0));
			}
		}

	}
}
