﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Honeycomb.GateControl.Services {
	public class Error {
		public static void ServiceError(Exception ex) {
			if(ex.Message.Contains("Your session has timed out") || ex.Message.Contains("authenticate")) {
				if(HttpContext.Current == null) {
					HttpContext.Current = new HttpContext(
					new HttpRequest("", "http://tempuri.org", ""),
					new HttpResponse(new System.IO.StringWriter())
					);
				}

				GateControl.AuthenticateAsync();
			} else {
				throw ex;
			}
		}
	}
}