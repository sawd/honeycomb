﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace Honeycomb.GateControl.Page {
	public class BasePage: System.Web.UI.Page {

		protected override void OnLoad(EventArgs e) {
			if(SessionManager.isLoggedIn == false) {
				Session.Clear();
				Session.Abandon();
				FormsAuthentication.SignOut();
				Response.Redirect("~/Public/Login.aspx");
			}

			if(!Request.IsAuthenticated) {
				FormsAuthentication.RedirectToLoginPage();
			}
			
			base.OnLoad(e);
		}

	}
}