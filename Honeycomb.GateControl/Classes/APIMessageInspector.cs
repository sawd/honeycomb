﻿#region Directives
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Configuration;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Web;
using System.Xml;
#endregion

namespace Honeycomb.GateControl.Services {
	class APIMessageInspector : IClientMessageInspector {

        #region IClientMessageInspector Members

        public void AfterReceiveReply(ref Message reply, object correlationState) {
            //throw new NotImplementedException();
        }

        public object BeforeSendRequest(ref Message request, IClientChannel channel) {
			//create a new http request message to send to Juice System
			HttpRequestMessageProperty channelRequestMessageProperty = new HttpRequestMessageProperty();

			//Apply Http Request Header if cached object is not null
			if(!string.IsNullOrEmpty(Honeycomb.GateControl.CacheManager.APISecurityToken)) {
				//Add Headers
				channelRequestMessageProperty.Headers["ASP.NET_SessionId"] = Honeycomb.GateControl.CacheManager.APISecurityToken;
				channelRequestMessageProperty.Headers["Cookie"] = String.Format("ASP.NET_SessionId={0}", Honeycomb.GateControl.CacheManager.APISecurityToken);
				//Append to current Message Request
				request.Properties[HttpRequestMessageProperty.Name] = channelRequestMessageProperty;
			}

			return request;
        } 
        #endregion
    }

	public class APIServiceOutputBehavior : BehaviorExtensionElement, IEndpointBehavior {


		public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters) {

		}

		public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime) {
			clientRuntime.MessageInspectors.Add(new APIMessageInspector());
		}
		
		public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher) {

		}

		public void Validate(ServiceEndpoint endpoint) {

		}

		public override Type BehaviorType {
			get {
				return typeof(APIServiceOutputBehavior);
			}
		}

		protected override object CreateBehavior() {
			return new APIServiceOutputBehavior();
		}
	}
}

