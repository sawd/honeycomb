﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Honeycomb.Custom;
using Honeycomb.Custom.Data;
using Honeycomb.Property;
using System.Web.SessionState;

namespace Honeycomb.Web.Httphandlers {
	/// <summary>
	/// Summary description for upload
	/// </summary>
	public class upload : IHttpHandler, IReadOnlySessionState {

		public void ProcessRequest(HttpContext context) {
			context.Response.ContentType = "application/json";

			HttpPostedFile file = context.Request.Files["upload"];
			ProcessResponse pRes = new ProcessResponse();
			WCFProperty clientService = new WCFProperty();

			if(file != null) {

				try {

					int entityID = 0;

					if(int.TryParse(context.Request["entityID"], out entityID) && !string.IsNullOrEmpty(context.Request["type"]) && !string.IsNullOrEmpty(context.Request["label"])) {

						var fileToUpload = new File();

						fileToUpload.FileID = Guid.NewGuid();
						fileToUpload.Type = context.Request["type"];
						fileToUpload.FileName = file.FileName.Split('\\').Last<string>();
						fileToUpload.Label = context.Request["label"];
						fileToUpload.FileData = ReadToEnd(file.InputStream);

						pRes = clientService.UpdateFile(fileToUpload, entityID);

					} else {
						pRes.success = false;
						pRes.message = "There is missing post data, required post data is, file: upload, string: type, string: label, int: entityID.";
					}

				}catch(Exception ex){
					Utility.SendExceptionMail(ex, "File upload error.");
					pRes.success = false;
					pRes.message = "File upload error";
				}

			} else {
				pRes.success = false;
				pRes.message = "File not included in upload";
			}

			context.Response.Write("{\"Data\": {\"success\": "+ pRes.success.ToString().ToLower() +", \"message\":\""+ pRes.message +"\"}}");	

			context.Response.End();
		}

		public static byte[] ReadToEnd(System.IO.Stream stream) {
			long originalPosition = stream.Position;
			stream.Position = 0;
			try {
				byte[] readBuffer = new byte[4096];
				int totalBytesRead = 0;
				int bytesRead;
				while((bytesRead = stream.Read(readBuffer, totalBytesRead, readBuffer.Length - totalBytesRead)) > 0) {
					totalBytesRead += bytesRead;
					if(totalBytesRead == readBuffer.Length) {
						int nextByte = stream.ReadByte();
						if(nextByte != -1) {
							byte[] temp = new byte[readBuffer.Length * 2];
							Buffer.BlockCopy(readBuffer, 0, temp, 0, readBuffer.Length);
							Buffer.SetByte(temp, totalBytesRead, (byte)nextByte);
							readBuffer = temp;
							totalBytesRead++;
						}
					}
				}
				byte[] buffer = readBuffer;
				if(readBuffer.Length != totalBytesRead) {
					buffer = new byte[totalBytesRead];
					Buffer.BlockCopy(readBuffer, 0, buffer, 0, totalBytesRead);
				}
				return buffer;
			} finally {
				stream.Position = originalPosition;
			}
		}

		public bool IsReusable {
			get {
				return false;
			}
		}
	}
}