﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Honeycomb.Web.HttpHandlers {
    /// <summary>
    /// Summary description for FileUpload
    /// </summary>
    public class FileUpload : IHttpHandler {

        public void ProcessRequest(HttpContext context) {
            HttpPostedFile file = context.Request.Files["file"];
            string fileName = context.Request.Form["filename"];
        }

        public bool IsReusable {
            get {
                return false;
            }
        }
    }
}