﻿using Honeycomb.Custom;
using Honeycomb.Custom.Data;
using System;
using System.Collections.Generic;
using System.Web.SessionState;
using System.Linq;
using System.Web;

namespace Honeycomb.Web.HttpHandlers
{
    /// <summary>
    /// Summary description for Handler1
    /// </summary>
    public class Handler1 : IHttpHandler, IReadOnlySessionState
    {

        public void ProcessRequest(HttpContext context)
        {

            if (!string.IsNullOrEmpty(context.Request.QueryString["id"]))
            {
                System.Guid fileID = new Guid(context.Request.QueryString["id"]);
                try
                {
                    Utility.CheckSession();
                    var refFile = new File();

                    using (var dbContext = new Honeycomb_Entities())
                    {
                        refFile = dbContext.File.Where(dr => dr.FileID == fileID && dr.DeletedOn == null).FirstOrDefault();
                    }

                    if (refFile != null)
                    {

                        byte[] buffer = (byte[])refFile.FileData;
                        string fileName = refFile.FileName;
                        string fileExtention = refFile.FileName.Split('.').Last().Trim();

                        string ContentType = "application/octet-stream";

                        //Switch to the correct content type for the attachment
                        switch (fileExtention)
                        {
                            case "doc":
                                ContentType = "application/msword";
                                break;

                            case "docx":
                                ContentType = "application/msword";
                                break;

                            case "pdf":
                                ContentType = "application/pdf";
                                break;

                            case "tif":
                                ContentType = "image/tiff";
                                break;

                            case "ppt":
                                ContentType = "application/vnd.ms-powerpoint";
                                break;

                            case "xls":
                                ContentType = "application/vnd.ms-excel";
                                break;

                            case "xlsx":
                                ContentType = "application/excel";
                                break;

                            case "jpg":
                                ContentType = "image/jpeg";
                                break;

                            case "gif":
                                ContentType = "image/gif";
                                break;

                            default:
                                ContentType = "application/octet-stream";
                                break;
                        }

                        //Stream the Blob to web page
                        context.Response.Clear();
                        context.Response.ContentType = ContentType;
                        context.Response.AppendHeader("content-disposition", "inline; filename=" + "\"" + fileName + "\"");

                        context.Response.BinaryWrite(buffer);

                    }
                    else
                    {
                        context.Response.Redirect("~/errors/404.html");
                    }


                }
                catch (Exception ex)
                {

                    context.Response.Redirect("~/errors/404.html");
                }

            }

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}