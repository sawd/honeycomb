﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Honeycomb.Master" AutoEventWireup="true" CodeBehind="VisitorStillOnPremises.aspx.cs" Inherits="Honeycomb.Web.Pages.Reports.VisitorStillOnPremises" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
     <link href="/Theme/css/report.css" rel="stylesheet" type="text/css"/>

    <%-- navigation --%>
    <input type="hidden" id="CompanyName" name="CompanyName" value="SEEHOA" />
    <input type="hidden" id="ReportTitle" name="ReportTitle" value="Visitors Stil OnEstate" />
    <ul class="hornav">
        <%--<li><asp:Button ID="btnViewReport" runat="server" Text="View Visitors Still On Estate" OnClick="btnViewReport_Click" /></li>--%>
    </ul>

    <%-- contents --%>
    <div id="contentwrapper" class="contentwrapper">
        <%-- log incident  --%>
        <div id="logincident" class="subcontent" style="display:none;"></div>
        <div id="viewVisitorStillOnEstate" class="subcontent">
            <rsweb:ReportViewer ID="ReportViewer1" CssClass="ReportViewer" Width="100%" Height="100%"   SizeToReportContent="True" runat="server"></rsweb:ReportViewer>
        </div>
    </div>
    <asp:ScriptManager ID="ScriptManager1" ScriptMode="Release" runat="server">
    </asp:ScriptManager>
</asp:Content>
