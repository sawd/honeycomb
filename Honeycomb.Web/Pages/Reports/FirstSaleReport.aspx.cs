﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;

using System.Web.UI;
using System.Web.UI.WebControls;
using Honeycomb.Custom;
using Honeycomb.Custom.Model;
using Honeycomb.Custom.Data;

namespace Honeycomb.Web.Pages.Reports
{
    public partial class FirstSaleReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            htmltext();
        }
        public void htmltext()
        {

            StringBuilder html = new StringBuilder();
            using (var context = new Honeycomb.Custom.Data.Honeycomb_Entities())
            {
                var approvedAgents = (from details in context.PropertyStatus
                                     join own in context.Ownership on details.ID equals own.PropertyStatusID
                                     join prop in context.Property on own.PropertyID equals prop.ID
                                     join erf in context.Erf on prop.ERFID equals erf.ID
                                     where own.PropertyStatusID == 10000
                                     select new
                                     {
                                         erfName = erf.Name,
                                         address = prop.StreetNumber + prop.StreetName,
                                         OwnerName = own.PrincipalOwnerName
                                     });

                Repeater1.DataSource = approvedAgents.ToList();
                Repeater1.DataBind();

            }

        }
    }
}