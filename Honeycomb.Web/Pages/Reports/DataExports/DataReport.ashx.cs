﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using Honeycomb.Custom.Data;
using Honeycomb.Custom;

namespace Honeycomb.Web.Pages.Reports {
	/// <summary>
	/// Summary description for DataReport
	/// </summary>
	public class DataReport : IHttpHandler, IReadOnlySessionState {
		private DateTime reportStartDate;
		private DateTime reportEndDate;
		private String reportType;
		private String fileName;

		public void ProcessRequest(HttpContext context) {
			if (!string.IsNullOrEmpty(context.Request.QueryString["StartDate"])) {
				reportStartDate = DateTime.ParseExact(context.Request.QueryString["StartDate"], "yyyy/MM/dd", System.Globalization.CultureInfo.InvariantCulture);
				reportEndDate = DateTime.ParseExact(context.Request.QueryString["EndDate"], "yyyy/MM/dd", System.Globalization.CultureInfo.InvariantCulture);
			}
			reportType = context.Request.QueryString["ReportType"];

			context.Response.ContentType = "text/csv";
			switch (reportType) {
				case "PropertyData":
					if (Utility.CheckUserAccess("PropertyReports,Admin".Split(',').ToList())) {
						fileName = String.Format("{0} {1}-{2}.csv", reportType, reportStartDate.ToString("yyyyMMdd"), reportEndDate.ToString("yyyyMMdd"));
						using (Honeycomb_Entities dbcontext = new Honeycomb_Entities()) {
							var data = dbcontext.GetPropertyReportData(reportStartDate, reportEndDate).AsQueryable();
							context.Response.Write(data.ToCSV());
						}
					} else {
						return403(context);
					}
					break;
				case "CurrentPropertyData":
					if (Utility.CheckUserAccess("PropertyReports,Admin".Split(',').ToList())) {
						fileName = String.Format("{0} {1}.csv", reportType, DateTime.Now.ToString("yyyyMMdd"));
						using (Honeycomb_Entities dbcontext = new Honeycomb_Entities()) {
							var data = dbcontext.GetPropertyReportLive().AsQueryable();
							context.Response.Write(data.ToCSV());
						}
					} else {
						return403(context);
					}
					break;
				case "CurrentOccupantData":
					if (Utility.CheckUserAccess("PropertyReports,Admin".Split(',').ToList())) {
						fileName = String.Format("{0} {1}.csv", reportType, DateTime.Now.ToString("yyyyMMdd"));
						using (Honeycomb_Entities dbcontext = new Honeycomb_Entities()) {
							var data = dbcontext.GetOccupantReportLive().AsQueryable();
							context.Response.Write(data.ToCSV());
						}
					} else {
						return403(context);
					}
					break;
				case "CurrentPetData":
					if (Utility.CheckUserAccess("PropertyReports,Admin".Split(',').ToList())) {
						fileName = String.Format("{0} {1}.csv", reportType, DateTime.Now.ToString("yyyyMMdd"));
						using (Honeycomb_Entities dbcontext = new Honeycomb_Entities()) {
							var data = dbcontext.GetPetReportLive().AsQueryable();
							context.Response.Write(data.ToCSV());
						}
					} else {
						return403(context);
					}
					break;
				case "CurrentGolfCartData":
					if (Utility.CheckUserAccess("PropertyReports,Admin".Split(',').ToList())) {
						fileName = String.Format("{0} {1}.csv", reportType, DateTime.Now.ToString("yyyyMMdd"));
						using (Honeycomb_Entities dbcontext = new Honeycomb_Entities()) {
							var data = dbcontext.GetGolfCartsReportLive().AsQueryable();
							context.Response.Write(data.ToCSV());
						}
					} else {
						return403(context);
					}
					break;
				case "CurrentVehicleData":
					if (Utility.CheckUserAccess("PropertyReports,Admin".Split(',').ToList())) {
						fileName = String.Format("{0} {1}.csv", reportType, DateTime.Now.ToString("yyyyMMdd"));
						using (Honeycomb_Entities dbcontext = new Honeycomb_Entities()) {
							var data = dbcontext.GetVehiclesReportLive().AsQueryable();
							context.Response.Write(data.ToCSV());
						}
					} else {
						return403(context);
					}
					break;
				case "CurrentPropertyStaffData":
					if (Utility.CheckUserAccess("PropertyReports,Admin".Split(',').ToList())) {
						fileName = String.Format("{0} {1}.csv", reportType, DateTime.Now.ToString("yyyyMMdd"));
						using (Honeycomb_Entities dbcontext = new Honeycomb_Entities()) {
							var data = dbcontext.GetStaffReportLive().AsQueryable();
							context.Response.Write(data.ToCSV());
						}
					} else {
						return403(context);
					}
					break;
				case "CurrentLeaseData":
					if (Utility.CheckUserAccess("PropertyReports,Admin".Split(',').ToList())) {
						fileName = String.Format("{0} {1}.csv", reportType, DateTime.Now.ToString("yyyyMMdd"));
						using (Honeycomb_Entities dbcontext = new Honeycomb_Entities()) {
							var data = dbcontext.GetLeaseReportData().AsQueryable();
							context.Response.Write(data.ToCSV());
						}
					} else {
						return403(context);
					}
					break;
				case "CurrentContractorData":
					if (Utility.CheckUserAccess("ContractorReports,Admin".Split(',').ToList())) {
						fileName = String.Format("{0} {1}.csv", reportType, DateTime.Now.ToString("yyyyMMdd"));
						using (Honeycomb_Entities dbcontext = new Honeycomb_Entities()) {
							var data = dbcontext.GetContractorsReportLive().AsQueryable();
							context.Response.Write(data.ToCSV());
						}
					} else {
						return403(context);
					}
					break;
				case "CurrentContractorStaffData":
					if (Utility.CheckUserAccess("ContractorReports,Admin".Split(',').ToList())) {
						fileName = String.Format("{0} {1}.csv", reportType, DateTime.Now.ToString("yyyyMMdd"));
						using (Honeycomb_Entities dbcontext = new Honeycomb_Entities()) {
							var data = dbcontext.GetContractorStaffReportLive().AsQueryable();
							context.Response.Write(data.ToCSV());
						}
					} else {
						return403(context);
					}
					break;
				case "ContractorAccessData":
					if(Utility.CheckUserAccess("SecurityReports,Admin".Split(',').ToList())) {
						fileName = String.Format("{0} {1}.csv", reportType, DateTime.Now.ToString("yyyyMMdd"));
						using(Honeycomb_Entities dbcontext = new Honeycomb_Entities()) {
							var data = dbcontext.GetContractorAccessData(reportStartDate, reportEndDate).AsQueryable();
							context.Response.Write(data.ToCSV());
						}
					} else {
						return403(context);
					}

					break;
				case "CurrentIncidentData":
					if (Utility.CheckUserAccess("SecurityReports,Admin".Split(',').ToList())) {
						fileName = String.Format("{0} {1}.csv", reportType, DateTime.Now.ToString("yyyyMMdd"));
						using (Honeycomb_Entities dbcontext = new Honeycomb_Entities()) {
							var data = dbcontext.GetIncidentsReportLive().AsQueryable();
							context.Response.Write(data.ToCSV());
						}
					} else {
						return403(context);
					}
					break;

                    // *** EDITS ***

                case "GolfMembersMembershipType":
                    if (Utility.CheckUserAccess("SecurityReports,Admin".Split(',').ToList()))
                    {
                        fileName = String.Format("{0} {1}.csv", reportType, DateTime.Now.ToString("yyyyMMdd"));
                        using (Honeycomb_Entities dbcontext = new Honeycomb_Entities())
                        {
                            var data = dbcontext.GetPropertyGolfReportData().AsQueryable();
                            context.Response.Write(data.ToCSV());
                        }
                    }
                    else
                    {
                        return403(context);
                    }
                    break;

                case "GolfPhaseException":
                    if (Utility.CheckUserAccess("SecurityReports,Admin".Split(',').ToList()))
                    {
                        fileName = String.Format("{0} {1}.csv", reportType, DateTime.Now.ToString("yyyyMMdd"));
                        using (Honeycomb_Entities dbcontext = new Honeycomb_Entities())
                        {
                            var data = dbcontext.GetPropertyGolfPhaseReportData().AsQueryable();
                            context.Response.Write(data.ToCSV());
                        }
                    }
                    else
                    {
                        return403(context);
                    }
                    break;

                    // *** EDITS ***

				default:
					return404(context);
					return;
			}

			context.Response.AddHeader("Content-Disposition", String.Format("attachment; filename=\"{0}\"", fileName));

		}

		public bool IsReusable {
			get {
				return false;
			}
		}

		public void return404(HttpContext context) {
			context.Response.Clear();
			context.Response.StatusCode = 404;
			context.Response.End();
			return;
		}

		public void return403(HttpContext context) {
			context.Response.Clear();
			context.Response.StatusCode = 403;
			context.Response.End();
			return;
		}
	}
}