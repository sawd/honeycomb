﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Honeycomb.Master" AutoEventWireup="true" CodeBehind="LeaseData.aspx.cs" Inherits="Honeycomb.Web.Pages.Reports.DataExports.LeaseData" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
	<script type="text/javascript" src="/Scripts/plugins/chosen.jquery.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.tagsinput.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="/Scripts/custom/forms.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.alerts.js"></script>
    <script type="text/javascript" src="leasedata.aspx.js"></script>

    <!--pageheader-->
    <ul class="hornav">
        <li class="current"><a href="#grid">
            Lease Data Export</a></li>
    </ul>
    <div id="contentwrapper" class="contentwrapper">
        <div id="grid" class="subcontent">
            <div class="stdform" style="width:400px;">
				<h4>Current Data</h4>
				<p> This report will export Lease data as it currently stands.</p>
				<p>
                    <label style="width:100px;"></label>
                    <span style="margin-left:0px;" class="field">
                        <a href="javascript:void(0);" style="text-align:left;" class="btn btn_orange btn_chart currentReport"><span>Download Export</span></a>
                    </span>
                </p>
				<div style="display:none;">
					<h4>Historical Data</h4>
				<p>
					This report will attempt to export data relevant to the timeperiod selected.
					This report will extract all data relevant to the time period selected. Please note the data may be different if the report is run for different time periods as owners change over time.
				</p>
                <p>
                    <label style="width:100px;">Date From:</label>
                    <span style="margin-left:130px;" class="field">
                        <input type="text" name="DateFrom" class="isDatePicker"  />
                    </span>
                </p>
                <p>
                    <label style="width:100px;">Date To:</label>
                    <span style="margin-left:130px;" class="field">
                        <input type="text" name="DateTo" class="isDatePicker"  />
                    </span>
                </p>
                <p>
                    <label style="width:100px;"></label>
                    <span style="margin-left:130px;" class="field">
                        <a href="javascript:void(0);" class="btn btn_orange btn_chart downloadReport"><span>Download Export</span></a>
                    </span>
                </p>
				</div>
            </div>
        </div>
    </div>
</asp:Content>
