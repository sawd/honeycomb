﻿/*global jAlert,dateFormat,window*/
var AccessReport = {
	validateForm: function () {
		if ($("input[name=DateFrom]").val() !== "" && $("input[name=DateTo]").val() !== "") {
			return true;
		} else {
			jAlert("Please supply a start and end date.", "Form Validation Error");
			return false;
		}
	},
	getReport: function () {
		var startDate = $("input[name=DateFrom]").datepicker("getDate");
		var endDate = $("input[name=DateTo]").datepicker("getDate");
		window.open('/Pages/Reports/DataExports/DataReport.ashx?ReportType=ContractorStaffData&StartDate=' + dateFormat(startDate, "yyyy%2Fmm%2Fdd") + "&EndDate=" + dateFormat(endDate, "yyyy%2Fmm%2Fdd"));
	},
	getCurrent: function () {
		window.open('/Pages/Reports/DataExports/DataReport.ashx?ReportType=CurrentContractorStaffData');
	}
};

//page object
var Page = {
	load: function () {
		//Utils.initDataPicker();
		Page.bindEvents();
	},
	bindEvents: function () {
		$(".downloadReport").click(Page.downloadReport_CLICK);
		$(".currentReport").click(Page.currentReport_CLICK);
	},
	currentReport_CLICK: function (e) {
		e.preventDefault();
		AccessReport.getCurrent();
	},
	downloadReport_CLICK: function (e) {
		e.preventDefault();
		if (AccessReport.validateForm()) {
			AccessReport.getReport();
		}
	}
};

//page load
$(function () {
	Page.load();
});