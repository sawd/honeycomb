﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Honeycomb.Master" AutoEventWireup="true" CodeBehind="ContractorAccessData.aspx.cs" Inherits="Honeycomb.Web.Pages.Reports.DataExports.ContractorAccessData" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
	<script type="text/javascript" src="/Scripts/plugins/chosen.jquery.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.tagsinput.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="/Scripts/custom/forms.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.alerts.js"></script>
    <script type="text/javascript" src="contractoraccessdata.aspx.js"></script>

    <!--pageheader-->
    <ul class="hornav">
        <li class="current"><a href="#grid">
            Contractor Access Data Export</a></li>
    </ul>
    <div id="contentwrapper" class="contentwrapper">
        <div id="grid" class="subcontent">
            <div class="stdform" style="width:400px;">
				<%--<p> This report will export Contractor Access data by date range as it currently stands.</p>
				<p>
                    <label style="width:100px;"></label>
                    <span style="margin-left:0px;" class="field">
                        <a href="javascript:void(0);" style="text-align:left;" class="btn btn_orange btn_chart currentReport"><span>Download Export</span></a>
                    </span>
                </p>--%>
				<div>
					<h4>Data for export</h4>
				<p>
					This report will export data relevant to the timeperiod selected.
				</p>
                <p>
                    <label style="width:100px;">Date From:</label>
                    <span style="margin-left:130px;" class="field">
                        <input type="text" name="DateFrom" class="isDatePicker"  />
                    </span>
                </p>
                <p>
                    <label style="width:100px;">Date To:</label>
                    <span style="margin-left:130px;" class="field">
                        <input type="text" name="DateTo" class="isDatePicker"  />
                    </span>
                </p>
                <p>
                    <label style="width:100px;"></label>
                    <span style="margin-left:130px;" class="field">
                        <a href="javascript:void(0);" class="btn btn_orange btn_chart downloadReport"><span>Download Export</span></a>
                    </span>
                </p>
				</div>
            </div>
        </div>
    </div>
</asp:Content>

