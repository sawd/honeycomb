﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Honeycomb.Custom;
using Microsoft.Reporting.WebForms;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using Honeycomb.Custom.Model;

namespace Honeycomb.Web.Pages.Reports {
    public partial class OpenIncidents : Custom.BaseClasses.BasePage {

        protected void Page_Load(object sender, EventArgs e) {
            //ReportViewer1.Visible = false;
        }

        protected void btnViewReport_Click(object sender, EventArgs e) {
            string reportName = "OpenIncidents";
            
            bool isUsername = false;
            ReportsController reportsController = new ReportsController();
            ReportsController.LoadReports(reportName, isUsername, ReportViewer1, HttpContext.Current, Server.MapPath("/"), "[security].[rpt_OpenIncidents]");

        }

    }
}