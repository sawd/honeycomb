﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AvailabilityScheduleTest.aspx.cs" Inherits="Honeycomb.Web.Reports.AvailabilityScheduleTest" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <link href="/Theme/css/report.css" rel="stylesheet" type="text/css" />

        <%-- navigation --%>
        <input id="CompanyName" name="CompanyName" type="hidden" value="SEEHOA" />
        <input id="ReportTitle" name="ReportTitle" type="hidden" value="Property Availability Schedule For Sale" />
        <ul class="hornav">
            <%--<li><asp:Button ID="btnViewReport" runat="server" Text="View Visitors Still On Estate" OnClick="btnViewReport_Click" /></li>--%>
        </ul>

        <%-- contents --%>
        <div id="contentwrapper" class="contentwrapper">
            <%-- log incident  --%>
            <div id="logincident" class="subcontent" style="display: none;"></div>
            <div id="PropertyAvailabilitySchedule" class="subcontent">
                <rsweb:ReportViewer ID="ReportViewer1" runat="server" CssClass="ReportViewer" Height="100%" SizeToReportContent="True" Width="100%"></rsweb:ReportViewer>
            </div>
        </div>
        <asp:ScriptManager ID="ScriptManager1" runat="server" ScriptMode="Release">
        </asp:ScriptManager>

    </form>
</body>
</html>
