﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Honeycomb.Custom;
using Microsoft.Reporting.WebForms;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using Honeycomb.Custom.Model;
using Honeycomb.Custom.Data;

namespace Honeycomb.Web.Pages.Reports
{
    public partial class VisitorStillOnPremises : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string reportName = "VisitorsStillOnEstate";

            bool isUsername = false;
            ReportsController reportsController = new ReportsController();
            if (!IsPostBack)
            {
                ReportsController.LoadReports(reportName, isUsername, ReportViewer1, HttpContext.Current, Server.MapPath("/"), "[security].rpt_VisitorsStillOnEstate"); 
            }
           
        }
    }
}