﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Honeycomb.Custom;
using Honeycomb.Custom.Data;
using System.Data.Objects;

namespace Honeycomb.Web.Pages.Reports.Security {
    /// <summary>
    /// Summary description for GenerateUnexitedExpiredCodes
    /// </summary>
    public class GenerateUnexitedExpiredCodes : IHttpHandler {

        public void ProcessRequest(HttpContext context) {
            DateTime dateFrom = new DateTime(long.Parse(context.Request.QueryString["dateFrom"]) + (72000000000));
            DateTime dateTo = new DateTime(long.Parse(context.Request.QueryString["dateTo"]) + (72000000000));

            context.Response.ContentType = "text/csv";
            context.Response.AddHeader("Content-Disposition", "attachment; filename=\"ExpiredExitCodeReport.csv\"");

            //write the headings
            context.Response.Write("Code,Used,Resident,Request Via,Date Requested,Date/Time Created,Date Delivered,Delivery Status,Gate In,Name In,Time/Date In,Veh. Reg In,# Occ In,Guard In,Gate Out,Name Out,Time/Date Out,Veh. Reg Out,Discrepancy,# Occ Out,Guard Out,Late Exit Reason,Created By\n");

            //get the data
            using(var dbContext = new Honeycomb_Entities()) {
                var accessCodes = dbContext.AccessCode.Include("Occupant").Include("AccessVisitor").Where(ac => ac.DateValidFor >= dateFrom && ac.DateValidFor <= dateTo && ac.ExitedOn == null && EntityFunctions.AddHours(ac.EnteredOn,CacheManager.SystemSetting.AccessCodeVaildPeriod) < DateTime.Now).OrderByDescending(o => o.DateValidFor).ToList();
                var gatesList = dbContext.Gate.ToList();

                foreach(var accessCode in accessCodes) {
                    //access code 1
                    context.Response.Write(accessCode.VisitorAccessCode + ",");

                    //used 2
                    if(accessCode.Used != null) {
                        if(accessCode.Used.Value) {
                            context.Response.Write("Yes,");
                        } else {
                            context.Response.Write("No,");
                        }
                    } else {
                        context.Response.Write("No,");
                    }

                    //resident 3
                    if(accessCode.Occupant != null) {
                        context.Response.Write(accessCode.Occupant.FirstName + " " + accessCode.Occupant.LastName + ",");
                    } else {
                        context.Response.Write("N/A,");
                    }

                    //origin 4
                    context.Response.Write(accessCode.Origin + ",");

                    //date request received 5
                    if(accessCode.RequestedRecievedOn != null) {
                        context.Response.Write(accessCode.RequestedRecievedOn.Value.ToString("HH:mm:ss dd/MM/yyyy") + ",");
                    } else {
                        context.Response.Write("N/A,");
                    }

                    //date created 6
                    if(accessCode.DateValidFor != null) {
                        context.Response.Write(accessCode.TimeCodeCreated.Value.ToString("HH:mm:ss dd//MM/yyyy") + ",");
                    } else {
                        context.Response.Write("N/A,");
                    }

                    //date delivered 7
                    context.Response.Write("N/A,");
                    //delivery status 8
                    context.Response.Write("N/A,");

                    //gate entered 9
                    if(gatesList.FirstOrDefault(g => g.ID == accessCode.EnteredGateID) != null) {
                        context.Response.Write("\"" + gatesList.First(g => g.ID == accessCode.EnteredGateID).Name + "\",");
                    } else {
                        context.Response.Write("N/A,");
                    }

                    //visitor name 10
                    context.Response.Write("\"" + accessCode.AccessVisitor.VisitorName + "\",");

                    //date entered 11
                    if(accessCode.EnteredOn != null) {
                        context.Response.Write(accessCode.EnteredOn.Value.ToString("HH:mm:ss dd/MM/yyyy") + ",");
                    } else {
                        context.Response.Write("N/A,");
                    }

                    //registration in 12
                    if(accessCode.VehicleRegIn != null) {
                        context.Response.Write("\"" + accessCode.VehicleRegIn + "\",");
                    } else {
                        context.Response.Write("N/A,");
                    }

                    //no of occupants in 13
                    context.Response.Write(accessCode.VehicleOccupantIn.ToString() + ",");

                    //entered guard 14
                    context.Response.Write("\"" + accessCode.GuardAuditEnteredBy + "\",");

                    //gate existed 15
                    if(gatesList.FirstOrDefault(g => g.ID == accessCode.ExitedGateID) != null) {
                        context.Response.Write("\"" + gatesList.First(g => g.ID == accessCode.ExitedGateID).Name + "\",");
                    } else {
                        context.Response.Write("N/A,");
                    }

                    //visitor name 16
					if(accessCode.ExitedOn != null) {
						context.Response.Write("\"" + accessCode.AccessVisitor.VisitorName + "\",");
					} else {
						context.Response.Write("N/A,");
					}

                    //exited on 17
                    if(accessCode.ExitedOn != null) {
                        context.Response.Write(accessCode.ExitedOn.Value.ToString("HH:mm:ss dd/MM/yyyy") + ",");
                    } else {
                        context.Response.Write("N/A,");
                    }

                    //registration out 18
                    context.Response.Write("\"" + accessCode.VehicleRegOut + "\",");

                    //discrepency reason 19
                    if(accessCode.DiscrepancyReason != null) {
                        context.Response.Write("\"" + accessCode.DiscrepancyReason + "\",");
                    } else {
                        context.Response.Write("N/A,");
                    }

                    //no of occupants out 20
                    context.Response.Write(accessCode.VehicleOccupantOut.ToString() + ",");

                    //guard exit 21
                    context.Response.Write("\"" + accessCode.GuardAuditExitedBy + "\",");

					//late exit reason 22
					if(accessCode.LateExitReason != "") {
						context.Response.Write("\"" + accessCode.LateExitReason + "\",");
					} else {
						context.Response.Write("N/A,");
					}

                    //Created By
                    if(accessCode.InsertedBy != null) {
                        context.Response.Write("\"" + accessCode.InsertedBy + "\",");
                    } else {
                        context.Response.Write("N/A,");
                    }

                    context.Response.Write("\n");
                }
            }

        }

        public bool IsReusable {
            get {
                return false;
            }
        }
    }
}