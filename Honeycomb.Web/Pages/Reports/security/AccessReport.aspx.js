﻿/*global jAlert*/
var AccessReport = {
	validateForm: function () {
		if ($("input[name=DateFrom]").val() !== "" && $("input[name=DateTo]").val() !== "") {
			return true;
		} else {
			jAlert("Please supply a start and end date.", "Form Validation Error");
			return false;
		}
	},
	getReport: function () {
		document.location = "GenerateReport.ashx?datefrom=" + (($("input[name=DateFrom]").datepicker("getDate").getTime() * 10000) + 621355968000000000) + "&dateTo=" + (($("input[name=DateTo]").datepicker("getDate").getTime() * 10000) + 621355968000000000);
	}
};

//page object
var Page = {
	load: function () {
		//Utils.initDataPicker();
		Page.bindEvents();
	},
	bindEvents: function () {
		$(".downloadReport").click(Page.downloadReport_CLICK);
	},
	downloadReport_CLICK: function (e) {
		e.preventDefault();
		if (AccessReport.validateForm()) {
			AccessReport.getReport();
		}
	}
};

//page load
$(function () {
	Page.load();
});