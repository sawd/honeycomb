﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage/Honeycomb.Master" CodeBehind="UnexitedExpiredCodes.aspx.cs" Inherits="Honeycomb.Web.Pages.Reports.Security.UnexitedExpiredCodes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
    <script type="text/javascript" src="/Scripts/plugins/chosen.jquery.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.tagsinput.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="/Scripts/custom/forms.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.alerts.js"></script>
    <script language="javascript">
        /*global jAlert*/
        var AccessReport = {
            validateForm: function () {
                if ($("input[name=DateFrom]").val() !== "" && $("input[name=DateTo]").val() !== "") {
                    return true;
                } else {
                    jAlert("Please supply a start and end date.", "Form Validation Error");
                    return false;
                }
            },
            getReport: function () {
                document.location = "GenerateUnexitedExpiredCodes.ashx?datefrom=" + (($("input[name=DateFrom]").datepicker("getDate").getTime() * 10000) + 621355968000000000) + "&dateTo=" + (($("input[name=DateTo]").datepicker("getDate").getTime() * 10000) + 621355968000000000);
            }
        };

        //page object
        var Page = {
            load: function () {
                //Utils.initDataPicker();
                Page.bindEvents();
            },
            bindEvents: function () {
                $(".downloadReport").click(Page.downloadReport_CLICK);
            },
            downloadReport_CLICK: function (e) {
                e.preventDefault();
                if (AccessReport.validateForm()) {
                    AccessReport.getReport();
                }
            }
        };

        //page load
        $(function () {
            Page.load();
        });
    </script>
    <!--pageheader-->
    <ul class="hornav">
        <li class="current"><a href="#grid">
            Expired Exit Code Report</a></li>
    </ul>
    <div id="contentwrapper" class="contentwrapper">
        <div id="grid" class="subcontent">
            <div class="stdform" style="width:400px;">
                <p>
                    <label style="width:100px;">Date From:</label>
                    <span style="margin-left:130px;" class="field">
                        <input type="text" name="DateFrom" class="isDatePicker"  />
                    </span>
                </p>
                <p>
                    <label style="width:100px;">Date To:</label>
                    <span style="margin-left:130px;" class="field">
                        <input type="text" name="DateTo" class="isDatePicker"  />
                    </span>
                </p>
                <p>
                    <label style="width:100px;"></label>
                    <span style="margin-left:130px;" class="field">
                        <a href="javascript:void(0);" class="btn btn_orange btn_chart downloadReport"><span>Download Report</span></a>
                    </span>
                </p>
            </div>
        </div>
    </div>
</asp:Content>