﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Honeycomb.Master" AutoEventWireup="true" CodeBehind="AccessReport.aspx.cs" Inherits="Honeycomb.Web.Pages.Reports.Security.AccessReport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
    <script type="text/javascript" src="/Scripts/plugins/chosen.jquery.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.tagsinput.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="/Scripts/custom/forms.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.alerts.js"></script>
    <script type="text/javascript" src="AccessReport.aspx.js"></script>

    <!--pageheader-->
    <ul class="hornav">
        <li class="current"><a href="#grid">
            Access Code Report</a></li>
    </ul>
    <div id="contentwrapper" class="contentwrapper">
        <div id="grid" class="subcontent">
            <div class="stdform" style="width:400px;">
                <p>
                    <label style="width:100px;">Date From:</label>
                    <span style="margin-left:130px;" class="field">
                        <input type="text" name="DateFrom" class="isDatePicker"  />
                    </span>
                </p>
                <p>
                    <label style="width:100px;">Date To:</label>
                    <span style="margin-left:130px;" class="field">
                        <input type="text" name="DateTo" class="isDatePicker"  />
                    </span>
                </p>
                <p>
                    <label style="width:100px;"></label>
                    <span style="margin-left:130px;" class="field">
                        <a href="javascript:void(0);" class="btn btn_orange btn_chart downloadReport"><span>Download Report</span></a>
                    </span>
                </p>
            </div>
        </div>
    </div>
</asp:Content>
