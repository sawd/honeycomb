﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Objects.SqlClient;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using Honeycomb.Custom.Data;
using Honeycomb.Custom;
using System.IO;
using System.Drawing.Imaging;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace Honeycomb.Web {
    /// <summary>
    /// Summary description for UploadFile
    /// </summary>
    public class UploadFile : IHttpHandler, IReadOnlySessionState {
        

        public void ProcessRequest(HttpContext context) {
            context.Response.ContentType = "text/plain";
            context.Response.Write(Uploader(context));
            return;
        }

        private string Uploader(HttpContext contextUpload) {
            string uploadMessage = string.Empty;
            try {

                HttpPostedFile file = contextUpload.Request.Files["qqfile"];
               
                string uploadType = (contextUpload.Request.QueryString["UploadType"] == "") ? null : contextUpload.Request.QueryString["UploadType"];
                string fName;
                string DisplayName = (contextUpload.Request.QueryString["DisplayName"] == "") ? null : contextUpload.Request.QueryString["DisplayName"];
                string FileName = (contextUpload.Request.QueryString["FileName"] == "") ? null : contextUpload.Request.QueryString["FileName"];
                string Path = (contextUpload.Request.QueryString["Path"] == "") ? null : contextUpload.Request.QueryString["Path"];         

                if (uploadType == "Folder") {
                   
                    if (file == null) {
                        //if mozila
                        byte[] buffer = new byte[1024];
                        fName = contextUpload.Request.QueryString["qqfile"];
                        string path = HttpContext.Current.Server.MapPath(Path + fName);
                        System.IO.File.WriteAllBytes(path, ReadToEnd(contextUpload.Request.InputStream));

                    } else {
                        //else IE
                        fName = file.FileName.Split('\\').Last<string>();
                        string path = HttpContext.Current.Server.MapPath(Path + fName);
                        file.SaveAs(path);
                    }

                    uploadMessage = "{\"success\":true}";
                } else {

                    DocumentRepository rep = new DocumentRepository();
                    rep.Id = Guid.NewGuid();

                    if (file == null) {
                        rep.FileData = ReadToEnd(contextUpload.Request.InputStream);
                        fName = contextUpload.Request.QueryString["qqfile"];
                    } else {
                        rep.FileData = ReadToEnd(file.InputStream);
                        fName = file.FileName;
                        fName = fName.Split('\\').Last<string>();
                    }

                    rep.FileName = FileName ?? fName.Substring(0, fName.LastIndexOf('.'));
                    rep.FileSize = rep.FileData.Length;
                    rep.DisplayName = DisplayName ?? rep.FileName;
                    rep.Extension = fName.Split('.').Last<string>();
                    rep.DateUploaded = DateTime.Now;
                    rep.UploadedBy = SessionManager.UserName;

                    using(var context = new Honeycomb_Entities()){
                        context.DocumentRepository.Add(rep);
                        context.SaveChanges();
                    }

                    uploadMessage = "{\"success\":true,\"fileid\":\""+ rep.Id +"\"}";
                    
                }
            } catch (Exception exception) {
  
              uploadMessage = "{\"error\":\"" + exception.Message + "\"}";
                
            }

            return uploadMessage;
        }

        public bool IsReusable {
            get {
                return false;
            }
        }

        public static byte[] ReadToEnd(System.IO.Stream stream) {
            long originalPosition = stream.Position;
            stream.Position = 0;
            try {
                byte[] readBuffer = new byte[4096];
                int totalBytesRead = 0;
                int bytesRead;
                while ((bytesRead = stream.Read(readBuffer, totalBytesRead, readBuffer.Length - totalBytesRead)) > 0) {
                    totalBytesRead += bytesRead;
                    if (totalBytesRead == readBuffer.Length) {
                        int nextByte = stream.ReadByte();
                        if (nextByte != -1) {
                            byte[] temp = new byte[readBuffer.Length * 2];
                            Buffer.BlockCopy(readBuffer, 0, temp, 0, readBuffer.Length);
                            Buffer.SetByte(temp, totalBytesRead, (byte)nextByte);
                            readBuffer = temp; totalBytesRead++;
                        }
                    }
                }
                byte[] buffer = readBuffer;
                if (readBuffer.Length != totalBytesRead) {
                    buffer = new byte[totalBytesRead];
                    Buffer.BlockCopy(readBuffer, 0, buffer, 0, totalBytesRead);
                }
                return buffer;
            }
            finally {
                stream.Position = originalPosition;
            }
        }

        public static bool isImage(string fileName) {
            var isFile = false;
            switch(fileName.Split('.').Last<string>()) {
                case "tif":
                    isFile = true;
                    break;
                case "jpg":
                    isFile = true;
                    break;
                case "jpeg":
                    isFile = true;
                    break;
                case "gif":
                    isFile = true;
                    break;

                default:
                    isFile = false;
                    break;
            }

            return isFile;
        }

        public static void Resize(Stream input, Stream output, int width, int height) {
            ImageCodecInfo myImageCodecInfo;
            System.Drawing.Imaging.Encoder myEncoder;
            EncoderParameter myEncoderParameter;
            EncoderParameters myEncoderParameters;

            // Get an ImageCodecInfo object that represents the JPEG codec.
            myImageCodecInfo = GetEncoderInfo("image/jpeg");
            // Create an Encoder object based on the GUID
            myEncoder = System.Drawing.Imaging.Encoder.Quality;
            // Create an EncoderParameters object.
            myEncoderParameters = new EncoderParameters(1);
            // Save the bitmap as a JPEG file with quality level 75.
            myEncoderParameter = new EncoderParameter(myEncoder, 85L);
            myEncoderParameters.Param [0] = myEncoderParameter;

            using(var image = Image.FromStream(input)) {

                // Figure out the ratio
                double ratioX = (double)width / (double)image.Width;
                double ratioY = (double)height / (double)image.Height;
                // use whichever multiplier is smaller
                double ratio = ratioX < ratioY ? ratioX : ratioY;

                // now we can get the new height and width
                int newHeight = Convert.ToInt32(image.Height * ratio);
                int newWidth = Convert.ToInt32(image.Width * ratio);

                var bmp = new Bitmap(newWidth, newHeight);
                var gr = Graphics.FromImage(bmp);

                gr.CompositingQuality = CompositingQuality.HighSpeed;
                gr.SmoothingMode = SmoothingMode.HighSpeed;
                gr.InterpolationMode = InterpolationMode.HighQualityBicubic;

                gr.DrawImage(image, 0, 0, newWidth, newHeight);

                //gr.DrawImage(image, new Rectangle(0, 0, width, height));
                bmp.Save(output, myImageCodecInfo, myEncoderParameters);

                bmp.Dispose();
                gr.Dispose();
            }
        }

        private static ImageCodecInfo GetEncoderInfo(String mimeType) {
            int j;
            ImageCodecInfo [] encoders;
            encoders = ImageCodecInfo.GetImageEncoders();
            for(j = 0; j < encoders.Length; ++j) {
                if(encoders [j].MimeType == mimeType)
                    return encoders [j];
            }
            return null;
        }
    }
}