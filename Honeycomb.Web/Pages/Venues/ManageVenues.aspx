﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Honeycomb.Master" AutoEventWireup="true" CodeBehind="ManageVenues.aspx.cs" Inherits="Honeycomb.Web.Pages.Venues.ManageVenues" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">


  <%-- References --%>
    <script type="text/javascript" src="/Scripts/plugins/jquery.tagsinput.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/chosen.jquery.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.validate.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.tmpl.min.js"></script>
    <script type="text/javascript" src="/Scripts/custom/tables.js"></script>
    <script type="text/javascript" src="/Scripts/custom/forms.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.alerts.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/Services/WCFAdmin.svc/js"></script>
       <script type="text/javascript" src="ManageVenues.aspx.js"></script>

      
 
         <script id="venueGridTemplate" type="text/x-jquery-tmpl">
        <tr>
            <td>${Name}</td>
            <td>${MaxVisitors}</td>
            <td>${Cost}</td>
           
            <td class="right" align="right"><a href="#" data-venueID="${VenueID}" class="btn btn3 btn_pencil editVenue" style="margin-right:10px;"></a>
                <a href="#" data-venueID="${VenueID}"  data-venueName="${Name}" class="btn btn3 btn_trash deleteVenue"></a></td>
        </tr>
    </script>

       <%-- Venue edit tab template --%>
    <script id="VenueTabTemplate" type="text/x-jquery-tmpl">
        <div class="pageheader nopadding">
            <h1 class="pagetitle">${Name}</h1>
            <div class="topSave">
                <button type="button" class="submitbutton" dataid="${VenueID}" name="topSave">Save Venue</button>
            </div>
            <input type="hidden" name="VenueID" value="${VenueID}" />
        </div>
        
     <div class="ui-box">
            <input type="hidden" name="ID" value="0" />
            <div class="contenttitle2">
                <h3>Venue Details</h3>
            </div><!--contenttitle-->

            <div class="stdform">
                <p>
                    <label>Venue Name:</label>
                    <span class="field"><input type="text" name="Name" value="${Name}" valtype="required;" class="smallinput" valtype="required;" /></span>
                </p>
                        
                                                              
                <p>
                    <label>Description:</label>
                    <span class="field"><textarea name="Description" cols="80" style="width:40%" rows="5" class="longinput" valtype="required;">${Description}</textarea></span> 
                </p>

                 <p>
                    <label>Max Visitors:</label>
                    <span class="field"><input type="text" name="MaxVisitors" value="${MaxVisitors}" class="smallinput" valtype="required;REGEX:INT"/></span>
                </p>

                <p>
                    <label>Time Increments:</label>
                    <span class="field">
                                            <select name="TimeIncrement" id="TimeIncrement"  class="uniformselect">
                                                <option value="15">15</option>
                                                <option value="30">30</option>
                                                <option value="45">45</option>
                                                <option value="60">60</option>
                                            </select>
                                        </span>
                </p>
                <p>
                    <label>Is Cost Time Based:</label>
                    <span class="field">
                        <input type="checkbox" name="IsCostTimeBased" value="1" {{if IsCostTimeBased}}checked="checked"{{/if}}
                    </span>
                </p>
                <p>
                    <label>Cost:</label>
                    <span class="field"><input type="text" name="Cost"  value="${Cost}" class="smallinput" valtype="required;REGEX:DOUBLE" /></span>
                </p>
                 <p>
                    <label>Is Public:</label>
                    <span class="field">
                        <input type="checkbox" name="IsPublic" value="1" {{if IsPublic}}checked="checked"{{/if}}
                        
                    </span>
                </p>
				  <p>
                    <label>Is Visible On Intranet:</label>
                    <span class="field">
                        <input type="checkbox" name="IsVisibleOnIntranet" value="1" {{if IsVisibleOnIntranet}}checked="checked"{{/if}}
                        
                    </span>
                </p>
            </div>

        </div>

        <div class="bottomSave">
            <button type="button" class="submitbutton" dataid="${ID}" name="topSave">Save Venue</button>
        </div>
         
    </script>
    
     <!-- main tab headers -->
    <ul class="hornav">
        <li><a href="#add"><img class='tabIcon' src='/Theme/Images/addfolder.png'/>&nbsp;</a></li>
        <li class="current">
            <a href="#grid"><img class="loader" style="display:none;" src="/Theme/images/loaders/loader2.gif" alt="" />All Venues</a>
        </li>
    </ul>
        <!-- end main tab headers -->
   <%-- begin content wrapper --%>
    <div id="contentwrapper" class="contentwrapper">
         <%-- begin grid content --%>
        <div id="grid" class="subcontent">

       <table cellpadding="0" cellspacing="0" border="0" class="stdtable">
                <thead>
                    <tr>
                        <th class="head0" style="width:381px">Venue Name</th>
                        <th class="head1" style="width:381px">Max Visitors</th>
                        <th class="head0" style="width:381px">Cost</th>                        
                        <th class="head1" style="width:185px">Function</th>
                  </tr>
                </thead>
                <tbody id="venueTemplateContainer">
                </tbody>
            </table>
        

            </div>
          <%-- end grid content --%>
        <div id="add" class="subcontent" style="display: none">
        </div>
      </div>

</asp:Content>
