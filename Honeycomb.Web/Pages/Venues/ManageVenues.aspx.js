﻿/*global Utils,AdminService,Entities,jAlert,jConfirm*/
var Venue = {
    GetVenues: function () {

        var $currentTab = Utils.tabs.getCurrentTab();

        var responce = function (result) {
            $currentTab.unblock();
            Utils.bindDataToGrid("#venueTemplateContainer", "#venueGridTemplate", result.EntityList);
        };
        $currentTab.block();

        AdminService.WCFAdmin.GetVenues(false, responce, Utils.responceFail);
  
    },
    loadEditTab: function (venueID) {
        if (!isNaN(venueID)) {
            var venueResponse = function (result) {
                if (result.EntityList[0] !== null) {
                    Utils.tabs.loadTab(result.EntityList[0].Name, venueID, function () {
                        var $venueTab = Utils.tabs.getCurrentTab();

                      Utils.bindDataToGrid($venueTab.selector, "#VenueTabTemplate", result.EntityList[0]);
                      Utils.tabs.initTab($venueTab.selector);
                        $venueTab.find("select[name=TimeIncrement]").val(result.EntityList[0].TimeIncrement);
                        // this is used to update the ui GUI when there is a change. 
                      $.uniform.update();
                    }, true, true);
                } else {
                    $.jGrowl("Record could not be loaded.");
                }
            };
           
            AdminService.WCFAdmin.GetVenue(venueID, venueResponse, Utils.responseFail);
        }
    },
    UpdateVenue: function () {
        
        var $currentTab = Utils.tabs.getCurrentTab();
        var venue = new Entities.Venue();
        if (Utils.validation.validateForm($currentTab.selector, "Company information tab is missing required fields", false, true)) {
            $currentTab.mapToObject(venue, "name");

            var venueResponse = function (result) {
                $currentTab.unblock();
                if (result.EntityList[0] !== 0) {
                    jAlert("The Venue has been saved", "Success");
                    //clear the html
                    $("#add").html("");
                    // Go to all the Venues
                    Utils.tabs.selectTab("#grid");

                } else {
                    jAlert("There was an error when attempting to save the venue.", "Error");
                }

            };
            $currentTab.block();
            AdminService.WCFAdmin.UpdateVenues(venue, venueResponse, Utils.responseFail);
        }
    },
    AddTab: function () {
       
            var $currentTab = Utils.tabs.getCurrentTab();
            var venueEntity = new Entities.Venue();
            Utils.bindDataToGrid($currentTab.selector, "#VenueTabTemplate", venueEntity);

            Utils.tabs.initTab($currentTab.selector);
            
    },
    DeleteVenue: function (venueID) {
        var $currentTab = Utils.tabs.getCurrentTab();
        jConfirm("Are you sure you want to delete this venue? " , "Confirm Delete", function (result) {
            var $currentTab = Utils.tabs.getCurrentTab();

            if (result) {
                var response = function (result) {
                    if (result.Count > 0) {
                        $.jGrowl("Venue Deleted");
                        $currentTab.block();
                        Venue.GetVenues();
                        $currentTab.unblock();
                    }
                };
                AdminService.WCFAdmin.ArchiveVenue(venueID, response, Utils.responceFail);
            }
        });

    }
    
 
};

var PageEvents = {
    //to contaiain load events and fire custom binding. 
    load: function () {

        Venue.GetVenues();
        PageEvents.bindEvents();
   },
    bindEvents: function () {
		$('.bodywrapper').on('Honeycomb.tabLoad', '#grid', PageEvents.VenueGridTab_click);
		$('.bodywrapper').on('click.editVenue', '.editVenue', PageEvents.venueEdit_click);
		$('.bodywrapper').on('click.submitbutton', '.submitbutton', PageEvents.saveVenue_click);
		$('.bodywrapper').on('Honeycomb.tabLoad', '#add', Venue.AddTab);
		$('.bodywrapper').on('click.deleteVenue', '.deleteVenue', PageEvents.deleteVenue_click);
    },

    venueEdit_click: function (e) {
        e.preventDefault();
        Venue.loadEditTab($(this).attr("data-venueid"));

        
    },
    VenueGridTab_click: function (e) {
        e.preventDefault();
        Venue.GetVenues();
    },
    saveVenue_click: function (e) {
        e.preventDefault();
        Venue.UpdateVenue();
       
    },
    deleteVenue_click: function (e) {
        e.preventDefault();
        Venue.DeleteVenue($(this).attr("data-venueid"));
          
    }
};

//need PageEvents to fire
$(function () {
    PageEvents.load();
});