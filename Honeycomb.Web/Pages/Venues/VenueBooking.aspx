﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Honeycomb.Master" AutoEventWireup="true" CodeBehind="VenueBooking.aspx.cs" Inherits="Honeycomb.Web.Pages.Venues.VenueBooking" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
	<%-- References --%>
   <script type="text/javascript" src="/Scripts/plugins/chosen.jquery.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/charCount.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.tagsinput.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.validate.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.tmpl.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.smartWizard-2.0.min.js"></script>
    <script type="text/javascript" src="/Scripts/custom/tables.js"></script>
    <script type="text/javascript" src="/Scripts/custom/forms.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.alerts.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.autogrow-textarea.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/ui.spinner.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/header.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/util.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/button.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/handler.base.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/handler.form.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/handler.xhr.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/uploader.basic.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/dnd.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/uploader.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/jquery-plugin.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fullcalendar.min.js"></script>
    <script type="text/javascript" src="/Scripts/custom/jquery-honeycomb.js"></script>
    <script type="text/javascript" src="/Scripts/custom/calendarDialogues.js"></script>
    <script type="text/javascript" src="/Services/WCFAdmin.svc/js"></script>
    <script type="text/javascript" src="VenueBooking.aspx.js?update=12202"></script>

	<!-- Templates -->
	<script type="text/x-jquery-tmpl" id="venueTemplate">
		<option value="${VenueID}">${Name}</option>
	</script>

	<script type="text/x-jquery-tmpl" id="pendingTmpl">
		<div class="stdform form">
			<div class="attendedTreatment">
				<form>
				<input type="hidden" name="VenueBookingID" value="${ID}"/>
				<input type="hidden" name="VenueID" value="${VenueID}"/>
				<input type="hidden" name="InsertedByID" value="${InsertedByID}"/>
				<input type="hidden" name="InsertedBy" value="${InsertedBy}"/>
				<input type="hidden" name="InsertedOn" value="${InsertedOn}"/>
				<input type="hidden" name="OccupantID" value="${OccupantID}"/>
				<input type="hidden" name="VenueBookingStatusID" value="${VenueBookingStatusID}"/>
				<input type="hidden" name="TotalBookingCost" value="${TotalBookingCost}"/>
				<div class="optional-fields">
					<p><label>Name:</label><span class="field"><input type="text" name="Name" valtype="Required;" value="${Name}"/></span></p>
					<p><label>Email:</label><span class="field"><input type="text" name="Email" valtype="Required;" value="${Email}"/></span></p>
					<p><label>Phone:</label><span class="field"><input type="text" name="PhoneNumber" valtype="required;Regex:PHONE" value="${Phone}"/></span></p>
				</div>
				<p style="clear:both;"><label>Requirements:</label><br style="clear: both;"/><span class="field" style="margin: 0;"><textarea type="text" class="longinput" style="width: 465px;" name="Requirements">${Requirements}</textarea></span></p>
				</form>
			</div>
		</div>
	</script>

	<script type="text/x-jquery-tmpl" id="addBookingTmpl">
		<div class="stdform">
			<div class="addBooking">
				<form>
				<input type="hidden" name="VenueBookingID" value="0"/>
				<input type="hidden" name="VenueID" value=""/>
				<input type="hidden" name="InsertedByID" value="0"/>
				<input type="hidden" name="InsertedBy" value=""/>
				<input type="hidden" name="InsertedOn" value=""/>
				<input type="hidden" name="OccupantID" value=""/>
				<input type="hidden" name="VenueBookingStatusID" value="0"/>
				<input type="hidden" name="TotalBookingCost" value="0"/>
				<div class="optional-fields">
					<p style="padding-left: 120px;">
						<select name="TimeStart" class="uniformselect" valtype="Required;" value=""></select>
						<label style="float: none;padding: 0 5px;">To:</label>
						<select name="TimeEnd" class="uniformselect" valtype="Required;" value=""></select>
					</p>
					<p><label>Name:</label><span class="field"><input type="text" name="Name" valtype="Required;" value="${Name}"/></span></p>
					<p><label>Email:</label><span class="field"><input type="text" name="Email" valtype="Required;REGEX:EMAIL" value=""/></span></p>
					<p><label>Phone:</label><span class="field"><input type="text" name="PhoneNumber" valtype="required;REGEX:PHONE" value=""/></span></p>
				</div>
				<p style="clear:both;"><label>Requirements:</label><br style="clear: both;"/><span class="field" style="margin: 0;"><textarea type="text" class="longinput" style="width: 465px;" name="Requirements">${Requirements}</textarea></span></p>
				</form>
			</div>
		</div>
	</script>

	<script type="text/x-jquery-tmpl" id="timeStartTmpl">
		<option value="${Time}" data-date="${DateObj}">${DisplayTime}</option>
	</script>


	<script type="text/x-jquery-tmpl" id="timeEndTmpl">
		<option value="${Time}" data-date="${DateObj}">${DisplayTime} (${hrs})</option>
	</script>

	 <!--pageheader-->

	<div id="contentwrapper" class="contentwrapper">
        <div id="grid" class="subcontent">
			  <div id="Tabs" class="tabs">
					<ul>
						<li><a href="#tabs-1">Booking Calendar for:</a></li>
						<li >
							 <div class="widgetbox" style="margin-bottom: 0; display: inline-block;">
								<!-- search table -->
								<div class="chatsearch" style="float: left;">
									<select name="VenueID" id="VenueID" class="uniformselect" >

									</select>
								</div>
								<!-- end search table -->
								<div id='availableTreatmentList' style="float: left; margin-left: 10px;"></div>
							</div>
						</li>
					</ul>
					<div class="legendCalendarbutton" style="display:none"><a id="legendButton" title="Calendar Legend" target="_blank"><img src="/Theme/images/icons/legend.png" /></a></div>
					<div class="clear"></div>
					<!-- tab 1-->
					<div id="tabs-1">
						<div id="calendar"></div>
					</div>
					<!-- end tab 1 -->
				</div>
        </div>
    </div>
</asp:Content>
