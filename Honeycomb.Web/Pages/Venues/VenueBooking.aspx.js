﻿/*global Occupants, Utils, PropertyService, jQuery, jAlert, Entities,jConfirm, ManagementAssociation, Ownership, Lease, Globals,dateFormat,Calendar,window,AdminService,CalendarService*/

(function ($, doc) {

	//#region Calendar Plug-in Initialization
	var calendarPlugin = {
		init: function (selector) {
			//BEGIN INIT
			/* initialize the external events */
			calendarPlugin.bindDraggableObjects();
			Calendar.selector = selector;

			/*custom set calendar buttons*/
			var buttons = {
				month: '<span class="fc-button fc-button-month fc-state-default fc-corner-left">'+
						'<span class="fc-button-inner">'+
							'<span class="fc-button-content">month</span>'+
							'<span class="fc-button-effect">'+
								'<span></span>'+
							'</span>'+
						'</span>'+
					'</span>',
				week: '<span class="fc-button fc-button-agendaWeek fc-state-default fc-state-active">'+
						'<span class="fc-button-inner">'+
							'<span class="fc-button-content">week</span>'+
							'<span class="fc-button-effect">'+
								'<span></span>'+
							'</span>'+
						'</span>'+
					'</span>',
				day : '<span class="fc-button fc-button-agendaDay fc-state-default fc-corner-right">'+
						'<span class="fc-button-inner">'+
							'<span class="fc-button-content">day</span>'+
							'<span class="fc-button-effect">'+
								'<span></span>'+
							'</span>'+
						'</span>' +
					'</span>',
				rightButtons: '<span class="fc-button fc-button-today fc-state-default fc-corner-left fc-corner-right fc-state-disabled"><span class="fc-button-inner"><span class="fc-button-content">today</span><span class="fc-button-effect"><span></span></span></span></span><span class="fc-header-space"></span><span class="fc-button fc-button-prev fc-state-default fc-corner-left fc-corner-right"><span class="fc-button-inner"><span class="fc-button-content">«</span><span class="fc-button-effect"><span></span></span></span></span><span class="fc-header-space"></span><span class="fc-button fc-button-next fc-state-default fc-corner-left fc-corner-right"><span class="fc-button-inner"><span class="fc-button-content">»</span><span class="fc-button-effect"><span></span></span></span></span>',
				init: function () {
					$(selector).find(".fc-header-left").html(this.month + this.week + this.day);
					$(selector).find(".fc-header-right").html(this.rightButtons);
					this.events.bind_clicks();

					//now perform initial load
					Calendar.loadDynamicEvents($(selector).fullCalendar("getView").name, $(selector).fullCalendar("getDate"), buttons.callbacks.renderEvents);
				},
				refresh: function () {
					Calendar.loadDynamicEvents($(selector).fullCalendar("getView").name, $(selector).fullCalendar("getDate"), buttons.callbacks.renderEvents);
				},
				callbacks: {
					renderEvents: function (events) {
						//before anything cleanup
						buttons.events.clean();

						$(events).each(function () {
							$(selector).fullCalendar("renderEvent", this, true);
						});
					}
				},
				events: {
					clean: function () {
						//full clean up
						$(selector).fullCalendar('removeEvents', function (event) {
							return true;
						});
					},
					renderCallback: function () {

					},
					bind_clicks: function () {
						//bind month button click
						$(selector).find(".fc-button-month").click(function (e) {
							e.preventDefault();
							$(selector).fullCalendar("changeView", "month");
							Calendar.loadDynamicEvents($(selector).fullCalendar("getView").name, $(selector).fullCalendar("getDate"), buttons.callbacks.renderEvents);
						});

						//bind week button click
						$(selector).find(".fc-button-agendaWeek").click(function (e) {
							e.preventDefault();
							$(selector).fullCalendar("changeView", "agendaWeek");
							Calendar.loadDynamicEvents($(selector).fullCalendar("getView").name, $(selector).fullCalendar("getDate"), buttons.callbacks.renderEvents);
						});

						//bind day button click
						$(selector).find(".fc-button-agendaDay").click(function (e) {
							e.preventDefault();
							$(selector).fullCalendar("changeView", "agendaDay");
							Calendar.loadDynamicEvents($(selector).fullCalendar("getView").name, $(selector).fullCalendar("getDate"), buttons.callbacks.renderEvents);
						});

						//bind today button click
						$(selector).find(".fc-button-today").click(function (e) {
							e.preventDefault();
							$(selector).fullCalendar("today");
							Calendar.loadDynamicEvents($(selector).fullCalendar("getView").name, $(selector).fullCalendar("getDate"), buttons.callbacks.renderEvents);
						});

						//bind next button click
						$(selector).find(".fc-button-prev").click(function (e) {
							e.preventDefault();
							$(selector).fullCalendar("prev");
							Calendar.loadDynamicEvents($(selector).fullCalendar("getView").name, $(selector).fullCalendar("getDate"), buttons.callbacks.renderEvents);
						});

						//bind previous button click
						$(selector).find(".fc-button-next").click(function (e) {
							e.preventDefault();
							$(selector).fullCalendar("next");
							//load events onto the calendar
							Calendar.loadDynamicEvents($(selector).fullCalendar("getView").name, $(selector).fullCalendar("getDate"), buttons.callbacks.renderEvents);
						});
					}
				}
			};

			/* initialize the calendar */
			$(selector).fullCalendar({
				header: {
					left: '',
					center: 'title',
					right: ''
				},
				buttonText: {
					prev: '&laquo;',
					next: '&raquo;',
					prevYear: '&nbsp;&lt;&lt;&nbsp;',
					nextYear: '&nbsp;&gt;&gt;&nbsp;',
					today: 'today',
					month: 'month',
					week: 'week',
					day: 'day'
				},
				events: /*Calendar.Events*/null,
				defaultView: 'agendaWeek',
				editable: true,
				dropAccept: '.external-event',
				defaultEventMinutes: 45,
				slotMinutes: 15,
				minTime: '7:00am',
				disableResizing: false,
				allDaySlot: false,
				droppable: true, // this allows things to be dropped onto the calendar !!!
				drop: function (date, allDay, jsEvent, ui) { // this function is called when something is dropped

					// retrieve the dropped element's stored Event Object
					var originalEventObject = $(this).data('eventObject');
					originalEventObject.TreatmentLength = parseInt(originalEventObject.TreatmentLength, 10) > 0 ? originalEventObject.TreatmentLength : 30;

					// we need to copy it, so that multiple events don't have a reference to the same object
					var copiedEventObject = $.extend({}, originalEventObject);

					// assign it the date that was reported
					copiedEventObject.start = date;
					copiedEventObject.allDay = allDay;
					var startDate = new Date(date);
					copiedEventObject.end = new Date(startDate.getTime() + (originalEventObject.TreatmentLength * 60 * 1000));
					copiedEventObject.ClientID = originalEventObject.ClientID;
					copiedEventObject.className = "scheduled";

					//WCF Update objects
					var $this = this,
						options = {
							context: $this,
							callback: function (success, passedevent) {
								// render the event on the calendar
								// the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
								if (success) {
									copiedEventObject.TreatmentStatusID = passedevent.TreatmentStatusID;
									copiedEventObject.className = Utils.getEnumKey(Utils.TreatmentStatusEnum, passedevent.TreatmentStatusID);
									copiedEventObject.Weight = passedevent.Weight;
									copiedEventObject.Height = passedevent.Height;
									copiedEventObject.Arms = passedevent.Arms;
									copiedEventObject.UpperThigh = passedevent.UpperThigh;
									copiedEventObject.Booty = passedevent.Booty;
									copiedEventObject.Waist = passedevent.Waist;
									copiedEventObject.Chest = passedevent.Chest;
									copiedEventObject.Notes = passedevent.Notes;
									$(selector).fullCalendar('renderEvent', copiedEventObject, success);
									// remove event after drop
									$(this).remove();

									//pull remaining treatment
									var options = {
										callback: calendarPlugin.bindDraggableObjects,
										template: "#availableTreatmentListTemplate",
										container: "#availableTreatmentList",
										ClientName: copiedEventObject.title
									};
									Calendar.getBookingsByClientID(copiedEventObject.ClientID, options);
								}
							}
						},
						customClientTreatment = {
							ID: copiedEventObject.ID,
							start: copiedEventObject.start,
							end: copiedEventObject.end,
							allDay: false,
							TreatmentStatusID: Utils.TreatmentStatusEnum.Scheduled
							, Weight: originalEventObject.Weight
							, Height: originalEventObject.Height
							, Arms: originalEventObject.Arms
							, UpperThigh: originalEventObject.UpperThigh
							, Booty: originalEventObject.Booty
							, Waist: originalEventObject.Waist
							, Chest: originalEventObject.Chest
							, Notes: originalEventObject.Notes
							, className: 'Scheduled'
						};
					Calendar.updateBooking(customClientTreatment, options);

				},
				eventDrop: function (event, dayDelta, minuteDelta, allDay, revertFunc) {

					//WCF Update objects
					var $this = this,
						options = {
							context: $this,
							callback: function (success, treatment, isConflict) {
								// render the event on the calendar
								// the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
								if (success === true) {
									event.className = isConflict ? "ScheduledConflict" : "Pending";
									$(selector).fullCalendar('renderEvent', event, success);
									// remove event after drop
									$(this).remove();
								} else {
									if (event.previousStatusID) {
										event.VenueBookingStatusID = event.previousStatusID;
										event.className = Utils.getEnumKey(Utils.BookingStatusEnum, event.VenueBookingStatusID);
									}
									revertFunc();
								}
							}
						};

					if (event.VenueBookingStatusID == Utils.BookingStatusEnum.Cancelled) {
						$('#confirmMissedDialog').dialog({
							title: 'Reschedule?',
							Modal: true,
							buttons: {
								'Yes': function () {
									event.previousStatusID = event.TreatmentStatusID;
									event.TreatmentStatusID = Utils.TreatmentStatusEnum.Scheduled;
									event.className = 'Scheduled';
									Calendar.proxyUpdate(event, options);
									$(this).dialog('close');
								},
								'No': function () {
									options.callback(this, false);
									$(this).dialog('close');
								}
							}
						});
					} else {
						Calendar.proxyUpdateBookingTimes(event, options);
					}
				},
				eventResize: function (event, dayDelta, minuteDelta, revertFunc, jsEvent, ui, view) {
					//WCF Update objects
					var $this = this,
						options = {
							context: $this,
							callback: function (success) {
								if (success) {
									// render the event on the calendar
									// the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
									$(selector).fullCalendar('renderEvent', event, success);
									// remove event after drop
									$(this).remove();
								} else {
									revertFunc();
								}
							}
						},
						customClientTreatment = {
							ID: event.ID,
							start: event.start,
							end: event.end,
							allDay: false,
							VenueBookingStatusID: Utils.BookingStatusEnum.Pending,
							className: 'Scheduled'
						};
					Calendar.proxyUpdateBookingTimes(event, options);
				},
				eventDragStart: function (event, jsEvent, ui, view) {
					if (event.VenueBookingStatusID === Utils.BookingStatusEnum.Attended) {
						$(ui.helper.context).css({ opacity: 1 });
						jsEvent.preventDefault();
						$.jGrowl("Attended bookings cannot be moved or rescheduled");
						return false;
					}
					$(ui).removeClass("Scheduled");
				},
				dayClick: function(date, allDay, jsEvent, view) {
					//call the add booking dialog and pass in the start date
					$.calendarDialogues("addBooking", date,selector, buttons);
					//code below is left as a reference to the developer, now you know of extra params you can make use of.
					//alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
					//alert('Current view: ' + view.name);
					// change the day's background color just for fun
					//$(this).css('background-color', 'red');

				},
				eventAfterRender: function (event, element, view) {
					//alert("rendered"+event.title);
				},
				eventClick: function (event, jsEvent, view) {
					switch (event.VenueBookingStatusID) {
						case Utils.BookingStatusEnum.Pending:
							//reset notes for these events
							event.Notes = null;
							var callback = function (newEvent, cancelBooking) {
								//remove the old event, filter works better than sending in an ID don't know why
								$(selector).fullCalendar('removeEvents', function (events) {
									return event.ID == events.ID;
								});

								if (cancelBooking === false) {
									//redraw the event with the new information
									//$(selector).fullCalendar('renderEvent', newEvent,true);
									Calendar.loadDynamicEvents($(selector).fullCalendar("getView").name, $(selector).fullCalendar("getDate"), buttons.callbacks.renderEvents);
								} else {

									$(selector).fullCalendar("rerenderEvents");
								}
							};
							$.calendarDialogues('pendingTreatment', event, callback);
							break;
						case Utils.BookingStatusEnum.Attended:
							$.calendarDialogues('attendedTreatment', event);
							break;
						case Utils.BookingStatusEnum.Cancelled:
							$.calendarDialogues('cancelledTreatment', event);
							break;
						//case Utils.BookingStatusEnum.ScheduledConflict:
							//$.calendarDialogues('scheduledTreatment', event);
							//break;
					}

				},
				eventMouseover: function (event, jsEvent, view) {
					//$(this).addClass("calendarEventHover");
				},
				eventMouseout: function (event, jsEvent, view) {
					//$(this).removeClass("calendarEventHover");
				}
			});

			/*place buttons into the calendar*/
			buttons.init();
			$(selector).data("calendarevents", buttons);
			///// SWITCHING LIST FROM 3 COLUMNS TO 2 COLUMN LIST /////
			function reposTitle() {
				if ($(window).width() < 450) {
					if (!$('.fc-header-title').is(':visible')) {
						if ($('h3.calTitle').length === 0) {
							var m = $('.fc-header-title h2').text();
							$('<h3 class="calTitle">' + m + '</h3>').insertBefore('#calendar table.fc-header');
						}
					}
				} else {
					$('h3.calTitle').remove();
				}
			}
			reposTitle();

			///// ON RESIZE WINDOW /////
			$(window).resize(function () {
				reposTitle();
			});
			//END INIT
		},
		bindDraggableObjects: function () {
			$('div.external-event').each(function () {
				// create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
				// it doesn't need to have a start or end
				var eventObject = {
					title: $.trim($(this).text()) // use the element's text as the event title
					, editable: true
					, allDay: false
					, ID: $(this).attr("data-id")
					, ClientID: $(this).attr("data-clientid")
					, ClientName: $(this).attr("data-clientname")
					, start: ($(this).attr("data-start") ? new Date($(this).attr("data-start")) : null)
					, end: ($(this).attr("data-end") ? new Date($(this).attr("data-end")) : null)
					, className: $(this).attr("data-className").toLowerCase()
					, TreatmentLength: $(this).attr("data-treatmentLength")
					, TreatmentStatusID: parseInt($(this).attr("data-TreatmentStatusID"), 10)
					, Weight: $(this).attr("data-Weight") ? $(this).attr("data-Weight") : null
					, Height: $(this).attr("data-Height") ? $(this).attr("data-Height") : null
					, Arms: $(this).attr("data-Arms") ? $(this).attr("data-Arms") : null
					, UpperThigh: $(this).attr("data-UpperThigh") ? $(this).attr("data-UpperThigh") : null
					, Booty: $(this).attr("data-Booty") ? $(this).attr("data-Booty") : null
					, Waist: $(this).attr("data-Waist") ? $(this).attr("data-Waist") : null
					, Chest: $(this).attr("data-Chest") ? $(this).attr("data-Chest") : null
					, Notes: ""
				};

				// store the Event Object in the DOM element so we can get to it later
				$(this).data('eventObject', eventObject);

				// make the event draggable using jQuery UI
				jQuery(this).draggable({
					zIndex: 999,
					revert: true,	  // will cause the event to go back to its
					revertDuration: 0  //  original position after the drag
				});

			});
		}
	};
	//#endregion

	//#region Calendar WCF Module Inter-facer!
	var Calendar = {
		Events: null,
		Clients: null,
		selector: null,
		CalendarContractID: null,
		error : function(e){
			jAlert("Something is not quit right, please try again.");
		},
		loadVenues: function () {
			var dfd = $.Deferred();

			$("#grid").block();
			var success = function (_result) {
				
				$("#VenueID").html($("#venueTemplate").tmpl(_result.EntityList)).prepend("<option value='0'>--select a venue--</option>").val(0);
				$("#grid").unblock();
				$.uniform.update();
				dfd.resolve();
			};

			var error = function (e) {
				jAlert("Unable to load venues, please try again.");
				dfd.reject();
				$("#grid").unblock();
			};
			AdminService.WCFAdmin.GetVenues(false,success, Calendar.error);

			return dfd.promise();
		},
		loadDynamicEvents: function (view, loadedDate,callbackFn) {
			var fromDate = new Date(loadedDate);
			var toDate = null;
			var day = null;
			var singleEpochDay = 86400 * 1000;
			var daysToStep = 0;
			switch (view) {
				case 'month':
					daysToStep = 41;
					//add 42 days from the first Sunday
					//set loaded date to the first of the loaded month
					if (loadedDate.getDate() != 1) {
						//set to the first day of the month
						loadedDate.setDate(1);
					}
					//set date to the last day of the previous month
					fromDate.setDate(0);
					//determine which day we on, then calculate the Sunday of that week
					day = fromDate.getDay();
					if (day < 6) {
						fromDate.setDate(fromDate.getDate() - day);
					} else {
						//Sunday is 1st of month 
						fromDate = new Date(loadedDate);
					}
					break;
				case 'agendaWeek':
					daysToStep = 6;
					//add 6 days from the first Sunday
					day = fromDate.getDay();
					if (day < 6) {
						fromDate.setDate(fromDate.getDate() - day);
					} else {
						//Sunday is 1st of month 
						fromDate = new Date(loadedDate);
					}
					break;
				case 'agendaDay':
					//no addition required
					fromDate = loadedDate;
					toDate = loadedDate;
					break;
			}

			//now set the toDate based on the above calculations
			toDate = new Date(fromDate.getTime() + (singleEpochDay * daysToStep));

			Calendar.getBookingsByDates(fromDate, toDate, callbackFn);

		},
		getBookingsByDates : function(fromDate, toDate,callbackFn){
			var dfd = $.Deferred();
			$("#grid").block();
			var response = function (result) {
				if (typeof (callbackFn) === "function") {
					callbackFn(result.EntityList);
				}
				$("#grid").unblock();
				dfd.resolve();
			};

			var fail = function () {
				Calendar.error();
				$("#grid").unblock();
				dfd.reject();
			};

			AdminService.WCFAdmin.GetBookingListByDates($("#VenueID").val(), fromDate, toDate, response, fail);

			return dfd.promise();
		},
		loadBookingsAndInitCalendar: function (VenueID) {
			//Calendar.CalendarContractID = (typeof (CalendarContractID) !== "undefined") ? CalendarContractID : null;
			$.when(Calendar.getBookings(VenueID, { setEvents: true })).then(function () {
				calendarPlugin.init("#calendar");
				//if (Calendar.CalendarContractID) {
					//var sampleEvent = Calendar.Events[0];
					//$(Calendar.selector).fullCalendar("gotoDate", sampleEvent.CalendarStartYear, sampleEvent.CalendarStartMonth - 1, sampleEvent.CalendarStartDate);
					//$(Calendar.selector).fullCalendar( 'changeView', "month" );
				//}
			});

		},
		getBookings: function (VenueID, options) {
			var dfd = $.Deferred();

			var response = function (_result) {
				if (options.setEvents === true) {
					if (_result.Count > 0) {
						//Calendar.Events = _result.EntityList;
						Calendar.StartDate = _result.EntityList[0]["StartDateTime"];
					} else {
						Calendar.Events = null;
					}
				} else {
					$(options.container).html($(options.template).render(_result.EntityList));
					options.callback.call(this);
				}
				dfd.resolve();
			};

			var fail = function (err) {
				Calendar.Events = null;
				dfd.resolve();
			};

			//retrieving bookings
			var date = new Date();
			AdminService.WCFAdmin.GetBookingList(VenueID, new Date(date.getFullYear(), date.getMonth(), date.getDate(),0,0,0), response, fail);

			return dfd.promise();
		},
		getBookingsByFilter: function (filter, options) {
			var dfd = $.Deferred();

			var response = function (_result) {
				$(options.container).html($(options.template).render(_result.EntityList));
				if (typeof options.callback === "function") {
					options.callback.call(this);
				}
				dfd.resolve();
			};

			var fail = function (err) {
				Calendar.Events = null;
				dfd.resolve();
			};

			//MetadataService.WCFMetadata.GetLookUps
			CalendarService.WCFCalendar.GetBookingsByFilter(filter, response, fail);

			return dfd.promise();
		},
		getBookingsByClientID: function (ClientID, options) {
			var dfd = $.Deferred();

			var response = function (_result) {
				if (_result.Count > 0) {
					for (var i = 0 ; i < _result.EntityList.length; i++) {
						_result.EntityList[i]["RemainingTreatments"] = _result.Count;
					}
					$(options.container).html($(options.template).render(_result.EntityList));
				} else {
					$(options.container).html("<p style='padding-top: 9px;margin: 0;'>No unscheduled appointments found for " + options.ClientName + "</p>");
				}

				if (typeof options.callback === "function") {
					options.callback.call(this);
				}
				dfd.resolve();
			};

			var fail = function (err) {
				Calendar.Events = null;
				dfd.resolve();
			};

			//MetadataService.WCFMetadata.GetLookUps
			CalendarService.WCFCalendar.GetBookingsByClientID(ClientID, 1, response, fail);

			return dfd.promise();
		},
		getLastClientBooking: function (ClientID, CalendarEvent, options) {
			var dfd = $.Deferred();

			var response = function (_result) {
				//set result into options result key
				options.result = _result;
				//execute any callbacks attached
				if (typeof options.callback === "function") {
					options.callback.call(this, options);
				}
				dfd.resolve();
			};

			//MetadataService.WCFMetadata.GetLookUps
			CalendarService.WCFCalendar.GetLastClientBooking(ClientID, CalendarEvent, response, Utils.responseFail);

			return dfd.promise();
		},
		proxyUpdate: function (event, options) {
			var customBooking = {
				ID: event.ID,
				OccupantID: event.OccupantID,
				Name: event.Name,
				Email: event.Email,
				PhoneNumber: event.Phone,
				Requirements: event.Requirements,
				start: event.start,
				end: event.end,
				allDay: false,
				VenueBookingStatusID: event.VenueBookingStatusID,
				className: Utils.getEnumKey(Utils.BookingStatusEnum, event.VenueBookingStatusID)
			};
			Calendar.updateBooking(customBooking, options);
		},
		proxyUpdateBookingTimes: function (event, options) {
			var customBooking = {
				ID: event.ID,
				start: event.start,
				end: event.end,
				allDay: false,
				VenueBookingStatusID: event.VenueBookingStatusID,
				className: Utils.getEnumKey(Utils.BookingStatusEnum, event.VenueBookingStatusID)
			};
			Calendar.updateBookingTimes(customBooking, options);
		},
		updateBooking: function (CustomBooking, options) {
		},
		updateBookingTimes: function (CustomBooking, options) {
			var dfd = $.Deferred();
			$('#contentwrapper').block();
			var response = function (_result) {
				var isConflict = false;
				if (_result.EntityList[0].success === false) {
					var message = _result.EntityList[0].message || "Unable to update booking, please try again.";
					$.jGrowl(message, { header: "<b class='jGrowlError'>Oops</b>", sticky: true });
				} else if (_result.EntityList[0].success === true) {
					$.jGrowl(_result.EntityList[0].message, { header: "<b class='jGrowlSuccess'>Booking Updated</b>", sticky: true });
					isConflict = false;
				}

				//expecting response to be handled by a callback
				if (typeof options.callback === "function") {
					options.callback.call(options.context, _result.EntityList[0].success, CustomBooking, isConflict);
				}

				$('#contentwrapper').unblock();
				dfd.resolve();
			};

			var fail = function (err) {
				$.jGrowl("Failed to update booking. Please try again.", { header: "<b class='jGrowlError'>Error</b>", sticky: true });
				dfd.resolve();
			};

			//MetadataService.WCFMetadata.GetLookUps
			AdminService.WCFAdmin.UpdateBookingTimes(CustomBooking.ID, CustomBooking.start, CustomBooking.end, response, fail);

			return dfd.promise();
		},
		loadBookingStatusList: function (selector, templateSelector) {
		},
		removeEvent: function (eventID) {
			$(Calendar.selector).fullCalendar('removeEvents', function (e) {
				if (e.ID === eventID) {
					var clientID = e.ClientID;
					var options = {
						callback: calendarPlugin.bindDraggableObjects,
						template: "#availableTreatmentListTemplate",
						container: "#availableTreatmentList",
						ClientName: e.ClientName
					};
					Calendar.getBookingsByClientID(clientID, options);
					return true;
				}
			});

		},
		renderEvent: function (event, success) {
			$(Calendar.selector).fullCalendar('renderEvent', event, success);

		},
		getEventInfo: function (ClientTreatmentID) {
		}
	};
	//#endregion

	//#region PageEvents
	var PageEvents = {
		load: function () {
			//first load the venues into the selector
			Calendar.loadVenues();
			this.bindEvents();
		},
		bindEvents: function () {
			$("#Tabs").tabs();

			//set venue on change to load calendar
			$("#VenueID").change(function (e) {
				if ($(this).val() === "0") {
					$("#calendar").fullCalendar('destroy');
				} else {
					//load the calendar and its bookings
					$("#calendar").fullCalendar('destroy');
					Calendar.loadBookingsAndInitCalendar($(this).val());
					//calendarPlugin.init("#calendar");
				}

			});

			$('#calendarDialog').on("change.BookingStatus", ".BookingStatus", PageEvents.bookingStatus_change);

			$('#calendar').on("mousedown.fc-event", ".fc-event", function (e) {
				if (e.which == 3) {
					e.preventDefault();
					//$(this).toggleClass("calendarEventHover");
					$.HoneycombCalendar("getEventInfo", $(this).attr("data-id"));
				}
			});

			$('#SearchFilter').keypress(function (e) {
				//if enter key pressed
				if (e.which === 13) {
					e.preventDefault();
				}
			});
			
			$("#legendButton").on("click", PageEvents.legendHover);
			//$("#legendButton").on("hover", PageEvents.legendHover);

		},
		bookingStatus_change: function () {
			if (parseInt(Utils.TreatmentStatusEnum.Cancelled, 10) === parseInt($(this).val(), 10)) {
				$('#calendarDialog').find('.optional-fields').hide();
			} else {
				$('#calendarDialog').find('.optional-fields').show();
			}
		},
		legendHover: function (e) {

			//alert($("#calendar").fullCalendar("next").fullCalendar("getDate"));
			if (e.type == "mouseenter") {
				$("#calendarLegend").show();
			} else {
				$("#calendarLegend").hide();
			}
		}
	};
	//#endregion

	$.HoneycombCalendar = function (method) {
		// Method calling logic
		if (Calendar[method]) {
			return Calendar[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else {
			$.error('Method ' + method + ' does not exist on jQuery.calendarDialogues');
		}
	};

	$(function () {
		//doc.oncontextmenu = function () { return false; };
		PageEvents.load();
	});
}
)(jQuery, document);