﻿/*global Properties,PropertyService,Entities,jAlert,Utils,jQuery*/

var Staff = {
    loadTab: function (_event,_overide) {
        var $tab = Utils.tabs.getCurrentTab().find(".stafftab");
        var $PropertyID = Properties.GetPropertyID();

        $tab.data("StaffList",[]);

        if (_overide === undefined) {
            _overide = false;
        }

        if (!$tab.data("loaded") || _overide) {

            var response = function (_result) {

                if (_result.EntityList.length > 0) {

                    $tab.data("StaffList", _result.EntityList);
                    Utils.bindDataToGrid($tab.selector + " #StaffContainer", "#PropertyStaffTemplate", _result.EntityList);
                    Utils.tabs.initTab($tab.selector);
                    //reactivate the accordion
                    $tab.find(".accordion").accordion('destroy').accordion({ collapsible: true, active: false });

                    $.when(Staff.GetJobTitleOptions(), Staff.GetSalutationOptions()).then(function () {

                        $tab.unblock();
                        //loop through each staff member and populate the select drop downs
                        for (var i = 0; i < _result.EntityList.length; i++) {

                            var staffID = "#staffno-" + _result.EntityList[i].ID;

                            //activate the tags input for display
                            $tab.find(staffID + ' .readOnlyTags').tagsInput({ interactive: false, removeWithBackspace: false });//, height: '38px', width: '220px'

                            //init file uploader
                            $tab.find(staffID + " .fubExample").fineUploader({
                                uploaderType: 'basic',
                                multiple: false,
                                autoUpload: true,
                                button: $tab.find(staffID + " .fubUploadButton"),
                                request: {
                                    endpoint: "/Pages/UploadFile.ashx"
                                }
                            }).on('complete', function (event, id, filename, responseJSON) {//ignore jslint
                                var $this = $(this);
                                $tab.find("#staffno-" + $this.attr("staffid") + " .photoDiv").css({ "background-image": "url(/Pages/DownloadFile.ashx?FileRef=" + responseJSON.fileid + ")" }).addClass("photoDivWithPhoto");
                                $tab.find("#staffno-" + $this.attr("staffid") + " input[name=FileID]").val(responseJSON.fileid);
                            })//ignore jslint
                            .on("submit", function (event, id, filename) {//ignore jslint
                                $tab.find("#staffno-" + $(this).attr("staffid") + " .photoDiv").css("background-image", "url(/Scripts/plugins/fileupload/loading.gif)").removeClass("photoDivWithPhoto");
                            })//ignore jslint
                            .on("error", function (event, id, filename, reason) {//ignore jslint
                                
                            });//ignore jslint

                            //populate the job title drop down
                            Utils.bindDataToGrid($tab.selector + " " + staffID + " select[name=JobTitleID]", "#ddlTemplate", Staff.JobTitleOptions);
                            //set the selected value
                            $tab.find(staffID + " select[name=JobTitleID]").val(_result.EntityList[i].JobTitleID);
                            //populate the key occupant rel select
                            Utils.bindDataToGrid($tab.selector + " " + staffID + " select[name=SalutationID]", "#ddlTemplate", Staff.SalutationOptions);
                            //set the selected value
                            $tab.find(staffID + " select[name=SalutationID]").val(_result.EntityList[i].SalutationID);

                            $tab.find(staffID + " input[name=GOVID]").tagHolder();

                        }

                        // activate the accordion
                        $tab.find(".accordion").accordion('destroy').accordion({ collapsible: true, active: false });
                        //update the controls after the population is complete
                        jQuery.uniform.update();
                    });
                } else {
                    //$.jGrowl("No records to display");
                    $tab.unblock();
                }
            };

            $tab.data("loaded", true);
            $tab.block();

            PropertyService.WCFProperty.GetPropertyStaff($PropertyID, response, Utils.responseFail);
        }
    },
    updateStaff: function (_event) {
        var $tab = Staff.GetTab();
        var $propertyID = Properties.GetPropertyID();
        var $staffID = 0;

        if ($(_event.target).is("span")) {
            $staffID = $(_event.target).parent().attr("staffid");
        } else {
            $staffID = $(_event.target).attr("staffid");
        }

        var $StaffDiv = $($tab.selector + " #staffno-" + $staffID);
        var StaffMember = new Entities.Staff();

        if (Utils.validation.validateForm($StaffDiv.selector, "", false, false)) {

            $StaffDiv.mapToObject(StaffMember, "name");

            $StaffDiv.block();

            var response = function (_result) {
                $StaffDiv.unblock();

                if (_result.EntityList[0] === "") {
                    //reload occupants to reflect latest changes
                    Staff.loadTab(null,true);
                } else {
                    jAlert(_result.EntityList[0], "Error: Save Failed");
                }
            };

            PropertyService.WCFProperty.UpdateStaff(StaffMember, $propertyID, response, Utils.responseFail);
        }
    },
    addStaff: function () {
        var $tab = Staff.GetTab();

        if ((parseInt($tab.find("#staffno-0").length, 10) === 0)) {
            var StaffMember = new Entities.Staff();

            StaffMember.Active = true;

            $("#PropertyStaffTemplate").tmpl(StaffMember).appendTo($tab.selector + " #StaffContainer");
            var staffDiv = $tab.find("#staffno-0");
            //activate the tags input for display
            staffDiv.find('.readOnlyTags').tagsInput({ interactive: false, removeWithBackspace: false });//, height: '38px', width: '220px'

            $($tab.selector + " #StaffContainer h3 a").last().text("New Staff Member");

            $tab.block();

            $.when(Staff.GetJobTitleOptions(), Staff.GetSalutationOptions()).then(function () {
                
                //init file uploader
                $tab.find("#staffno-0 .fubExample").fineUploader({
                    uploaderType: 'basic',
                    multiple: false,
                    autoUpload: true,
                    button: $tab.find("#staffno-0 .fubUploadButton"),
                    request: {
                        endpoint: "/Pages/UploadFile.ashx"
                    }
                }).on('complete', function (event, id, filename, responseJSON) {//ignore jslint
                    var $this = $(this);
                    $tab.find("#staffno-0 .photoDiv").css({ "background-image": "url(/Pages/DownloadFile.ashx?FileRef=" + responseJSON.fileid + ")" }).addClass("photoDivWithPhoto");
                    $tab.find("#staffno-0 input[name=FileID]").val(responseJSON.fileid);
                })//ignore jslint
                .on("submit", function (event, id, filename) {//ignore jslint
                    $tab.find("#staffno-0 .photoDiv").css("background-image", "url(/Scripts/plugins/fileupload/loading.gif)").removeClass("photoDivWithPhoto");
                })//ignore jslint
                .on("error", function (event, id, filename, reason) {//ignore jslint

                });//ignore jslint
                //end file upload init

                //populate the job title drop down
                Utils.bindDataToGrid($tab.selector + " #staffno-0 select[name=JobTitleID]", "#ddlTemplate", Staff.JobTitleOptions);
                //populate the key occupant rel select
                Utils.bindDataToGrid($tab.selector + " #staffno-0 select[name=SalutationID]", "#ddlTemplate", Staff.SalutationOptions);

                $tab.find("#staffno-0 input[name=GOVID]").tagHolder();
                $tab.find("#staffno-0 input[name=GOVID]").tagHolder("disable");

                $tab.unblock();
                //update the custom field types
                $.uniform.update();
            });

            $tab.find(".accordion").accordion('destroy').accordion({ collapsible: true, active: false });
            $tab.find(".accordion").accordion("activate", ($tab.data("StaffList").length));
            $tab.find("#staffno-0 .updatestaff span").html("Add Staff Member");
            Utils.tabs.initTab($tab.selector + " #staffno-0");

        } else {
            jAlert("You can only add one new staff member at a time. Please click the 'Add Staff Member' button to save the new staff.", "Cannot add new staff");
        }
    },
    GetSalutationOptions: function () {
        var dfd = new $.Deferred();

        var response = function (_result) {
            Staff.SalutationOptions = _result.EntityList;
            dfd.resolve();
        };

        PropertyService.WCFProperty.GetLookUpOptions(10001, response, function (e) { dfd.reject(); Utils.responseFail(e); });
        return dfd.promise();
    },
    GetJobTitleOptions: function () {
        var dfd = new $.Deferred();

        var response = function (_result) {
            Staff.JobTitleOptions = _result.EntityList;
            dfd.resolve();
        };

        PropertyService.WCFProperty.GetLookUpOptions(10000, response, function (e) { dfd.reject(); Utils.responseFail(e); });
        return dfd.promise();
    },
    GetStaffNotes: function (_event) {
        var $staffTab = Staff.GetTab();
        var $staffID = 0;

        if ($(_event.target).is("span")) {
            $staffID = $(_event.target).parent().attr("staffid");
        } else {
            $staffID = $(_event.target).attr("staffid");
        }

        var response = function (_result) {

            $staffTab.unblock();
            
            $("#staffnotes #NoteID").children().remove();
            $("#staffnotes #NoteID").append("<option value='0'>Add New</option>");

            if (_result.EntityList.length > 0) {
                //populate the drop down with the notes                
                $("#StaffNotesTemplate").tmpl(_result.EntityList).appendTo("#staffnotes #NoteID");
                Staff.StaffNotes = _result.EntityList;
                Staff.ClearStaffNote();
            }

            //call the various update methods to refresh the custom controls
            $("#staffnotes #NoteID").trigger("liszt:updated");
            Utils.tabs.initTab("#staffnotes");
            $.uniform.update();

            //show dialog
            $("#staffnotes").dialog({
                resizable: false,
                modal: false,
                width: "600px",
                title: "Staff Notes",
                buttons: {
                    Update: function () {
                        $("#staffnotes").block();

                        var updateResponse = function (_results) {
                            $("#staffnotes").unblock();
                            $("#staffnotes").dialog("close");
                            Staff.ClearStaffNote();
                        };

                        var StaffNote = new Entities.StaffNote();

                        StaffNote.Note = $("#staffnotes textarea[name=NoteDescription]").val();
                        StaffNote.ID = $("#staffnotes #NoteID").val();
                        StaffNote.StaffID = $staffID;
                        StaffNote.Title = $("#staffnotes input[name=Title]").val();

                        PropertyService.WCFProperty.UpdateStaffNote(StaffNote, updateResponse, Utils.responseFail);

                    },
                    Close: function () {
                        $(this).dialog("close");
                    }
                }
            });

        };

        if (!isNaN($staffID) && parseInt($staffID,10) !== 0) {

        	$staffTab.block();

        	PropertyService.WCFProperty.GetStaffNotes($staffID, response, Utils.responseFail);
        } else {
        	jAlert("You cannot edit this staff members notes. Please add the staff member first.");
        }
    },
    ClearStaffNote: function () {
        $("#staffnotes textarea[name=NoteDescription]").val("");
        $("#staffnotes #NoteID").val(0);
        $("#staffnotes input[name=Title]").val("");
    },
    StaffNoteChange: function (_event) {

        var $select = $(_event.target);
        var $selectedIndex = ($select.find(":selected").index() - 1);

        if (Staff.StaffNotes.length > 0 && $selectedIndex != -1) {
            //insert the values
            var $popup = $("#staffnotes");
            //populate the fields
            $popup.find("input[name=Title]").val(Staff.StaffNotes[$selectedIndex].Title);
            $popup.find("textarea[name=NoteDescription]").val(Staff.StaffNotes[$selectedIndex].Note);
        } else {
            Staff.ClearStaffNote();
        }
    },
    SalutationOptions: [],
    JobTitleOptions: [],
    StaffList: [],
    StaffNotes: [],
    GetStaffErven: function (_staffID,_staffContainer) {
        var dfd = new $.Deferred();

        var response = function (_result) {
            //you have the staff erven now build the access lists

            if (_result.EntityList.length > 0) {

                //clear list
                var staffErventags = _staffContainer.find("input[name=staffErven]");
                staffErventags.importTags('');

                for (var i = 0; i < _result.EntityList.length; i++) {
                    staffErventags.addTag(_result.EntityList[i].ErfNumber);
                }

                //remove the delete icons
                _staffContainer.find(".readonlytaginputs .tag a").remove();
            }

            dfd.resolve();
        };

        PropertyService.WCFProperty.GetStaffErven(_staffID, response, function (e) { dfd.reject(); Utils.responseFail(e); });
        return dfd.promise();
    },
    StaffSelected: function (event, ui) {

        var staffID = $(ui.newContent[0]).find("input[name=ID]").val();
        var staffDiv = $(ui.newContent[0]);

        if (ui.newContent.length > 0) {

            if (parseInt(staffID, 10) !== 0) {

                if (!staffDiv.data("loaded")) {

                    staffDiv.data("loaded", true);

                    staffDiv.block();
                   
                    var response = function (_result) {
                        //you have the staff erven now build the access lists
                        staffDiv.unblock();

                        if (_result.EntityList.length > 0) {

                            //clear list
                            var staffErventags = staffDiv.find("input[name=staffErven]");
                            staffErventags.importTags('');

                            for (var i = 0; i < _result.EntityList.length; i++) {
                                staffErventags.addTag(_result.EntityList[i].ErfNumber);
                            }

                            //remove the delete icons
                            staffDiv.find(".readonlytaginputs .tag a").remove();
                        }

                    };

                    PropertyService.WCFProperty.GetStaffErven(staffID, response, Utils.responseFail);
                }
            }
        }
    },
    GetStaffByID: function (event) {
        var $tab = Utils.tabs.getCurrentTab();
        var staffDiv = $($tab.selector + " #staffno-" + $(event.target).attr("staffid"));
        var staffGOVID = $(event.target).val();

        var response = function (_result) {
            staffDiv.unblock();

            if (_result.EntityList[0] !== null) {
                //bind data to the form
                //Utils.bindDataToGrid(staffDiv.selector, "#PropertyStaffTemplate", _result.EntityList[0]);
                staffDiv.mapObjectTo(_result.EntityList[0], "name", true);
                $.uniform.update();
            }

        };

        staffDiv.block();

        PropertyService.WCFProperty.GetStaffByID(staffGOVID,Properties.GetPropertyID(), response,Utils.responseFail);
    },
    GetTab: function () {
        var $tab = Utils.tabs.getCurrentTab().find(".stafftab");
        return $tab;
    }
};

var StaffPageEvents = {
    load: function(){
        StaffPageEvents.bindEvents();
    },
    bindEvents: function () {
        //staff tab load
        $('.bodywrapper').on("Honeycomb.tabLoad", ".stafftab", Staff.loadTab);
        //button clicks
        $(".bodywrapper").on("click.updatestaff", ".stafftab .updatestaff", Staff.updateStaff);
        $(".bodywrapper").on("click.staffNotesButton", ".stafftab .staffNotesButton", Staff.GetStaffNotes);
        $(".bodywrapper").on("click.addstaff", ".stafftab #addstaff", Staff.addStaff);
        //accordion activate event
        $(".bodywrapper").on("accordionchange", ".staffaccordion", Staff.StaffSelected);
        //staff id number change
        $(".bodywrapper").on("change.GOVID", ".stafftab input[name=GOVID]", Staff.GetStaffByID);
        $("#staffnotes #NoteID").on("change", Staff.StaffNoteChange);
    }
};

//staff dom ready
$(function () {
    StaffPageEvents.load();
});