﻿/*global PropertyService,Properties,Entities,dateFormat, jAlert,jConfirm,Utils */

var Lease = {
	loadTab: function (_event, _overide) {
		var $leaseTab = Lease.getTab();
		var $propertyID = Properties.GetPropertyID();
		var $propertyTab = Utils.tabs.getCurrentTab();

		if (_overide === undefined) {
			_overide = false;
		}

		//only run this once
		if (!$leaseTab.data("loaded") || _overide) {

			//#region lease file uploader
			//init file uploader
			$($leaseTab.selector + " #leaseUpload").fineUploader({
				uploaderType: 'basic',
				multiple: false,
				autoUpload: true,
				button: $($leaseTab.selector + " #leaseUploadButton"),
				request: {
					endpoint: "/Pages/UploadFile.ashx"
				}
			}).on('complete', function (event, id, filename, responseJSON) {//ignore jslint
				$leaseTab.find("input[name=FileID]").val(responseJSON.fileid);
				$leaseTab.unblock();
				$.jGrowl("File uploaded successfully");
			})//ignore jslint
            .on("submit", function (event, id, filename) {//ignore jslint
				$leaseTab.block();
            })//ignore jslint
            .on("error", function (event, id, filename, reason) {//ignore jslint
				$leaseTab.unblock();
				$.jGrowl("Error Uploading file");
            });//ignore jslint
			//#endregion

			//#region extend lease file uploader
			$leaseTab.block();

			//init the modal dialog fields
			//Utils.tabs.initTab($leaseTab.selector + " #extendLeaseModal");

			$leaseTab.data("loaded", true);

			//call functions to set up the leases tab
			$.when(Lease.GetAgencyOptions(), Lease.GetAgentOptions(), Lease.getLeaseExtensions(), Lease.GetPropertyTenants($propertyID)).then(function () {
				//call the lease retrieval now
				$.uniform.update();

				var leaseResponse = function (_leaseresult) {

					if (_leaseresult.EntityList[0] !== null) {

						if (_leaseresult.EntityList[0].Terminated !== null) {
							if (!_leaseresult.EntityList[0].Terminated) {
								//disable the lease termination fields if the lease is not terminated
								$leaseTab.find(".terminated").remove();
							}
						} else {
							//disable the lease termination fields if the lease is not terminated
							$leaseTab.find(".terminated").remove();
						}

						//map the values to the field
						$leaseTab.mapObjectTo(_leaseresult.EntityList[0], "name", false);
						//set the selected agency
						Lease.SetAgencyByAgent();
						//set the link to the file on record
						if (_leaseresult.EntityList[0].FileID !== null) {
							$leaseTab.find("#CurrentFile").attr("href", "/Pages/DownloadFile.ashx?FileRef=" + _leaseresult.EntityList[0].FileID);
						} else {
							$leaseTab.find("#CurrentFile").remove();
						}

						//set the selected tenant lease holder if there is one
						if (_leaseresult.EntityList[0].LeaseOccupantID !== null) {
							$leaseTab.find("select[name=LeaseOccupantID]").val(_leaseresult.EntityList[0].LeaseOccupantID);
						}

						$.uniform.update();
					} else {
						$leaseTab.find("#CurrentFile").remove();
						//disable the lease termination fields if the lease is not terminated
						$leaseTab.find(".terminated").remove();
						//remove the extend lease button
						$leaseTab.find("#extendLeaseAddButton").remove();
					}

					$leaseTab.unblock();
				};

				PropertyService.WCFProperty.GetPropertyLease(Properties.GetPropertyID(), leaseResponse, Utils.responseFail);
			});
		}

	},
	GetAgencyOptions: function () {
		var dfd = new $.Deferred();

		var $leaseTab = Lease.getTab();

		var response = function (_result) {

			if (_result.EntityList.length > 0) {

				Utils.bindDataToGrid($leaseTab.find("select[name=SalesAgent]").selector, "#ddlTemplate", _result.EntityList);
				$leaseTab.find("select[name=SalesAgent]").prepend("<option value='-1'>-- Select --</option>");
				Lease.Agencies = _result.EntityList;
			}

			dfd.resolve();
		};

		PropertyService.WCFProperty.GetSalesAgents(response, function (e) { dfd.reject(); Utils.responseFail(e); });
		return dfd.promise();
	},
	GetPropertyTenants: function (propertyID) {
		var dfd = new $.Deferred();

		var $leaseTab = Lease.getTab();

		var response = function (_result) {

			if (_result.EntityList.length > 0) {

				Utils.bindDataToGrid($leaseTab.find("select[name=LeaseOccupantID]").selector, "#ddlTenantTemplate", _result.EntityList);
				$leaseTab.find("select[name=LeaseOccupantID]").prepend("<option value='-1'>-- Select --</option>");
			}

			dfd.resolve();
		};

		PropertyService.WCFProperty.GetPropertyTenants(propertyID, response, function (e) { dfd.reject(); Utils.responseFail(e); });
		return dfd.promise();
	},
	GetAgentOptions: function () {
		var dfd = new $.Deferred();
		var $leaseTab = Lease.getTab();

		var response = function (_result) {

			if (_result.EntityList.length > 0) {
				Utils.bindDataToGrid($leaseTab.find("select[name=AgentID]").selector, "#agentDDLTemplate", _result.EntityList);
				$leaseTab.find("select[name=AgentID]").prepend("<option value='-1'>-- Select --</option>");
				Lease.Agents = _result.EntityList;
			}

			dfd.resolve();
		};

		PropertyService.WCFProperty.GetAgents(response, function (e) { dfd.reject(); Utils.responseFail(e); });
		return dfd.promise();
	},
	PopulateAgencyAgents: function (_event) {
		var $leaseTab = Lease.getTab();
		var tmpArr = [];
		var salesAgentSel = $leaseTab.find("select[name=SalesAgent]");
		var agencyID = parseInt(salesAgentSel.val(), 10);

		//get all the agents for the agency
		for (var i = 0; i < Lease.Agents.length; i++) {
			if (parseInt(Lease.Agents[i].AgencyID, 10) === agencyID) {
				tmpArr.push(Lease.Agents[i]);
			}
		}

		Utils.bindDataToGrid($leaseTab.find("select[name=AgentID]").selector, "#agentDDLTemplate", tmpArr);
		$leaseTab.find("select[name=AgentID]").prepend("<option value='-1'>-- Select --</option>");
		$.uniform.update();
	},
	SetAgencyByAgent: function () {
		var $leaseTab = Lease.getTab();
		var currentAgentID = parseInt($leaseTab.find("select[name=AgentID]").val(), 10);

		if (currentAgentID > 0) {
			//there is a valid selected agent, now set the agency
			//get the agency from the agent
			for (var i = 0; i < Lease.Agents.length; i++) {
				if (parseInt(Lease.Agents[i].AgentID, 10) === currentAgentID) {
					//set the agency and break out of the loop break out of the loop
					if (!isNaN(Lease.Agents[i].AgencyID)) {
						$leaseTab.find("select[name=SalesAgent]").val(Lease.Agents[i].AgencyID);
					}

					break;
				}
			}
		}

	},
	getTab: function () {
		return Utils.tabs.getCurrentTab().find(".leasetab");
	},
	ExtendLease: function (_event, _leaseID) {

		var $leaseID = 0;
		var leaseExtention = new Entities.LeaseExtension();
		var $leaseTab = Lease.getTab();

		if (_leaseID !== undefined) {
			$leaseID = _leaseID;
		}

		if ($leaseID === 0) {
			//it's a new extention you don't need to retrieve it
			Lease.OpenLeaseModal(leaseExtention);
		} else {
			//it's a current extention, get it from the propertyservice
			var response = function (_result) {
				$leaseTab.unblock();

				if (_result.EntityList[0] !== null) {
					//open the modal dialog
					Lease.OpenLeaseModal(_result.EntityList[0]);
				} else {
					$.jGrowl("Error retrieving lease extention.");
				}
			};

			$leaseTab.block();

			PropertyService.WCFProperty.GetLeaseExtention($leaseID, response, Utils.responseFail);
		}
	},
	OpenLeaseModal: function (_leaseExtention) {

		//clear the current html in the modal
		$("#extendLeaseModal").html("");

		//append the template to the modal dialog
		$("#LeaseExtensionModalTemplate").tmpl(_leaseExtention).appendTo("#extendLeaseModal");

		//intialize the controls on the element
		Utils.tabs.initTab("#extendLeaseModal");

		//#region register upload component
		//init file uploader
		$("#extendUpload").fineUploader({
			uploaderType: 'basic',
			multiple: false,
			autoUpload: true,
			button: $("#extendUploadButton"),
			request: {
				endpoint: "/Pages/UploadFile.ashx"
			}
		}).on('complete', function (event, id, filename, responseJSON) {//ignore jslint
			$("#extendLeaseModal input[name=FileID]").val(responseJSON.fileid);
			$("#extendLeaseModal").unblock();
		})//ignore jslint
        .on("submit", function (event, id, filename) {//ignore jslint
			$("#extendLeaseModal").block();
        })//ignore jslint
        .on("error", function (event, id, filename, reason) {//ignore jslint
			$("#extendLeaseModal").unblock();
			$.jGrowl("Error Uploading file");
        });//ignore jslint
		//#endregion 

		//open the dialog
		$("#extendLeaseModal").dialog({
			resizable: false,
			modal: true,
			width: "270px",
			title: "Extend Lease",
			buttons: {
				"Extend Lease": function () {

					var msg = "";

					//validate that the date has been entered and that a file has been uploaded
					if ($(this).find("input[name=FileID]").val() === "") {
						msg += "Please upload a file.";
					}

					if ($(this).find("input[name=EndDate]").val() === "") {
						msg += "\nPlease supply the extention date.";
					}

					if (msg !== "") {
						alert(msg);
					} else {
						var extendResponse = function (_extendResult) {
							$("#extendLeaseModal").unblock();
							$("#extendLeaseModal").dialog("close");

							if (!_extendResult.EntityList[0]) {
								jAlert("There was an error while attempting to save the lease extention.", "Error");
							} else {
								Lease.getLeaseExtensions();
								jAlert("The lease has been extended.", "Lease Extended");
							}
						};

						$(this).block();

						var LeaseExtention = new Entities.LeaseExtension();

						$("#extendLeaseModal").mapToObject(LeaseExtention, "name");

						LeaseExtention.PropertyID = Properties.GetPropertyID();

						PropertyService.WCFProperty.ExtendLease(LeaseExtention, extendResponse, Utils.responseFail);
					}

				},
				Close: function () {
					$(this).dialog("close");
				}
			}
		});
		//end extend lease modal
	},
	getLeaseExtensions: function () {

		var leaseExtTable = Lease.getTab().find(".leaseExtentions");

		var exResponse = function (_result) {

			if (_result.EntityList.length > 0) {
				leaseExtTable.parent("table").show();
				Utils.bindDataToGrid(leaseExtTable.selector, "#LeaseExtensionTemplate", _result.EntityList);
			} else {
				leaseExtTable.parent("table").hide();
			}

		};

		PropertyService.WCFProperty.GetPropertyLeaseExtentions(Properties.GetPropertyID(), exResponse, Utils.responseFail);
	},
	terminateLease: function () {
		//yes because the lease id is the property id: hahahahaha
		var leaseID = Properties.GetPropertyID();

		$("#modalcontainer").html("Termination Reason<br/><textarea name='TerminationReason' cols='80' rows='5' class='longinput'></textarea>");

		$("#modalcontainer").dialog({
			resizable: false,
			modal: true,
			width: "270px",
			title: "Lease Termination",
			buttons: {
				"Terminate": function () {
					var termResponse = function (_termResult) {

						$("#modalcontainer").unblock();
						$("#modalcontainer").dialog("close");

						if (_termResult.EntityList[0] !== null) {

							if (_termResult.EntityList[0] === "") {
								jAlert("Successfully terminated lease.", "Lease Terminatd");
							} else {
								jAlert("There was an error terminating the lease.", "Error");
							}

						} else {
							jAlert("There was an error terminating the lease.", "Error");
						}
					};

					$("#modalcontainer").block();

					PropertyService.WCFProperty.TerminateLease(leaseID, $("#modalcontainer textarea[name=TerminationReason]").val(), termResponse, Utils.responseFail);
				},
				"Cancel": function () { $(this).dialog("close"); }
			}
		});

	},
	loaded: false,
	Agencies: [],
	Agents: []
};

//page events
var LeasePageEvents = {
	load: function () {
		LeasePageEvents.bindEvents();
	},
	bindEvents: function () {
		//lease tab load
		$('.bodywrapper').on("Honeycomb.tabLoad", ".leasetab", Lease.loadTab);
		$('.bodywrapper').on("change", ".leasetab select[name=SalesAgent]", Lease.PopulateAgencyAgents);

		$(".bodywrapper").on("click", ".leasetab #extendLeaseAddButton", Lease.ExtendLease);

		$(".bodywrapper").on("click", ".leasetab .terminateLeaseButton", LeasePageEvents.TerminateLease_Click);

	},
	TerminateLease_Click: function (_event) {
		jConfirm("Terminating the lease will result in denying the current tenants access to the estate. Tenants will be archived and the lease termination date set to the currrent date. Are you sure you want to terminate the lease?", "Terminate Lease?", function (_confirm) {
			if (_confirm) {
				Lease.terminateLease();
			}
		});
	}
};

//document ready load
$(function () {
	LeasePageEvents.load();
});