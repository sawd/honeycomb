﻿/*global PropertyService, Properties,jAlert,Entities,jConfirm,jQuery,Utils*/
//main personal object
var Personal = {
    load: function () {

        var $tab = Personal.getTab();
        $tab.block();

        $.when(Pets.loadPets(), GolfCarts.loadCarts(), Vehicles.loadVehicles()).then(function () {//ignore jslint

            $tab.unblock();

        });

    },
    getTab: function () {
        return Utils.tabs.getCurrentTab().find(".personaltab");
    }
};

//pets object
var Pets = {//ignore jslint
    loadPets: function () {
        var $tab = Personal.getTab();
        var $petAccordion = $tab.find(".petAccordion");
        
        var response = function (_result) {

            $tab.data("PetsList", _result.EntityList);

            if (_result.EntityList.length > 0) {
                $petAccordion.show();

                Utils.bindDataToGrid($petAccordion.selector, "#PetsAccordionTemplate", _result.EntityList);
                Utils.tabs.initTab($petAccordion.selector);
                for (var i = 0; i < _result.EntityList.length; i++) {
                    var PetID = "#pet-" + _result.EntityList[i].PetID;

                    //init file uploader
                    $petAccordion.find(PetID + " .fubExample").fineUploader({
                        uploaderType: 'basic',
                        multiple: false,
                        autoUpload: true,
                        button: $tab.find(PetID + " .fubUploadButton"),
                        request: {
                            endpoint: "/Pages/UploadFile.ashx"
                        }
                    }).on('complete', function (event, id, filename, responseJSON) {//ignore jslint
                        var $this = $(this);
                        $tab.find("#pet-" + $(this).data('pet-id') + " .photoDiv").css({ "background-image": "url(/Pages/DownloadFile.ashx?FileRef=" + responseJSON.fileid + ")" }).addClass("photoDivWithPhoto");
                        $tab.find("#pet-" + $(this).data('pet-id') + " input[name=PetPhoto]").val(responseJSON.fileid);
                    })//ignore jslint
                    .on("submit", function (event, id, filename) {//ignore jslint
                        $tab.find("#pet-" + $(this).data('pet-id') + " .photoDiv").css("background-image", "url(/Scripts/plugins/fileupload/loading.gif)").removeClass("photoDivWithPhoto");
                    })//ignore jslint
                    .on("error", function (event, id, filename, reason) {//ignore jslint

                    });//ignore jslint
                    //end file upload init
                }

                $petAccordion.find(".filesButton").files();
                

                $petAccordion.accordion('destroy').accordion({ collapsible: true, active: false });

            } else {
                $petAccordion.hide();
            }
        };

        PropertyService.WCFProperty.GetPropertyPets(Properties.GetPropertyID(),response,Utils.responseFail);
    },
    addPet: function () {
        var $tab = Personal.getTab();
        var $petAccordion = $tab.find(".petAccordion");

        if ($petAccordion.find("#pet-0").length === 0) {
            var pet = new Entities.Pet();

            pet.PropertyID = Properties.GetPropertyID();
            $petAccordion.show();

            $("#PetsAccordionTemplate").tmpl(pet).appendTo($petAccordion.selector);

            $($petAccordion.selector + " h3 a").last().text("New Pet");
       
            $petAccordion.find('#pet-0 .photoDiv').hide();
            $petAccordion.find('#pet-0 .hidelabels').hide();

            $petAccordion.accordion('destroy').accordion({ collapsible: true, active: false });
            $petAccordion.accordion("activate", ($tab.data("PetsList").length));
            $petAccordion.find("#pet-0 .deletePet").remove();
            $petAccordion.find("#pet-0 .updatePet span").html("Add pet to property");
            $petAccordion.find(".filesButton").files();
            $('#pet-0').addClass("petDiv");
            Utils.tabs.initTab($petAccordion.selector + " #pet-0");

        } else {
            jAlert("You cannot add more than one pet at a time. Please save the current new pet and then add another.");
        }
    },
    updatePet: function (_petID) {
        var $tab = Personal.getTab();
        var $petDiv = $tab.find("#pet-" + _petID);

        // *** EDITS
        var $MChip = $petDiv.find('input[name="HasMicrochip"]:checked');
        var tagId = $petDiv.find('input[name="TagID"]').val().length;
        if ($MChip.val() == true && tagId <= 1) {
            jAlert("PLEASE NOTE - You have indicated that this pet has a microchip however you haven't entered a Tag / Chip ID Number");
        }

        // *** EDITS
        
        if (Utils.validation.validateForm($petDiv.selector, "", false, false)) {
            //instantiate and populate entity
            var petEntity = new Entities.Pet();
            $petDiv.mapToObject(petEntity, "name");
            $petDiv.block();

            var response = function (_updateResult) {

                $petDiv.unblock();

                if (!_updateResult.EntityList[0].success) {
                    jAlert("There was an error updating the pet, you may have reached the allowed maximum pet limit!", "Error");
                }
      
                Pets.loadPets();
            };
                      
            PropertyService.WCFProperty.UpdatePet(petEntity, petEntity.PropertyID, response, Utils.responseFail);

            
        }
    },
    deletePet: function (_petID) {
        if (!isNaN(_petID)) {
            var $tab = Personal.getTab();
            var $petDiv = $tab.find("#pet-" + _petID);
            $petDiv.block();

            var deleteResponse = function (_deleteResult) {

                if (!_deleteResult.EntityList[0]) {
                    jAlert("There was a problem deleting the pet.");
                }

                $petDiv.unblock();
                Pets.loadPets();
            };

            PropertyService.WCFProperty.DeletePet(_petID,deleteResponse,Utils.responseFail);
        }
    },

    PetsList:[]
};

//main golf cart object
var GolfCarts = {//ignore jslint
    loadCarts: function () {
        var $tab = Personal.getTab();
        var $golfAccordion = $tab.find(".golfCartAccordion");

        var response = function (_result) {
            
            $tab.data("CartsList", _result.EntityList);

            if (_result.EntityList.length > 0) {
                $golfAccordion.show();

                Utils.bindDataToGrid($golfAccordion.selector, "#GolfCartAccordionTemplate", _result.EntityList);
                Utils.tabs.initTab($golfAccordion.selector);

                $golfAccordion.accordion('destroy').accordion({ collapsible: true, active: false });
            } else {
                $golfAccordion.hide();
            }
        };

        PropertyService.WCFProperty.GetPropertyGolfCarts(Properties.GetPropertyID(), response, Utils.responseFail);
    },
    addCart: function () {
        var $tab = Personal.getTab();
        var $golfAccordion = $tab.find(".golfCartAccordion");

        if ($golfAccordion.find("#cart-0").length === 0) {
            var cart = new Entities.GolfCart();

            cart.PropertyID = Properties.GetPropertyID();
            $golfAccordion.show();

            $("#GolfCartAccordionTemplate").tmpl(cart).appendTo($golfAccordion.selector);

            $($golfAccordion.selector + " h3 a").last().text("New Golf Cart");

            $golfAccordion.accordion('destroy').accordion({ collapsible: true, active: false });
            $golfAccordion.accordion("activate", ($tab.data("CartsList").length));
            $golfAccordion.find("#cart-0 .deleteGolfCart").remove();
            $golfAccordion.find("#cart-0 .updateGolfCart span").html("Add cart to property");
            Utils.tabs.initTab($golfAccordion.selector + " #cart-0");


        } else {
            jAlert("You cannot add more than one Golf Cart at a time. Please save the current new Golf Cart and then add another.");
        }
    },
    updateCart: function (_cartID) {
        var $tab = Personal.getTab();
        var $cartDiv = $tab.find("#cart-" + _cartID);

        if (Utils.validation.validateForm($cartDiv.selector, "", false, false)) {
            //instantiate and populate entity
            var cartEntity = new Entities.GolfCart();
            $cartDiv.mapToObject(cartEntity, "name");
            $cartDiv.block();

            var response = function (_updateResult) {

                $cartDiv.unblock();

                if (!_updateResult.EntityList[0].success) {
                    jAlert("There was an error updating the Golf Cart.", "Error");
                }

                GolfCarts.loadCarts();
            };

            PropertyService.WCFProperty.UpdateGolfCart(cartEntity, response, Utils.responseFail);
        }
    },
    deleteCart: function (_cartID) {
        if (!isNaN(_cartID)) {
            var $tab = Personal.getTab();
            var $cartDiv = $tab.find("#cart-" + _cartID);
            $cartDiv.block();

            var deleteResponse = function (_deleteResult) {

                if (!_deleteResult.EntityList[0]) {
                    jAlert("There was a problem deleting the Golf Cart.");
                }

                $cartDiv.unblock();
                GolfCarts.loadCarts();
            };

            PropertyService.WCFProperty.DeleteGolfCart(_cartID, deleteResponse, Utils.responseFail);
        }
    },
    CartsList: []
};

//main vehicles object
var Vehicles = {//ignore jslint
    loadVehicles: function () {
        var $tab = Personal.getTab();
        var $vehicleAccordion = $tab.find(".vehicleAccordion");

        var response = function (_result) {
            $tab.data("VehiclesList", _result.EntityList);
            if (_result.EntityList.length > 0) {
                
                $vehicleAccordion.show();

                Utils.bindDataToGrid($vehicleAccordion.selector, "#VehicleAccordionTemplate", _result.EntityList);
                Utils.tabs.initTab($vehicleAccordion.selector);

                $vehicleAccordion.accordion('destroy').accordion({ collapsible: true, active: false });
            } else {
                $vehicleAccordion.hide();
            }
        };

        PropertyService.WCFProperty.GetPropertyVehicles(Properties.GetPropertyID(), response, Utils.responseFail);
    },
    addVehicle: function () {
        var $tab = Personal.getTab();
        var $vehicleAccordion = $tab.find(".vehicleAccordion");

        if ($vehicleAccordion.find("#vehicle-0").length === 0) {
            var vehicle = new Entities.Vehicle();

            vehicle.PropertyID = Properties.GetPropertyID();
            $vehicleAccordion.show();

            $("#VehicleAccordionTemplate").tmpl(vehicle).appendTo($vehicleAccordion.selector);

            $($vehicleAccordion.selector + " h3 a").last().text("New Vehicle");

            $vehicleAccordion.accordion('destroy').accordion({ collapsible: true, active: false });
            $vehicleAccordion.accordion("activate", ($tab.data("VehiclesList").length));

            $vehicleAccordion.find("#vehicle-0 .deleteVehicle").remove();
            $vehicleAccordion.find("#vehicle-0 .updateVehicle span").html("Add vehicle to property");
            Utils.tabs.initTab($vehicleAccordion.selector + " #vehicle-0");


        } else {
            jAlert("You cannot add more than one Vehicle at a time. Please save the current new Vehicle and then add another.");
        }
    },
    updateVehicle: function (_vehicleID) {
        var $tab = Personal.getTab();
        var $vehicleDiv = $tab.find("#vehicle-" + _vehicleID);

        if (Utils.validation.validateForm($vehicleDiv.selector, "", false, false)) {
            //instantiate and populate entity
            var vehicleEntity = new Entities.Vehicle();
            $vehicleDiv.mapToObject(vehicleEntity, "name");
            $vehicleDiv.block();

            var response = function (_updateResult) {

                $vehicleDiv.unblock();

                if (!_updateResult.EntityList[0].success) {
                    jAlert("There was an error updating the Vehicle.", "Error");
                }

                Vehicles.loadVehicles();
            };

            PropertyService.WCFProperty.UpdateVehicle(vehicleEntity, response, Utils.responseFail);
        }
    },
    deleteVehicle: function (_vehicleID) {
        if (!isNaN(_vehicleID)) {
            var $tab = Personal.getTab();
            var $vehicleDiv = $tab.find("#vehicle-" + _vehicleID);
            $vehicleDiv.block();

            var deleteResponse = function (_deleteResult) {

                if (!_deleteResult.EntityList[0]) {
                    jAlert("There was a problem deleting the Vehicle.");
                }

                $vehicleDiv.unblock();
                Vehicles.loadVehicles();
            };

            PropertyService.WCFProperty.DeleteVehicle(_vehicleID, deleteResponse, Utils.responseFail);
        }
    },
    VehiclesList: []
};

//personal page events
var PersonalPageEvents = {
    load: function () {
        PersonalPageEvents.bindEvents();
    },
    bindEvents: function () {
        //personal tab click
        $('.bodywrapper').on("Honeycomb.tabLoad", ".personaltab", Personal.load);
        //button clicks for pets
        $(".bodywrapper").on("click.updatePet", ".updatePet", PersonalPageEvents.updatePet_click);
        $(".bodywrapper").on("click.deletePet", ".deletePet", PersonalPageEvents.deletePet_click);
        $(".bodywrapper").on("click.addPet", ".addPet", PersonalPageEvents.addPet_click);
        //button clicks for golf carts
        $(".bodywrapper").on("click.updateGolfCart", ".updateGolfCart", PersonalPageEvents.updateCart_click);
        $(".bodywrapper").on("click.deleteGolfCart", ".deleteGolfCart", PersonalPageEvents.deleteCart_click);
        $(".bodywrapper").on("click.addGolfCart", ".addGolfCart", PersonalPageEvents.addCart_click);
        //button clicks for vehicles
        $(".bodywrapper").on("click.updateVehicle", ".updateVehicle", PersonalPageEvents.updateVehicle_click);
        $(".bodywrapper").on("click.deleteVehicle", ".deleteVehicle", PersonalPageEvents.deleteVehicle_click);
        $(".bodywrapper").on("click.addVehicle", ".addVehicle", PersonalPageEvents.addVehicle_click);
 },
    addPet_click: function (e) {
        e.preventDefault();
        Pets.addPet();
    },
    updatePet_click: function (e) {
        e.preventDefault();
        var updateButton = $(this);
        Pets.updatePet(updateButton.attr("petid"));
    },
    deletePet_click: function (e) {
        e.preventDefault();
        var updateButton = $(this);

        jConfirm("Are you sure you want to delete this pet?", "Delete Pet", function (_confirm) {
            if (_confirm) {
                Pets.deletePet(updateButton.attr("petid"));
            }
        });
    },
    addCart_click: function (e) {
        e.preventDefault();
        GolfCarts.addCart();
    },
    updateCart_click: function (e) {
        e.preventDefault();
        var updateButton = $(this);
        GolfCarts.updateCart(updateButton.attr("cartid"));
    },
    deleteCart_click: function (e) {
        e.preventDefault();
        var updateButton = $(this);

        jConfirm("Are you sure you want to delete this Golf Cart?", "Delete Golf Cart", function (_confirm) {
            if (_confirm) {
                GolfCarts.deleteCart(updateButton.attr("cartid"));
            }
        });
    },
    addVehicle_click: function (e) {
        e.preventDefault();
        Vehicles.addVehicle();
    },

    updateVehicle_click: function (e) {
        e.preventDefault();
        var updateButton = $(this);
        Vehicles.updateVehicle(updateButton.attr("vehicleid"));
    },
    deleteVehicle_click: function (e) {
        e.preventDefault();
        var updateButton = $(this);

        jConfirm("Are you sure you want to delete this Vehicle?", "Delete Vehicle", function (_confirm) {
            if (_confirm) {
                Vehicles.deleteVehicle(updateButton.attr("vehicleid"));
            }
        });
    }
};

//main window onload
$(function () {
    PersonalPageEvents.load();
});