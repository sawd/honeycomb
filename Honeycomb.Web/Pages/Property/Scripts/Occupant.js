﻿/*global PropertyService,Properties,Entities,jAlert,jConfirm,Utils,jQuery,Lease*/
//script resource file for occupants object
var Occupants = {
	load: function (_event, _overide) {

		var $tab = Utils.tabs.getCurrentTab().find(".occupanttab");
		var $propertyID = Properties.GetPropertyID();

		$tab.data("OccupantsList", []);

		if (_overide === undefined) {
			_overide = false;
		}

		if (!$tab.data("loaded") || _overide) {

			var response = function (_result) {
				$tab.unblock();

				if (_result.EntityList.length > 0) {

					$($tab.selector + " #OccupantContainer").show();

					$tab.data("OccupantsList", _result.EntityList);
					Utils.bindDataToGrid($tab.selector + " #OccupantContainer", "#PropertyOccupantTemplate", _result.EntityList);
					Utils.tabs.initTab($tab.selector + " #OccupantContainer");
					$tab.find(".restoreoccupant").parent().remove();
					$tab.find(".accordion").accordion('destroy').accordion({ collapsible: true, active: false });

					$.when(Occupants.getPORelationships(), Occupants.getKORelationships()).then(function () {

						//loop through each occupant and populate the select drop downs
						for (var i = 0; i < _result.EntityList.length; i++) {
							//populate the principle owner rel select
							Utils.bindDataToGrid($tab.selector + " #occupantno-" + _result.EntityList[i].ID + " select[name=PrincipalOwnerRelID]", "#ddlTemplate", Occupants.PORelationships);

						


						    //set the selected value
							$tab.find("#occupantno-" + _result.EntityList[i].ID + " select[name=PrincipalOwnerRelID]").val(_result.EntityList[i].PrincipalOwnerRelID);
							//populate the key occupant rel select
							Utils.bindDataToGrid($tab.selector + " #occupantno-" + _result.EntityList[i].ID + " select[name=KeyOccupantRelID]", "#ddlTemplate", Occupants.KORelatrionships);
							//set the selected value
							$tab.find("#occupantno-" + _result.EntityList[i].ID + " select[name=KeyOccupantRelID]").val(_result.EntityList[i].KeyOccupantRelID);

							$tab.find("#occupantno-" + _result.EntityList[i].ID + " input[name=IDOrPassportNo]").tagHolder();

						    //init file uploader
							$tab.find("#occupantno-" + _result.EntityList[i].ID + " .fubExample").fineUploader({
							    uploaderType: 'basic',
							    multiple: false,
							    autoUpload: true,
							    button: $tab.find("#occupantno-" + _result.EntityList[i].ID + " .fubUploadButton"),
							    request: {
							        endpoint: "/Pages/UploadFile.ashx"
							    }
							}).on('complete', function (event, id, filename, responseJSON) {//ignore jslint
							    var $this = $(this);
							    $tab.find("#occupantno-" + $this.attr("occupantid") + " .photoDiv").css({ "background-image": "url(/Pages/DownloadFile.ashx?FileRef=" + responseJSON.fileid + ")" }).addClass("photoDivWithPhoto");
							    $tab.find("#occupantno-" + $this.attr("occupantid") + " input[name=OccupantPhoto]").val(responseJSON.fileid);
							})//ignore jslint
                            .on("submit", function (event, id, filename) {//ignore jslint
                                $tab.find("#staffno-" + $(this).attr("staffid") + " .photoDiv").css("background-image", "url(/Scripts/plugins/fileupload/loading.gif)").removeClass("photoDivWithPhoto");
                            })//ignore jslint
                            .on("error", function (event, id, filename, reason) {//ignore jslint

                            });//ignore jslint
						}

						//update the controls after the population is complete
						jQuery.uniform.update();

					});
				} else {
					//$.jGrowl("No records to display");
					$($tab.selector + " #OccupantContainer").hide();
				}

			};

			$tab.block();

			PropertyService.WCFProperty.GetOccupants($propertyID, response, Utils.responseFail);

			$tab.data("loaded", true);
		}

	},
	getPORelationships: function () {
		var dfd = new $.Deferred();

		var response = function (_result) {
			Occupants.PORelationships = _result.EntityList;
			dfd.resolve();
		};

		PropertyService.WCFProperty.GetPrincipalRelationships(response, function (e) { dfd.reject(); Utils.responseFail(e); });
		return dfd.promise();
	},
	getKORelationships: function () {
		var dfd = new $.Deferred();

		var response = function (_result) {
			Occupants.KORelatrionships = _result.EntityList;
			dfd.resolve();
		};

		PropertyService.WCFProperty.GetKeyOccupantRelationships(response, function (e) { dfd.reject(); Utils.responseFail(e); });
		return dfd.promise();
	},
	updateOccupant: function (_event) {

		var $tab = Utils.tabs.getCurrentTab().find(".occupanttab");
		var $propertyID = Properties.GetPropertyID();
		var $occupantID = 0;

		if ($(_event.target).is("span")) {
			$occupantID = $(_event.target).parent().attr("occupantid");
		} else {
			$occupantID = $(_event.target).attr("occupantid");
		}

		var $OccupantDiv = $($tab.selector + " #occupantno-" + $occupantID);
		var Occupant = new Entities.Occupant();

		if (Utils.validation.validateForm($OccupantDiv.selector, "", false, false)) {

			$OccupantDiv.mapToObject(Occupant, "name");

			$OccupantDiv.block();

			var response = function (_result) {
				$OccupantDiv.unblock();

				if (_result.EntityList[0] === "") {
					// check if the added occupant was a tenant
					if (Occupant.IsTenant) {
						//activate the lease tab
						$("#" + $propertyID + "Tabs").tabs("enable", 4);

						//there is a lease, get the details
						Lease.loadTab(null, true);
						$("#" + $propertyID).data("hasLease", true);
					}
					//reload occupants to reflect latest changes
					Occupants.load(null, true);
				} else {
				    if (_result.EntityList[0].indexOf("golf membership") > -1) {
				        jAlert(_result.EntityList[0], "Warning");
				    } else {
				        jAlert(_result.EntityList[0], "Error: Save Failed");
				    }
				}
			};

			//check if we must send the occupant intranet details email
			if ($OccupantDiv.find("input[name=SendIntranetDetails]").is(":checked")) {
				PropertyService.WCFProperty.UpdateOccupant(Occupant, $propertyID, true, response, Utils.responseFail);
			} else {
				PropertyService.WCFProperty.UpdateOccupant(Occupant, $propertyID, false, response, Utils.responseFail);
			}

		}
	},
	addOccupant: function (_event) {

		var $tab = Utils.tabs.getCurrentTab().find(".occupanttab");

		if ((parseInt($tab.find("#occupantno-0").length, 10) === 0)) {
			var occupant = new Entities.Occupant();

			$($tab.selector + " #OccupantContainer").show();

			$("#PropertyOccupantTemplate").tmpl(occupant).appendTo($tab.selector + " #OccupantContainer");

			$($tab.selector + " #OccupantContainer h3 a").last().text("New Occupant");

			$tab.block();

			$.when(Occupants.getPORelationships(), Occupants.getKORelationships()).then(function () {

			    //init file uploader
			    $tab.find("#occupantno-0 .fubExample").fineUploader({
			        uploaderType: 'basic',
			        multiple: false,
			        autoUpload: true,
			        button: $tab.find("#occupantno-0 .fubUploadButton"),
			        request: {
			            endpoint: "/Pages/UploadFile.ashx"
			        }
			    }).on('complete', function (event, id, filename, responseJSON) {//ignore jslint
			        var $this = $(this);
			        $tab.find("#occupantno-0 .photoDiv").css({ "background-image": "url(/Pages/DownloadFile.ashx?FileRef=" + responseJSON.fileid + ")" }).addClass("photoDivWithPhoto");
			        $tab.find("#occupantno-0 input[name=OccupantPhoto]").val(responseJSON.fileid);
			    })//ignore jslint
                .on("submit", function (event, id, filename) {//ignore jslint
                    $tab.find("#occupantno-0 .photoDiv").css("background-image", "url(/Scripts/plugins/fileupload/loading.gif)").removeClass("photoDivWithPhoto");
                })//ignore jslint
                .on("error", function (event, id, filename, reason) {//ignore jslint

                });//ignore jslint
			    //end file upload init


				//populate the principle owner rel select
				Utils.bindDataToGrid($tab.selector + " #occupantno-0 select[name=PrincipalOwnerRelID]", "#ddlTemplate", Occupants.PORelationships);
				//populate the key occupant rel select
				Utils.bindDataToGrid($tab.selector + " #occupantno-0 select[name=KeyOccupantRelID]", "#ddlTemplate", Occupants.KORelatrionships);

				$tab.find("#occupantno-0 input[name=IDOrPassportNo]").tagHolder();
				$tab.find("#occupantno-0 input[name=IDOrPassportNo]").tagHolder("disable");

				$tab.unblock();
				//update the custom field types
				$.uniform.update();
			});

			$tab.find(".accordion").accordion('destroy').accordion({ collapsible: true, active: false });
			$tab.find(".accordion").accordion("activate", ($tab.data("OccupantsList").length));
			$tab.find("#occupantno-0 .archiveoccupant").parent().remove();
			$tab.find("#occupantno-0 .restoreoccupant").parent().remove();
			$tab.find("#occupantno-0 .updateoccupant span").html("Add Occupant to property");
			Utils.tabs.initTab($tab.selector + " #occupantno-0");

		} else {
			jAlert("You can only add one new occupant at a time. Please click the 'Add Occupant' button to save the new occupant.", "Cannot add new occupant");
		}
	},
	archiveOccupant: function (_event) {

		var $tab = Utils.tabs.getCurrentTab().find(".occupanttab");
		var $propertyID = Properties.GetPropertyID();
		var $occupantID = 0;
		var isPrinciple = "no";

		if ($(_event.target).is("span")) {
			$occupantID = $(_event.target).parent().attr("occupantid");
			isPrinciple = $(_event.target).parent().attr("isprinciple");
		} else {
			$occupantID = $(_event.target).attr("occupantid");
			isPrinciple = $(_event.target).attr("isprinciple");
		}

		var $OccupantDiv = $($tab.selector + " #occupantno-" + $occupantID);

		jConfirm("Are you sure you wish to archive this occupant?", "Confirm Archive", function (_confirm) {
			if (_confirm) {

				if (isPrinciple === "no") {

					var response = function (_result) {
						$OccupantDiv.unblock();

						if (_result.EntityList[0].success) {
							Occupants.load(null, true);
						} else {
							jAlert(_result.EntityList[0].message,"Archive Error");
						}
					};

					$OccupantDiv.block();
					PropertyService.WCFProperty.ArchiveOccupant($occupantID, $propertyID, response, Utils.responseFail);
				} else {
					jAlert("You cannot archive the princple occupant", "Warning");
				}

			}
		});
	},
	restoreOccupant: function (_event) {

		var $tab = Utils.tabs.getCurrentTab().find(".occupanttab");
		var $propertyID = Properties.GetPropertyID();
		var $occupantID = 0;
		var isPrinciple = "no";

		if ($(_event.target).is("span")) {
			$occupantID = $(_event.target).parent().attr("occupantid");
			isPrinciple = $(_event.target).parent().attr("isprinciple");
		} else {
			$occupantID = $(_event.target).attr("occupantid");
			isPrinciple = $(_event.target).attr("isprinciple");
		}

		var $OccupantDiv = $($tab.selector + " #occupantno-" + $occupantID);

		jConfirm("Are you sure you wish to restore this occupant? <br/>Please be aware that if this occupant was a <b>principle occupant</b>, they will not be marked as such after being restored.", "Confirm Restore", function (_confirm) {
			if (_confirm) {

				var response = function (_result) {
					$OccupantDiv.unblock();

					if (_result.Count > 0) {
						//success
						Occupants.loadOccupantHistory(null);
					} else {
						//fail
						$.jGrowl("There was an error when updating the Occupant");
					}

				};

				$OccupantDiv.block();
				PropertyService.WCFProperty.RestoreOccupant($occupantID, $propertyID, response, Utils.responseFail);
			}
		});
	},
	loadOccupantHistory: function (_event) {
		var $tab = Utils.tabs.getCurrentTab().find(".occupanttab");
		var $propertyID = Properties.GetPropertyID();

		var response = function (_result) {
			$tab.unblock();

			$tab.data("OccupantsList", _result.EntityList);

			Utils.bindDataToGrid($tab.selector + " #OccupantContainer", "#PropertyOccupantTemplate", _result.EntityList);
			Utils.tabs.initTab($tab.selector + " #OccupantContainer");

			$tab.find(".accordion").accordion('destroy').accordion({ collapsible: true, active: false });
			$tab.find(".updateoccupant").parent().remove();
			$tab.find(".archiveoccupant").parent().remove();

			$.when(Occupants.getPORelationships(), Occupants.getKORelationships()).then(function () {

				//loop through each occupant and populate the select drop downs
				for (var i = 0; i < _result.EntityList.length; i++) {
					//populate the principle owner rel select
					Utils.bindDataToGrid($tab.selector + " #occupantno-" + _result.EntityList[i].ID + " select[name=PrincipalOwnerRelID]", "#ddlTemplate", Occupants.PORelationships);
					//set the selected value
					$tab.find("#occupantno-" + _result.EntityList[i].ID + " select[name=PrincipalOwnerRelID]").val(_result.EntityList[i].PrincipalOwnerRelID);
					//populate the key occupant rel select
					Utils.bindDataToGrid($tab.selector + " #occupantno-" + _result.EntityList[i].ID + " select[name=KeyOccupantRelID]", "#ddlTemplate", Occupants.KORelatrionships);
					//set the selected value
					$tab.find("#occupantno-" + _result.EntityList[i].ID + " select[name=KeyOccupantRelID]").val(_result.EntityList[i].KeyOccupantRelID);
				}

				//update the controls after the population is complete
				jQuery.uniform.update();

			});

		};

		$tab.block();
		PropertyService.WCFProperty.GetArchivedOccupants($propertyID, response, Utils.responseFail);
	},
	GetOccupantByID: function (event) {
	    var $tab = Utils.tabs.getCurrentTab();
	    var occDiv = $($tab.selector + " #occupantno-" + $(event.target).attr("ID"));
	    var occGOVID = $(event.target).val();

	    var response = function (_result) {
	        occDiv.unblock();

	        if (_result.EntityList[0] !== null) {
	            //bind data to the form
	            //Utils.bindDataToGrid(staffDiv.selector, "#PropertyStaffTemplate", _result.EntityList[0]);
	            occDiv.mapObjectTo(_result.EntityList[0], "name", true);
	            $.uniform.update();
	        }

	    };

	    staffDiv.block();

	    PropertyService.WCFProperty.GetOccupantByID(occGOVID, response, Utils.responseFail);
	},
	OccupantsList: [],
	PORelationships: [],
	KORelatrionships: []
};

var OccupantPageEvents = {
	load: function () {
		OccupantPageEvents.bindEvents();
	},
	bindEvents: function () {
		//occupants tab load
		$('.bodywrapper').on("Honeycomb.tabLoad", ".occupanttab", Occupants.load);
		//button clicks
		$(".bodywrapper").on("click.updateoccupant", ".updateoccupant", Occupants.updateOccupant);
		$(".bodywrapper").on("click.archiveoccupant", ".archiveoccupant", Occupants.archiveOccupant);
		$(".bodywrapper").on("click.restoreoccupant", ".restoreoccupant", Occupants.restoreOccupant);
		$(".bodywrapper").on("click.addoccupant", "#addoccupant", Occupants.addOccupant);
		$(".bodywrapper").on("change.viewmode", "#viewmode", OccupantPageEvents.viewmode_change);

	},
	viewmode_change: function (_event) {
		var $element = $(_event.target);
		if ($element.val() === "current") {
			Occupants.load(null, true);
		} else {
			Occupants.loadOccupantHistory();
		}
	}
};

$(function () {
	OccupantPageEvents.load();
});