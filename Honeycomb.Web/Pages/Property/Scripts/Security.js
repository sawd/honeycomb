﻿/*global Properties,PropertyService,Utils*/
var PropertySecurity = {
    tabload: function (_event) {
        var $tab = Utils.tabs.getCurrentTab().find(".securitytab");

        if (!$tab.data("loaded")) {
            $tab.data("loaded", true);

            $tab.block();

            var response = function (_result) {

                $tab.unblock();

                if (_result.Count > 0) {
                    $tab.find(".accordion").show();
                    Utils.bindDataToGrid($tab.selector + " #IncidentHistoryContainer", "#SecurityHistoryTmpl", _result.EntityList);
                    $tab.find(".accordion").accordion('destroy').accordion({ collapsible: true});
                    Utils.tabs.initTab($tab.selector);
                } else {
                    //hide the accordion because there are no values
                    $tab.find(".accordion").hide();
                }
            };

            PropertyService.WCFProperty.GetPropertyIncidentHistory(Properties.GetPropertyID(),response,Utils.responseFail);
        }
    },
    viewIncidentModal: function (_event) {
        var incidentID = $(_event.target).parent().attr("incidentid");
        var $tab = Utils.tabs.getCurrentTab();

        var response = function (_result) {

            $tab.unblock();

            if (_result.Count > 0) {

                Utils.bindDataToGrid("#viewhistory", "#ViewIncidentTmpl", _result.EntityList[0]);
                Utils.tabs.initTab("#viewhistory");
                $.uniform.update();

                $("#viewhistory").dialog({
                    resizable: true,
                    modal: true,
                    width: "900px",
                    title: "View incident details <span style='font-size:12px;'>(Overview. For full details see Security - Incidents)</span>",
                    buttons: {
                        Close: function () {
                            $(this).dialog("close");
                        }
                    }
                });

            } else {
                $.jGrowl("No Record to display");
            }
        };

        $tab.block();
        PropertyService.WCFProperty.GetIncidentDetails(incidentID,response,Utils.responseFail);
    }
};

var PropertySecurityPageEvents = {
    load: function () {
        PropertySecurityPageEvents.bindEvents();
    },
    bindEvents: function () {
        //bind the tab select event
        $('.bodywrapper').on('Honeycomb.tabLoad', '.securitytab', PropertySecurity.tabload);
        $(".bodywrapper").on('click.viewincidenthistory', '.viewincidenthistory', PropertySecurity.viewIncidentModal);
    }
};

//master page load for property security
$(function () {
    PropertySecurityPageEvents.load();
});