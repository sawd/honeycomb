﻿/*global Occupants, Utils, PropertyService, jQuery, jAlert, Entities,jConfirm, ManagementAssociation, Ownership, Lease, Globals*/
var Properties = {
	getResidentialProperties: function (_firstLoad) {

		var propertyResponse = function (result) {
			Properties.propertyResidentialPaginate.TableID = "#PropertyTable";
			Properties.propertyResidentialPaginate.PaginationID = "#propertyPagination";
			Properties.propertyResidentialPaginate.TotalCount = result.Count;

			Utils.bindDataToGrid("#PropertyTemplateContainer", "#PropertyTemplate", result.EntityList);
			Properties.propertyResidentialPaginate.bindPagination();

			if ($(".loader").is(":visible"))
				$(".loader").slideToggle();//ignore jslint

			if (result.EntityList.length === 0) {
				$.jGrowl("close");
				$.jGrowl("There were no items found", { header: "<b>No records</b>" });
			}

		};

		if (!$(".loader").is(":visible") && !_firstLoad) {
			$(".loader").slideToggle();
		}

		PropertyService.WCFProperty.GetSingleResidentProperties(Properties.propertyResidentialPaginate, $("#filterSR").val(), $("#occupantFilterSR").val(), propertyResponse, Utils.responseFail);
	},
	getSchemeProperties: function (_firstLoad) {

		var propertyResponse = function (result) {
			Properties.propertySchemePaginate.TableID = "#SchemeTable";
			Properties.propertySchemePaginate.PaginationID = "#schemePagination";
			Properties.propertySchemePaginate.TotalCount = result.Count;

			Utils.bindDataToGrid("#SchemeContainer", "#PropertyTemplate", result.EntityList);
			Properties.propertySchemePaginate.bindPagination();

			if ($(".loader").is(":visible")) {
				$(".loader").slideToggle();
			}

			if (result.EntityList.length === 0) {
				$.jGrowl("close");
				$.jGrowl("There were no items found", { header: "<b>No records</b>" });
			}

		};

		if (!$(".loader").is(":visible") && !_firstLoad) {
			$(".loader").slideToggle();
		}

		PropertyService.WCFProperty.GetSectionalSchemeProperties(Properties.propertySchemePaginate, $("#filterScheme").val(), $("#filterOccupant").val(), propertyResponse, Utils.responseFail);
	},
	getSectionalProperties: function (_firstLoad) {

		var selMulti = $.map($("#propertyGroupSelect option:selected"), function (el, i) {
			return $(el).val();
		});

		var complexIDList = selMulti.join(", ");

		var propertyResponse = function (result) {
			Properties.propertySectionalPaginate.TableID = "#SectionalTable";
			Properties.propertySectionalPaginate.PaginationID = "#sectionalPagination";
			Properties.propertySectionalPaginate.TotalCount = result.Count;

			Utils.bindDataToGrid("#SectionalTemplateContainer", "#SectionalTemplate", result.EntityList);
			Properties.propertySectionalPaginate.bindPagination();

			if ($(".loader").is(":visible")) {
				$(".loader").slideToggle();
			}

			if (result.EntityList.length === 0) {
				$.jGrowl("close");
				$.jGrowl("There were no items found", { header: "<b>No records</b>" });
			}

		};

		if (!$(".loader").is(":visible") && !_firstLoad) {
			$(".loader").slideToggle();
		}

		PropertyService.WCFProperty.GetSectionTitleProperties(Properties.propertySectionalPaginate, $("#filterST").val(), $("#occupantFilterST").val(), complexIDList, propertyResponse, Utils.responseFail);

	},
	getPropertyTypes: function () {
		var response = function (result) {
			Utils.bindDataToGrid("#propertyType", "#ddlTemplate", result.EntityList);
			jQuery.uniform.update();
		};
		PropertyService.WCFProperty.GetPropertyTypes(response, Utils.responseFail);
	},
	getPropertyGroups: function () {
		var response = function (result) {
			Utils.bindDataToGrid("#propertyGroup", "#ddlTemplate", result.EntityList);
			Utils.bindDataToGrid("#propertyGroupSelect", "#ddlTemplate", result.EntityList);
			//update multi select
			$("#propertyGroupSelect").trigger("liszt:updated");
			//update select box
			jQuery.uniform.update();
		};
		PropertyService.WCFProperty.GetPropertyGroups(response, Utils.responseFail);
	},
	getOwners: function () {
		var ownerResponse = function (result) {
			Properties.currentOwners = result.EntityList;
		};
		PropertyService.WCFProperty.GetOwners(ownerResponse, Utils.responseFail);
	},
	commitProperties: function (event) {
		var propertyID = $(event.target).attr("dataid");

		var tabID;
		if (parseInt(propertyID, 10) !== 0) {
			tabID = '#' + propertyID;
		} else {
			tabID = '#add';
		}

		var commitResponse = function (result) {
			$("button[name=topSave]").attr("disabled", false);

			if (result.EntityList[0][0] > 0) {
				//saved successfully, now get the owner's id and set it in the owner form
				if (result.EntityList[0][1] > 0) {
					var currentOwnerTab = Utils.tabs.getCurrentTab().find(".ownershiptab");
					currentOwnerTab.find("input[name=ID]").val(result.EntityList[0][1]);
					currentOwnerTab.find("input[name=PropertyID]").val(result.EntityList[0][0]);
				}
				//move the focus to the all properties tab
				if (tabID === "#add") {
					Utils.tabs.selectTab("#grid");
				}

				jAlert("Property Saved", "Saved");
				$(tabID).unblock();
			}
			else {
				jAlert("There was an error while attempting a save,this item was not saved", "Error");
				$(tabID).unblock();
			}

		};
		var propertyEntity = new Entities.Property();
		var ownerEntity = new Entities.Ownership();
		var leaseEntity = new Entities.Lease();
		var $propertyTab = $(tabID);

		//validate the 3 tabs that are required for properties, description, ownership and lease
		var descTabValid = Utils.validation.validateForm(tabID + " .descriptiontab", "Property tab missing required fields", false, true);
		var ownerTabValid = Utils.validation.validateForm(tabID + " .ownershiptab", "Ownership tab missing required fields", false, true);
		var leaseTabValid;

		if ($propertyTab.data("hasLease")) {
			leaseTabValid = Utils.validation.validateForm(tabID + " .leasetab", "Lease tab missing required fields", false, true);
		} else {
			leaseTabValid = true;
		}

		var securityTabValid = Utils.validation.validateForm(tabID + " .securitytab", "Security tab missing required fields", false, true);

		if (descTabValid && ownerTabValid && leaseTabValid && securityTabValid) {

			$(tabID + " .descriptiontab").mapToObject(propertyEntity, "name");
			$(tabID + " .ownershiptab").mapToObject(ownerEntity, "name");

			//if the property can have a lease populate it, otherwise send up and empty entity
			if ($propertyTab.data("hasLease")) {
				$(tabID + " .leasetab").mapToObject(leaseEntity, "name");
				leaseEntity.PropertyID = Properties.GetPropertyID();
			}

			//populate specific property related data from the security tab
			propertyEntity.AlarmCompany = $(tabID + " .securitytab input[name=AlarmCompany]").val();
			propertyEntity.TransmitterNo = $(tabID + " .securitytab input[name=TransmitterNo]").val();

			/*owner.getPhaseStatuses = $(tabID + ".descriptiontab input[name=Phase]").val(); /* *** EDIT */

			$("button[name=topSave]").attr("disabled",true);

			$(tabID).block();
			PropertyService.WCFProperty.UpdateProperties(propertyEntity, ownerEntity, leaseEntity, commitResponse, function (result) {
				$(tabID).unblock();
				Utils.responseFail(result);
			});
		}


	},
	deleteProperty: function (dataID, rowElement) {
		jConfirm("Are you sure you want to delete this property", "", function (_result) {
			if (_result) {

				var deleteReponse = function (_result) {

					if (_result.EntityList) {
						if (_result.EntityList[0].success) {

							$.jGrowl("Property Deleted");
							//mark the row as queued for deletion
							Utils.tables.markRowAsDeleted(rowElement);

							if ($.contains($("#tabs-1")[0], rowElement[0])) {
								Properties.getResidentialProperties();
							} else if ($.contains($("#tabs-2")[0], rowElement[0])) {
								Properties.getSectionalProperties();
							} else if ($.contains($("#tabs-3")[0], rowElement[0])) {
								Properties.getSchemeProperties();
							}

						} else {
							jAlert(_result.EntityList[0].message, "Failed to delete property");
						}
					}

				};

				PropertyService.WCFProperty.DeleteProperty(dataID, deleteReponse, Utils.responseFail);
			}
		});
	},
	mergeOwnerProperty: function () {

	},
	getProperty: function (_propertyID) {
		var onSuccess = function (_result) {
			if (_result.EntityList[0]) {
				var property = _result.EntityList[0];
				var owner = ((property.OwnerName) ? " : " + property.OwnerName : "").split(",")[0];

				var header = property.ErfNumber + owner;
				Utils.tabs.loadTab(header, _propertyID, function () {
					try {
						var template = $($("#PropertyFormTemplate").html().replace(/{ID}/gi, _propertyID));//ignore jslint
						template.find('h1.pagetitle').html(header);

						if (property.PropertyTypeID === 10000 || property.PropertyTypeID === 10002) {
							template.find(".SectionalTitle").remove();
						}
						var $tab = $('#' + _propertyID);
						$tab.append(template);
						Utils.tabs.initTab('#' + _propertyID);
						$tab.block({ message: null });

						$('#' + _propertyID + 'Tabs').tabs({
							select: function (event, ui) {
								$(ui.panel).trigger("Honeycomb.tabLoad");
							}
						});

						$.when(ManagementAssociation.getAssociations('#' + _propertyID + 'Tabs'), Properties.getErfScheme(property.ERFID), Ownership.getOwnerDetail(_propertyID), Properties.activateLeaseTab(_propertyID), Ownership.getPhaseStatuses()).then(function () { /* *** EDIT */
							$tab.find(".descriptiontab").mapObjectTo(property, "name", true);

							//map the object to the security tab as well
							$tab.find(".securitytab").mapObjectTo(property, "name", true);

							$.uniform.update();
							$tab.unblock();
						}, Utils.responseFail);

					}
					catch (err) {
						$.jGrowl(err.message, { header: "<b>No records</b>" });
					}
				}, true, true);
			}
		};

		PropertyService.WCFProperty.GetProperty(_propertyID, onSuccess, Utils.responseFail);


	},
	getErfScheme: function (_erfNo) {
		var $tab = Utils.tabs.getCurrentTab();
		var dfd = new $.Deferred();

		var response = function (result) {
			Properties.ErfSchemes = result.EntityList;
			Utils.bindDataToGrid($tab.find("select[name='SchemeID']").selector, "#schemeDDL", result.EntityList);
			jQuery.uniform.update();
			dfd.resolve();
		};

		PropertyService.WCFProperty.GetErfScheme(_erfNo, response, function (e) { dfd.reject(); Utils.responseFail(e); });
		return dfd.promise();
	},
	addModal: function (_event) {
		if ($(_event.target).attr("id") == "add") {
			$('#add').html("");
			$('#Erven').val(0);
			$('#addForm .addButtons').hide();
			$("#Erven").trigger('liszt:updated');
			$("#addForm").dialog({
				resizable: true,
				modal: true,
				width: "450px",
				title: "Add a new Property",
				close: Properties.addModalClose
			});
		}
	},
	addModalClose: function (event, ui) {
		if (event.currentTarget && !$(event.currentTarget).is("button")) {
			Utils.tabs.selectTab("#grid");
		}
	},
	addFormSelection: function (event)
	{
		var typeID = event.target.value;
		var dataID = $(event.target).find(":selected").attr("dataid");
		$("#addForm button").attr("disabled", "disabled");
		var isShown = true;
		switch (typeID) {
			case "10001": //Common Property
				$('#addSingleResidenceBtn').removeAttr("disabled");
				break;
			case "10002": //Mixed
				$('#addForm button').removeAttr("disabled");
				break;
			case "10003": //Sectional Title
				$('#addSectionalSchemeBtn').removeAttr("disabled");
				$('#addSectionalSection').removeAttr("disabled");
				break;
			case "10004": //Single Residential
				$('#addSingleResidenceBtn').removeAttr("disabled");
				break;
			case "0":
				isShown = false;
				break;
		}
		$('#addForm .addButtons').toggle(isShown);
	},
	addForm: function (erfID, isSectional, PropertyType) {
		$('#addForm').dialog("close");
		var owner = "";
		var $tab = $('#add');
		Properties.propertyHasLease = false;

		$tab.block({ message: null });
		var onSuccess = function (_result) {
			var property;

			if (_result.EntityList.length > 0) {
				Properties.ErfSchemes = _result.EntityList;
			}

			if (_result.EntityList[0]) {
				property = Utils.clone(_result.EntityList[0]);
				property.SchemeID = (isSectional ? property.ID : "");

			} else {
				property = new Entities.Property();
			}

			property.ID = 0;
			property.ERFID = erfID;
			property.PropertyTypeID = PropertyType;

			var owner = ((property.OwnerName) ? " : " + property.OwnerName : "").split(",")[0];

			var header = property.ErfNumber;
			try {
				var template = $($("#PropertyFormTemplate").html().replace(/{ID}/gi, property.ID));//ignore jslint
				template.find('h1.pagetitle').html(header);

				if (!isSectional) {
					template.find(".SectionalTitle").remove();
				}

				$tab.append(template);
				Utils.tabs.initTab('#add');

				$('#0Tabs').tabs({
					select: function (event, ui) {
						$(ui.panel).trigger("Honeycomb.tabLoad");
					}
				});

				//disable the occupants,personal, lease and staff tabs because that functionality is not available before the property has been saved
				$("#0Tabs").tabs("option", "disabled", [2, 3, 4, 6]);

				$.when(ManagementAssociation.getAssociations('#add'), Ownership.getPhaseStatuses(), Ownership.getOwnerDetail(0)).then(function () {
					$tab.find(".descriptiontab").mapObjectTo(property, "name", true);

					if (isSectional) {
						Utils.bindDataToGrid($tab.find(".descriptiontab select[name=SchemeID]").selector, "#schemeDDL", _result.EntityList);
					}

					$.uniform.update();
					$tab.unblock();
				}, Utils.responseFail);

			}
			catch (err) {
				$.jGrowl(err.message, { header: "<b>No records</b>" });
			}

		};

		PropertyService.WCFProperty.GetErfScheme(erfID, onSuccess, Utils.responseFail);

	},
	activateLeaseTab: function (_propertyID) {
		var dfd = new $.Deferred();

		var response = function (_result) {

			if (_result.EntityList[0] !== true) {
				//disable the lease tab for the property
				$("#" + _propertyID + "Tabs").tabs("option", "disabled", [4]);
				$("#" + _propertyID).data("hasLease", false);
			} else {
				//there is a lease, get the details
				Lease.loadTab(null, true);
				$("#" + _propertyID).data("hasLease", true);
			}

			dfd.resolve();
		};

		PropertyService.WCFProperty.PropertyHasTenants(_propertyID, response, function (e) { dfd.reject(); Utils.responseFail(e); });
		return dfd.promise();
	},
	currentOwners: null,
	currentProperties: null,
	firstLoad: true,
	ownerProperty: [],
	propertyResidentialPaginate: null,
	propertySectionalPaginate: null,
	propertySchemePaginate: null,
	ShemeName_change: function (_event) {
		var $tab = Utils.tabs.getCurrentTab();
		var erfScheme = Properties.ErfSchemes[$(_event.target).find(":selected").index()];

		if (erfScheme.SchemeNumber !== "" && erfScheme.SchemeNumber !== null) {
			$tab.find(".descriptiontab .field[name='SchemeNumber']").text(erfScheme.SchemeNumber);
		} else {
			$tab.find(".descriptiontab .field[name='SchemeNumber']").html('&nbsp;');
		}
	},
	ErfSchemes: [],
	GetPropertyID: function () {
		var propertyID = Utils.tabs.getCurrentTab().attr("id");

		if (isNaN(propertyID)) {
			propertyID = 0;
		}

		return propertyID;
	}

};

var ManagementAssociation = {//ignore jslint
	getAssociations: function (_container) {
		_container = (_container || "");
		var dfd = new $.Deferred();
		var response = function (result) {
			Utils.bindDataToGrid(_container + " select[name=ManagementAssociationID]", "#ddlTemplate", result.EntityList);
			jQuery.uniform.update();
			dfd.resolve();
		};

		PropertyService.WCFProperty.GetManagementAssociations(response, function (e) { dfd.reject(); Utils.responseFail(e); });
		return dfd.promise();
	}
};

var Erven = {
	ervenData: {},
	getErven: function (_container) {
		_container = (_container || "");
		var onSuccess = function (_result) {
			Erven.ervenData = _result.EntityList;
			Utils.bindDataToGrid(_container + " select[name=ErfID]", "#erfDDL", Erven.ervenData);
			$(_container + " select[name=ErfID]").prepend("<option value='0' dataid='0'>-- Erf --</option>");
			$(_container + " select[name=ErfID]").trigger('liszt:updated');
			$('.erfloader').toggle(false);
		};
		$('.erfloader').toggle(true);
		PropertyService.WCFProperty.GetErven(onSuccess, Utils.responseFail);
	}
};

var Ownership = {//ignore jslint
	load: function (_event) {

		var $tab = $(_event.target);

		if (!$tab.data("loaded")) {

			$tab.data("loaded", true);
			$tab.block();

			var propertyID = Utils.tabs.getCurrentTab().attr("id");

			if (isNaN(propertyID)) {
				propertyID = 0;
			}

			$.when(Ownership.getOwnershipHistory(propertyID, _event), Ownership.getFinancialSettings()).then(function () {
				//$tab.find(".spinner").spinner({ min: 1, max: 20, increment: 1 });
				Utils.numericOnly(".numericOnly");
				//trigger the change handler for person types to enable the correct fields.
				$tab.find("select[name=PersonTypeID]").trigger("change");
				//refresh the accordian because if it was hidden it cannot automatically work out the height
				$tab.find(".accordion").accordion('destroy').accordion();

				//#region sales doc upload component
				//init file uploader
				$tab.find("#salesDocUpload").fineUploader({
					uploaderType: 'basic',
					multiple: false,
					autoUpload: true,
					button: $($tab.selector + " #saleDocUploadButton"),
					request: {
						endpoint: "/Pages/UploadFile.ashx"
					}
				}).on('complete', function (event, id, filename, responseJSON) {//ignore jslint
					$tab.find("input[name=SaleDocFileID]").val(responseJSON.fileid);
					$tab.unblock();
					$.jGrowl("File uploaded successfully");
				})//ignore jslint
				.on("submit", function (event, id, filename) {//ignore jslint
					$tab.block();
				})//ignore jslint
				.on("error", function (event, id, filename, reason) {//ignore jslint
					$tab.unblock();
					$.jGrowl("Error Uploading file");
				});//ignore jslint
				//#endregion

				//update the controls
				jQuery.uniform.update();
				$tab.unblock();
			});

		}
	},
	getSalesAgents: function () {
		var $tab = Utils.tabs.getCurrentTab().find(".ownershiptab");
		var dfd = new $.Deferred();

		var responseAction = function (result) {
			Utils.bindDataToGrid($tab.find("select[name=AgencyID]"), "#ddlTemplate", result.EntityList);
			dfd.resolve();
		};

		PropertyService.WCFProperty.GetSalesAgents(responseAction, function (e) { dfd.reject(); Utils.responseFail(e); });
		return dfd.promise();
	},
	getPropertyStatuses: function () {
		var $tab = Utils.tabs.getCurrentTab().find(".ownershiptab");
		var dfd = new $.Deferred();

		var responseAction = function (result) {
		    Utils.bindDataToGrid($tab.find("select[name=PropertyStatusID]"), "#ddlTemplate", result.EntityList);
			dfd.resolve();
		};

		PropertyService.WCFProperty.GetPropertyStatus(responseAction, function (e) { dfd.reject(); Utils.responseFail(e); });
		return dfd.promise();
	},

    // *** EDIT ***

	getPhaseStatuses: function () {
	    var $tab = Utils.tabs.getCurrentTab().find(".descriptiontab");
	    var dfd = new $.Deferred();

	    var responseAction = function (result) {
	        Utils.bindDataToGrid($tab.find("select[name=PhaseID]"), "#ddlTemplate", result.EntityList);
	        dfd.resolve();
	    };

	    PropertyService.WCFProperty.GetPhase(responseAction, function (e) { dfd.reject(); Utils.responseFail(e); });
	    return dfd.promise();
	},

    // *** EDIT END ***

	getOwnerDetail: function (_propertyID) {
		var dfd = new $.Deferred();
		var $tab = Utils.tabs.getCurrentTab().find(".ownershiptab");

		var response = function (_result) {

		    $.when(Ownership.getOwnerTypes(), Ownership.getSalesAgents(), Ownership.getPropertyStatuses()).then(function () {
		        

				if (_result.EntityList[0] !== null) {
					Ownership.currentOwner = _result.EntityList[0];
				} else {
					Ownership.currentOwner = new Entities.Ownership();
				}

				//set the values onto the fields
				$tab.mapObjectTo(Ownership.currentOwner, "name", true);
				//trigger the change handler for person types to enable the correct fields.
				$tab.find("select[name=PersonTypeID]").trigger("change");
				//add or remove the download sale agreement document link
				if (Ownership.currentOwner.SaleDocFileID !== null && Ownership.currentOwner.SaleDocFileID !== undefined) {
					$tab.find("#CurrentFile").attr("href", "/Pages/DownloadFile.ashx?FileRef=" + Ownership.currentOwner.SaleDocFileID);
				} else {
					$tab.find("#CurrentFile").remove();
				}

				//update the controls
				jQuery.uniform.update();
				//resolve the deffered call
				dfd.resolve();
			});

		};

		PropertyService.WCFProperty.GetOwner(_propertyID, response, function (e) { dfd.reject(); Utils.responseFail(e); });

		return dfd.promise();
	},
	getOwnerTypes: function (_event) {
		var dfd = new $.Deferred();
		var $tab = Utils.tabs.getCurrentTab().find(".ownershiptab");

		var response = function (_result) {
			Utils.bindDataToGrid($tab.find("select[name=PersonTypeID]"), "#ddlTemplate", _result.EntityList);
			dfd.resolve();
		};

		PropertyService.WCFProperty.GetPersonTypes(response, function (e) { dfd.reject(); Utils.responseFail(e); });

		return dfd.promise();
	},
	currentOwner: new Entities.Ownership(),
	getOwnershipHistory: function (_propertyID, _event) {
		var dfd = new $.Deferred();
		var $tab = $(_event.target);

		var response = function (_result) {
			Utils.bindDataToGrid($tab.find("#OwnershipHistoryContainer"), "#OwnershipHistoryTmpl", _result.EntityList);
			dfd.resolve();
		};

		PropertyService.WCFProperty.GetPropertyOwnershipHistory(_propertyID, response, function (e) { dfd.reject(); Utils.responseFail(e); });
		return dfd.promise();
	},
	TransferOwnership: function (_event) {
		var $tab = Utils.tabs.getCurrentTab();
		var $ownershipTab = $tab.find(".ownershiptab");
		var Owner = new Entities.Ownership();
		var newOwner = new Entities.Ownership();

		if (Utils.validation.validateForm("#transferOwnershipModal", "All fields are required, please complete and try again.", false, true)) {

			$ownershipTab.mapToObject(Owner, "name");
			$("#transferOwnershipModal").mapToObject(newOwner, "name");
			newOwner.PropertyID = Owner.PropertyID;

			$ownershipTab.block();

			var response = function (_result) {
				//now check if the owner was successfully archived. If so then create new entity and bind to form
				if (_result.EntityList[0] !== 0) {
					
					newOwner.ID = _result.EntityList[0];
					$ownershipTab.mapObjectTo(newOwner, "name", true);
					$ownershipTab.unblock();
					jQuery.uniform.update();

					$("#transferOwnershipModal").dialog("close");
					jAlert("The previous owner was successfully archived and the new owner associated with the property.", "Owner archived");
				} else {
					jAlert("There was a problem while attempting to archive, please try again.", "Error: Could not archive");
				}
			};

			PropertyService.WCFProperty.ArchiveOwner(Owner, newOwner, response, Utils.responseFail);
		}
	},
	initTransferModal: function () {
		$("#transferOwnershipModal").dialog({
			modal: true,
			width: 400,
			autoOpen: false,
			title: "Transfer Property",
			buttons: {
				"Archive Current Owner": function () {
					Ownership.TransferOwnership();
				},
				"Cancel": function () {
					$(this).dialog("close");
				}
			},
			open: function () {
				//call functions to populate add form				
				var $ownershipTab = Utils.tabs.getCurrentTab().find(".ownershiptab").find("select[name=PersonTypeID]");
				var $modalSelect = $("#transferOwnershipModal select[name=PersonTypeID]");

				if ($modalSelect.find("option").length === 0) {

					if ($ownershipTab.length > 0) {
						$ownershipTab.find("option").each(function () {
							$modalSelect.append("<option value='"+ $(this).val() +"'>" + $(this).text() + "</option>");
						});

						$.uniform.update();
					}
				}

			}
		});
	},
	transferOwnership_CLICK: function (e) {
		e.preventDefault();
		var $tab = Utils.tabs.getCurrentTab();
		var $ownershipTab = $tab.find(".ownershiptab");

		if ($ownershipTab.find("input[name=DateOfTransfer]").val() !== "") {
			$("#transferOwnershipModal").dialog("open");
		} else {
			jAlert("You cannot transfer ownership without a date of transfer. Please assign a date of transfer.", "Date of Transfer required");
		}
	},
	getFinancialSettings: function () {
		var $ownershipTab = Utils.tabs.getCurrentTab().find(".ownershiptab");

		var settingsResponse = function (_ownResult) {
			if (_ownResult.EntityList[0] !== null) {
				$ownershipTab.find("input[name=EstateLevy]").val(_ownResult.EntityList[0].EstateLevy);
				$ownershipTab.find("input[name=LandMaintenanceLevy]").val(_ownResult.EntityList[0].LandMaintenanceLevy);
				$ownershipTab.find("input[name=ClubMembership]").val(_ownResult.EntityList[0].ClubFamilyMembership);
				$ownershipTab.find("input[name=ResidentialGolfSubs]").val(_ownResult.EntityList[0].ResidentialGolfSubs);
			}
		};

		PropertyService.WCFProperty.GetSystemSettings(settingsResponse, Utils.responseFail);
	}

};

var PageEvents = {
	load: function () {
		Properties.propertyResidentialPaginate = new Globals.paginationContainer("ERFNumber", 10, Properties.getResidentialProperties);
		Properties.propertySectionalPaginate = new Globals.paginationContainer("ERFNumber", 10, Properties.getSectionalProperties);
		Properties.propertySchemePaginate = new Globals.paginationContainer("ERFNumber", 10, Properties.getSchemeProperties);
		Ownership.initTransferModal();

		PageEvents.bindEvents();
		Properties.getResidentialProperties(true);
		Properties.getSectionalProperties(true);
		Properties.getSchemeProperties(true);
		Properties.getPropertyTypes();
		Properties.getPropertyGroups();
		Erven.getErven("#addForm");
		$("#addForm").on("change", Properties.addFormSelection);
		ManagementAssociation.getAssociations();

		$("[href=#grid]").bind("click", Properties.getProperties);

	},
	bindEvents: function () {

		$('#addSectionalSchemeBtn').bind("click", function () { Properties.addForm($('#Erven :selected').attr("dataid"), false, 10002); });
		$('#addSectionalSection').bind("click", function () { Properties.addForm($('#Erven :selected').attr("dataid"), true, 10001); });
		$('#addSingleResidenceBtn').bind("click", function () { Properties.addForm($('#Erven :selected').attr("dataid"), false, 10000); });

		$("#PropertyTemplateContainer").on("click.deleteProperty", ".deleteProperty", PageEvents.deleteProperty_click);
		$("#SectionalTemplateContainer").on("click.deleteProperty", ".deleteProperty", PageEvents.deleteProperty_click);
		$("#PropertyTemplateContainer").on("click.editProperty", ".editProperty", PageEvents.editProperty_click);
		$("#SectionalTemplateContainer").on("click.editProperty", ".editProperty", PageEvents.editProperty_click);

		$("#SchemeContainer").on("click.deleteProperty", ".deleteProperty", PageEvents.deleteProperty_click);
		$("#SchemeContainer").on("click.editProperty", ".editProperty", PageEvents.editProperty_click);

		$(".uniformselect[name='PropertyTypeID']").change(PageEvents.propertyType_change);
		//person type change on ownership tab
		$('.bodywrapper').on('change', ".uniformselect[name='PersonTypeID']", PageEvents.personType_change);
		//Scheme change for sectional title.
		$(".bodywrapper").on("change", ".uniformselect[name='SchemeID']", Properties.ShemeName_change);

		//Property Tab Binding Functions
		$('.bodywrapper').on('Honeycomb.tabLoad', '#add', Properties.addModal);
		$('.bodywrapper').on("Honeycomb.tabLoad", ".ownershiptab", Ownership.load);
		$('.bodywrapper').on('click.Save', "button[name=topSave]", Properties.commitProperties);

		//ownership tab bindings
		$(".bodywrapper").on('click.transferownership', ".transferownership", Ownership.transferOwnership_CLICK);

		$("#searchSRButt").click(function () {
			Properties.propertyResidentialPaginate = new Globals.paginationContainer("ERFNumber", 10, Properties.getResidentialProperties);
			Properties.getResidentialProperties();
		});

		$("#searchSchemeButt").click(function () {
			Properties.propertySchemePaginate = new Globals.paginationContainer("ERFNumber", 10, Properties.getSchemeProperties);
			Properties.getSchemeProperties();
		});

		$("#filterSR").keypress(function (e) {
			if (e.which === 13) {
				e.preventDefault();
				Properties.propertyResidentialPaginate = new Globals.paginationContainer("ERFNumber", 10, Properties.getResidentialProperties);
				Properties.getResidentialProperties();
			}
		});
		$("#occupantFilterSR").keypress(function (e) {
			if (e.which === 13) {
				e.preventDefault();
				Properties.propertyResidentialPaginate = new Globals.paginationContainer("ERFNumber", 10, Properties.getResidentialProperties);
				Properties.getResidentialProperties();
			}
		});

		$("#secfilter").click(function () {
			Properties.propertySectionalPaginate = new Globals.paginationContainer("ERFNumber", 10, Properties.getSectionalProperties);
			Properties.getSectionalProperties();
		});

		$("#propertyTabs").tabs();
		$("#propertyTabs").bind("tabsselect", function (event, ui) {
			if (ui.index === 0) {
				//reload single residential
				Properties.getResidentialProperties();
			} else if (ui.tab.tabIndex === 1) {
				//reload sectional title
				Properties.getSectionalProperties();
			} else {
				//reload sectional scheme
				Properties.getSchemeProperties();
			}
		});
	},
	addProperty_click: function (e) {
		$(".hornav li.current a").removeClass("current");
		$(".hornav li a[href=#add]").click().addClass("current");
	},
	editProperty_click: function (e) {
		e.preventDefault();
		Properties.getProperty($(this).attr("propertyid"));
	},
	deleteProperty_click: function (e) {
		e.preventDefault();
		Properties.deleteProperty($(this).attr("propertyID"), $(this).parent().parent());
	},
	propertyType_change: function (e) {
		var $tab = Utils.tabs.getCurrentTab();

		if ($(this).val() === "10000") {

			$tab.find(".sectional").hide();
		}
		else {

			$tab.find(".sectional").show();
		}
	},
	personType_change: function (e) {

		var $tab = Utils.tabs.getCurrentTab();

		switch ($(this).val()) {
			case "10001":
				$tab.find(".companyfields").show();
				$tab.find(".trustfields").hide();
				break;

			case "10004":
				$tab.find(".trustfields").show();
				$tab.find(".companyfields").hide();
				break;

			default:
				$tab.find(".companyfields").hide();
				$tab.find(".trustfields").hide();
				break;

		}

	}

};

$(function () {
	PageEvents.load();
});