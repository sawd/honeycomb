﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Honeycomb.Master" AutoEventWireup="true" CodeBehind="Property.aspx.cs" Inherits="Honeycomb.Web.Pages.Property.Property" %>

<%@ Register Src="~/UserControls/PropertyOwnershipTemplate.ascx" TagPrefix="prop" TagName="PropertyOwnershipTemplate" %>
<%@ Register Src="~/UserControls/PropertyDescriptionTemplate.ascx" TagPrefix="prop" TagName="PropertyDescriptionTemplate" %>
<%@ Register Src="~/UserControls/PropertyOccupantTemplate.ascx" TagPrefix="prop" TagName="PropertyOccupantTemplate" %>
<%@ Register Src="~/UserControls/PropertySecurityTemplate.ascx" TagPrefix="prop" TagName="PropertySecurityTemplate" %>
<%@ Register Src="~/UserControls/PropertyStaffTemplate.ascx" TagPrefix="prop" TagName="PropertyStaffTemplate" %>
<%@ Register Src="~/UserControls/PropertyLeaseTemplate.ascx" TagPrefix="prop" TagName="PropertyLeaseTemplate" %>
<%@ Register Src="~/UserControls/PropertyPersonal.ascx" TagPrefix="prop" TagName="PropertyPersonal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
    <%-- References --%>
    <script type="text/javascript" src="/Scripts/plugins/chosen.jquery.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/charCount.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.tagsinput.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.validate.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.tmpl.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.smartWizard-2.0.min.js"></script>
    <script type="text/javascript" src="/Scripts/custom/tables.js"></script>
    <script type="text/javascript" src="/Scripts/custom/forms.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.alerts.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.autogrow-textarea.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/ui.spinner.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/header.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/util.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/button.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/handler.base.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/handler.form.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/handler.xhr.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/uploader.basic.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/dnd.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/uploader.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/jquery-plugin.js"></script>
    <script type="text/javascript" src="/Scripts/custom/jquery-honeycomb.js"></script>
    <script type="text/javascript" src="/Scripts/custom/jquery.sawd.js"></script>
    <script type="text/javascript" src="/Services/WCFProperty.svc/js"></script>
    <script type="text/javascript" src="Scripts/occupant.js"></script>
    <script type="text/javascript" src="Scripts/Security.js"></script>
    <script type="text/javascript" src="Scripts/Staff.js"></script>
    <script type="text/javascript" src="Scripts/Lease.js"></script>
    <script type="text/javascript" src="Scripts/Personal.js"></script>
    <script type="text/javascript" src="Property.aspx.js?update=12202"></script>

    
   
    <link href="/Scripts/plugins/fileupload/fineuploader.css" rel="stylesheet" type="text/css" />

    <%-- Templates --%>
    <%-- templates for personal --%>
    <script type="text/x-jquery-tmpl" id="PetsAccordionTemplate">
        <h3><a href="#">${Type}: ${Name}</a></h3>
        <div id="pet-${PetID}">
            <div class="stdform">
                <input type="hidden" name="PetID" value="${PetID}" />
                <input type="hidden" name="PropertyID" value="${PropertyID}" />
                <input type="hidden" name="PetPhoto" value="${PetPhoto}" />

                    <!-- *** EDITS *** -->

                    <div style="position:absolute; float:left;">
                    <%-- photo --%>
                    <div class="photoDiv{{if PetPhoto!=null}} photoDivWithPhoto{{/if}}" {{if PetPhoto!=null}} style="background-color: #DADADA; background-image:url(/Pages/DownloadFile.ashx?FileRef=${PetPhoto})"{{/if}}>
                        <div class="example">
                            <div id="photo-${PetID}" class="fubExample" data-pet-id="${PetID}"></div>
                            <div class="fubUploadButton" class="btn btn-primary">Upload Pet Photo</div>
                        </div>
                    </div>
                    </div>

                    <!-- *** EDITS *** -->

                    <p>
                        <label>Type:</label>
                        <span class="field">
                             <select name="Type" class="uniformselect">
                                <option {{if Type == 'Cat'}}selected="selected"{{/if}} value="Cat">Cat</option>
                                <option {{if Type == 'Dog'}}selected="selected"{{/if}} value="Dog">Dog</option>
                            </select>
                        </span>
                    </p>

                    <p>
                        <label>Breed:</label>
                        <span class="field">
                            <input type="text" name="Breed" value="${Breed}" valtype="required;" />
                        </span>
                    </p>
                    <p>
                        <label>Name:</label>
                        <span class="field">
                            <input type="text" name="Name" value="${Name}" valtype="required;" />
                        </span>
                    </p>
                    <p>
                        <label>Age:</label>
                        <span class="field">
                            <input type="text" name="Age" value="${Age}"  />
                        </span>
                    </p>
                    <p>
                        <label>Gender:</label>
                        <span class="field">
                            <select name="Gender" class="uniformselect">
                                <option {{if Gender == 'M'}}selected="selected"{{/if}} value="M">Male</option>
                                <option {{if Gender == 'F'}}selected="selected"{{/if}} value="F">Female</option>
                            </select>
                        </span>
                    </p>
                    <p>
                        <label>Owned By:</label>
                        <span class="field">
                             <select name="OwnedBy" class="uniformselect">
                                <option {{if OwnedBy == 'Owner'}}selected="selected"{{/if}} value="Owner">Owner</option>
                                <option {{if OwnedBy == 'Tenant'}}selected="selected"{{/if}} value="Tenant">Tenant</option>
                            </select>
                        </span>
                    </p>
                    <p>
                        <label>Weight:</label>
                        <span class="field">
                            <input type="text" name="Weight" value="${Weight}"  />
                        </span>
                    </p>
                    <div class="contenttitle2">
                    <h3>Chip Details</h3>
                    </div>
                    <p>
                        <label>Tag / Chip ID Number:</label>
                        <span class="field">
                            <input type="text" name="TagID" value="${TagID}"  />
                        </span>
                    </p>
                    <p>
                        <label>Pet has microchip:</label>
                        <span class="field">
                            <input type="checkbox" value="1" name="HasMicrochip" {{if HasMicrochip}}checked="checked" {{/if}} />
                        </span>
                    </p>
                    <p class="hidelabels">
						<label>Files</label>
						<span class="field">
						    <a href="" data-type="ChipCertificate" data-entityID="${PetID}" class="btn btn_archive filesButton"><span>Manage Files</span></a>
						</span>
				    </p>
                 
                    <div class="contenttitle2">
                    <h3>Vet Details</h3>
                    </div>
                    <p>
                        <label>Vet Certificate:</label>
                        <span class="field">
                            <input type="checkbox" value="1" name="VetCertified" {{if VetCertified}}checked="checked"{{/if}} />
                        </span>
                    </p>
                    <p>
                        <label>Vet Name:</label>
                        <span class="field">
                            <input type="text" name="VetName" value="${VetName}"  />
                        </span>
                    </p>
                    <p>
                        <label>Vet Contact Number:</label>
                        <span class="field">
                            <input type="text" name="VetContactNo" value="${VetContactNo}"  />
                        </span>
                    </p>
                    <p class="hidelabels">
						<label>Files</label>
						<span class="field">
						    <a href="" data-type="VetCertificate" data-entityID="${PetID}" class="btn btn_archive filesButton"><span>Manage Files</span></a>
						</span>
				    </p>
                     <ul class="buttonlist" style="text-align:center;margin-top:30px;">
                        <li><a petid="${PetID}" href="javascript:void(0);" class="btn btn_pencil radius50 updatePet"><span>Update Pet</span></a></li>
                        <li><a petid="${PetID}" href="javascript:void(0);" class="btn btn_trash deletePet"><span>Delete Pet</span></a></li>
                    </ul>
                </div>
        </div>
    </script>


    <script type="text/x-jquery-tmpl" id="GolfCartAccordionTemplate">
        <h3><a href="#">${RegNo}: ${dateFormat(RegDate,'dd/mm/yyyy')}</a></h3>
        <div id="cart-${GolfCartID}">
            <div class="stdform">
                <input type="hidden" name="GolfCartID" value="${GolfCartID}" />
                <input type="hidden" name="PropertyID" value="${PropertyID}" />
                    <p>
                        <label>Golf Cart Owner:</label>
                        <span class="field">
                             <select name="OwnedBy" class="uniformselect">
                                <option {{if OwnedBy == 'O'}}selected="selected"{{/if}} value="O">Owner</option>
                                <option {{if OwnedBy == 'T'}}selected="selected"{{/if}} value="T">Tenant</option>
                            </select>
                        </span>
                    </p>
                    <p>
                        <label>Registration No:</label>
                        <span class="field">
                            <input type="text" name="RegNo" value="${RegNo}" valtype="required;" />
                        </span>
                    </p>
                    <p>
                        <label>Registration Date:</label>
                        <span class="field">
                            <input type="text" name="RegDate" class="isDatePicker" value="${dateFormat(RegDate,'dd/mm/yyyy')}" valtype="required;" />
                        </span>
                    </p>
                    <p>
                        <label>Insured:</label>
                        <span class="field">
                            <input type="checkbox" value="1" name="Insured" {{if Insured}}checked="checked"{{/if}} />
                        </span>
                    </p>
                     <ul class="buttonlist" style="text-align:center;margin-top:30px;">
                        <li><a cartid="${GolfCartID}" href="javascript:void(0);" class="btn btn_pencil radius50 updateGolfCart"><span>Update Cart</span></a></li>
                        <li><a cartid="${GolfCartID}" href="javascript:void(0);" class="btn btn_trash deleteGolfCart"><span>Delete Cart</span></a></li>
                    </ul>
                </div>
        </div>
    </script>

    <script type="text/x-jquery-tmpl" id="VehicleAccordionTemplate">
        <h3><a href="#">${Make}: ${RegNo}</a></h3>
        <div id="vehicle-${VehicleID}">
            <div class="stdform">
                <input type="hidden" name="VehicleID" value="${VehicleID}" />
                <input type="hidden" name="PropertyID" value="${PropertyID}" />
                    <p>
                        <label>Make:</label>
                        <span class="field">
                            <input type="text" name="Make" value="${Make}" valtype="required;" />
                        </span>
                    </p>
                    <p>
                        <label>Registration No:</label>
                        <span class="field">
                            <input type="text" name="RegNo" value="${RegNo}" valtype="required;" />
                        </span>
                    </p>
                    <p>
                        <label>Licence Registration Date:</label>
                        <span class="field">
                            <input type="text" name="LicRegDate" class="isDatePicker" value="${dateFormat(LicRegDate, 'dd/mm/yyyy')}" valtype="required;" />
                        </span>
                    </p>
                    <p>
                        <label>License Expiry:</label>
                        <span class="field">
                            <input type="text" name="LicExpiryDate" class="isDatePicker" value="${dateFormat(LicExpiryDate, 'dd/mm/yyyy')}" valtype="required;" /> 
                        </span>
                    </p>
                     <ul class="buttonlist" style="text-align:center;margin-top:30px;">
                        <li><a vehicleid="${VehicleID}" href="javascript:void(0);" class="btn btn_pencil radius50 updateVehicle"><span>Update Vehicle</span></a></li>
                        <li><a vehicleid="${VehicleID}" href="javascript:void(0);" class="btn btn_trash deleteVehicle"><span>Delete Vehicle</span></a></li>
                    </ul>
                </div>
        </div>
    </script>

	<%-- lease templates --%>
	<script type="text/x-jquery-tmpl" id="ddlTenantTemplate">
		<option value="${ID}">${FirstName} ${LastName}</option>
	</script>

    <%-- render the templates required for occupants --%>
    <prop:PropertyOccupantTemplate runat="server" ID="PropertyOccupantTemplate" />

    <%-- render the templates required for staff --%>
    <prop:PropertyStaffTemplate runat="server" ID="PropertyStaffTemplate" />
   
     <script id="LeaseExtensionTemplate" type="text/x-jquery-tmpl">
        <tr>
            <td>${dateFormat(EndDate,'dd/mm/yyyy')}</td>
            <td><a href="/Pages/DownloadFile.ashx?FileRef=${FileID}">Download Document</a></td>
            <td class="right" align="right"><a href="#" onclick="Lease.ExtendLease(null,${ID});" class="btn btn3 btn_pencil" ></a></td>
        </tr>
    </script>
   
    <script id="LeaseExtensionModalTemplate" type="text/x-jquery-tmpl">
        <input type="text" name="EndDate" id="EndDateDatePicker" class="smallinput isDatePicker" value="${dateFormat(EndDate,'dd/mm/yyyy')}" />&nbsp;&nbsp;<span id="extendUpload"><a id="extendUploadButton" class="btn btn_document" href="javascript:void(0);"><span>Upload File</span></a></span>
        <input type="hidden" name="FileID" value="${FileID}" />
        <input type="hidden" name="ID" value="${ID}" />
    </script>
  
    <script id="PropertyTemplate" type="text/x-jquery-tmpl">
        <tr>
            <td>${ERFNumber}</td>
            <td>${Phase}</td>
            <td>${StreetNumber}</td>
            <td>${StreetName}</td>
            <td>${PrincipalOwnerName}</td>
            <td>${ContactNumber}</td>
            <td class="right" align="right"><a href="#" propertyid="${PropertyID}" class="btn btn3 btn_search editProperty" style="margin-right:10px;"></a><a href="#" propertyid="${PropertyID}" class="btn btn3 btn_trash deleteProperty"></a></td>
        </tr>
    </script>
    <script id="SectionalTemplate" type="text/x-jquery-tmpl">
        <tr>
            <td>${ERFNumber}</td>
            <td>${Phase}</td>
            <td>${Complex}</td>
            <td>${SectionNumber}</td>
            <td>${DoorNumber}</td>
            <td>${StreetNumber}</td>
            <td>${StreetName}</td>
            <td>${PrincipalOwnerName}</td>
            <td>${ContactNumber}</td>
            <td class="right" align="right"><a href="#" propertyid="${PropertyID}" class="btn btn3 btn_search editProperty" style="margin-right:10px;"></a><a href="#" propertyid="${PropertyID}" class="btn btn3 btn_trash deleteProperty"></a></td>
        </tr>
    </script>
    <script id="PropertyFormTemplate" type="text/x-jquery-tmpl">
        <div class="pageheader nopadding">
            <h1 class="pagetitle">&nbsp;</h1>
            <div class="topSave">
                <button type="button" class="submitbutton" dataid="{ID}" name="topSave">Save Property</button>
            </div>
        </div>
        
        <div id="{ID}Tabs" class="tabs">
            <ul>
                <li><a href="#{ID}-Description" >Description</a></li>
                <li><a href="#{ID}-Ownership" >Ownership Detail</a></li>
                <li><a href="#{ID}-Personal">Personal</a></li>
                <li><a href="#{ID}-Occupant">Occupants</a></li>
                <li><a href="#{ID}-Lease">Lease</a></li>
                <li><a href="#{ID}-Security">Security</a></li>
                <li><a href="#{ID}-Staff">Staff</a></li>
            </ul>
            <div id="{ID}-Description" class="descriptiontab current" >
                <prop:PropertyDescriptionTemplate runat="server" ID="PropertyDescriptionTemplate" />
            </div>
            <div id="{ID}-Ownership" class="ownershiptab" >
                <prop:PropertyOwnershipTemplate runat="server" ID="PropertyOwnershipTemplate" />
            </div>
            <div id="{ID}-Occupant" class="occupanttab">
                <p>Add new occupant &nbsp;&nbsp;<button id="addoccupant" type="button" class="stdbtn">Add Occupant</button> display: <select name="viewmode" id="viewmode" class="uniformselect"><option value="current">Current Occupants</option><option value="history">Occupant History</option></select></p>
                <div id="OccupantContainer" class="accordion">
                </div>
            </div>
            <div id="{ID}-Security" class="securitytab">
                <prop:PropertySecurityTemplate runat="server" ID="PropertySecurityTemplate" />
            </div>
            <div id="{ID}-Staff" class="stafftab">
                <p>Add new staff &nbsp;&nbsp;<button id="addstaff" type="button" class="stdbtn">Add A Staff Member</button></p>
                <div id="StaffContainer" class="accordion staffaccordion"></div>
            </div>
            <div id="{ID}-Lease" class="leasetab">
                <prop:PropertyLeaseTemplate runat="server" ID="PropertyLeaseTemplate" />
            </div>
            <div id="{ID}-Personal" class="personaltab">
                <prop:PropertyPersonal runat="server" ID="PropertyPersonal" />
            </div>
        </div>
        <div class="bottomSave">
            <button type="button" class="submitbutton" dataid="{ID}" name="topSave">Save Property</button>
        </div>
    </script>
    <script type="text/x-jquery-tmpl" id="schemeDDL">
        <option value="${ID}">${SchemeName}</option>
    </script>
    <script type="text/x-jquery-tmpl" id="ddlTemplate">
        <option value="${ID}">${Name}</option>
    </script>
    <script type="text/x-jquery-tmpl" id="erfDDL">
        <option value="${ErfTypeID}" dataid="${ID}">${ErfNumber}</option>
    </script>
    <script type="text/x-jquery-tmpl" id="agentDDLTemplate">
        <option value="${AgentID}">${AgentFirstName} ${AgentLastName}</option>
    </script>
    <script type="text/x-jquery-tmpl" id="OwnershipHistoryTmpl">
        <h3><a href="#">Transfer Date: ${dateFormat(DateOfTransfer, "dd/mm/yyyy")} : ${PrincipalOwnerName}</a></h3>
        <div>
            <div><strong>Transfer Details:</strong></div>
            <div class="one_half nomargin">
                <table class="OwnershipHistoryTable">
                    <tr>
                        <td class="title">Owner:</td>
                        <td>${PrincipalOwnerName}</td>
                        <td class="title">ID/Reg Number:</td>
                        <td>${GovID}</td>
                    </tr>
                    <tr>
                        <td class="title">Owner Type:</td>
                        <td>${OwnerType}</td>
                        <td class="title">Mobile Number:</td>
                        <td>${Cell}</td>
                    </tr>
                    <tr>
                        <td class="title">Transfer Date:</td>
                        <td>${dateFormat(DateOfTransfer, "dd/mm/yyyy")}</td>
                        <td class="title">Email Address:</td>
                        <td>${Email}</td>
                    </tr>
                    {{if SaleAgreementFile !== null}}
                    <tr>
                        <td class="title" colspan="2">Copy of sale agreement:</td>
                        <td colspan="2"><a href="/Pages/DownloadFile.ashx?FileRef=${SaleAgreementFile}">Download Document</a></td>
                    </tr>
                    {{/if}}
                </table>
            </div>
            <div class="one_half nomargin"></div>
            <div class="clearall"></div>
        </div>
    </script>

    <script type="text/x-jquery-tmpl" id="SecurityHistoryTmpl">
        <h3><a href="#">${dateFormat(IncidentDate, "dd/mm/yyyy")} Type : ${IncidentType}</a></h3>
        <div>
            <table class="OwnershipHistoryTable">
                <tr>
                    <td class="title">Incident Date:</td>
                    <td>${dateFormat(IncidentDate, "dd/mm/yyyy")}</td>
                </tr>
                <tr>
                    <td class="title">Incident Time:</td>
                    <td>${IncidentTime}</td>
                </tr>
                <tr>
                    <td class="title">Status:</td>
                    <td>${Status}</td>
                </tr>
            </table>
            <p><a href="javascript:void(0);" incidentid="${ID}" class="btn  btn_search radius50 viewincidenthistory"><span>View Details</span></a></p>
        </div>
    </script>

    <script type="text/x-jquery-tmpl" id="ViewIncidentTmpl">
        <form>
        <div id="historyLeft" class="floatLeft">
                <table>
                    <tr>
                        <td>Assigned To:</td><td>${AssignedTo}</td>
                    </tr>
                    <tr>
                        <td>Source:</td><td>${Source}</td>
                    </tr>
                    <tr>
                        <td>Type:</td><td>${IncidentType}</td>
                    </tr>
                    <tr>
                        <td>Priority:</td><td>${Priority}</td>
                    </tr>
                    <tr>
                        <td>Date:</td><td>${dateFormat(IncidentDate,"dd/mm/yyyy")}</td>
                    </tr>
                    {{if ResolutionDate != null}}
                    <tr>
                        <td>Resolution Date:</td><td>${dateFormat(ResolutionDate,"dd/mm/yyyy")}</td>
                    </tr>
                    {{/if}}
                    <tr>
                        <td>Time:</td><td>${IncidentTime}</td>
                    </tr>
                    {{if FileID != null}}
                    <tr>
                        <td>View Photo:</td><td><a href="javascript:void(0)"></a></td>
                    </tr>
                    {{/if}}
                    <tr>
                        <td>Status:</td><td>${Status}</td>
                    </tr>
                </table>
            </div>
            <div id="historyRight" class="floatRight">
                <h4>Parties Involved</h4>
                <textarea cols="80" rows="2" readonly="readonly" class="longinput">{{each PartiesInvolved}}${$value}{{if $index+1 != $data.PartiesInvolved.length}}, {{/if}}{{/each}}</textarea>
                <h4>Description</h4>
                <textarea cols="80" rows="5" readonly="readonly" class="longinput">${Description}</textarea>
                <h4>Resolution Description{{if ResolutionDate != null}} (Resolved ${dateFormat(ResolutionDate,'dd/mm/yyyy')}){{/if}}</h4>
                <textarea cols="80" rows="5" readonly="readonly" class="longinput">${ResolutionDescription}</textarea>

            </div>
        </form>
    </script>
    <%-- Content --%>

    <!-- modal -->
    <div style="display:none">
        <div id="addForm">
                <label>Please select an Erf: </label>
                <span class="field">
                        <select id="Erven" name="ErfID" data-placeholder="Select Erf..." class="chzn-select" style="width:280px;">
                        </select>         
                    <img class="erfloader" src="/Theme/images/loaders/loader2.gif"/>
                </span><br />
            <div class="addButtons">
                <label>What kind of property would you like to add?</label><br />
                <button type="button" id="addSingleResidenceBtn" class="stdbtn btn_lime" disabled="disabled">Single Residence</button>
                <button type="button" id="addSectionalSchemeBtn" class="stdbtn btn_lime" disabled="disabled">Sectional Scheme</button>
                <button type="button" id="addSectionalSection" class="stdbtn btn_lime" disabled="disabled">Sectional Section</button>
            </div>
        </div>
    </div>

    <!-- view history modal -->
    <div style="display:none">
        <div id="viewhistory" class="clearMe">
            
        </div>
    </div>

    <!-- staff notes modal -->
    <div style="display:none">
        <div id="staffnotes" class="clearMe">
            <label>Please select a note: </label>
                <span class="field">
                        <select id="NoteID" name="NoteID" data-placeholder="Select Note..." class="chzn-select" style="width:280px;">
                        </select>         
                    <img class="erfloader" src="/Theme/images/loaders/loader2.gif"/>
                </span>
            <div style="margin:10px 0;"><input type="text" name="Title" class="smallinput" /></div>
                <div style="margin-top:10px;">
                    <textarea style="width:98%;" name="NoteDescription" cols="80" rows="8" class="longinput"></textarea>
                </div>
        </div>
    </div>

    <!-- extend lease modal -->
    <div style="display:none">
        <div id="extendLeaseModal" class="clearMe">
            
        </div>
    </div>


    <%-- generic modal container --%>
    <div style="display:none">
        <div id="modalcontainer" class="clearMe">
            
        </div>
    </div>

	<%-- transfer ownership modal container --%>
    <div class="stdform" id='transferOwnershipModal' style="display:none;">
		<p>Please provide the new owners details so that we can archive the current owner.</p>
        <table cellpadding="10">
			<tr><td>Owner Type:</td><td width="20"></td><td><select name="PersonTypeID" class="uniformselect"></select></td></tr>
            <tr><td>Property Name:</td><td width="20"></td><td><input type="text" name="PropertyName" valtype="required;" /></td></tr>
            <tr><td>Principal Owner Name:</td><td width="20"></td><td><input type="text" name="PrincipalOwnerName" valtype="required;" /></td></tr>
			<tr><td>Mobile Number:</td><td width="20"></td><td><input type="text" name="Cell" valtype="required;" /></td></tr>
			<tr><td>Email Address:</td><td width="20"></td><td><input type="text" name="Email" valtype="required;" /></td></tr>
        </table>
    </div>

    <!--pageheader-->
    <ul class="hornav">
        <li><a href="#add"><img class='tabIcon' src='/Theme/Images/addfolder.png'/>&nbsp;</a></li>
        <li class="current"><a href="#grid">
            <img class="loader" style="display:none;" src="/Theme/images/loaders/loader2.gif" alt="" />All Properties</a></li>
        
    </ul>
    <div id="contentwrapper" class="contentwrapper">
        <div id="grid" class="subcontent">

            <div id="propertyTabs" class="">
	            <ul>
		            <li><a href="#tabs-1">Single Residential</a></li>
		            <li><a href="#tabs-2">Sectional Title</a></li>
                    <li><a href="#tabs-3">Sectional Scheme</a></li>
	            </ul>
	            <div id="tabs-1"> 

                        <div class="tableoptions">Search <input type="text" name="filter" id="filterSR" placeholder="Owner surname / ERF No. / email" class="smallinput radius3" style="width:200px;" /> &nbsp; <input type="text" name="occupantFilterSR" id="occupantFilterSR" placeholder="Occupant name / email" class="smallinput radius3" style="width:200px;" /> &nbsp; <button type="button" id="searchSRButt" class="radius2">Search</button></div>
                        <table cellpadding="0" cellspacing="0" border="0" id="PropertyTable" class="stdtable">
                            <thead>
                                <tr>
                                    <th class="head0" sortcolumn="ERFNumber">Erven</th>
                                    <th class="head1" sortcolumn="Phase">Phase</th>
                                    <th class="head0" sortcolumn="StreetNumber">Street #</th>
                                    <th class="head1" sortcolumn="StreetName">Street Name</th>
                                    <th class="head0" sortcolumn="PrincipalOwnerName">Principal Owner</th>
                                    <th class="head1">Contact #</th>
                                    <th class="head0">Function</th>
                                </tr>
                            </thead>
                            <tbody id="PropertyTemplateContainer">
                            </tbody>
                        </table>
                        <div class="dataTables_paginate">
                            <div id="propertyPagination"></div>

                        </div>	    

	            </div>
	            <div id="tabs-2"> 
                        <div class="tableoptions">
                            <div class="clearMe">
                                <div style="display:inline;float:left;">
                                    <select id="propertyGroupSelect" data-placeholder="Filter by Complex..." class="chzn-select radius3" multiple="multiple" style="width:450px;" tabindex="4">
                                </select>
                                </div>
                                <div style="display:inline;float:left;margin-left:5px;"> Search <input type="text" name="filter" id="filterST" placeholder="Owner surname / ERF No. / email" class="smallinput radius3" style="width:200px;" /> &nbsp; <input type="text" name="occupantFilterST" id="occupantFilterST" placeholder="Occupant Name / email" class="smallinput radius3" style="width:200px;" /> &nbsp; <button type="button" id="secfilter" class="radius2">Search</button></div>
                           </div>
                        </div>
                        <table cellpadding="0" cellspacing="0" border="0" id="SectionalTable" class="stdtable radius2">
                            <thead>
                                <tr>
                                    <th class="head0" sortcolumn="ERFNumber">Erven</th>
                                    <th class="head1" sortcolumn="Phase">Phase</th>
                                    <th class="head0" sortcolumn="Complex">Complex</th>
                                    <th class="head1" sortcolumn="SectionNumber">Section #</th>
                                    <th class="head0" sortcolumn="DoorNumber">Door #</th>
                                    <th class="head1" sortcolumn="StreetNumber">Street #</th>
                                    <th class="head0" sortcolumn="StreetName">Street Name</th>
                                    <th class="head2" style="width:120px;" sortcolumn="PrincipalOwnerName">Principal Owner</th>
                                    <th class="head0">Contact #</th>
                                    <th class="head1">Function</th>
                                </tr>
                            </thead>
                            <tbody id="SectionalTemplateContainer">
                            </tbody>
                        </table>
                        <div class="dataTables_paginate">
                            <div id="sectionalPagination"></div>

                        </div>

	            </div>
                <div id="tabs-3">

                    <div class="tableoptions">Search <input type="text" name="filter" id="filterScheme" placeholder="Owner surname / ERF No." class="smallinput radius3" style="width:200px;" /> &nbsp; <input type="text" name="filterOccupant" id="filterOccupant" placeholder="Occupant name" class="smallinput radius3" style="width:200px;" /> &nbsp; <button type="button" id="searchSchemeButt" class="radius2">Search</button></div>
                        <table cellpadding="0" cellspacing="0" border="0" id="SchemeTable" class="stdtable">
                            <thead>
                                <tr>
                                    <th class="head0" sortcolumn="ERFNumber">Erven</th>
                                    <th class="head1" sortcolumn="Phase">Phase</th>
                                    <th class="head0" sortcolumn="StreetNumber">Street #</th>
                                    <th class="head1" sortcolumn="StreetName">Street Name</th>
                                    <th class="head0" sortcolumn="PrincipalOwnerName">Principal Owner</th>
                                    <th class="head1">Contact #</th>
                                    <th class="head0">Function</th>
                                </tr>
                            </thead>
                            <tbody id="SchemeContainer">
                            </tbody>
                        </table>
                        <div class="dataTables_paginate">
                            <div id="schemePagination"></div>

                        </div>	    

                </div>
            </div><!-- #tabs -->


        </div>
        <div id="add" class="subcontent" style="display: none">


        </div>

    </div>
    <!--contentwrapper-->
</asp:Content>
