﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Honeycomb.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="Honeycomb.Web.Pages.Dashboard.Home" %>

<%@ Import Namespace="Honeycomb.Custom" %>
<asp:Content runat="server" ID="HomeContent" ContentPlaceHolderID="SiteContent">
	<script type="text/javascript" src="/services/WCFDashboard.svc/js"></script>
	<script src="/scripts/custom/jquery-sawd-graph.js"></script>
	<script src="/scripts/custom/jquery-sawd-widget.js"></script>
	<script src="/scripts/plugins/countup.min.js"></script>
	<div class="dashboardWrapper">
		<div id="DashBoardTable">
			<div id="DashBoardRow">
				<div id="TableContainer" runat="server" clientidmode="Static"></div>
				<div id="GraphContainer" runat="server" clientidmode="Static"></div>
				<div id="WidgetContainer" runat="server" clientidmode="Static"></div>
			</div>
		</div>
	</div>
</asp:Content>
