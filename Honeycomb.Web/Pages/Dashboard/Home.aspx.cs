﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Honeycomb.Custom;

namespace Honeycomb.Web.Pages.Dashboard {
    public partial class Home : Custom.BaseClasses.BasePage {
        protected override void OnInit(EventArgs e) {
            PageRoles.Add(17);
			SessionManager.HorizontalPageID = 0;
            base.OnInit(e);
        }
		protected void Page_Load(object sender, EventArgs e) {
			
			//Check Incidents Permission

            #region tabular graphs
            if (Utility.CheckUserAccess(new List<string> { "DBEstateDemographics", "Admin" })) {
                TableContainer.Controls.Add(LoadControl("~/UserControls/Dashboard/PropertyDemographics.ascx"));
            }

            if (Utility.CheckUserAccess(new List<string> { "DBProjectedFees", "Admin" })) {
                TableContainer.Controls.Add(LoadControl("~/UserControls/Dashboard/CurrentProjectedFees.ascx"));
            }

            if (Utility.CheckUserAccess(new List<string> { "DBPeopleTotals", "Admin" })) {
                TableContainer.Controls.Add(LoadControl("~/UserControls/Dashboard/PeopleTotalsTable.ascx"));
            }
            #endregion

            #region widget graphs (lozenges)
            if (Utility.CheckUserAccess(new List<string> { "DBVisitorsOnEstate", "Admin" })) {
                WidgetContainer.Controls.Add(LoadControl("~/UserControls/Dashboard/NonResidentsOnEstate.ascx"));
            }

            if (Utility.CheckUserAccess(new List<string> { "DBSuppliersOnEstate", "Admin" })) {
                WidgetContainer.Controls.Add(LoadControl("~/UserControls/Dashboard/NonResidentsOnEstateSupplier.ascx"));
            }

            if (Utility.CheckUserAccess(new List<string> { "DBResidentCartNotInsured", "Admin" })) {
                WidgetContainer.Controls.Add(LoadControl("~/UserControls/Dashboard/ExceptionAlertGolfCarts.ascx"));
            }

            if (Utility.CheckUserAccess(new List<string> { "DBResidentNotInducted", "Admin" })) {
                WidgetContainer.Controls.Add(LoadControl("~/UserControls/Dashboard/ExceptionAlertInduction.ascx"));
            }
            #endregion

            #region chart graphs
            if (Utility.CheckUserAccess(new List<string> { "DBCurrentIncidents", "Admin" })) {
                GraphContainer.Controls.Add(LoadControl("~/UserControls/Dashboard/CurrentIncidentsGraph.ascx"));
            }

            if (Utility.CheckUserAccess(new List<string> { "DBAverageIncidentTurnaround", "Admin" })) {
                GraphContainer.Controls.Add(LoadControl("~/UserControls/Dashboard/AverageIncidentTurnAround.ascx"));
            }

            if (Utility.CheckUserAccess(new List<string> { "DBIncidentsByType", "Admin" })) {
                GraphContainer.Controls.Add(LoadControl("~/UserControls/Dashboard/IncidentCategoryCountGraph.ascx"));
            }

            if (Utility.CheckUserAccess(new List<string> { "DBCodesHistorical", "Admin" })) {
                GraphContainer.Controls.Add(LoadControl("~/UserControls/Dashboard/AccessCodesForEntranceHistorical.ascx"));
            }

            if (Utility.CheckUserAccess(new List<string> { "DBCodesFuture", "Admin" })) {
                GraphContainer.Controls.Add(LoadControl("~/UserControls/Dashboard/AccessCodesForEntranceFuture.ascx"));
            }

            if (Utility.CheckUserAccess(new List<string> { "DBCodeSources", "Admin" })) {
                GraphContainer.Controls.Add(LoadControl("~/UserControls/Dashboard/AccessCodeSources.ascx"));
            }

            if (Utility.CheckUserAccess(new List<string> { "DBSentSMS", "Admin" })) {
                GraphContainer.Controls.Add(LoadControl("~/UserControls/Dashboard/SentSMSs.ascx"));
            }
            #endregion


			//hide if there are no controls
			if(WidgetContainer.Controls.Count == 0) {
				WidgetContainer.Visible = false;
			}
			//hide if there are no controls
			if(GraphContainer.Controls.Count == 0) {
				GraphContainer.Visible = false;
			}
			//hide if there are no controls
			if(TableContainer.Controls.Count == 0) {
				TableContainer.Visible = false;
			}

		}
    }
}