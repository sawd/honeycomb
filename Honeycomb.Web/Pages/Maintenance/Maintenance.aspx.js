﻿/*global Utils,MaintenanceService,jConfirm,jAlert,Entities, Globals*/
var lookups = {
    maintenancePaginate: null,
    getMaintainance: function () {

        var lookupsResponse = function (result) {
            lookups.maintenancePaginate.TableID = "#MaintenanceTable";
            lookups.maintenancePaginate.PaginationID = "#MaintantencePagination";
            lookups.maintenancePaginate.TotalCount = result.Count;

            Utils.bindDataToGrid("#MaintenanceTemplateContainer", "#maintenenceTemplate", result.EntityList);
            lookups.maintenancePaginate.bindPagination();

            if (result.EntityList.length === 0) {
                $.jGrowl("close");
                $.jGrowl("There were no items found", { header: "<b>No records</b>" });
            }

            if ($(".loader").is(":visible")) {
                $(".loader").slideToggle();//ignore jslint
            }
        };
        
        MaintenanceService.WCFMaintenance.GetLookUpOptions(lookups.maintenancePaginate, $("#filterSR").val(), lookupsResponse, Utils.responseFail);
    },

    deleteLookUp: function (dataID, rowElement) {
        jConfirm("Are you sure you want to delete this lookUp Option", "", function (_result) {
            if (_result) {

                var deleteReponse = function (_result) {
                    $.jGrowl("lookUp Option Deleted");
                    if (_result.Count > 0) {
                        if (_result.EntityList[0].lookUpID === 10000) {
                            lookups.getMaintainance();
                        }
                    }
                };

                //mark the row as queued for deletion
                Utils.tables.markRowAsDeleted(rowElement);

                MaintenanceService.WCFMaintenance.DeleteLookUpOption(dataID, deleteReponse, Utils.responseFail);
            }
        });
    },
    lookUpArr: [],
    saveLookups:function(event)
    {

        var lookUpID = $(event.target).attr('lookUpTypeID'),
            tabID,
            isNew = false;
        tabID = Utils.tabs.getCurrentTab().append($(' div .descriptiontab:eq(1)'));

        var lookUpOptionEntity = new Entities.LookUpOption();
        
        if (Utils.validation.validateForm(tabID, "LookUpoption tab missing required fields", false, true))
        {
            $(tabID).find('input[type=text]').attr('lookUpTypeID');

            $(tabID).mapToObject(lookUpOptionEntity, "Name");
            lookUpOptionEntity.LookUpID = lookups.GetLookUpID();

            lookUpOptionEntity.Name = $('input[dataid=' + lookUpID + ' ]').val();
            lookUpOptionEntity.ID = lookUpID;
            //check if this is new entry
            if (lookUpID === 0 || lookUpID === "0") {
                isNew = true;
                if (lookUpOptionEntity.Name.replace(/[\s]/g, '').length === 0) {
                    $('input[dataid=' + lookUpID + ' ]').addClass("error");
                    //break away from program execution
                    return;
                } else {
                    $('input[dataid=' + lookUpID + ' ]').removeClass("error");
                }
            }
        }

        var commitResponse = function (result) {
            if (result.Message === "") {
                jAlert("LookUp Saved", "Saved");
                $(tabID).unblock();

                if (isNew) {
                    $(Utils.tabs.getCurrentTab()).html("");
                    lookups.getLookUp(lookups.GetLookUpID());
                }
            }
            else {
                jAlert("There was an error while attempting a save,this item was not saved", "Error");
                $(tabID).unblock();
            }
        };

        var fail = function (result) {
			$(tabID).unblock();
			Utils.responseFail(result);
        };

        $(tabID).block();
        MaintenanceService.WCFMaintenance.UpdateLookUpOptions(lookUpOptionEntity,  commitResponse, fail);
    },
    GetLookUpID: function () {
        var lookUpID = Utils.tabs.getCurrentTab();
        lookUpID = lookUpID[0].id.replace('lookup', '');
        if (isNaN(lookUpID)) {
            lookUpID = 0;
        }
        return lookUpID;    
    },
    addLookUp: function (LookUpEntity) {
        var ret = false;
        var response = function (_result) {
            if (_result.EntityList[0].success) {
                lookups.getMaintainance();
            }
        };

        MaintenanceService.WCFMaintenance.UpdateLookups(LookUpEntity, response, Utils.responseFails);
        return ret;
    },
    getLookUp: function (lookUpID) {

        var currentTab = Utils.tabs.getCurrentTab();
        var onsuccess = function (_result) {
            if (_result.EntityList[0]) {
                var lookUpHeader = _result.EntityList[0].lookUpName;
                //loading 
                var template = $($("#MaintenanceLookUpTemplate").html().replace(/{ID}/gi, lookUpID));//ignore jslint
                template.find('h1.pagetitle').html(lookUpHeader);

                Utils.tabs.bindAndInitTab("MaintenanceLookUpTemplate", _result.EntityList);
                $(currentTab).append($("#newOptionTemplate").html());
            } else {
                $(currentTab).append($("#newOptionTemplate").html());
            }
            return;
        };

        MaintenanceService.WCFMaintenance.GetLookUpOptionsBylookUptypeID(lookUpID, onsuccess, Utils.responseFails);
    },

    addModal: function (_event) {

        if ($(_event.target).attr("id") == "add") {
            Utils.bindDataToGrid("#editLookUpContainer", "#LookUpOptionCreateTemplate", {});
            $("#editLookUpContainer").dialog({
                autoOpen: true,
                resizable: true,
                modal: true,
                width: "300px",
                title: "Add lookUp option",
                beforeClose: lookups.addModalClose,
                buttons: {
                    Save: function () {

                        if (Utils.validation.validateForm("#editLookUpContainer", "There are missing values, please check the form.", false, true)) {
                            //send information to save to Db
                            lookups.addLookUp($("#editLookUpContainer").mapToObject(new Entities.LookUp(), "name"));
                            $(this).dialog("close");
                        }
                    },
                    Cancel: function () {
                        $(this).dialog("close");
                    }
                }
            });
        }
    },
    addModalClose: function (event, ui) {
            Utils.tabs.selectTab("#grid");
    }
};

var PageEvents = {
    load: function () {
        lookups.maintenancePaginate = new Globals.paginationContainer("lookUpTypeName", 5, lookups.getMaintainance);
        PageEvents.bindEvents();
        lookups.getMaintainance();

        $('.bodywrapper').uniform();

    },
    bindEvents: function () {

        $('.bodywrapper').on('Honeycomb.tabLoad', '#add', lookups.addModal);
        $("#MaintenanceTemplateContainer").on("click.deleteLookUp", ".deleteLookUp", PageEvents.deleteLookUp_click);
        $("#contentwrapper").on("click.deleteLookUp", ".deleteLookUpOption", PageEvents.deleteLookUp_click);
        $("#MaintenanceTemplateContainer").on("click.editLookUp", ".editLookUp", PageEvents.editLookUp_click);

        $('#contentwrapper').on('click.Save', ".saveLookUp", lookups.saveLookups);
        $("#searchSRButt").click(function () {

            lookups.getMaintainance();
        });

        $("#Tabs").tabs();
    },
    editLookUp_click: function (e) {

        e.preventDefault();
        var editButton = $(this);
        Utils.tabs.loadTab($(this).attr("data-lookupheader"), "lookup" + $(this).attr("lookupid"), function () { lookups.getLookUp(editButton.attr("lookupid")); });
    },
    addLookUp_click :function (e){
        e.preventDefault();
        lookups.addLookUp();
    },
    deleteLookUp_click: function (e) {
        e.preventDefault();
        lookups.deleteLookUp($(e.target).attr('lookUpTypeID'), $(this).parent().parent());
    }


};

$(function () {
    /*$(".hornav li a")
      .button()
      .click(function () {
          $("#editLookUpContainer").dialog("open");
      });
      */
    PageEvents.load();

});