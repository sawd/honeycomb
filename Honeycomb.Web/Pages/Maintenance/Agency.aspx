﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Honeycomb.Master" AutoEventWireup="true" CodeBehind="Agency.aspx.cs" Inherits="Honeycomb.Web.Pages.Maintenance.Agency" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
	<%-- References --%>
	<script type="text/javascript" src="/Scripts/plugins/jquery.tagsinput.min.js"></script>
	<script type="text/javascript" src="/Scripts/plugins/chosen.jquery.js"></script>
	<script type="text/javascript" src="/Scripts/plugins/jquery.uniform.min.js"></script>
	<script type="text/javascript" src="/Scripts/plugins/jquery.validate.min.js"></script>
	<script type="text/javascript" src="/Scripts/plugins/jquery.tmpl.min.js"></script>
	<script type="text/javascript" src="/Scripts/custom/tables.js"></script>
	<script type="text/javascript" src="/Scripts/custom/forms.js"></script>
	<script type="text/javascript" src="/Scripts/plugins/jquery.alerts.js"></script>
	<script type="text/javascript" src="/Scripts/plugins/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="/Scripts/custom/jquery-honeycomb.js"></script>
	<script type="text/javascript" src="/Services/WCFMaintenance.svc/js"></script>
	<script type="text/javascript" src="Agency.aspx.js"></script>

	<%-- grid template --%>

	<script id="AgencyDDL" type="text/x-jquery-tmpl">
        <option value="${ID}">${Name}</option>
    </script>

	<script id="AgentGridTemplate" type="text/x-jquery-tmpl">
		<tr>
			<td>${AgentFirstName} ${AgentLastName}</td>
			<td>${Email}</td>
			<td>${CelNumber}</td>
			<td class="right" align="right"><a href="#" agentID="${AgentID}" class="btn btn3 btn_pencil editAgent" style="margin-right: 10px;"></a></td>
		</tr>
	</script>

	    <%-- contractor tab template --%>
    <script id="AgentTabTemplate" type="text/x-jquery-tmpl">
        <div class="pageheader nopadding">
            <input type="hidden" name="AgentID" value="${AgentID}" />
        </div>
		<div class="stdform">
			<div class="one_half nomargin">
				<div class="contentBlock" style="margin-bottom:10px;">
					<div class="contenttitle2">
						<h3>Agent Details</h3>
					</div>
				
					<p>
				<label>Agency:</label>
				<span class="field">
					<select name="AgencyID" data-placeholder="Choose a Agency..." class="chzn-select" style="width:200px" valtype="required;" ></select>
				</span>
			</p>
			<p>
				<label>First Name:</label>
				<span class="field">
					<input type="text" name="AgentFirstName" value="${AgentFirstName}" valtype="required;" />
				</span>
			</p>
			<p>
				<label>Last Name:</label>
				<span class="field">
					<input type="text" name="AgentLastName" value="${AgentLastName}" valtype="required;"  />
				</span>
			</p>
			<p>
				<label>Email:</label>
				<span class="field">
					<input type="text" name="Email" value="${Email}" valtype="required;regex:email"  />
				</span>
			</p>
			<p>
				<label>Cell Number:</label>
				<span class="field">
					<input type="text" name="CelNumber" value="${CelNumber}" valtype="regex:phone"  />
				</span>
			</p>
			<p>
				<label>Telephone Number:</label>
				<span class="field">
					<input type="text" name="Telephone" value="${Telephone}" valtype="regex:phone"  />
				</span>
			</p>
			<p>
				<label>Office Number:</label>
				<span class="field">
					<input type="text" name="OfficeNumber" value="${OfficeNumber}" valtype="regex:phone"  />
				</span>
			</p>
						
            </div>
				<div style="float:right;"><button type="button" class="submitbutton" dataid="${AgentID}" name="topSave">Save Agent</button></div>
        </div>
        </div>
		
    </script>

	<!-- main tab headers -->
	<ul class="hornav">
		<li><a href="#add">
			<img class='tabIcon' src='/Theme/Images/addfolder.png' />&nbsp;</a></li>
		<li class="current">
			<a href="#grid">
				<img class="loader" style="display: none;" src="/Theme/images/loaders/loader2.gif" alt="" />All Agencies</a>
		</li>
	</ul>
	<!-- end main tab headers -->
	<%-- begin content wrapper --%>
	<div id="contentwrapper" class="contentwrapper">
		<%-- begin grid content --%>
		<div id="grid" class="subcontent">
			<div class="tableoptions">
				<table>
					<tr>
						<td>
							<select id="AgencyID" name="AgencyID" data-placeholder="Choose a Agency..." class="chzn-select" style="width: 250px;" tabindex="2" ></select></td>
						<td width="10">&nbsp;</td>
						<td>
							<button id="addNewAgency">Add New Agency</button></td>
					</tr>
				</table>
			</div>
			<table cellpadding="0" cellspacing="0" border="0" id="agentTable" class="stdtable">
				<thead>
					<tr>
						<th class="head0">Name</th>
						<th class="head1">Email</th>
						<th class="head0">Cell Number</th>
						<th class="head0">Function</th>
					</tr>
				</thead>
				<tbody id="agentTemplateContainer">
				</tbody>
			</table>

		</div>
		<%-- end grid content --%>
		<div id="add" class="subcontent" style="display: none">
		</div>

		<div id="user-toolbar-options" style="display: none">
			<a href="#"><i class="icon-user"></i></a>
			<a href="#"><i class="icon-star"></i></a>
			<a href="#"><i class="icon-edit"></i></a>
			<a href="#"><i class="icon-delete"></i></a>
			<a href="#"><i class="icon-ban"></i></a>
		</div>
	</div>
	<!--contentwrapper-->

	<div id="addAgencyDialog" style="display:none;">
		Agency Name:&nbsp;&nbsp;<input type="text" name="Name" />
	</div>

</asp:Content>
