﻿/*global Globals,Utils,Entities,jAlert,MaintenanceService,jConfirm*/

var Agency = {
	addTab: function () {
		var $addTab = $("#add");
		var agentEntity = new Entities.Agent();

		Utils.bindDataToGrid($addTab.selector, "#AgentTabTemplate", agentEntity);
		Utils.tabs.initTab("#add");

		$addTab.block();

		$.when(Agency.getAgencies('#add')).then(function () {
			$addTab.unblock();
			$.uniform.update();
		});
	},
	loadEditTab: function (agentID) {
		if (!isNaN(agentID)) {
			var agentResponse = function (_result) {
				if (_result.EntityList[0] !== null) {
					Utils.tabs.loadTab(_result.EntityList[0].AgentFirstName + ' ' + _result.EntityList[0].AgentLastName, agentID, function () {
						var $agentTab = Utils.tabs.getCurrentTab();

						Utils.bindDataToGrid($agentTab.selector, "#AgentTabTemplate", _result.EntityList[0]);
						Utils.tabs.initTab($agentTab.selector);

						$agentTab.block();

						//make calls to retrieve related data for the contractor
						$.when(Agency.getAgencies($agentTab.selector)).then(function () {
							$agentTab.mapObjectTo(_result.EntityList[0], "name", false);
							$agentTab.find('select[name=AgencyID]').trigger("liszt:updated");
							$.uniform.update();
							$agentTab.unblock();
						});

					}, true, true);

				} else {
					$.jGrowl("Record could not be loaded.");
				}
			};
			MaintenanceService.WCFMaintenance.GetSalesAgent(agentID, agentResponse, Utils.responseFail);
		}
	},
	getAgencies: function ($tab) {
		var dfd = new $.Deferred();
		//gets all the required items for the contractors tab, it populates the category dropdowns
		var response = function (_result) {
			if (_result.Count > 0) {
				var elementSelector = $tab + " select[name=AgencyID]";
				Utils.bindDataToGrid(elementSelector, "#AgencyDDL", _result.EntityList);
				$(elementSelector).prepend("<option value='-1'>Choose an agency</option>");
				//update multi select
				$(elementSelector).trigger("liszt:updated");
				dfd.resolve();
			}
		};

		MaintenanceService.WCFMaintenance.GetSalesAgencies(response, function (e) { dfd.reject(); Utils.responseFail(e); });
		return dfd.promise();
	},
	getAgents: function (agencyID) {
		
		var $currentTab = Utils.tabs.getCurrentTab();

		var response = function (_result) {
			$currentTab.unblock();

			Utils.bindDataToGrid("#agentTemplateContainer", "#AgentGridTemplate", _result.EntityList);
		};

		$currentTab.block();
		MaintenanceService.WCFMaintenance.GetSalesAgents(agencyID, response, Utils.responseFail);
	},
	initAddAgencyDialog: function () {
		//init the dialog
		$("#addAgencyDialog").dialog({
			resizable: false,
			modal: true,
			width: "350px",
			title: "Add a new agency",
			autoOpen: false,
			buttons: {
				"Add Agency": function () {
					
					var agencyEntity = new Entities.SalesAgent();
					agencyEntity.ID = 0;
					agencyEntity.Name = $("#addAgencyDialog input[name=Name]").val();

					var response = function (result) {
						$("#addAgencyDialog").unblock();

						if (result.EntityList[0].success) {
							
							$("#grid").block();
							$("#addAgencyDialog").dialog("close");
							$("#addAgencyDialog input[name=Name]").val("");

							$.when(Agency.getAgencies('#grid')).then(function () {
								$("#grid").unblock();
							});

						} else {
							jAlert(result.EntityList[0].message,"Error during agency add");
						}
						
					};

					$("#addAgencyDialog").block();
					MaintenanceService.WCFMaintenance.UpdateSalesAgency(agencyEntity, response, Utils.responseFail);
					
				},
				Cancel: function () {
					$(this).dialog("close");
				}
			}
		});
	},
	openAddAgencyDialog: function () {

	},
	updateAgent: function (e) {
		e.preventDefault();

		var $currentTab = Utils.tabs.getCurrentTab();

		var agent = new Entities.Agent();

		if (Utils.validation.validateForm($currentTab.selector, "There are missing required fields", false, true)) {

			$currentTab.mapToObject(agent, "name");

			var response = function (_result) {
				$currentTab.unblock();

				if (_result.EntityList[0] !== 0) {
					jAlert("The agent has been saved", "Success");
					//clear the html
					$("#add").html("");
					//got to all agents tab
					Utils.tabs.selectTab("#grid");

				} else {
					jAlert("There was an error when attempting to save the agent.", "Error");
				}
			};

			$currentTab.block();
			MaintenanceService.WCFMaintenance.UpdateSalesAgent(agent, response, Utils.responseFail);
			//ContractorService.WCFContractor.UpdateContractor(contractor, $infoTab.find("select[name=ContractorSubCategory]").val(), contactPersons, response, Utils.responseFail);
		}
	}
};

var PageEvents = {
	load: function () {
		PageEvents.bindEvents();
		Agency.getAgencies('#grid');
		Agency.getAgents();
		Agency.initAddAgencyDialog();
	},
	bindEvents: function () {
		$('.bodywrapper').on('Honeycomb.tabLoad', '#grid', PageEvents.agencyGridTab_click);
		$('.bodywrapper').on('Honeycomb.tabLoad', '#add', Agency.addTab);
		$('.bodywrapper').on('change', '#AgencyID', PageEvents.categoryOnChange);
		$('.bodywrapper').on('click', "#addNewAgency", PageEvents.addNewAgency_CLICK);
		$('.bodywrapper').on('click.Save', "button[name=topSave]", Agency.updateAgent);

		$('.bodywrapper').on('click.editAgent', '.editAgent', PageEvents.agentEdit_click);
	},
	categoryOnChange : function(e) {
		e.preventDefault();
		var agencyID = $('#AgencyID').val();
		if (agencyID == -1) {
			agencyID = null;
		}
		Agency.getAgents(agencyID);
	},
	agencyGridTab_click: function (e) {
		var agencyID = $('#AgencyID').val();
		if (agencyID == -1) {
			agencyID = null;
		}
		Agency.getAgents(agencyID);
	},
	agentEdit_click: function (e) {
		e.preventDefault();
		Agency.loadEditTab($(this).attr("agentid"));
	},
	addNewAgency_CLICK: function (e) {
		e.preventDefault();
		$("#addAgencyDialog").dialog("open");
	}
};

//main page load event
$(function () {
	PageEvents.load();
});