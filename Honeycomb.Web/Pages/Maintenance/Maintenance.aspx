﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Honeycomb.Master" AutoEventWireup="true" CodeBehind="Maintenance.aspx.cs" Inherits="Honeycomb.Web.Pages.Maintenance.Maintenance" %>

<%--User Controls--%>
<%@ Register Src="~/UserControls/MaintenanceLookUpOptionTemplate.ascx" TagPrefix="lkUp" TagName="MaintenanceLookUpOptionTemplate" %>


<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">

    <%--references--%>
    <script type="text/javascript" src="/Scripts/plugins/chosen.jquery.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/charCount.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.tagsinput.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.validate.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.tmpl.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.smartWizard-2.0.min.js"></script>
    <script type="text/javascript" src="/Scripts/custom/tables.js"></script>
    <script type="text/javascript" src="/Scripts/custom/forms.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.validate.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.alerts.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.autogrow-textarea.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/ui.spinner.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/header.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/util.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/button.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/handler.base.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/handler.form.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/handler.xhr.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/uploader.basic.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/dnd.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/uploader.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/jquery-plugin.js"></script>
    <script type="text/javascript" src="/Services/WCFMaintenance.svc/js"></script>
    <!--<script type="text/javascript" src="/Scripts/occupant.js"></script>
    <script type="text/javascript" src="/Scripts/Security.js"></script>
    <script type="text/javascript" src="/Scripts/Staff.js"></script>-->
    <script type="text/javascript" src="Maintenance.aspx.js"></script>
    <link href="/Scripts/plugins/fileupload/fineuploader.css" rel="stylesheet" type="text/css" />

    <%-- Templates --%>
    <%-- render the templates required for lookUp Templates --%>
    <script id="MaintenanceLookUpTemplateTable" type="text/x-jquery-tmpl">
        <table class="options">
            ${lookuptypename}
            <div class="right" align="right"><a href="#" lookUpTypeID="${lookUpTypeID}" class="btn btn3 btn_search editlookup" style="margin-right: 10px;"></a><a href="#" lookUpTypeID="${lookUpTypeID}" class="btn btn3 btn_trash deletelookup"></a></div>
        </table>
    </script>

    <script id="LookUpOptionCreateTemplate" type="text/x-jquery-tmpl">
        <form>
        <table>
            <tr>
                <td>Title</td>
                <td><input type="text" name="Name" value="" valtype="required;"/></td>
            </tr>
            <tr>
                <td>Description</td>
                <td><input type="text" name="Description" value="" valtype="required;"/></td>
            </tr>
        </table>
        </form>
    </script>

    <script id="newOptionTemplate" type="text/x-jquery-tmpl">    
        <div style='display: table-row; width: 600px;' id="Div1">
            <div style="display: table-cell">
                <input type="text" name="newItem" value="" savetype="autosave" dataid="0" style="width: 400px;" />
            </div>
            <div style="display: table-cell; width: 200px;vertical-align: bottom;" class="right" align="right">
                <a href="#" lookUpTypeID="0" class="btn btn3 btn_search saveLookUp" style="margin-right: 10px; background-position: -85px -480px;"></a>
                <span style="width: 36px;display: inline-block;"></span>
            </div>
            <input type="hidden" name="ID" value="" />
        </div>
    </script>

    <div class="lookUpOptions">
        <script id="MaintenanceLookUpTemplate" type="text/x-jquery-tmpl">
            <div style='display: table-row; width: 600px;' id="{ID}Tabs">
                <div style="display: table-cell">
                    <input type="text" name="${lookUpTypeName}" value="${lookUpTypeName}" savetype="autosave" dataid="${lookUpTypeID}" style="width: 400px;" />
                </div>
                <div style="display: table-cell; width: 200px;vertical-align: bottom;" class="right" align="right">
                    <a href="#" lookUpTypeID="${lookUpTypeID}" class="btn btn3 btn_search saveLookUp" style="margin-right: 10px; background-position: -85px -480px;"></a>
                    <a href="#" lookUpTypeID="${lookUpTypeID}" class="btn btn3 btn_trash deleteLookUpOption"></a>
                </div>
                <input type="hidden" name="ID" value="${lookUpTypeID}" />
            </div>
        </script>
    </div>

        <script id="maintenenceTemplate" type="text/x-jquery-tmpl">
            <tr>
                <td>${lookUpTypeName}</td>
                <td>${lookUpDescription}</td>
                <td>${lookUpName}</td>
                <td class="right" align="right"><a href="#" lookupid="${lookUpID}" data-lookupheader="${lookUpTypeName}" class="btn btn3 btn_search editLookUp" style="margin-right: 10px;"></a></td>
            </tr>
        </script>
       
        <div class="options" style='width: 600px'>
        </div>
        <!--pageheader-->
        <ul class="hornav">
            <li><a href="#add">
                <img class='tabIcon' src='/Theme/Images/addfolder.png' />&nbsp;</a></li>
            <li class="current"><a href="#grid">
                <img class="loader" style="display: none;" src="/Theme/images/loaders/loader2.gif" alt="" />Look UP Options</a></li>
        </ul>
        <!-- modal -->


        <div id="contentwrapper" class="contentwrapper">
            <div id="grid" class="subcontent">

                <div id="Tabs" class="tabs">
                    <ul>
                        <li><a href="#tabs-1">Look Up Lists</a></li>
                    </ul>
                    <div id="tabs-1">
                        <div id="editLookUpContainer" style=""></div>

                        <div id='editForm' style="display: none;">
                            <p>
                                <label>LookUp Type</label>
                                <select name="LookUpID" id="LookUpID" data-placeholder="Select LookUpOption..." style="min-width: 25%!important;" class="uniformselect">
                                    <option value="null">Select Option Type</option>
                                </select>
                            </p>
                            <input type="text" name="OptionName" placeholder="<option Name....>" style="width: 20%;" required>
                        </div>

                        <div id="GridContainer">

                            <div class="tableoptions">
                                Search
                                <input type="text" name="filter" id="filterSR" placeholder="LookUp Option" class="smallinput radius3" style="width: 200px;" />
                                &nbsp;
                                <button type="button" id="searchSRButt" class="radius2">Search</button>
                            </div>
                            <table cellpadding="0" cellspacing="0" border="0" id="MaintenanceTable" class="stdtable">
                                <thead>
                                    <tr>
                                        <th class="head0" sortcolumn="lookUpTypeName">LookUp Type</th>
                                        <th class="head1" sortcolumn="lookUpDescription">Description</th>
                                        <th class="head1" sortcolumn="lookUpName">Example</th>
                                        <th class="head1" sortcolumn="lookUpName">Function</th>
                                    </tr>
                                </thead>
                                <tbody id="MaintenanceTemplateContainer">
                                </tbody>
                            </table>
                            <div class="dataTables_paginate">
                                <div id="MaintantencePagination"></div>
                            </div>
                        </div>
                    </div>

                   
                </div>
                <!-- #tabs -->
            </div>
            <div id="add" class="subcontent" style="display: none">
            </div>

        </div>
        <!--contentwrapper-->
</asp:Content>
