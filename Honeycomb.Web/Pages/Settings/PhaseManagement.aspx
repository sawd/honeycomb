﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Honeycomb.Master" AutoEventWireup="true" CodeBehind="PhaseManagement.aspx.cs" Inherits="Honeycomb.Web.Pages.Settings.PhaseManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
	<%-- References --%>
	<script type="text/javascript" src="/Scripts/plugins/jquery.tagsinput.min.js"></script>
	<script type="text/javascript" src="/Scripts/plugins/chosen.jquery.js"></script>
	<script type="text/javascript" src="/Scripts/plugins/jquery.uniform.min.js"></script>
	<script type="text/javascript" src="/Scripts/plugins/jquery.validate.min.js"></script>
	<script type="text/javascript" src="/Scripts/plugins/jquery.tmpl.min.js"></script>
	<script type="text/javascript" src="/Scripts/custom/tables.js"></script>
	<script type="text/javascript" src="/Scripts/custom/forms.js"></script>
	<script type="text/javascript" src="/Scripts/plugins/jquery.alerts.js"></script>
	<script type="text/javascript" src="/Scripts/plugins/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="/Scripts/custom/jquery-honeycomb.js"></script>
	<script type="text/javascript" src="/Services/WCFMaintenance.svc/js"></script>
	<script type="text/javascript" src="PhaseManagement.aspx.js"></script>

	<%-- grid template --%>

	<script id="PhaseDDL" type="text/x-jquery-tmpl">
        <option value="${ID}">${Name}</option>
    </script>
	<script id="PhaseGridTemplate" type="text/x-jquery-tmpl">
		<tr>
			<td>${Name}</td>
			<td>${Description}</td>
			<td>${MembershipCompulsory}</td>
			<td class="right" align="right"><a href="#" agentID="${ID}" class="btn btn3 btn_pencil editPhase" style="margin-right: 10px;"></a></td>
		</tr>
	</script>

	    <%-- phase tab template --%>
    <script id="PhaseTabTemplate" type="text/x-jquery-tmpl">
        <div class="pageheader nopadding">
            <input type="hidden" name="ID" value="${ID}" />
        </div>
		<div class="stdform">
			<div class="one_half nomargin">
				<div class="contentBlock" style="margin-bottom:10px;">
					<div class="contenttitle2">
						<h3>Phase Details</h3>
					</div>
			<p>
				<label>Phase Name:</label>
				<span class="field">
					<input type="text" name="Name" value="${Name}" valtype="required;"  />
				</span>
			</p>
			<p>
				<label>Phase Description:</label>
				<span class="field">
					<input type="text" name="Description" value="${Description}" valtype="required;"  />
				</span>
			</p>
			<p>
				<label>Compulsory Membership:</label>
				<span class="field">
					<input type="checkbox" name="MembershipCompulsory" value="${MembershipCompulsory}" />
				</span>
			</p>
			
            </div>
				<div style="float:right;"><button type="button" class="submitbutton" dataid="${ID}" name="topSave">Save Phase</button></div>
        </div>
        </div>
		
    </script>

	<!-- main tab headers -->
	<ul class="hornav">
		<li><a href="#add">
			<img class='tabIcon' src='/Theme/Images/addfolder.png' />&nbsp;</a></li>
		<li class="current">
			<a href="#grid">
				<img class="loader" style="display: none;" src="/Theme/images/loaders/loader2.gif" alt="" />All Phases</a>
		</li>
	</ul>
	<!-- end main tab headers -->
	<%-- begin content wrapper --%>
	<div id="contentwrapper" class="contentwrapper">
		<%-- begin grid content --%>
		<div id="grid" class="subcontent">

			<table cellpadding="0" cellspacing="0" border="0" id="agentTable" class="stdtable">
				<thead>
					<tr>
						<th class="head0">Phase Name</th>
						<th class="head1">Phase Description</th>
						<th class="head0">Is Phase Compulsory</th>
						<th class="head0">Edit Phase</th>
					</tr>
				</thead>
				<tbody id="PhaseTemplateContainer">
				</tbody>
			</table>

		</div>
		<%-- end grid content --%>
		<div id="add" class="subcontent" style="display: none">
		</div>

		<div id="user-toolbar-options" style="display: none">
			<a href="#"><i class="icon-user"></i></a>
			<a href="#"><i class="icon-star"></i></a>
			<a href="#"><i class="icon-edit"></i></a>
			<a href="#"><i class="icon-delete"></i></a>
			<a href="#"><i class="icon-ban"></i></a>
		</div>
	</div>
	<!--contentwrapper-->

</asp:Content>
