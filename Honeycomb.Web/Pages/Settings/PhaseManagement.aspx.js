﻿/*global Globals,Utils,Entities,jAlert,MaintenanceService,jConfirm*/

var Phase = {
    addTab: function () {
        var $addTab = $("#add");
        var phaseEntity = new Entities.Phase();

        Utils.bindDataToGrid($addTab.selector, "#PhaseTabTemplate", phaseEntity);
        Utils.tabs.initTab("#add");

        $addTab.block();

        $.when(Phase.getPhases('#add')).then(function () {
            $addTab.unblock();
            $.uniform.update();
        });
    },
    loadEditTab: function (agentID) {
        if (!isNaN(agentID)) {
            var agentResponse = function (_result) {
                if (_result.EntityList[0] !== null) {
                    Utils.tabs.loadTab(_result.EntityList[0].ID + ' ' + _result.EntityList[0].Name, agentID, function () {
                        var $agentTab = Utils.tabs.getCurrentTab();

                        Utils.bindDataToGrid($agentTab.selector, "#PhaseTabTemplate", _result.EntityList[0]);
                        Utils.tabs.initTab($agentTab.selector);

                        $agentTab.block();

                        //make calls to retrieve related data for the contractor
                        $.when(Phase.getPhases($agentTab.selector)).then(function () {
                            $agentTab.mapObjectTo(_result.EntityList[0], "name", false);
                            $agentTab.find('select[name=ID]').trigger("liszt:updated");
                            $.uniform.update();
                            $agentTab.unblock();
                        });

                    }, true, true);

                } else {
                    $.jGrowl("Record could not be loaded.");
                }
            };
            MaintenanceService.WCFMaintenance.GetPhase(agentID, agentResponse, Utils.responseFail);
        }
    },
    getPhases: function ($tab) {

        var dfd = new $.Deferred();
        var $currentTab = Utils.tabs.getCurrentTab();

        var response = function (_result) {
            $currentTab.unblock();

                if (_result.Count > 0) {
                    var elementSelector = $tab + " select[name=ID]";
                    //Utils.bindDataToGrid(elementSelector, "#PhaseDDL", _result.EntityList);
                    $(elementSelector).prepend("<option value='-1'>Choose an agency</option>");
                    //update multi select
                    $(elementSelector).trigger("liszt:updated");
                    dfd.resolve();
                }

                Utils.bindDataToGrid("#PhaseTemplateContainer", "#PhaseGridTemplate", _result.EntityList);
        };

        //$currentTab.block();
        MaintenanceService.WCFMaintenance.GetPropertyPhase(response, Utils.responseFail);
        return dfd.promise();
    },
    getMorePhases: function (phaseID) {

        var $currentTab = Utils.tabs.getCurrentTab();

        var response = function (_result) {
            $currentTab.unblock();

            Utils.bindDataToGrid("#PhaseTemplateContainer", "#PhaseGridTemplate", _result.EntityList);
        };

        $currentTab.block();
        MaintenanceService.WCFMaintenance.GetPropertyPhase(response, Utils.responseFail);
    },

    updatePhase: function (e) {
        e.preventDefault();

        var $currentTab = Utils.tabs.getCurrentTab();

        var phase = new Entities.Phase();

        if (Utils.validation.validateForm($currentTab.selector, "There are missing required fields", false, true)) {

            $currentTab.mapToObject(phase, "Name");

            var response = function (_result) {
                $currentTab.unblock();

                if (_result.EntityList[0] !== 0) {
                    jAlert("The phase has been saved", "Success");
                    //clear the html
                    $("#add").html("");
                    //got to all agents tab
                    Utils.tabs.selectTab("#grid");

                } else {
                    jAlert("There was an error when attempting to save the agent.", "Error");
                }
            };

            $currentTab.block();
            MaintenanceService.WCFMaintenance.UpdatePhase(phase, response, Utils.responseFail);
            //ContractorService.WCFContractor.UpdateContractor(contractor, $infoTab.find("select[name=ContractorSubCategory]").val(), contactPersons, response, Utils.responseFail);
        }
    }
};

var PageEvents = {
    load: function () {
        PageEvents.bindEvents();
        Phase.getPhases('#grid');
        Phase.getMorePhases();
    },

    bindEvents: function () {
        $('.bodywrapper').on('Honeycomb.tabLoad', '#grid', PageEvents.phaseGridTab_click);
        $('.bodywrapper').on('Honeycomb.tabLoad', '#add', Phase.addTab);
        $('.bodywrapper').on('click.Save', "button[name=topSave]", Phase.updatePhase);
        $('.bodywrapper').on('click.editPhase', '.editPhase', PageEvents.phaseEdit_click);
    },

    phaseGridTab_click: function (e) {
        var phaseID = $('#ID').val();
        if (phaseID == -1) {
            phaseID = null;
        }
        Phase.getMorePhases(phaseID);
    },
    phaseEdit_click: function (e) {
        e.preventDefault();
        Phase.loadEditTab($(this).attr("agentid"));
    }
};

//main page load event
$(function () {
    PageEvents.load();
});