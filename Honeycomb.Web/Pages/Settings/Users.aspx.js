﻿/*global Utils, SecurityService, jAlert, Entities,jConfirm, jQuery, Globals*/

var Users = {
    deferredSave: new $.Deferred(),
    getUsers: function () {
        //jQuery.jGrowl("Page information loading please wait.");
        var userResponse = function (result, userContext) {
            Users.userPaginate.TableID = "#UserTable";
            Users.userPaginate.PaginationID = "#userPagination";
            Users.userPaginate.TotalCount = result.Count;


            Users.userEntityList = result.EntityList;
            Utils.bindDataToGrid("#usersTemplateContainer", "#usersTemplate", Users.userEntityList);
            Users.userPaginate.bindPagination();

            if ($(".loader").is(":visible")) {
                $(".loader").slideToggle();
            }
        };

        if (!$(".loader").is(":visible")) {
            $(".loader").slideToggle();
        }
        
        SecurityService.WCFSecurity.GetSiteUsers(Users.userPaginate, userResponse, Utils.responseFail);
    },
    validateUser: function (e) {
        var currentTabID = "#" + Utils.tabs.getCurrentTab().attr("id");

        var isValid = Utils.validation.validateForm(currentTabID + " #wiz1step3_1", "Please review the form");
        if (isValid) {
            Users.commitUser();
        }
    },
    commitUser: function () {
        /// <summary>AED of a users information</summary>
      
        var saveResponse = function (result) {
            if (result.Message === "") {
                jAlert("User was saved ,successfully", "Saved");
                var currentTabID = "#" + Utils.tabs.getCurrentTab().attr("id");
                $(currentTabID + " .formwiz").find("input[type=text],input[type=password]").val("");
            }
            else {
                jAlert("There was an error while attempting a save, this may be due to a duplicate username. Please make sure the username you have chosen is unique.", "Error");
            }
        };
        var currentTab = $(".hornav li.current a").attr("href");
        var user = new Entities.User();
        user = Utils.populateEntityFromForm(currentTab, user);
        
        //get the users roles from the select
        var userRolesArr = [];
        var selectedUserRoles = $(currentTab).find(".userPermissions");
        //create entities to send up to wcf service call
        selectedUserRoles.find("option").each(function () {
            userRolesArr[userRolesArr.length] = $(this).val();
        });

        SecurityService.WCFSecurity.UpdateUser(user, userRolesArr, saveResponse, Utils.responseFail);
    },
    deleteUser: function (dataID) {
        jConfirm("Are you sure you want to delete this user", "", function (_result) {
            if (_result) {
                //delete the user
                var deleteResponse = function (_return) {
                    if (_return.Count > 0) {
                        //refresh the users display
                        $.jGrowl("User deleted");
                        Users.getUsers();
                    }
                };
                //call service method
                SecurityService.WCFSecurity.DeleteUser(dataID,deleteResponse,Utils.responseFail);
            }
        });

       
    },
    currentUser: "",
    userEntityList: null,
    userPaginate: null,
    poplateUserTab: function (userID) {
        var response = function (result) {
            var currentTabID = "#" + Utils.tabs.getCurrentTab().attr("id");
            Utils.tabs.bindAndInitTab("userEditTemplate", result.EntityList);
            jQuery(currentTabID + ' #wizard3').smartWizard({ onFinish: Users.validateUser, enableAllSteps: true, labelFinish: 'Save' });


            ///// DUAL BOX /////
            var db = jQuery(currentTabID + ' #dualselect').find('.ds_arrow .arrow');	//get arrows of dual select
            var sel1 = jQuery(currentTabID + ' #dualselect select:first-child');		//get first select element
            var sel2 = jQuery(currentTabID + ' #dualselect select:last-child');			//get second select element

            //sel2.empty(); //empty it first from dom.

            db.click(function () {
                var t = (jQuery(this).hasClass('ds_prev')) ? 0 : 1;	// 0 if arrow prev otherwise arrow next
                if (t) {
                    sel1.find('option').each(function () {
                        if (jQuery(this).is(':selected')) {
                            jQuery(this).attr('selected', false);
                            var op = sel2.find('option:first-child');
                            sel2.append(jQuery(this));
                        }
                    });
                } else {
                    sel2.find('option').each(function () {
                        if (jQuery(this).is(':selected')) {
                            jQuery(this).attr('selected', false);
                            sel1.append(jQuery(this));
                        }
                    });
                }
            });
        };

        SecurityService.WCFSecurity.GetUser(userID, response, Utils.responseFail);
    }
};

var PageEvents = {
    load: function () {

        Users.userPaginate = new Globals.paginationContainer("FirstName", 8, Users.getUsers);

        PageEvents.bindEvents();
        Users.getUsers();

        // Smart Wizard
        jQuery('#wizard3').smartWizard({ onFinish: Users.validateUser, labelFinish: 'Save' });
        $('input:checkbox').uniform();
        $("[href=#grid]").bind("click", Users.getUsers);


        ///// DUAL BOX /////
        var db = jQuery('#dualselect').find('.ds_arrow .arrow');	//get arrows of dual select
        var sel1 = jQuery('#dualselect select:first-child');		//get first select element
        var sel2 = jQuery('#dualselect select:last-child');			//get second select element

        sel2.empty(); //empty it first from dom.

        db.click(function () {
            var t = (jQuery(this).hasClass('ds_prev')) ? 0 : 1;	// 0 if arrow prev otherwise arrow next
            if (t) {
                sel1.find('option').each(function () {
                    if (jQuery(this).is(':selected')) {
                        jQuery(this).attr('selected', false);
                        var op = sel2.find('option:first-child');
                        sel2.append(jQuery(this));
                    }
                });
            } else {
                sel2.find('option').each(function () {
                    if (jQuery(this).is(':selected')) {
                        jQuery(this).attr('selected', false);
                        sel1.append(jQuery(this));
                    }
                });
            }
        });

    },
    bindEvents: function () {
        $("#addUser").bind("click.addUser", PageEvents.addUser_click);
        $("#usersTemplateContainer").on("click.deleteUser", ".deleteUser", PageEvents.deleteUser_click);
        $("#usersTemplateContainer").on("click.btn_search", ".btn_search", PageEvents.editUser_click);
    },
    addUser_click: function (e) {
        $(".hornav li.current a").removeClass("current");
        $(".hornav li a[href=#edit]").click().addClass("current");
    },
    deleteUser_click: function (e) {
        e.preventDefault();
        Users.deleteUser($(this).attr("userid"));
    },
    editUser_click: function (e) {
        e.preventDefault();
        
        Users.currentUser = $(this).attr("userid");

        Utils.tabs.loadTab($(this).attr("tabname"), "usertab-" + $(this).attr("userid"), function () { Users.poplateUserTab(Users.currentUser); });
    }
};

$(function () {
    PageEvents.load();
});