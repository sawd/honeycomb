﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Honeycomb.Master" AutoEventWireup="true" CodeBehind="ErfManagement.aspx.cs" Inherits="Honeycomb.Web.Pages.Settings.ErfManagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
    <%-- References --%>
    <script type="text/javascript" src="/Scripts/plugins/chosen.jquery.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/charCount.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.tagsinput.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.validate.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.tmpl.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.smartWizard-2.0.min.js"></script>
    <script type="text/javascript" src="/Scripts/custom/tables.js"></script>
    <script type="text/javascript" src="/Scripts/custom/forms.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.validate.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.alerts.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.autogrow-textarea.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/ui.spinner.min.js"></script>
    <script type="text/javascript" src="/Scripts/custom/elements.js"></script>
    <script type="text/javascript" src="/Services/WCFProperty.svc/js"></script>
    <script type="text/javascript" src="ErfManagement.aspx.js"></script>
    <%-- Templates --%>
    <script id="ErfTemplate" type="text/x-jquery-tmpl">
        <tr>
            <td>${ErfNumber}</td>
            <td>${Name}</td>
            <td>${ErfType}</td>
            <td class="right" align="right"><a href="#" erfid="${ID}" erfno="${ErfNumber}" class="btn btn3 btn_search editErf" style="margin-right:10px;"></a><a href="#" erfid="${ID}" erfno="${ErfNumber}" class="btn btn3 btn_trash deleteErf"></a></td>
        </tr>
    </script>

    <%-- drop down template --%>
    <script type="text/x-jquery-template" id="ddlTemplate">
        <option value="${ID}">${Name}</option>
    </script>

    <%-- edit erf template --%>
    <script id="ErfEditTemplate" type="text/x-jquery-tmpl">

        <div class="pageheader nopadding">
            <h1 class="pagetitle">Erf No: ${ErfNumber}</h1>
            <div class="topSave">
                <button type="button" class="submitbutton" dataid="${ID}" name="topSave">Save Erf</button>
            </div>
        </div>

        <div class="ui-box">
            <input type="hidden" name="ID" value="0" />
            <div class="contenttitle2">
                <h3>Edit Erf</h3>
            </div><!--contenttitle-->

            <div class="stdform">
                <p>
                    <label>Erf No.</label>
                    <span class="field"><input type="text" name="ErfNumber" value="${ErfNumber}" valtype="required;" class="smallinput" /></span>
                </p>
                        
                <p>
                    <label>Erf Name:</label>
                    <span class="field"><input type="text" name="Name" value="${Name}" class="mediuminput" /></span>
                </p>
                                                
                <p>
                    <label>Description:</label>
                    <span class="field"><textarea name="Description" cols="80" rows="5" class="longinput">${Description}</textarea></span> 
                </p>

                <p>
                    <label>Erf Type:</label>
                    <span class="field"><select name="ErfTypeID" class="uniformselect"></select></span>
                </p>
 
            </div>

        </div>

        <div class="bottomSave">
            <button type="button" class="submitbutton" dataid="${ID}" name="topSave">Save Erf</button>
        </div>
    </script>

    <ul class="hornav">
        <li><a href="#create"><img class='tabIcon' src='/Theme/Images/addfolder.png'/>&nbsp;</a></li>
        <li class="current"><a href="#grid">
            <img class="loader" src="/Theme/images/loaders/loader2.gif" alt="">All Erven</a></li>
    </ul>
    <div id="contentwrapper" class="contentwrapper">
        <div id="grid" class="subcontent">
            <table id="ErfTable" cellpadding="0" cellspacing="0" border="0" class="stdtable stdtablecb">
                <colgroup>
                    <col class="con0" />
                    <col class="con1" />
                    <col class="con0" />
                    <col class="con1" />
                </colgroup>
                <thead>
                    <tr>
                        <th class="head0" sortcolumn="ErfNumber">Erf Number</th>
                        <th class="head1" sortcolumn="Name">Name</th>
                        <th class="head0" sortcolumn="ErfType">Erf Type</th>
                        <th class="head1">Function</th>
                    </tr>
                </thead>
                <tbody id="ErfTemplateContainer">
                </tbody>
            </table>
            <div class="dataTables_paginate">
                <div id="ErfPagination"></div>

            </div>	    
            <br clear="all" />
        </div>
        <%-- add erven tab --%>
        <div id="create" class="subcontent">

        </div>
    </div>
</asp:Content>