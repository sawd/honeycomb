﻿/*global Utils,MaintenanceService,Globals,jAlert,Entities*/

var MessageTemplate = {
	getMessageTemplates: function () {
		var $currentTab = Utils.tabs.getCurrentTab();
		$currentTab.block();
		MaintenanceService.WCFMaintenance.GetMessageTemplates(MessageTemplate.pagination,MessageTemplate.Events.getMessageTemplates_SUCCESS, Utils.responseFail, $currentTab);
	},
	renderMessagesToGrid: function (data, totalCount) {
		//set up pagination
		MessageTemplate.pagination.TableID = "#MessageTemplateTable";
		MessageTemplate.pagination.PaginationID = "#GridPagination";
		MessageTemplate.pagination.TotalCount = totalCount;
		//bind to grid
		Utils.bindDataToGrid("#GridTemplateContainer", "#GridTemplate", data);
		//bind pagination
		MessageTemplate.pagination.bindPagination();
	},
	getMessageTemplate: function (templateID, tabName) {
		var $currentTab = Utils.tabs.getCurrentTab();
		$currentTab.block();
		MaintenanceService.WCFMaintenance.GetMessageTemplate(templateID, MessageTemplate.Events.getMessageTemplate_SUCCESS, Utils.responseFail, $currentTab);
	},
	renderMessageToTab: function (data) {
		if (data !== null) {
			Utils.tabs.loadTab(data.Name, "MessageTemplate" + data.ID, function () {
				var $currentTab = Utils.tabs.getCurrentTab();

				$currentTab.data("templateData", data);				

				Utils.bindDataToGrid($currentTab.selector, "#EditTemplate", data);

				Utils.tabs.initTab($currentTab.selector);

				//if type is sms then add a charactor count to the textarea
				if (data.Type === "SMS") {
					$currentTab.find(".longinput").charCount({
						allowed: 160,
						warning: 10,
						counterText: 'Characters left (may not be accurate due to potential length of placeholders): '
					});
				} else {
					$currentTab.find(".longinput").htmlEditor();
				}

				//render placeholders into template
				if (data.Placeholders !== null) {
					var placeholdersData = $.parseJSON(data.Placeholders);

					for (var i = 0; i < placeholdersData.Placeholders.length; i++) {
						//create new jquery object with which we can interact
						var currentPlaceHolder = $("<a href='javascript:void(0);' class='btn btn_orange btn_flag radius50'><span>" + placeholdersData.Placeholders[i].name + "</span></a>");
						//store the place holder we need to insert into the text area
						currentPlaceHolder.data("placeholder", placeholdersData.Placeholders[i].placeholder);
						//bind a click event handler
						currentPlaceHolder.click(MessageTemplate.Events.placeHolder_CLICK);
						$currentTab.find(".placeholderContainer").append(currentPlaceHolder);
					}
				} else {
					$currentTab.find(".placeholderContainer").append("<strong>none</strong>");
				}
			});
		}
	},
	updateMessageTemplate: function () {
		var $currentTab = Utils.tabs.getCurrentTab();
		var MessageEntity = new Entities.MessageTemplate();
		$currentTab.mapToObject(MessageEntity, "name");
		$currentTab.block();
		MaintenanceService.WCFMaintenance.UpdateMessageTemplate(MessageEntity, MessageTemplate.Events.updateMessageTemplate_SUCCESS, Utils.responseFail , $currentTab);
	},
	validateMessageTemplate: function () {
		var $currentTab = Utils.tabs.getCurrentTab();
		var isValid = true;

		if ($currentTab.find(".longinput").val() === "") {
			jAlert("You cannot have a blank message, please provide a message!", "Validation Error");
			isValid = false;
		}

		return isValid;
	},
	insertPlaceHolder: function (placeHolder) {
		var $currentTab = Utils.tabs.getCurrentTab();
		var $currrentTextarea = $currentTab.find(".longinput");
		var data = $currentTab.data("templateData");

		if (data.Type === "SMS") {
			$currrentTextarea.insertAtCaret(placeHolder).trigger('change');
		} else {
			$currrentTextarea.tinymce().execCommand('mceInsertContent', false, placeHolder);
		}
	},
	Events: {
		getMessageTemplates_SUCCESS: function (result, $context) {
			$context.unblock();
			MessageTemplate.renderMessagesToGrid(result.EntityList, result.Count);
		},
		getMessageTemplate_SUCCESS: function (result, $context) {

			$context.unblock();

			if (result.EntityList !== null) {
				MessageTemplate.renderMessageToTab(result.EntityList[0]);
			}
		},
		updateMessageTemplate_SUCCESS: function (result, $context) {
			$context.unblock();
			if (result.EntityList !== null) {
				if (result.EntityList[0].success) {
					jAlert("Successfully updated the message template", "Update Succcessfull");
				} else {
					jAlert(result.EntityList[0].message, "Error");
				}
			}
		},
		saveMessageTemplate_CLICK: function (e) {
			e.preventDefault();
			if (MessageTemplate.validateMessageTemplate()) {
				MessageTemplate.updateMessageTemplate();
			}
		},
		placeHolder_CLICK: function (e) {
			e.preventDefault();
			MessageTemplate.insertPlaceHolder($(this).data("placeholder"));
		}
	},
	pagination: null
};

var PageEvents = {
	load: function () {
		PageEvents.bindEvents();
		MessageTemplate.pagination = new Globals.paginationContainer("Name", 10, MessageTemplate.getMessageTemplates);
		MessageTemplate.getMessageTemplates();
	},
	bindEvents: function () {
		$('.bodywrapper').on('click.editEntity', '.editEntity', PageEvents.messageTemplateEdit_CLICK);
		$('.bodywrapper').on('click.Save', "button[name=topSave]", MessageTemplate.Events.saveMessageTemplate_CLICK);
	},
	messageTemplateEdit_CLICK: function (e) {
		e.preventDefault();
		if (!isNaN($(this).attr("data-identity"))) {
			MessageTemplate.getMessageTemplate($(this).attr("data-identity"), $(this).attr("data-name"));
		}
	}
};

//custom extentions
$.fn.htmlEditor = function () {

	return this.tinymce({
		// Location of TinyMCE script
		script_url: '/Scripts/plugins/tinymce/tiny_mce.js',

		// General options
		theme: "advanced",
		skin: "themepixels",
		width: "100%",
		height: "300px",
		plugins: "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",
		inlinepopups_skin: "themepixels",
		// Theme options
		theme_advanced_buttons1: "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,outdent,indent,blockquote,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2: "pastetext,pasteword,|,bullist,numlist,|,undo,redo,|,link,unlink,code,|,preview,|,forecolor,backcolor,removeformat,|,charmap",
		theme_advanced_buttons3: "",
		theme_advanced_toolbar_location: "top",
		theme_advanced_toolbar_align: "left",
		theme_advanced_statusbar_location: "bottom",
		theme_advanced_resizing: true,

		// Example content CSS (should be your site CSS)
		content_css: "css/plugins/tinymce.css",

		// Drop lists for link/image/media/template dialogs
		template_external_list_url: "lists/template_list.js",
		external_link_list_url: "lists/link_list.js",
		external_image_list_url: "lists/image_list.js",
		media_external_list_url: "lists/media_list.js",

		// Replace values for the template plugin	
		template_replace_values: {
			username: "Some User",
			staffid: "991234"
		}
	});
	
};

$(function () {
	PageEvents.load();
});