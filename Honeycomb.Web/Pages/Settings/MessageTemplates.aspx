﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Honeycomb.Master" AutoEventWireup="true" CodeBehind="MessageTemplates.aspx.cs" Inherits="Honeycomb.Web.Pages.Settings.MessageTemplates" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
<%-- References --%>
    <script type="text/javascript" src="/Scripts/plugins/chosen.jquery.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/charCount.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.tagsinput.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.validate.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.tmpl.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.smartWizard-2.0.min.js"></script>
    <script type="text/javascript" src="/Scripts/custom/tables.js"></script>
    <script type="text/javascript" src="/Scripts/custom/forms.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.validate.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.alerts.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.autogrow-textarea.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/ui.spinner.min.js"></script>
	<script type="text/javascript" src="/Scripts/plugins/tinymce/jquery.tinymce.js"></script>
    <script type="text/javascript" src="/Scripts/custom/elements.js"></script>
	<script type="text/javascript" src="/Services/WCFMaintenance.svc/js"></script>
	<script type="text/javascript" src="MessageTemplates.aspx.js"></script>

	<style type="text/css">
		.placeholderContainer a.btn_orange {
			background-image:none;
			margin-left:5px;
		}

		.placeholderContainer a.btn span {
			margin-left:0;
		}
	</style>

    <%-- Templates --%>
	<script id="GridTemplate" type="text/x-jquery-tmpl">
		 <tr>
            <td>${Name}</td>
            <td>${Type}</td>
            <td class="right" align="right"><a href="#" data-identity="${ID}" data-name="${Name}" class="btn btn3 btn_search editEntity" style="margin-right:10px;"></a></td>
        </tr>
	</script>

	<script id="EditTemplate" type="text/x-jquery-tmpl">
		<div class="pageheader nopadding">
            <h1 class="pagetitle">Template: ${Name}</h1>
            <div class="topSave">
                <button type="button" class="submitbutton" data-id="${ID}" name="topSave">Save Template</button>
            </div>
        </div>

        <div class="ui-box">
            <input type="hidden" name="ID" value="${ID}" />
			<p style="text-align:center;">Click on a place-holder button to insert it at the blinking cursor's location. Please note the place-holder will be replaced with the actual value when the message is sent.</p>
            <div class="stdform">
				<p>
                    <label>Place Holders:</label>
                    <span class="field placeholderContainer">&nbsp;</span> 
                </p>                                            
                <p>
                    <label>Description:</label>
                    <span class="field"><textarea name="Message" cols="80" rows="10" class="longinput">${Message}</textarea></span> 
                </p>
            </div>

        </div>

        <div class="bottomSave">
            <button type="button" class="submitbutton" data-id="${ID}" name="topSave">Save Template</button>
        </div>
	</script>

	<ul class="hornav">
        <li class="current"><a href="#grid">Message Templates</a></li>
		<%--<li><a href="#notificationLists">Notification Lists</a></li>--%>
    </ul>

    <div id="contentwrapper" class="contentwrapper">
		<%-- begin grid --%>
        <div id="grid" class="subcontent">
            <table id="MessageTemplateTable" cellpadding="0" cellspacing="0" border="0" class="stdtable stdtablecb">
                <colgroup>
                    <col class="con0" />
                    <col class="con1" />
                    <col class="con0" />
                </colgroup>
                <thead>
                    <tr>
                        <th class="head0" sortcolumn="Name">Name</th>
                        <th class="head1" sortcolumn="Type">Type</th>
                        <th class="head0">Function</th>
                    </tr>
                </thead>
                <tbody id="GridTemplateContainer">
                </tbody>
            </table>
			<div class="dataTables_paginate">
                <div id="GridPagination"></div>
            </div>
        </div>
		<%-- end grid --%>
		<%-- notification lists --%>
		<div id="notificationLists" style="display:none;" class="subcontent">Notification lists</div>
    </div>

</asp:Content>
