﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Honeycomb.Custom.Data;
using Honeycomb.Custom;

namespace Honeycomb.Web.Pages.Settings {
	public partial class SystemSettings : Custom.BaseClasses.BasePage {
		protected void Page_Load(object sender, EventArgs e) {

			if(!IsPostBack) {
				SystemSetting settings = new SystemSetting();

				using(var context = new Honeycomb_Entities()) {
					settings = context.SystemSetting.Where(s => s.SystemID == CacheManager.SystemID).FirstOrDefault();
				}

				if(settings != null) {
					SendCodeToOccupant.Checked = settings.SendCodeToOccupant;
					SendCodeToGuest.Checked = settings.SendCodeToGuest;
					FromEmailAddress.Value = settings.FromEmailAddress ?? "";
					OccupantsPerRoomLimit.Value = settings.OccupantsPerRoomLimit.ToString();

                    DogsLimit.Value = settings.DogsLimit.ToString();
                    CatsLimit.Value = settings.CatsLimit.ToString();

					ResidentialGolfSubs.Value = settings.ResidentialGolfSubs.HasValue ? settings.ResidentialGolfSubs.Value.ToString("0.00") : "0";
					EstateLevy.Value = settings.EstateLevy.HasValue ? settings.EstateLevy.Value.ToString("0.00") : "0";
					LandMaintenanceLevy.Value = settings.LandMaintenanceLevy.HasValue ? settings.LandMaintenanceLevy.Value.ToString("0.00") : "0";
					ClubFamilyMembership.Value = settings.ClubFamilyMembership.HasValue ? settings.ClubFamilyMembership.Value.ToString("0.00") : "0";

                    PrincipalGolfMembershipRate.Value = settings.PrincipalGolfMembershipRate.HasValue ? settings.PrincipalGolfMembershipRate.Value.ToString("0.00") : "0";
                    SecondaryGolfMembershipRate.Value = settings.SecondaryGolfMembershipRate.HasValue ? settings.SecondaryGolfMembershipRate.Value.ToString("0.00") : "0";
                    JuniorGolfMembershipRate.Value = settings.JuniorGolfMembershipRate.HasValue ? settings.JuniorGolfMembershipRate.Value.ToString("0.00") : "0";
                    PrincipalExternalMember.Value = settings.PrincipalExternalMember.HasValue ? settings.PrincipalExternalMember.Value.ToString("0.00") : "0";
                    SecondaryExternalMember.Value = settings.SecondaryExternalMember.HasValue ? settings.SecondaryExternalMember.Value.ToString("0.00") : "0";

					FromCellNumber.Value = settings.FromCellNumber ?? "";
					AccessCodeExpire.Checked = settings.AccessCodeExpire;
					AccessCodeVaildPeriod.Value = settings.AccessCodeVaildPeriod.ToString();
					SendCodeToOccupantIntranet.Checked = settings.SendCodeToOccupantIntranet;
					SendCodeToGuestIntranet.Checked = settings.SendCodeToGuestIntranet;
					SendCodeXDaysBefore.Value = settings.SendCodeXDaysBefore.ToString();
					BookingsEmailRecipient.Value = settings.BookingsEmailRecipient ?? "";
					IsSupplierGateRestricted.Checked = settings.IsSupplierGateRestricted;
				}
			}
		}

		protected void SaveSettings_ServerClick(object sender, EventArgs e) {
			
			using(var context = new Honeycomb_Entities()){
				var settingsDB = context.SystemSetting.Where(s => s.SystemID == CacheManager.SystemID).FirstOrDefault();

				if(settingsDB != null) {
					settingsDB.SendCodeToOccupant = SendCodeToOccupant.Checked;
					settingsDB.SendCodeToGuest = SendCodeToGuest.Checked;
					settingsDB.FromEmailAddress = FromEmailAddress.Value;
					settingsDB.ResidentialGolfSubs = !string.IsNullOrEmpty(ResidentialGolfSubs.Value) ? Decimal.Parse(ResidentialGolfSubs.Value) : 0;
					settingsDB.EstateLevy = !string.IsNullOrEmpty(EstateLevy.Value) ? Decimal.Parse(EstateLevy.Value) : 0;
					settingsDB.LandMaintenanceLevy = !string.IsNullOrEmpty(LandMaintenanceLevy.Value) ? Decimal.Parse(LandMaintenanceLevy.Value) : 0;
					settingsDB.ClubFamilyMembership = !string.IsNullOrEmpty(ClubFamilyMembership.Value) ? Decimal.Parse(ClubFamilyMembership.Value) : 0;
					settingsDB.FromCellNumber = FromCellNumber.Value;
					settingsDB.OccupantsPerRoomLimit = !string.IsNullOrEmpty(OccupantsPerRoomLimit.Value) ? int.Parse(OccupantsPerRoomLimit.Value) : 0;

                    settingsDB.DogsLimit = !string.IsNullOrEmpty(DogsLimit.Value) ? int.Parse(DogsLimit.Value) : 0;
                    settingsDB.CatsLimit = !string.IsNullOrEmpty(CatsLimit.Value) ? int.Parse(CatsLimit.Value) : 0;

                    settingsDB.PrincipalGolfMembershipRate = !string.IsNullOrEmpty(PrincipalGolfMembershipRate.Value) ? Decimal.Parse(PrincipalGolfMembershipRate.Value) : 0;
                    settingsDB.SecondaryGolfMembershipRate = !string.IsNullOrEmpty(SecondaryGolfMembershipRate.Value) ? Decimal.Parse(SecondaryGolfMembershipRate.Value) : 0;
                    settingsDB.JuniorGolfMembershipRate = !string.IsNullOrEmpty(JuniorGolfMembershipRate.Value) ? Decimal.Parse(JuniorGolfMembershipRate.Value) : 0;
                    settingsDB.PrincipalExternalMember = !string.IsNullOrEmpty(PrincipalExternalMember.Value) ? Decimal.Parse(PrincipalExternalMember.Value) : 0;
                    settingsDB.SecondaryExternalMember = !string.IsNullOrEmpty(SecondaryExternalMember.Value) ? Decimal.Parse(SecondaryExternalMember.Value) : 0;

					settingsDB.AccessCodeExpire = AccessCodeExpire.Checked;
					settingsDB.AccessCodeVaildPeriod = !string.IsNullOrEmpty(AccessCodeVaildPeriod.Value) ? int.Parse(AccessCodeVaildPeriod.Value) : 24;
					settingsDB.SendCodeToOccupantIntranet = SendCodeToOccupantIntranet.Checked;
					settingsDB.SendCodeToGuestIntranet = SendCodeToGuestIntranet.Checked;
					settingsDB.SendCodeXDaysBefore = !string.IsNullOrEmpty(SendCodeXDaysBefore.Value) ? int.Parse(SendCodeXDaysBefore.Value) : 7;
					settingsDB.IsSupplierGateRestricted = IsSupplierGateRestricted.Checked;

					context.SaveChanges();

					CacheManager.SystemSetting = settingsDB;
				}
			}

		}

	}
}