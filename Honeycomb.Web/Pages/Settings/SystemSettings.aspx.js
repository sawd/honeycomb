﻿/*global __doPostBack,Utils*/
var SystemSettings = {
	init: function () {
		//init tabs and trigger tabload event on select
		$('#SettingsTabs').tabs({
			select: function (event, ui) {
				$(ui.panel).trigger("Honeycomb.tabLoad");
			}
		});

		$(".saveSettings").attr("onclick","");
		$(".saveSettings").click(SystemSettings.events.saveSettings_CLICK);
	},
	saveSettings: function () {
		if (Utils.validation.validateForm("#contentwrapper", "There are missing system setting fields, please check the form and do the needful", false, true)) {
			//do a microsoft postback so that our server side method is called.
			__doPostBack('ctl00$SiteContent$SaveSettings', '');
		}
	},
	events: {
		saveSettings_CLICK: function (e) {
			e.preventDefault();
			SystemSettings.saveSettings();
		}
	}
};

var Page = {
	load: function () {
		SystemSettings.init();
	}
};

$(function () {
	Page.load();
});