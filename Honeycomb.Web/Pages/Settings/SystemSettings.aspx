﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Honeycomb.Master" AutoEventWireup="true" CodeBehind="SystemSettings.aspx.cs" Inherits="Honeycomb.Web.Pages.Settings.SystemSettings" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
	<%-- References --%>
	<script type="text/javascript" src="/Scripts/plugins/chosen.jquery.min.js"></script>
	<script type="text/javascript" src="/Scripts/plugins/charCount.js"></script>
	<script type="text/javascript" src="/Scripts/plugins/jquery.tagsinput.min.js"></script>
	<script type="text/javascript" src="/Scripts/plugins/jquery.uniform.min.js"></script>
	<script type="text/javascript" src="/Scripts/plugins/jquery.validate.min.js"></script>
	<script type="text/javascript" src="/Scripts/plugins/jquery.tmpl.min.js"></script>
	<script type="text/javascript" src="/Scripts/plugins/jquery.smartWizard-2.0.min.js"></script>
	<script type="text/javascript" src="/Scripts/custom/tables.js"></script>
	<script type="text/javascript" src="/Scripts/custom/forms.js"></script>
	<script type="text/javascript" src="/Scripts/plugins/jquery.validate.min.js"></script>
	<script type="text/javascript" src="/Scripts/plugins/jquery.alerts.js"></script>
	<script type="text/javascript" src="/Scripts/plugins/jquery.autogrow-textarea.js"></script>
	<script type="text/javascript" src="/Scripts/plugins/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="/Scripts/custom/elements.js"></script>
	<script type="text/javascript" src="/Services/WCFAdmin.svc/js"></script>
	<script type="text/javascript" src="SystemSettings.aspx.js"></script>
	<%-- Templates --%>

	<!--pageheader-->

	<div id="contentwrapper" class="contentwrapper">
		<div class="pageheader nopadding">
			<h1 class="pagetitle" style="margin-left: 0; margin-bottom: 20px;">System Settings</h1>
			<div class="topSave">
				<button type="button" runat="server" id="SaveSettings" onserverclick="SaveSettings_ServerClick" class="submitbutton saveSettings" name="topSave">Save Settings</button>
			</div>
		</div>

		<div id="grid" class="subcontent">

			<div id="SettingsTabs">
				<ul>
					<li><a href="#StandardSettings">Standard Settings</a></li>
					<li><a href="#AccessSettings">Access Settings</a></li>
					<%--<li><a href="#CachingSettings">Caching Options</a></li>--%>
				</ul>
				<div id="StandardSettings" class="standardtab current">

					<div class="stdform">

						<div class="contentBlock" style="margin-bottom: 10px;">

                        

							<div style="width: 600px;">
								<div class="contenttitle2">
									<!--<h3>Standard Settings</h3>-->
                                    <h3>Default Contacts</h3>
								</div>

                                <p>
									<label>From Cell No.:</label>
									<span class="field">
										<input type="text" name="FromCellNumber" id="FromCellNumber" runat="server" class="smallinput" valtype="required;regex:int" maxlength="50" /></span>
								</p>

								<p>
									<label>Default From Email:</label>
									<span class="field">
										<input type="text" name="FromEmailAddress" id="FromEmailAddress" clientidmode="Static" runat="server" valtype="required;regex:EMAIL" maxlength="100" /></span>
								</p>

								<p>
									<label>Bookings Email Recipient:</label>
									<span class="field">
										<input type="text" name="BookingsEmailRecipient" id="BookingsEmailRecipient" runat="server" valtype="required;regex:EMAIL" maxlength="100" /></span>
								</p>

                                

                                <div class="contenttitle2">
									<h3>Estate Levies</h3>
								</div>
								<p>
									<label>Estate Levy:</label>
									<span class="field">
										<input type="text" name="EstateLevy" id="EstateLevy" runat="server" class="smallinput" valtype="required;regex:DOUBLE" /></span>
								</p>

								<p>
									<label>Land Maintenance Levy:</label>
									<span class="field">
										<input type="text" name="LandMaintenanceLevy" id="LandMaintenanceLevy" runat="server" class="smallinput" valtype="required;regex:DOUBLE" /></span>
								</p>

                                <div class="contenttitle2">
									<h3>Golf Fees</h3>
								</div>

                                <p>
									<label>Residential Golf Subs:</label>
									<span class="field">
										<input type="text" name="ResidentialGolfSubs" id="ResidentialGolfSubs" runat="server" class="smallinput" valtype="required;regex:DOUBLE" /></span>
								</p>

								<p>
									<label>Club Family Membership:</label>
									<span class="field">
										<input type="text" name="ClubFamilyMembership" id="ClubFamilyMembership" runat="server" class="smallinput" valtype="required;regex:DOUBLE" /></span>
								</p>

                                <p>
									<label>Principal Golf Membership Rate:</label>
									<span class="field">
										<input type="text" name="PrincipalGolfMembershipRate" id="PrincipalGolfMembershipRate" runat="server" class="smallinput" valtype="required;regex:DOUBLE" /></span>
								</p>

                                <p>
									<label>Secondary Golf Membership Rate:</label>
									<span class="field">
										<input type="text" name="SecondaryGolfMembershipRate" id="SecondaryGolfMembershipRate" runat="server" class="smallinput" valtype="required;regex:DOUBLE" /></span>
								</p>

                                <p>
									<label>Junior Golf Membership Rate:</label>
									<span class="field">
										<input type="text" name="JuniorGolfMembershipRate" id="JuniorGolfMembershipRate" runat="server" class="smallinput" valtype="required;regex:DOUBLE" /></span>
								</p>

                                <p>
									<label>Principal External Member:</label>
									<span class="field">
										<input type="text" name="PrincipalExternalMember" id="PrincipalExternalMember" runat="server" class="smallinput" valtype="required;regex:DOUBLE" /></span>
								</p>

                                <p>
									<label>Secondary External Member:</label>
									<span class="field">
										<input type="text" name="SecondaryExternalMember" id="SecondaryExternalMember" runat="server" class="smallinput" valtype="required;regex:DOUBLE" /></span>
								</p>

								<p>
									<label>Occupants Per Room Limit:</label>
									<span class="field">
										<input type="text" name="OccupantsPerRoomLimit" id="OccupantsPerRoomLimit" runat="server" class="smallinput" valtype="required;regex:INT" /></span>
								</p>
                                <div class="contenttitle2">
									<h3>Pets</h3>
								</div>
                                <p>
									<label>Number of Cats Limit:</label>
									<span class="field">
										<input type="text" name="CatsLimit" id="CatsLimit" runat="server" class="smallinput" valtype="required;regex:INT" /></span>
								</p>
                                <p>
									<label>Number of Dogs Limit:</label>
									<span class="field">
										<input type="text" name="DogsLimit" id="DogsLimit" runat="server" class="smallinput" valtype="required;regex:INT" /></span>
								</p>
						
							</div>
						</div>
					</div>
				</div>
				<div id="AccessSettings" class="accesstab">

					<div class="stdform">
						<div class="contentBlock" style="margin-bottom: 10px;">
							<div style="width: 600px;">
								<div class="contenttitle2">
									<h3>Access Settings</h3>
								</div>

								<p>
									<label>Send Code To Occupant:</label>
									<span class="field">
										<input type="checkbox" name="SendCodeToOccupant" id="SendCodeToOccupant" runat="server"  /></span>
								</p>

								<p>
									<label>Send Code To Guest:</label>
									<span class="field">
										<input type="checkbox" name="SendCodeToGuest" id="SendCodeToGuest" runat="server"  /></span>
								</p>

								<p>
									<label>Intranet Send Code To Occupant:</label>
									<span class="field">
										<input type="checkbox" name="SendCodeToOccupantIntranet" id="SendCodeToOccupantIntranet" runat="server"  /></span>
								</p>

								<p>
									<label>Intranet Send Code To Guest:</label>
									<span class="field">
										<input type="checkbox" name="SendCodeToGuestIntranet" id="SendCodeToGuestIntranet" runat="server"  /></span>
								</p>

								<p>
									<label>Access Code Expires:</label>
									<span class="field">
										<input type="checkbox" name="AccessCodeExpire" id="AccessCodeExpire" runat="server" /></span>
								</p>

								<p>
									<label>Access Code Valid Period:</label>
									<span class="field">
										<input type="text" name="AccessCodeVaildPeriod" id="AccessCodeVaildPeriod" runat="server" class="smallinput" valtype="required;regex:int" /></span>
								</p>

								<p>
									<label>Send Code X Days Before:</label>
									<span class="field">
										<input type="text" name="SendCodeXDaysBefore" id="SendCodeXDaysBefore" runat="server" class="smallinput" valtype="required;regex:int" /></span>
								</p>

								<p>
									<label>Contractors Enter/Exit at same gate:</label>
									<span class="field">
									<input type="checkbox" name="IsSupplierGateRestricted" id="IsSupplierGateRestricted" runat="server"  /></span>
								</p>
							</div>
						</div>
					</div>
				</div>
				<%-- commented out to add later --%>
				<%--<div id="CachingSettings" class="cachingtab">
					<div class="stdform">
						<div class="contentBlock" style="margin-bottom: 10px;">
							<div style="width: 600px;">
								<div class="contenttitle2">
									<h3>Caching Options</h3>
								</div>
							</div>
						</div>
				</div>
				</div>--%>

			<div class="bottomSave">
				<button type="button" class="submitbutton saveSettings" name="topSave">Save Settings</button>
			</div>

		</div>
	</div>

</asp:Content>
