﻿/*global Utils, PropertyService, jAlert, jConfirm, Entities,jQuery,Globals */
//Script file for ErfManagement

//Erven methods
var Erf = {
    GetErven: function () {
        var response = function (_result) {
            Erf.ErfPaginate.TableID = "#ErfTable";
            Erf.ErfPaginate.PaginationID = "#ErfPagination";
            Erf.ErfPaginate.TotalCount = _result.Count;

            if ($(".loader").is(":visible")) {
                $(".loader").slideToggle();
            }

            Utils.bindDataToGrid("#ErfTemplateContainer", "#ErfTemplate", _result.EntityList);
            Erf.ErfPaginate.bindPagination();
        };

        if (!$(".loader").is(":visible")) {
            $(".loader").slideToggle();
        }

        PropertyService.WCFProperty.GetErvenAndTypes(Erf.ErfPaginate, response, Utils.responseFail);
    },
    DeleteErf: function (_erfNum) {
        var response = function (_result) {

            if (!_result.EntityList[0]) {
                jAlert("This Erf cannot be deleted because a property is attached to it.", "Cannot Delete");
            } else {
                Erf.GetErven();
            }

            if ($(".loader").is(":visible")) {
                $(".loader").slideToggle();
            }
        };

        if (!$(".loader").is(":visible")) {
            $(".loader").slideToggle();
        }

        PropertyService.WCFProperty.DeleteErf(_erfNum,response,Utils.responseFail);
    },
    EditErf: function (_erfID) {
        var response = function (_result) {
            var $tab = $("#erf" + _erfID);
            var data = _result.EntityList[0];
            //bind the data
            Utils.tabs.bindAndInitTab("ErfEditTemplate", _result.EntityList[0]);
            $tab.block();
            $tab.find("input[name=ID]").val(_erfID);
            //call other service which will retrieve and populate the property types dropdown
            $.when(Erf.PopulateErfTypes("#erf" + _erfID)).then(function () {
                $tab.unblock();
                $tab.find("select[name=ErfTypeID]").val(_result.EntityList[0].ErfTypeID).attr("selected","selected");
                $.uniform.update();

                if (data.HasProperties) {
                    $tab.find("select[name=ErfTypeID]").attr("disabled", "disabled");
                    $.uniform.update();
                    $.jGrowl("Erf Type edit disabled as there are properties associated with this Erf.", { header: '<span style="color:red;font-size:13px;">Erf Type Disabled</span>', sticky: true });
                }
            });

        };

        PropertyService.WCFProperty.GetErf(_erfID,response,Utils.responseFail);
        
    },
    PopulateErfTypes: function (_container) {
        _container = (_container || "");
        var dfd = new $.Deferred();
        var response = function (_result) {
            Utils.bindDataToGrid(_container + " select[name=ErfTypeID]", "#ddlTemplate", _result.EntityList);
            jQuery.uniform.update();
            dfd.resolve();
        };

        PropertyService.WCFProperty.GetErfTypes(response, function (e) { dfd.reject(); Utils.responseFail(e); });
        return dfd.promise();
    },
    SaveErf: function (event) {
        var erfID = $(event.target).attr("dataid");
        var erf = new Entities.Erf();
        var tabID = "";

        //check if it's an add or edit
        if (parseInt(erfID,10) === 0) {
            //it's an add so set the tab to the add tab
            tabID = "#create";
        } else {
            //it's an update pull the erf id from the button on the tab
            tabID = "#erf" + erfID;
        }

        var response = function (_result) {
            if (_result.Message === "") {
                jAlert("The erf was saved.","Save Success");
                $(tabID).unblock();
            } else {
                jAlert("There was an error while saving the erf was not saved.","Error during save");
            }
        };

        if (Utils.validation.validateForm(tabID, "There are missing values, please check the form.", false, true)) {

            $(tabID).mapToObject(erf, "name");
            $(tabID).block();

            PropertyService.WCFProperty.UpdateErf(erf, response, function (result) {
                $(tabID).unblock();
                Utils.responseFail(result);
            });
        }

    },
    AddErf: function () {

        var $tab = $("#create");
        Utils.bindDataToGrid("#create", "#ErfEditTemplate", new Entities.Erf());

        $tab.block();

        $.when(Erf.PopulateErfTypes("#create")).then(function () {
            $.uniform.update();
            Utils.tabs.initTab("#create");
            $tab.unblock();
        });
    },
    ErfPaginate: null
};

//page methods
var Page = {
    Load: function () {
        Erf.ErfPaginate = new Globals.paginationContainer("ErfNumber", 8, Erf.GetErven);

        Page.BindEvents();
        Erf.GetErven();
    },
    BindEvents: function () {
        $("#ErfTemplateContainer").on("click.deleteErf", ".deleteErf", Page.deleteErf_click);
        $("#ErfTemplateContainer").on("click.editErf", ".editErf", Page.editErf_click);
        $('.bodywrapper').on('click.Save', "button[name=topSave]", Erf.SaveErf);
        //click handlers for tabs
        $('.bodywrapper').on('Honeycomb.tabLoad', '#create', Page.AddErf_click);
        $('.bodywrapper').on('Honeycomb.tabLoad', '#grid', Page.AllErven_click);
    },
    deleteErf_click: function (e) {
        e.preventDefault();
        var deleteButton = $(this);
        jConfirm("Are you sure you wish to delete ERF " + $(this).attr("erfno"), "Confirm Delete", function (_confirm) {
            if (_confirm) {
                Erf.DeleteErf(deleteButton.attr("erfid"));
            }
        });
    },
    editErf_click: function (e) {
        e.preventDefault();
        var editButton = $(this);
        Utils.tabs.loadTab($(this).attr("erfno"), "erf" + $(this).attr("erfid"), function () { Erf.EditErf(editButton.attr("erfid")); });
    },
    AddErf_click: function (e) {
        e.preventDefault();
        Erf.AddErf();
    },
    AllErven_click: function (e) {
        e.preventDefault();
        Erf.GetErven();
    }
};

// on dom ready
$(function () {
    Page.Load();
});
