﻿#region Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls; 
#endregion

namespace Honeycomb.Web.Pages.Settings {
    public partial class Users : Honeycomb.Custom.BaseClasses.BasePage {
        protected void Page_Load(object sender, EventArgs e) {

            try {
                    using(var context = new Custom.Data.Honeycomb_Entities()){
                        var userRoles = (from ur in context.Role
                                             select ur).DefaultIfEmpty().ToList();
                        permissionsSelect.DataSource = userRoles;
                        permissionsSelect.DataTextField = "RoleName";
                        permissionsSelect.DataValueField = "ID";
                        permissionsSelect.DataBind();
                    }
            } catch(Exception ex){

            }

        }
    }
}