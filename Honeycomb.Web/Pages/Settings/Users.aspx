﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Honeycomb.Master" AutoEventWireup="true" CodeBehind="Users.aspx.cs" Inherits="Honeycomb.Web.Pages.Settings.Users" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
    <%-- References --%>
    <script type="text/javascript" src="/Scripts/plugins/chosen.jquery.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/charCount.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.tagsinput.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.validate.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.tmpl.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.smartWizard-2.0.min.js"></script>
    <script type="text/javascript" src="/Scripts/custom/tables.js"></script>
    <script type="text/javascript" src="/Scripts/custom/forms.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.validate.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.alerts.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.autogrow-textarea.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/ui.spinner.min.js"></script>
    <script type="text/javascript" src="/Scripts/custom/elements.js"></script>
    <script type="text/javascript" src="Users.aspx.js"></script>
    <script type="text/javascript" src="js/plugins/chosen.jquery.min.js"></script>

    <%-- Templates --%>
    <script id="usersTemplate" type="text/x-jquery-tmpl">
        <tr>
            <td>${FirstName}</td>
            <td>${LastName}</td>
            <td><a href="mailto:${Email}">${Email}</a></td>
            <td class="center">${Cell}</td>
            <td class="center">${Phone}</td>
            <td class="right" align="right">
                <a class="btn btn3 btn_search" tabname="${FirstName} ${LastName}" userid="${ID}" href="#"></a>
                <a href="#" userid="${ID}" class="btn btn3 btn_trash deleteUser"></a>
            </td>
        </tr>
    </script>
    <script id="editUserTemplate" type="text/x-jquery-tmpl">
        <fieldset>
        </fieldset>
    </script>

    <script id="clientTemplate" type="text/x-jquery-tmpl">
        <p><a href="clients/${id}">${name} - Age: ${age}</a></p>
        {{tmpl($data) "#phoneTemplate"}}
    </script>

     <script id="phoneTemplate" type="text/x-jquery-tmpl">
        <ul>{{each phone}}<li><input type="text" value="${$value}" class="longinput" />&nbsp;&nbsp;<input type="text" class="isDatePicker" /></li>{{/each}}</ul>
    </script>

    <script id="rolesTemplate" type="text/x-jquery-tmpl">
        {{each UserRoles}}<option value="${ID}">${RoleName}</option>{{/each}}
    </script>

    <script id="allRolesTemplate" type="text/x-jquery-tmpl">
        {{each Roles}}<option value="${ID}">${RoleName}</option>{{/each}}
    </script>

    <%-- user edit templates  --%>
    <script id="userEditTemplate" type="text/x-jquery-tmpl">
        <!-- START OF VERTICAL WIZARD -->
            <div class="stdform">
                <div id="wizard3" class="wizard verwizard">

                    <ul class="verticalmenu">
                        <li>
                            <a href="#wiz1step3_1">
                                <span class="label">Basic Information</span>
                            </a>
                        </li>
                        <li>
                            <a href="#wiz1step3_2">
                                <span class="label">Account Permissions</span>
                            </a>
                        </li>
                    </ul>

                    <div id="wiz1step3_1" class="formwiz">
                        <h4>Basic Information</h4>
                        <input type="hidden" name="InsertedOn" value="${User.InsertedOn}" />
                        <input type="hidden" name="InsertedBy" value="${User.InsertedBy}" />
                        <input type="hidden" name="DeletedOn" value="${User.DeletedOn}" />
                        <input type="hidden" name="DeletedBy" value="${User.DeletedBy}" />
                        <input type="hidden" name="SystemID" value="${User.SystemID}" />
                        <input type="hidden" name="GateID" value="${User.GateID}" />
                        <input type="hidden" name="ID" value="${User.ID}" />
						<input type="hidden" name="APIToken" value="${User.APIToken}" />
                        <p>
                                    <label>Title</label>
                                <span class="field">
                                    <input type="text" name="Salutation" class="longinput" valtype="required" value="${User.Salutation}" /></span>

                        </p>
                           <p>
                                    <label>First Name</label>
                                <span class="field">
                                    <input type="text" name="FirstName" class="longinput" valtype="required" value="${User.FirstName}" /></span>
                           </p>
                            <p>
                                 <label>Last Name</label>
                                <span class="field">
                                    <input type="text" name="LastName" class="longinput" valtype="required" value="${User.LastName}" /></span>
                            </p>
                            <p>
                                
                                    <label>Email</label>
                                <span class="field">
                                    <input type="text" name="Email" value="${User.Email}" class="longinput" valtype="required;regex:email" /></span>
                            </p>
                            <tr>
                                <td>
                                    <label>Cell</label></td>
                                <td><span class="field">
                                    <input type="text" name="Cell" value="${User.Cell}" class="longinput" valtype="required" /></span></td>
                            </tr>
                            <p>
                                
                                    <label>Phone</label>
                                <span class="field">
                                    <input type="text" name="Phone" value="${User.Phone}" class="longinput" valtype="required" /></span>
                            </p>
                            <p>
                                
                                    <label>User Name</label>
                                <span class="field">
                                    <input type="text" name="UserName" value="${User.UserName}" class="longinput" valtype="required" /></span>
                            </p>
                            <p>
                                
                                    <label>Password</label>
                                <span class="field">
                                    <input type="password" name="Password" value="${User.Password}" class="longinput" valtype="required" /></span>
                            </p>
                            <p>
                             
                                    <label>Confirm Password</label>
                                <span class="field">
                                    <input type="password" name="Password" value="${User.Password}" class="longinput" valtype="required" /></span>
                            </p>

                    </div>
                    <!--#wiz1step3_1-->

                    <div id="wiz1step3_2" class="formwiz">
                        <h4>Account Information</h4>

                        <p>
                        	<label>Select Permissions</label>
                            <span id="dualselect" class="dualselect">
                            	<select class="uniformselect" id="Select1" name="select3" multiple="true" size="10">
                                    {{tmpl($data) "#allRolesTemplate"}}
                                </select>
                                <span class="ds_arrow">
                                  <%--  <span class="arrow ds_next">&raquo;&raquo;</span>--%>
                                    <span class="arrow ds_next">&raquo;</span>
                                	<span class="arrow ds_prev">&laquo;</span>
                                 <%--   <span class="arrow ds_prev">&laquo;&laquo;</span>--%>
                                </span>
                                <select name="userPermissions" class="userPermissions" multiple="multiple" size="10">
                                	{{tmpl($data) "#rolesTemplate"}}
                                </select>
                            </span>
                        </p>
                    </div>
                    <!--#wiz1step3_2-->

                </div>
                <!--#wizard-->
            </div>

            <br clear="all" />
            <br />

            <!-- END OF VERTICAL WIZARD -->
    </script>


    <%-- end user edit tempaltes --%>

   

    <%-- Content --%>

    <!--pageheader-->
    <ul class="hornav">
        <li class="current"><a href="#grid">
            <img class="loader" src="/Theme/images/loaders/loader2.gif" alt="">All Users</a></li>
        <li><a href="#create"><img class='tabIcon' src='/Theme/Images/addfolder.png'/>&nbsp;</a></li>
    </ul>
    <div id="contentwrapper" class="contentwrapper">
        <div id="grid" class="subcontent">
            <table id="UserTable" cellpadding="0" cellspacing="0" border="0" class="stdtable stdtablecb">
                <colgroup>
                    <col class="con0" />
                    <col class="con1" />
                    <col class="con0" />
                    <col class="con1" />
                    <col class="con0" />
                </colgroup>
                <thead>
                    <tr>
                        <th class="head0" sortcolumn="FirstName">First Name</th>
                        <th class="head1" sortcolumn="LastName">Last Name</th>
                        <th class="head0" sortcolumn="Email">Email</th>
                        <th class="head1">Cell</th>
                        <th class="head0">Phone</th>
                        <th class="head1">Function</th>
                    </tr>
                </thead>
                <tbody id="usersTemplateContainer">
                </tbody>
            </table>
            <div class="dataTables_paginate">
                <div id="userPagination"></div>

            </div>	    
            <br clear="all" />
        </div>
        <div id="create" class="subcontent" style="display: none">

            <!-- START OF VERTICAL WIZARD -->
            <div class="stdform">
                <div id="wizard3" class="wizard verwizard">

                    <ul class="verticalmenu">
                        <li>
                            <a href="#wiz1step3_1">
                                <span class="label">Step 1: Basic Information</span>
                            </a>
                        </li>
                        <li>
                            <a href="#wiz1step3_2">
                                <span class="label">Step 2: Account Permissions</span>
                            </a>
                        </li>
                    </ul>

                    <div id="wiz1step3_1" class="formwiz">
                        <h4>Step 1: Basic Information</h4>
                        <input type="hidden" name="InsertedOn" value="<%= DateTime.Now.ToString() %>" />
                        <input type="hidden" name="InsertedBy" value="0" />
                        <input type="hidden" name="DeletedOn" value="" />
                        <input type="hidden" name="DeletedBy" value="" />
                        <input type="hidden" name="ID" value="-1" />
                        <p>
                                    <label>Title</label>
                                <span class="field">
                                    <input type="text" name="Salutation" class="longinput" valtype="required" /></span>

                        </p>
                           <p>
                                    <label>First Name</label>
                                <span class="field">
                                    <input type="text" name="FirstName" class="longinput" valtype="required" /></span>
                           </p>
                            <p>
                                 <label>Last Name</label>
                                <span class="field">
                                    <input type="text" name="LastName" class="longinput" valtype="required" /></span>
                            </p>
                            <p>
                                
                                    <label>Email</label>
                                <span class="field">
                                    <input type="text" name="Email" class="longinput" valtype="required;regex:email" /></span>
                            </p>
                            <tr>
                                <td>
                                    <label>Cell</label></td>
                                <td><span class="field">
                                    <input type="text" name="Cell" class="longinput" valtype="required" /></span></td>
                            </tr>
                            <p>
                                
                                    <label>Phone</label>
                                <span class="field">
                                    <input type="text" name="Phone" class="longinput" valtype="required" /></span>
                            </p>
                            <p>
                                
                                    <label>User Name</label>
                                <span class="field">
                                    <input type="text" name="UserName" class="longinput" valtype="required" /></span>
                            </p>
                            <p>
                                
                                    <label>Password</label>
                                <span class="field">
                                    <input type="password" name="Password" class="longinput" valtype="required" /></span>
                            </p>
                            <p>
                             
                                    <label>Confirm Password</label>
                                <span class="field">
                                    <input type="password" name="Password" class="longinput" valtype="required" /></span>
                            </p>

                    </div>
                    <!--#wiz1step3_1-->

                    <div id="wiz1step3_2" class="formwiz">
                        <h4>Step 2: Account Information</h4>

                        <p>
                        	<label>Select Permissions</label>
                            <span id="dualselect" class="dualselect">
                            	<select class="uniformselect" runat="server" id="permissionsSelect" name="select3" multiple="true" size="10">
                                    <option value="">Selection One</option>
                                    <option value="">Selection Two</option>
                                    <option value="">Selection Three</option>
                                    <option value="">Selection Four</option>
                                    <option value="">Selection Five</option>
                                    <option value="">Selection Six</option>
                                    <option value="">Selection Seven</option>
                                    <option value="">Selection Eight</option>
                                </select>
                                <span class="ds_arrow">
                                    <span class="arrow ds_next">&raquo;</span>
                                	<span class="arrow ds_prev">&laquo;</span>
                                </span>
                                <select name="userPermissions" class="userPermissions" multiple="multiple" size="10">
                                	<option value=""></option>
                                </select>
                            </span>
                        </p>

                        
                    </div>
                    <!--#wiz1step3_2-->

                </div>
                <!--#wizard-->
            </div>

            <br clear="all" />
            <br />

            <!-- END OF VERTICAL WIZARD -->

        </div>
    </div>
    <!--contentwrapper-->
    
</asp:Content>
