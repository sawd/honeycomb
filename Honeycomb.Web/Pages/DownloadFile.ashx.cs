﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Drawing;
using System.Data.SqlClient;
using Honeycomb.Custom.Data;
using Honeycomb.Custom;
using System.Web.SessionState;

namespace Honeycomb.Web {
    /// <summary>
    /// Summary description for DownloadFile
    /// </summary>
    public class DownloadFile : IHttpHandler, IReadOnlySessionState {

        public void ProcessRequest(HttpContext context) {
            if (!string.IsNullOrEmpty(context.Request.QueryString["FileRef"])) {

                System.Guid? fileID = null;

                if (context.Request.QueryString["FileRef"].ToLower().Contains("occupant") || context.Request.QueryString["FileRef"].ToLower().Contains("staff")) {
                    string ID = context.Request.QueryString["ID"];
                    var impro = new ImproSecurity();
                    byte[] imageBA = impro.GetMasterImageByID(ID);

                    /// an image exists, add it to the document repository and associate it with the entity

                    if (imageBA != null) {
                        if (imageBA.Length > 0) {
                            fileID = Custom.Model.Document.SaveDocument(imageBA, context.Request.QueryString["FileRef"] + "image.jpg", "", SessionManager.UserName);
                            if (fileID != null) {
                                if (context.Request.QueryString["FileRef"].ToLower().Contains("occupant")) {
                                    using (var dbContext = new Honeycomb_Entities()) {
                                        var update = dbContext.Occupant.Where(dr => dr.IDOrPassportNo == ID).ToList();
                                        foreach (Occupant occ in update) {
                                            occ.OccupantPhoto = fileID;
                                        }
                                        dbContext.SaveChanges();
                                    }
                                } else if (context.Request.QueryString["FileRef"].ToLower().Contains("staff")) {
                                    using (var dbContext = new Honeycomb_Entities()) {
                                        var update = dbContext.Staff.Where(dr => dr.GOVID == ID).ToList();
                                        foreach (Staff occ in update) {
                                            occ.FileID = fileID;
                                        }
                                        dbContext.SaveChanges();
                                    }
                                }
                            }
                        }
                    }

                } else {
                    fileID = new Guid(context.Request.QueryString["FileRef"]);
                }

                if (fileID != null) {
                    try {
                        var refFile = new DocumentRepository();

                        using (var dbContext = new Honeycomb_Entities()) {
                            refFile = dbContext.DocumentRepository.Where(dr => dr.Id == fileID).FirstOrDefault();
                        }

                        if (refFile != null) {

                            byte[] buffer = (byte[])refFile.FileData;
                            string fileName = refFile.FileName + "." + refFile.Extension.Trim();

                            string ContentType = "application/octet-stream";

                            //Switch to the correct content type for the attachment
                            switch (refFile.Extension.Trim()) {
                                case "doc":
                                    ContentType = "application/msword";
                                    break;

                                case "docx":
                                    ContentType = "application/msword";
                                    break;

                                case "pdf":
                                    ContentType = "application/pdf";
                                    break;

                                case "tif":
                                    ContentType = "image/tiff";
                                    break;

                                case "ppt":
                                    ContentType = "application/vnd.ms-powerpoint";
                                    break;

                                case "xls":
                                    ContentType = "application/vnd.ms-excel";
                                    break;

                                case "xlsx":
                                    ContentType = "application/excel";
                                    break;

                                case "jpg":
                                    ContentType = "image/jpeg";
                                    break;

                                case "gif":
                                    ContentType = "image/gif";
                                    break;

                                default:
                                    ContentType = "application/octet-stream";
                                    break;
                            }

                            //Stream the Blob to web page
                            context.Response.Clear();
                            context.Response.ContentType = ContentType;
                            context.Response.AppendHeader("content-disposition", "inline; filename=" + "\"" + fileName + "\"");
                            context.Response.BinaryWrite(buffer);

                        } else {
                            context.Response.Redirect("~/errors/404.html");
                        }
                    } catch (Exception ex) {

                        context.Response.Redirect("~/errors/404.html");
                    }
                }
            }
        }

        public bool IsReusable {
            get {
                return false;
            }
        }
    }
}
