﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using Honeycomb.Custom;
using Honeycomb.Custom.Data;

namespace Honeycomb.Web.Pages.Communication {
    /// <summary>
    /// Summary description for ExportOccupants1
    /// </summary>
    public class ExportOccupants1 : IHttpHandler {

        public void ProcessRequest(HttpContext context) {

            //set reponse headers
            context.Response.ContentType = "text/csv";
            context.Response.AddHeader("Content-Disposition", "attachment; filename=\"OccupantExport.csv\"");

            //write the headings
			context.Response.Write("FirstName,LastName,Email,Cellphone,ErfNumber,Address\n");

            bool All = bool.Parse(context.Request.QueryString["All"]);
            bool Tenant = bool.Parse(context.Request.QueryString["Tenant"]);
            bool Owner = bool.Parse(context.Request.QueryString["Owner"]);
            bool Occupant = bool.Parse(context.Request.QueryString["Occupant"]);

            using (var dbcontext = new Honeycomb_Entities())
            {
                StringBuilder sb = new StringBuilder();
                List<ClientEmailResult> OccupentInfoList = dbcontext.proc_GetOccupantBulkEmails(All, Owner, Tenant, Occupant).ToList();

                if (OccupentInfoList != null)
                {
                    foreach (var item in OccupentInfoList)
                    {
						sb.AppendLine(string.Format("{0},{1},{2},{3},{4},\"{5}\"", item.FirstName, item.LastName, item.Email, item.CellPhone, item.ErfNumber, item.Address));
                    }
                }
                //write data from db to file
                context.Response.Write(sb.ToString());
            }

        }

        public bool IsReusable {
            get {
                return false;
            }
        }
    }
}