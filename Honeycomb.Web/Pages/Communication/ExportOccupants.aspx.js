﻿/*global jAlert*/
var EmailExport = {
	validateForm: function () {
		if ($('input[class=ToSendCheck]:checked').length > 0) {
			return true;
		} else {
			jAlert("Please select an option from one of the checkboxes provided.", "Form Validation Error");
			return false;
		}
	},
	getEmails: function () {
		document.location = "ExportOccupants.ashx?All=" + $("#sendCodeToAll").is(":checked") + "&Tenant=" + $("#sendCodeToTenant").is(":checked") + "&Owner=" + $("#sendCodeToAOwner").is(":checked") + "&Occupant=" + $("#sendCodeToOccupant").is(":checked");
	}
};

//page object
var Page = {
	load: function () {
		//Utils.initDataPicker();
		Page.bindEvents();
	},
	bindEvents: function () {
		$("#btnExportEmails").click(Page.getEmails_CLICK);
	},
	getEmails_CLICK: function (e) {
		e.preventDefault();
		if (EmailExport.validateForm()) {
			EmailExport.getEmails();
		}
	}
};

//page load
$(function () {
	Page.load();
});