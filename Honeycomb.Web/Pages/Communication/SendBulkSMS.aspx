﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Honeycomb.Master" AutoEventWireup="true" CodeBehind="SendBulkSMS.aspx.cs" Inherits="Honeycomb.Web.Pages.Communication.SendBulkSMS" %>

<asp:Content ID="Content2" ContentPlaceHolderID="SiteContent" runat="server">
    <script type="text/javascript" src="/Scripts/plugins/chosen.jquery.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/charCount.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.tagsinput.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.validate.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.tmpl.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.smartWizard-2.0.min.js"></script>
    <script type="text/javascript" src="/Scripts/custom/tables.js"></script>

    <script type="text/javascript" src="/Scripts/plugins/jquery.validate.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.alerts.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.autogrow-textarea.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/ui.spinner.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/header.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/util.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/button.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/handler.base.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/handler.form.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/handler.xhr.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/uploader.basic.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/dnd.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/uploader.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/jquery-plugin.js"></script>

    <script type="text/javascript" src="/Services/WCFSms.svc/js"></script>
    <script type="text/javascript" src="SendBulkSMS.aspx.js"></script>


    <ul class="hornav">
        <li class="current"><a href="#grid">
            <span class="loader"  alt="" >Send SMS</span></a></li>
    </ul>
    <div id="contentwrapper" class="contentwrapper">
        <div id="grid" class="subcontent">
            <div class="contenttitle2" style="border:none;">
                <h3>Bulk SMS</h3>

                <table width="500px">
                    <tr></tr>
                    <tr>
                        <td>
                            <input class="ToSendCheck" type="checkbox" id="sendCodeToAll">All</td>
                        <td></td>
                        <td></td>

                    </tr>
                    <tr>
                        <td>
                            <input class="ToSendCheck" type="checkbox" id="sendCodeToAOwner">Owner</td>
                        <td></td>
                        <td></td>

                    </tr>
                    <tr>
                        <td>
                            <input class="ToSendCheck" type="checkbox" id="sendCodeToTenant">Tenant
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <input class="ToSendCheck" type="checkbox" id="sendCodeToOccupant">Occupant
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="3" style="">
                     
                            <textarea id="message"  maxlength="160" rows="3" cols="1" style="height: 100px; width: 500px;"  ></textarea>Maximum :160 Characters
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            <button id="btnSendSms" title="Send SMS" style="float:right;margin-top:33px;" class="stdbtn btn_orange btn_mail">Send SMS</button>
                        </td>

                    </tr>
                </table>
            </div>


        </div>
    </div>


</asp:Content>
