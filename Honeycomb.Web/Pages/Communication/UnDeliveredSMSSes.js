﻿/*global SMS, Globals,Utils*/
var UndeliveredSMSes = {
    getAllUndeliveredSMSes: function (firstLoad) {


        var undeliveredResponse = function (result) {


            UndeliveredSMSes.SMSResidentialPaginate.TableID = "#SMSTable";
            UndeliveredSMSes.SMSResidentialPaginate.PaginationID = "#SMSPagination";
            UndeliveredSMSes.SMSResidentialPaginate.TotalCount = result.Count;

            Utils.bindDataToGrid("#SMSTemplateContainer", "#SMSTemplate", result.EntityList);
            UndeliveredSMSes.SMSResidentialPaginate.bindPagination();
            if ($(".loader").is(":visible")) {
                $(".loader").slideToggle();
            }
            if (result.EntityList.length === 0) {
                $.jGrowl("close");
                $.jGrowl("There were no items found", { header: "<b>No records</b>" });
            }
        };
        SMS.SMSService.GetUndeliveredSMSes(UndeliveredSMSes.SMSResidentialPaginate, $("#filterSR").val(), undeliveredResponse, Utils.responseFail);
        if (!$(".loader").is(":visible") && !firstLoad) {
            $(".loader").slideToggle();
        }

    
    },

    SMSResidentialPaginate: null
};
var PageEvents = {
    bindEvents: function () {

        $("#contentwrapper").on("click.sendmail", "#btnSendSms", PageEvents.sendsms_click);

        $("#searchSRButt").click(function () {

            UndeliveredSMSes.getAllUndeliveredSMSes();
        });

    },
    load: function () {
        UndeliveredSMSes.SMSResidentialPaginate = new Globals.paginationContainer("NumberTo", 10, UndeliveredSMSes.getAllUndeliveredSMSes);

        
        UndeliveredSMSes.getAllUndeliveredSMSes(true);

    }

};
$(function () {

    PageEvents.load();
});