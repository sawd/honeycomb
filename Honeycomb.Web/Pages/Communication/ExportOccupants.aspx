﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Honeycomb.Master" AutoEventWireup="true" CodeBehind="ExportOccupants.aspx.cs" Inherits="Honeycomb.Web.Pages.Communication.ExportOccupants" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">

    <%-- References --%>
    <script type="text/javascript" src="/Scripts/plugins/chosen.jquery.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/charCount.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.tagsinput.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.validate.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.tmpl.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.smartWizard-2.0.min.js"></script>
    <script type="text/javascript" src="/Scripts/custom/tables.js"></script>
    <script type="text/javascript" src="/Scripts/custom/forms.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.validate.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.alerts.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.autogrow-textarea.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/ui.spinner.min.js"></script>
    <script type="text/javascript" src="/Scripts/custom/elements.js"></script>
    <script type="text/javascript" src="ExportOccupants.aspx.js"></script>

      <!--pageheader-->
    <ul class="hornav">
        <li class="current"><a href="#grid">Export Occupants</a></li>
    </ul>
    <div id="contentwrapper" class="contentwrapper">
        <div id="grid" class="subcontent">
            <p>
                <input class="ToSendCheck" type="checkbox" id="sendCodeToAll">All&nbsp;&nbsp;
                <input class="ToSendCheck" type="checkbox" id="sendCodeToTenant">Tenant&nbsp;&nbsp;
                <input class="ToSendCheck" type="checkbox" id="sendCodeToAOwner">Owner&nbsp;&nbsp;
                <input class="ToSendCheck" type="checkbox" id="sendCodeToOccupant">Occupant
            </p>
            <button id="btnExportEmails" title="Export Emails"  class="stdbtn btn_orange btn_mail">Export Occupants</button>
        </div>
    </div>

</asp:Content>