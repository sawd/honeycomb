﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Honeycomb.Master" AutoEventWireup="true" CodeBehind="UnDeliveredSMSSes.aspx.cs" Inherits="Honeycomb.Web.Pages.Communication.UnDeliveredSMSSes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">

    <script type="text/javascript" src="/Scripts/plugins/chosen.jquery.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/charCount.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.tagsinput.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.validate.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.tmpl.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.smartWizard-2.0.min.js"></script>
    <script type="text/javascript" src="/Scripts/custom/tables.js"></script>
    <script type="text/javascript" src="/Scripts/custom/forms.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.alerts.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.autogrow-textarea.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/ui.spinner.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/header.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/util.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/button.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/handler.base.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/handler.form.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/handler.xhr.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/uploader.basic.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/dnd.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/uploader.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/jquery-plugin.js"></script>

    <script type="text/javascript" src="/Services/WCFSms.svc/js"></script>
    <script type="text/javascript" src="UnDeliveredSMSSes.js"></script>



    <script id="SMSTemplate" type="text/x-jquery-tmpl">
        <tr>
            <td>${NumberTo}</td>
            <td>${Data}</td>
            <td>${Status}</td>
            <td>${StatusDate}</td>
        </tr>
    </script>




    <ul class="hornav">
        
        <li class="current"><a href="#grid">
            <img class="loader" style="display: none;" src="/Theme/images/loaders/loader2.gif" alt="" />Undelivered SMSes</a></li>

    </ul>
    <div id="contentwrapper" class="contentwrapper">
        <div id="grid" class="subcontent">

            <div id="propertyTabs" class="">
                
                <div id="tabs-1">

                    <div class="tableoptions">
                        Search
                        <input type="text" name="filter" id="filterSR" placeholder="Cell Number" class="smallinput radius3" style="width: 200px;" />
                        &nbsp;
                        <button type="button" id="searchSRButt" class="radius2">Search</button>
                    </div>
                    <table cellpadding="0" cellspacing="0" border="0" id="SMSTable" width="80%" class="stdtable">
                        <thead>
                            <tr>
                                <th class="head0" sortcolumn="NumberTo">Cell Number</th>
                                <th class="head1" sortcolumn="Data">Message</th>
                                <th class="head0" sortcolumn="Status">Status</th>
                                <th class="head1" sortcolumn="StatusDate">StatusDate</th>

                            </tr>
                        </thead>
                        <tbody id="SMSTemplateContainer">
                        </tbody>
                    </table>
                    <div class="dataTables_paginate">
                        <div id="SMSPagination"></div>

                    </div>

                </div>
</asp:Content>
