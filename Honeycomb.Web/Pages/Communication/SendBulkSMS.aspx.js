﻿/*global SMS,Utils,jAlert*/
var sms = {
   SendBulkSMS: function (firstLoad) {
        var sendObj = {};
        var sendTo = [];
        sendObj.message = $("#message").val();
        sendObj.sentTo = '';
		//Gets the value of the checkBox
        if ($('#sendCodeToAll').attr('checked')) {
			sendTo.push('All');
        }

        if ($('#sendCodeToAOwner').attr('checked')) {
            sendTo.push('Owner');
        }
        
        if($('#sendCodeToTenant').attr('checked')) {
			sendTo.push('Tenant');
        }

        if ($('#sendCodeToOccupant').attr('checked')) {
            sendTo.push('Occupant');
        }

        sendObj.sentTo = sendTo.join(",");

        var sendsmsResponse = function (result) {
            if ($(".loader").is(":visible")) {
                $(".loader").slideToggle();
            }
            jAlert("SMSes queued for sending...");

        };

        SMS.SMSService.SendBulkSMS(sendObj.message, sendObj.sentTo, sendsmsResponse, Utils.responseFail);
        if (!$(".loader").is(":visible") && !firstLoad) {
            $(".loader").slideToggle();
        }
    }
};
var PageEvents = {


    load: function () {
        PageEvents.bindEvents();

    },
    bindEvents: function () {

        $("#contentwrapper").on("click.sendmail", "#btnSendSms", PageEvents.sendsms_click);
    },

    sendsms_click: function (e) {
        e.preventDefault();
        sms.SendBulkSMS();
    }
};
$(function () {

    PageEvents.load();

});