﻿/*global Globals,Utils,ContractorService,Entities,jAlert,MaintenanceService,jConfirm*/

//contractor object
var Contractor = {
	getContractors: function () {

		var $currentTab = Utils.tabs.getCurrentTab();

		var response = function (_result) {
			$currentTab.unblock();

			Contractor.pagination.TableID = "#contractorTable";
			Contractor.pagination.PaginationID = "#contractorPagination";
			Contractor.pagination.TotalCount = _result.Count;

			Utils.bindDataToGrid("#contractorTemplateContainer", "#ContractorGridTemplate", _result.EntityList);
			Contractor.pagination.bindPagination();
		};

		$currentTab.block();
		ContractorService.WCFContractor.GetContractors(Contractor.pagination, $("#filter").val(), $("#SubCategory").val(), response, Utils.responseFail);
	},
	allContractorsInit: function () {
		var $currentTab = Utils.tabs.getCurrentTab();

		//gets all the required items for the contractors tab, it populates the category dropdowns
		var response = function (_result) {
			if (_result.Count > 0) {
				Utils.bindDataToGrid("#MainCategory", "#CategoriesDDL", _result.EntityList);
				$("#MainCategory").prepend("<option value=''>Choose a category</option>");
				$("#MainCategory").val("");
				//update multi select
				$("#MainCategory").trigger("liszt:updated");
			}
		};

		ContractorService.WCFContractor.GetCategories(response, Utils.responseFail);
	},
	getMainCategories: function () {
		var $currentTab = Utils.tabs.getCurrentTab();
		var dfd = new $.Deferred();
		//gets all the required items for the contractors tab, it populates the category dropdowns
		var response = function (_result) {
			if (_result.Count > 0) {
				var elementSelector = $currentTab.selector + " select[name=ContractorMainCategory]";
				Utils.bindDataToGrid(elementSelector, "#CategoriesDDL", _result.EntityList);
				$(elementSelector).prepend("<option value=''>Choose a category</option>");
				$(elementSelector).val("");
				//update multi select
				$(elementSelector).trigger("liszt:updated");
				dfd.resolve();
			}
		};

		ContractorService.WCFContractor.GetCategories(response, function (e) { dfd.reject(); Utils.responseFail(e); });
		return dfd.promise();
	},
	getSubCategories: function (_elementToBind) {

		if ($("#MainCategory").val() !== "") {

			var $currentTab = Utils.tabs.getCurrentTab();

			var response = function (_result) {
				$currentTab.unblock();

				if (_result.Count > 0) {
					Utils.bindDataToGrid("#SubCategory", "#SubCategoryDDL", _result.EntityList);
					//update multi select
					$("#SubCategory").trigger("liszt:updated");
				}

			};

			$currentTab.block();

			ContractorService.WCFContractor.GetSubCategories($("#MainCategory").val(), response, Utils.responseFail);
		}
	},
	getMainSubCategories: function () {
		var dfd = new $.Deferred();
		var $currentTab = Utils.tabs.getCurrentTab();
		var $mainCatSelect = $currentTab.find("select[name=ContractorMainCategory]");
		var $subCategorySelect = $currentTab.find("select[name=ContractorSubCategory]");

		//check if a valid main cat has been chosen
		if ($mainCatSelect.val() !== "") {
			//populate the sub categories
			var subResponse = function (_result) {
				Utils.bindDataToGrid($subCategorySelect.selector, "#SubCategoryDDL", _result.EntityList);
				//update multi select
				$subCategorySelect.trigger("liszt:updated");
				$currentTab.unblock();

				dfd.resolve();
			};

			$currentTab.block();
			ContractorService.WCFContractor.GetSubCategories($mainCatSelect.val(), subResponse, function (e) { dfd.reject(); Utils.responseFail(e); });
			return dfd.promise();
		}
	},
	addTab: function (_event) {
		if ($(_event.target).attr("id") === "add") {
			var $addTab = $("#add");
			var contractorEntity = new Entities.Contractor();

			Utils.bindDataToGrid($addTab.selector, "#ContractorTabTemplate", contractorEntity);
			Utils.tabs.initTab("#add");

			//init tabs and trigger tabload event on select
			$('#0Tabs').tabs({
				select: function (event, ui) {
					$(ui.panel).trigger("Honeycomb.tabLoad");
				}
			});

			//disable the staff and sites tabs because that functionality is not available before the contractor has been saved
			$("#0Tabs").tabs("option", "disabled", [2, 3]);

			$addTab.block();

			$.when(Contractor.getJobTitles(), Contractor.getMainCategories()).then(function () {
				$addTab.unblock();
				$.uniform.update();
			});

		}
	},
	loadEditTab: function (_contractorID) {
		if (!isNaN(_contractorID)) {
			var contractorResponse = function (_result) {
				if (_result.EntityList[0] !== null) {
					Utils.tabs.loadTab(_result.EntityList[0].TradingName, _contractorID, function () {
						var $contractorTab = Utils.tabs.getCurrentTab();

						Utils.bindDataToGrid($contractorTab.selector, "#ContractorTabTemplate", _result.EntityList[0]);
						Utils.tabs.initTab($contractorTab.selector);

						//init tabs and trigger tabload event on select
						$('#' + _contractorID + 'Tabs').tabs({
							select: function (event, ui) {
								$(ui.panel).trigger("Honeycomb.tabLoad");
							}
						});

						$('#' + _contractorID + 'Tabs').tabs("option", "disabled", [3]);

						$contractorTab.block();

						//make calls to retrieve related data for the contractor
						$.when(Contractor.getJobTitles(), Contractor.getMainCategories(), Contractor.getContractorContactPersons(_contractorID)).then(function () {

							Contractor.getContractorSubCategories(_contractorID);

							$.uniform.update();
							$contractorTab.unblock();
						});

					}, true, true);

				} else {
					$.jGrowl("Record could not be loaded.");
				}
			};

			ContractorService.WCFContractor.GetContractor(_contractorID, contractorResponse, Utils.responseFail);
		}
	},
	getContractorSubCategories: function (_contractorID) {
		var $currentTab = Utils.tabs.getCurrentTab();
		var $subCategorySel = $currentTab.find("select[name=ContractorSubCategory]");
		var $mainCategorySel = $currentTab.find("select[name=ContractorMainCategory]");

		var sbResponse = function (_result) {
			if (_result.EntityList.length > 0) {
				//This was written to work for simbithis current implementation, it will need to change
				//set the main category, the default catefory for simbithi is 10000
				$mainCategorySel.val("10000");
				$mainCategorySel.trigger("liszt:updated");

				//get the sub categories for the main getcory (for simbithi there is only one)
				$.when(Contractor.getMainSubCategories()).then(function () {
					for (var i = 0; i < _result.EntityList.length; i++) {
						$subCategorySel.find("option[value=" + _result.EntityList[i].SubCategoryID + "]").prop("selected", "selected");
					}

					$subCategorySel.trigger("liszt:updated");
				});
			}
		};

		ContractorService.WCFContractor.GetContractorSubCategories(_contractorID, sbResponse, Utils.responseFail);
	},
	getContractorContactPersons: function (_contractorID) {
		var $contractorTab = Utils.tabs.getCurrentTab();
		var $contactTab = $contractorTab.find(".contacttab");

		var contactResponse = function (_result) {
			if (_result.Count > 0) {
				var primaryContactForm = $contactTab.find(".primaryContact");
				var secondaryContactForm = $contactTab.find(".secondaryContact");
				var adminContactForm = $contactTab.find(".financeContact");

				for (var i = 0; i < _result.EntityList.length; i++) {
					switch (_result.EntityList[i].Type) {
						case "O":
							primaryContactForm.mapObjectTo(_result.EntityList[i], "name", true);
							break;
						case "P":
							secondaryContactForm.mapObjectTo(_result.EntityList[i], "name", true);
							break;
						case "A":
							adminContactForm.mapObjectTo(_result.EntityList[i], "name", true);
							break;
						default:
							alert("PEBCAK");
							break;
					}
				}
			}
		};

		ContractorService.WCFContractor.GetContractorContactPersons(_contractorID, contactResponse, Utils.responseFail);
	},
	updateContractor: function (_event) {
		_event.preventDefault();

		var $currentTab = Utils.tabs.getCurrentTab();
		var $infoTab = $currentTab.find(".infotab");
		var $contactTab = $currentTab.find(".contacttab");

		var contractor = new Entities.Contractor();

		if (Utils.validation.validateForm($infoTab.selector, "Company information tab is missing required fields", false, true) && Utils.validation.validateForm($contactTab.selector, "Company contact details tab is missing required fields", false, true)) {

			//.primaryContact, .secondaryContact,.financeContact
			var contactPersons = [];

			//check if there is an owner contact person
			if ($contactTab.find(".primaryContact input[name=FirstName]").val() !== "") {
				var ownerContact = new Entities.ContactPerson();

				$contactTab.find(".primaryContact").mapToObject(ownerContact, "name");

				ownerContact.Type = "O";

				contactPersons[contactPersons.length] = ownerContact;
			}

			//check if there is a primary contact person
			if ($contactTab.find(".secondaryContact input[name=FirstName]").val() !== "") {
				var primaryContact = new Entities.ContactPerson();

				$contactTab.find(".secondaryContact").mapToObject(primaryContact, "name");

				primaryContact.Type = "P";

				contactPersons[contactPersons.length] = primaryContact;
			}

			//check if there is a admin contact person
			if ($contactTab.find(".financeContact input[name=FirstName]").val() !== "") {
				var adminContact = new Entities.ContactPerson();

				$contactTab.find(".financeContact").mapToObject(adminContact, "name");

				adminContact.Type = "A";

				contactPersons[contactPersons.length] = adminContact;
			}

			$infoTab.mapToObject(contractor, "name");

			var response = function (_result) {
				$currentTab.unblock();

				if (_result.EntityList[0] !== 0) {
					jAlert("The contractor has been saved", "Success");
					//clear the html
					$("#add").html("");
					//got to all contractors tab
					Utils.tabs.selectTab("#grid");

				} else {
					jAlert("There was an error when attempting to save the contractor.", "Error");
				}
			};

			$currentTab.block();
			ContractorService.WCFContractor.UpdateContractor(contractor, $infoTab.find("select[name=ContractorSubCategory]").val(), contactPersons, response, Utils.responseFail);
		}

	},
	getJobTitles: function () {
		var $currentTab = Utils.tabs.getCurrentTab();
		var dfd = new $.Deferred();

		var jobResponse = function (_result) {
			if (_result.Count > 0) {
				Utils.bindDataToGrid($currentTab.selector + " select[name=JobTitleID]", "#JobTitleDDL", _result.EntityList);
			}
			dfd.resolve();
		};

		MaintenanceService.WCFMaintenance.GetLookUpOptionsByID(10000, jobResponse, function (e) { dfd.reject(); Utils.responseFail(e); });
		return dfd.promise();
	},
	getContractorID: function () {
		var contractorID = Utils.tabs.getCurrentTab().attr("id");

		if (isNaN(contractorID)) {
			contractorID = 0;
		}

		return parseInt(contractorID, 10);
	},
	deleteContractor: function (_contractorID) {
		if (!isNaN(_contractorID)) {
			var currentTab = Utils.tabs.getCurrentTab();

			var response = function (_result) {
				currentTab.unblock();

				if (_result.EntityList !== null) {
					if (_result.EntityList[0].success) {
						jAlert(_result.EntityList[0].message, "Successfully Deleted");
						Contractor.getContractors();
					} else {
						jAlert(_result.EntityList[0].message, "Deletion Error");
					}
				}
			};

			currentTab.block();
			ContractorService.WCFContractor.DeleteContractor(_contractorID, response, Utils.responseFail);
		}
	},
	clearContractorStaff: function (_contractorID) {
		var $currenttab = Utils.tabs.getCurrentTab();
		$currenttab.block();

		ContractorService.WCFContractor.RemoveAllContractorStaff(_contractorID, Contractor.events.clearContractorStaff_Success, Utils.responseFail, $currenttab);
	},
	events: {
		clearStaff_click: function (e) {
			e.preventDefault();
			jConfirm("Click ok if you wish to remove all staff members from the current contractor.<br/> This will not delete staff, it will only remove their association with this contrator.", "Clear All Contractor Staff?", function (_confirm) {
				if (_confirm) {
					Contractor.clearContractorStaff(Contractor.getContractorID());
				}
			});
		},
		clearContractorStaff_Success: function (_result, $context) {
			$context.unblock();

			jAlert(_result.EntityList[0].message, _result.EntityList[0].success ? "Clear Staff Success" : "Clear Staff Failed");

			if (_result.EntityList[0].success) {
				$context.find("#StaffContainer").html("");
			}
		}
	},
	pagination: null
};

var Staff = {
    SalutationOptions: [],
    JobTitleOptions: [],
    StaffList: [],
    StaffNotes: [],
	loadTab: function (_event, _overide) {
		var $tab = Utils.tabs.getCurrentTab().find(".stafftab");

		$tab.data("StaffList", []);

		if (_overide === undefined) {
			_overide = false;
		}

		if (!$tab.data("loaded") || _overide) {

			var response = function (_result) {

				if (true) {//_result.EntityList.length > 0

					$tab.data("StaffList", _result.EntityList);
					Staff.StaffList = _result.EntityList;

					$.when(Staff.GetJobTitleOptions(), Staff.GetSalutationOptions()).then(function () {

						$tab.unblock();

						Utils.bindDataToGrid($tab.selector + " #StaffContainer", "#StaffTemplate", _result.EntityList);
						Utils.tabs.initTab($tab.selector);

						//loop through each staff member and populate the select drop downs
						for (var i = 0; i < _result.EntityList.length; i++) {

							var staffID = "#staffno-" + _result.EntityList[i].ID;

							//activate the tags input for display
							$tab.find(staffID + ' .readOnlyTags').tagsInput({ interactive: false, removeWithBackspace: false });//, height: '38px', width: '220px'

							//init file uploader
							$tab.find(staffID + " .fubExample").fineUploader({
								uploaderType: 'basic',
								multiple: false,
								autoUpload: true,
								button: $tab.find(staffID + " .fubUploadButton"),
								request: {
									endpoint: "/Pages/UploadFile.ashx"
								}
							}).on('complete', function (event, id, filename, responseJSON) {//ignore jslint
								var $this = $(this);
								$tab.find("#staffno-" + $this.attr("staffid") + " .photoDiv").css({ "background-image": "url(/Pages/DownloadFile.ashx?FileRef=" + responseJSON.fileid + ")" }).addClass("photoDivWithPhoto");
								$tab.find("#staffno-" + $this.attr("staffid") + " input[name=FileID]").val(responseJSON.fileid);
							})//ignore jslint
                            .on("submit", function (event, id, filename) {//ignore jslint
								$tab.find("#staffno-" + $(this).attr("staffid") + " .photoDiv").css("background-image", "url(/Scripts/plugins/fileupload/loading.gif)").removeClass("photoDivWithPhoto");
                            })//ignore jslint
                            .on("error", function (event, id, filename, reason) {//ignore jslint

                            });//ignore jslint

							////populate the job title drop down
							//Utils.bindDataToGrid($tab.selector + " " + staffID + " select[name=JobTitleID]", "#ddlTemplate", Staff.JobTitleOptions);
							////set the selected value
							//$tab.find(staffID + " select[name=JobTitleID]").val(_result.EntityList[i].JobTitleID);
							////populate the key occupant rel select
							//Utils.bindDataToGrid($tab.selector + " " + staffID + " select[name=SalutationID]", "#ddlTemplate", Staff.SalutationOptions);
							////set the selected value
							//$tab.find(staffID + " select[name=SalutationID]").val(_result.EntityList[i].SalutationID);

							//$tab.find(staffID + " input[name=GOVID]").tagHolder();
						}

						// activate the accordion
						$tab.find(".accordion").accordion('destroy').accordion({ collapsible: true, active: false });
						//update the controls after the population is complete
						$.uniform.update();
					});
				} else {
					//$.jGrowl("No records to display");
					$tab.unblock();
				}
			};

			$tab.data("loaded", true);
			$tab.block();

			ContractorService.WCFContractor.GetStaff(Contractor.getContractorID(), response, Utils.responseFail);
		}
	},
	updateStaff: function (_event) {
		var $tab = Staff.GetTab();
		var $contractorID = Contractor.getContractorID();
		var $staffID = 0;

		if ($(_event.target).is("span")) {
			$staffID = $(_event.target).parent().attr("staffid");
		} else {
			$staffID = $(_event.target).attr("staffid");
		}

		var $StaffDiv = $($tab.selector + " #staffno-" + $staffID);
		var StaffMember = new Entities.Staff();

		if (Utils.validation.validateForm($StaffDiv.selector, "", false, false)) {

			$StaffDiv.mapToObject(StaffMember, "name");

			$StaffDiv.block();

			var response = function (_result) {
				$StaffDiv.unblock();

				if (_result.EntityList[0] === "") {
					//reload occupants to reflect latest changes
					Staff.loadTab(null, true);
				} else {
					jAlert(_result.EntityList[0], "Error: Save Failed");
				}
			};

			ContractorService.WCFContractor.UpdateStaff(StaffMember, $contractorID, response, Utils.responseFail);
		}
	},
	addStaff: function () {
		var $tab = Staff.GetTab();

		if ((parseInt($tab.find("#staffno-0").length, 10) === 0)) {
			var StaffMember = new Entities.Staff();

			StaffMember.Active = true;

			$("#StaffTemplate").tmpl(StaffMember).appendTo($tab.selector + " #StaffContainer");
			var staffDiv = $tab.find("#staffno-0");
			//activate the tags input for display
			staffDiv.find('.readOnlyTags').tagsInput({ interactive: false, removeWithBackspace: false });//, height: '38px', width: '220px'

			$($tab.selector + " #StaffContainer h3 a").last().text("New Staff Member");

			$tab.block();

			$.when(Staff.GetJobTitleOptions(), Staff.GetSalutationOptions()).then(function () {

				//init file uploader
				$tab.find("#staffno-0 .fubExample").fineUploader({
					uploaderType: 'basic',
					multiple: false,
					autoUpload: true,
					button: $tab.find("#staffno-0 .fubUploadButton"),
					request: {
						endpoint: "/Pages/UploadFile.ashx"
					}
				}).on('complete', function (event, id, filename, responseJSON) {//ignore jslint
					var $this = $(this);
					$tab.find("#staffno-0 .photoDiv").css({ "background-image": "url(/Pages/DownloadFile.ashx?FileRef=" + responseJSON.fileid + ")" }).addClass("photoDivWithPhoto");
					$tab.find("#staffno-0 input[name=FileID]").val(responseJSON.fileid);
				})//ignore jslint
                .on("submit", function (event, id, filename) {//ignore jslint
					$tab.find("#staffno-0 .photoDiv").css("background-image", "url(/Scripts/plugins/fileupload/loading.gif)").removeClass("photoDivWithPhoto");
                })//ignore jslint
                .on("error", function (event, id, filename, reason) {//ignore jslint

                });//ignore jslint
				//end file upload init

				//populate the job title drop down
				Utils.bindDataToGrid($tab.selector + " #staffno-0 select[name=JobTitleID]", "#ddlTemplate", Staff.JobTitleOptions);
				//populate the key occupant rel select
				Utils.bindDataToGrid($tab.selector + " #staffno-0 select[name=SalutationID]", "#ddlTemplate", Staff.SalutationOptions);

				$tab.find("#staffno-0 input[name=GOVID]").tagHolder();
				$tab.find("#staffno-0 input[name=GOVID]").tagHolder("disable");

				$tab.unblock();
				//update the custom field types
				$.uniform.update();
			});

			$tab.find(".accordion").accordion('destroy').accordion({ collapsible: true, active: false });
			$tab.find(".accordion").accordion("activate", ($tab.data("StaffList").length));
			$tab.find("#staffno-0 .updatestaff span").html("Add Staff Member");
			Utils.tabs.initTab($tab.selector + " #staffno-0");

		} else {
			jAlert("You can only add one new staff member at a time. Please click the 'Add Staff Member' button to save the new staff.", "Cannot add new staff");
		}
	},
	GetSalutationOptions: function () {
		var dfd = new $.Deferred();

		var response = function (_result) {
			Staff.SalutationOptions = _result.EntityList;
			dfd.resolve();
		};

		MaintenanceService.WCFMaintenance.GetLookUpOptionsByID(10001, response, function (e) { dfd.reject(); Utils.responseFail(e); });
		return dfd.promise();
	},
	GetJobTitleOptions: function () {
		var dfd = new $.Deferred();

		var response = function (_result) {
			Staff.JobTitleOptions = _result.EntityList;
			dfd.resolve();
		};

		MaintenanceService.WCFMaintenance.GetLookUpOptionsByID(10000, response, function (e) { dfd.reject(); Utils.responseFail(e); });
		return dfd.promise();
	},
	GetStaffNotes: function (_event) {
		var $staffTab = Staff.GetTab();
		var $staffID = 0;

		if ($(_event.target).is("span")) {
			$staffID = $(_event.target).parent().attr("staffid");
		} else {
			$staffID = $(_event.target).attr("staffid");
		}

		var response = function (_result) {

			$staffTab.unblock();

			$("#staffnotes #NoteID").children().remove();
			$("#staffnotes #NoteID").append("<option value='0'>Add New</option>");

			if (_result.EntityList.length > 0) {
				//populate the drop down with the notes                
				$("#StaffNotesTemplate").tmpl(_result.EntityList).appendTo("#staffnotes #NoteID");
				Staff.StaffNotes = _result.EntityList;
			}
			
			Staff.ClearStaffNote();

			//call the various update methods to refresh the custom controls
			$("#staffnotes #NoteID").trigger("liszt:updated");
			Utils.tabs.initTab("#staffnotes");
			$.uniform.update();

			//show dialog
			$("#staffnotes").dialog({
				resizable: false,
				modal: false,
				width: "600px",
				title: "Staff Notes",
				buttons: {
					Update: function () {
						$("#staffnotes").block();

						var updateResponse = function (_results) {
							$("#staffnotes").unblock();
							$("#staffnotes").dialog("close");
							Staff.ClearStaffNote();
						};

						var StaffNote = new Entities.StaffNote();

						StaffNote.Note = $("#staffnotes textarea[name=NoteDescription]").val();
						StaffNote.ID = $("#staffnotes #NoteID").val();
						StaffNote.StaffID = $staffID;
						StaffNote.Title = $("#staffnotes input[name=Title]").val();

						ContractorService.WCFContractor.UpdateStaffNote(StaffNote, updateResponse, Utils.responseFail);

					},
					Close: function () {
						$(this).dialog("close");
					}
				}
			});

		};

		if (!isNaN($staffID)) {

			$staffTab.block();

			ContractorService.WCFContractor.GetStaffNotes($staffID, response, Utils.responseFail);
		}
	},
	ClearStaffNote: function () {
		$("#staffnotes textarea[name=NoteDescription]").val("");
		$("#staffnotes #NoteID").val(0);
		$("#staffnotes input[name=Title]").val("");
	},
	StaffNoteChange: function (_event) {

		var $select = $(_event.target);
		var $selectedIndex = ($select.find(":selected").index() - 1);

		if (Staff.StaffNotes.length > 0 && $selectedIndex != -1) {
			//insert the values
			var $popup = $("#staffnotes");
			//populate the fields
			$popup.find("input[name=Title]").val(Staff.StaffNotes[$selectedIndex].Title);
			$popup.find("textarea[name=NoteDescription]").val(Staff.StaffNotes[$selectedIndex].Note);
		} else {
			Staff.ClearStaffNote();
		}
	},
	GetStaffErven: function (_staffID, _staffContainer) {
		var dfd = new $.Deferred();

		var response = function (_result) {
			//you have the staff erven now build the access lists

			if (_result.EntityList.length > 0) {

				//clear list
				var staffErventags = _staffContainer.find("input[name=staffErven]");
				staffErventags.importTags('');

				for (var i = 0; i < _result.EntityList.length; i++) {
					staffErventags.addTag(_result.EntityList[i].ErfNumber);
				}

				//remove the delete icons
				_staffContainer.find(".readonlytaginputs .tag a").remove();
			}

			dfd.resolve();
		};

		ContractorService.WCFContractor.GetStaffErven(_staffID, response, function (e) { dfd.reject(); Utils.responseFail(e); });
		return dfd.promise();
	},
	StaffSelected: function (event, ui) {

		var staffID = $(ui.newContent[0]).find("input[name=ID]").val();
		var staffDiv = $(ui.newContent[0]);
		var $currentTab = Utils.tabs.getCurrentTab();

		if (ui.newContent.length > 0) {

			if (parseInt(staffID, 10) !== 0) {

				if (!staffDiv.data("loaded")) {

					staffDiv.data("loaded", true);

					staffDiv.block();

					var response = function (_result) {
						//you have the staff erven now build the access lists
						staffDiv.unblock();
						var photoDiv = staffDiv.find(".photoDiv");
						photoDiv.css("background-image", photoDiv.attr("image"));

						var staffDetails = Staff.StaffList[$currentTab.find("#StaffContainer > div").index(staffDiv)];

						//set up drop downs and tag holder functionality
						//populate the job title drop down;
						Utils.bindDataToGrid(staffDiv.selector + " select[name=JobTitleID]", "#ddlTemplate", Staff.JobTitleOptions);
						//set the selected value
						staffDiv.find("select[name=JobTitleID]").val(staffDetails.JobTitleID);
						//populate the key occupant rel select
						Utils.bindDataToGrid(staffDiv.selector + " select[name=SalutationID]", "#ddlTemplate", Staff.SalutationOptions);
						//set the selected value
						staffDiv.find("select[name=SalutationID]").val(staffDetails.SalutationID);

						staffDiv.find("input[name=GOVID]").tagHolder();

						$.uniform.update();

						if (_result.EntityList.length > 0) {

							//clear list
							var staffErventags = staffDiv.find("input[name=staffErven]");
							staffErventags.importTags('');

							for (var i = 0; i < _result.EntityList.length; i++) {
								staffErventags.addTag(_result.EntityList[i].ErfNumber);
							}

							//remove the delete icons
							staffDiv.find(".readonlytaginputs .tag a").remove();
						}

					};

					ContractorService.WCFContractor.GetStaffErven(staffID, response, Utils.responseFail);
				}
			} 
			Staff.GetCompanies(staffID);
        }


	},
	GetStaffByID: function (event) {

		var $tab = Utils.tabs.getCurrentTab();
		var staffDiv = $($tab.selector + " #staffno-" + $(event.target).attr("staffid"));
		var staffGOVID = $(event.target).val();

		if (staffGOVID !== "") {
			var response = function (_result) {
				staffDiv.unblock();

				if (_result.EntityList[0] !== null) {
					//bind data to the form
					staffDiv.mapObjectTo(_result.EntityList[0], "name", true);
					$.uniform.update();

					if (_result.EntityList[0].FileID !== null) {
						staffDiv.find(".photoDiv").css({ "background-image": "url(/Pages/DownloadFile.ashx?FileRef=" + _result.EntityList[0].FileID + ")" }).addClass("photoDivWithPhoto");
					}
				}

			};

			staffDiv.block();

			ContractorService.WCFContractor.GetStaffByID(staffGOVID, Contractor.getContractorID(), response, Utils.responseFail);
		}
	},
	GetTab: function () {
		var $tab = Utils.tabs.getCurrentTab().find(".stafftab");
		return $tab;
	},
	GetCompanies: function (staffId) {

	        var response = function (_result) {

	            if (_result.EntityList[0] !== null) {
	                //bind data to the form
	                $("#staffno-" + staffId).find(".CompaniesDisplay").html(_result.EntityList[0]);
	            }

	        };

	        ContractorService.WCFContractor.GetStaffCompanies(staffId, response, Utils.responseFail);
	}
};

var StaffPageEvents = {
	load: function () {
		StaffPageEvents.bindEvents();
	},
	bindEvents: function () {
		//staff tab load
		$('.bodywrapper').on("Honeycomb.tabLoad", ".stafftab", Staff.loadTab);
		//button clicks
		$(".bodywrapper").on("click.updatestaff", ".stafftab .updatestaff", Staff.updateStaff);
		$(".bodywrapper").on("click.staffNotesButton", ".stafftab .staffNotesButton", Staff.GetStaffNotes);
		$(".bodywrapper").on("click.addstaff", ".stafftab #addstaff", Staff.addStaff);
		$(".bodywrapper").on("click.clearStaff", ".stafftab #clearStaff", Contractor.events.clearStaff_click);
		//accordion activate event
		$(".bodywrapper").on("accordionchange", ".staffaccordion", Staff.StaffSelected);
		//staff id number change
		$(".bodywrapper").on("change.GOVID", ".stafftab input[name=GOVID]", Staff.GetStaffByID);
		$("#staffnotes #NoteID").on("change", Staff.StaffNoteChange);
	}
};

//page events
var PageEvents = {
	load: function () {
		//first bind all events
		PageEvents.bindEvents();
		//load the pagination object
		Contractor.pagination = new Globals.paginationContainer("TradingName", 10, Contractor.getContractors);
		Contractor.getContractors();
		Contractor.allContractorsInit();

	},
	bindEvents: function () {
		//bind tab load event to all contractors to load the contractors
		$('.bodywrapper').on('Honeycomb.tabLoad', '#grid', PageEvents.contractorGridTab_click);
		//bind change event to Categories dropdown to get sub categories
		$("#MainCategory").change(PageEvents.contractorCategory_change);
		//bind onclick event to search button
		$("#searchButt").click(PageEvents.search_click);
		//bind tab load events to tabs
		$('.bodywrapper').on('Honeycomb.tabLoad', '#add', Contractor.addTab);
		//$('.bodywrapper').on("Honeycomb.tabLoad", ".contacttab", Contractor.loadContactDetails);
		//bind save to save contractor button
		$('.bodywrapper').on('click.Save', "button[name=topSave]", Contractor.updateContractor);
		$('.bodywrapper').on('change', 'select[name=ContractorMainCategory]', PageEvents.category_change);
		$('.bodywrapper').on('click.editContractor', '.editContractor', PageEvents.contractorEdit_click);
		$(".bodywrapper").on("click.deleteContractor", ".deleteContractor", PageEvents.contractorDelete_CLICK);
	},
	contractorGridTab_click: function (e) {
		e.preventDefault();
		Contractor.getContractors();
	},
	contractorCategory_change: function (e) {
		e.preventDefault();
		Contractor.getSubCategories();
	},
	search_click: function (e) {
		e.preventDefault();
		Contractor.getContractors();
	},
	category_change: function (e) {
		e.preventDefault();
		Contractor.getMainSubCategories();
	},
	contractorEdit_click: function (e) {
		e.preventDefault();
		Contractor.loadEditTab($(this).attr("contractorid"));
	},
	contractorDelete_CLICK: function (e) {
		var thiz = this;
		e.preventDefault();
		jConfirm("Are you sure you wish to delete the contractor?", "Delete contractor", function (_confirm) {
			if (_confirm) {
				Contractor.deleteContractor($(thiz).attr("contractorid"));
			}
		});
	}
};

//main page load event
$(function () {
	PageEvents.load();
	StaffPageEvents.load();
});