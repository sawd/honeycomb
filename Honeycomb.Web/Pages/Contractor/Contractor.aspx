﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Honeycomb.Master" AutoEventWireup="true" CodeBehind="Contractor.aspx.cs" Inherits="Honeycomb.Web.Pages.Contractor.Contractor" %>

<%@ Register Src="~/UserControls/ContractorInfoTemplate.ascx" TagPrefix="uc1" TagName="ContractorInfoTemplate" %>
<%@ Register Src="~/UserControls/ContractorContactTemplate.ascx" TagPrefix="uc1" TagName="ContractorContactTemplate" %>
<%@ Register Src="~/UserControls/ContractorStaffTemplate.ascx" TagPrefix="uc1" TagName="ContractorStaffTemplate" %>



<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">

     <%-- References --%>
    <script type="text/javascript" src="/Scripts/plugins/jquery.tagsinput.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/chosen.jquery.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.validate.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.tmpl.min.js"></script>
    <script type="text/javascript" src="/Scripts/custom/tables.js"></script>
    <script type="text/javascript" src="/Scripts/custom/forms.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.alerts.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/header.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/util.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/button.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/handler.base.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/handler.form.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/handler.xhr.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/uploader.basic.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/dnd.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/uploader.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/jquery-plugin.js"></script>
    <script type="text/javascript" src="/Scripts/custom/jquery-honeycomb.js"></script>
    <script type="text/javascript" src="/Services/WCFContractor.svc/js"></script>
    <script type="text/javascript" src="/Services/WCFMaintenance.svc/js"></script>
    <script type="text/javascript" src="Contractor.aspx.js"></script>
    <link href="/Scripts/plugins/fileupload/fineuploader.css" rel="stylesheet" type="text/css" />  

    <uc1:ContractorStaffTemplate runat="server" ID="ContractorStaffTemplate" />

    <script type="text/x-jquery-tmpl" id="ddlTemplate">
        <option value="${ID}">${Name}</option>
    </script>

    <%-- category drop down template --%>
     <script id="CategoriesDDL" type="text/x-jquery-tmpl">
        <option value="${CategoryID}">${Name}</option>
    </script>

    <%-- sub category drop down template --%>
     <script id="SubCategoryDDL" type="text/x-jquery-tmpl">
        <option value="${SubCategoryID}">${Name}</option>
    </script>

    <%-- job title drop down template --%>
     <script id="JobTitleDDL" type="text/x-jquery-tmpl">
        <option value="${ID}">${Name}</option>
    </script>

    <%-- grid template --%>
    <script id="ContractorGridTemplate" type="text/x-jquery-tmpl">
        <tr>
            <td>${TradingName}</td>
            <td>${Contact}</td>
            <td>${Email}</td>
            <td>${MobileNo}</td>
            <td class="right" align="right"><a href="#" contractorid="${ContractorID}" class="btn btn3 btn_pencil editContractor" style="margin-right:10px;"></a><a href="#" contractorid="${ContractorID}" class="btn btn3 btn_trash deleteContractor"></a></td>
        </tr>
    </script>

    <%-- contractor tab template --%>
    <script id="ContractorTabTemplate" type="text/x-jquery-tmpl">
        <div class="pageheader nopadding">
            <h1 class="pagetitle">${TradingName}</h1>
            <div class="topSave">
                <button type="button" class="submitbutton" dataid="${ContractorID}" name="topSave">Save Contractor</button>
            </div>
            <input type="hidden" name="ContractorID" value="${ContractorID}" />
        </div>
        
        <div id="${ContractorID}Tabs">
            <ul>
                <li><a href="#${ContractorID}-CompanyInfo">Company Information</a></li>
                <li><a href="#${ContractorID}-ContactDetail" >Contact Details</a></li>
                <li><a href="#${ContractorID}-Staff">Staff</a></li>
                <li><a href="#${ContractorID}-Sites">Sites</a></li>
            </ul>
            <div id="${ContractorID}-CompanyInfo" class="infotab current" >
                <uc1:ContractorInfoTemplate runat="server" id="ContractorInfoTemplate" />
            </div>
            <div id="${ContractorID}-ContactDetail" class="contacttab" >
                <uc1:ContractorContactTemplate runat="server" ID="ContractorContactTemplate" />
            </div>
            <div id="${ContractorID}-Staff" class="stafftab">
                <p><button id="addstaff" type="button" class="stdbtn">Add A Staff Member</button>&nbsp;&nbsp;<button id="clearStaff" type="button" class="stdbtn">Clear All Staff</button></p>
                <div id="StaffContainer" class="accordion staffaccordion"></div>
            </div>
            <div id="${ContractorID}-Sites" class="sitestab">
                sites
            </div>
        </div>
        <div class="bottomSave">
            <button type="button" class="submitbutton" dataid="${ContractorID}" name="topSave">Save Contractor</button>
        </div>
    </script>

	<!-- staff notes modal -->
    <div style="display:none">
        <div id="staffnotes" class="clearMe">
            <label>Please select a note: </label>
                <span class="field">
                        <select id="NoteID" name="NoteID" data-placeholder="Select Note..." class="chzn-select" style="width:280px;">
                        </select>         
                </span>
            <div style="margin:10px 0;"><input type="text" name="Title" class="smallinput" /></div>
                <div style="margin-top:10px;">
                    <textarea style="width:98%;" name="NoteDescription" cols="80" rows="8" class="longinput"></textarea>
                </div>
        </div>
    </div>

    <!-- main tab headers -->
    <ul class="hornav">
        <li><a href="#add"><img class='tabIcon' src='/Theme/Images/addfolder.png'/>&nbsp;</a></li>
        <li class="current">
            <a href="#grid"><img class="loader" style="display:none;" src="/Theme/images/loaders/loader2.gif" alt="" />All Contractors</a>
        </li>
    </ul>
    <!-- end main tab headers -->
   <%-- begin content wrapper --%>
    <div id="contentwrapper" class="contentwrapper">
         <%-- begin grid content --%>
        <div id="grid" class="subcontent">

            <div class="tableoptions">
                <table>
                    <tr>
                        <td><select id="MainCategory" name="MainCategory" data-placeholder="Choose a Category..." class="chzn-select" style="width:250px;" tabindex="2"></select></td><td width="10">&nbsp;</td>
                        <td><select id="SubCategory" name="SubCategory" data-placeholder="Choose a sub category..." class="chzn-select" multiple="multiple" style="width:400px;" tabindex="4"></select></td><td width="10"></td>
                        <td>or <input type="text" name="filter" id="filter" placeholder="Trading Name" class="smallinput radius3" style="width:200px;" /> &nbsp; <button type="button" id="searchButt" class="radius2">Search</button></td>
                    </tr>
                </table>
                
            </div>
            <table cellpadding="0" cellspacing="0" border="0" id="contractorTable" class="stdtable">
                <thead>
                    <tr>
                        <th class="head0" sortcolumn="TradingName">Trading Name</th>
                        <th class="head1" sortcolumn="Contact">Contact</th>
                        <th class="head0" sortcolumn="Email">Email</th>
                        <th class="head1" sortcolumn="MobileNumber">Mobile Number</th>
                        <th class="head0">Function</th>
                    </tr>
                </thead>
                <tbody id="contractorTemplateContainer">
                </tbody>
            </table>
            <div class="dataTables_paginate">
                <div id="contractorPagination"></div>
            </div>
        </div>
        <%-- end grid content --%>
        <div id="add" class="subcontent" style="display: none">
        </div>

        <div id="user-toolbar-options" style="display: none">
	        <a href="#"><i class="icon-user"></i></a>
	        <a href="#"><i class="icon-star"></i></a>
	        <a href="#"><i class="icon-edit"></i></a>
	        <a href="#"><i class="icon-delete"></i></a>
	        <a href="#"><i class="icon-ban"></i></a>
        </div>
    </div>
    <!--contentwrapper-->
</asp:Content>
