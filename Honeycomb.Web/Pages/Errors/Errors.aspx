﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Honeycomb.Master" AutoEventWireup="true" CodeBehind="Errors.aspx.cs" Inherits="Honeycomb.Web.Pages.Errors.Errors" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
    <asp:Panel ID="Error403" runat="server" Visible="false">
        <div class="contentwrapper padding10">
    	<div class="errorwrapper error403">
        	<div class="errorcontent">
                <h1>403 Forbidden Access</h1>
                <h3>Your dont' have permission to access this page.</h3>
                
                <p>This is likely to be caused by one of the following</p>
                <ul>
                    <li>The author of the page has intentionally limited access to it.</li>
                    <li>The computer on which the page is stored is unreachable.</li>
                    <li>You like this page.</li>
                </ul>
                <br />
                <button class="stdbtn btn_black" onclick="javascript:history.back(1); return false;">Go Back to Previous Page</button> &nbsp; 
                <button onclick="document.location.href='/Pages/Dashboard/Home.aspx'; return false;" class="stdbtn btn_orange">Go Back to Dashboard</button>
            </div><!--errorcontent-->
        </div><!--errorwrapper-->
    </div>    
    </asp:Panel>
    <asp:Panel ID="Error500" runat="server" Visible="false">
        <div class="contentwrapper padding10">
    	<div class="errorwrapper error403">
        	<div class="errorcontent">
                <h1>500 Internal Server Error</h1>
                <h3>The server encountered an internal error and was unable to complete your request.</h3>
                
                <p>Please contact the server administrator <strong>webmaster@yourdomain.com</strong> and informed them of the time the error occurred.<br /> More information about this error may be available in the server error log.</p>
                <br />
                <button class="stdbtn btn_black" onclick="javascript:history.back(1); return false;">Go Back to Previous Page</button> &nbsp; 
                <button onclick="document.location.href='/Pages/Dashboard/Home.aspx'; return false;" class="stdbtn btn_orange">Go Back to Dashboard</button>
            </div><!--errorcontent-->
        </div><!--errorwrapper-->
    </div>    
    </asp:Panel>
    <asp:Panel ID="Error404" runat="server" Visible="false">
         <div class="contentwrapper padding10">
    	<div class="errorwrapper error404">
        	<div class="errorcontent">
                <h1>404 Page Not Found</h1>
                <h3>We couldn't find that page. It appears that you are lost.</h3>
                
                <p>The page you are looking for is not found. This could be for several reasons</p>
                <ul>
                    <li>It never existed.</li>
                    <li>It got deleted for some reason.</li>
                    <li>You were looking for something that is not here.</li>
                    <li>You like this page.</li>
                </ul>
                <br />
                <button class="stdbtn btn_black" onclick="javascript:history.back(1); return false;">Go Back to Previous Page</button> &nbsp; 
                <button onclick="document.location.href='/Pages/Dashboard/Home.aspx'; return false;" class="stdbtn btn_orange">Go Back to Dashboard</button>
            </div><!--errorcontent-->
        </div><!--errorwrapper-->
    </div>    
    </asp:Panel>
    <asp:Panel ID="Unknown" runat="server" Visible="false">
         <div class="contentwrapper padding10">
    	<div class="errorwrapper error404">
        	<div class="errorcontent">
                <h1>Unknown Error</h1>
                <h3>We couldn't diagnose the issue.</h3>
                <br />
                <button class="stdbtn btn_black" onclick="javascript:history.back(1); return false;">Go Back to Previous Page</button> &nbsp; 
                <button onclick="document.location.href='/Pages/Dashboard/Home.aspx'; return false;; return false;" class="stdbtn btn_orange">Go Back to Dashboard</button>
            </div><!--errorcontent-->
        </div><!--errorwrapper-->
    </div>    
    </asp:Panel>
</asp:Content>
