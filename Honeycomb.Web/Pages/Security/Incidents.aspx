﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Honeycomb.Master" AutoEventWireup="true" CodeBehind="Incidents.aspx.cs" Inherits="Honeycomb.Web.Pages.Security.Incidents" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
	
    <script type="text/javascript" src="/Scripts/plugins/chosen.jquery.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.tagsinput.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.tmpl.min.js"></script>
  <%-- <%=System.Web.Optimization.Scripts.Render("~/Scripts/fileupload.js")%>--%>
    <script type="text/javascript" src="/Scripts/plugins/tag-it.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/header.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/util.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/button.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/handler.base.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/handler.form.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/handler.xhr.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/uploader.basic.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/dnd.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/uploader.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/fileupload/jquery-plugin.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.alerts.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.validate.min.js"></script>
	<script type="text/javascript" src="/Scripts/custom/forms.js"></script>
    <script type="text/javascript" src="/Services/WCFMaintenance.svc/js"></script>
    <script type="text/javascript" src="Incidents.aspx.js"></script>
    <link href="/Scripts/plugins/fileupload/fineuploader.css" rel="stylesheet" type="text/css"/>
    <link href="/Theme/css/jquery.tagit.css" rel="Stylesheet" type="text/css" />

	<style type="text/css">
		div.selector {
			width:190px;
		}
	</style>

    <%-- drop down list template --%>
    <script type="text/x-jquery-tmpl" id="DDLTemplate">
        <option value="${ID}">${Name}</option>
    </script>

    <%-- occupants drop down template --%>
    <script type="text/x-jquery-tmpl" id="occupantsTemplate">
        <option value="${ID}">${FirstName} ${LastName}</option>
    </script>

     <%-- occupants drop down template --%>
    <script type="text/x-jquery-tmpl" id="usersTemplate">
        <option value="${ID}">${FirstName} ${LastName}</option>
    </script>

    <%-- template for the grid --%>
    <script id="IncidentGridTemplate" type="text/x-jquery-tmpl">
        <tr>
            <td>INC${ID}</td>
            <td>${ErfNumber}</td>
            <td>${dateFormat(IncidentDate,"dd/mm/yyyy")}</td>
            <td>${IncidentType}</td>
            <td>${Priority}</td>
            <td>${Status}</td>
            <td>${AssignedTo}</td>
            <td class="right" align="right"><a href="#" incidentid="${ID}" tabtitle="INC${ID}" class="btn btn3 btn_pencil editIncident" style="margin-right:10px;"></a><%--<a href="#" incidentid="${ID}" class="btn btn3 btn_trash deleteIncident"></a>--%></td>
        </tr>
    </script>

    <%-- Incident modal template --%>
    <script id="UpdateIncidentTemplate" type="text/x-jquery-tmpl">
        <span class="uploadFile">Photo:&nbsp;&nbsp;<a class="btn btn_document uploadButton" href="javascript:void(0);"><span>Upload Photo</span></a></span>
        <div style="margin-top:10px;">
            <p>Notes</p>
            <textarea style="width:98%;" name="Description" cols="80" rows="8" class="longinput"></textarea>
        </div>
        <input type="hidden" name="FileID" value="${FileID}" />
        <input type="hidden" name="ID" value="${ID}" />
        <input type="hidden" name="IncidentID" value="${IncidentID}" />
    </script>

    <%-- Incident modal template --%>
    <script id="ResolveIncidentTemplate" type="text/x-jquery-tmpl">
        <p>Resolution Description</p>
        <textarea style="width:98%;" name="ResolutionDescription" cols="80" rows="8" class="longinput"></textarea>
        
        <input type="hidden" name="ID" value="${ID}" />
        <input type="hidden" name="IncidentID" value="${IncidentID}" />
    </script>

    <%-- incident template --%>
    <script id="IncidentTemplate" type="text/x-jquery-tmpl">
        <div class="pageheader nopadding">
            <h1 class="pagetitle">&nbsp;</h1>
            <div class="topSave">
                <button type="button" class="submitbutton" dataid="${ID}" name="topSave">Save Incident</button>
            </div>
        </div>
        <div class="clearMe">
        <div class="two_third nomargin">
            <div class="contentBlock">
                <div class="contenttitle2">
                    <h3>Incident Information</h3>
                    <input type="hidden" name="FileID" value="${FileID}" />
                    <input type="hidden" name="ID" value="${ID}" />
                    <input type="hidden" name="PropertyID" value="${PropertyID}"/>
                    <input type="hidden" name="ErfID" value="${ErfID}" />
                    <input type="hidden" name="IncidentStatusID" value="${IncidentStatusID}" />
                </div>
                <div class="stdform">
                    <p>
                        <label>Source:</label>
                        <span class="field">
                            <select name="IncidentSourceID" class="uniformselect">
                            </select>
                        </span>
                    </p>
                    <p>
                        <label>Property:</label>
                        <span class="field">
                            <input type="text" name="PropertyField" valtype="required;" class="smallinput" />
                        </span>
                    </p>
                    <p>
                        <label>Incident Type:</label>
                        <span class="field">
                            <select name="IncidentTypeID" class="uniformselect">
                            </select>
                        </span>
                    </p>
                    <p>
                        <label>Priority:</label>
                        <span class="field">
                            <select name="IncidentPriorityID" class="uniformselect">
                            </select>
                        </span>
                    </p>
                    <p>
                        <label>Incident Date:</label>
                        <span class="field">
                            <input type="text" name="IncidentDate" class="isDatePicker smallinput" valtype="required;" value="${dateFormat(IncidentDate,'dd/mm/yyyy')}" />
                        </span>
                    </p>
                    <p>
                        <label>Expected Resolution Date:</label>
                        <span class="field">
                            <input type="text" name="ResolutionDate" class="isDatePicker smallinput" value="${dateFormat(ResolutionDate,'dd/mm/yyyy')}"/>
                        </span>
                    </p>
                    <p>
                        <label>Incident Time:</label>
                        <span class="field">
                            <input type="text" name="IncidentTime" class="smallinput" valtype="required;" value="${IncidentTime}"/>
                        </span>
                    </p>
                    <p>
                        <label>Incident Title:</label>
                        <span class="field">
                            <input type="text" name="Title" maxlength="200" class="smallinput" valtype="required;" value="${Title}"/>
                        </span>
                    </p>
                    <p>
                        <label>Description</label>
                        <span class="field">
                            <textarea name="Description" cols="80" rows="5" valtype="required;" class="longinput">${Description}</textarea>
                        </span>
                    </p>
                    <p>
                        <label>Parties Involved</label>
                        <span class="field">
                            <ul name="Occupants" data-placeholder="Select Occupant..." multiple="multiple" class="" style="width:395px;">
                            </ul>     
                        </span>
                    </p>
                    <p>
                        <label>Parties Involved Other</label>
                        <span class="field">
                            <input type="text" name="InvolvedPersonsOther" class="smallinput" value="${InvolvedPersonsOther}"/>
                        </span>
                    </p>
                    <p>
                        <label>Assign to:</label>
                        <span class="field">
                            <select name="AssignedUserID" data-placeholder="Select Assignee..." valtype="required;" class="chzn-select" style="width:250px;">
                                <option value="">None</option>
                            </select>
                        </span>
                    </p>
                    <p>
                        <label>Related Photo</label>
                        <span class="field uploadFile"><a href="javascript:void(0);" class="btn btn_document uploadButton"><span>Upload Photo</span></a> {{if FileID !== null}}&nbsp;&nbsp;<a target="_blank" href="/Pages/DownloadFile.ashx?FileRef=${FileID}">Download Photo</a>{{/if}}</span>
                    </p>
                </div>
            </div>
        </div>
        <div class="one_third nomargin">
            <div class="contentBlock" style="margin-left:10px;">
                 <div class="contenttitle2">
                    <h3>Incident Functions</h3>
                </div>
                <p align="center"><a href="javascript:void(0);" incidentid="${ID}" class="btn btn_document updateIncidentButt" style="width:220px;"><span incidentid="${ID}">Update investigation</span></a></p>
                {{if IncidentStatusID == 10000}}
                <p align="center"><a href="javascript:void(0);" incidentid="${ID}" class="btn btn_document setIncidentClosureButt" style="width:220px;"><span incidentid="${ID}">Resolve incident</span></a></p>
                {{/if}}
                {{if IncidentStatusID == 10000 || IncidentTypeID == 10001 }}
                <p align="center"><a href="javascript:void(0);" incidentid="${ID}" class="btn btn_document setIncidentClosedButt" style="width:220px;"><span incidentid="${ID}">Close this incident</span></a></p>
                {{/if}}
                {{if IncidentStatusID == 10002}}
                <p align="center"><a href="javascript:void(0);" incidentid="${ID}" class="btn btn_document setIncidentOpenButt" style="width:220px;"><span incidentid="${ID}">Open this incident</span></a></p>
                {{/if}}
                {{if ID != 0}}
                <p align="center"><a href="javascript:void(0);" incidentid="${ID}" class="btn btn_document viewIncident" style="width:220px;"><span incidentid="${ID}">View incident details</span></a></p>
                {{/if}}
            </div>
        </div>
        </div>

        <div class="bottomSave">
            <button type="button" class="submitbutton" dataid="${ID}" name="topSave">Save Incident</button>
        </div>
    </script>

    <!-- incident modal container -->
    <div style="display:none">
        <div id="incidentModal" class="clearMe">
        </div>
    </div>

     <%-- navigation --%>
    <ul class="hornav">
        <li><a href="#logincident"><img class='tabIcon' src='/Theme/Images/addfolder.png'/>&nbsp;</a></li>
        <li class="current"><a href="#viewincidents">All incidents</a></li>
    </ul>

    <%-- contents --%>
    <div id="contentwrapper" class="contentwrapper">
        <%-- log incident  --%>
        <div id="logincident" class="subcontent" style="display:none;"></div>
        <div id="viewincidents" class="subcontent">
            <%-- incidents grid --%>
            <div class="tableoptions">Search By <select name="filterBy" class="uniformselect" style="width:200px;"><option value="IncidentID">Incident No</option><option value="OwnerName">Owner Name</option><option value="ErfNumber">Erf No</option><option value="OccupantName">Occupant Name</option></select> &nbsp;&nbsp; <input type="text" name="filterValue" id="filterValue" placeholder="Type Filter Here" class="smallinput radius3" style="width:200px;" />&nbsp;&nbsp;Type&nbsp;&nbsp;<select name="IncidentTypeIDFilter" class="uniformselect" style="width:200px;"></select> &nbsp;Date From&nbsp;&nbsp;<input type="text" class="isDatePicker" id="FromDate" name="FromDate"  />&nbsp;To Date&nbsp;&nbsp;<input type="text" class="isDatePicker" id="ToDate" name="ToDate"  /> <button type="button" id="searchButt" class="radius2">Search</button></div>
            <table cellpadding="0" cellspacing="0" border="0" id="IncidentTable" class="stdtable">
                <thead>
                    <tr>
                        <th class="head0" sortcolumn="ID">No</th>
                        <th class="head1" sortcolumn="ErfNumber">Erven</th>
                        <th class="head0" sortcolumn="IncidentDate">Date</th>
                        <th class="head1" sortcolumn="IncidentType">Type</th>
                        <th class="head0" sortcolumn="Priority">Priority</th>
                        <th class="head1" sortcolumn="Status">Status</th>
                        <th class="head0">Captured by</th>
                        <th class="head1">Function</th>
                    </tr>
                </thead>
                <tbody id="IncidentTemplateContainer">
                </tbody>
            </table>
            <div class="dataTables_paginate">
                <div id="incidentPagination"></div>
            </div>	    
        </div>
    </div>

</asp:Content>