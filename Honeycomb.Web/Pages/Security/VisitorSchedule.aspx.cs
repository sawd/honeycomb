﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Honeycomb.Custom;

namespace Honeycomb.Web.Pages.Security {
    public partial class VisitorSchedule : Custom.BaseClasses.BasePage {
        protected void Page_Load(object sender, EventArgs e) {
            var systemSettings = CacheManager.SystemSetting;

			if(!systemSettings.SendCodeToOccupant) {
				SendCodeToOccupantHidden.Value = "0";
			} else {
				SendCodeToOccupantHidden.Value = "1";
			}

			if(!systemSettings.SendCodeToGuest) {
				SendCodeToGuestHidden.Value = "0";
			} else {
				SendCodeToGuestHidden.Value = "1";
			}
        }
    }
}