﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Honeycomb.Master" AutoEventWireup="true" CodeBehind="ContractorAuthority.aspx.cs" Inherits="Honeycomb.Web.Pages.Security.ContractorAuthority" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
	<div id="contentwrapper" class="contentwrapper">
		<script type="text/javascript" src="/Scripts/plugins/chosen.jquery.min.js"></script>
		<script type="text/javascript" src="/Scripts/plugins/jquery.uniform.min.js"></script>
		<script type="text/javascript" src="/Scripts/custom/forms.js"></script>

        <div class="contenttitle2">
            <h3>Contractor Exit Authority</h3>
        </div>
        <p>
            Select User:&nbsp;&nbsp;<select name="UserID" id="UserIDSelect" runat="server" style="width:200px;" class="uniformselect"></select>&nbsp;&nbsp;<button type="button" id="AssignExitAuthority" runat="server" onserverclick="AssignExitAuthority_ServerClick" class="stdbtn addButton">Assign User</button>
        </p>
	</div>
</asp:Content>
