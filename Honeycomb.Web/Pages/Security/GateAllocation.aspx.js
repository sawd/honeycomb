﻿/*global Globals,SecurityService,Utils,jAlert*/
//main object
var GateAllocation = {
    loadUsers: function () {
        var $container = GateAllocation.getContainer();
        var gateID = $("select[name=GateID]").val() === "0" ? null : $("select[name=GateID]").val();

        $container.block();

        var response = function (_result) {
            $container.unblock();

            GateAllocation.paginate.TableID = "#GateUserTable";
            GateAllocation.paginate.PaginationID = "#GateUserPagination";
            GateAllocation.paginate.TotalCount = _result.Count;

            Utils.bindDataToGrid("#GateUserTemplateContainer", "#GateUserGridTemplate", _result.EntityList);
            GateAllocation.paginate.bindPagination();
        };

        SecurityService.WCFSecurity.GetGateUsers(GateAllocation.paginate, gateID, response, Utils.responseFail);
    },
    loadGates: function () {
        var $container = GateAllocation.getContainer();

        var response = function (_result) {
            $container.unblock();

            Utils.bindDataToGrid("select[name=GateID]", "#ddlTemplate", _result.EntityList);

            $container.find("select[name=GateID]").prepend("<option value='0'>-- All Gates --</option>");

            $.uniform.update();
        };

        $container.block();
        SecurityService.WCFSecurity.GetGates(response, Utils.responseFail);
    },
    getContainer: function () {
        return $("#contentwrapper");
    },
    assignUserModal: function () {
        var $modalElement = $("#assignModal");
        var $container = GateAllocation.getContainer();

        var response = function (_result) {
            $container.unblock();
            //append the template to the modal dialog
            Utils.bindDataToGrid("#GateUsers", "#GateUserDDLTemplate", _result.EntityList);

            //intialize the controls on the element
            $.uniform.update();

            //open the dialog
            $modalElement.dialog({
                resizable: false,
                modal: true,
                width: "350px",
                title: "Assign User to gate",
                buttons: {
                    "Assign User": function () {
                        var $container = GateAllocation.getContainer();

                        var assignResponse = function (_result) {
                            $container.unblock();

                            if (_result.EntityList[0] === true) {
                                GateAllocation.loadUsers();
                            } else {
                                jAlert("There was an error when attempting to assign the user to the specified gate.");
                            }

                        };

                        $container.block();
                        SecurityService.WCFSecurity.AssignUserTogate($("#GateUsers").val(), $("select[name=GateID]").val(), assignResponse, Utils.responseFail);
                        $(this).dialog("close");
                    },
                    Cancel: function () {
                        $(this).dialog("close");
                    }
                }
            });
            //end modal
        };//end response

        $container.block();
        SecurityService.WCFSecurity.GetGateControlUsers(response,Utils.responseFail);
    },
    clearUsergate: function (_userID) {
        var $container = GateAllocation.getContainer();
        
        var response = function (_result) {
            $container.unblock();
            GateAllocation.loadUsers();
        };

        $container.block();
        SecurityService.WCFSecurity.AssignUserTogate(_userID, null, response, Utils.responseFail);
    },
    paginate: null
};

//Page events
var PageEvents = {
    load: function () {
        PageEvents.bindEvents();

        GateAllocation.paginate = new Globals.paginationContainer("Name", 10, GateAllocation.loadUsers);

        GateAllocation.loadUsers();
        GateAllocation.loadGates();
    },
    bindEvents: function () {
        $("select[name=GateID]").change(function () {
            if ($(this).val() === "0") {
                $(".addButton").prop("disabled",true);
            } else {
                $(".addButton").prop("disabled", false);
            }
            GateAllocation.loadUsers();
        });

        $(".addButton").click(PageEvents.assignUser_click);

        $(".bodywrapper").on("click.deleteGateGuard", ".deleteGateGuard", PageEvents.clearUser_click);
    },
    assignUser_click: function (_event) {
        _event.preventDefault();
        GateAllocation.assignUserModal();
    },
    clearUser_click: function (_event) {
        _event.preventDefault();
        GateAllocation.clearUsergate($(this).attr("userid"));
    }
};

//page ready event
$(function () {
    PageEvents.load();
});