﻿/*global jAlert, Entities, SecurityService, Utils*/

//#region Visitors
var Visitors = {
    commitVisitors: function () {

        if (Visitors.getSelectedOccupant().CanSMS) {

            var response = function (result) {

                $("#visitorCodes").find("tbody").children().remove();

                if (result.EntityList.length > 0) {

                    for (var i in result.EntityList) {
                        $("#visitorCodes").append("<tr><td>" + result.EntityList[i].FullName + "</td><td>" + result.EntityList[i].Cellphone + "</td><td>" + result.EntityList[i].AccessCode + "</td><td>" + result.EntityList[i].Message + "</td></tr>");
                    }

                    $("#visitorCodes").show("slow");

                    if (result.EntityList.length > 0) {
                        jAlert("Your access codes have been generated", "Success");
                    } else {
                        jAlert("No codes were generated", "Error");
                    }

                    $("#requestCodes").attr("disabled", false);
                    Visitors.resetVisitorForm();
                    $("#grid").unblock();
                }
            };

            var occupantID = null;
            var visitorArr = [];

            $("#visitorAED tr").each(function () {
                if ($(this).index() > 0) {

                    var tmp = new Entities.AccessVisitor();
                    tmp.ID = "00000000-0000-0000-0000-000000000000";
                    tmp.VisitorName = $(this).find("td input").first().val();
                    tmp.VisitorCellphone = $(this).find("td input").last().val();

                    if (Visitors.validateVisitor(tmp.VisitorName, tmp.VisitorCellphone)) {
                        visitorArr[visitorArr.length] = tmp;
                    }
                }
            });

            if (visitorArr.length > 0) {
                var sendToOccupantB = $("#SiteContent_SendCodeToOccupant").is(":disabled") ? false : $("#SiteContent_SendCodeToOccupant").is(":checked");
                var sendToGuestB = $("#SiteContent_SendCodeToGuest").is(":disabled") ? false : $("#SiteContent_SendCodeToGuest").is(":checked");

                //disable button
                $("#requestCodes").attr("disabled", true);
                //block out ui
                $("#grid").block({ message: null });
                //call service
                SecurityService.WCFSecurity.CreateAccesCodes(visitorArr, Visitors.getSelectedOccupant().ID, null, sendToOccupantB, sendToGuestB, false, response, Utils.responseFail);
            } else {
                jAlert("There are no valid visitors entered. No codes were generated.");
            }

        } else {
            jAlert("Either there is no selected occupant or the occupant is not authorised to request access codes.");
        }

    },
    validateVisitor: function (_visitorName, _visitorCellphone) {
        var valid = true;
        var reg = /^\d+$/;
        var sendToGuestB = $("#SiteContent_SendCodeToGuest").is(":disabled") ? false : $("#SiteContent_SendCodeToGuest").is(":checked");

        if (_visitorName === "") {
            valid = false;
        }

        if ((_visitorCellphone === "" || !reg.test(_visitorCellphone)) && sendToGuestB) {
            valid = false;
        }

        return valid;
    },
    occupantsArr: [],
    visitorCellphoneFlag: false,
    addNewVisitor: function (_originator) {

        $(_originator).remove();

        var currentRow = $("<tr><td><input type='text' name='visitorName'></td><td><input class='numeric' type='text' name='visitorCellphone' style='display:none;' /></td><td class='buttonTD'></td></tr>");
        var addButton = $("<button type='button' class='stdbtn addButton'>Add</button>");
        var deleteButton = $("<button style='margin-right:10px;' type='button' class='stdbtn'>Delete</button>");

        addButton.click(function () {
            Visitors.addNewVisitor($(this));
        });

        deleteButton.click(function () {
            $(this).parent().parent().remove();
            if ($("#visitorAED").find("tr").last().find(".addButton").length === 0) {
                $("#visitorAED").find("tr").last().find("td").last().append('<button onclick="Visitors.addNewVisitor(this);" type="button" class="stdbtn addButton">Add</button>');
            }
        });

        currentRow.find("td.buttonTD").append(deleteButton);
        currentRow.find("td.buttonTD").append(addButton);

        $("#visitorAED").append(currentRow);

        Utils.numericOnly(".numeric");
    },
    getSelectedOccupant: function () {
        return Visitors.currentOccupant !== null ? Visitors.currentOccupant : new Entities.Occupant();
    },
    isOccupantSelected: function () {
        return Visitors.currentOccupant !== null ? true : false;
    },
    resetVisitorForm: function () {
        var htmlToAppend = '<tr><td>Visitor Name</td><td  style="display:none;">Visitor Cellphone</td><td>&nbsp;</td></tr><tr><td><input type="text" name="visitorName" /></td><td><input type="text" name="visitorCellphone" style="display:none;" /></td><td><button onclick="Visitors.addNewVisitor(this);" type="button" class="stdbtn addButton">Add</button></td></tr>';
        $("#visitorAED").html(htmlToAppend);
    },
    activateForm: function () {

        if (Visitors.isOccupantSelected) {
            if (Visitors.getSelectedOccupant().CanSMS) {
                $("#visitorAED").show("slow");
            } else {
                $("#visitorAED").hide();
            }

            Visitors.resetVisitorForm();
            $("#visitorCodes").hide();

        } else {
            $("#visitorAED").hide();
        }
    },
    currentOccupant: null
    //, CheckVisitorCellphone: function () {

    //    var response = function (result) {
    //        visitorCellphoneFlag = result.EntityList[0];
    //        if (visitorCellphoneFlag === false) {
    //            $('input[name=visitorCellphone]').css('display', 'none');
    //            $('input[name=visitorCellphone]').css('width', '148%');
    //        }
    //    };

    //    SecurityService.WCFSecurity.CheckVisitorCellphone(response, Utils.responseFail)
    //}
};
//#endregion

var PageEvents = {
    load: function () {
        //Visitors.CheckVisitorCellphone();
        Visitors.activateForm();
        PageEvents.bindEvents();
    },
    bindEvents: function () {

        Utils.numericOnly(".numeric");

        $("#requestCodes").click(function () {
            Visitors.commitVisitors();
        });

        ///// SELECT WITH SEARCH /////
        $("input[name=occupantSearch]").autocomplete({
            source: function (request, response) {
                var requestProperties = function (result) {
                    if (result.EntityList[0] === null) { result.EntityList = []; }
                    response(result.EntityList);
                };

                //call the service method to return both matching erven and properties
                SecurityService.WCFSecurity.GetActiveOccupants(request.term, requestProperties, Utils.responseFail);
            }
            , minLength: 3
            , select: function (event, ui) {
                var occupantID = ui.item.value;
                var $occupantField = $("input[name=occupantSearch]");
                
                $occupantField.val(ui.item.label);

                var response = function (_result) {
                    if (_result.EntityList[0] !== null) {
                        Visitors.currentOccupant = _result.EntityList[0]; 
                        $("#userdet").show("slow");

                        $("#idno").html(Visitors.currentOccupant.IDNo);
                        $("#address").html(Visitors.currentOccupant.Address);

                        $("#cellphone").html(Visitors.currentOccupant.Cellphone);

                        if (_result.EntityList[0].CanSMS) {
                            $("#cansms").html("Yes");
                        } else {
                            $("#cansms").html("No");
                        }


                    }

                    $("#grid").unblock();
                    Visitors.activateForm();
                };
                
                $("#grid").block();
                SecurityService.WCFSecurity.GetOccupant(occupantID, response, Utils.responseFail);

                return false;
            }
        });
    }
};

//init
$(function () {
    PageEvents.load();
});