﻿/*global SecurityService,jAlert,Entities,jConfirm,PageEvents,Utils*/

var SystemSettings = {
	sendCodeToOccupant: false,
	sendCodeToGuest: false
};

//#region GuestList
var GuestList = {
	validateVisitor: function (_visitorName, _visitorCellphone) {
		var valid = true;
		var reg = /^\d+$/;
		var sendToGuestB = $("#SiteContent_SendCodeToGuest").is(":disabled") ? false : $("#SiteContent_SendCodeToGuest").is(":checked");

		if (_visitorName === "") {
			valid = false;
		}

		if ((_visitorCellphone === "" || !reg.test(_visitorCellphone)) && sendToGuestB) {
			valid = false;
		}

		return valid;
	},
	occupantsArr: [],
	addNewVisitor: function (_originator, _fullName, _cellphone, _appendAddButton) {

		if (_originator !== null) {
			$(_originator).remove();
		}

		var currentRow = $("<tr><td><input type='text' name='visitorName'></td><td style='display:none;'><input class='numeric' type='text' name='visitorCellphone' /></td><td class='buttonTD'></td></tr>");
		var addButton = $("<button type='button' class='stdbtn addButton'>Add</button>");
		var deleteButton = $("<button style='margin-right:10px;' type='button' class='stdbtn'>Delete</button>");

		if (_fullName) {
			currentRow.find("input[name=visitorName]").val(_fullName);
		}

		if (_cellphone) {
			currentRow.find("input[name=visitorCellphone]").val(_cellphone);
		}

		addButton.click(function () {
			GuestList.addNewVisitor($(this), null, null, true);
		});

		deleteButton.click(function () {
			$(this).parent().parent().remove();
			if ($("#visitorAED").find("tr").last().find(".addButton").length === 0) {
				$("#visitorAED").find("tr").last().find("td").last().append('<button onclick="GuestList.addNewVisitor(this);" type="button" class="stdbtn addButton">Add</button>');
			}
		});

		currentRow.find("td.buttonTD").append(deleteButton);

		if (_appendAddButton) {
			currentRow.find("td.buttonTD").append(addButton);
		}

		$("#visitorAED").append(currentRow);

		Utils.numericOnly(".numeric");
	},
	getSelectedOccupant: function () {
		var selectedItem = $("#Occupants option:selected");

		if (selectedItem.index() > 0) {

			return selectedItem.val();

		} else {
			return null;
		}
	},
	isOccupantSelected: function () {

		var selectedItem = $("#Occupants option:selected");

		if (selectedItem.index() > 0) {
			return true;
		} else {
			return false;
		}
	},
	resetVisitorForm: function () {
		var htmlToAppend = '<tr><td>Visitor Name</td><td style="display:none">Visitor Cellphone</td><td>&nbsp;</td></tr><tr><td><input type="text" name="visitorName" /></td><td style="display:none"><input type="text" name="visitorCellphone" /></td><td><button onclick="GuestList.addNewVisitor(this,null,null,true);" type="button" class="stdbtn addButton">Add</button></td></tr>';
		$("#visitorAED").html(htmlToAppend);
	},
	activateForm: function () {

		if (GuestList.currentOccupant !== null) {
			//is an occupant check if they can send an sms
			if (GuestList.currentOccupant.CanSMS) {
				//they have selected an occupant who can sms get the guest lists for the occupant
				GuestList.GetOccupantGuestLists();
				$("#visitorAED").show("slow");
			} else {
				$("#visitorAED").hide();
				$("#GuestListID").prop("disabled", true);
				$("#GuestListID").trigger("liszt:updated");
				jAlert("This person is not permitted to send access codes.", "Error");
			}

		} else {
			//not an occupant default to seehoa
			$("#visitorAED").show("slow");
		}
	},
	GetOccupantGuestLists: function () {

		if (GuestList.currentOccupant !== null) {

			var _occupantID = GuestList.currentOccupant.ID;

			var occupantGuestListResponse = function (_result) {
				if (_result.EntityList !== null) {
					//populate the guest lists
					$("#GuestListContainer").html($("#guestListTableTemplate").tmpl(_result.EntityList));
					GuestList.OccupantGuestLists = _result.EntityList;

					$("#OccupantGuestList").show();
				}

				$("#grid").unblock();
			};

			$("#grid").block();
			SecurityService.WCFSecurity.GetOccupantGuestLists(_occupantID, occupantGuestListResponse, Utils.responseFail);
		}
	},
	ShowGuestListOptions: function (guestList) {
		//render main template
		$("#GuestListOptionsContainer").html($("#guestListContainerTemplate").tmpl()).show("slow");
		GuestList.GetGuestListVisitors(guestList);

		//clear the current html in the modal
		$("#editGuestList").html("");

		//append the template to the modal dialog which will be used to edit guest list details
		$("#guestListTemplate").tmpl(guestList).appendTo("#editGuestList");
		$("#editGuestList").mapObjectTo(guestList, "name", true);

		$("#editGuestList").find("select[name=RecurrenceType]").val(guestList.RecurrenceType).attr("selected", "selected");

		//intialize the controls on the element
		Utils.tabs.initTab("#editGuestList");

		//add event handler to change function for recurrence type
		$(".uniformselect[name='RecurrenceType']").change(PageEvents.occuranceType_change);

		//trigger the event
		$('#RecurrenceType').trigger('change');

		//if the recurrence type is weekly, loop through the chosen options and set them as checked
		if (guestList.RecurrenceType === "Weekly") {
			$("#weeklyOptions input[name=DaysOfWeek]").attr("checked", false);
			var tmpArr = guestList.DaysOfWeek.split(",");
			$("#weeklyOptions input[name=DaysOfWeek]").each(function () {

				for (var i = 0; i < tmpArr.length; i++) {
					if ($(this).val() == tmpArr[i]) {
						$(this).prop("checked", true);
					}
				}
			});
		}

		//set the global settings for the guest list, this is saved against each guest list
		if ($("#SiteContent_SendCodeToOccupantHidden").val() === "1") {
			$("#SendCodeToOccupant").attr("checked", guestList.SendToOccupant);
		} else {
			$("#SendCodeToOccupant").attr("disabled", true);
		}

		if ($("#SiteContent_SendCodeToGuestHidden").val() === "1") {
			$("#SendCodeToGuest").attr("checked", guestList.SendToVisitor);
		} else {
			$("#SendCodeToGuest").attr("disabled", true);
		}

		$.uniform.update();

	},
	SaveGuestListObject: function () {
		$("#editGuestList").mapToObject(GuestList.CurrentGuestList, "name");
		//get the list of chosen days and set them in the object
		var tmpArr = [];
		$("#editGuestList input[name=DaysOfWeek]:checked").each(function () {
			tmpArr.push($(this).val());
		});

		GuestList.CurrentGuestList.DaysOfWeek = tmpArr.join(",");
		//GuestList.CurrentGuestList.StartDate = $("#editGuestList #StartDate").val();
	},
	GetGuestListVisitors: function (guestList) {
		var response = function (_result) {
			//load the visitors list for the guest list

			$("#GuestListVisitorsContainer").html($("#guestListVisitorsTemplate").tmpl(_result.EntityList));
			//store the current guest list so that we can add 
			GuestList.currentGuestListVisitors = _result.EntityList;

			$("#grid").unblock();
		};

		$("#grid").block();

		SecurityService.WCFSecurity.GetVisitorsByList(guestList.ID, response, Utils.responseFail);
	},
	CurrentGuestList: new Entities.GuestList(),
	OccupantGuestLists: null,
	currentOccupant: null,
	currentGuestListIndex: null,
	currentGuestListVisitors: null,
	SaveGuestList: function () {

		if (GuestList.ValidateGuestListSave()) {

			var response = function (_result) {
				if (_result.EntityList[0] === 0) {
					jAlert("There was an error while attempting to save the guest list.");
				} else {

					jAlert("Guest list saved successfully.");
					//check if the saved guest list is a new one, if so set the id
					if (parseInt($("#editGuestList input[name=ID]").val(),10) === 0) {
						$("#editGuestList input[name=ID]").val(_result.EntityList[0]);
					}

					GuestList.activateForm();
				}

				$("#grid").unblock();
			};

			//create entity
			var GuestListEntity = new Entities.GuestList();

			$("#editGuestList").mapToObject(GuestListEntity, "name");
			//get the list of chosen days and set them in the object
			var tmpArr = [];
			$("#editGuestList input[name=DaysOfWeek]:checked").each(function () {
				tmpArr.push($(this).val());
			});

			GuestListEntity.DaysOfWeek = tmpArr.join(",");

			//make sure the occupant is stored against the guest list
			GuestListEntity.OccupantID = GuestList.currentOccupant.ID;

			//set the values for sending to occupants and visitors from the global settings checkboxes
			GuestListEntity.SendToOccupant = $("#SendCodeToOccupant").is(":disabled") ? false : $("#SendCodeToOccupant").is(":checked");
			GuestListEntity.SendToVisitor = $("#SendCodeToGuest").is(":disabled") ? false : $("#SendCodeToGuest").is(":checked");

			//update the stored visitors list
			GuestList.updateCurrentVisitorList();
			//save the changes to the current object
			GuestList.OccupantGuestLists[GuestList.currentGuestListIndex] = GuestListEntity;

			$("#grid").block();
			SecurityService.WCFSecurity.SaveGuestList(GuestListEntity, GuestList.currentGuestListVisitors, response, Utils.responseFail);
		}
	},
	ValidateGuestListSave: function () {
		var reg = /^\d+$/;
		var sendToVisitor = $("#SendCodeToGuest").attr("disabled") ? false : $("#SendCodeToGuest").attr("checked") === "checked";
		var isValid = true;

		for (var i = 0; i < GuestList.currentGuestListVisitors.length; i++) {
			//if we must send to guest, check if the cellphone is provided and in the correct format. Also check if visitor name is provided.
			if ((sendToVisitor && (!reg.test($("#GuestListVisitorsContainer .VisitorCellphone").eq(i).val()) || $("#GuestListVisitorsContainer .VisitorCellphone").eq(i).val() === "" || $("#GuestListVisitorsContainer .VisitorCellphone").eq(i).val().length != 10))) {
				$.jGrowl("Visitor cellphone \"" + $("#GuestListVisitorsContainer .VisitorCellphone").eq(i).val() + "\" must contain only numbers and must be 10 characters long. Please correct and try again.", { header: "<span style='color:red;font-size:13px;'>Validation Error</span>", sticky: true });
				$("#GuestListVisitorsContainer .VisitorCellphone").eq(i).addClass("error");
				isValid = false;
			} else {
				$("#GuestListVisitorsContainer .VisitorCellphone").eq(i).removeClass("error");
			}

			if ($("#GuestListVisitorsContainer .VisitorName").eq(i).val() === "") {
				$.jGrowl("Please provide a visitor name for each visitor.", { header: "<span style='color:red;font-size:13px;'>Validation Error</span>", sticky: true });
				$("#GuestListVisitorsContainer .VisitorName").eq(i).addClass("error");
				isValid = false;
			} else {
				$("#GuestListVisitorsContainer .VisitorName").eq(i).removeClass("error");
			}
		}

		if ($("#editGuestList select[name=RecurrenceType]").val() === "Weekly") {
			
			if ($("#editGuestList input[name=DaysOfWeek]:checked").length === 0) {
				$.jGrowl("Please indicate which days of the week your weekly guest applies to.", { header: "<span style='color:red;font-size:13px;'>Validation Error</span>", sticky: true });
				isValid = false;
			}
		}

		if (GuestList.currentGuestListVisitors.length === 0) {
			$.jGrowl("There are no visitors to send to.", { header: "<span style='color:red;font-size:13px;'>Validation Error</span>", sticky: true });
			isValid = false;
		}

		return isValid;
	},
	DeleteGuestList: function (guestListID) {

		jConfirm("Are you sure you want to delete this list", "Confirm Delete", function (_confirm) {

			if (_confirm) {
				var response = function (_result) {
					if (_result.EntityList[0]) {
						jAlert("Successfully deleted Guest List", "Delete Success");
						GuestList.GetOccupantGuestLists();
					} else {
						jAlert("There was an unexpected error while attempting to delete the guest list.", "Delete Error");
					}
				};

				SecurityService.WCFSecurity.DeleteGuestList(guestListID, response, Utils.responseFail);
			}
		});

	},
	addVisitor: function () {
		if (GuestList.currentGuestListVisitors.length < 20) {
			//get all the values that may have changed and update them in the list
			GuestList.updateCurrentVisitorList();
			//add new visitor to array
			GuestList.currentGuestListVisitors.push(new Entities.GuestListVisitor());
			//draw the new list with the newly appened visitor
			GuestList.drawCurrentVisitorList();
			$("#GuestListVisitorsContainer .VisitorName:last-child").focus();
		} else {
			jAlert("A visitor list cannot exceed 20 persons, you cannot add another visitor.", "Error: Visitor Limit Reached");
		}
	},
	deleteVisitor: function (visitorIndex) {
		if (visitorIndex >= 0) {
			//get all the values that may have changed and update them in the list
			GuestList.updateCurrentVisitorList();
			//remove the visitor specified
			GuestList.currentGuestListVisitors.splice(visitorIndex, 1);
			//draw the resultant list
			GuestList.drawCurrentVisitorList();
		}
	},
	updateCurrentVisitorList: function () {
		//get all the visitor fields and loop through them updating the current arrary with the values
		for (var i = 0; i < GuestList.currentGuestListVisitors.length; i++) {
			GuestList.currentGuestListVisitors[i].VisitorName = $("#GuestListOptionsContainer .VisitorName").eq(i).val();
			GuestList.currentGuestListVisitors[i].VisitorCellphone = $("#GuestListOptionsContainer .VisitorCellphone").eq(i).val();
		}
	},
	drawOnceOffVisitorsList: function () {
		$("#OnceOffVisitorsContainer").html($("#guestListVisitorsTemplate").tmpl(GuestList.onceOffVisitorsList));
	},
	drawCurrentVisitorList: function () {
		$("#GuestListVisitorsContainer").html($("#guestListVisitorsTemplate").tmpl(GuestList.currentGuestListVisitors));
	},
	events: {
		saveGuestList_CLICK: function (e) {
			e.preventDefault();
			GuestList.SaveGuestList();
		},
		deleteGuestList_CLICK: function (e) {
			if (!isNaN($(this).attr("data-guestlistid"))) {
				GuestList.DeleteGuestList($(this).attr("data-guestlistid"));
			}
		},
		editGuestList_CLICK: function (e) {

			var currentListIndex = $("#GuestListContainer .editList").index($(this));

			if (currentListIndex > -1 && currentListIndex < GuestList.OccupantGuestLists.length) {

				GuestList.currentGuestListIndex = currentListIndex;
				GuestList.ShowGuestListOptions(GuestList.OccupantGuestLists[currentListIndex]);
			}
		},
		addVisitor_CLICK: function (e) {
			e.preventDefault();
			GuestList.addVisitor();
		},
		deleteVisitor_CLICK: function (e) {
			e.preventDefault();
			GuestList.deleteVisitor($("#GuestListOptionsContainer .deleteVisitor").index($(this)));
		},
		addGuestList_CLICK: function (e) {
			e.preventDefault();

			var currentListIndex = GuestList.OccupantGuestLists.length;
			var GuestListEntity = new Entities.GuestList();

			GuestList.currentGuestListIndex = currentListIndex;
			GuestList.ShowGuestListOptions(GuestListEntity);
		},
		sendToGuestListOnceOff_CLICK: function (e) {
			e.preventDefault();
			OnceOff.sendToGuestListVisitors();//ignore jslint
		}
	}
};
//#endregion
var OnceOff = {//ignore jslint
	visitorsList: [],
	addVisitor: function () {
		if (OnceOff.visitorsList.length < 20) {
			//get all the values that may have changed and update them in the list
			OnceOff.updateVisitorList();
			//add new visitor to array
			OnceOff.visitorsList.push(new Entities.AccessVisitor({ ID: "00000000-0000-0000-0000-000000000000" }));
			//draw the new list with the newly appended visitor
			OnceOff.drawVisitorList();
			$("#AccessCodeVisitorsContainer .VisitorName:last-child").focus();
		} else {
			jAlert("A visitor list cannot exceed 20 persons, you cannot add another visitor.", "Error: Visitor Limit Reached");
		}
	},
	deleteVisitor: function (visitorIndex) {
		if (visitorIndex >= 0) {
			//get all the values that may have changed and update them in the list
			OnceOff.updateVisitorList();
			//remove the visitor specified
			OnceOff.visitorsList.splice(visitorIndex, 1);
			//draw the resultant list
			OnceOff.drawVisitorList();
		}
	},
	updateVisitorList: function () {
		//get all the visitor fields and loop through them updating the current arrary with the values
		for (var i = 0; i < OnceOff.visitorsList.length; i++) {
			OnceOff.visitorsList[i].VisitorName = $("#AccessCodeVisitorsContainer .VisitorName").eq(i).val();
			OnceOff.visitorsList[i].VisitorCellphone = $("#AccessCodeVisitorsContainer .VisitorCellphone").eq(i).val();
		}
	},
	drawVisitorList: function () {
		$("#OnceOffTableContainer").html($("#OnceOffVisitorTableTmpl").tmpl());
		$("#AccessCodeVisitorsContainer").html($("#guestListVisitorsTemplate").tmpl(OnceOff.visitorsList));
	},
	createAccessCodes: function (dateValid) {

		OnceOff.updateVisitorList();

		var sendToOccupantB = $("#AccessCodeOnceOffContainer input[name=SendCodeToOccupant]").attr("disabled") ? false : $("#AccessCodeOnceOffContainer input[name=SendCodeToOccupant]").attr("checked") === "checked";
		var sendToGuestB = $("#AccessCodeOnceOffContainer input[name=SendCodeToGuest]").attr("disabled") ? false : $("#AccessCodeOnceOffContainer input[name=SendCodeToGuest]").attr("checked") === "checked";

		$("#instantCodes").block();
		SecurityService.WCFSecurity.CreateAccesCodes(OnceOff.visitorsList, GuestList.currentOccupant.ID, dateValid, sendToOccupantB, sendToGuestB, false, OnceOff.events.createAccessCodes_SUCCESS, Utils.responseFail);

	},
	showOnceOffModal: function () {
		$("#onceOffModal").dialog("open");
		$("#onceOffModal input[name=ValidDate]").blur();
	},
	initOnceOffModal: function () {
		//init the date picker
		$("#onceOffModal input[name=ValidDate]").datepicker({ dateFormat: "dd/mm/yy", minDate: "+0d", changeMonth: true, changeYear: true });

		//init the dialog
		$("#onceOffModal").dialog({
			resizable: false,
			modal: true,
			width: "350px",
			title: "Send Access Codes",
			autoOpen: false,
			open: function (e) {
				$("#onceOffModal input[name=dummy]").css("display", "none");
			},
			close: function (e) {
				$("#onceOffModal input[name=dummy]").css("display", "inline");
			},
			buttons: {
				"Send Codes": function () {
					//save guest list to current object
					if ($(this).find("input[name=ValidDate]").val() !== "") {
						OnceOff.createAccessCodes($(this).find("input[name=ValidDate]").datepicker("getDate"));
						$(this).dialog("close");
					} else {
						jAlert("Please enter the date the codes will be valid for.", "Date Error");
					}

				},
				Cancel: function () {
					$(this).dialog("close");
				}
			}
		});
	},
	validateVisitorList: function () {
		var reg = /^\d+$/;
		var sendToVisitor = $("#AccessCodeOnceOffContainer input[name=SendCodeToGuest]").attr("disabled") ? false : $("#AccessCodeOnceOffContainer input[name=SendCodeToGuest]").attr("checked") === "checked";
		var isValid = true;

		for (var i = 0; i < OnceOff.visitorsList.length; i++) {
			//if we must send to guest, check if the cellphone is provided and in the correct format. Also check if visitor name is provided.
			if ((sendToVisitor && (!reg.test($("#AccessCodeVisitorsContainer .VisitorCellphone").eq(i).val()) || $("#AccessCodeVisitorsContainer .VisitorCellphone").eq(i).val() === "" || $("#AccessCodeVisitorsContainer .VisitorCellphone").eq(i).val().length != 10))) {
				$.jGrowl("Visitor cellphone \"" + $("#AccessCodeVisitorsContainer .VisitorCellphone").eq(i).val() + "\" must contain only numbers and must be 10 characters long. Please correct and try again.", { header: "<span style='color:red;font-size:13px;'>Validation Error</span>", sticky: true });
				$("#AccessCodeVisitorsContainer .VisitorCellphone").eq(i).addClass("error");
				isValid = false;
			} else {
				$("#AccessCodeVisitorsContainer .VisitorCellphone").eq(i).removeClass("error");
			}

			if ($("#AccessCodeVisitorsContainer .VisitorName").eq(i).val() === "") {
				$.jGrowl("Please provide a visitor name for each visitor.", { header: "<span style='color:red;font-size:13px;'>Validation Error</span>", sticky: true });
				$("#AccessCodeVisitorsContainer .VisitorName").eq(i).addClass("error");
				isValid = false;
			} else {
				$("#AccessCodeVisitorsContainer .VisitorName").eq(i).removeClass("error");
			}
		}

		if (OnceOff.visitorsList.length === 0) {
			$.jGrowl("There are no visitors to send to.", { header: "<span style='color:red;font-size:13px;'>Validation Error</span>", sticky: true });
			isValid = false;
		}

		return isValid;
	},
	sendToGuestListVisitors: function () {
		GuestList.updateCurrentVisitorList();

		OnceOff.visitorsList = [];

		for (var i = 0; i < GuestList.currentGuestListVisitors.length; i++) {

			var tmp = new Entities.AccessVisitor();
			tmp.ID = "00000000-0000-0000-0000-000000000000";
			tmp.VisitorName = GuestList.currentGuestListVisitors[i].VisitorName;
			tmp.VisitorCellphone = GuestList.currentGuestListVisitors[i].VisitorCellphone;

			OnceOff.visitorsList.push(tmp);
		}

		OnceOff.drawVisitorList();
		Utils.tabs.selectTab("#instantCodes");
	},
	events: {
		createAccessCode_CLICK: function (e) {
			e.preventDefault();

			if (OnceOff.validateVisitorList()) {
				OnceOff.showOnceOffModal();
			}
		},
		addVisitor_CLICK: function (e) {
			e.preventDefault();
			OnceOff.addVisitor();
		},
		deleteVisitor_CLICK: function (e) {
			e.preventDefault();
			OnceOff.deleteVisitor($("#AccessCodeVisitorsContainer .deleteVisitor").index($(this)));
		},
		createAccessCodes_SUCCESS: function (result) {
			$("#instantCodes").unblock();

			if (result.EntityList !== null) {
				$("#OnceOffTableContainer").html($("#OnceOffVisitorCodesTableTmpl").tmpl());
				$("#AccessCodeVisitorsContainer").html($("#VisitorCodeTemplate").tmpl(result.EntityList));
				OnceOff.visitorsList = [];
			} else {
				jAlert("There was a problem generating the codes", "Code Generation Error");
			}
		},
		print_CLICK: function (e) {
			e.preventDefault();
			$("#OnceOffTableContainer").printThis({
				debug: false,
				importCSS: false,
				printContainer: true,
				loadCSS: "styles/VisitorSchedule-Print.css",
				pageTitle: "",
				removeInline: false,
				printDelay: 333,
				header: null
			});
		}
	}
};


var PageEvents = {//ignore jslint
	load: function () {
		PageEvents.bindEvents();
		OnceOff.initOnceOffModal();

		$("input[name=occupantSearch]").focus();

		//set the system setting checkboxes
		if ($("#SiteContent_SendCodeToOccupantHidden").val() === "1") {
			$("#AccessCodeOnceOffContainer input[name=SendCodeToOccupant]").attr("checked", true);
			SystemSettings.sendCodeToOccupant = true;
		} else {
			$("#AccessCodeOnceOffContainer input[name=SendCodeToOccupant]").attr("checked", false);
			$("#AccessCodeOnceOffContainer input[name=SendCodeToOccupant]").attr("disabled", true);
		}

		if ($("#SiteContent_SendCodeToGuestHidden").val() === "1") {
			$("#AccessCodeOnceOffContainer input[name=SendCodeToGuest]").attr("checked", true);
			SystemSettings.sendCodeToGuest = true;
		} else {
			$("#AccessCodeOnceOffContainer input[name=SendCodeToGuest]").attr("checked", false);
			$("#AccessCodeOnceOffContainer input[name=SendCodeToGuest]").attr("disabled", true);
		}

		$.uniform.update();
	},
	bindEvents: function () {

		Utils.numericOnly(".numeric");

		$("#requestCodes").click(function () { GuestList.SaveGuestList(); });
		$("#Occupants").change(function () { GuestList.activateForm(); });
		$(".btn_search").click(PageEvents.guestListOptions_click);

		$("#deleteGL").click(GuestList.DeleteGuestList);
		//$(".bodywrapper").on("click.updateoccupant", ".updateoccupant", Occupants.updateOccupant);
		$(".bodywrapper").on("click.deleteList", ".deleteList", GuestList.events.deleteGuestList_CLICK);
		$(".bodywrapper").on("click.editList", ".editList", GuestList.events.editGuestList_CLICK);
		$(".bodywrapper").on("click.addVisitor", ".addVisitor", GuestList.events.addVisitor_CLICK);
		$(".bodywrapper").on("click.deleteVisitor", ".deleteVisitor", GuestList.events.deleteVisitor_CLICK);
		$(".bodywrapper").on("click.saveGuestList", ".saveGuestList", GuestList.events.saveGuestList_CLICK);
		$(".bodywrapper").on("click.sendToOnceOff", ".sendToOnceOff", GuestList.events.sendToGuestListOnceOff_CLICK);
		$(".bodywrapper").on("click.addGuestList", ".addGuestList", GuestList.events.addGuestList_CLICK);

		//#region OnceOff GUI bindings
		$(".bodywrapper").on("click.addOnceOffVisitor", ".addOnceOffVisitor", OnceOff.events.addVisitor_CLICK);
		$(".bodywrapper").on("click.deleteVisitor", "#AccessCodeVisitorsContainer .deleteVisitor", OnceOff.events.deleteVisitor_CLICK);
		$(".bodywrapper").on("click.createAccessCodes", "#AccessCodeOnceOffContainer .createAccessCodes", OnceOff.events.createAccessCode_CLICK);
		$(".bodywrapper").on("click.Print", "#AccessCodeOnceOffContainer .Print", OnceOff.events.print_CLICK);
		//#endregion

		///// SELECT WITH SEARCH /////
		$("input[name=occupantSearch]").autocomplete({
			source: function (request, response) {
				var requestProperties = function (result) {
					if (result.EntityList[0] === null) { result.EntityList = []; }
					response(result.EntityList);
				};

				//call the service method to return matching occupants
				SecurityService.WCFSecurity.GetActiveOccupants(request.term, requestProperties, Utils.responseFail);
			}
            , minLength: 3
            , select: function (event, ui) {
				var occupantID = ui.item.value;
				var $occupantField = $("input[name=occupantSearch]");

				$occupantField.val(ui.item.label);

				var response = function (_result) {
					if (_result.EntityList[0] !== null) {
						GuestList.currentOccupant = _result.EntityList[0];
						$("#userdet").show("slow");

						$("#idno").html(GuestList.currentOccupant.IDNo);
						$("#address").html(GuestList.currentOccupant.Address);

						$("#cellphone").html(GuestList.currentOccupant.Cellphone);

						if (_result.EntityList[0].CanSMS) {
							$(".hornav").css("visibility", "visible");
							$("#cansms").html("Yes");
						} else {
							$("#cansms").html("No");
							$(".hornav").css("visibility", "hidden");
						}
					}

					$("#grid").unblock();
					GuestList.activateForm();
				};

				$("#grid").block();
				SecurityService.WCFSecurity.GetOccupant(occupantID, response, Utils.responseFail);

				return false;
            }
		});

		$("#GuestListID").change(function (_event) {
			if (parseInt(_event.target.value, 10) === 0) {
				//add new list means clear the visitors and pop up a modal dialog to capture the details
				GuestList.CurrentGuestList = new Entities.GuestList();
				GuestList.CurrentGuestList.OccupantID = parseInt(GuestList.getSelectedOccupant(), 10);
				GuestList.resetVisitorForm();
				GuestList.ShowGuestListOptions(_event.target.value);
			} else {
				//get the list of visitors for the guest list
				GuestList.CurrentGuestList = GuestList.OccupantGuestLists[($("#GuestListID option:selected").index() - 1)];
				GuestList.GetGuestListVisitors();

				//set the global settings for the guest list, this is saved against each guest list
				if (!$("#SiteContent_SendCodeToOccupant").is(":disabled")) {
					$("#SiteContent_SendCodeToOccupant").prop("checked", GuestList.CurrentGuestList.SendToOccupant);
				}

				if (!$("#SiteContent_SendCodeToGuest").is(":disabled")) {
					$("#SiteContent_SendCodeToGuest").prop("checked", GuestList.CurrentGuestList.SendToVisitor);
				}

				$.uniform.update();
			}
		});
	},
	occuranceType_change: function (event) {
		if (event.target.value === "Weekly") {
			$("#weeklyOptions").show("slow");
		} else {
			$("#weeklyOptions").hide("slow");
		}

		if (event.target.value !== "") {
			$("#endDateOption").show("slow");
		} else {
			$("#endDateOption").hide("slow");
		}

	},
	guestListOptions_click: function (event) {
		//show the modal popup for the guest list
		if ($("#GuestListID option:selected").index() >= 0) {
			GuestList.ShowGuestListOptions(GuestList.CurrentGuestList.ID);
		}
	}
};

//init
$(function () {
	PageEvents.load();
});