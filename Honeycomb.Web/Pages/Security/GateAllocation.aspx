﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Honeycomb.Master" AutoEventWireup="true" CodeBehind="GateAllocation.aspx.cs" Inherits="Honeycomb.Web.Pages.Security.GateAllocation" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
     <%-- References --%>
    <script type="text/javascript" src="/Scripts/plugins/chosen.jquery.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/charCount.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.tagsinput.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.validate.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.tmpl.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.smartWizard-2.0.min.js"></script>
    <script type="text/javascript" src="/Scripts/custom/tables.js"></script>
    <script type="text/javascript" src="/Scripts/custom/forms.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.validate.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.alerts.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.autogrow-textarea.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/ui.spinner.min.js"></script>
    <script type="text/javascript" src="/Scripts/custom/elements.js"></script>
    <script type="text/javascript" src="GateAllocation.aspx.js"></script>

    <%-- Templates --%>
    <script id="GateUserGridTemplate" type="text/x-jquery-tmpl">
        <tr>
            <td>${Name}</td>
            <td>${Username}</td>
            <td>${Gate}</td>
            <td class="right" align="right"><a href="#" userid="${UserID}" gateid="${GateID}" class="btn btn3 btn_trash deleteGateGuard"></a></td>
        </tr>
    </script>

    <%-- drop down template --%>
    <script type="text/x-jquery-tmpl" id="ddlTemplate">
        <option value="${ID}">${Name}</option>
    </script>

    <%-- user template --%>
    <script type="text/x-jquery-tmpl" id="GateUserDDLTemplate">
        <option value="${ID}">${FirstName} ${LastName}</option>
    </script>

    <div id="assignModal" style="display:none;text-align:center;">
        Select User:&nbsp;&nbsp;<select id="GateUsers" name="GateUsers" class="uniformselect"></select>
    </div>

    <div id="contentwrapper" class="contentwrapper">

        <div class="contenttitle2">
            <h3>Gate Guard Allocation</h3>
        </div>
        <p>
            Select Gate:&nbsp;&nbsp;<select name="GateID" class="uniformselect"><option value="0">-- All Gates --</option></select>&nbsp;&nbsp;<button type="button" disabled="disabled" class="stdbtn addButton">Assign User</button>
        </p>
        <table id="GateUserTable" cellpadding="0" cellspacing="0" border="0" class="stdtable">
                <thead>
                    <tr>
                        <th class="head0" sortcolumn="Name">Name</th>
                        <th class="head1" sortcolumn="Username">Username</th>
                        <th class="head0" sortcolumn="Gate">Gate</th>
                        <th class="head1">Function</th>
                    </tr>
                </thead>
                <tbody id="GateUserTemplateContainer">
                </tbody>
            </table>
            <div class="dataTables_paginate">
                <div id="GateUserPagination"></div>
            </div>	    
    </div>
</asp:Content>