﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Honeycomb.Master" AutoEventWireup="true" CodeBehind="VisitorSchedule.aspx.cs" Inherits="Honeycomb.Web.Pages.Security.VisitorSchedule" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
	<%--<link rel="stylesheet" href="styles/VisitorSchedule-Print.css" type="text/css" media="print" />--%>
	<style type="text/css">
		.guestListDetails {
			border: 1px solid #ddd;
			padding: 10px;
		}

			.guestListDetails td {
				padding: 5px;
			}

		#onceOffModal p {
			margin-bottom:10px;
		}
	</style>
	<%-- References --%>
	<script type="text/javascript" src="/Scripts/plugins/chosen.jquery.min.js"></script>
	<script type="text/javascript" src="/Scripts/plugins/charCount.js"></script>
	<script type="text/javascript" src="/Scripts/plugins/jquery.tagsinput.min.js"></script>
	<script type="text/javascript" src="/Scripts/plugins/jquery.uniform.min.js"></script>
	<script type="text/javascript" src="/Scripts/plugins/jquery.validate.min.js"></script>
	<script type="text/javascript" src="/Scripts/plugins/jquery.tmpl.min.js"></script>
	<script type="text/javascript" src="/Scripts/plugins/jquery.smartWizard-2.0.min.js"></script>
	<script type="text/javascript" src="/Scripts/custom/tables.js"></script>
	<script type="text/javascript" src="/Scripts/custom/forms.js"></script>
	<script type="text/javascript" src="/Scripts/plugins/jquery.validate.min.js"></script>
	<script type="text/javascript" src="/Scripts/plugins/jquery.alerts.js"></script>
	<script type="text/javascript" src="/Scripts/plugins/jquery.autogrow-textarea.js"></script>
	<script type="text/javascript" src="/Scripts/plugins/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="/Scripts/plugins/ui.spinner.min.js"></script>
	<script type="text/javascript" src="/Scripts/plugins/printThis.js"></script>
	<script type="text/javascript" src="VisitorSchedule.aspx.js"></script>
	<%--Template--%>
	<script id="occupantsTemplate" type="text/x-jquery-tmpl">
		<option value="${ID}" cansms="${CanSMS}" cell="${Cellphone}">${FirstName} ${LastName} - ${Cellphone}</option>
	</script>

	<script type="text/x-jquery-tmpl" id="ddlTemplate">
		<option value="${ID}">${Name}</option>
	</script>

	<script type="text/x-jquery-tmpl" id="guestListTableTemplate">
		<tr>
			<td>${Name}</td>
			<td class="right" align="right">
				<a href="#" data-guestlistid="${ID}" class="btn btn3 btn_pencil editList" style="margin-right: 10px;"></a>
				<a href="#" data-guestlistid="${ID}" class="btn btn3 btn_trash deleteList"></a>
			</td>
		</tr>
	</script>

	<script type="text/x-jquery-tmpl" id="guestListTemplate">

		<input type="hidden" name="ID" value="${ID}" />

		<table class="guestListDetails">
			<tr>
				<td>Name:</td>
				<td>
					<input type="text" name="Name" value="${Name}" class="smallinput" /></td>
				<td>Date Valid:</td>
				<td>
					<input type="text" class="isDatePicker" id="StartDate" name="StartDate" value="${StartDate}" /></td>
			</tr>
			<tr>
				<td>Occurrence:</td>
				<td>
					<select name="RecurrenceType" id="RecurrenceType" class="uniformselect">
						<option value="">None</option>
						<option value="Yearly">Yearly</option>
						<option value="Monthly">Monthly</option>
						<option value="Weekly">Weekly</option>
					</select>
				</td>
				<td>Send to:</td>
				<td>Occupant
					<input name="SendCodeToOccupant" id="SendCodeToOccupant" type="checkbox" value="1" />
					Visitor:
					<input name="SendCodeToGuest" id="SendCodeToGuest" type="checkbox" value="1" /></td>
			</tr>
			<tr id="endDateOption">
				<td>End Date:</td>
				<td colspan="3">
					<input type="text" class="isDatePicker" id="EndDate" name="EndDate" value="${EndDate}" /></td>
			</tr>
			<tr id="weeklyOptions">
				<td>&nbsp;</td>
				<td colspan="3">Sun
					<input type="checkbox" value="0" name="DaysOfWeek" />
					Mon
					<input type="checkbox" value="1" name="DaysOfWeek" />
					Tue
					<input type="checkbox" value="2" name="DaysOfWeek" />
					Wed
					<input type="checkbox" value="3" name="DaysOfWeek" /><br />
					Thu
					<input type="checkbox" value="4" name="DaysOfWeek" />
					Fri
					<input type="checkbox" value="5" name="DaysOfWeek" />
					Sat
					<input type="checkbox" value="6" name="DaysOfWeek" />
				</td>
			</tr>
		</table>
		<div style="width: 450px; margin-top: 10px; text-align: right;"><a href="javascript:void(0);" class="btn btn_orange btn_archive radius50 saveGuestList"><span>Save Guest List</span></a><a href="javascript:void(0);" style="margin-left: 15px; display: inline-block;" class="btn btn_orange btn_mail radius50 sendToOnceOff"><span>Send Once Off</span></a></div>
	</script>

	<script type="text/x-jquery-tmpl" id="guestListVisitorsTemplate">
		<tr>
			<td style="padding-bottom: 5px;">
				<input type="text" name="VisitorName" class="VisitorName" value="${VisitorName}" /></td>
			<td style="padding-bottom: 5px;">
				<input type="text" name="VisitorCellphone" class="VisitorCellphone" value="${VisitorCellphone}" /></td>
			<td style="padding-bottom: 5px;" class="right" align="right">
				<a href="javascript:void(0);" data-visitorid="${ID}" class="btn btn3 btn_trash deleteVisitor"></a>
			</td>
		</tr>
	</script>

	<script type="text/x-jquery-tmpl" id="guestListContainerTemplate">

		<table style="width: 100%;">
			<tr>
				<td style="width: 50%; vertical-align: top;">
					<div class="contenttitle2">
						<h3>Guest List Visitors</h3>
					</div>
					<div class="tableoptions">
						<button class="addVisitor radius3" title="table1">Add Visitor</button>
					</div>
					<table cellpadding="0" cellspacing="0" border="0" class="stdtable">
						<colgroup>
							<col class="con0" />
							<col class="con1" />
							<col class="con0" />
						</colgroup>
						<thead>
							<tr>
								<th class="head0">Visitor Name</th>
								<th class="head1">Visitor Cellphone</th>
								<th class="head0">Functions</th>
							</tr>
						</thead>
						<tbody id="GuestListVisitorsContainer">
						</tbody>
					</table>
				</td>
				<td style="width: 50%; vertical-align: top; padding-left: 15px;">
					<div class="contenttitle2">
						<h3>Guest List Details</h3>
					</div>
					<div id="editGuestList"></div>
				</td>
			</tr>
		</table>
	</script>

	<script type="text/x-jquery-tmpl" id="OnceOffVisitorTableTmpl">
		<table cellpadding="0" cellspacing="0" border="0" class="stdtable">
			<colgroup>
				<col class="con0" />
				<col class="con1" />
				<col class="con0" />
			</colgroup>
			<thead>
				<tr>
					<th class="head0">Visitor Name</th>
					<th class="head1">Visitor Cellphone</th>
					<th class="head0">Functions</th>
				</tr>
			</thead>
			<tbody id="AccessCodeVisitorsContainer">
			</tbody>
		</table>
	</script>

	<script type="text/x-jquery-tmpl" id="OnceOffVisitorCodesTableTmpl">
		<table cellpadding="0" cellspacing="0" border="0" class="stdtable">
			<colgroup>
				<col class="con0" />
				<col class="con1" />
				<col class="con0" />
			</colgroup>
			<thead>
				<tr>
					<th class="head0">Visitor Name</th>
					<th class="head1">Visitor Cellphone</th>
					<th class="head0">Access Code</th>
					<th class="head1">Message</th>
				</tr>
			</thead>
			<tbody id="AccessCodeVisitorsContainer">
			</tbody>
		</table>
	</script>

	<script type="text/x-jquery-tmpl" id="VisitorCodeTemplate">
		<tr>
			<td style="padding-bottom: 5px;">${FullName}</td>
			<td style="padding-bottom: 5px;">${Cellphone}</td>
			<td style="padding-bottom: 5px;">${AccessCode}</td>
			<td style="padding-bottom: 5px;">${Message}</td>
		</tr>
	</script>
	<!--pageheader-->

	<table style="margin: 15px 15px;">
		<tr>
			<td colspan="2">Select an occupant&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="occupantSearch" style="width: 200px;" class="smallinput" /></td>
		</tr>
		<tr>
			<td colspan="2">
				<table id="userdet" style="display: none; margin: 10px 0;" cellpadding="0" cellspacing="0" border="0" class="stdtable">
					<colgroup>
						<col class="con0" />
						<col class="con1" />
					</colgroup>
					<thead>
						<tr>
							<th class="head0">ID</th>
							<th class="head1">Address</th>
							<th class="head0">Can Request Codes</th>
							<th class="head1">Cellphone</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td id="idno"></td>
							<td id="address"></td>
							<td id="cansms"></td>
							<td id="cellphone"></td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</table>

	<%-- modal date selector --%>
	<div id="onceOffModal">
		<p>Please enter the date that the access codes are valid for.</p>
		<input type="input" name="dummy" />
		<p>Valid Date:&nbsp;&nbsp;<input type="text" name="ValidDate" /></p>
	</div>

	<ul class="hornav" style="visibility: hidden;">
		<li class="current"><a href="#guestlist">Guest Lists</a></li>
		<li><a href="#instantCodes">Once off Instant</a></li>
	</ul>

	<div id="contentwrapper" class="contentwrapper">

		<div id="guestlist" class="subcontent">

			<div id="OccupantGuestList" style="display: none;">

				<div class="contenttitle2" style="margin-top: 0;">
					<h3>Guest Lists</h3>
				</div>

				<div class="tableoptions">
					<button class="addGuestList radius3" title="table1">Add Guest List</button>
				</div>

				<table cellpadding="0" cellspacing="0" border="0" class="stdtable">
					<colgroup>
						<col class="con0" />
						<col class="con1" />
					</colgroup>
					<thead>
						<tr>
							<th class="head0">Name</th>
							<th class="head1">Functions</th>
						</tr>
					</thead>
					<tbody id="GuestListContainer">
					</tbody>
				</table>

			</div>
			<div id="GuestListOptionsContainer" style="display: none;"></div>
		</div>

		<div id="instantCodes" class="subcontent" style="display: none;">
			<div id="AccessCodeOnceOffContainer" style="width: 52%;">
				<div class="tableoptions">
					<button class="addOnceOffVisitor radius3" title="table1">Add Visitor</button>&nbsp;&nbsp;
					<%--Valid Date:&nbsp;&nbsp;<input type="text" class="isDatePicker" id="ValidDate" name="ValidDate" value="" />&nbsp;&nbsp;--%>
					Send to Occupant&nbsp;&nbsp;<input name="SendCodeToOccupant" type="checkbox" value="1" />
					Send to Visitor&nbsp;&nbsp;<input name="SendCodeToGuest" type="checkbox" value="1" />&nbsp;&nbsp;<a href="javascript:void(0);" style="display: inline-block;" class="btn btn_orange btn_mail radius50 createAccessCodes"><span>Select Date &amp; Generate Codes</span></a>
					&nbsp;&nbsp;<button class="stdbtn btn_orange Print">Print</button>
				</div>
				<div id="OnceOffTableContainer">
					<table cellpadding="0" cellspacing="0" border="0" class="stdtable">
						<colgroup>
							<col class="con0" />
							<col class="con1" />
							<col class="con0" />
						</colgroup>
						<thead>
							<tr>
								<th class="head0">Visitor Name</th>
								<th class="head1">Visitor Cellphone</th>
								<th class="head0">Functions</th>
							</tr>
						</thead>
						<tbody id="AccessCodeVisitorsContainer">
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<div style="visibility: hidden;">
		<input runat="server" name="SendCodeToOccupantHidden" id="SendCodeToOccupantHidden" type="hidden" value="1" />
		<input runat="server" name="SendCodeToGuestHidden" id="SendCodeToGuestHidden" type="hidden" value="1" />
	</div>
</asp:Content>
