﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Honeycomb.Custom;
using Honeycomb.Custom.Data;
using System.Data;
using System.Data.Entity;

namespace Honeycomb.Web.Pages.Security {
    public partial class IncidentReport : System.Web.UI.Page {

        protected long incidentID = 0;
        protected Incident incidentDB = null;
		protected string incidentOrignator = "N/A";
		protected List<IncidentUpdate> incidentNotes = null;

        protected void Page_Load(object sender, EventArgs e) {

            //check if we receive an int incident id
            if(Int64.TryParse(Request.QueryString["incidentid"],out incidentID)){
                using(var context = new Honeycomb_Entities()){
                   
                    incidentDB = context.Incident
                                .Include("IncidentType")
                                .Include("IncidentStatus")
                                .Include("Occupant")
                                .Include("IncidentUpdate")
                                .Include("LookUpOption")//source
                                .Include("LookUpOption1")//priority
                                .Include("User")//assigned to this user
                                .Where(i => i.ID == incidentID).FirstOrDefault();

                    if(incidentDB != null) {
                        //the incident exists, get other required data
						incidentOrignator = context.User.Where(u => u.ID == incidentDB.InsertedByID).Select(s => s.FirstName + " " + s.LastName).FirstOrDefault();

						if(incidentDB.IncidentUpdate.Count > 0) {
							incidentNotes = incidentDB.IncidentUpdate.OrderBy(o => o.InsertedOn).ToList();
						}
                    } 
                }
            }
        }
    }
}