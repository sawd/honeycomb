﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Honeycomb.Custom;

namespace Honeycomb.Web.Pages.Security {
    public partial class Visitors : Custom.BaseClasses.BasePage {
        protected void Page_Load(object sender, EventArgs e) {
            var systemSettings = CacheManager.SystemSetting;
            
            if(!systemSettings.SendCodeToOccupant) {
                SendCodeToOccupant.Disabled = true;
      
                
            } else {
                SendCodeToOccupant.Checked = true;
            }

            if(!systemSettings.SendCodeToGuest) {
                SendCodeToGuest.Disabled = true;
            } else {
                SendCodeToGuest.Checked = true;
            }
        }
    }
}