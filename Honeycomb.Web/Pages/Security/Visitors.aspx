﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Honeycomb.Master" AutoEventWireup="true" CodeBehind="Visitors.aspx.cs" Inherits="Honeycomb.Web.Pages.Security.Visitors" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
    <%-- References --%>
    <script type="text/javascript" src="/Scripts/plugins/chosen.jquery.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/charCount.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.tagsinput.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.validate.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.tmpl.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.smartWizard-2.0.min.js"></script>
    <script type="text/javascript" src="/Scripts/custom/tables.js"></script>
    <script type="text/javascript" src="/Scripts/custom/forms.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.validate.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.alerts.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.autogrow-textarea.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/ui.spinner.min.js"></script>
    <script type="text/javascript" src="/Services/WCFProperty.svc/js"></script>
    <%--<script type="text/javascript" src="/Services/WCFSecurity.svc/js"></script>--%>
    <script type="text/javascript" src="Visitors.aspx.js"></script>
    <%--Template--%>
    <script type="text/x-jquery-template" id="occupantsTemplate">
        <option value="${ID}" cansms="${CanSMS}" cell="${Cellphone}">${FirstName} ${LastName} - ${Cellphone}</option>
    </script>

    <!--pageheader-->
    <ul class="hornav">
        <li class="current"><a href="#grid">
            Access Codes</a></li>
    </ul>
    <div id="contentwrapper" class="contentwrapper">
        <div id="grid" class="subcontent">
             <input type="hidden" id ="sendCodeToOccupantHdn" value= ''/> 
            <table>
                <tr>
                    <td style="vertical-align:middle;padding-right:15px;">Select an occupant</td>
                    <td>
                        <input type="text" name="occupantSearch" style="width:200px;" class="smallinput" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">

                        <table id="userdet" style="display:none;margin:10px 0;" cellpadding="0" cellspacing="0" border="0" class="stdtable">
                            <colgroup>
                                <col class="con0" />
                                <col class="con1" />
                            </colgroup>
                            <thead>
                                <tr>
                                    <th class="head0">ID</th>
                                    <th class="head1">Address</th>
                                    <th class="head0">Can Request Codes</th>
                                    <th class="head1">Cellphone</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td id="idno"></td>
                                    <td id="address"></td>
                                    <td id="cansms"></td>
                                    <td id="cellphone"></td>
                                </tr>
                            </tbody>
                        </table>

                        <table id="visitorCodes" cellpadding="0" cellspacing="0" border="0" class="stdtable stdtablecb" style="display:none;margin:15px 0;">
                           <colgroup><col class='con0' /><col class='con1' /></colgroup>
                            <thead> <tr><th class='head0'>Visitor Name</th><th class='head1' >Cellphone</th><th class='head0'>Code</th><th class="head1">SMS Status</th></tr></thead>
                        </table>
                        <table id="visitorAED">
                            <tr>
                                <td>Visitor Name</td>
                                <td  style="display:none;">Visitor Cellphone</td>
                                <td>&nbsp;</td>
                               
                            </tr>
                            <tr>
                                <td><input type="text" name="visitorName" id="visitorName" /></td>
                                <td style="display:none;"><input type="text" class="numericOnly" name="visitorCellphone" runat="server" id="visitorCellphone"/></td>
                                <td><button onclick="Visitors.addNewVisitor(this);" type="button" class="stdbtn addButton">Add</button></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" height="15"></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table>
                            <tr><td>Send code to occupant</td><td width="15"></td><td><input runat="server" name="SendCodeToOccupant" id="SendCodeToOccupant" type="checkbox" value="1" /></td></tr>
                            <tr><td>Send code to visitor</td><td width="15"></td><td><input runat="server" name="SendCodeToGuest" id="SendCodeToGuest" type="checkbox" value="1" /></td></tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" height="15"></td>
                </tr>
                <tr>
                    <td>

                    </td>
                    <td align="right">
                           <button id="requestCodes" type="button" class="stdbtn btn_orange btn_mail">Request Codes</button>
                        <%--<input type="button" id="sendSMS" onclick="Visitors.sendsms(); return false;" value="Send" />--%>
                    </td>
                </tr>
            </table>
            
        </div>
    </div>
</asp:Content>
