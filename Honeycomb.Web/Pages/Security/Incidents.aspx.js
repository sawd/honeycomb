﻿/*global SecurityService,Entities,MaintenanceService,jAlert,jConfirm,Utils,Globals,window*/

//#region incidents
var Incident = {
	loadIncidents: function () {
		if (Incident.validateIncidentSearchForm()) {
			var $tab = Utils.tabs.getCurrentTab();
			var dateFromFilter = null;
			var dateToFilter = null;
			var incidentIDFilter = null;
			var incidentTypeIDFilter = null;
			var ownerNameFilter = null;
			var erfNumberFilter = null;
			var occupantNameFilter = null;

			dateFromFilter = $("#FromDate").val() === "" ? null : $("#FromDate").datepicker("getDate");
			dateToFilter = $("#ToDate").val() === "" ? null : $("#ToDate").datepicker("getDate");
			incidentTypeIDFilter = $("select[name=IncidentTypeIDFilter]").val() === "all" ? null : $("select[name=IncidentTypeIDFilter]").val();

			switch ($("select[name=filterBy]").val()) {
				case "IncidentID": {//ignore jslint
					incidentIDFilter = $("#filterValue").val() === "" ? null : $("#filterValue").val();
					break;
				}
				case "OwnerName": {
					ownerNameFilter = $("#filterValue").val() === "" ? null : $("#filterValue").val();
					break;
				}
				case "ErfNumber": {
					erfNumberFilter = $("#filterValue").val() === "" ? null : $("#filterValue").val();
					break;
				}
				case "OccupantName": {
					occupantNameFilter = $("#filterValue").val() === "" ? null : $("#filterValue").val();
					break;
				}
			}

			//process service response
			var response = function (_result) {

				$tab.unblock();

				Incident.paginate.TableID = "#IncidentTable";
				Incident.paginate.PaginationID = "#incidentPagination";
				Incident.paginate.TotalCount = _result.Count;

				Utils.bindDataToGrid("#IncidentTemplateContainer", "#IncidentGridTemplate", _result.EntityList);
				Incident.paginate.bindPagination();

			};

			$tab.block();
			//call service method
			SecurityService.WCFSecurity.GetIncidents(Incident.paginate, dateFromFilter, dateToFilter, incidentIDFilter, incidentTypeIDFilter, ownerNameFilter, erfNumberFilter, occupantNameFilter, response, Utils.responseFail);
		}
	},
	validateIncidentSearchForm: function () {
		//some simple validation to prevent errors when submitting the incident search.
		var isValid = true;
		var message = "";

		switch ($("select[name=filterBy]").val()) {
			case "IncidentID": {//ignore jslint
				
				if (isNaN($("#filterValue").val())) {
					isValid = false;
					message = "When searching by Incident Number please make sure to only enter numbers in the filter.";
				}

				break;
			}
			case "ErfNumber": {

				if (isNaN($("#filterValue").val())) {
					isValid = false;
					message = "When searching by Erf Number please make sure to only enter numbers in the filter.";
				}

				break;
			}
		}

		if (!isValid) {
			jAlert(message, "Validation Error");
		}

		return isValid;
	},
	loadIncidentTypes: function () {
		var $tab = Utils.tabs.getCurrentTab();
		var dfd = new $.Deferred();

		var typeResponse = function (_result) {
			Utils.bindDataToGrid($("select[name=IncidentTypeIDFilter]"), "#DDLTemplate", _result.EntityList);
			dfd.resolve();
		};

		SecurityService.WCFSecurity.GetIncidentTypes(typeResponse, function (e) { dfd.reject(); Utils.responseFail(e); });
		return dfd.promise();
	},
	loadAddTab: function () {
		var $tab = Utils.tabs.getCurrentTab();
		var incidentEntity = new Entities.Incident();

		incidentEntity.IncidentStatusID = 10000;

		Utils.bindDataToGrid("#logincident", "#IncidentTemplate", incidentEntity);
		Utils.tabs.initTab("#logincident");

		$tab.block();

		$.when(Incident.getSources(), Incident.getPriorities(), Incident.getTypes(), Incident.getIncidentUsers()).then(function () {
			$tab.unblock();
			Incident.bindFileUpload($tab);
			//set the autocomplete field for choosing a property or erf
			Incident.bindAutoComplete($tab);
			$tab.find("ul[name=Occupants]").tagit({
				placeholderText: "Select Occupant...",
				onlyAllowAutoComplete: true,
				useLabelforLabel: true,
				autocomplete: {
					delay: 0,
					minLength: 2,
					source: function (request, response) {
						var onSuccess = function (result) {
							if (result.EntityList[0] === null) { result.EntityList = []; }
							response(result.EntityList);
						};
						SecurityService.WCFSecurity.GetOccupantsAutoCompleteLookUp(request.term, onSuccess, Utils.responseFail);
					}
				}
			});
			$.uniform.update();
		});

	},
	loadIncidentTab: function (_incidentID) {

		var $tab = Utils.tabs.getCurrentTab();

		var response = function (_result) {

			if (_result.EntityList[0] !== null) {

				var incidentEntity = _result.EntityList[0];

				Utils.bindDataToGrid($tab.selector, "#IncidentTemplate", _result.EntityList[0]);
				Utils.tabs.initTab($tab.selector);

				$tab.block();

				$.when(Incident.getSources(), Incident.getPriorities(), Incident.getTypes(), Incident.getIncidentUsers(), Incident.getIncidentOccupants(_incidentID), Incident.getPropertyDesc(_incidentID)).then(function () {
					$tab.unblock();
					Incident.bindFileUpload($tab);
					//set the autocomplete field for choosing a property or erf
					Incident.bindAutoComplete($tab);
					//set the incident source type
					$tab.find("select[name=IncidentSourceID]").val(incidentEntity.IncidentSourceID);
					//set the incident type
					$tab.find("select[name=IncidentTypeID]").val(incidentEntity.IncidentTypeID);
					//set the priority type
					$tab.find("select[name=IncidentPriorityID]").val(incidentEntity.IncidentPriorityID);
					//set the person that the incident is assigned to
					$tab.find("select[name=AssignedUserID]").val(incidentEntity.AssignedUserID);
					//set the occupants who are associated with this incident
					$tab.find("ul[name=Occupants]").tagit({
						placeholderText: "Select Occupant...",
						onlyAllowAutoComplete: true,
						useLabelforLabel: true,
						autocomplete: {
							delay: 0,
							minLength: 2,
							source: function (request, response) {
								var onSuccess = function (result) {
									if (result.EntityList[0] === null) { result.EntityList = []; }
									response(result.EntityList);
								};
								SecurityService.WCFSecurity.GetOccupantsAutoCompleteLookUp(request.term, onSuccess, Utils.responseFail);
							}
						}
					});

					if (Incident.currrentIncidentOccupants.length > 0) {


						for (var i = 0; i < Incident.currrentIncidentOccupants.length; i++) {
							$tab.find('ul[name=Occupants]').tagit('createTag', Incident.currrentIncidentOccupants[i].value, null, null, Incident.currrentIncidentOccupants[i].label);
							//    $tab.find("select[name=Occupants] option[value=" + Incident.currrentIncidentOccupants[i].ID + "]").prop("selected", true);
						}

						//$tab.find("select[name=Occupants]").trigger("liszt:updated");
					}

					$tab.find("select[name=AssignedUserID]").trigger("liszt:updated");
					$.uniform.update();
				});
			} else {
				$.jGrowl("No incident found");
			}

		};

		SecurityService.WCFSecurity.GetIncident(_incidentID, response, Utils.responseFail);
	},
	openUpdateModal: function (_incidentID) {

		var $modalElement = $("#incidentModal");

		var incidentEntity = new Entities.IncidentUpdate();

		incidentEntity.IncidentID = _incidentID;

		//clear the current html in the modal
		$("#incidentModal").html("");

		//append the template to the modal dialog
		$("#UpdateIncidentTemplate").tmpl(incidentEntity).appendTo("#incidentModal");

		//intialize the controls on the element
		Utils.tabs.initTab("#incidentModal");

		//bind the upload component
		Incident.bindFileUpload($modalElement);

		//open the dialog
		$modalElement.dialog({
			resizable: false,
			modal: true,
			width: "450px",
			title: "Update Incident Progress",
			buttons: {
				"Update Incident": function () {

					var incidentEntity = new Entities.IncidentUpdate();
					$modalElement.mapToObject(incidentEntity, "name");
					$modalElement.block();

					var response = function (_result) {
						$modalElement.dialog("close");
						$modalElement.unblock();

						if (_result.EntityList[0].success) {
							jAlert("Successfully updated incident progress.", "Success");
						} else {
							jAlert("There was an error when attempting to save the update.", "Error");
						}
					};

					SecurityService.WCFSecurity.UpdateIncidentProgress(incidentEntity, response, Utils.responsefail);
				},
				Close: function () {
					$(this).dialog("close");
				}
			}
		});
		//end modal
	},
	markForClosure: function (_incidentID) {
		var $modalElement = $("#incidentModal");

		var incidentEntity = new Entities.Incident();

		//clear the current html in the modal
		$("#incidentModal").html("");

		//append the template to the modal dialog
		$("#ResolveIncidentTemplate").tmpl(incidentEntity).appendTo("#incidentModal");

		//intialize the controls on the element
		Utils.tabs.initTab("#incidentModal");

		//open the dialog
		$modalElement.dialog({
			resizable: false,
			modal: true,
			width: "450px",
			title: "Resolve Incident",
			buttons: {
				"Resolve Incident": function () {

					var response = function (_result) {
						$modalElement.dialog("close");
						$modalElement.unblock();

						if (_result.EntityList[0].success) {
							jAlert("Successfully resolved incident.", "Success");
						} else {
							jAlert("There was an error when attempting to resolve the incident.", "Error");
						}
					};

					if ($modalElement.find("textarea[name=ResolutionDescription]").val() !== "") {
						$modalElement.block();
						SecurityService.WCFSecurity.MarkIncidentForClosure(_incidentID, $modalElement.find("textarea[name=ResolutionDescription]").val(), response, Utils.responsefail);
					} else {
						jAlert("Please supply a resolution description", "Missing resolution date");
					}

				},
				Close: function () {
					$(this).dialog("close");
				}
			}
		});
		//end modal
	},
	closeIncident: function (_incidentID) {
		var $tab = Utils.tabs.getCurrentTab();

		var response = function (_result) {
			$tab.unblock();

			if (_result.EntityList[0].success) {
				jAlert("Successfully closed incident", "Incident closed");
			} else {
				jAlert("There was an error while attempting to close the incident.", "Error");
			}
		};

		$tab.block();
		SecurityService.WCFSecurity.CloseIncident(_incidentID, response, Utils.responseFail);
	},
	openIncident: function (_incidentID) {
		var $tab = Utils.tabs.getCurrentTab();

		var response = function (_result) {
			$tab.unblock();

			if (_result.EntityList[0].success) {
				jAlert("Successfully opened incident", "Incident opened");
			} else {
				jAlert("There was an error while attempting to open the incident.", "Error");
			}
		};

		$tab.block();
		SecurityService.WCFSecurity.OpenIncident(_incidentID, response, Utils.responseFail);
	},
	getSources: function () {
		var $tab = Utils.tabs.getCurrentTab();
		var dfd = new $.Deferred();

		var sourceResponse = function (_result) {
			Utils.bindDataToGrid($tab.find("select[name=IncidentSourceID]").selector, "#DDLTemplate", _result.EntityList);
			dfd.resolve();
		};

		MaintenanceService.WCFMaintenance.GetLookUpOptionsByID(Utils.LookUpEnum.IncidentSource, sourceResponse, function (e) { dfd.reject(); Utils.responseFail(e); });
		return dfd.promise();
	},
	getTypes: function () {
		var $tab = Utils.tabs.getCurrentTab();
		var dfd = new $.Deferred();

		var typeResponse = function (_result) {
			Utils.bindDataToGrid($tab.find("select[name=IncidentTypeID]").selector, "#DDLTemplate", _result.EntityList);
			dfd.resolve();
		};

		SecurityService.WCFSecurity.GetIncidentTypes(typeResponse, function (e) { dfd.reject(); Utils.responseFail(e); });
		return dfd.promise();
	},
	getPriorities: function () {
		var $tab = Utils.tabs.getCurrentTab();
		var dfd = new $.Deferred();

		var priorityResponse = function (_result) {
			Utils.bindDataToGrid($tab.find("select[name=IncidentPriorityID]").selector, "#DDLTemplate", _result.EntityList);
			dfd.resolve();
		};

		MaintenanceService.WCFMaintenance.GetLookUpOptionsByID(Utils.LookUpEnum.IncidentPriority, priorityResponse, function (e) { dfd.reject(); Utils.responseFail(e); });
		return dfd.promise();
	},
	getOccupants: function () {
		var $tab = Utils.tabs.getCurrentTab();
		var dfd = new $.Deferred();

		var occupantResponse = function (_result) {
			Utils.bindDataToGrid($tab.find("select[name=Occupants]").selector, "#occupantsTemplate", _result.EntityList);
			$tab.find("select[name=Occupants]").trigger("liszt:updated");
			dfd.resolve();
		};

		SecurityService.WCFSecurity.GetOccupantsLookUp(occupantResponse, function (e) { dfd.reject(); Utils.responseFail(e); });
		return dfd.promise();
	},
	getIncidentUsers: function () {
		var $tab = Utils.tabs.getCurrentTab();
		var dfd = new $.Deferred();

		var response = function (_result) {
			Utils.bindDataToGrid($tab.find("select[name=AssignedUserID]").selector, "#occupantsTemplate", _result.EntityList);
			$tab.find("select[name=AssignedUserID]").trigger("liszt:updated");
			dfd.resolve();
		};

		SecurityService.WCFSecurity.GetIncidentUsers(response, function (e) { dfd.reject(); Utils.responseFail(e); });
		return dfd.promise();
	},
	getIncidentOccupants: function (_incidentID) {
		var dfd = new $.Deferred();

		var response = function (_result) {

			if (_result.EntityList !== null) {
				Incident.currrentIncidentOccupants = _result.EntityList;
			}

			dfd.resolve();
		};

		SecurityService.WCFSecurity.GetIncidentOccupants(_incidentID, response, function (e) { dfd.reject(); Utils.responseFail(e); });
		return dfd.promise();
	},
	currrentIncidentOccupants: [],
	getPropertyDesc: function (_incidentID) {
		var $tab = Utils.tabs.getCurrentTab();
		var dfd = new $.Deferred();

		var response = function (_result) {

			if (_result.EntityList !== null) {
				$tab.find("input[name=PropertyField]").val(_result.EntityList[0]);
			}

			dfd.resolve();
		};

		SecurityService.WCFSecurity.GetIncidentPropertyDesc(_incidentID, response, function (e) { dfd.reject(); Utils.responseFail(e); });
		return dfd.promise();
	},
	saveIncident: function () {
		var $tab = Utils.tabs.getCurrentTab();

		if (Utils.validation.validateForm($tab.selector, "Missing required fields.", false, true)) {

			var incidentEntity = new Entities.Incident();

			$tab.mapToObject(incidentEntity, "name");
			$tab.block();

			var response = function (_result) {

				$tab.unblock();

				if (_result.EntityList[0] !== null) {
					if (_result.EntityList[0].success) {
						jAlert("Successfully saved incident");
					} else {
						jAlert(_result.EntityList[0].message);
					}
				}
			};
			var tags = $tab.find("ul[name=Occupants]").tagit('assignedTags');

			if (parseInt(incidentEntity.ID, 10) !== 0) {
				SecurityService.WCFSecurity.UpdateIncident(incidentEntity, tags, response, function (e) { $tab.unblock(); Utils.responseFail(e); });
			} else {
				SecurityService.WCFSecurity.AddIncident(incidentEntity, tags, response, function (e) { $tab.unblock(); Utils.responseFail(e); });
			}
		}
	},
	bindAutoComplete: function (_tabToBind) {

		///// SELECT WITH SEARCH /////
		$(_tabToBind.selector + " input[name=PropertyField]").autocomplete({
			source: function (request, response) {
				var requestProperties = function (result) {
					if (result.EntityList[0] === null) { result.EntityList = []; }
					response(result.EntityList);
				};

				//call the service method to return both matching erven and properties
				SecurityService.WCFSecurity.GetProperties(request.term, requestProperties, Utils.responseFail);
			}
            , minLength: 2
            , select: function (event, ui) {
            	$(_tabToBind.selector + " input[name=PropertyField]").val(ui.item.label);

            	if (ui.item.type === "property") {
            		_tabToBind.find("input[name=PropertyID]").val(ui.item.value);
            		_tabToBind.find("input[name=ErfID]").val("");
            	} else {
            		_tabToBind.find("input[name=ErfID]").val(ui.item.value);
            		_tabToBind.find("input[name=PropertyID]").val("");
            	}

            	return false;
            }
		});

	},
	bindFileUpload: function (_tabToBindTo) {
		//init file uploader
		$(_tabToBindTo.selector + " .uploadFile").fineUploader({
			uploaderType: 'basic',
			multiple: false,
			autoUpload: true,
			button: $(_tabToBindTo.selector + " .uploadButton"),
			request: {
				endpoint: "/Pages/UploadFile.ashx"
			}
		}).on('complete', function (event, id, filename, responseJSON) {//ignore jslint
			_tabToBindTo.find("input[name=FileID]").val(responseJSON.fileid);
			_tabToBindTo.unblock();
			$.jGrowl("File uploaded successfully");
		})//ignore jslint
        .on("submit", function (event, id, filename) {//ignore jslint
        	_tabToBindTo.block();
        })//ignore jslint
        .on("error", function (event, id, filename, reason) {//ignore jslint
        	_tabToBindTo.unblock();
        	$.jGrowl("Error Uploading file");
        });//ignore jslint
	},
	paginate: null
};
//#endregion

//page events
var PageEvents = {
	load: function () {
		Incident.paginate = new Globals.paginationContainer("ErfNumber", 10, Incident.loadIncidents);

		Incident.loadIncidents();

		$.when(Incident.loadIncidentTypes()).then(function () {
			$("select[name=IncidentTypeIDFilter]").prepend("<option value='all'>All Types</option>");
			$.uniform.update();
		});

		PageEvents.bindEvents();
	},
	bindEvents: function () {
		//bind tab click to the add tab
		$('.bodywrapper').on('Honeycomb.tabLoad', '#logincident', Incident.loadAddTab);
		$('.bodywrapper').on('Honeycomb.tabLoad', '#viewincidents', Incident.loadIncidents);
		//bind save function to save incident button
		$('.bodywrapper').on('click.Save', "button[name=topSave]", Incident.saveIncident);
		//bind edit function to incident edit button
		$(".bodywrapper").on("click.editIncident", ".editIncident", PageEvents.editIncident_click);
		//bind update incident investigation method to button
		$(".bodywrapper").on("click.updateIncidentButt", ".updateIncidentButt", PageEvents.updateProgress_click);
		$(".bodywrapper").on("click.setIncidentClosureButt", ".setIncidentClosureButt", PageEvents.markForClosure_click);
		$(".bodywrapper").on("click.setIncidentClosedButt", ".setIncidentClosedButt", PageEvents.closeIncident_click);
		$(".bodywrapper").on("click.setIncidentOpenButt", ".setIncidentOpenButt", PageEvents.openIncident_click);
		$(".bodywrapper").on("click.viewIncident", ".viewIncident", PageEvents.openIncidentReport_click);

		$("#searchButt").click(function (e) {
			//NB: Reset the pagination so that the current page is set to page 1 etc
			Incident.paginate.reset();

			Incident.loadIncidents();
		});

	},
	editIncident_click: function (e) {
		e.preventDefault();
		var editButton = $(this);
		Utils.tabs.loadTab($(this).attr("tabtitle"), "incident" + $(this).attr("incidentid"), function () { Incident.loadIncidentTab(editButton.attr("incidentid")); });
	},
	updateProgress_click: function (e) {
		e.preventDefault();
		var updateButton = $(e.currentTarget);

		if (parseInt(updateButton.attr("incidentid"),10) > 0) {
			Incident.openUpdateModal(updateButton.attr("incidentid"));
		} else {
			console.log(updateButton);
			console.log("IncidentID" + updateButton.attr("incidentid"));
			jAlert("There was an error retrieving the incident details. Please close the incident tab and try again.");
		}
	},
	markForClosure_click: function (e) {
		e.preventDefault();
		var updateButton = $(this);
		jConfirm("Are you sure you wish to resolve this incident?", "Resolve Incident", function (_confirm) {
			if (_confirm) {
				Incident.markForClosure(updateButton.attr("incidentid"));
			}
		});
	},
	closeIncident_click: function (e) {
		e.preventDefault();
		var updateButton = $(this);
		jConfirm("Are you sure you wish to close this incident?", "Close incident", function (_confirm) {
			if (_confirm) {
				Incident.closeIncident(updateButton.attr("incidentid"));
			}
		});
	},
	openIncident_click: function (e) {
		e.preventDefault();
		var updateButton = $(this);
		jConfirm("Are you sure you wish to open this incident?", "Open incident", function (_confirm) {
			if (_confirm) {
				Incident.openIncident(updateButton.attr("incidentid"));
			}
		});
	},
	openIncidentReport_click: function (e) {
		e.preventDefault();
		window.open("IncidentReport.aspx?incidentID=" + $(this).attr("incidentid"), "_blank");
	}
};

//page load
$(function () {
	PageEvents.load();
});
