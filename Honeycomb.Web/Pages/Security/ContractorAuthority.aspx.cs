﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Honeycomb.Custom.Data;
using Honeycomb.Security;
using Honeycomb.Custom;

namespace Honeycomb.Web.Pages.Security {
	public partial class ContractorAuthority : Custom.BaseClasses.BasePage {
		protected void Page_Load(object sender, EventArgs e) {

			if(!IsPostBack) {
				WCFSecurity securityInst = new WCFSecurity();
				var users = securityInst.GetIncidentUsers().EntityList;

				foreach(var user in users) {
					UserIDSelect.Items.Add(new ListItem() {
						Text = user.FirstName + " " + user.LastName,
						Value = user.ID.ToString(),
						Selected = CacheManager.SystemSetting.ContractorAuthorityID == user.ID
					});
				}
			}
		}

		protected void AssignExitAuthority_ServerClick(object sender, EventArgs e) {

			using(var context = new Honeycomb_Entities()){
				CacheManager.SystemSetting.ContractorAuthorityID = long.Parse(UserIDSelect.Value);
				var systemSettings = context.SystemSetting.FirstOrDefault();
				if(systemSettings != null) {
					systemSettings.ContractorAuthorityID = long.Parse(UserIDSelect.Value);
					context.SaveChanges();
				}
			}

		}
	}
}