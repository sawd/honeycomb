﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="IncidentReport.aspx.cs" Inherits="Honeycomb.Web.Pages.Security.IncidentReport" %>
<%@ Import Namespace="Honeycomb.Custom" %>
<%@ Import Namespace = "System.Data" %>
<%@ Import Namespace = "System.Data.Entity" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" media="screen" href="/Theme/css/reports.css"/>
    <link rel="stylesheet" media="print" href="/Theme/css/reports.css"/>
    <style type="text/css">
        #historyLeft table tr td {
            padding-bottom:5px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    
    <div id="reportContainer">
        <%if(incidentDB != null){ %>
        <h1>Incident Report : <%= incidentDB.Title %></h1>
        <div id="topDetails" class="clearMe">
            <table>
                <tr>
                    <td valign="top">
                        <div id="historyLeft" class="floatLeft">
                            <table>
                                 <tr>
                                    <td><strong>Incident No:</strong></td><td width="20"></td><td>INC<%= incidentDB.ID %></td>
                                </tr>
                                <tr>
                                    <td><strong>Assigned To:</strong></td><td width="20"></td><td><%= incidentDB.User.FirstName %> <%= incidentDB.User.LastName %></td>
                                </tr>
								<tr>
                                    <td><strong>Captured By:</strong></td><td width="20"></td><td><%= incidentOrignator %></td>
                                </tr>
                                <tr>
                                    <td><strong>Source:</strong></td><td width="20"></td><td><%= incidentDB.LookUpOption.Name %></td>
                                </tr>
                                <tr>
                                    <td><strong>Type:</strong></td><td width="20"></td><td><%= incidentDB.IncidentType.Name %></td>
                                </tr>
                                <tr>
                                    <td><strong>Priority:</strong></td><td width="20"></td><td><%= incidentDB.LookUpOption1.Name %></td>
                                </tr>
                                <tr>
                                    <td><strong>Date:</strong></td><td width="20"></td><td><%= incidentDB.IncidentDate.ToString("dd/MM/yyyy") %></td>
                                </tr>
                                <% if(incidentDB.ResolutionDate != null){ %>
                                <tr>
                                    <td><strong>Resolution Date:</strong></td><td width="20"></td><td><%= incidentDB.ResolutionDate.Value.ToString("dd/MM/yyyy") %></td>
                                </tr>
                                <%} %>
                                <tr>
                                    <td><strong>Time:</strong></td><td width="20"></td><td><%= incidentDB.IncidentTime %></td>
                                </tr>
                                <%if(incidentDB.FileID != null){ %>
                                <tr>
                                    <td><strong>View Photo:</strong></td><td width="20"></td><td><a href="/Pages/DownloadFile.ashx?FileRef=<%= incidentDB.FileID %>" target="_blank">View Photo</a></td>
                                </tr>
                                <%} %>
                                <tr>
                                    <td><strong>Status:</strong></td><td width="20"></td><td><%= incidentDB.IncidentStatus.Name %></td>
                                </tr>
                            </table>
                        </div>
                    </td>
                    <td width="30"></td>
                    <td valign="top">
                        <div id="historyRight" class="floatRight">

                            <h4>Parties Involved</h4>
                            <p>
                                <%if(incidentDB.Occupant.Count() > 0){ %>
                                <%= String.Join(",",incidentDB.Occupant.Select(s => s.FirstName + " " + s.LastName).ToArray()) %>
                                <%} %>
                            </p>
                            <%if(!string.IsNullOrEmpty(incidentDB.InvolvedPersonsOther)) {
                                  Response.Write("<strong>Other</strong><p>" + incidentDB.InvolvedPersonsOther + "</p>");
                              } %>
                            <h4>Description</h4>
                            <%= incidentDB.Description %>
                            <% if(!string.IsNullOrEmpty(incidentDB.ResolutionDescription)){ %>
                            <h4>Resolution Description</h4>
                            <%= incidentDB.ResolutionDescription %>
                            <%} %>
                        </div>
                    </td>
                </tr>
            </table>
            </div>
            <div class="separator"></div>
            <div id="incidentUpdates">
                <%
              if(incidentNotes != null) {
                  foreach(var incidentUpdateItem in incidentNotes) {
                    %>
                    <div class="incidentUpdate">
                        <div class="incidentDateHeader"><%= incidentUpdateItem.InsertedOn.Value.ToString("dd/MM/yyyy - HH:mm:ss")%> - <%= incidentUpdateItem.InsertedBy%></div>
                        <div class="updateContainer clearMe">
                            <div class="incidentDesc floatLeft"><%= incidentUpdateItem.Description%></div>
                            <div class="incidentPhoto floatRight">
                                <%if(incidentUpdateItem.FileID != null) { %>
                                    <a href="/Pages/DownloadFile.ashx?FileRef=<%=incidentUpdateItem.FileID%>" target="_blank">View photo</a>
                                <%} %>
                            </div>
                        </div>
                    </div>
                    <%           
                  }
              }
                     %>
            </div>
        <%}else { %>
        <p>The incident cannot be located please try again.</p>
        <%} %>
        <div align="right" style="padding:10px 0 0 0;"><%=System.DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") %></div>
    </div>
    </form>
</body>
</html>
