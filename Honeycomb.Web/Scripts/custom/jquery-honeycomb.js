﻿/*global jQuery,events,methods,SecurityService,Utils,jConfirm,jAlert,dateFormat*/
//custom honeycomb jquery plugins

//Honeycomb Tag Holder Access Group Plugin
//Will update and display current Tag Holders Acess Group in Impro (based off of an ID no in an input field)
//access will depend on assigned user roles in the security service
(function ($) {

	var methods = {
		init: function (options) {
			//plugin defaults
			$.extend($.fn.tagHolder.defaults, options);
			//loop through each input and append a button after the element
			return this.filter("input[type=text]").each(function () {

				var $button = $("<a href=\"javascript:void(0);\" class=\"btn btn3 btn_lock tagHolderButt\" title=\"Set Access Permissions\"></a>");
				$(this).after($button).addClass("tagHolderInput");
				$(this).data("button", $button);
				$(this).data("enabled", true);
				//assign click event on button and store a reference to the input
				$button.click(events.tagholderClick).data("sourceInput",$(this));
			});
		},
		updateAccessGroup: function (_tagHolderID) { 

			if (parseInt($("#TagHolderDialog select[name=AccessGroup]").val(),10) !== 0) {//$("#TagHolderDialog input[name=StartDate]").val() !== "" && $("#TagHolderDialog input[name=EndDate]").val()

				jConfirm("Are you sure you wish to update the Tag Holders Access Group? <br/>Please note: updating the tag holders group will remove any other groups assigned to the tag holder.", "Confirm Access Group Update", function (_confirm) {
					if (_confirm) {
						//save the setting that the user has chosen
						var response = function (_result) {
							$("#TagHolderDialog").unblock();
							$.fn.tagHolder.defaults.onComplete.call(this);
							$("#TagHolderDialog").dialog("close");
							jAlert(_result.EntityList[0], "Info");
						};

						$("#TagHolderDialog").block();
						SecurityService.WCFSecurity.SetTagHolderAccessGroup(_tagHolderID, $("#TagHolderDialog select[name=AccessGroup]").val(), $("#TagHolderDialog input[name=StartDate]").datepicker("getDate"), $("#TagHolderDialog input[name=EndDate]").datepicker("getDate"), response, Utils.responseFail);
					}
				});
			} else {
				jAlert("Please select an access group to continue.", "Warning");
			}

		},
		loadAccessGroups: function (_tagHolderID) {
			//intialize the controls on the element
			Utils.tabs.initTab("#TagHolderDialog");

			var response = function (_response) {
				$.tmpl("<option value=\"${TAGRP_No}\">${TAGRP_Name}</option>", _response.EntityList).appendTo("#TagHolderDialog select[name=AccessGroup]");
				$("#TagHolderDialog select[name=AccessGroup]").prepend("<option value='0'>-- Select one --</option>");


				$.when(methods.getTagHolderCache(_tagHolderID)).then(function () {
					$("#TagHolderDialog").unblock();
					$.uniform.update();
				});

			};

			$("#TagHolderDialog").block();
			SecurityService.WCFSecurity.GetTagAccessGroups(response, Utils.responseFail);
		},
		getTagHolderCache: function (_tagHolderID) {
			var dfd = new $.Deferred();

			var cacheresponse = function (_result) {

				if (_result.EntityList[0] !== null) {
					$("#TagHolderDialog select[name=AccessGroup]").val(_result.EntityList[0].AccessGroupID);

					if (_result.EntityList[0].StartDate !== null) {
						$("#TagHolderDialog #StartDate").val(dateFormat(_result.EntityList[0].StartDate, "dd/mm/yyyy"));
					}

					if (_result.EntityList[0].ExpiryDate !== null) {
						$("#TagHolderDialog #EndDate").val(dateFormat(_result.EntityList[0].ExpiryDate, "dd/mm/yyyy"));
					}
					
				}

				dfd.resolve();
			};

			SecurityService.WCFSecurity.GetTagHolderAccess(_tagHolderID, cacheresponse, function (e) { dfd.reject(); Utils.responseFail(e); });
			return dfd.promise();
		},
		showTagHolderGroupsDialog: function (_tagHolderID) {
			
			$("<div id=\"TagHolderDialog\" class=\"tagHolderDialog\"><p>Last known access details set in Honeycomb system.</p><div class='form'><p><label>Access Group</label><span class=\"field\"><select style\"width:150px;\" class=\"uniformselect\" name=\"AccessGroup\"></select></span></p><p><label>Start Date:</label><span class=\"field\"><input type=\"text\" class=\"isDatePicker\" id=\"StartDate\" name=\"StartDate\"></span></p><p><label>Expiry Date:</label><span class=\"field\"><input type=\"text\" class=\"isDatePicker\" id=\"EndDate\" name=\"EndDate\">&nbsp;&nbsp;If blank access will not expire.</span></p></div></div>").dialog({
				resizable: true,
				modal: true,
				width: "420px",
				title: "Edit Tag Holder Access Group",
				buttons: {
					"Update Access Group": function () {
						methods.updateAccessGroup(_tagHolderID);
					},
					Cancel: function () {
						$(this).dialog("close");
					}
				},
				close: function (event, ui) {
					$("#TagHolderDialog").remove();
				},
				open: function (event, ui) {
					methods.loadAccessGroups(_tagHolderID);
				}
			});
		},
		disable: function () {
			var $button = $(this).data("button");
			$(this).data("enabled",false);
			$button.attr("disabled","disabled");
		},
		enable: function () {
			var $button = $(this).data("button");
			$(this).data("enabled", true);
			$button.attr("disabled", "false");
		}
	};

	var events = {
		tagholderClick: function (e) {
			var $this = $(e.currentTarget);
			var $sourceInput = $this.data("sourceInput");

			if ($sourceInput.val() !== "" && $sourceInput.data("enabled")) {
				$.fn.tagHolder("showTagHolderGroupsDialog", $sourceInput.val());
			}
		}
	};

	$.fn.tagHolder = function (method) {

		// Method calling logic
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if (typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist on jQuery.tagHolder');
		}
	};

	$.fn.tagHolder.defaults = {
		"buttonField": "tagAccessButton",
		"displayContainer": "none",
		onComplete: function () { }
	};

}(jQuery));