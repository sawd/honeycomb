﻿/*global jQuery,Metadata,MetadataService */

//files plugin
(function ($) {
    var methods = {
        init: function (options) {
            //plugin defaults
            $.extend($.fn.files.defaults, options);
            //initialise the dialog
            if ($("#orderFilesDialog").length == 0) {

                $("body").append("<div id=\"orderFilesDialog\"><div class='orderfilesForm'></div></div>");
                $("body").append("<div id=\"addOrderFilesDialog\"></div>");

                var $uploadButton = $("<a style='margin-bottom: 12px;float: right;' class=\"btn btn3 btn_add addNewFile\" href=\"#\" title=\"Add New File\"></a>");

                $("#orderFilesDialog .orderfilesForm").append($uploadButton).append(tmpl.filesTableTmpl.render());

                $uploadButton.click(events.uploadButton_CLICK);

                $("#orderFilesDialog").dialog({
                    modal: true,
                    width: 800,
                    minHeight: 500,
                    autoOpen: false,
                    title: "Files",
                    buttons: {
                        "Close": function () {
                            $(this).dialog("close");
                        }
                    },
                    open: function () {
                        //call functions to populate the table with the current files
                        events.dialog_OPEN($(this));
                    },
                    close: function () {
                        $("#addOrderFilesDialog").dialog("close");
                    }
                });

                $("#addOrderFilesDialog").dialog({
                    modal: true,
                    width: 310,
                    minHeight: 200,
                    autoOpen: false,
                    resizable: false,
                    title: "Add new file",
                    buttons: {
                        "Add File": events.addFile_CLICK,
                        "Cancel Upload": function () {
                            $(this).dialog("close");
                        }
                    },
                    open: function () {
                        //call functions to populate the table with the current files
                        $("#addOrderFilesDialog").html(tmpl.addFileTmpl.render()).find("input").uniform();
                    },
                    beforeClose: function () {
                        return $(this).data("isUploading") ? false : true;
                    }
                });

                //assign event handlers to grid buttons
                $("#orderFilesDialog").on("click.deleteFile", ".deleteFile", events.deleteFile_CLICK);
            }
            //loop through each item and check that they are populated with the correct data attributes, if so assign click handlers
            return this.each(function () {
                var $this = $(this);

                if (!$this.attr("init")) {
                    $this.attr("init", true);

                    if (utils.isDefined($this.attr("data-type")) && utils.isDefined($this.attr("data-entityID"))) {
                        //store the options on the object
                        $this.data("options", $.extend({}, $.fn.files.defaults, { type: $this.attr("data-type"), entityID: $this.attr("data-entityID") }));
                        $this.click(events.filesButton_CLICK);

                    } else {
                        console.log("Element: '" + $this.selector + "' does not have a \"data-type\" attribute or a \"data-entityID\" attribute.");
                    }
                }
            });
        },
        GetFiles: function (entityID, type) {
            if (type !== "" && !isNaN(entityID)) {
                var $dialog = $("#orderFilesDialog");
                $dialog.block();
                PropertyService.WCFProperty.GetFiles(entityID, type, events.GetFiles_SUCCESS, Utils.responseFail);
            }
        },
        validateAdd: function () {
            var addDialog = $("#addOrderFilesDialog");
            var isvalid = true;
            //check there is a file
            if (addDialog.find("input[type=file]")[0].files.length === 0) {
                $.jGrowl("Please select a file");
                isvalid = false;
            } else {
                var file = addDialog.find("input[type=file]")[0].files[0];

                if (file !== null) {
                    if (typeof file.size !== "undefined") {
                        if (!isNaN(file.size)) {
                            if ((file.size / 1024 / 1024) > $.fn.files.defaults.filesize) {
                                isvalid = false;
                                $.jGrowl("The file you have specified is too large, please upload a smaller file. Limit is " + $.fn.files.defaults.filesize + " MB");
                            }
                        }
                    }
                }
            }

            if (addDialog.find("input[type=text]").val().length === 0) {
                $.jGrowl("Please provide a name for your file");
                isvalid = false;
            }

            return isvalid;
        },
        addFile: function () {

            var currentSettings = utils.getCurrentSettings();
            var addDialog = $("#addOrderFilesDialog");
            var client = new XMLHttpRequest();
            var progressBar = addDialog.find("progress");
            /* Create a FormData instance */
            var formData = new FormData();
            /* Add the file */
            formData.append("upload", addDialog.find("input[type=file]")[0].files[0]);
            formData.append("type", currentSettings.type);
            formData.append("entityID", currentSettings.entityID);
            formData.append("label", addDialog.find("input[type=text]").val());

            /*formData.append("upload", fileDOMElement.files[0]);
            formData.append("entityID", fileDOMElement.getAttribute("data-entityID"));
            formData.append("type", "file");
            formData.append("label", "vendorFile");*/

            if (progressBar.length > 0) {
                // Listen to the upload progress.
                progressBar.css("visibility", 'visible');

                client.upload.onprogress = function (e) {
                    if (e.lengthComputable) {
                        progressBar[0].value = (e.loaded / e.total) * 100;
                        progressBar[0].textContent = progressBar.value; // Fallback for unsupported browsers.
                    }
                };
            }

            /* Check the response status */
            client.onreadystatechange = function () {
                if (client.readyState == 4 && client.status == 200) {
                    //addDialog.unblock();
                    addDialog.data("isUploading", false);
                    $(".ui-dialog-buttonpane button:contains('Add File')").button("enable");
                    $(".ui-dialog-buttonpane button:contains('Cancel Upload')").button("enable");

                    var response = $.parseJSON(client.response).Data;

                    if (response.success) {
                        addDialog.dialog("close");
                        $.jGrowl(response.message);
                        methods.GetFiles(currentSettings.entityID, currentSettings.type);
                    } else {
                        $.jGrowl(response.message);
                    }
                }
            }

            //addDialog.block();
            $(".ui-dialog-buttonpane button:contains('Add File')").button("disable");
            $(".ui-dialog-buttonpane button:contains('Cancel Upload')").button("disable");
            addDialog.data("isUploading", true);
            client.open("POST", "/httphandlers/upload.ashx", true);
            client.send(formData);  /* Send to server */
        }
    };

    var events = {
        uploadButton_CLICK: function (e) {
            e.preventDefault();
            $("#addOrderFilesDialog").dialog("open");
        },
        filesButton_CLICK: function (e) {
            e.preventDefault();
            //store the current button context on the dialog
            $("#orderFilesDialog").data("currentContext", $(this));
            //open the dialog
            $("#orderFilesDialog").dialog("open");
        },
        deleteFile_CLICK: function (e) {
            e.preventDefault();
            var thiz = $(this);

            jConfirm("Are you sure you wish to delete this file?", "Delete File?", function (confirm) {
                if (confirm) {
                    $("#orderFilesDialog").block();
                    PropertyService.WCFProperty.DeleteFile(thiz.attr("data-fileid"), events.DeleteFile_SUCCESS, Utils.responseFail, $("#orderFilesDialog"));
                }
            });
        },
        addFile_CLICK: function (e) {

            if (methods.validateAdd()) {
                methods.addFile();
            }
        },
        dialog_OPEN: function (context) {
            var currentFileContext = $("#orderFilesDialog").data("currentContext");
            var currentOptions = currentFileContext.data("options");
            methods.GetFiles(currentOptions.entityID, currentOptions.type);
        },
        GetFiles_SUCCESS: function (result) {
            var $dialog = $("#orderFilesDialog");
            //bind the results
            if (orderFilesDialog.EntityList !== null) {
                $dialog.find(".orderfilesTemplateContainer").html(tmpl.fileListTmpl.render(result.EntityList));
                $dialog.unblock();
            }
        },
        DeleteFile_SUCCESS: function (response, context) {
            context.unblock();
            if (response.EntityList !== null) {
                if (response.EntityList[0].success) {
                    var currentFileContext = $("#orderFilesDialog").data("currentContext");
                    var currentOptions = currentFileContext.data("options");
                    $.jGrowl("File deleted successfully.");
                    methods.GetFiles(currentOptions.entityID, currentOptions.type);
                } else {
                    jAlert("There was an error when attempting to delete the file, please try again.", "Error Deleting File");
                }
            }
        }
    };

    var tmpl = {
        fileListTmpl: $.templates("<tr><td>{{>Label}}</td><td>{{>~formatDate(InsertedOn,'dd/mm/yyyy HH:MM')}}</td><td><a class=\"btn btn3 btn_document\" title=\"Download the file\" href=\"/httphandlers/download.ashx?id={{:FileID}}\" target=\"_blank\" style=\"margin-right: 10px;margin-bottom:5px;\"></a><a style=\"margin-bottom:5px;\" href=\"#\" data-fileid=\"{{:FileID}}\" class=\"btn btn3 btn_trash deleteFile\" title\"Delete the file\"></a></td></tr>"),
        filesTableTmpl: $.templates("<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" class=\"orderfilesTable stdtable stdtablecb\"><colgroup><col class=\"con0\" /><col class=\"con1\" /><col class=\"con0\" /></colgroup><thead><tr><th class=\"head0\" width=\"521\">Label</th><th class=\"head1\">Uploaded</th><th class=\"head0\" width=\"85\"></th></tr></thead><tbody class=\"orderfilesTemplateContainer\"></tbody></table>"),
        addFileTmpl: $.templates("<p><label for=\"filename\">File Upload</label><input type=\"file\" name=\"filename\" /></p><p><label for=\"label\">Label</label><input type=\"text\" name=\"label\" /></p><p><progress min=\"0\" max=\"100\" value=\"0\">0% complete</progress></p>")
    };

    var utils = {
        isDefined: function (chkVariable) {
            if (typeof chkVariable === "undefined") {
                return false;
            } else {
                return true;
            }
        },
        getCurrentSettings: function () {

            var currentFileContext = $("#orderFilesDialog").data("currentContext");
            var currentOptions = currentFileContext.data("options");

            return currentOptions || null;
        }
    };

    $.fn.files = function (method) {
        // Method calling logic
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.files');
        }
    };

    $.fn.files.defaults = {
        type: "",
        entityID: 0,
        filesize: 50
    };

})(jQuery);