﻿/*global window,events,DashboardService,Utils,dateFormat,jQuery,Globals,countUp*/
; (function ($, window, document, undefined) {//ignore jslint

    // define your widget under a namespace of your choice
    //  with additional parameters e.g.
    // $.widget( "namespace.widgetname", (optional) - an
    // existing widget prototype to inherit from, an object
    // literal to become the widget's prototype );

    $.widget("honeycomb.dashboardWidget", {

        //Options to be used as defaults
        options: {
            showRefresh: true,
            color: "#3366cc"
        },

        _handlers: {
            widgetButtonClick: function (event) {
                console.log("honeycomb.dashboardWidget widgetButtonClick called");

                //trigger the renderWidget callback and pass through the type and the element in the html which will contain the graph
                this._triggerRenderWidget(event);
            }
        },
        //Setup widget (eg. element creation, apply theming
        // , bind events etc.)
        _create: function () {

            // _create will automatically run the first time
            // this widget is called. Put the initial widget
            // setup code here, then you can access the element
            // on which the widget was called via this.element.
            // The options defined above can be accessed
            // via this.options this.element.addStuff();

            if (!this.options.showRefresh) {
                this.element.find(".graphButtons .refresh").hide();
            }
            if (this.options.color !== "") {
                this.element.find(".innerContainer").css("background-color", this.options.color);
            }

            this.element.on("click.widget", ".innerContainer", $.proxy(this._handlers.widgetButtonClick, this));
            //now that we have registered the handlers we can trigger the render callback
            this._triggerRenderWidget();
        },

        _triggerRenderWidget: function (event) {

            //trigger the renderWidget callback and pass through the type and the element in the html which will contain the graph
            this._trigger("renderWidget", event, {
                container: this.element.find('.innerContainer')
            });
        },

        refresh: function () {
            this._triggerRenderWidget();
        },
        // Destroy an instantiated plugin and clean up
        // modifications the widget has made to the DOM
        destroy: function () {

            // this.element.removeStuff();
            // For UI 1.8, destroy must be invoked from the
            // base widget
            $.Widget.prototype.destroy.call(this);
            // For UI 1.9, define _destroy instead and don't
            // worry about
            // calling the base widget
        },


        // Respond to any changes the user makes to the
        // option method
        _setOption: function (key, value) {
            switch (key) {
                case "showRefresh":
                    //this.options.someValue = doSomethingWith( value );
                    this.options.showRefresh = value;

                    break;
                case "color":
                        this.options.color = value;
                        this._triggerRenderWidget();
                    break;
                default:
                    //this.options[ key ] = value;
                    break;
            }

            // For UI 1.8, _setOption must be manually invoked
            // from the base widget
            $.Widget.prototype._setOption.apply(this, arguments);
            // For UI 1.9 the _super method can be used instead
            // this._super( "_setOption", key, value );
        }
    });

})(jQuery, window, document);