﻿/*global jQuery,window,document,Utils,scheduledTreatmentEvents,ClientContract,CalendarService,jAlert,jConfirm,SecurityService,templates,AdminService,Entities*/
(function ($, window, document, undefined) {
	/*
		var templates = {
			scheduledTmpl: $.templates('<div class="scheduledTreatment" dataid="{{attr:clientTreatmentID}}"><button class="Attended" type="button">Attended</button><button class="Missed" type="button">Missed</button><div class="clear"></div></div>'),
			pendingTmpl: $.templates('<div class="attendedTreatment"><input type="hidden" name="VenueBookingStatusID" value="{{attr:VenueBookingStatusID}}"/>' +
				'<div class="optional-fields">' +//left column start
				'<p><label>Name:</label><span class="field"><input type="text" name="Name" valtype="Required;" value="{{attr:Name}}"/></span></p>' +
				'<p><label>Email:</label><span class="field"><input type="text" name="Email" valtype="Required;" value="{{attr:Email}}"/></span></p>' +
				'<p><label>Phone:</label><span class="field"><input type="text" name="Phone" valtype="Required;" value="{{attr:PhoneNumber}}"/></span></p>' +
				'</div>' +//end left column
				'<p style="clear:both;"><label>Requirements:</label><br/><span class="field"><textarea type="text" class="longinput" style="width: 100%;" name="Requirements">{{>Requirements}}</textarea></span></p></div>'
				),
			missedTmpl: $.templates('<div class="missedTreatment"><input type="hidden" name="TreatmentStatusID" value="{{attr:TreatmentStatusID}}"/><p><label>Notes:</label><span class="field"><textarea type="text" class="longinput" name="Booking_Notes">{{>(Notes || "")}}</textarea></span></p></div>'),
			resetTmpl: $.templates('<div class="resetTreatment">This will clear all information attached to this treatment and return it to the available treatment pool. Are you sure you want to do this?</div>'),
			infoTmpl: $.templates('<div class="eventInfo">' +
				'<p><label>Full Name:</label><span class="field">{{attr:FirstName}} {{attr:LastName}}</span></p>' +
				'<p><label>Client Number:</label><span class="field">{{attr:ClientNumber}}</span></p>' +
				'<p><label>Phone Number:</label><span class="field">{{>Phone}}&nbsp;</span></p>' +
				'<p><label>Email:</label><span class="field">{{>Email}}&nbsp;</span></p>' +
				'<p><label>Treatment Type:</label><span class="field">{{>TreatmentName}}</span></p>' +
				'<p><label>Treatment Date and Time:</label><span class="field">{{>~formatDate(StartDate,"dd/mm/yyyy HH:MM")}} - {{>~formatDate(EndDate,"dd/mm/yyyy HH:MM")}}</span></p>' +
				'<p><label>Treatment Status:</label><span class="field {{attr:TreatmentStatus}}">{{attr:TreatmentStatus}}</span></p>' +
				'<p><label>No. of Remaining Treatments:</label><span class="field">{{attr:RemainingTreatments}}</span></p>' +
				'<p><label>No. of Attended Treatments:</label><span class="field">{{attr:AttendedTreatments}}</span></p>' +
				'<p><label>No. of Missed Treatments:</label><span class="field">{{attr:MissedTreatments}}</span></p>' +
				'</div>')
		};
	*/
	var methods = {
		pendingTreatment: function (event, callback) {

			var $scheduleDialog = $($("#pendingTmpl").tmpl(event)).dialog({
				title: event.title,
				modal: true,
				resizable: false,
				width: "500px",
				buttons: [
					{
						text: "Save",
						click: function () {
							scheduledTreatmentEvents.updateBooking($(this), event, false, callback);
						}
					},
					{
						text: "Cancel this Booking",
						click: function () {
							var $dialog = this;
							jConfirm("Are you sure you want to cancel this booking?", "Cancel Booking", function (_r) {
								//I will probably get penalized for this '_r' meaningless variable name that is so obvious what it does! so yeah BITE ME!
								if (_r) {
									scheduledTreatmentEvents.updateBooking($($dialog), event, true, callback);
								}
							});
						}
					}
				],
				open: function () {
					var $nameField = $(this).find("input[name=Name]");
					var $dialog = $(this);
					$nameField.autocomplete({
						source: function (request, response) {
							var requestProperties = function (result) {
								if (result.EntityList[0] === null) { result.EntityList = []; }
								response(result.EntityList);
							};

							//call the service method to return both matching erven and properties
							SecurityService.WCFSecurity.GetActiveOccupants(request.term, requestProperties, Utils.responseFail);
						}
						, minLength: 3
						, select: function (event, ui) {
							var occupantID = ui.item.value;

							$nameField.val(ui.item.label);

							var response = function (_result) {
								if (_result.EntityList[0] !== null) {
									var currentOccupant = _result.EntityList[0];

									$dialog.find("input[name=Email]").val(currentOccupant.Email);
									$dialog.find("input[name=PhoneNumber]").val(currentOccupant.Cellphone);
								}

								$dialog.unblock();
							};

							$dialog.block();
							SecurityService.WCFSecurity.GetOccupant(occupantID, response, Utils.responseFail);

							return false;
						}
					});
				}
			});
			if ($('#ResetAccess').length > 0) {
				$scheduleDialog.append('<button class="Reschedule" type="button">Reset</button>');
			}

			$scheduleDialog.data('scheduleTreatmentEvent', event);

			var tomorrow = new Date();
			tomorrow.setDate(tomorrow.getDate() + 1);
			tomorrow.setHours(0);
			tomorrow.setMinutes(0);
			tomorrow.setSeconds(0);

			//hide attended and missed buttons on future dated events
			//if (event._start > tomorrow || event.TreatmentStatusID == Utils.TreatmentStatusEnum.ScheduledConflict) {
			//$scheduleDialog.find(".Attended , .Missed").css("display", "none");
			//}

			$scheduleDialog.on('click.scheduleTreatment', 'button.Attended', scheduledTreatmentEvents.attendedClick);
			$scheduleDialog.on('click.scheduleTreatment', 'button.Missed', scheduledTreatmentEvents.missedClick);
			$scheduleDialog.on('click.scheduleTreatment', 'button.Reschedule', scheduledTreatmentEvents.rescheduleClick);
		},
		addBooking: function (date, calendarSelector/*,[callback]*/) {
			var callbackoptions = null;


			var $addBookingDialog = $($("#addBookingTmpl").tmpl({ Name: "Booking Name..." })).dialog({
				title: "New Booking",
				modal: true,
				resizable: false,
				width: 500,
				buttons: {
					Save: function () {
						//WCF Update objects
						if (Utils.validation.validateForm(".addBooking", "There are missing values, please check the form.", false, true)) {
							scheduledTreatmentEvents.addNewBooking($(this), calendarSelector);
						}
					}
				},
				open: function () {
					var thiz = this;
					$(this).find("select[name=TimeStart]").change(function (e) {
						var date = new Date($(this).find(":selected").attr("data-date"));
						scheduledTreatmentEvents.collectEndTimes($(thiz), date);

					});
					//collect start times
					var $dialog = $(this);
					var $nameField = $(this).find("input[name=Name]");
					$.when(scheduledTreatmentEvents.collectStartTimes($(this), date)).then(function () {
						$nameField.autocomplete({
							source: function (request, response) {
								var requestProperties = function (result) {
									if (result.EntityList[0] === null) { result.EntityList = []; }
									response(result.EntityList);
								};

								//call the service method to return both matching erven and properties
								SecurityService.WCFSecurity.GetActiveOccupants(request.term, requestProperties, Utils.responseFail);
							}
							, minLength: 3
							, select: function (event, ui) {
								var occupantID = ui.item.value;

								$nameField.val(ui.item.label);

								var response = function (_result) {
									if (_result.EntityList[0] !== null) {
										var currentOccupant = _result.EntityList[0];

										$dialog.find("input[name=Email]").val(currentOccupant.Email);
										$dialog.find("input[name=PhoneNumber]").val(currentOccupant.Cellphone);
										$dialog.find("input[name=OccupantID]").val(currentOccupant.ID);
									}

									$dialog.unblock();
								};

								$dialog.block();
								SecurityService.WCFSecurity.GetOccupant(occupantID, response, Utils.responseFail);

								return false;
							}
						});
					});


				},
				beforeClose: function () {
					//clean up crew
					$(this).find("input").each(function (i, o) {
						$(this).val("");
					});

					$(this).find("textarea").each(function (i, o) {
						$(this).val("");
					});
				},
				close: function () {
					$(this).find(".addBooking").html("");
					$(this).dialog("destroy");
				}
			});
		},
		missedTreatment: function (event) {
			var $missedDialog = $(templates.missedTmpl.render(event)).dialog({
				title: event.title,
				modal: true,
				resizable: false,
				buttons: {
					Save: function () {
						//WCF Update objects
						var $this = $(this);

						var treatmentStatusID = parseInt($this.find("input[name=TreatmentStatusID]").val(), 10),
							className = Utils.getEnumKey(Utils.TreatmentStatusEnum, event.TreatmentStatusID),
							notes = $this.find("textarea[name=Booking_Notes]").val();

						var options = {
							context: $this,
							callback: function (success, passedevent) {
								if (success) {
									//on success update event on calender

									event.TreatmentStatusID = passedevent.TreatmentStatusID;
									event.className = Utils.getEnumKey(Utils.TreatmentStatusEnum, passedevent.TreatmentStatusID);
									event.Notes = passedevent.Notes;

									if (parseInt(event.TreatmentStatusID, 10) === Utils.TreatmentStatusEnum.Available) {
										//	//event has been set as available remove from calendar
										$.erosCalendar('removeEvent', event.ID);
									} else {
										//render event on calendar with new information
										$.erosCalendar('renderEvent', event, success);
										//	$(selector).fullCalendar('renderEvent', event, success);

									}
									$(this).dialog("close");
								}
							}
						},
							customClientTreatment = {
								ID: event.ID,
								start: event.start,
								end: event.end,
								allDay: false,
								TreatmentStatusID: event.TreatmentStatusID
								, Notes: notes || null
							};


						if (parseInt(treatmentStatusID, 10) === Utils.TreatmentStatusEnum.Cancelled && notes.length > 0) {
							$.erosCalendar('updateBooking', customClientTreatment, options);
						} else if (Utils.validation.validateForm(this, "Invalid or missing information supplied.", false, false)) {
							//Make WCF call to update event information
							customClientTreatment.TreatmentStatusID = Utils.TreatmentStatusEnum.Missed;

							if ($.erosCalendar) {
								$.erosCalendar('updateBooking', customClientTreatment, options);
							} else {
								ClientContract.saveTreatment('UpdateTreatment', customClientTreatment, options);
							}
						} else {
							$.jGrowl("Please review the details below");
						}
					},
					Close: function () {
						$(this).dialog("close");
					}
				},
				beforeClose: function () {
					$(this).find("input").each(function (i, o) {
						$(this).val("");
					});

					$(this).find("textarea").each(function (i, o) {
						$(this).val("");
					});
				}
			});
			if ($('#ResetAccess').length > 0) {
				var buts = $missedDialog.dialog("option", "buttons");
				buts["Reset"] = function () {
					$(this).dialog("close");
					$.calendarDialogues('rescheduleTreatment', event);
				};
				$missedDialog.dialog("option", "buttons", buts);
			}
		},
		rescheduleTreatment: function (event) {
			var $resetDialog = $(templates.resetTmpl.render(event)).dialog({
				title: event.title,
				modal: true,
				resizable: false,
				buttons: {
					Yes: function () {
						//WCF Update objects
						var $this = $(this);

						var options = {
							context: $this,
							callback: function (success, passedevent) {
								if (success) {
									//on success update event on calender
									$.erosCalendar('removeEvent', event.ID);
									$(this).dialog("close");
								}
							}
						},
						customClientTreatment = {
							ID: event.ID,
							start: null,
							end: null,
							allDay: false,
							TreatmentStatusID: Utils.TreatmentStatusEnum.Available
						};


						if (Utils.validation.validateForm(this, "Invalid or missing information supplied.", false, false)) {
							//Make WCF call to update event information
							if ($.erosCalendar) {
								$.erosCalendar('updateBooking', customClientTreatment, options);
							} else {
								//no calendar object so call the method ourselves so that the functionality can still work, please forgive me for this.
								var response = function (result) {

									//close the dialog
									$resetDialog.dialog("close");

									if (result.EntityList[0].success === true) {
										jAlert("Treatment succesfully reset", "Treatment saved");
										ClientContract.events.treatmentTab_Click(null);
									} else {
										jAlert(result.EntityList[0].message, "Treatment saved");

									}
								};

								CalendarService.WCFCalendar.UpdateBooking(customClientTreatment, response, Utils.responseFail);
							}
						} else {
							$.jGrowl("Please review the details below");
						}
					},
					No: function () {
						$(this).dialog("close");
					}
				},
				beforeClose: function () {
					$(this).find("input").each(function (i, o) {
						$(this).val("");
					});

					$(this).find("textarea").each(function (i, o) {
						$(this).val("");
					});
				}
			});
		},
		getEventInfo: function (event) {
			var $infoDialog = $(templates.infoTmpl.render(event)).dialog({
				title: "Treatment Information",
				width: 500,
				modal: true,
				resizable: false,
				buttons: {
					Close: function () {
						$(this).dialog("close");
					}
				}
			});
		},
		getLastClientTreatment_DFD: function (ClientID, CalendarEvent, OriginalEvent) {
			var dfd = $.Deferred();

			var response = function (_result) {
				//resolve now so that the dialog will close.
				dfd.resolve();

				//now trigger opening of the attended dialog with the event we have retrieved
				$.calendarDialogues('attendedTreatment', OriginalEvent, { result: _result });
			};

			CalendarService.WCFCalendar.GetLastClientBooking(ClientID, CalendarEvent, response, Utils.responseFail);

			return dfd.promise();
		},

	};


	//#region scheduledTreatmentEvents
	var scheduledTreatmentEvents = {
		collectStartTimes: function ($dialog, date) {
			var $dfd = new $.Deferred(),
				$container = $dialog.find(".addBooking");

			$container.block();
			var success = function ($result) {
				$container.find("select[name=TimeStart]").html($("#timeStartTmpl").tmpl($result.EntityList));
				$dfd.resolve();
				$.uniform.update();

				$container.unblock();
				//set the value to the current date time value
				$container.find("select[name=TimeStart]").val(date.getHours() + ":" + date.getMinutes());
				$.uniform.update();
				$container.find("select[name=TimeStart]").change();
			};

			var error = function ($err) {
				$dfd.reject();
			};

			AdminService.WCFAdmin.GetAvailableBookingStartTimes($("#VenueID").val(), date, success, error);

			return $dfd.promise();
		},
		collectEndTimes: function ($dialog, date) {
			var $dfd = new $.Deferred(),
				$container = $dialog.find(".addBooking");

			$container.block();
			var success = function ($result) {
				$container.find("select[name=TimeEnd]").html($("#timeEndTmpl").tmpl($result.EntityList));
				$dfd.resolve();
				$.uniform.update();

				$container.unblock();
				//set the value to the current date time value
				$container.find("select[name=TimeEnd]").val(date.getHours() + ":" + date.getMinutes());
				$.uniform.update();
			};

			var error = function ($err) {
				$dfd.reject();
			};

			AdminService.WCFAdmin.GetAvailableBookingEndTimes($("#VenueID").val(), date, success, error);

			return $dfd.promise();
		},
		updateBooking: function ($dialog, event, cancelBooking, callback) {
			var Booking = new Entities.VenueBooking();
			Booking = Utils.populateEntityFromForm($dialog, Booking);
			Booking.StartDateTime = event.start;
			Booking.EndDateTime = event.end;
			Booking.InsertedOn = new Date($dialog.find("input[name=InsertedOn]").val());
			Booking.VenueID = parseInt($("#VenueID").val(), 10);
			Booking.VenueBookingID = event.ID;
			Booking.VenueBookingStatusID = event.VenueBookingStatusID;
			Booking.TotalBookingCost = event.TotalBookingCost;
			Booking.InsertedByID = event.InsertedByID;
			Booking.InsertedOn = event.InsertedOn;
			Booking.CancelledByID = 0;
			if (Booking.OccupantID === null || Booking.OccupantID == "null") {
				Booking.OccupantID = 0;
			}

			if (Booking.Requirements === null) {
				Booking.Requirements = "";
			}

			var success = function (result) {
				if (result.EntityList[0].ID > 0) {
					$.jGrowl("Booking successfully updated.");
					//run callback if passed
					if (typeof (callback) == "function") {
						//update the event object in case this information changed
						event.title = Booking.Name;
						event.Email = Booking.Email;
						event.Phone = Booking.PhoneNumber;
						event.Requirements = Booking.Requirements;

						callback(event, cancelBooking);
					}
				} else {
					$.jGrowl("Unable to save this booking, please try again.");
				}

				$($dialog).dialog("close");
			};

			var error = function () {

			};

			AdminService.WCFAdmin.UpdateBooking(Booking, cancelBooking, success, error);
		},
		addNewBooking: function ($dialog, calendarSelector) {
			var Booking = new Entities.VenueBooking();
			Booking = Utils.populateEntityFromForm($dialog, Booking);
			Booking.VenueID = $("#VenueID").val();
			Booking.StartDateTime = new Date($dialog.find("select[name=TimeStart]").find(":selected").attr("data-date"));
			Booking.EndDateTime = new Date($dialog.find("select[name=TimeEnd]").find(":selected").attr("data-date"));
			Booking.InsertedOn = new Date();
			Booking.VenueBookingID = -1;
			var success = function (result) {
				if (result.EntityList[0].ID > 0) {
					$.jGrowl("Booking successfully saved.");
					//load the event into the calendar
					//$(calendarSelector).fullCalendar('renderEvent', result.EntityList[0]);
					$(calendarSelector).data("calendarevents").refresh();
					//calendarEvents.refresh();

				} else {
					$.jGrowl("Unable to save this booking, please try again.");
				}
				$dialog.dialog("close");

			};

			var error = function () {

			};

			AdminService.WCFAdmin.UpdateBooking(Booking, false, success, error);
		},
		attendedClick: function (e) {
			var $dialog = $(e.currentTarget).parents('.scheduledTreatment');
			var event = $dialog.data('scheduleTreatmentEvent');
			var customClientTreatment = {
				ID: event.ID,
				start: event.start,
				end: event.end,
				allDay: false,
				TreatmentStatusID: event.TreatmentStatusID
				, Weight: event.Weight || null
				, Height: event.Height || null
				, Arms: event.Arms || null
				, UpperThigh: event.UpperThigh || null
				, Booty: event.Booty || null
				, Waist: event.Waist || null
				, Chest: event.cChest || null
				, Notes: event.Notes || null
				, RibCage: event.RibCage || null
				, StomachHip: event.StomachHip || null
				, Knee: event.Knee || null
			};
			//var options = {
			//	callback: function (callbackoptions) {
			//		$dialog.dialog('close');
			//		$.calendarDialogues('attendedTreatment', event, callbackoptions);
			//	}
			//};

			//$.erosCalendar('getLastClientBooking', event.ClientID, customClientTreatment, options);

			//call the dialog method which retrieves the last booking to aquire the previous measurments
			$.when($.calendarDialogues('getLastClientTreatment_DFD', event.ClientID, customClientTreatment, event)).then(function () {
				//close the current dialog
				$dialog.dialog('close');
			});

		},
		missedClick: function (e) {
			var $dialog = $(e.currentTarget).parents('.scheduledTreatment');
			var event = $dialog.data('scheduleTreatmentEvent');
			$dialog.dialog('close');
			$.calendarDialogues('missedTreatment', event);
		},
		rescheduleClick: function (e) {
			var $dialog = $(e.currentTarget).parents('.scheduledTreatment');
			var event = $dialog.data('scheduleTreatmentEvent');
			$dialog.dialog('close');
			$.calendarDialogues('rescheduleTreatment', event);
		}
	};
	//#endregion

	$.calendarDialogues = function (method) {
		// Method calling logic
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if (typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist on jQuery.calendarDialogues');
		}
	};


})(jQuery, window, document);