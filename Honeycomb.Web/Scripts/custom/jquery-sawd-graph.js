﻿/*global jQuery,window, document*/

; (function ($, window, document, undefined) {//ignore jslint

	// define your widget under a namespace of your choice
	//  with additional parameters e.g.
	// $.widget( "namespace.widgetname", (optional) - an
	// existing widget prototype to inherit from, an object
	// literal to become the widget's prototype );

	$.widget("honeycomb.graph", {

		//Options to be used as defaults
		options: {
			showButtons: true,
			showRefresh: true,
			currentType: "week"
		},

		_handlers: {
			graphButtonClick: function (event) {

				console.log("honeycomb.graph graphButtonClick called");

				var currentButton = $(event.currentTarget);
				var currentType = "";
				if (event.type.toLowerCase() === 'click') {
					currentType = currentButton.text().toLowerCase();
				} else if (event.type.toLowerCase() === 'change') {
					currentType = $(event.currentTarget).val();
					this.options.currentType = currentType;
				}


				//if there is a type and we want to replace it with the new one
				if (currentType === "week" || currentType === "month" || currentType === "year") {
					currentButton.siblings().removeClass('btn_orange');
					currentButton.addClass('btn_orange');

					this.options.currentType = currentType;
				}

				//trigger the renderGraph callback and pass through the type and the element in the html which will contain the graph
				this._triggerRenderGraph(event);
			}
		},
		//Setup widget (eg. element creation, apply theming
		// , bind events etc.)
		_create: function () {

			// _create will automatically run the first time
			// this widget is called. Put the initial widget
			// setup code here, then you can access the element
			// on which the widget was called via this.element.
			// The options defined above can be accessed
			// via this.options this.element.addStuff();
			if (!this.options.showButtons) {
				this.element.find(".graphButtons .stdbtn").not(".refresh").hide();
			}

			if (!this.options.showRefresh) {
				this.element.find(".graphButtons .refresh").hide();
			}

			//set the correct buttons to orange
			if (this.options.showButtons && this.element.find(".graphButtons button").hasClass(this.options.currentType)) {
				this.element.find(".graphButtons button." + this.options.currentType).addClass('btn_orange').siblings().removeClass('btn_orange');
			}

			this.element.on("click.graphButtons", ".graphButtons button", $.proxy(this._handlers.graphButtonClick, this));
			this.element.on("change.graphButtons", ".graphButtons select", $.proxy(this._handlers.graphButtonClick, this));
			//now that we have registered the handlers we can trigger the render callback
			this._triggerRenderGraph(null);
		},

		_triggerRenderGraph: function (event) {

			//trigger the renderGraph callback and pass through the type and the element in the html which will contain the graph
			this._trigger("renderGraph", event, {
				type: this.options.currentType,
				container: this.element.find('.dashboardGraph')
			});
		},

		refresh: function () {
			this._triggerRenderGraph(null);
		},
		// Destroy an instantiated plugin and clean up
		// modifications the widget has made to the DOM
		destroy: function () {

			// this.element.removeStuff();
			// For UI 1.8, destroy must be invoked from the
			// base widget
			$.Widget.prototype.destroy.call(this);
			// For UI 1.9, define _destroy instead and don't
			// worry about
			// calling the base widget
		},


		// Respond to any changes the user makes to the
		// option method
		_setOption: function (key, value) {
			switch (key) {
				case "showButtons":
					//this.options.someValue = doSomethingWith( value );
					this.options.showButtons = value;

					if (!this.options.showButtons) {
						this.element.find(".graphButtons .stdbtn").not(".refresh").hide();
					} else {
						this.element.find(".graphButtons .stdbtn").not(".refresh").show();
					}

					break;
				case "showRefresh":
					//this.options.someValue = doSomethingWith( value );
					this.options.showRefresh = value;

					if (!this.options.showRefresh) {
						this.element.find(".graphButtons .refresh").hide();
					} else {
						this.element.find(".graphButtons .refresh").show();
					}

					break;
				case "currentType":
					if (value === "week" || value === "month" || value === "year") {
						this.options.currentType = value;
						this._triggerRenderGraph(null);
					} else {
						console.log("The value '" + value + "', provided for 'currentType' is not allowed. Supported types are 'week','month' and 'year'. ");
					}
					break;
				default:
					//this.options[ key ] = value;
					break;
			}

			// For UI 1.8, _setOption must be manually invoked
			// from the base widget
			$.Widget.prototype._setOption.apply(this, arguments);
			// For UI 1.9 the _super method can be used instead
			// this._super( "_setOption", key, value );
		}
	});

})(jQuery, window, document);