﻿

//#region Plugins
/// <reference path="../plugins/jquery-1.8.2.js" />
/// <reference path="../plugins/jquery-1.8.2.intellisense.js" />
/// <reference path="../plugins/jquery-ui-1.8.16.custom.min.js" />
/// <reference path="../plugins/jquery-sawd-graph.js" />
/// <reference path="../plugins/jquery.bxSlider.min.js" />
/// <reference path="../plugins/jquery.slimscroll.js" />
/// <reference path="../plugins/jquery.tagsinput.min.js" />
/// <reference path="../plugins/jquery.validate.min.js" />
/// <reference path="../plugins/MicrosoftAjax.js" />
/// <reference path="../plugins/jquery.alerts.js" />
/// <reference path="../plugins/jquery.jgrowl.js" />
/// <reference path="../plugins/jquery.tmpl.js" />
/// <reference path="../plugins/jquery.autogrow-textarea.js" />
/// <reference path="../plugins/jquery.blockUI.js" />
//#endregion

//#region Custom
/// <reference path="general.js" />
/// <reference path="tables.js" />
/// <reference path="../generated/EntitiesIntellisense.js
//#endregion

//#region Service references
/// <reference path="/Services/WCFSecurity.svc"/>
/// <reference path="/Services/WCFProperty.svc"/>
/// <reference path="/Services/WCFSMS.svc"/>
/// <reference path="/Services/WCFContractor.svc"/>
/// <reference path="/Services/WCFMaintenance.svc"/> 
//#endregion

