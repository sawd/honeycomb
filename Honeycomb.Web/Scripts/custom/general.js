/*global SecurityService,Sys,window,jQuery*/

//#region Globals
var Globals = function () {
	var globalObj = {
		cookieOptions: {
			expires: 60,
			path: '/'
		},
		graphColors: function () {
		    return ['#3366cc', '#ff9900', '#109618', '#0099c6', '#5c3292', '#dc3912', '#999999', '#1a1a1a', '#990099'];
		},
		paginationContainer: function (orderBy, pageSize, callBack) {
			/// <summary>
			/// An object that initialises various pagination required variables.
			/// </summary>
			/// <param name="orderBy" type="String">Optional: The field name used to order the returns set of data related to this object(defaults to '')</param>
			/// <param name="pageSize" type="int">Optional: The number of items per page to be returned. (defaults to Global.PageSize)</param>
			/// <param name="totalCount" type="int">Optional: The total number of items in the pagination (defaults to -1)</param>
			/// <param name="currentPage" type="int">Optional: The current page of pagination. (defaults to -0)</param>
			/// <returns type="String" />
			this.TotalCount = -1;
			this.CurrentPage = 0;
			this.OrderBy = orderBy || '';
			this.PageSize = pageSize || 10;
			this.PaginationID = undefined;
			this.TableID = undefined;
			this.Callback = callBack;

		},
		defaults: function () {
			$.datepicker.setDefaults({ "dateFormat": "dd/mm/yy" });
			$.blockUI.defaults.message = null;
			$.views.helpers({

			    formatDate: function (val, format) {
			        if (val !== null) {
			            return dateFormat(val, format);//ignore jslint
			        } else {
			            return "";
			        }
			    },
			    getTreatmentStatus: function (statusID) {
			        return Utils.getEnumDescription(Utils.TreatmentStatusEnum, statusID);
			    },

			    multiply: function (parameter1, parameter2) {
			        return isNaN(parameter1) || isNaN(parameter2) ? 0 : parameter1 * parameter2;
			    }
			});
		}
	};
	globalObj.paginationContainer.prototype.reset = function (orderBy) {
		this.TotalCount = -1;
		this.CurrentPage = 0;
		this.OrderBy = orderBy || this.OrderBy;
	};
	globalObj.paginationContainer.prototype.bindPagination = function (options) {
		var container = this;
		if (container.PaginationID && container.TableID) {
			options = $.extend({
				items_per_page: container.PageSize,
				current_page: container.CurrentPage,
				callback: container.paginationCallback,
				context: container
			}, options);
			$(container.PaginationID).pagination(container.TotalCount, options);
			container.bindTableOrdering(container);
		}
		else {
			alert("Pagination Error! No Table or Pagination Location");
		}
	};
	globalObj.paginationContainer.prototype.paginationCallback = function (page_index, jq) {
		var container = this.context;
		if (page_index > 0) {
			container.CurrentPage = page_index;
			container.Callback();
		} else if (container.CurrentPage > 0 && page_index === 0) {
			container.CurrentPage = 0;
			container.Callback();
		}
		return false;
	};
	globalObj.paginationContainer.prototype.bindTableOrdering = function (container) {
		if (container.isOrderBound !== true) {
			$(container.TableID + " th[sortColumn]").each(function () {
				var $header = $(this),
                    sortColumn = $header.attr('sortColumn');
				if (sortColumn === container.OrderBy) {
					$header.removeClass("sorting sorting_desc").addClass("sorting_asc");
				} else if (sortColumn + ' desc' === container.OrderBy) {
					$header.removeClass("sorting sorting_asc").addClass("sorting_desc");
				} else {
					$header.addClass("sorting");
				}
				$header.unbind('click.ordering');
				$header.bind('click.ordering', function () {
					$(container.TableID + " th[sortColumn]").not(this).addClass("sorting").removeClass("sorting_asc sorting_desc");
					var column = $(this);
					if (column.hasClass("sorting") || column.hasClass("sorting_desc")) {
						column.removeClass("sorting sorting_desc").addClass("sorting_asc");
						container.OrderBy = column.attr('sortColumn');
						container.Callback();
					} else if (column.hasClass("sorting_asc")) {
						column.removeClass("sorting sorting_asc").addClass("sorting_desc");
						container.OrderBy = column.attr('sortColumn') + ' desc';
						container.Callback();
					}

				});
				container.isOrderBound = true;
			});

		}
	};

	return globalObj;
}();

//#endregion

//#region Utils
var Utils = {
	masterLoadEvent: function () {
		//#region Load Defaults 
		Globals.defaults();
		//#endregion

		//#region SHOW/HIDE USERDATA WHEN USERINFO IS CLICKED
		$('.userinfo').click(function () {
			if (!$(this).hasClass('active')) {
				$('.userinfodrop').show();
				$(this).addClass('active');
			} else {
				$('.userinfodrop').hide();
				$(this).removeClass('active');
			}
			//remove notification box if visible
			$('.notification').removeClass('active');
			$('.noticontent').remove();

			return false;
		});
		//#endregion

		//#region SHOW/HIDE NOTIFICATION 
		$('.notification a').click(function () {
			var t = $(this);
			var url = t.attr('href');
			if (!$('.noticontent').is(':visible')) {
				$.post(url, function (data) {
					t.parent().append('<div class="noticontent">' + data + '</div>');
				});
				//this will hide user info drop down when visible
				$('.userinfo').removeClass('active');
				$('.userinfodrop').hide();
			} else {
				t.parent().removeClass('active');
				$('.noticontent').hide();
			}
			return false;
		});
		//#endregion

		//#region SHOW/HIDE BOTH NOTIFICATION & USERINFO WHEN CLICKED OUTSIDE OF THIS formindex 
		$(document).click(function (event) {
			var ud = $('.userinfodrop');
			var nb = $('.noticontent');

			//hide user drop menu when clicked outside of this formindex
			if (!$(event.target).is('.userinfodrop')
                && !$(event.target).is('.userdata')
                && ud.is(':visible')) {
				ud.hide();
				$('.userinfo').removeClass('active');
			}

			//hide notification box when clicked outside of this formindex
			if (!$(event.target).is('.noticontent') && nb.is(':visible')) {
				nb.remove();
				$('.notification').removeClass('active');
			}
		});
		//#endregion

		//#region NOTIFICATION CONTENT 
		$('.notitab a').live('click', function () {
			var id = $(this).attr('href');
			$('.notitab li').removeClass('current'); //reset current 
			$(this).parent().addClass('current');
			if (id == '#messages') {
				$('#activities').hide();
			}
			else {
				$('#messages').hide();
			}

			$(id).show();
			return false;
		});
		//#endregion

		//#region SHOW/HIDE VERTICAL SUB MENU	
		$('.vernav > ul li a, .vernav2 > ul li a').each(function (index, Element) {

			$(Element).click(function (e) {
				var url = $(this).attr('href');
				if (url.indexOf("#") > -1) {
					if ($(url).length > 0) {
						if ($(url).is(':visible')) {
							if (!$(this).parents('div').hasClass('menucoll') &&
                               !$(this).parents('div').hasClass('menucoll2')) {
								$(url).slideUp();
							}
						} else {
							$('.vernav ul ul, .vernav2 ul ul').each(function () {
								$(this).slideUp();
							});
							if (!$(this).parents('div').hasClass('menucoll') &&
                               !$(this).parents('div').hasClass('menucoll2')) {
								$(url).slideDown();
							}
						}
						return false;
					}

				}
			});
		});
		//#endregion

		//#region SHOW/HIDE SUB MENU WHEN MENU COLLAPSED 
		$('.menucoll > ul > li, .menucoll2 > ul > li').live('mouseenter mouseleave', function (e) {
			if (e.type == 'mouseenter') {
				$(this).addClass('hover');
				$(this).find('ul').show();
			} else {
				$(this).removeClass('hover').find('ul').hide();
			}
		});
		//#endregion

		//#region HORIZONTAL NAVIGATION (AJAX/INLINE DATA)	
		$('.hornav a').click(function () {

			if (!$(this).parent().hasClass('current')) {
				//this is only applicable when window size below 450px
				if ($(this).parents('.more').length === 0) {
					$('.hornav li.more ul').hide();
				}

				//remove current menu
				$('.hornav li').each(function () {
					$(this).removeClass('current');
				});

				$(this).parent().addClass('current');	// set as current menu

				var url = $(this).attr('href');
				if ($(url).length > 0) {
					$('.contentwrapper .subcontent').hide();
					$(url).show();
				} else {
					$.post(url, function (data) {
						$('#contentwrapper').html(data);
						$('.stdtable input:checkbox').uniform();	//restyling checkbox
					});
				}

				$($(this).attr('href')).trigger("Honeycomb.tabLoad");
			}
			return false;
		});
		//#endregion

		//#region COLLAPSED/EXPAND LEFT MENU 
		$('.togglemenu').click(function () {
			if (!$(this).hasClass('togglemenu_collapsed')) {

				//if($('.iconmenu').hasClass('vernav')) {
				if ($('.vernav').length > 0) {
					if ($('.vernav').hasClass('iconmenu')) {
						$('body').addClass('withmenucoll');
						$('.iconmenu').addClass('menucoll');
					} else {
						$('body').addClass('withmenucoll');
						$('.vernav').addClass('menucoll').find('ul').hide();
					}
				} else if ($('.vernav2').length > 0) {
					//} else {
					$('body').addClass('withmenucoll2');
					$('.iconmenu').addClass('menucoll2');
				}

				$(this).addClass('togglemenu_collapsed');

				$('.iconmenu > ul > li > a').each(function () {
					var label = $(this).text();
					$('<li><span>' + label + '</span></li>')
                        .insertBefore($(this).parent().find('ul li:first-child'));
				});
			} else {

				//if($('.iconmenu').hasClass('vernav')) {
				if ($('.vernav').length > 0) {
					if ($('.vernav').hasClass('iconmenu')) {
						$('body').removeClass('withmenucoll');
						$('.iconmenu').removeClass('menucoll');
					} else {
						$('body').removeClass('withmenucoll');
						$('.vernav').removeClass('menucoll').find('ul').show();
					}
				} else if ($('.vernav2').length > 0) {
					//} else {
					$('body').removeClass('withmenucoll2');
					$('.iconmenu').removeClass('menucoll2');
				}
				$(this).removeClass('togglemenu_collapsed');

				$('.iconmenu ul ul li:first-child').remove();
			}
		});
		//#endregion

		//#region RESPONSIVE 
		if ($(document).width() < 640) {
			$('.togglemenu').addClass('togglemenu_collapsed');
			if ($('.vernav').length > 0) {

				$('.iconmenu').addClass('menucoll');
				$('body').addClass('withmenucoll');
				$('.centercontent').css({ marginLeft: '56px' });
				if ($('.iconmenu').length === 0) {
					$('.togglemenu').removeClass('togglemenu_collapsed');
				} else {
					$('.iconmenu > ul > li > a').each(function () {
						var label = $(this).text();
						$('<li><span>' + label + '</span></li>')
                            .insertBefore($(this).parent().find('ul li:first-child'));
					});
				}

			} else {

				$('.iconmenu').addClass('menucoll2');
				$('body').addClass('withmenucoll2');
				$('.centercontent').css({ marginLeft: '36px' });

				$('.iconmenu > ul > li > a').each(function () {
					var label = $(this).text();
					$('<li><span>' + label + '</span></li>')
                        .insertBefore($(this).parent().find('ul li:first-child'));
				});
			}
		}


		$('.searchicon').live('click', function () {
			$('.searchinner').show();
		});

		$('.searchcancel').live('click', function () {
			$('.searchinner').hide();
		});
		//#endregion

		//#region On resize window 
		$(window).resize(function () {
			if ($(window).width() > 640) {
				$('.centercontent').removeAttr('style');
			}
		});
		//#endregion

		//#region Change theme 
		$('.changetheme a').click(function () {
			var c = $(this).attr('class');
			if ($('#addonstyle').length === 0) {
				if (c != 'default') {
					$('head').append('<link id="addonstyle" rel="stylesheet" href="/Theme/css/style.' + c + '.css" type="text/css" />');
					$.cookie("addonstyle", c, Globals.cookieOptions);
				}
			} else {
				if (c != 'default') {
					$('#addonstyle').attr('href', '/Theme/css/style.' + c + '.css');
					$.cookie("addonstyle", c, Globals.cookieOptions);
				} else {
					$('#addonstyle').remove();
					$.removeCookie('addonstyle', Globals.cookieOptions);
				}
			}
		});
		//#endregion

		//#region Load addon style when it's already set 
		if ($.cookie('addonstyle')) {
			var c = $.cookie('addonstyle');
			if (c !== '') {
				$('head').append('<link id="addonstyle" rel="stylesheet" href="/Theme/css/style.' + c + '.css" type="text/css" />');
				$.cookie("addonstyle", c, Globals.cookieOptions);
			}
		}
		//#endregion

		//#region Custom Executions
		//Keep Session alive
		setInterval(Utils.keepAlive, 30000);

		//Select Vertical Nav
		if (document.location.toString().toUpperCase().indexOf("PUBLIC") === -1) {
			var verticalMenu = String.format(".vernav2 a[href='{0}']", document.location.toString().replace("http://" + document.domain, ""));
			var verticalItem = $(verticalMenu);
			if (verticalItem.length > 0) {
				$(".vernav2 a.current").removeClass("current");
				verticalItem.parent().parent().show();
				verticalItem.addClass("current");
				$(".pagetitle").html(verticalItem.attr("pagetitle"));
				$(".pagedesc").html(verticalItem.attr("pagedesc"));
				//Select horizontal menu
				$("#" + verticalItem.attr("horizontalmenu")).parent().addClass("current");
			}
			else {
				var horizontalmenu = $(".headermenu li.current a");

				$(".pagetitle").html(horizontalmenu.attr("pagetitle"));
				$(".pagedesc").html(horizontalmenu.attr("pagedesc"));
			}
		}

		//#endregion

	},
	
	//#region Custom Enums
	BookingStatusEnum: {
		Pending: 10000,
		Cancelled: 10001,
		Attended: 10002
	},
	getEnumKey: function (enums, value) {
		for (var prop in enums) {
			if (enums.hasOwnProperty(prop)) {
				if (enums[prop] === value) {
					return prop;
				}
			}
		}
	},
	//#endregion

	//#region Tab Function
	tabs: {
		//#region addTabToPage
		addTabToPage: function (tabTitle, contentID, removeable, changeToTab) {
			/// <summary>Adds a preexisting content area to the hornav tab list</summary>
			/// <param name="tabTitle" type="String">The tab title. Can contain HTML</param>
			/// <param name="contentID" type="String">The # prefixed ID of the content you wish to link the tab to.</param>
			/// <param name="removeable" type="Bool">Whether or not a remove tab link should be generated.</param>
			/// <param name="changeToTab" type="Bool">Whether or not to make the added tab current on completion </param>
			if ($('.hornav').length > 0) {
				var existingTab = $('.hornav a[href="#' + contentID + '"]');
				if (existingTab.length > 0) {
					Utils.tabs.selectTab(existingTab.parent());
					return false;
				}
				var newTab = $('<li></li>');
				var newLink = $('<a href="#' + contentID + '">' + tabTitle + '</a>');
				newLink.appendTo(newTab);
				if (removeable) {
					var deleteLink = $('<img src="/Theme/images/icons/cross.png" class="tabIcon right" />').click(function (e) {
						e.preventDefault();
						var $tab = $(this).parent().parent();
						if ($tab.hasClass('current')) {
							Utils.tabs.removeTab($(this).parent().attr('href'));
						}
					});
					deleteLink.appendTo(newLink);
				}
				$('#' + contentID).addClass('subcontent');
				//$('#' + contentID).hide();
				newLink.click(function () {

					//this is only applicable when window size below 450px
					if (jQuery(this).parents('.more').length === 0) {
						jQuery('.hornav li.more ul').hide();
					}

					//remove current menu
					jQuery('.hornav li').each(function () {
						jQuery(this).removeClass('current');
					});

					jQuery(this).parent().addClass('current');	// set as current menu

					var url = jQuery(this).attr('href');
					if (jQuery(url).length > 0) {
						jQuery('.contentwrapper .subcontent').hide();
						jQuery(url).show();
					} else {
						jQuery.post(url, function (data) {
							jQuery('#contentwrapper').html(data);
							jQuery('.stdtable input:checkbox').uniform();	//restyling checkbox
						});
					}
					return false;
				});
				newTab.appendTo('.hornav');
				if (changeToTab) {
					newLink.click();
				}
			} else {
				alert("No Tab Navigation found. Unable to add new tab");
			}


		},
		//#endregion
		//#region removeTab
		removeTab: function (contentID, callBack) {
			/// <summary>Removes a tab and its associated content</summary>
			/// <param name="contentID" type="String">The # prefixed ID of the content you wish to link the tab to.</param>
			/// <param name="callBack" type="Function">A function to execute after tab removal.</param>

			var $tab = Utils.tabs.getTab(contentID);
			var anchor = $tab.find("a");
			$(anchor.attr('href')).remove();
			if ($tab.hasClass('current')) {
				$tab.prev().find('a').click();
			}
			$tab.remove();
			if (callBack) {
				callBack(this);
			}
		},
		//#endregion
		//#region selectTab
		selectTab: function (contentID, callBack) {
			var $tab = Utils.tabs.getTab(contentID);
			$tab.find('a').click();
			if (callBack) {
				callBack(this);
			}
		},
		//#endregion
		//#region getTab
		getTab: function (contentID) {
			return $('.hornav a[href="' + contentID + '"]').parent();
		},
		//#endregion
		//#region loadTab
		loadTab: function (_tabTitle, _elementID, _callBack, _removable, _changeToTab) {
			/// <summary>Adds a new tab with the supplied title, creates an element with the provided id, displays a loader and calls the provided function once added</summary>
			/// <param name="_tabTitle" type="String">The string title of the tab which is displayed within the tab.</param>
			/// <param name="_elementID" type="String">The id of an element to be created.</param>
			/// <param name="_callBack" type="Function">A function to execute after tab added.</param>
			if (_removable === undefined) {
				_removable = true;
			}

			if (_changeToTab === undefined) {
				_changeToTab = true;
			}

			var existingTab = $('.hornav a[href="#' + _elementID + '"]');
			if (existingTab.length > 0) {
				existingTab.click();
			} else {

				var newTabContent = $("<div style=\"min-height:250px;\" id=\"" + _elementID + "\"></div>");

				if ($("#contentwrapper").length > 0) {
					newTabContent.appendTo("#contentwrapper");
				} else {
					alert("No element with id of contentwrapper to append to. Please make sure that element exisits.");
					return;
				}

				Utils.tabs.addTabToPage(_tabTitle, _elementID, _removable, _changeToTab);

				//Utils.tabs.getCurrentTab().block();

				if (_callBack) {
					_callBack(this);
				}

			}
		},
		//#endregion
		bindAndInitTab: function (_tmplID, _dataToBind, _initMethod) {
			/// <summary>Binds data to the current tab using a specified jquery template. Calls an init method which will initialise the controls on the tab.</summary>
			/// <param name="_tmplID" type="String">The id of the jquery template to use.</param>
			/// <param name="_dataToBind" type="JSON object">The serialised entity or data to bind.</param>
			/// <param name="_initMethod" type="Function">A function to execute the data has been bound, will call a default init method if one is not provided.</param>
			var currentTab = Utils.tabs.getCurrentTab();
			$("#" + _tmplID).tmpl(_dataToBind).appendTo(currentTab);

			if (_initMethod) {
				_initMethod(this);
			} else {
				Utils.tabs.initTab();
			}
		},
		initTab: function (tabID) {
			var tab;
			if (tabID) {
				tab = $(tabID);
			}
			else {
				tab = Utils.tabs.getCurrentTab();
			}

			///// FORM TRANSFORMATION /////
			tab.find('input:checkbox, input:radio, select.uniformselect, input:file').uniform();

			///// TAG INPUT /////

			tab.find('.tags').tagsInput();

			///// SELECT WITH SEARCH /////
			tab.find(".chzn-select").chosen();

			//Jquery tabs
			tab.find(".tabs").tabs();

			//Date pickers
			tab.find(".isDatePicker").datepicker();

			//accordian
			tab.find(".accordion").accordion();
		},
		getCurrentTab: function () {
			return $($(".hornav li.current a").attr("href"));
		}
	},
	//#endregion

	//#region Table function
	tables: {
		//#region markAsDeleted
		markRowAsDeleted: function (_trElement) {
			_trElement.find("td").each(function () {
				$(this).addClass("tblRowDeleted");
			});
		},
		//#endregion
	},
	//#endregion

	//#region LookUpEnum
	LookUpEnum: {
		JobTitle: 10000,
		Saluation: 10001,
		IncidentType: 10002,
		IncidentSource: 10003,
		IncidentPriority: 10004
	},
	//#endregion

	bindDataToGrid: function (templateContainer, template, data) {

		$(templateContainer).children().remove();

		$(template).tmpl(data).appendTo(templateContainer);

		//if ($(".loader").is(":visible"))
		// $(".loader").slideToggle();
	},
	populateEntityFromForm: function (formID, entity) {
		for (var entityProperty in entity) {
			if (entityProperty != "__type") {
				var value;
				var $currentElem = $(formID).find(String.format('[name="{0}"]', entityProperty));

				if ($currentElem.length > 0) {
					value = $currentElem.val();

					//detect if the element is a datpicker, if so then use the built in Jquery UI date picker to get the javascript date
					if ($currentElem.hasClass("isDatePicker")) {
						value = $currentElem.datepicker("getDate");
					}
					else if (entityProperty.toUpperCase().indexOf("INSERTEDON") > -1 || entityProperty.toUpperCase().indexOf("DELETEDON") > -1) {
						value = null;
					}
					//Checkbox
					if ($currentElem.is("input[type=checkbox]")) {
						value = $currentElem.is(":checked");
					}
				}
				else {
					value = null;
				}

				if ($currentElem.val() !== "") {
					entity[entityProperty] = value;
				}
				else {
					entity[entityProperty] = null;
				}
			}
		}

		return entity;
	},
	responseFail: function (result) {

		if (result !== null) {

			var messageToCompare = result.get_message();
			var isThere = false;

			if (result.get_statusCode() === 0 || result.get_statusCode() === 503 || result.get_statusCode() === 404 || Utils.checkResponseErrors(result.get_message())) {
				messageToCompare = "You are not connected! Please check your network connection. <br>Error Code: " + result.get_statusCode();
			} else if (messageToCompare.indexOf("The server method") > -1) {
				messageToCompare += " Hard Error";
			}

			$("#jGrowl .jGrowl-message").each(function (index) {
				if ($(this).html() === messageToCompare) {
					isThere = true;
				}
			});
			if (!isThere) {
				$.jGrowl(messageToCompare, { header: "<b class='jGrowlError'>Error</b>", sticky: true });
			}
		}
	},
	checkResponseErrors: function (message) {
		var errorHash = [
			"There was no endpoint listening",
			"A network-related or instance-specific error"
		];
		var isCatchable = false;
		for (var i = 0; i < errorHash.length; i++) {
			if (message.indexOf(errorHash[i]) > -1) {
				isCatchable = true;
				break;
			}
		}
		return isCatchable;
	},
	keepAlive: function () {
		if (document.location.toString().toUpperCase().indexOf("PUBLIC") === -1) {

			var response = function (_result) {
				if (!_result.EntityList[0]) {
					//returned false, that means the session expired. Logout the user.
					document.location = "/Public/Logout.aspx";
				}
			};

			SecurityService.WCFSecurity.KeepAlive(response, Utils.responseFail);
		}
	},
	validation: {
		validateForm: function (jqSelector, failureMessage, clearForm, isNotVisible) {
			/// <summary>Validates any DOM formindex against a 'valtype' attribute</summary>
			/// <param name="jqSelector" type="String">Jquery typed selector used to find the collection of formindexs</param>
			/// <param name="failureMessage" type="String">Message to display once validation has failed</param>
			/// <param name="clearForm" type="Boolean">When true clears all data within formindexs</param>
			/// <param name="isNotVisible" type="Boolean">When true validates formindexs that are not visible</param>
			$(jqSelector).off("mouseover.validation");
			$(jqSelector).on("mouseover.validation", ".moreerrorinfo", null, function (e) {
				$.jGrowl("close");
				var target = $(e.currentTarget).siblings("[valmessage]");
				$.jGrowl(target.attr("valmessage"), { header: "Field Information" });
			});

			var form;
			if (isNotVisible) {
				form = $(jqSelector).find('select,input,textarea');
			}
			else {
				form = $(jqSelector).find('select,input,textarea');
			}
			var formId = jqSelector;
			form = form.toArray();
			var hasErrors = false;

			if (form.length === 0) {
				return true;
			}

			for (var formindex = 0; formindex < form.length; formindex++) {
				if (form[formindex].getAttribute("valtype")) {

					var toValidate = form[formindex].getAttribute("valtype").split(";");
					var isRequired = false;
					if (form[formindex].getAttribute("valtype").toUpperCase().indexOf("REQUIRED") > -1) {
						isRequired = true;
					}
					for (var i = 0; i < toValidate.length; i++) {
						var validate = toValidate[i].split(":");

						switch (validate[0].trim().toUpperCase()) {
							case "REQUIRED":
								if ($(form[formindex]).val() === "-1" || form[formindex].value === "" || form[formindex].selectedIndex == -1 || form[formindex].value == "-1" || (form[formindex].type == "radio" && $('input:radio[name="' + $(form[formindex]).attr('name') + '"]:checked').val() === undefined)) {
									hasErrors = true;
									Utils.validation.validateFail(form, formId, formindex, jqSelector);
								}
								else {
									Utils.validation.validatePass(form, formId, formindex, jqSelector);
								}
								break;
							case "DATEREQ":
								break;
							case "REGEX":
								//if (!hasErrors) {
								if ((isRequired === true) || (form[formindex].value !== "")) {
									var pattern = Utils.validation.validateRegex(validate[1]);
									var val = form[formindex].value;
									var test = val.match(new RegExp(pattern, 'gi'));
									if (!test) {
										hasErrors = true;
										form[formindex].setAttribute("valmessage", Utils.validation.validateRegexMessage(validate[1]));
										Utils.validation.validateFail(form, formId, formindex, jqSelector);
									} else {
										Utils.validation.validatePass(form, formId, formindex, jqSelector);
									}
								}
								//}
								break;
							default:
								break;
						}
					}
				}
			}

			if (failureMessage === "" || failureMessage === undefined) {
				failureMessage = Utils.validation.vaildationDefaultMessage;
			}
			if (hasErrors) {
				$.jGrowl(failureMessage, { header: "<span style='color:#fb9337;font-size:13px;'>Validation Error</span>" });
			}
			else {
				$(".moreerrorinfo").off("mouseover.validation", ".moreerrorinfo").remove();
			}

			return !hasErrors;
		},
		validatePass: function (form, formId, formindex, jqSelector) {
			/// <summary>Handles successful validation on an formindex</summary>
			var $currentElement = $(form[formindex]);

			if ($currentElement.hasClass("datepicker") || $currentElement.hasClass("dpicker")) {
				$currentElement.parent(".input-type-text").removeClass("error");
			}
			else {
				$currentElement.removeClass("error");
			}

			if ($.browser.msie) {
				$currentElement.unbind("focusout");
			} else {
				$currentElement.unbind("blur");
			}
		},
		validateFail: function (form, formId, formindex, jqSelector) {
			/// <summary>Handles unsuccessful validation on an formindex</summary>
			var $currentElement = $(form[formindex]);

			if ($currentElement.hasClass("datepicker") || $currentElement.hasClass("dpicker")) {
				$currentElement.parent(".input-type-text").addClass("error");
			}
			else {
				$currentElement.addClass("error");
			}

			if (!$currentElement.attr("infoon") && $currentElement.attr("valmessage")) {
				$currentElement.attr("infoon", "true");
				$currentElement.parent().append("<a class='moreerrorinfo error'>(?)</a>");
			}
		},
		validateRegex: function (regType) {
			switch (regType.trim().toUpperCase()) {
				case "DATE":
					return "^(((0[1-9]|[12]\\d|3[01])\\/(0[13578]|1[02])\\/((19|[2-9]\\d)\\d{2}))|((0[1-9]|[12]\\d|30)\\/(0[13456789]|1[012])\\/((19|[2-9]\\d)\\d{2}))|((0[1-9]|1\\d|2[0-8])\\/02\\/((19|[2-9]\\d)\\d{2}))|(29\\/02\\/((1[6-9]|[2-9]\\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$";
				case "STRING":
					return "[a-z][A-Z]";
				case "INT":
					return "^\\d+$";
				case "DOUBLE":
					return "^[-+]?[0-9]*\\.?[0-9]+$";
				case "PHONE":
					return "^[\\d]{10}$";
				case "PASSWORD":
					return "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{4,12}$"; //Password matching expression. Password must be at least 4 characters, no more than 8 characters, and must include at least one upper case letter, one lower case letter, and one numeric digit
				case "EMAIL":
					return "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$";
				default:
					return regType;
			}
		},
		validateRegexMessage: function (regType) {
			switch (regType.trim().toUpperCase()) {
				case "STRING":
					return "Only text characters are allowed for this field";
				case "INT":
					return "Only numeric characters are allowed for this field";
				case "DOUBLE":
					return "Only numeric values are allowed for this field";
				case "PHONE":
					return "Only phone numbers are allowed for this field, please use only numbers and exclude spaces. EG 0821234567";
				case "EMAIL":
					return "Only email addresses are allowed for this field";
				default:
					return "Please enter a value for this field.";
			}
		},
		vaildationDefaultMessage: "Please review the form"
	},
	ClearForm: function (_selector) {
		_selector = _selector || "";
		$(_selector + " :input").val("");
	},
	uploadFile: function (jqSelector) {
		var $fileElement = $(jqSelector);
		if ($fileElement.length > 0) {
			var fileData = {
				fileName: $fileElement.fileName,
				file: $fileElement.attr("value")
			};
			var formData = $("<form action='/HttpHandlers/FileUpload.ashx' method='post' enctype='multipart/form-data'></form>");

			$fileElement.clone().appendTo(formData);
			formData.appendTo(document);

			formData.submit();

			//$.ajax({
			//    type: "POST",
			//    url: "",
			//    data: fileData,
			//    success: function (result) {
			//        alert(result);
			//    },
			//    error: function () {
			//        alert("There was error uploading file!");
			//    }
			//});
		}
	},
	initDataPicker: function () {
		$(".isDatePicker").datepicker({ dateFormat: "dd/mm/yy" });
	},
	numericOnly: function (_elementID) {

		if ($(_elementID).length > 0) {
			$(_elementID).keydown(function (event) {
				// Allow: backspace, delete, tab, escape, and enter
				if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || event.keyCode == 110 || event.keyCode == 190 ||
					// Allow: Ctrl+A
                    (event.keyCode == 65 && event.ctrlKey === true) ||
					// Allow: home, end, left, right
                    (event.keyCode >= 35 && event.keyCode <= 39)) {
					// let it happen, don't do anything
					return;
				}
				else {
					// Ensure that it is a number and stop the keypress
					if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
						event.preventDefault();
					}
				}
			});
		}
	},
	clone: function (obj) {
		if (obj === null) { return obj; }
		if (typeof obj !== "object") { return obj; }

		if ("constructor" in obj) {
			if (obj.constructor === RegExp) { return obj; }
		}

		if ("constructor" in obj) {
			var retVal = new obj.constructor();
			for (var key in obj) {
				if (!obj.hasOwnProperty(key)) { continue; }
				retVal[key] = Utils.clone(obj[key]);
			}
			return retVal;
		} else {
			return obj;
		}
	},
	findInArray: function (ary, idfield) {
		///	<summary>
		///		Get query string elements by their name
		///	</summary>
		///	<returns type="string" />
		///	<param name="ary" type="Array">
		///		The array that you want to interrogate
		///	</param>
		///	<param name="idfield" type="String">
		///		The name of the column that you want to interrogate
		///	</param>

		for (var i = 0; i < ary.length; i++) {
			if (ary[i].name == idfield) {
				return ary[i].value;
			}
	}
		return null;
	},
	findByInArray: function (ary, idfield, value, returnIndex) {
		///	<summary>
		///		Get elements by their name
		///	</summary>
		///	<returns type="string" />
		///	<param name="ary" type="Array">
		///		The array that you want to interrogate
		///	</param>
		///	<param name="idfield" type="String">
		///		The name of the column that you want to interrogate
		///	</param>
		///	<param name="value" type="Variable">
		///		The value for the idfield column
		///	</param>
		///	<param name="returnIndex" type="Boolean">
		///		The index value for the idfield column
		///	</param>

		for (var i = 0; i < ary.length; i++) {
			if (value == ary[i][idfield]) {
				if (!returnIndex) {
					return ary[i];
				}
				else {
					return i;
				}

			}
		}
		return null;
	},
	groupByInArray: function (ary, idfield) {
		///	<summary>
		///		Returns a grouped array
		///	</summary>
		///	<returns type="array-like object" />
		///	<param name="ary" type="Array">
		///		The array that you want to interrogate
		///	</param>
		///	<param name="idfield" type="String">
		///		The name of the column that you want to group by
		///	</param>
		var arraySet = {};
		for (var i = 0; i < ary.length; i++) {
			if (arraySet[ary[i][idfield]] === undefined) {
				arraySet[ary[i][idfield]] = [];
			}
			arraySet[ary[i][idfield]].push(ary[i]);
		}
		return arraySet;
	},
	groupByInArrayObject: function (arr, idfield) {
		///	<summary>
		///		Returns a grouped array
		///	</summary>
		///	<returns type="array" />
		///	<param name="ary" type="Array">
		///		The array that you want to interrogate
		///	</param>
		///	<param name="idfield" type="String">
		///		The name of the column that you want to group by
		///	</param>
		var newArray = [];
		for (var i = 0; i < arr.length; i++) {
			var value = arr[i][idfield];
			var group = Utils.findByInArray(newArray, idfield, value);
			if (group === undefined || group === null) {
				var objContainer = {
					Collection: [arr[i]]
				};
				objContainer[idfield] = value;
				newArray.push(objContainer);
			} else {
				group.Collection.push(arr[i]);
			}
		}
		return newArray;
	}
};
//#endregion

//#region JQuery Extensions

$.fn.insertAtCaret = function (text) {
	return this.each(function () {
		if (document.selection && this.tagName == 'TEXTAREA') {
			//IE textarea support
			this.focus();
			var sel = document.selection.createRange();
			sel.text = text;
			this.focus();
		} else if (this.selectionStart || this.selectionStart == '0') {
			//MOZILLA/NETSCAPE support
			var startPos = this.selectionStart;
			var endPos = this.selectionEnd;
			var scrollTop = this.scrollTop;
			this.value = this.value.substring(0, startPos) + text + this.value.substring(endPos, this.value.length);
			this.focus();
			this.selectionStart = startPos + text.length;
			this.selectionEnd = startPos + text.length;
			this.scrollTop = scrollTop;
		} else {
			// IE input[type=text] and other browsers
			this.value += text;
			this.focus();
			this.value = this.value;    // forces cursor to end
		}
	});
};

//#region $.fn.mapInputsToObject - Maps form element values to an object by field.
$.fn.mapToObject = function (object, field) {
	/// <summary>
	/// Maps form element values to an object by field name.
	/// Currently only maps text, hidden, checkbox, selects but can and will be expanded
	/// </summary>
	/// <param name="object" type="object">(optional) The object to map values to / extend.</param>
	/// <param name="field" type="string">(optional) The field name to map values to.</param>
	/// <returns type="String" />
	object = object || {};
	field = field || "id";
	var inputs = this.find(':input');
	inputs.each(function () {
		var i = $(this);
		if (i.is('input[type="text"], input[type="hidden"], select, textarea')) {
			if (i.hasClass("isDatePicker")) {
				object[i.attr(field)] = i.datepicker("getDate");
			} else {
				object[i.attr(field)] = (i.val() !== "") ? i.val() : null;
			}
		}
		else if (i.is('input[type="checkbox"]')) {
			object[i.attr(field)] = i.is(":checked");
		} else if (i.is("input[type='radio']")) {

			if (i.is(":checked")) {
				object[i.attr(field)] = i.val();
			}
		}
		//TODO: Garth - Expand to cover more input types.
	});

	return object;
};
//#endregion
//#region $.fn.applyObjecttoInputs- Maps object values to form elements by field.
$.fn.mapObjectTo = function (object, field, overwrite) {
	/// <summary>
	/// Maps object values to form elements by field.
	/// Currently only maps text, hidden, checkbox but can and will be expanded
	/// </summary>
	/// <param name="object" type="object">The object to draw values from.</param>
	/// <param name="field" type="string">(optional) The field to use as key for value mapping.</param>
	/// <returns type="String" />
	object = object || {};
	overwrite = overwrite || false;
	field = field || "id";
	var inputs = this.find(':input, .field[' + field + ']');
	inputs.each(function () {
		var i = $(this);
		if (i.is('input[type="text"], input[type="hidden"], select, textarea')) {
			var value;

			if (object[i.attr(field)] instanceof Date) {
				value = object[i.attr(field)].format("dd/MM/yyyy");
			}
			else {
				value = object[i.attr(field)];
			}


			if (overwrite) {
				i.val(value !== null ? value : "");
			}
			else {
				i.val(value !== null ? value : i.val());
			}
		}
		else if (i.is('input[type="checkbox"]')) {
			if (object[i.attr(field)]) {
				i.attr('checked', 'checked');
			}
			else {
				i.removeAttr('checked');
			}
		}
		else if (i.is('.field')) {
			i.html(object[i.attr(field)] || "&nbsp;");
		}
		//TODO: Garth - Expand to cover more input types.
	});

	return object;
};
//#endregion
//#endregion

Number.prototype.formatMoney = function (decPlaces, thouSeparator, decSeparator) {
	var n = this,
        decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,//ignore jslint
        decSeparator = decSeparator === undefined ? "." : decSeparator,//ignore jslint
        thouSeparator = thouSeparator === undefined ? "," : thouSeparator,//ignore jslint
        sign = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces),10) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
	return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
};

//#region date format
/*
 * Date Format 1.2.3
 * (c) 2007-2009 Steven Levithan <stevenlevithan.com>
 * MIT license
 *
 * Includes enhancements by Scott Trenda <scott.trenda.net>
 * and Kris Kowal <cixar.com/~kris.kowal/>
 *
 * Accepts a date, a mask, or a date and a mask.
 * Returns a formatted version of the given date.
 * The date defaults to the current date/time.
 * The mask defaults to dateFormat.masks.default.
 */

var dateFormat = function () {
	var token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,//ignore jslint
		timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,//ignore jslint
		timezoneClip = /[^-+\dA-Z]/g,//ignore jslint
		pad = function (val, len) {
			val = String(val);
			len = len || 2;
			while (val.length < len) { val = "0" + val; }
			return val;
		};

	// Regexes and supporting functions are cached through closure
	return function (date, mask, utc) {
		var dF = dateFormat;

		// You can't provide utc if you skip other args (use the "UTC:" mask prefix)
		if (arguments.length == 1 && Object.prototype.toString.call(date) == "[object String]" && !/\d/.test(date)) {
			mask = date;
			date = undefined;
		}

		// Passing date through Date applies Date.parse, if necessary
		date = date ? new Date(date) : new Date();
		if (isNaN(date)) { throw SyntaxError("invalid date"); }

		mask = String(dF.masks[mask] || mask || dF.masks["default"]);

		// Allow setting the utc argument via the mask
		if (mask.slice(0, 4) == "UTC:") {
			mask = mask.slice(4);
			utc = true;
		}

		var _ = utc ? "getUTC" : "get",
			d = date[_ + "Date"](),
			D = date[_ + "Day"](),
			m = date[_ + "Month"](),
			y = date[_ + "FullYear"](),
			H = date[_ + "Hours"](),
			M = date[_ + "Minutes"](),
			s = date[_ + "Seconds"](),
			L = date[_ + "Milliseconds"](),
			o = utc ? 0 : date.getTimezoneOffset(),
			flags = {
				d: d,
				dd: pad(d),
				ddd: dF.i18n.dayNames[D],
				dddd: dF.i18n.dayNames[D + 7],
				m: m + 1,
				mm: pad(m + 1),
				mmm: dF.i18n.monthNames[m],
				mmmm: dF.i18n.monthNames[m + 12],
				yy: String(y).slice(2),
				yyyy: y,
				h: H % 12 || 12,
				hh: pad(H % 12 || 12),
				H: H,
				HH: pad(H),
				M: M,
				MM: pad(M),
				s: s,
				ss: pad(s),
				l: pad(L, 3),
				L: pad(L > 99 ? Math.round(L / 10) : L),
				t: H < 12 ? "a" : "p",
				tt: H < 12 ? "am" : "pm",
				T: H < 12 ? "A" : "P",
				TT: H < 12 ? "AM" : "PM",
				Z: utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
				o: (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
				S: ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
			};

		return mask.replace(token, function ($0) {
			return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
		});
	};
}();

// Some common format strings
dateFormat.masks = {
	"default": "ddd mmm dd yyyy HH:MM:ss",
	shortDate: "m/d/yy",
	mediumDate: "mmm d, yyyy",
	longDate: "mmmm d, yyyy",
	fullDate: "dddd, mmmm d, yyyy",
	shortTime: "h:MM TT",
	mediumTime: "h:MM:ss TT",
	longTime: "h:MM:ss TT Z",
	isoDate: "yyyy-mm-dd",
	isoTime: "HH:MM:ss",
	isoDateTime: "yyyy-mm-dd'T'HH:MM:ss",
	isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
};

// Internationalization strings
dateFormat.i18n = {
	dayNames: [
		"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
		"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
	],
	monthNames: [
		"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
		"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
	]
};

//#endregion

//#region Master Load
$(function () {
	Utils.masterLoadEvent();

	try {

		Sys.Serialization.JavaScriptSerializer._serializeWithBuilder = function Sys$Serialization$JavaScriptSerializer$_serializeWithBuilder(object, stringBuilder, sort, prevObjects) {
			var i;
			switch (typeof object) {
				case 'object':
					if (object) {
						if (prevObjects) {
							for (var j = 0; j < prevObjects.length; j++) {
								if (prevObjects[j] === object) {
									throw Error.invalidOperation(Sys.Res.cannotSerializeObjectWithCycle);
								}
							}
						}
						else {
							prevObjects = [];
						}
						try {
							Array.add(prevObjects, object);

							if (Number.isInstanceOfType(object)) {
								Sys.Serialization.JavaScriptSerializer._serializeNumberWithBuilder(object, stringBuilder);
							}
							else if (Boolean.isInstanceOfType(object)) {
								Sys.Serialization.JavaScriptSerializer._serializeBooleanWithBuilder(object, stringBuilder);
							}
							else if (String.isInstanceOfType(object)) {
								Sys.Serialization.JavaScriptSerializer._serializeStringWithBuilder(object, stringBuilder);
							}

							else if (Array.isInstanceOfType(object)) {
								stringBuilder.append('[');

								for (i = 0; i < object.length; ++i) {
									if (i > 0) {
										stringBuilder.append(',');
									}
									Sys.Serialization.JavaScriptSerializer._serializeWithBuilder(object[i], stringBuilder, false, prevObjects);
								}
								stringBuilder.append(']');
							}
							else {
								if (Date.isInstanceOfType(object)) {
									stringBuilder.append('"\\/Date(');
									stringBuilder.append(object.getTime());
									if (object.getTimezoneOffset() == -120) {
										stringBuilder.append(object.getTimezoneOffset() / 60);
									}
									else {
										var newtimeoffset = (object.getTimezoneOffset() + 120) / 60;
										if (newtimeoffset > 0) {
											newtimeoffset = "+" + newtimeoffset;
										}
										stringBuilder.append(newtimeoffset);
									}

									stringBuilder.append(')\\/"');
									break;
								}
								var properties = [];
								var propertyCount = 0;
								for (var name in object) {
									if (name.startsWith('$')) {
										continue;
									}
									if (name === Sys.Serialization.JavaScriptSerializer._serverTypeFieldName && propertyCount !== 0) {
										properties[propertyCount++] = properties[0];
										properties[0] = name;
									}
									else {
										properties[propertyCount++] = name;
									}
								}
								if (sort) { properties.sort(); }
								stringBuilder.append('{');
								var needComma = false;

								for (i = 0; i < propertyCount; i++) {
									var value = object[properties[i]];
									if (typeof value !== 'undefined' && typeof value !== 'function') {
										if (needComma) {
											stringBuilder.append(',');
										}
										else {
											needComma = true;
										}

										Sys.Serialization.JavaScriptSerializer._serializeWithBuilder(properties[i], stringBuilder, sort, prevObjects);
										stringBuilder.append(':');
										Sys.Serialization.JavaScriptSerializer._serializeWithBuilder(value, stringBuilder, sort, prevObjects);

									}
								}
								stringBuilder.append('}');
							}
						}
						finally {
							Array.removeAt(prevObjects, prevObjects.length - 1);
						}
					}
					else {
						stringBuilder.append('null');
					}
					break;
				case 'number':
					Sys.Serialization.JavaScriptSerializer._serializeNumberWithBuilder(object, stringBuilder);
					break;
				case 'string':
					Sys.Serialization.JavaScriptSerializer._serializeStringWithBuilder(object, stringBuilder);
					break;
				case 'boolean':
					Sys.Serialization.JavaScriptSerializer._serializeBooleanWithBuilder(object, stringBuilder);
					break;
				default:
					stringBuilder.append('null');
					break;
			}
		};

	} catch (e) { }

});
//#endregion