﻿using Honeycomb.Custom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Optimization;
using System.Web.Caching;

namespace Honeycomb.Web {
	public class Global : System.Web.HttpApplication {

		protected void Application_Start(object sender, EventArgs e) {
			Honeycomb.Custom.Application.ApplicationManager.JobScheduler = new SAWD.JobAgent.JobScheduler();

			//var bundles = BundleTable.Bundles;
			////file upload bundle
			//bundles.Add(new ScriptBundle("~/Scripts/fileupload.js").Include(
			//    "~/Scripts/plugins/fileupload/header.js",
			//    "~/Scripts/plugins/fileupload/util.js",
			//    "~/Scripts/plugins/fileupload/button.js",
			//    "~/Scripts/plugins/fileupload/handler.base.js",
			//    "~/Scripts/plugins/fileupload/handler.form.js",
			//    "~/Scripts/plugins/fileupload/handler.xhr.js",
			//    "~/Scripts/plugins/fileupload/uploader.basic.js",
			//    "~/Scripts/plugins/fileupload/dnd.js",
			//    "~/Scripts/plugins/fileupload/uploader.js",
			//    "~/Scripts/plugins/fileupload/jquery-plugin.js"));

		}

		protected void Session_Start(object sender, EventArgs e) {

		}

		protected void Application_BeginRequest(object sender, EventArgs e) {
			if(Request.IsLocal)
				System.Threading.Thread.Sleep(50);

			var request = Request;
			string currentRelativePath = request.AppRelativeCurrentExecutionFilePath;

			if(request.HttpMethod == "GET") {
				if(currentRelativePath.EndsWith(".aspx")) {
					var folderPath = currentRelativePath.Substring(2, currentRelativePath.LastIndexOf('/') - 1);
					Response.Filter = new SAWD.WebVersioning.JSVersion(Response, relativePath => {
						if(Context.Cache[relativePath] == null) {
							try {
								var physicalPath = Server.MapPath(relativePath);
								var version = "?v=" + new System.IO.FileInfo(physicalPath).LastWriteTime.ToString("yyyyMMddhhmmss");
								Context.Cache.Add(relativePath, version, null, DateTime.Now.AddMinutes(1), TimeSpan.Zero, CacheItemPriority.Normal, null);
								return version;
							} catch {
								return "";
							}

						} else {
							return Context.Cache[relativePath] as string;
						}
					});
				}
			}
		}

		protected void Application_AuthenticateRequest(object sender, EventArgs e) {

		}

		protected void Application_Error(object sender, EventArgs e) {

		}

		protected void Session_End(object sender, EventArgs e) {

		}

		protected void Application_End(object sender, EventArgs e) {

		}
	}
}