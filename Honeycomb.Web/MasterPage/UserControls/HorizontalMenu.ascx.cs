﻿#region Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Honeycomb.Custom.Data;
using System.Text;
using System.Data.Linq;
using Honeycomb.Custom;
#endregion

namespace Honeycomb.Web.MasterPage.UserControls {
    public partial class HorizontalMenu : System.Web.UI.UserControl {
        Honeycomb_Entities context = new Honeycomb_Entities();
        protected void Page_Load(object sender, EventArgs e) {
            if (String.IsNullOrEmpty(CacheManager.HorizontalMenu)) {
                GenerateMenu();
            } else {
                litHorizontalMenuContainer.Text = CacheManager.HorizontalMenu;
            }
        }

        private void GenerateMenu() {
            StringBuilder menuBuilder = new StringBuilder();
            string menuTemplate = "<li {0} ><a href=\"{1}\"  pagetitle=\"{3}\" pagedesc=\"{4}\"><span class=\"icon {5}\"></span>{3}</a></li>";
            var horizontalMenu = context.HorizontalMenu.Select(t => t).OrderBy(ord=> ord.ItemOrder);
            var userRoles = SessionManager.UserRoles;

            //start ul
            menuBuilder.Append("<ul class=\"headermenu\">");

            //generate menu
            foreach (Custom.Data.HorizontalMenu menuItem in horizontalMenu) {
                if(userHasAccess(menuItem.Roles, userRoles)) {
                    string currentClass = menuItem.Url == Request.Url.AbsolutePath ? "class=\"current\"" : string.Empty;
                    menuBuilder.Append(String.Format(menuTemplate, currentClass, menuItem.Url, menuItem.ID, menuItem.Title, menuItem.Detail, menuItem.CSSClassName));
                    if(!String.IsNullOrEmpty(currentClass)) {
                        SessionManager.HorizontalPageID = menuItem.ID;
                    }
                }
            }

            //end ul
            menuBuilder.Append("</ul>");

            litHorizontalMenuContainer.Text = menuBuilder.ToString();

            ((VerticalMenu)Page.Master.FindControl("VerticalMenu1")).GenerateMenu();
        }

        private bool userHasAccess(string menuRoles, List<string> userRoles) {
            if(!string.IsNullOrEmpty(menuRoles)) {

                List<string> menuAllowedRoles = new List<string>(menuRoles.Split(','));

                return menuAllowedRoles.Intersect(userRoles).Any();
            } else {
                return false;
            }
        }
    }
}