﻿#region Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Honeycomb.Custom;
using Honeycomb.Custom.Data;
#endregion

namespace Honeycomb.Web.MasterPage.UserControls {
    public partial class VerticalMenu : System.Web.UI.UserControl {
        Honeycomb_Entities context = new Honeycomb_Entities();
        protected void Page_Load(object sender, EventArgs e) {

        }

        public void GenerateMenu() {
            StringBuilder menuBuilder = new StringBuilder();
            
                string menuTemplate = "<li><a href=\"{0}\" class=\"{1}\" pagetitle=\"{2}\" pagedesc=\"{3}\">{2}</a>{4}</li>";
                var verticalMenu = context.VerticalMenu.Where(t => t.HorizontalMenuID == SessionManager.HorizontalPageID && t.Parent == 0).OrderBy(ord => ord.ItemOrder).ToList<Custom.Data.VerticalMenu>();

                if (verticalMenu.Count() == 0) {
                    litVerticalMenuContainer.Visible = false;
                    Page.ClientScript.RegisterStartupScript(GetType(), "No Vertical Menu", "$('.vernav2').hide();$('body').removeClass('withvernav');$('.centercontent').removeClass('centercontent');", true);
                    return;
                }
                //start ul
                menuBuilder.Append("<ul>");

                //generate menu
                foreach (Custom.Data.VerticalMenu menuItem in verticalMenu) {
					//if the menu item does not have roles specified, add them to the menu structure
					if (string.IsNullOrEmpty(menuItem.Roles)) {
						//check if item has children
						string children = GenerateChildMenu(menuItem.ID);
						if (!String.IsNullOrEmpty(children))
							menuBuilder.Append(String.Format(menuTemplate, "#parent_" + menuItem.ID, menuItem.CSSClassName, menuItem.Title, menuItem.Detail, children));
						else
							menuBuilder.Append(String.Format(menuTemplate, menuItem.Url, menuItem.CSSClassName, menuItem.Title, menuItem.Detail, string.Empty));
					} else {
						//if there are roles specified check that the current user has the role that gives them access to the menu item.
						if (userHasAccess(menuItem.Roles, SessionManager.UserRoles)) {
							//check if item has children
							string children = GenerateChildMenu(menuItem.ID);
							if (!String.IsNullOrEmpty(children))
								menuBuilder.Append(String.Format(menuTemplate, "#parent_" + menuItem.ID, menuItem.CSSClassName, menuItem.Title, menuItem.Detail, children));
							else
								menuBuilder.Append(String.Format(menuTemplate, menuItem.Url, menuItem.CSSClassName, menuItem.Title, menuItem.Detail, string.Empty));
						}

					}
                }

                //end ul
                menuBuilder.Append("</ul>");
                litVerticalMenuContainer.Text = menuBuilder.ToString();
                CacheManager.VerticalMenu = menuBuilder.ToString();
            


        }
        //This is a user control. Are you using this??
        //yes I am but how is that relevant to the issue?
        //you are calling the Vertical Menu here right?

        //No I dont interact with the service from here on from the js
        private string GenerateChildMenu(long parentID) {
            var menuItems = context.VerticalMenu.Where(mnu => mnu.Parent == parentID);
            StringBuilder menuBuilder = new StringBuilder();

            if (menuItems.Count() > 0) {
                string menuTemplate = "<li><a href=\"{0}\"  horizontalmenu=\"{1}\" pagetitle=\"{2}\" pagedesc=\"{3}\">{2}</a></li>";
                //start ul
                menuBuilder.Append(String.Format("<span class=\"arrow\"></span><ul id='parent_{0}'>", parentID));

				//generate menu
				foreach (Custom.Data.VerticalMenu menuItem in menuItems) {
					//if no roles required for item, include it
					if (string.IsNullOrEmpty(menuItem.Roles)) {
						menuBuilder.Append(String.Format(menuTemplate, menuItem.Url, SessionManager.HorizontalPageID, menuItem.Title, menuItem.Detail));
					} else {
						//check if the logged in user has the correct role for the menu item
						if (userHasAccess(menuItem.Roles, SessionManager.UserRoles)) {
							menuBuilder.Append(String.Format(menuTemplate, menuItem.Url, SessionManager.HorizontalPageID, menuItem.Title, menuItem.Detail));
						}
					}
				}

                //end ul
                menuBuilder.Append("</ul>");
            }
            return menuBuilder.ToString();
        }

		private bool userHasAccess(string menuRoles, List<string> userRoles) {
			if (!string.IsNullOrEmpty(menuRoles)) {

				List<string> menuAllowedRoles = new List<string>(menuRoles.Split(','));

				return menuAllowedRoles.Intersect(userRoles).Any();
			} else {
				return false;
			}
		}
    }
}