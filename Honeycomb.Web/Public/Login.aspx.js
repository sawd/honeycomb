﻿/*global jQuery, login*/
/// <reference path="/Scripts/custom/general.js" />
/// <reference path="/Scripts/plugins/jquery-ui-1.8.16.custom.min.js" />
/// <reference path="/Scripts/plugins/jquery-1.7.min.js" />
/// <reference path="/Scripts/plugins/MicrosoftAjaxTemplates.debug.js" />
/// <reference path="/Scripts/plugins/MicrosoftAjax.debug.js" />
/// <reference path="/Services/HoneycombWCF.Core.svc/js" />
/// <reference path="/Services/HoneycombWCF.Security.svc/js" />
/// <reference path="/Scripts/custom/index.js" />
$(function () {
	///// TRANSFORM CHECKBOX /////							
	jQuery('input:checkbox').uniform();

	///// LOGIN FORM SUBMIT /////
	jQuery('#LoginUser_btnSignup').click(login.validateLogin);

	///// ADD PLACEHOLDER /////
	jQuery('#LoginUser_UserName').attr('placeholder', 'Username');
	jQuery('#LoginUser_Password').attr('placeholder', 'Password');

});

var login = {//ignore jslint
	validateLogin: function () {
		if (jQuery('#LoginUser_UserName').val() === '' && jQuery('#LoginUser_Password').val() === '') {
			jQuery('.nousername').fadeIn();
			jQuery('.nopassword').hide();
			return false;
		}
		if (jQuery('#LoginUser_UserName').val() !== '' && jQuery('#LoginUser_Password').val() === '') {
			jQuery('.nopassword').fadeIn().find('.userlogged h4, .userlogged a span').text(jQuery('#LoginUser_UserName').val());
			jQuery('.nousername,.username').hide();
			return false;
		}
	},
	invalidLogin: function (message) {
		$(".loginmsg").text(message);
		jQuery('.invalidlogin').fadeIn(500);
	}
};