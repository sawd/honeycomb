﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Honeycomb.Web.Public.Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Login Page | Estate Management</title>
    <link rel="stylesheet" href="/Theme/css/style.default.css" type="text/css" />
    <script type="text/javascript" src="/Scripts/plugins/jquery-1.7.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.cookie.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="/Scripts/plugins/jquery.blockUI.js"></script>
    <script type="text/javascript" src="/Scripts/custom/general.js"></script>
    <script type="text/javascript" src="Login.aspx.js"></script>
    <!--[if IE 9]>
    <link rel="stylesheet" media="screen" href="/Theme/css/style.ie9.css"/>
<![endif]-->
    <!--[if IE 8]>
    <link rel="stylesheet" media="screen" href="/Theme/style.ie8.css"/>
<![endif]-->
    <!--[if lt IE 9]>
	<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->
</head>

<body class="loginpage">
    <form id="login" runat="server">
        <asp:Login ID="LoginUser" runat="server" DestinationPageUrl="~/Pages/Dashboard/Home.aspx" onloginerror="LoginUser_LoginError" EnableViewState="false" RenderOuterTable="false">
            <LayoutTemplate>
                <div class="loginbox">
                    <div class="loginboxinner">

                        <div class="logo">
                            <h1><span>SEEHOA</span></h1>
                            <p style="font-style:normal;font-size:14px;">HONEYCOMB</p>
                            <p>Residential Community Management Solution</p>
                        </div>
                        <!--logo-->

                        <br clear="all" />
                        <br />

                        <div class="invalidlogin">
                            <div class="loginmsg">The username and password did not return a match,please try again.</div>
                        </div>
                        <!--invalidlogin-->

                        <div class="nousername">
                            <div class="loginmsg">Please enter your details.</div>
                        </div>
                        <!--nousername-->

                        <div class="nopassword">
                            <div class="loginmsg">Please enter your password.</div>
                            <div class="loginf">
                                <div class="thumb">
                                    <img alt="" src="/Theme/images/thumbs/avatar1.png" />
                                </div>
                                <div class="userlogged">
                                    <h4></h4>
                                    <a href="/Public/Login.aspx">Not <span></span>?</a>
                                </div>
                            </div>
                            <!--loginf-->
                        </div>
                        <!--nopassword-->



                        <div class="username">
                            <div class="usernameinner">
                               <asp:TextBox ID="UserName" runat="server" name="username" Style="width: 285px" title="Username"></asp:TextBox>
                            </div>
                        </div>

                        <div class="password">
                            <div class="passwordinner">
                              <asp:TextBox ID="Password" runat="server" TextMode="Password" name="password" ></asp:TextBox>
                            </div>
                        </div>
                        <asp:Button ID="btnSignup" CommandName="Login" OnClientClick="return login.validateLogin();" CssClass="button" runat="server" Text="Sign In" />

                        <%--<div class="keep">
                            <input type="checkbox" />
                            Keep me logged in
                        </div>--%>



                    </div>
                    <!--loginboxinner-->
                </div>
            </LayoutTemplate>
        </asp:Login>
        <!--loginbox-->
    </form>

</body>
</html>
