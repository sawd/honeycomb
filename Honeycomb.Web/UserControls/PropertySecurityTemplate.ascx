﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PropertySecurityTemplate.ascx.cs" Inherits="Honeycomb.Web.UserControls.PropertySecurity" %>
<div class="stdform">
    <div class="one_half nomargin">
        <div class="contenttitle2">
            <h3>Incidents</h3>
        </div>
        <p><a href="#" title="Click here to log incident">Click here</a> to log incident.</p>
        <h4>Security Detail</h4>
        <p>
            <label>Alarm Company:</label>
            <span class="field">
                <input type="text" name="AlarmCompany" value=""/>
            </span>
        </p>
        <p>
            <label>Radio Transmitter No:</label>
            <span class="field">
                <input type="text" name="TransmitterNo" value=""/>
            </span>
        </p>
    </div>
    <div class="one_half nomargin">
        <div class="contenttitle2"><h3>Incident History</h3></div>
        <%-- ownership history container --%>
        <div id="IncidentHistoryContainer" class="accordion"></div>
        <!-- accordion -->
    </div>
    <div class="clearall"></div>
</div>