﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContractorStaffTemplate.ascx.cs" Inherits="Honeycomb.Web.UserControls.ContractorStaffTemplate" %>
<script id="StaffTemplate" type="text/x-jquery-tmpl">
    <h3><a href="#">${Lastname}, ${FirstName}</a></h3>
    <div id="staffno-${ID}" style="display:none;">
        <input type="hidden" name="ID" value="${ID}" />
        <input type="hidden" name="FileID" value="${FileID}" />
        <div class="stdform">
            <div style="width:150px;" class="floatLeft">Blacklisted <input type="checkbox" name="BlackListed" value="1" {{if BlackListed}}checked="checked"{{/if}} /></div>
            <div style="width:150px;" class="floatLeft">Is Active: <input type="checkbox" name="Active" value="1" {{if Active}}checked="checked"{{/if}}  /></div>

            <div class="clearall"></div>

            <div class="clearMe">
                <div class="floatLeft">
                    <%-- details table --%>
                    <table class="details">
                        <tr>
                            <td class="label">I.D./Passport</td>
                            <td class="formfield"><input type="text" name="GOVID" staffid="${ID}" value="${GOVID}" valtype="required;" class="smallinput" /></td>
                            <td class="label">Gender</td>
                            <td class="formfield">
                                <select name="Gender" style="width:180px;" class="uniformselect">
                                    <option {{if Gender == 'Male'}}selected="selected"{{/if}} value="Male">Male</option>
                                    <option {{if Gender == 'Female'}}selected="selected"{{/if}} value="Female">Female</option>
                                </select>f
                            </td>
                        </tr>
                        <tr>
                            <td class="label">Title</td>
                            <td class="formfield">
                                <select name="SalutationID" class="uniformselect">
                                </select>
                            </td>
                            <td class="label">Job Title</td>
                            <td class="formfield">
                                <select style="width:180px;" name="JobTitleID" class="uniformselect">
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">First Name</td>
                            <td class="formfield"><input type="text" name="FirstName" value="${FirstName}" class="smallinput" /></td>
                            <td class="label">Last Name</td>
                            <td class="formfield"><input type="text" name="Lastname" value="${Lastname}" class="smallinput" /></td>
                        </tr>
                        <tr>
                            <td class="label">Date of Birth</td>
                            <td class="formfield"><input type="text" name="DOB" value="{{if DOB!=null}}${dateFormat(DOB,'dd/mm/yyyy')}{{/if}}" class="smallinput isDatePicker" /></td>
                            <td class="label">Email</td>
                            <td class="formfield"><input type="text" name="Email" value="${Email}" class="smallinput" /></td>
                        </tr>
                        <tr>
                            <td class="label">Mobile No</td>
                            <td class="formfield"><input type="text" name="Cellphone" value="${Cellphone}" valtype="regex:phone" class="smallinput" /></td>
                            <td class="label">Access Ref#</td>
                            <td class="formfield"><input type="text" name="AccessReffNo" value="${AccessReffNo}" class="smallinput" /></td>
                        </tr>
                    </table>
                </div>
                <div class="floatLeft">
                    <%-- photo --%>
                    <div class="photoDiv{{if FileID!=null}} photoDivWithPhoto{{/if}}" {{if FileID!=null}} image="url(/Pages/DownloadFile.ashx?FileRef=${FileID})"{{/if}}>
                        <div class="example">
                            <div staffid="${ID}" class="fubExample"></div>
                            <div class="fubUploadButton" class="btn btn-primary">Upload Image</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="contenttitle2">
                <h3>Current Access Permissions</h3>
            </div>

            <div class="clearMe readonlytaginputs">
                <div class="floatLeft associatedSites">Associated Sites (<a href="javascript:void(0);">click here to manage</a>) <input name="staffErven" class="longinput readOnlyTags" value="" style="display: none;"></div>
                <div class="floatLeft buttonsdiv">
                    <a staffid="${ID}" href="javascript:void(0);" class="btn btn_note radius50 staffNotesButton"><span>Notes</span></a><br/>
                    <a staffid="${ID}" href="javascript:void(0);" style="margin-top:20px;" class="btn  btn_pencil radius50 updatestaff"><span>Update Staff</span></a>
                </div>
            </div>

            <div class="CurrentCompanyStyle" style="position:absolute; left:485px; top:235px;" >

            <div class="contenttitle2">
                <h3>Current Companies</h3>
            </div>
            <div class="StaffCompanies">
                <div class="CompaniesDisplay" style=" width:312px; height:112px; border:1px solid #CCC; margin-top:15px; float:left;"> </div>
            </div>
                
            </div> <!-- close div CurrentCompanyStyle -->
                      
        </div>
    </div>
</script>

<script type="text/x-jquery-tmpl" id="StaffNotesTemplate">
    <option value="${ID}">${Title} - ${dateFormat(InsertedOn,"dd/mm/yyyy")}</option>
</script>