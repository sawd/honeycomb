﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IncidentCategoryCountGraph.ascx.cs" Inherits="Honeycomb.Web.UserControls.Dashboard.IncidentCategoryCountGraph" %>
<script src="/UserControls/Dashboard/IncidentCategoryCountGraph.js"></script>

<div id="incidentCategoryCountsGraph" class="dashboardWidget width2X incidentCategoryCount">
	<div class="innerContainer">
		<h2>Incidents by type<span class="graphButtons"><button type="button" class="stdbtn refresh"><span></span></button><button type="button" class="stdbtn year">Year</button><button type="button" class="stdbtn month">Month</button><button type="button" class="stdbtn week btn_orange">Week</button></span></h2>
		<div class="graphContainer">
			<div class="dashboardGraph" ></div>
		</div>
	</div>
</div>