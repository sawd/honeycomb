﻿/*global window,events,DashboardService,Utils,dateFormat,jQuery,Globals*/
(function ($, window, document, undefined) {
	var methods = {
		init: function () {
			$('.peopleTotalsTable').graph({
				renderGraph: methods.getData
			});
		},
		render: function (data, container) {
			if (data.length > 0) {
				var $table = $('<table/>', {
					style: 'width: 100%;',
					cellpadding: 0,
					cellspacing: 0
				}).append($('<tr/>').append('<th>Type</th><th>Amount</th>'));
				for (var i = 0; i < data.length; i++) {
					$table.append('<tr><td>' + data[i].label + '</td><td align="right">' + data[i].data + '</td>');
				}
				container.html($table);
			} else {
				container.html('No Data to Chart');
			}

		},
		getData: function (event, result) {
			var placeholder = result.container;
			placeholder.parent().parent().block({ message: null });
			DashboardService.WCFDashboard.GetTotalOccupants(events.getDataSuccess, Utils.responseFail, placeholder);
		}
	};

	var events = {
		getDataSuccess: function (result, context) {
			context.parent().parent().unblock();
			methods.render(result.EntityList, context);
		}
	};

	$(function () {
		methods.init();
	});

})(jQuery, window, document);

