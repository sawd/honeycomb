﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AccessCodesForEntranceHistorical.ascx.cs" Inherits="Honeycomb.Web.UserControls.Dashboard.AccessCodesForEntranceHistorical" %>
<script src="/usercontrols/dashboard/accesscodesforentrancehistorical.ascx.js"></script>
<div id="accessCodesForEntranceHistoricalGraph" class="dashboardWidget accessCodesForEntranceHistorical width2X">
	<div class="innerContainer">
		<h2>Access Codes Historical<span class="graphButtons"><button type="button" class="stdbtn refresh"><span></span></button><button type="button" class="stdbtn year">Year</button><button type="button" class="stdbtn month">Month</button><button type="button" class="stdbtn week btn_orange">Week</button></span></h2>
		<div class="graphContainer">
			<div class="dashboardGraph" ></div>
		</div>
	</div>
</div>
