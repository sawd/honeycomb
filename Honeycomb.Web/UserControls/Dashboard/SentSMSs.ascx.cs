﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Honeycomb.Web.UserControls.Dashboard {
	public partial class SentSMSs : System.Web.UI.UserControl {
		protected void Page_Load(object sender, EventArgs e) {
			List<ListItem> months = new List<ListItem>();

			for (int i = 0; i < 14; i++) {
				DateTime thisDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(i * -1);
				months.Add(new ListItem(thisDate.ToString("MMM yyyy"), thisDate.ToString("yyyy/MM/dd")));
			}
			
			SMSMonth.DataSource = months;
			SMSMonth.DataValueField = "Value";
			SMSMonth.DataTextField = "Text";
			SMSMonth.DataBind();
		}
	}
}