﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AccessCodesForEntranceFuture.ascx.cs" Inherits="Honeycomb.Web.UserControls.Dashboard.AccessCodesForEntranceFuture" %>
<script src="/usercontrols/dashboard/accesscodesforentrancefuture.ascx.js"></script>
<div id="accessCodesForEntranceFutureGraph" class="dashboardWidget accessCodesForEntranceFuture width2X">
	<div class="innerContainer">
		<h2>Access Codes Future<span class="graphButtons"><button type="button" class="stdbtn refresh"><span></span></button><button type="button" class="stdbtn year">Year</button><button type="button" class="stdbtn month">Month</button><button type="button" class="stdbtn week btn_orange">Week</button></span></h2>
		<div class="graphContainer">
			<div class="dashboardGraph" ></div>
		</div>
	</div>
</div>
