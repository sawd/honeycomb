﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PeopleTotalsTable.ascx.cs" Inherits="Honeycomb.Web.UserControls.Dashboard.PeopleTotalsTable" %>
<script src="/UserControls/Dashboard/PeopleTotalsTable.js"></script>
<div class="dashboardWidget peopleTotalsTable tableWidget">
	<div class="innerContainer">
		<h2>People Totals<span class="graphButtons"><button type="button" class="stdbtn refresh"><span></span></button></span></h2>
		<div class="graphContainer">
			<div class="dashboardGraph">
			</div>
		</div>
	</div>
</div>