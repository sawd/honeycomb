﻿/*global window,events,DashboardService,Utils,dateFormat,jQuery,Globals,countUp*/
var ExceptionAlertInduction = {
    init: function () {
        $('#exceptionalertinductionWidget').dashboardWidget({
            showRefresh: true,
            color: "#E3AB01",
            renderWidget: ExceptionAlertInduction.events.getData
        });
    },

    events: {
        render: function (data, container) {
            var options = {
                useEasing: true,
                useGrouping: true,
                separator: ',',
                decimal: '.',
                prefix: '',
                suffix: ''
            };

            var demo = new countUp("exceptionalertinductionWidgetVal", 0, data[0].data, 0, 2.5, options);
            demo.start();
        },
        getData: function (e, d) {
            d.container.block({ message: null });
            DashboardService.WCFDashboard.GetExceptionAlertInduction(ExceptionAlertInduction.events.getDataSuccess, Utils.responseFail, d.container);
        },
        getDataSuccess: function (result, context) {
            context.unblock();
            ExceptionAlertInduction.events.render(result.EntityList, context);
        },
    }
};

$(function () {
    ExceptionAlertInduction.init();
});

