﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CurrentProjectedFees.ascx.cs" Inherits="Honeycomb.Web.UserControls.Dashboard.CurrentProjectedFees" %>

<script src="/UserControls/Dashboard/CurrentProjectedFees.js"></script>
<div class="dashboardWidget currentProjectedFees tableWidget">
	<div class="innerContainer">
		<h2>Current projected fees<span class="graphButtons"><button type="button" class="stdbtn refresh"><span></span></button></span></h2>
		<div class="graphContainer">
			<div class="dashboardGraph">
				
			</div>
		</div>
	</div>
</div>