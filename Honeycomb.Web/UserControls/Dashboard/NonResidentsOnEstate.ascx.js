﻿/*global window,events,DashboardService,Utils,dateFormat,jQuery,Globals,countUp*/


var NonResidentsOnEstate = {
    init: function () {
        $('#nonresidentsWidget').dashboardWidget({
            showRefresh: true,
            color: "#109618",
            renderWidget: NonResidentsOnEstate.events.getData
        });
    },

    events: {
        render: function (data, container) {
            var options = {
                useEasing: true,
                useGrouping: true,
                separator: ',',
                decimal: '.',
                prefix: '',
                suffix: ''
            };

            var demo = new countUp("nonresidentsWidgetVal", 0, data[0].data, 0, 2.5, options);
            demo.start();
        },
        getData: function (e, d) {
            d.container.block({ message: null });
            DashboardService.WCFDashboard.GetNonResidentsOnEstate(NonResidentsOnEstate.events.getDataSuccess, Utils.responseFail, d.container);
        },
        getDataSuccess: function (result, context) {
            context.unblock();
            NonResidentsOnEstate.events.render(result.EntityList, context);
        },
    }
};

$(function () {
    NonResidentsOnEstate.init();
});

