﻿/*global window,events,DashboardService,Utils,dateFormat,jQuery,Globals,countUp*/
var ExceptionAlertGolfCarts = {
    init: function () {
        $('#exceptionalertgolfcartsWidget').dashboardWidget({
            showRefresh: true,
            color: "#ED1C2B",
            renderWidget: ExceptionAlertGolfCarts.events.getData
        });
    },

    events: {
        render: function (data, container) {
            var options = {
                useEasing: true,
                useGrouping: true,
                separator: ',',
                decimal: '.',
                prefix: '',
                suffix: ''
            };

            var demo = new countUp("exceptionalertgolfcartsWidgetVal", 0, data[0].data, 0, 2.5, options);
            demo.start();
        },
        getData: function (e, d) {
            d.container.block({ message: null });
            DashboardService.WCFDashboard.GetExceptionAlertGolfCarts(ExceptionAlertGolfCarts.events.getDataSuccess, Utils.responseFail, d.container);
        },
        getDataSuccess: function (result, context) {
            context.unblock();
            ExceptionAlertGolfCarts.events.render(result.EntityList, context);
        },
    }
};

$(function () {
    ExceptionAlertGolfCarts.init();
});

