﻿/*global window,events,DashboardService,Utils,dateFormat,jQuery,Globals,countUp*/
var NonResidentsOnEstateSupplier = {
    init: function () {
        $('#nonresidentsSupplierWidget').dashboardWidget({
            showRefresh: true,
            color: "#3366cc",
            renderWidget: NonResidentsOnEstateSupplier.events.getData
        });
    },

    events: {
        render: function (data, container) {
            var options = {
                useEasing: true,
                useGrouping: true,
                separator: ',',
                decimal: '.',
                prefix: '',
                suffix: ''
            };

            var demo = new countUp("nonresidentsSupplierWidgetVal", 0, data[1].data, 0, 2.5, options);
            demo.start();
        },
        getData: function (e, d) {
            d.container.block({ message: null });
            DashboardService.WCFDashboard.GetNonResidentsOnEstate(NonResidentsOnEstateSupplier.events.getDataSuccess, Utils.responseFail, d.container);
        },
        getDataSuccess: function (result, context) {
            context.unblock();
            NonResidentsOnEstateSupplier.events.render(result.EntityList, context);
        },
    }
};

$(function () {
    NonResidentsOnEstateSupplier.init();
});

