﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AccessCodeSources.ascx.cs" Inherits="Honeycomb.Web.UserControls.Dashboard.AccessCodeSources" %>
<script src="/UserControls/Dashboard/AccessCodeSources.js"></script>

<div id="accessCodeSourcesGraph" class="dashboardWidget width2X">
	<div class="innerContainer">
		<h2>Access Code Sources<span class="graphButtons"><button type="button" class="stdbtn refresh"><span></span></button><button type="button" class="stdbtn year">Year</button><button type="button" class="stdbtn month">Month</button><button type="button" class="stdbtn week btn_orange">Week</button></span></h2>
		<div class="graphContainer">
			<div class="dashboardGraph" ></div>
		</div>
	</div>
</div>