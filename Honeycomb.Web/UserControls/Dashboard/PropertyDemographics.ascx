﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PropertyDemographics.ascx.cs" Inherits="Honeycomb.Web.UserControls.Dashboard.PropertyDemographics" %>
<script src="/UserControls/Dashboard/PropertyDemographics.js"></script>
<div class="dashboardWidget propertyDemographics tableWidget">
	<div class="innerContainer">
		<h2>Estate Demographics<span class="graphButtons"><button type="button" class="stdbtn refresh"><span></span></button></span></h2>
		<div class="graphContainer">
			<div class="dashboardGraph">
				
			</div>
		</div>
	</div>
</div>