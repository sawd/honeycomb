﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ExceptionAlertGolfCarts.ascx.cs" Inherits="Honeycomb.Web.UserControls.Dashboard.ExceptionAlertGolfCarts" %>
<script src="/usercontrols/dashboard/exceptionalertgolfcarts.ascx.js"></script>
<div id="exceptionalertgolfcartsWidget" class="dashboardWidgets">
	<div class="innerContainer">
		<h1 id="exceptionalertgolfcartsWidgetVal">0</h1>
        <h2>Residents</h2>
        <p>Golf carts not insured</p>
	</div>
</div>