﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NonResidentsOnEstate.ascx.cs" Inherits="Honeycomb.Web.UserControls.Dashboard.NonResidentsOnEstate" %>
<script src="/usercontrols/dashboard/nonresidentsonestate.ascx.js"></script>
<div id="nonresidentsWidget" class="dashboardWidgets">
	<div class="innerContainer">
		<h1 id="nonresidentsWidgetVal">0</h1>
        <h2>Visitors</h2>
        <p>Currently on the estate</p>
	</div>
</div>