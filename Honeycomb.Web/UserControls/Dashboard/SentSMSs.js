﻿/*global DashboardService,Globals,Utils*/

var SentSMSs = {
	init: function () {

		$('#SentSMSsGraph').graph({
			currentType: $('#SMSMonth').val(),
			renderGraph: function (event, values) {
				var accessCodeSourceGraph = $(this);
				accessCodeSourceGraph.block();
				var mdate = new Date(values.type);
				DashboardService.WCFDashboard.GetSentSMSs(mdate, SentSMSs.events.getDataSuccess, Utils.responseFail, values);
			}
		});
	},
	events: {
		getDataSuccess: function (result, values) {

			var SentSMSsGraph = $('#SentSMSsGraph');
			var graphContainer = values.container;

			SentSMSsGraph.unblock();

			if (result.EntityList.length > 0) {

				graphContainer.html("");

				$.plot(graphContainer, result.EntityList, {
					series: {
						pie: {
							show: true,
							radius: 1,
							innerRadius: 0.4,
							label: {
								show: true,
								radius: 3 / 4,
								formatter: function (label, series) {
									return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + Math.round(series.percent) + "%</div>";
								}
							},
							combine: {
								threshold: 0.04,
								label: "Other",
								color: '#999'
							}
						}
					},
					legend: {
						show: true
					},
					grid: {
						hoverable: true
					},
					tooltip: true,
					tooltipOpts: {
						content: "%s, %p.0% (%y.0 SMSs)"
					},
					colors: Globals.graphColors()
				});

			} else {
				graphContainer.html('No Data to Chart');
				graphContainer.parent().find('.legend').html('');
			}

		}
	}
};

$(function () {
	SentSMSs.init();
});
