﻿/*global window,events,DashboardService,Utils,dateFormat,jQuery,Globals*/
(function ($, window, document, undefined) {
	var methods = {
		init: function () {
			return this.each(function () {
				$(this).graph({
					currentType: "month",
					renderGraph: methods.getIncidentData

				});
			});
		},
		render: function (data, container) {
			var type = container.data('dashboardIncidents');
			var groupedResult = Utils.groupByInArrayObject(data, "Status");
			var chartData = [];
			var categories = [];
			for (var e = 0; e < data.length; e++) {
				data[e].dayMonth = dateFormat(data[e].dayMonth, type === 'year' ? 'mmm<br/>yyyy' : 'dd<br/> mmm');
				if (categories.indexOf(data[e].dayMonth) === -1) {
					categories.push(data[e].dayMonth);
				}
			}
			//Group data into series by Status and build up the chart data object.
			//Each item in the arrays is a paired value of the date it occurs on and the value.
			for (var i = 0; i < groupedResult.length; i++) {
				var cat = groupedResult[i].Status;
				var series = [];
				for (var j = 0; j < groupedResult[i].Collection.length; j++) {
					var item = groupedResult[i].Collection[j];
					series.push([item.dayMonth, item.numberOfIncidents]);
				}
				chartData.push({
					label: cat,
					data: series
				});
			}
			if (data.length === 0) {
				container.html('No Incidents to Chart');
				container.parent().find('.legend').html('');
			} else {
				container.html('');
				$.plot(container,
					chartData,
					{
						series: {
							stack: true,
							lines: {
								show: false,
								fill: true,
								steps: false
							},
							bars: {
								show: true,
								barWidth: 0.6,
								align: 'center',
								zero: true,
								fill: 1
							},
						},
						xaxis: {
							mode: "categories",
							categories: categories
						},
						yaxis: {
							min: 0,
							minTickSize: 1,
							tickDecimals: 0
						},
						legend: {
							show: true,
							position: 'ne',
							container: container.parent().find('.legend')
						},
						grid: {
							hoverable: true,
							borderWidth: 0

						},
						tooltip: true,
						tooltipOpts: {
							content: "%s %y"
						},
						colors: Globals.graphColors()
					}
					);
			}
		},
		getIncidentData: function (event, result) {
			var placeholder = result.container;
			placeholder.parent().parent().block({ message: null });
			placeholder.data('dashboardIncidents', result.type);
			DashboardService.WCFDashboard.GetDashboardIncidents(result.type, null, events.getDataSuccess, Utils.responseFail, placeholder);
		}
	};

	var events = {
		getDataSuccess: function (result, context) {
			context.parent().parent().unblock();
			methods.render(result.EntityList, context);
		}
	};

	$.fn.CurrentIncidentGraph = function (method) {
		// Method calling logic
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if (typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist on jQuery.CurrentIncidentGraph');
		}
	};

	$(function () {
		$('.dashboardWidget.currentIncidents').CurrentIncidentGraph();
	});

})(jQuery, window, document);

