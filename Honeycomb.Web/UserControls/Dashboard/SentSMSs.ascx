﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SentSMSs.ascx.cs" Inherits="Honeycomb.Web.UserControls.Dashboard.SentSMSs" %>
<script src="/UserControls/Dashboard/SentSMSs.js"></script>

<div id="SentSMSsGraph" class="dashboardWidget width2X">
	<div class="innerContainer">
		<h2>Monthly Sent SMSs <span class="graphButtons"><asp:DropDownList ID="SMSMonth" CssClass="smsmonth" ClientIDMode="Static" runat="server"></asp:DropDownList></span></h2>
		<div class="graphContainer">
			<div class="dashboardGraph"></div>
		</div>
	</div>
</div>