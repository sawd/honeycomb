﻿/*global DashboardService,Globals,Utils*/

var IncidentCategoryCountsPie = {
	init: function () {

		$('#incidentCategoryCountsGraph').graph({
			renderGraph: function (event, values) {

				var incidentGraph = $(this);

				incidentGraph.block();
				DashboardService.WCFDashboard.GetIncidentCategoryCounts(values.type, IncidentCategoryCountsPie.events.getData_SUCCESS, Utils.responseFail, values);
			}
		});
	},
	events: {
		getData_SUCCESS: function (result, values) {

			var incidentGraph = $('#incidentCategoryCountsGraph');
			var graphContainer = values.container;

			incidentGraph.unblock();

			if (result.EntityList.length > 0) {

				graphContainer.html("");

				$.plot(graphContainer, result.EntityList, {
					series: {
						pie: {
							show: true,
							radius: 1,
							innerRadius: 0.4,
							label: {
								show: true,
								radius: 3 / 4,
								formatter: function (label, series) {
									return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + Math.round(series.percent) + "%</div>";
								}
							},
							combine: {
								threshold: 0.04,
								label: "Other",
								color: '#999'
							}
						}
					},
					legend: {
						show: true
					},
					grid: {
						hoverable: true
					},
					tooltip: true,
					tooltipOpts: {
						content: "%s, %p.0%"
					},
					colors: Globals.graphColors()
				});

			} else {
				graphContainer.html('No Data to Chart');
				graphContainer.parent().find('.legend').html('');
			}

		}
	}
};

$(function () {
	IncidentCategoryCountsPie.init();
});
