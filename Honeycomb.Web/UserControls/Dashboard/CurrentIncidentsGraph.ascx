﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CurrentIncidentsGraph.ascx.cs" Inherits="Honeycomb.Web.UserControls.Dashboard.CurrentIncidentsGraph" %>
<script src="/UserControls/Dashboard/CurrentIncidentsGraph.js"></script>
<div class="dashboardWidget currentIncidents width4X">
	<div class="innerContainer">
		<h2>Current Incidents <span class="graphButtons"><button type="button" class="stdbtn refresh"><span></span></button><button type="button" class="stdbtn year">Year</button><button type="button" class="stdbtn month">Month</button><button type="button" class="stdbtn week btn_orange">Week</button></span></h2>
		<div class="graphContainer">
			<div class="legend"></div>
			<div class="dashboardGraph"></div>
		</div>
	</div>
</div>