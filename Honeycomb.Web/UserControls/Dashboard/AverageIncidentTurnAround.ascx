﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AverageIncidentTurnAround.ascx.cs" Inherits="Honeycomb.Web.UserControls.Dashboard.AverageIncidentTurnAround" %>
<script src="/usercontrols/dashboard/averageincidentturnaround.ascx.js"></script>
<div id="averagetncidentGraph" class="dashboardWidget averageIncidentTurnaround width2X">
	<div class="innerContainer">
		<h2>Average Incident Turnaround<span class="graphButtons"><button type="button" class="stdbtn refresh"><span></span></button><button type="button" class="stdbtn year">Year</button><button type="button" class="stdbtn month">Month</button><button type="button" class="stdbtn week btn_orange">Week</button></span></h2>
		<div class="graphContainer">
			<div class="dashboardGraph" ></div>
		</div>
	</div>
</div>