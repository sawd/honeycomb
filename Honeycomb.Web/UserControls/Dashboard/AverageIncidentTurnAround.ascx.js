﻿/*global window,events,DashboardService,Utils,dateFormat,jQuery,Globals*/

var AverageIncidentTurnAroundGraph = {

	init: function () {
		$('#averagetncidentGraph').graph({
			currentType: "month",
			renderGraph: AverageIncidentTurnAroundGraph.events.getData
		});
	},

	events: {
		render: function (data, container) {
			var type = container.data('dashboardData');
			var chartData = [{ label: "Hours", data: [] }];

			if (data.length > 1) {
				chartData = [];
				for (var i = 0; i < data.length; i++) {
					chartData.push({ label: data[i].label, data: [["" + data[i].label + "", data[i].data]] });
				}
			}

			if (data.length === 0) {
				container.html('No Data to Chart');
				container.parent().find('.legend').html('');
			} else {
				container.html('');

				$.plot(container, chartData, {
					series: {
						bars: {
							show: true,
							barWidth: 0.6,
							align: "center",
							fill: 1
						}
					},
					xaxis: {
						mode: "categories",
						tickLength: 0,
						show: false
					},
					grid: {
						hoverable: true,
						borderWidth: 0
					},
					tooltip: true,
					tooltipOpts: {
						content: "%s %y h"
					},
					legend: {
						show: true,
						container: '#alegend'
					},
					colors: Globals.graphColors()
				});

			}
		},
		getData: function (e, d) {
			d.container.parent().parent().block({ message: null });
			DashboardService.WCFDashboard.GetAverageIncidentTurnAround(d.type, AverageIncidentTurnAroundGraph.events.getDataSuccess, Utils.responseFail, d.container);
		},
		getDataSuccess: function (result, context) {
			context.parent().parent().unblock();
			AverageIncidentTurnAroundGraph.events.render(result.EntityList, context);
		}
	}
};

$(function () {
	AverageIncidentTurnAroundGraph.init();
});

