﻿/*global DashboardService,Globals,Utils*/

var AccessCodeSources = {
	init: function () {

		$('#accessCodeSourcesGraph').graph({
			renderGraph: function (event, values) {

				var accessCodeSourceGraph = $(this);

				accessCodeSourceGraph.block();
				DashboardService.WCFDashboard.GetAccessCodeSource(values.type, AccessCodeSources.events.getDataSuccess, Utils.responseFail, values);
			}
		});
	},
	events: {
		getDataSuccess: function (result, values) {

			var accessCodeSourcesGraph = $('#accessCodeSourcesGraph');
			var graphContainer = values.container;

			accessCodeSourcesGraph.unblock();

			if (result.EntityList.length > 0) {

				graphContainer.html("");

				$.plot(graphContainer, result.EntityList, {
					series: {
						pie: {
							show: true,
							radius: 1,
							innerRadius: 0.4,
							label: {
								show: true,
								radius: 3 / 4,
								formatter: function (label, series) {
									return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + Math.round(series.percent) + "%</div>";
								}
							},
							combine: {
								threshold: 0.04,
								label: "Other",
								color: '#999'
							}
						}
					},
					legend: {
						show: true
					},
					grid: {
						hoverable: true
					},
					tooltip: true,
					tooltipOpts: {
						content: "%s, %p.0%"
					},
					colors: Globals.graphColors()
				});

			} else {
				graphContainer.html('No Data to Chart');
				graphContainer.parent().find('.legend').html('');
			}

		}
	}
};

$(function () {
	AccessCodeSources.init();
});
