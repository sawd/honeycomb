﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NonResidentsOnEstateSupplier.ascx.cs" Inherits="Honeycomb.Web.UserControls.Dashboard.NonResidentsOnEstateSupplier" %>
<script src="/usercontrols/dashboard/nonresidentsonestatesupplier.ascx.js"></script>
<div id="nonresidentsSupplierWidget" class="dashboardWidgets">
	<div class="innerContainer">
		<h1 id="nonresidentsSupplierWidgetVal">0</h1>
        <h2>Suppliers</h2>
        <p>Currently on the estate</p>
	</div>
</div>