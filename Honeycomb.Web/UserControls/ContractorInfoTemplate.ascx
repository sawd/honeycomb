﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContractorInfoTemplate.ascx.cs" Inherits="Honeycomb.Web.UserControls.ContractorInfoTemplate" %>
<div class="stdform">
    <input type="hidden" name="ContractorID" value="${ContractorID}" />

    <div class="contentBlock" style="margin-top:10px;">
        <div class="contenttitle2"><h3>Contractor Categories</h3></div>
        <table>
            <tr>
                <td><select name="ContractorMainCategory" data-placeholder="Choose a Category..." class="chzn-select" style="width:250px;" tabindex="2"></select></td><td width="10">&nbsp;</td>
                <td><select name="ContractorSubCategory" data-placeholder="Choose a sub category..." class="chzn-select" valtype="required;" multiple="multiple" style="width:400px;" tabindex="4"></select></td>
            </tr>
        </table>
    </div>
    <div class="contentBlock" style="margin-top:10px;">
            <div class="contenttitle2"><h3>Company Registration Details</h3></div>
            <div style="clear:both"></div>
            <div class="one_half nomargin">
                <p>
                <label>Trading Name</label>
                <span class="field">
                    <input type="text" class="" valtype="required" name="TradingName" value="${TradingName}">
                </span>
            </p>
                <p>
                    <label>Business Type</label>
                    <span class="field">
                        <input type="text" class="" name="BusinessType" value="${BusinessType}">
                    </span>
                </p>
                <p>
                    <label>Registration No</label>
                    <span class="field">
                        <input type="text" class="" name="RegistrationNo" value="${RegistrationNo}">
                    </span>
                </p>
                <p>
                    <label>Telephone No1</label>
                    <span class="field">
                        <input type="text" class="" name="TelephoneNo1" value="${TelephoneNo1}">
                    </span>
                </p>
                <p>
                    <label>Fax Number</label>
                    <span class="field">
                        <input type="text" class="" name="FaxNo" value="${FaxNo}">
                    </span>
                </p>
                <p>
                    <label>Docex Number</label>
                    <span class="field">
                        <input type="text" class="" name="DocexNo" value="${DocexNo}">
                    </span>
                </p>
               <%--  <p>
                    <label>Upload Logo</label>
                    <span class="field">
                    
                    </span>
                </p>--%>
            </div>
            <div class="one_half nomargin">
                <p>
                    <label>Registered Name</label>
                    <span class="field">
                        <input type="text" class="" name="RegisteredName" value="${RegisteredName}">
                    </span>
                </p>
                <p>
                    <label>Industry Type</label>
                    <span class="field">
                        <input type="text" class="" name="IndustryType" value="${IndustryType}">
                    </span>
                </p>
                <p>
                    <label>VAT Number</label>
                    <span class="field">
                        <input type="text" class="" name="VatNumber" value="${VatNumber}">
                    </span>
                </p>
                 <p>
                    <label>Telephone No 2</label>
                    <span class="field">
                        <input type="text" class="" name="TelephoneNo2" value="${TelephoneNo2}">
                    </span>
                </p>
                <p>
                    <label>Account Code</label>
                    <span class="field">
                        <input type="text" class="" name="AccountCode" value="${AccountCode}">
                    </span>
                </p>
                <p>
                    <label>Docex Town</label>
                    <span class="field">
                        <input type="text" class="" name="DocexTown" value="${DocexTown}">
                    </span>
                </p>
            </div>
        <div style="clear:both"></div>
    </div>

    <div class="contentBlock" style="margin-top:10px;">
            <div class="contenttitle2"><h3>Address Details</h3></div>
            <div style="clear:both"></div>
            <div class="one_half nomargin">
                <p>Physical Address</p>
                <p>
                <label>Street Number</label>
                <span class="field">
                    <input type="text" class="" name="PhysStreetNo" value="${PhysStreetNo}">
                </span>
            </p>
                <p>
                    <label>Street Name</label>
                    <span class="field">
                        <input type="text" class="" name="PhysStreetName" value="${PhysStreetName}">
                    </span>
                </p>
                <p>
                    <label>Province</label>
                    <span class="field">
                        <input type="text" class="" name="PhysProvince" value="${PhysProvince}">
                    </span>
                </p>
                <p>
                    <label>Town</label>
                    <span class="field">
                        <input type="text" class="" name="PhysTown" value="${PhysTown}">
                    </span>
                </p>
                <p>
                    <label>Suburb</label>
                    <span class="field">
                        <input type="text" class="" name="PhysSuburb" value="${PhysSuburb}">
                    </span>
                </p>
                <p>
                    <label>Postal Code</label>
                    <span class="field">
                        <input type="text" class="" name="PhysPostalCode" value="${PhysPostalCode}">
                    </span>
                </p>
            </div>
            <div class="one_half nomargin">
                 <p>Postal Address</p>
                <p>
                <label>Box number</label>
                <span class="field">
                    <input type="text" class="" name="PostalBoxNo" value="${PostalBoxNo}">
                </span>
            </p>
                <p>
                    <label>Street Name</label>
                    <span class="field">
                        <input type="text" class="" name="PostalStreetName" value="${PostalStreetName}">
                    </span>
                </p>
                <p>
                    <label>Province</label>
                    <span class="field">
                        <input type="text" class="" name="PostalProvince" value="${PostalProvince}">
                    </span>
                </p>
                <p>
                    <label>Town</label>
                    <span class="field">
                        <input type="text" class="" name="PostalTown" value="${PostalTown}">
                    </span>
                </p>
                <p>
                    <label>Suburb</label>
                    <span class="field">
                        <input type="text" class="" name="PostalSuburb" value="${PostalSuburb}">
                    </span>
                </p>
                <p>
                    <label>Postal Code</label>
                    <span class="field">
                        <input type="text" class="" name="PostalCode" value="${PostalCode}">
                    </span>
                </p>
            </div>
        <div style="clear:both"></div>
    </div>
</div>