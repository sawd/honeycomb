﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContractorContactTemplate.ascx.cs" Inherits="Honeycomb.Web.UserControls.ContractorContactTemplate" %>
<div class="stdform">
    <div class="contentBlock primaryContact" style="margin-top:10px;">
            <input type="hidden" name="Type" value="O" />
            <input type="hidden" name="ContactPersonID" value="0" />
            <div class="contenttitle2"><h3>Owner Contact Person</h3></div>
            <div style="clear:both"></div>
            <div class="one_half nomargin">
                <p>
                <label>Title</label>
                <span class="field">
                    <input type="text" class="" name="Title" value="${Title}">
                </span>
            </p>
                <p>
                    <label>First Name</label>
                    <span class="field">
                        <input type="text" class="" valtype="required" name="FirstName" value="${FirstName}">
                    </span>
                </p>
                <p>
                    <label>I.D. No</label>
                    <span class="field">
                        <input type="text" class="" name="IDNo" value="${IDNo}">
                    </span>
                </p>
                <p>
                    <label>Date of Birth</label>
                    <span class="field">
                        <input type="text" class="isDatePicker" name="DateOfBirth" value="${DateOfBirth}">
                    </span>
                </p>
                <p>
                    <label>Mobile Number</label>
                    <span class="field">
                        <input type="text" class="" name="MobileNo"  value="${MobileNo}">
                    </span>
                </p>
            </div>
            <div class="one_half nomargin">
                <p>
                    <label>Job Title</label>
                    <span class="field">
                        <select name="JobTitleID" class="uniformselect"></select>
                    </span>
                </p>
                <p>
                    <label>Last Name</label>
                    <span class="field">
                        <input type="text" class="" name="LastName" valtype="required" value="${LastName}">
                    </span>
                </p>
                <p>
                    <label>WorkNo</label>
                    <span class="field">
                        <input type="text" class="" name="WorkNo" valtype="required" value="${WorkNo}">
                    </span>
                </p>
                 <p>
                    <label>Email</label>
                    <span class="field">
                        <input type="text" class="" name="Email" valtype="required" value="${Email}">
                    </span>
                </p>
            </div>
        <div style="clear:both"></div>
    </div>

    <div class="contentBlock secondaryContact" style="margin-top:10px;">
        <input type="hidden" name="Type" value="P" />
        <input type="hidden" name="ContactPersonID" value="0" />
            <div class="contenttitle2"><h3>Primary Contact Person</h3></div>
            <div style="clear:both"></div>
            <div class="one_half nomargin">
                <p>
                <label>Title</label>
                <span class="field">
                    <input type="text" class="" name="Title" value="${Title}">
                </span>
            </p>
                <p>
                    <label>First Name</label>
                    <span class="field">
                        <input type="text" class="" name="FirstName" value="${FirstName}">
                    </span>
                </p>
                <p>
                    <label>I.D. No</label>
                    <span class="field">
                        <input type="text" class="" name="IDNo" value="${IDNo}">
                    </span>
                </p>
                <p>
                    <label>Date of Birth</label>
                    <span class="field">
                        <input type="text" class="isDatePicker" name="DateOfBirth" value="${DateOfBirth}">
                    </span>
                </p>
                <p>
                    <label>Mobile Number</label>
                    <span class="field">
                        <input type="text" class="" name="MobileNo" value="${MobileNo}">
                    </span>
                </p>
            </div>
            <div class="one_half nomargin">
                <p>
                    <label>Job Title</label>
                    <span class="field">
                        <select name="JobTitleID" class="uniformselect"></select>
                    </span>
                </p>
                <p>
                    <label>Last Name</label>
                    <span class="field">
                        <input type="text" class="" name="LastName" value="${LastName}">
                    </span>
                </p>
                <p>
                    <label>WorkNo</label>
                    <span class="field">
                        <input type="text" class="" name="WorkNo" value="${WorkNo}">
                    </span>
                </p>
                 <p>
                    <label>Email</label>
                    <span class="field">
                        <input type="text" class="" name="Email" value="${Email}">
                    </span>
                </p>
            </div>
        <div style="clear:both"></div>
    </div>

    <div class="contentBlock financeContact" style="margin-top:10px;">
        <input type="hidden" name="Type" value="A" />
        <input type="hidden" name="ContactPersonID" value="0" />
            <div class="contenttitle2"><h3>Admin Contact Person</h3></div>
            <div style="clear:both"></div>
            <div class="one_half nomargin">
                <p>
                <label>Title</label>
                <span class="field">
                    <input type="text" class="" name="Title" value="${Title}">
                </span>
            </p>
                <p>
                    <label>First Name</label>
                    <span class="field">
                        <input type="text" class="" name="FirstName" value="${FirstName}">
                    </span>
                </p>
                <p>
                    <label>I.D. No</label>
                    <span class="field">
                        <input type="text" class="" name="IDNo" value="${IDNo}">
                    </span>
                </p>
                <p>
                    <label>Date of Birth</label>
                    <span class="field">
                        <input type="text" class="isDatePicker" name="DateOfBirth" value="${DateOfBirth}">
                    </span>
                </p>
                <p>
                    <label>Mobile Number</label>
                    <span class="field">
                        <input type="text" class="" name="MobileNo" value="${MobileNo}">
                    </span>
                </p>
            </div>
            <div class="one_half nomargin">
                <p>
                    <label>Job Title</label>
                    <span class="field">
                        <select name="JobTitleID" class="uniformselect"></select>
                    </span>
                </p>
                <p>
                    <label>Last Name</label>
                    <span class="field">
                        <input type="text" class="" name="LastName" value="${LastName}">
                    </span>
                </p>
                <p>
                    <label>WorkNo</label>
                    <span class="field">
                        <input type="text" class="" name="WorkNo" value="${WorkNo}">
                    </span>
                </p>
                 <p>
                    <label>Email</label>
                    <span class="field">
                        <input type="text" class="" name="Email" value="${Email}">
                    </span>
                </p>
            </div>
        <div style="clear:both"></div>
    </div>
</div>