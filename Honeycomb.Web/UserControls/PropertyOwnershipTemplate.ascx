﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PropertyOwnershipTemplate.ascx.cs" Inherits="Honeycomb.Web.UserControls.PropertyOwnershipTemplate" %>
<div>
<div class="one_half nomargin">
    <div class="contentBlock" style="margin-bottom:10px;">
    <div class="contenttitle2">
        <h3>Ownership Details</h3>
        <input type="hidden" name="ID" value="0" />
        <input type="hidden" name="PropertyID" value="0" />
        <input type="hidden" name="SaleDocFileID" value="${SaleDocFileID}" />
    </div>
    <div class="stdform">
        <p>
            <label>Property Name:</label>
            <span class="field">
                <input type="text" name="PropertyName" valtype="required;" value="${Ownership.PropertyName}"/>
            </span>
        </p>
        <p>
            <label>Owner Type:</label>
            <span class="field">
                <select name="PersonTypeID" class="uniformselect">
                
                </select>
            </span>
        </p>
        <p class="companyfields">
            <label>Company Name:</label>
            <span class="field">
                <input type="text" name="CompanyName"  value="${Ownership.CompanyName}"/>
            </span>
        </p>
        <p class="trustfields">
            <label>Trust No:</label>
            <span class="field">
                <input type="text" name="TrustNumber" value="${Ownership.TrustNumber}"/>
            </span>
        </p>
        <p>
            <label>Principal Owner Name:</label>
            <span class="field">
                <input type="text" name="PrincipalOwnerName" value="${Ownership.PrincipalOwnerName}" valtype="required;"/>
            </span>
        </p>
        <p>
            <label>ID/Reg Number:</label>
            <span class="field">
                <input type="text" name="GovID" value="${Ownership.GovID}"/>
            </span>
        </p>
        <p>
            <label>Account Number:</label>
            <span class="field">
                <input type="text" name="AccountNumber" value="${Ownership.AccountNumber}"/>
            </span>
        </p>
        <p>
            <label>Telephone Number:</label>
            <span class="field">
                <input type="text" name="Phone" value="${Ownership.Phone}"/>
            </span>
        </p>
        <p>
            <label>Fax Number:</label>
            <span class="field">
                <input type="text" name="Fax" value="${Ownership.Fax}"/>
            </span>
        </p>
        <p>
            <label>Mobile Number:</label>
            <span class="field">
                <input type="text" name="Cell" value="${Ownership.Cell}" valtype="required;"/>
            </span>
        </p>
        <p>
            <label>Email Address:</label>
            <span class="field">
                <input type="text" name="Email" value="${Ownership.Email}" valtype="required;regex:email"/>
            </span>
        </p>
    </div>
    </div>
     <div class="contentBlock" style="margin-top:10px;">

        <div class="contenttitle2">
            <h3>Sale Agreement Document</h3>
        </div>
         <div class="stdform">
             <p>
                <label>Copy of sale agreement</label>
                <span class="field" id="salesDocUpload"><a href="javascript:void(0);" class="btn btn_document" id="saleDocUploadButton"><span>Upload File</span></a>&nbsp;&nbsp;<a id="CurrentFile" href="">Download Current</a></span>
            </p>
         </div>

    <div class="contenttitle2">
        <h3>Fees</h3>
    </div>
    <div class="stdform">
        <p>
            <label>Levy Stabilisation Fund:</label>
            <span class="field">
                <input type="text" style="width:110px;" name="LevyStabilisationFund" value="${Ownership.LevyStabilisationFund}"/>&nbsp;&nbsp;<input name="LevyStabilisationFundDate" type="text" class="isDatePicker"  />
            </span>
        </p>
         <p>
            <label>Club Entrance Fee:</label>
            <span class="field">
                <input type="text" style="width:110px;" name="ClubEntranceFee" value="${Ownership.ClubEntranceFee}"/>&nbsp;&nbsp;<input name="ClubEntranceFeeDate" type="text" class="isDatePicker"  />
            </span>
        </p>
        <p>
            <label>Marketing Fee:</label>
            <span class="field">
                <input type="text" style="width:110px;" name="MarketingFee" value="${Ownership.MarketingFee}"/>&nbsp;&nbsp;<input name="MarketingFeeDate" type="text" class="isDatePicker"  />
            </span>
        </p>
        <p style="margin-top:20px;">
            <label>Residential Golf Subs (R):</label>
            <span class="field">
                <input name="ResidentialGolfSubs" type="text" style="width:110px;" readonly="readonly" value=""/>
            </span>
        </p>
        <p>
            <label>Estate Levy (R):</label>
            <span class="field">
                <input type="text" name="EstateLevy" style="width:110px;" readonly="readonly" value=""/>&nbsp;&nbsp;x&nbsp;&nbsp;<input type="text" class="numericOnly width50 noradiusright" name="EstateLevyMultiplyer" />
            </span>
        </p>
		<p>
            <label>Land Maintenance Levy (R):</label>
            <span class="field">
                <input type="text" name="LandMaintenanceLevy" style="width:110px;" readonly="readonly" value=""/>&nbsp;&nbsp;x&nbsp;&nbsp;<input type="text" class="numericOnly width50 noradiusright" name="LandMaintenanceLevyMultiplyer" />
            </span>
        </p>
         <p>
            <label>Club Family Membership (R):</label>
            <span class="field">
                <input type="text" name="ClubMembership" style="width:110px;" readonly="readonly" value=""/>&nbsp;&nbsp;x&nbsp;&nbsp;<input type="text" class="numericOnly width50 noradiusright" name="ClubMembershipMultiplyer" />
            </span>
        </p>
    </div>
        </div>

</div>
<div class="one_half nomargin">
    <div class="contentBlock" style="margin-left:10px;">
        <div class="contenttitle2">
            <h3>Ownership Status</h3>
        </div>
         <div class="stdform">
            <p>
                <label>Property Status:</label>
                <select name="PropertyStatusID" class="uniformselect" value="selected">                
                </select>
            </p>
       </div>
        </div>

    <div class="contentBlock" style="margin-left:10px;margin-top:10px;height:1068px;">
        <div class="contenttitle2">
            <h3>Sale Procedure</h3>
        </div>
        <div class="stdform">
            <p>
                <label>Sales Agent:</label>
                <select name="AgencyID" class="uniformselect">
                
                </select>
            </p>
            <p>
            <label>Attorney:</label>
            <span class="field">
                <input type="text"  name="Attorney" value="${Ownership.Attorney}"/>
            </span>
        </p>
             <p>
            <label>Agreement Draft No.</label>
            <span class="field">
                <input type="text"  name="AgreementDraftNumber" value="${Ownership.AgreementDraftNumber}"/>
            </span>
        </p>

            <p>
            <label>Agreement Received:</label>
            <span class="field">
                <input name="AgreementReceived" type="text" class="isDatePicker"  />
            </span>
        </p>
            <p>
            <label>Purchase Price:</label>
            <span class="field">
                <input type="text"  name="PurchasePrice" value="${Ownership.PurchasePrice}"/>
            </span>
        </p>
             <p>
            <label>Title Deed:</label>
            <span class="field">
                <input type="text"  name="TitleDeedNumber" value="${Ownership.TitleDeedNumber}"/>
            </span>
        </p>
            <p>
            <label>LCC Issued:</label>
            <span class="field">
                <input name="LCCIssued" type="text" class="isDatePicker"  />
            </span>
        </p>
            <p>
            <label>Undertaking Received:</label>
            <span class="field">
                <input name="UndertakingReceived" type="text" class="isDatePicker"  />
            </span>
        </p>
              <p>
            <label>Transfer Documents Signed:</label>
            <span class="field">
                <input name="TransferDocumentsSigned" type="text" class="isDatePicker"  />
            </span>
        </p>
              <p>
            <label>Debit order form received:</label>
            <span class="field">
                <input name="DebitOrderFormReceived" type="text" class="isDatePicker"  />
            </span>
        </p>
               <p>
            <label>Fica Received:</label>
            <span class="field">
                <input name="FicaReceived" type="text" class="isDatePicker"  />
            </span>
        </p>
        <p>
            <label>Purchase Date:</label>
            <span class="field">
                <input name="PurchaseDate" type="text" class="isDatePicker"  />
            </span>
        </p>
            <%--  <p>
            <label>Registration Date:</label>
            <span class="field">
                <input name="RegistrationDate" type="text" class="isDatePicker"  />
            </span>
        </p>
            
            <p>
            <label>Occupation Date:</label>
            <span class="field">
                <input name="OccupationDate" type="text" class="isDatePicker"  />
            </span>
        </p>
            <p>
            <label>Date of Sale:</label>
            <span class="field">
                <input name="DateOfSale" type="text" class="isDatePicker"  />
            </span>
        </p>--%>
             <p>
            <label>Date of transfer:</label>
            <span class="field">
                <input name="DateOfTransfer" type="text" class="isDatePicker"  />
            </span>
        </p>
            <p><a href="javascript:void(0);" class="btn btn_trash transferownership"><span>Transer Ownership</span></a></p>
       </div>
       </div>

</div>
<div class="clearall"></div>
</div>
<div>
    <div class="one_half nomargin">

       

        
    </div>
    <div class="one_half nomargin">
        <%-- bottom right --%>
    </div>
<div class="clearall"></div>
</div>
<div>
<div class="contenttitle2">
    <h3>Transfer History</h3>
</div>
<%-- ownership history container --%>
<div id="OwnershipHistoryContainer" class="accordion"></div> 
<!-- accordion -->
</div>