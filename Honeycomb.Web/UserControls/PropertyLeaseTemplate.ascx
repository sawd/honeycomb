﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PropertyLeaseTemplate.ascx.cs" Inherits="Honeycomb.Web.UserControls.PropertyLeaseTemplate" %>

<div class="stdform clearMe">
    <div class="one_half nomargin contentBlock">
        <div class="contenttitle2">
            <h3>Lease Information</h3>
            <input type="hidden" name="FileID" />
            <input type="hidden" name="PropertyID" />
        </div>
        <p>
            <label>Agency</label>
            <span class="field">
                <select name="SalesAgent" valtype="required;" class="uniformselect">
                </select>
            </span>
        </p>
        <p>
            <label>Agent</label>
            <span class="field">
                <select name="AgentID" valtype="required;" class="uniformselect">
                </select>
            </span>
        </p>
		<p>
            <label>Lease Holder</label>
            <span class="field">
                <select name="LeaseOccupantID" valtype="required;" class="uniformselect">
                </select>
            </span>
        </p>
        <p>
            <label>Start Date</label>
            <span class="field"><input type="text" name="StartDate" valtype="required;" class="isDatePicker smallinput"></span>
        </p>
            <p>
            <label>End Date</label>
            <span class="field"><input type="text" name="EndDate" valtype="required;" class="isDatePicker smallinput"></span>
        </p>
        <p>
            <label>Monthly Rental</label>
            <span class="field"><input type="text" name="MonthlyRental" valtype="required;" class="smallinput"></span>
        </p>
        <p>
            <label>Number of Occupants</label>
            <span class="field"><input type="text" name="NumberOfOccupants" valtype="required;" class="smallinput"></span>
        </p>
        <p>
            <label>Date induction attended</label>
            <span class="field"><input type="text" name="DateOfInduction" class="isDatePicker smallinput"></span>
        </p>
        <p>
            <label>Induction done by</label>
            <span class="field"><input type="text" name="InductionBy" class="smallinput"></span>
        </p>
        <p>
            <label>Copy of original lease on record</label>
            <span class="field" id="leaseUpload"><a href="javascript:void(0);" class="btn btn_document" id="leaseUploadButton"><span>Upload File</span></a>&nbsp;&nbsp;<a id="CurrentFile" href="javascript:void(0);">Download Current</a></span>
        </p>
    </div>
    <div class="one_half nomargin">
        <div class="contentBlock" style="min-height:620px;margin-left:10px;">
            <div class="contenttitle2">
                <h3>Lease Termination</h3>
            </div>
            <div class="terminated">
                <table>
                    <tr>
                        <td>Terminated:</td>
                        <td width="20"></td>
                        <td>
                            <input name="Terminated" type="checkbox" value="true" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" height="10"></td>
                    </tr>
                    <tr>
                        <td>Termination Date:</td>
                        <td width="20"></td>
                        <td><input type="text" readonly="readonly" class="isDatePicker" name="TerminationDate" /></td>
                    </tr>
                    <tr>
                        <td colspan="3" height="10"></td>
                    </tr>
                    <tr>
                        <td colspan="3">Termination Reason</td>
                    </tr>
                    <tr>
                        <td colspan="3"><textarea style="width:100%;" name="TerminationReason" readonly="readonly" cols="20" rows="6"></textarea></td>
                    </tr>
                </table>
            </div>
             <p><a href="javascript:void(0);" class="btn btn_document terminateLeaseButton"><span>Terminate Lease &amp; Disable All Tenant Access to Estate</span></a></p>
            <div class="contenttitle2">
                <h3>Lease Extensions</h3>
            </div>
            <p><a href="javascript:void(0);" class="btn btn_document" id="extendLeaseAddButton"><span>Extend Lease</span></a></p>
            
               <table cellpadding="0" cellspacing="0" border="0" class="stdtable">
                    <colgroup>
                        <col class="con0">
                        <col class="con1">
                        <col class="con0">
                    </colgroup>
                    <thead>
                        <tr>
                            <th class="head0">Extension Date</th>
                            <th class="head1">Document</th>
                            <th class="head0">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody class="leaseExtentions">
                    </tbody>
                </table>
           
        </div>
    </div>
</div>