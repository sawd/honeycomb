﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PropertyPersonal.ascx.cs" Inherits="Honeycomb.Web.UserControls.PropertyPersonal" %>
<style type="text/css">
    .personaltab .ui-accordion-content {
        background-color:#EFEFEF;
    }
</style>
<div class="stdform">
    <div class="clearMe">
        <div class="one_half nomargin">
            <div class="contentBlock">
                <div class="contenttitle2">
                    <h3>Pets</h3>
                </div>
                <p>Add new pet &nbsp;&nbsp;<%--<a href="javascript:void(0);" class="btn  btn_pencil radius50 addPet"><span>Add Pet</span></a>--%><button type="button" class="addPet stdbtn btn_note">Add Pet</button></p>
                <div class="petAccordion accordion"></div>
            </div>
            <div class="contentBlock" style="margin-top:10px;">
                <div class="contenttitle2">
                    <h3>Vehicles</h3>
                </div>
                <p>Add new vehicle &nbsp;&nbsp;<button type="button" class="addVehicle stdbtn">Add Vehicle</button></p>
                <div class="vehicleAccordion accordion"></div>
            </div>
        </div>
        <div class="one_half nomargin" style="margin-left:10px;">
            <div class="contentBlock">
                <div class="contenttitle2">
                    <h3>Golf Carts</h3>
                </div>
                <p>Add new Golf Cart &nbsp;&nbsp;<button type="button" class="addGolfCart stdbtn">Add Golf Cart</button></p>
                <div class="golfCartAccordion accordion"></div>
            </div>
        </div>
    </div>
    <%--<div class="clearMe" style="margin-top:10px;">
        <div class="one_half nomargin">
            
        </div>
        <div class="one_half nomargin" style="margin-left:10px;">
            <div class="contentBlock">
                <div class="contenttitle2">
                    <h3>Communication History</h3>                    
                </div>
                <div class="commHistoryAccordion accordion"></div>
            </div>
        </div>
    </div>--%>
</div>