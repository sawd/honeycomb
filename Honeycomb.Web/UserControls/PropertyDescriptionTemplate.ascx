﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PropertyDescriptionTemplate.ascx.cs" Inherits="Honeycomb.Web.UserControls.PropertyDescriptionTemplate" %>
<div class="stdform">
    <div>
       <%-- <p>
            <label>Property Type</label>
            <span class="field" name="PropertyType">
            </span>
        </p>--%>
        <div class="one_half nomargin">
            <div class="contentBlock" style="margin-bottom:10px;">
            <div class="contenttitle2">
                <h3>Property Description</h3>
                <input type="hidden" name="ID" value="0" />
                <input type="hidden" name="ERFID" />
                <input type="hidden" name="PropertyTypeID" />
                <%--<input type="hidden" name="SchemeID" />--%>
            </div>
            <p>
                <label>Erf Number</label>
                <span class="field" style="height: 28px;padding-top: 5px;" name="ErfNumber">
                </span>
            </p>
            <div class="SectionalTitle">
                <p>
                    <label>Scheme Name</label>
                    <select name="SchemeID" valtype="required;" class="uniformselect">
                    </select>
                </p>
                <p>
                    <label>Scheme Number</label>
                    <span class="field" name="SchemeNumber">
                    </span>
                </p>
                <p>
                    <label>Section Number</label>
                    <span class="field">
                        <input type="text" name="SectionNumber" valtype="required;" />
                    </span>
                </p>
            </div>
            <p>
                <label>Extent</label>
                <span class="field">
                    <input type="text" name="Extent" />
                </span>
            </p>
            <p>
                <label>Township</label>
                <span class="field">
                    <input type="text" name="Township" />
                </span>
            </p>
            <p>
                <label>Registration Division</label>
                <span class="field">
                    <input type="text" name="RegistrationDivision" />
                </span>
            </p>
            <p>
                <label>Phase:</label>
                <select name="PhaseID" class="uniformselect" value="selected">                
                </select>
                <!--<label>Phase</label>    *** EDITS ***
                <span class="field">
                    <input type="text" name="Phase"  />
                </span> -->
            </p>
            </div>
        </div>
        <div class="one_half nomargin">
            <div class="contentBlock" style="margin-left:10px;height: 352px;">
            <div class="contenttitle2">
                <h3>Physical Address</h3>
            </div>
            <div class="SectionalTitle">
                <p>
                    <label>Door Number</label>
                    <span class="field">
                        <input type="text" name="DoorNumber" value="${Property.DoorNumber}" />
                    </span>
                </p>
            </div>
            <p>
                <label>Street Number</label>
                <span class="field">
                    <input type="text" name="StreetNumber" value="${Property.StreetNumber}" />
                </span>
            </p>
            <p>
                <label>Street Name</label>
                <span class="field">
                    <input type="text" name="StreetName" value="${Property.StreetName}" />
                </span>
            </p>
            <p>
                <label>Google Maps Link</label>
                <span class="field">
                    <input type="text" name="GoogleMapsUrl" value="${Property.GoogleMapsUrl}" />
                </span>
            </p>
                </div>
        </div>
        <div style="clear:both"></div>
    </div>
    <div>
        <div class="one_half nomargin">
            <div class="contentBlock">
            <div class="contenttitle2">
                <h3>Building Description</h3>
            </div>
            <p>
                <label>Lot m<sup>2</sup></label>
                <span class="field">
                    <input type="text" name="LotMeterage" value="${Property.LotMeterage}" />
                </span>
            </p>
            <p>
                <label>Building m<sup>2</sup></label>
                <span class="field">
                    <input type="text" name="BuildingMeterage" value="${Property.BuildingMeterage}" valtype="required; regex:double" />
                </span>
            </p>
            <%--<p>
                <label>FAR(%)</label>
                <span class="field">
                    <input type="text" name="FAR" value="${Property.FAR}" />
                </span>
            </p>--%>
			<p>
                <label>No of Rooms</label>
                <span class="field">
                    <input type="text" name="NoOfRooms" value="${Property.NoOfRooms}" />
                </span>
            </p>
            <div class="contenttitle2">
                <h3>Utilities</h3>
            </div>
            <p>
                <label>Water Meter Number</label>
                <span class="field">
                    <input type="text" name="WaterMeterNumber" value="${Property.WaterMeterNumber}" />
                </span>
            </p>
            <p>
                <label>Electricity Meter Number</label>
                <span class="field">
                    <input type="text" name="ElectricityMeterNumber" value="${Property.ElectricityMeterNumber}" />
                </span>
            </p>
            </div>
        </div>
        <div class="one_half nomargin">
            <div class="contentBlock" style="margin-left:10px;height: 493px;">
            <div class="contenttitle2">
                <h3>Rates & Levies</h3>
            </div>
            <p>
                <label>Rates Number</label>
                <span class="field">
                    <input type="text" name="RateNumber" value="${Property.RateNumber}" />
                </span>
            </p>
            <p>
                <label>Rates Market Value</label>
                <span class="field">
                    <input type="text" name="RatesMarketValue" value="${Property.RatesMarketValue}" />
                </span>
            </p>
            <p>
                <label>Date Valued</label>
                <span class="field">
                    <input type="text" class="isDatePicker" name="DateValued"  value="${Property.DateValued}" />
                </span>
            </p>
            <p>
                <label>Date Occupational Certificate Received</label>
                <span class="field">
                    <input type="text" class="isDatePicker"  name="OccupationalCertificateReceived" value="${Property.OccupationalCertificateReceived}" />
                </span>
            </p>
            <p>
                <label>Occupational Certificate Number</label>
                <span class="field">
                    <input type="text" class=""  name="OccupationalCertificateNumber" value="${Property.OccupationalCertificateNumber}" />
                </span>
            </p>
            </div>
        </div>
        <div style="clear:both"></div>
    </div>
    <div>
        <div class="contentBlock" style="margin-top:10px;">
        <div class="contenttitle2">
                <h3>Insurance</h3>
            </div>
        <div style="clear:both"></div>
        <div class="one_half nomargin">
            <p>
            <label>Insurance Company</label>
            <span class="field">
                <input type="text" class=""  name="InsuranceCompany" value="${Property.InsuranceCompany}" />
            </span>
        </p>
            <p>
                <label>Owner's Policy Number</label>
                <span class="field">
                    <input type="text" class=""  name="OwnerPolicyNumber" value="${Property.OwnerPolicyNumber}" />
                </span>
            </p>
            <p>
                <label>Contractor Works Policy Number</label>
                <span class="field">
                    <input type="text" class=""  name="ContractorPolicyNumber" value="${Property.ContractorPolicyNumber}" />
                </span>
            </p>
            <p>
                <label>Golf Cart Insurance</label>
                <span class="field">
                    <input type="text" class=""  name="GolfCartInsurance" value="${Property.GolfCartInsurance}" />
                </span>
            </p>
        </div>
        <div class="one_half nomargin">
            <p>
                <label>Value</label>
                <span class="field">
                    <input type="text" class=""  name="OwnerPolicyValue" value="${Property.OwnerPolicyValue}" />
                </span>
            </p>
            <p>
                <label>Value</label>
                <span class="field">
                    <input type="text" class=""  name="ContractorPolicyValue" value="${Property.ContractorPolicyValue}" />
                </span>
            </p>
            <p>
                <label>Value</label>
                <span class="field">
                    <input type="text" class=""  name="GolfCartPolicyValue" value="${Property.GolfCartPolicyValue}" />
                </span>
            </p>
        </div>
        <div style="clear:both"></div>
        <p style="display:none;">
            <label>Upload Golf Policy</label>
            <span class="field">
                <input type="file" name="GolfPolicy" />
            </span>
        </p>
            </div>
    </div>
        <div class="contentBlock" style="margin-top:10px;">
    <div class="contenttitle2">
        <h3>ManageMent Association</h3>
    </div>
    <p>
        <label>Type</label>
        <span class="field">
            <select name="ManagementAssociationID" class="uniformselect">
            </select>
        </span>
    </p>
            </div>
</div>
