﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PropertyOccupantTemplate.ascx.cs" Inherits="Honeycomb.Web.UserControls.PropertyOccupantControl" %>
<script id="PropertyOccupantTemplate" type="text/x-jquery-tmpl">
    <h3><a href="#">${FirstName} ${LastName} ({{if KeyOccupantRelID == 10000}}Husband{{else KeyOccupantRelID == 10001}}Wife{{else KeyOccupantRelID == 10002}}Son{{else KeyOccupantRelID == 10003}}Daughter{{else KeyOccupantRelID == 10004}}Extended Family Member{{/if}}) {{if IsPrincipalOccupant}}(*Principle Occupant){{/if}}</a></h3>
    <div id="occupantno-${ID}">

        <div class="one_half nomargin">

            <div class="contentBlock">

            <div class="contenttitle2">
                <h3>Occupant Details</h3>
                <input type="hidden" name="ID" value="${ID}" />
                <input type="hidden" name="OccupantPhoto" value="${OccupantPhoto}" />
                
            </div>

              <div class="stdform">

                <div style="position:relative; left:87px; float:left; left:50px; padding-bottom:20px;">
                    <%-- photo --%>
                    <div class="photoDiv{{if OccupantPhoto!=null}} photoDivWithPhoto {{/if}}" {{if OccupantPhoto!=null}} style="background-color:#B5B5B5; background-size:cover; background-image:url(/Pages/DownloadFile.ashx?FileRef=${OccupantPhoto})" {{else IDOrPassportNo!=null}} style="background-color:#B5B5B5; background-size:cover; background-image:url(/Pages/DownloadFile.ashx?FileRef=Occupant&ID=${IDOrPassportNo})" {{/if}} >
                        <div class="example">
                            <div occupantID="${ID}" class="fubExample"></div>
                            <div class="fubUploadButton" class="btn btn-primary">Upload Image</div>
                        </div>
                    </div>
                </div>

                <div style="position:relative; float:left; top:30px; left:40px;">
                <p>
                    <label>Is Owner:</label>
                    <span class="field">
                        <input type="checkbox" value="1" name="IsOwner" {{if IsOwner}}checked="checked"{{/if}} />
                    </span>
                </p>

                <p>
                    <label>Principle Occupant:</label>
                    <span class="field">
                        <input type="checkbox" value="1" name="IsPrincipalOccupant" {{if IsPrincipalOccupant}}checked="checked"{{/if}} />
                    </span>
                </p>
                <p>
                    <label>Is Tenant:</label>
                    <span class="field">
                        <input type="checkbox" value="1" name="IsTenant" {{if IsTenant}}checked="checked"{{/if}} />
                    </span>
                </p>
                </div>

                <p style="clear:both;">
                    <label>First Name:</label>
                    <span class="field">
                        <input type="text" name="FirstName" value="${FirstName}" valtype="required;" />
                    </span>
                </p>
                <p>
                    <label>Surname:</label>
                    <span class="field">
                        <input type="text" name="LastName" value="${LastName}" valtype="required;" />
                    </span>
                </p>
                <p>
                    <label>ID/Passport:</label>
                    <span class="field">
                        <input type="text" name="IDOrPassportNo" value="${IDOrPassportNo}" />
                    </span>
                </p>
                <p>
                    <label>Gender:</label>
                    <span class="field">
                        <select name="Gender" class="uniformselect">
                            <option {{if Gender == 'Male'}}selected="selected"{{/if}} value="Male">Male</option>
                            <option {{if Gender == 'Female'}}selected="selected"{{/if}} value="Female">Female</option>
                        </select>
                    </span>
                </p>
                <p>
                    <label>Mobile No:</label>
                    <span class="field">
                        <input type="text" name="Cellphone" value="${Cellphone}" valtype="regex:phone" />
                    </span>
                </p>
                <p>
                    <label>Email Address:</label>
                    <span class="field">
                        <input type="text" name="Email" value="${Email}" valtype="regex:email" />
                    </span>
                </p>
                <p>
                    <label>Relationship to Principal Owner:</label>
                    <span class="field">
                        <select name="PrincipalOwnerRelID" class="uniformselect">

                        </select>
                    </span>
                </p>
                <p>
                    <label>Role in household:</label>
                    <span class="field">
                        <select name="KeyOccupantRelID" class="uniformselect">

                        </select>
                    </span>
                </p>
            </div>

            </div>
        </div>
        <div class="one_half nomargin">

            <div class="contentBlock" style="margin-left:10px;">

            <div class="contenttitle2">
                <h3>Access Details</h3>
            </div>

             <div class="stdform">
                <p>
                    <label>Access card:</label>
                    <span class="field">
                        <input type="checkbox" value="1" name="HasAccessCard" {{if HasAccessCard}}checked="checked"{{/if}} />
                    </span>
                </p>
                 <p>
                    <label>SMS Visitors:</label>
                    <span class="field">
                        <input type="checkbox" value="1" name="CanSMS" {{if CanSMS}}checked="checked"{{/if}} />
                    </span>
                </p>
                 <p>
                    <label>Receive Communication:</label>
                    <span class="field">
                        <input type="checkbox" value="1" name="RecieveComms" {{if RecieveComms}}checked="checked"{{/if}} />
                    </span>
                </p>
                 <p>
                    <label>Registered on Clubmaster:</label>
                    <span class="field">
                        <input type="checkbox" value="1" name="ClubmasterReg" {{if ClubmasterReg}}checked="checked"{{/if}} />
                    </span>
                </p>
                 <p>
                    <label>Date Registered on Clubmaster:</label>
                    <span class="field">
                        <input type="text" name="ClubMasterRegDate" class="isDatePicker" value='{{if ClubMasterRegDate!=null}}${dateFormat(ClubMasterRegDate, "dd/mm/yyyy")}{{/if}}' />
                    </span>
                </p>
                 <p>
                    <label>Golf Member:</label>
                    <span class="field">
                        <input type="checkbox" value="1" name="GolfMember" {{if GolfMember}}checked="checked"{{/if}} />
                    </span>
                </p>
                 <p>
                    <label>Social Member:</label>
                    <span class="field">
                        <input type="checkbox" value="1" name="SocialMember" {{if SocialMember}}checked="checked"{{/if}} />
                    </span>
                </p>
                 
             </div>
          </div>
            <div class="contentBlock" style="margin:10px 0 0 10px;">
                <div class="stdform">
                <div class="contenttitle2">
                <h3>Intranet Access</h3>
                </div>
					<p>
                    <label>Send Intranet Login Details:</label>
                    <span class="field">
                        <input type="checkbox" value="1" name="SendIntranetDetails"  />
                    </span>
                </p>
                <p>
                    <label>Username:</label>
                    <span class="field">
                        <input type="text" name="IntranetUsername" value="${IntranetUsername}" />
                    </span>
                </p>
					<p>
                    <label>Password:</label>
                    <span class="field">
                        <input type="text" name="IntranetPassword" value="${IntranetPassword}" />
                    </span>
                </p>
                </div>
            </div>
             <ul class="buttonlist">
                <li><a occupantid="${ID}" href="javascript:void(0);" class="btn  btn_pencil radius50 updateoccupant"><span>Update Occupant</span></a></li>
                <li><a occupantid="${ID}" isprinciple="{{if IsPrincipalOccupant}}yes{{else}}no{{/if}}" href="javascript:void(0);" class="btn  btn_trash archiveoccupant"><span>Archive</span></a></li>
				<li><a occupantid="${ID}" isprinciple="{{if IsPrincipalOccupant}}yes{{else}}no{{/if}}" href="javascript:void(0);" class="btn  btn_trash restoreoccupant"><span>Restore</span></a></li>
            </ul>
        </div>
        <div class="clearall"></div>
    </div>
</script>