﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace SAWD.JobAgent.Config
{
    public class JobConfigSection : ConfigurationSection
    {
        /// <summary>
        /// A collection of Jobs to be run by the JobAgent
        /// </summary>
        [ConfigurationProperty("Jobs")]
        public JobConfigElementCollection Jobs {
            get { return ((JobConfigElementCollection)(base["Jobs"])); }
        }

        ///// <summary>
        ///// Whether or not the minified version of the DataPusher javascript should be used when using the DataPusherJS handler
        ///// </summary>
        //[ConfigurationProperty("useminifiedJS", DefaultValue = "false", IsKey = false, IsRequired = false)]
        //public bool UseMinifiedJS {
        //    get {
        //        return ((bool)(base["useminifiedJS"]));
        //    }
        //    set {
        //        base["useminifiedJS"] = value;
        //    }
        //}
    }
}
