﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace SAWD.JobAgent.Config
{
    [ConfigurationCollection(typeof(JobConfigElement))]
    public class JobConfigElementCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement() {

            return new JobConfigElement();

        }



        protected override object GetElementKey(ConfigurationElement element) {

            return ((JobConfigElement)(element)).Name;

        }



        public JobConfigElement this[int idx] {
            get {
                return (JobConfigElement)BaseGet(idx);
            }
        }


        public JobConfigElement this[string name] {
            get {
                return (JobConfigElement)BaseGet(name);
            }
        }

    }

}
