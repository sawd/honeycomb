﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace SAWD.JobAgent.Config
{
    public class JobConfigElement : ConfigurationElement
    {
        /// <summary>
        /// The job name
        /// </summary>
        [ConfigurationProperty("name", DefaultValue = "", IsKey = true, IsRequired = true)]
        public string Name {
            get {
                return ((string)(base["name"]));
            }
            set {
                base["name"] = value;
            }
        }

        /// <summary>
        /// The assembly that the callback method belongs to. This should be a valid Namespace/Assembly notation e.g. "Juice.Feeds.TestPriceCheck, Juice.Feeds"
        /// </summary>
        [ConfigurationProperty("callbackassembly", DefaultValue = "", IsKey = false, IsRequired = true)]
        public string CallbackAssembly {
            get {
                return ((string)(base["callbackassembly"]));
            }
            set {
                base["callbackassembly"] = value;
            }
        }

        /// <summary>
        /// The method to to be run on the given interval. This belongs to the callbackassembly. This must be a public static method with no parameters
        /// </summary>
        [ConfigurationProperty("callbackmethod", DefaultValue = "", IsKey = false, IsRequired = true)]
        public string CallbackMethod {
            get {
                return ((string)(base["callbackmethod"]));
            }
            set {
                base["callbackmethod"] = value;
            }
        }

        /// <summary>
        /// The interval at which the CallbackExecution method is called
        /// </summary>
        [ConfigurationProperty("interval", DefaultValue = "60000", IsKey = false, IsRequired = true)]
        public int Interval {
            get {
                return ((int)(base["interval"]));
            }
            set {
                base["interval"] = value;
            }
        }
    }
}
