﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;
using System.Configuration;
using System.Web;

namespace SAWD.JobAgent
{

    /// <summary>
    /// A class that, when instantiated, creates a series of Timers based on the "JobAgentJobs" configsection of a config file. Each Timer will execute at a given interval and 
    /// will then invoke a method on a given class defined by CallbackMethod and CallbackAssembly respectively
    /// </summary>
    public class JobScheduler
    {
        /// <summary>
        /// A private transpoter class used for passing state info over to the Timer object's callback delegate's stateinfo parameter
        /// </summary>
        private class JobStateInfo
        {
            public Config.JobConfigElement JobConfig { get; set; }
            public HttpContext Context { get; set; }
        }

        /// <summary>
        /// A list of Timer objects
        /// </summary>
        public List<Timer> TimedJobs { get; set; }

        /// <summary>
        /// Constructor that returns all elements (jobs) in the configsection and creates a timer for each one with the specific interval
        /// </summary>
        public JobScheduler() {
            SAWD.JobAgent.Config.JobConfigSection config = (SAWD.JobAgent.Config.JobConfigSection)ConfigurationManager.GetSection("JobAgentJobs");
            TimedJobs = new List<Timer>();
            foreach (Config.JobConfigElement job in config.Jobs) {
                JobStateInfo jobstateinfo = new JobStateInfo() { JobConfig = job, Context = HttpContext.Current };
                Timer timer = new System.Threading.Timer(ExecuteJob, jobstateinfo, job.Interval, job.Interval);
                TimedJobs.Add(timer);
            }            
        }

        /// <summary>
        /// Method called by the timer delegate. It uses reflection to execute the static method that is found in the Config.JobConfigElement item
        /// </summary>
        /// <param name="stateInfo">The stateinfo parameter will always be a JobStateInfo item</param>
        public void ExecuteJob(Object stateInfo) {            
            JobStateInfo jobstateinfo = (JobStateInfo)stateInfo;
            System.Reflection.Assembly o = System.Reflection.Assembly.Load(jobstateinfo.JobConfig.CallbackAssembly.Split(',')[1]);
            object[] methodparams = new object[2];
            //All callback methods require that we pass them the current HttpContext as they may not always have access to it, as well as the interval
            methodparams[0] = jobstateinfo.Context;
            methodparams[1] = jobstateinfo.JobConfig.Interval;
            o.GetType(jobstateinfo.JobConfig.CallbackAssembly.Split(',')[0]).GetMethod(jobstateinfo.JobConfig.CallbackMethod, System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public).Invoke(null, methodparams);                            
        }

    }
}
