﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace TestInterface {
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class Service1 : IService1 {

        public Honeycomb.Custom.EntityTransport<Honeycomb.Custom.Data.EMPLOYEE> GetUsers() {
            throw new NotImplementedException();
        }

        public Honeycomb.Custom.EntityTransport<Honeycomb.Custom.Data.EMPLOYEE> AddUsers(Honeycomb.Custom.Data.EMPLOYEE userData) {
            throw new NotImplementedException();
        }

        public Honeycomb.Custom.EntityTransport<Honeycomb.Custom.Data.EMPLOYEE> CreateUser(Honeycomb.Custom.Data.EMPLOYEE userData) {
            throw new NotImplementedException();
        }

        public Honeycomb.Custom.EntityTransport<Honeycomb.Custom.Data.EMPLOYEE> UpdateUser(Honeycomb.Custom.Data.EMPLOYEE userData) {
            throw new NotImplementedException();
        }

        public Honeycomb.Custom.EntityTransport<Honeycomb.Custom.Data.EMPLOYEE> DeleteUser(Honeycomb.Custom.Data.EMPLOYEE userData) {
            throw new NotImplementedException();
        }
    }
}
