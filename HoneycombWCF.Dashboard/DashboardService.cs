﻿#region Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using Honeycomb.Custom;
using Custom = Honeycomb.Custom;
using Honeycomb.Security;
using System.ComponentModel;
using System.Web;
using System.Web.Security;
using Honeycomb.Custom.Data;

#endregion

namespace Honeycomb.Dashboard {

    [ServiceContract(Namespace = "DashboardService")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceOutputBehavior]
    public class WCFDashboard {

        [OperationContract]
		[Security("Admin,DBCurrentIncidents")]
        public EntityTransport<DashboardIncidents> GetDashboardIncidents(string dashType = "month", DateTime? endDate = null) {
            Utility.WCFCheckSecurityAttribute();
            List<DashboardIncidents> graphIncidents = new List<DashboardIncidents>();
            if (endDate == null) {
                endDate = DateTime.Now;
            }
            try {
                using (var context = new Honeycomb_Entities()) {
                    graphIncidents = context.GetDashboardIncidents(dashType, endDate).ToList();
                }

                return Utility.ServiceResponse(ref graphIncidents);
            } catch (Exception ex) {
                return Utility.ServiceResponse(ref graphIncidents, ex);
            }
        }

        [OperationContract]
		[Security("Admin,DBAverageIncidentTurnaround")]
        public EntityTransport<GraphData> GetAverageIncidentTurnAround(string dashType = "week") {
            Utility.WCFCheckSecurityAttribute();
            List<GraphData> graphIncidents = new List<GraphData>();
			DateTime startDate = GetReportDate(dashType);

            try {
                using (var context = new Honeycomb_Entities()) {
					graphIncidents = context.GetAverageIncidentTurnAround(startDate, DateTime.Now).ToList();
                }

                return Utility.ServiceResponse(ref graphIncidents);
            } catch (Exception ex) {
                return Utility.ServiceResponse(ref graphIncidents, ex);
            }
        }

		[OperationContract]
		[Security("Admin,DBIncidentsByType")]
		public EntityTransport<GraphData> GetIncidentCategoryCounts(string dashType = "week") {
			Utility.WCFCheckSecurityAttribute();
			List<GraphData> graphIncidentCounts = new List<GraphData>();
			DateTime startDate = GetReportDate(dashType);

			try {
				using(var context = new Honeycomb_Entities()){
					graphIncidentCounts = context.GetIncidentCategoryCounts(startDate, DateTime.Now).ToList();
				}

				return Utility.ServiceResponse(ref graphIncidentCounts);
			}catch(Exception ex){
				return Utility.ServiceResponse(ref graphIncidentCounts, ex);
			}

		}

		[OperationContract]
		[Security("Admin,DBEstateDemographics")]
		public EntityTransport<GraphData> GetPropertyDemographics() {
			Utility.WCFCheckSecurityAttribute();
			List<GraphData> graphData = new List<GraphData>();
			try {
				using (var context = new Honeycomb_Entities()) {
					graphData = context.GetPropertyDemographics().ToList();
				}

				return Utility.ServiceResponse(ref graphData);
			} catch (Exception ex) {
				return Utility.ServiceResponse(ref graphData, ex);
			}
		}

        [OperationContract]
		[Security("Admin,DBVisitorsOnEstate,DBSuppliersOnEstate")]
        public EntityTransport<GraphData> GetNonResidentsOnEstate() {
            Utility.WCFCheckSecurityAttribute();
            List<GraphData> graphData = new List<GraphData>();

            try {
                using (var context = new Honeycomb_Entities()) {
                    graphData = context.GetNonResidentsOnEstate().ToList();
                }

                return Utility.ServiceResponse(ref graphData);
            } catch (Exception ex) {
                return Utility.ServiceResponse(ref graphData, ex);
            }

        }


        [OperationContract]
		[Security("Admin,DBResidentNotInducted")]
        public EntityTransport<GraphData> GetExceptionAlertInduction() {
            Utility.WCFCheckSecurityAttribute();
            List<GraphData> graphData = new List<GraphData>();

            try {
                using (var context = new Honeycomb_Entities()) {
                    graphData = context.GetExceptionAlertInduction().ToList();
                }

                return Utility.ServiceResponse(ref graphData);
            } catch (Exception ex) {
                return Utility.ServiceResponse(ref graphData, ex);
            }

        }


        [OperationContract]
		[Security("Admin,DBResidentCartNotInsured")]
        public EntityTransport<GraphData> GetExceptionAlertGolfCarts() {
            Utility.WCFCheckSecurityAttribute();
            List<GraphData> graphData = new List<GraphData>();

            try {
                using (var context = new Honeycomb_Entities()) {
                    graphData = context.GetExceptionAlertGolfCarts().ToList();
                }

                return Utility.ServiceResponse(ref graphData);
            } catch (Exception ex) {
                return Utility.ServiceResponse(ref graphData, ex);
            }

        }


        [OperationContract]
		[Security("Admin,DBCodesHistorical")]
        public EntityTransport<GraphData> GetAccessCodesForEntranceHistorical(string dashType = "month", DateTime? endDate = null) {
            Utility.WCFCheckSecurityAttribute();
            List<GraphData> graphData = new List<GraphData>();
            if (endDate == null) {
                endDate = DateTime.Now;
            }
            try {
                using (var context = new Honeycomb_Entities()) {
                    graphData = context.GetAccessCodesForEntranceHistorical(dashType, endDate).ToList();
                }

                return Utility.ServiceResponse(ref graphData);
            } catch (Exception ex) {
                return Utility.ServiceResponse(ref graphData, ex);
            }

        }


        [OperationContract]
		[Security("Admin,DBCodesFuture")]
        public EntityTransport<GraphData> GetAccessCodesForEntranceFuture(string dashType = "month") {
            Utility.WCFCheckSecurityAttribute();
            List<GraphData> graphData = new List<GraphData>();
            try {
                using (var context = new Honeycomb_Entities()) {
                    graphData = context.GetAccessCodesForEntranceFuture(dashType).ToList();
                }

                return Utility.ServiceResponse(ref graphData);
            } catch (Exception ex) {
                return Utility.ServiceResponse(ref graphData, ex);
            }
        }


		[OperationContract]
		[Security("Admin,DBProjectedFees")]
		public EntityTransport<GraphData> GetCurrentProjectedFeeCollection() {
			Utility.WCFCheckSecurityAttribute();
			List<GraphData> graphData = new List<GraphData>();

			try {
				using(var context = new Honeycomb_Entities()){
					graphData = context.GetCurrentProjectedFeeCollection().ToList();
				}

				return Utility.ServiceResponse(ref graphData);
			}catch(Exception ex){
				return Utility.ServiceResponse(ref graphData, ex);
			}
		}

		[OperationContract]
		[Security("Admin,DBCodeSources")]
		public EntityTransport<GraphData> GetAccessCodeSource(string dashType = "week") {
			Utility.WCFCheckSecurityAttribute();
			List<GraphData> graphData = new List<GraphData>();

			try {
				using (var context = new Honeycomb_Entities()) {
					graphData = context.GetAccessCodeSources(dashType, DateTime.Now).ToList();
				}

				return Utility.ServiceResponse(ref graphData);
			} catch (Exception ex) {
				return Utility.ServiceResponse(ref graphData, ex);
			}
		}

		[OperationContract]
		[Security("Admin,DBPeopleTotals")]
		public EntityTransport<GraphData> GetTotalOccupants() {
			Utility.WCFCheckSecurityAttribute();
			List<GraphData> graphData = new List<GraphData>();

			try {
				using(var context = new Honeycomb_Entities()) {
					graphData = context.GetTotalOccupants().ToList();
				}

				return Utility.ServiceResponse(ref graphData);
			} catch(Exception ex) {
				return Utility.ServiceResponse(ref graphData, ex);
			}
		}

		[OperationContract]
		[Security("Admin,DBSentSMS")]
		public EntityTransport<GraphData> GetSentSMSs(DateTime monthToQuery) {
			Utility.WCFCheckSecurityAttribute();
			List<GraphData> graphData = new List<GraphData>();

			try {
				using (var context = new Honeycomb_Entities()) {
					graphData = context.GetSentSMSs(monthToQuery).ToList();
				}

				return Utility.ServiceResponse(ref graphData);
			} catch (Exception ex) {
				return Utility.ServiceResponse(ref graphData, ex);
			}
		}

        private DateTime GetReportDate(string dashType) {
            switch (dashType.ToLower()) {
                case "week":
                    return DateTime.Now.AddDays(-6);
                    break;
                case "month":
                    return DateTime.Now.AddMonths(-1);
                    break;
                case "year":
                    return DateTime.Now.AddYears(-1);
                    break;
                default:
                    return DateTime.Now.AddDays(-6);
                    break;
            }
        }

    }
}
