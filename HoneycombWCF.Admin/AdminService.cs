﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using Honeycomb.Custom;
using Honeycomb.Custom.Data;

namespace Honeycomb.Admin {
	[ServiceContract(Namespace = "AdminService")]
	[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
	public class WCFAdmin {
		private Honeycomb.Custom.Data.Honeycomb_Entities context = new Honeycomb.Custom.Data.Honeycomb_Entities();

		#region Venues
        /// <summary>
        /// Get a List of Venues
        /// </summary>
        /// <returns></returns>
		[OperationContract]
        //[Security("Admin,VenueManagement")] *made this global
		public EntityTransport<Venue> GetVenues(bool intranetVisibleOnly = true) {
            //Utility.WCFCheckSecurityAttribute();
            List<Venue> venueList = new List<Venue>();
            try {
				if(intranetVisibleOnly) {
					venueList = context.Venue.Where(v => v.DeletedOn == null && v.IsVisibleOnIntranet == true).OrderBy(o => o.Name).ToList();
				} else {
					venueList = context.Venue.Where(v => v.DeletedOn == null).OrderBy(o => o.Name).ToList();
				}
                return Utility.ServiceResponse(ref venueList);
			} catch(Exception ex) {
				return Utility.ServiceResponse(ref venueList, ex);
			}
            
        
		}
        /// <summary>
        /// Add and Edit a Venue
        /// </summary>
        /// <param name="venueEntity"></param>
        /// <returns></returns>

        [OperationContract]
        [Security("Admin,VenueManagement")]
        public EntityTransport<ProcessResponse> UpdateVenues(Venue venueEntity) {
            Utility.WCFCheckSecurityAttribute();
            var pr = new ProcessResponse();

            try {
                
				if(venueEntity.VenueID == 0) {
                        venueEntity.InsertedOn = DateTime.Now;
                        venueEntity.InsertedBy = SessionManager.UserName;
                        venueEntity.InsertedByID = SessionManager.UserID;

                        context.Venue.Add(venueEntity);
                    } else {
                        var venueDB = context.Venue.Where(v => v.VenueID == venueEntity.VenueID).FirstOrDefault();

					if(venueEntity != null) {
                            venueEntity.MergeInto(venueDB, new string[]{
                           "InsertedOn",
                           "InsertedBy",
                           "InsertedByID",
                           "VenueID"
                           }, true);
                        }
                    }
                    pr.success = true;
                    context.SaveChanges();
                
                return Utility.ServiceResponse(ref pr);

            } catch(Exception Exception) {
                pr.success = false;
                return Utility.ServiceResponse(ref pr, Exception);
            }

        }
        /// <summary>
        /// Archive the Venues
        /// </summary>
        /// <param name="venueEntity"></param>
        /// <returns></returns>
        [OperationContract]
        [Security("Admin,VenueManagement")]
        public EntityTransport<bool> ArchiveVenue(long VenueID) {

            Utility.WCFCheckSecurityAttribute();

            bool success = true;
            try {
                
                    var venueDB = context.Venue.Where(v => v.VenueID == VenueID).FirstOrDefault();

				if(venueDB != null) {
                        venueDB.DeletedOn = DateTime.Now;
					venueDB.DeletedBy = SessionManager.UserName;
                        venueDB.DeletedByID = SessionManager.UserID;

                        context.SaveChanges();
                        success = true;
				} else {
                        success = false;
                    }                
                
				return Utility.ServiceResponse(ref success);
			} catch(Exception Exception) {
                return Utility.ServiceResponse(ref success, Exception);
            }
        }


        [OperationContract]
        [Security("Admin,VenueManagement")]
		public EntityTransport<Venue> GetVenue(long VenueID) {

            Utility.WCFCheckSecurityAttribute();
            var venueDB = new Venue();
            
            try {
                venueDB = context.Venue.Where(v => v.VenueID == VenueID).FirstOrDefault();
                return Utility.ServiceResponse(ref venueDB);
			} catch(Exception e) {
                return Utility.ServiceResponse(ref venueDB, e);

            }
        }




		#endregion

		#region Bookings
		/// <summary>
		/// gets all available booking dates.
		/// </summary>
		/// <param name="VenueID"></param>
		/// <param name="bookingDay"></param>
		/// <returns></returns>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
		[OperationContract]
        [Security("Admin,VenueManagement")]
		public EntityTransport<CustomTime> GetAvailableBookingStartTimes(long VenueID, DateTime bookingDay) {
            Utility.WCFCheckSecurityAttribute();
			List<VenueBooking> venueList = new List<VenueBooking>();
			List<CustomTime> bookingTimes = new List<CustomTime>();
            try {

				venueList = context.VenueBooking.Where(v => v.VenueID == VenueID && v.VenueBookingStatusID == (long)Enumerators.VenueBookingStatus.Pending).OrderBy(o => o.StartDateTime).ToList();
				//set the bookings to start time for the booking day at 7:00am
				DateTime globalStart = new DateTime(bookingDay.Year, bookingDay.Month, bookingDay.Day, 7, 0, 0);

				//set the cap time for all bookings in this case all bookings end at 7:30 so we add 12 hours and 30 minutes to the start time
				DateTime globalEnd = new DateTime(bookingDay.Year, bookingDay.Month, bookingDay.Day, 23, 45, 0);
				
				//now we loop through time, incrementing at 30 minute intervals to fnd available booking slots
				DateTime slotCheck = new DateTime(bookingDay.Year, bookingDay.Month, bookingDay.Day, 7, 0, 0);
				int checker = 0;
				while(slotCheck <= globalEnd) {
					checker = venueList.Where(v => (v.StartDateTime <= slotCheck && v.EndDateTime > slotCheck.AddSeconds(59)) && v.VenueBookingStatusID == (long)Enumerators.VenueBookingStatus.Pending && v.CancelledOn == null).Count();

					if(checker == 0) {
						//add the 
						bookingTimes.Add(new CustomTime() {
							DateObj = slotCheck,
							Time = string.Format("{0}:{1}", slotCheck.Hour, slotCheck.Minute),
							DisplayTime = string.Format("{0}:{1} {2}", slotCheck.ToString("HH"), slotCheck.ToString("mm"), slotCheck.ToString("tt")),
							hrs = "0"
						});
					}

					//increment by 30 Minute interval
					slotCheck = slotCheck.AddMinutes(15);
				}

				return Utility.ServiceResponse(ref bookingTimes);
			} catch(Exception ex) {
				return Utility.ServiceResponse(ref bookingTimes, ex);
			}        
		}
         
        /// <summary>
        /// gets the available endTimes for a specific time
        /// </summary>
        /// <param name="VenueID"></param>
        /// <param name="bookingDay"></param>
        /// <returns></returns>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
		[OperationContract]
        [Security("Admin,VenueManagement")]
		public EntityTransport<CustomTime> GetAvailableBookingEndTimes(long VenueID, DateTime bookingDay) {
            Utility.WCFCheckSecurityAttribute();
			List<VenueBooking> venueList = new List<VenueBooking>();
			List<CustomTime> bookingTimes = new List<CustomTime>();
            try {

				venueList = context.VenueBooking.Where(v => v.VenueID == VenueID && v.VenueBookingStatusID == (long)Enumerators.VenueBookingStatus.Pending).OrderBy(o => o.StartDateTime).ToList();
				//set the bookings to the specified date passed in the parameters above
				DateTime globalStart = new DateTime(bookingDay.Year, bookingDay.Month, bookingDay.Day, 7, 0, 0);

				//set the cap time for all bookings in this case all bookings end at 7:30 so we add 12 hours and 30 minutes to the start time
				DateTime globalEnd = new DateTime(bookingDay.Year, bookingDay.Month, bookingDay.Day, 23, 45, 0);
				
				//now we loop through time, incrementing at 30 minute intervals to fnd available booking slots
				DateTime slotCheck = bookingDay.AddMinutes(15);
				while(slotCheck <= globalEnd && venueList.Where(v => (v.StartDateTime < slotCheck && v.EndDateTime > slotCheck.AddSeconds(59)) && v.VenueBookingStatusID == (long)Enumerators.VenueBookingStatus.Pending && v.CancelledOn == null).Count() == 0) {
					
					//add the time
					TimeSpan diff = (slotCheck - bookingDay);
					bookingTimes.Add(new CustomTime() {
						DateObj = slotCheck,
						Time = string.Format("{0}:{1}", slotCheck.Hour, slotCheck.Minute),
						DisplayTime = string.Format("{0}:{1} {2}", slotCheck.ToString("HH"), slotCheck.ToString("mm"), slotCheck.ToString("tt")),
						hrs = ((diff.Hours != 0 ? diff.Hours + " Hrs " : "") + (diff.Minutes != 0 ? diff.Minutes + " Mins" : "")).Trim()
					});

					//increment by 30 Minute interval
					slotCheck = slotCheck.AddMinutes(15);
				}

				return Utility.ServiceResponse(ref bookingTimes);
			} catch(Exception ex) {
				return Utility.ServiceResponse(ref bookingTimes, ex);
			}        
		}

		/// <summary>
		/// Add, Update or Delete a Venuebooking, validating for time and venue.
		/// </summary>
		/// <param name="booking"></param>
		/// <param name="cancelBooking"></param>
		/// <returns></returns>
		[OperationContract]
		[Security("Admin,BookingManagement")]
		public EntityTransport<CustomCalendarEvent> UpdateBooking(VenueBooking booking, bool cancelBooking) {

			Utility.WCFCheckSecurityAttribute();
			bool isNewBooking = true;
			var processResponse = new ProcessResponse();
			var bookingEntity = new CustomCalendarEvent();
			bookingEntity.start = DateTime.Now;
			bookingEntity.end = DateTime.Now;
			Communications com = new Communications();

			try {
				Utility.CheckSession();

				//check validity
				int conflicts = (from b in context.VenueBooking
								 where b.VenueID == booking.VenueID 
								 && b.VenueBookingID != booking.VenueBookingID											// Only include other bookings
								 && b.CancelledOn == null																	// that are not cancelled
								 && ((b.StartDateTime <= booking.StartDateTime && b.EndDateTime >= booking.EndDateTime)		//that surrounds the time
									|| (b.EndDateTime > booking.StartDateTime && b.EndDateTime <= booking.EndDateTime)		//starts before the booking
									|| (b.StartDateTime >= booking.StartDateTime && b.StartDateTime < booking.EndDateTime)) // starts after booking
								 select b).Count();

				if(conflicts > 0) {
					processResponse.success = false;
					processResponse.message = "The dates chosen for this booking would cause a conflict.";
				} else {
					VenueBooking dbBooking;
					if(booking.VenueBookingID == -1) {
						dbBooking = booking;
						dbBooking.InsertedOn = DateTime.Now;
						dbBooking.InsertedBy = SessionManager.UserName;
						dbBooking.InsertedByID = SessionManager.UserID;
						dbBooking.VenueID = booking.VenueID;
						dbBooking.VenueBookingStatusID = (long)Enumerators.VenueBookingStatus.Pending;
						context.VenueBooking.Add(dbBooking);
					} else {
						isNewBooking = false;
						//Get Database VenueBooking
						dbBooking = context.VenueBooking.Where(w => w.VenueBookingID == booking.VenueBookingID).FirstOrDefault();
						if(cancelBooking == true && dbBooking.CancelledOn == null) {
							dbBooking.CancelledOn = DateTime.Now;
							dbBooking.CancelledBy = SessionManager.UserName;
							dbBooking.CancelledByID = SessionManager.UserID;
						} else {
							//Merge the booking object into the DB object
							booking.MergeInto(dbBooking, new string[] { 
                                "CancelledBy",
                                "CancelledByID",
                                "CancelledOn",                                
                                "InsertedBy",
                                "InsertedByID",
                                "InsertedOn"
                            }, true);
						}
					}
					Venue venue = context.Venue.Where(w => w.VenueID == dbBooking.VenueID).FirstOrDefault();
					if(venue.IsCostTimeBased) {
						//Calculate cost based on the number of increments in the timespan.
						TimeSpan bookingTime = dbBooking.EndDateTime - dbBooking.StartDateTime;
						Decimal increments = (Decimal)bookingTime.TotalMinutes / venue.TimeIncrement;
						dbBooking.TotalBookingCost = increments * venue.Cost;
					} else {
						//flat booking rate.
						dbBooking.TotalBookingCost = venue.Cost;
					}
					context.SaveChanges();

					//now send notifications
					if(isNewBooking) {
						//new booking, send reminder

						//send notification to client
						com.SendEmail(dbBooking.Email, "Booking at " + venue.Name, Utility.GetMessageContent(Enumerators.MessageType.EmailClientBookingNotification).ReplaceObjectTokens(
								new {
									BookingPerson = dbBooking.Name,
									VenueName = venue.Name,
									StartDateTime = dbBooking.StartDateTime.ToString("HH:mm dd/MM/yyyy"),
									EndDateTime = dbBooking.EndDateTime.ToString("HH:mm dd/MM/yyyy"),
									BookingCost = "R" + dbBooking.TotalBookingCost.ToString("0.00")
								}
							));

						//send notification to admin
						com.SendEmail(CacheManager.SystemSetting.BookingsEmailRecipient, "Booking received for " + venue.Name, Utility.GetMessageContent(Enumerators.MessageType.EmailAdminBookingNotification).ReplaceObjectTokens(
								new {
									BookingPerson = dbBooking.Name,
									VenueName = venue.Name,
									StartDateTime = dbBooking.StartDateTime.ToString("HH:mm dd/MM/yyyy"),
									EndDateTime = dbBooking.EndDateTime.ToString("HH:mm dd/MM/yyyy"),
									BookingCost = "R" + dbBooking.TotalBookingCost.ToString("0.00")
								}
							));
					}

					if(cancelBooking) {
						//send notification to client
						com.SendEmail(dbBooking.Email, "Booking at " + venue.Name + " Cancelled", Utility.GetMessageContent(Enumerators.MessageType.EmailClientBookingCancelled).ReplaceObjectTokens(
								new {
									BookingPerson = dbBooking.Name,
									VenueName = venue.Name,
									StartDateTime = dbBooking.StartDateTime.ToString("HH:mm dd/MM/yyyy"),
									EndDateTime = dbBooking.EndDateTime.ToString("HH:mm dd/MM/yyyy"),
									BookingCost = "R" + dbBooking.TotalBookingCost.ToString("0.00")
								}
							));

						//send notification to admin
						com.SendEmail(CacheManager.SystemSetting.BookingsEmailRecipient, "Booking at " + venue.Name + " Cancelled", Utility.GetMessageContent(Enumerators.MessageType.EmailAdminBookingCancelled).ReplaceObjectTokens(
								new {
									BookingPerson = dbBooking.Name,
									VenueName = venue.Name,
									StartDateTime = dbBooking.StartDateTime.ToString("HH:mm dd/MM/yyyy"),
									EndDateTime = dbBooking.EndDateTime.ToString("HH:mm dd/MM/yyyy"),
									BookingCost = "R" + dbBooking.TotalBookingCost.ToString("0.00")
								}
							));
					}

					processResponse.success = true;
					processResponse.message = "Successfully updated booking.";

					bookingEntity = context.VenueBooking.Where(b => b.VenueBookingID == dbBooking.VenueBookingID).Select(b => new CustomCalendarEvent() {
						OccupantID = b.OccupantID,
						ID = b.VenueBookingID,
						VenueID = b.VenueID,
						TotalBookingCost = b.TotalBookingCost,
						InsertedByID = b.InsertedByID,
						InsertedBy = b.InsertedBy,
						InsertedOn = b.InsertedOn,
						title = b.Name,
						Name = b.Name,
						Email = b.Email,
						Phone = b.PhoneNumber,
						VenueBookingStatusID = b.VenueBookingStatusID,
						Requirements = b.Requirements,
						allDay = false,
						start = b.StartDateTime,
						end = b.EndDateTime,
						className = b.VenueBookingStatus.Name,
						editable = true
					}).FirstOrDefault();
				}

				return Utility.ServiceResponse(ref bookingEntity);

			} catch(Exception ex) {
				return Utility.ServiceResponse(ref bookingEntity, ex);
			}
		}

		/// <summary>
		/// Updates the bookings time for a single booking entry by ID.
		/// </summary>
		/// <param name="BookingID"></param>
		/// <param name="start"></param>
		/// <param name="end"></param>
		/// <returns></returns>
		[OperationContract]
		[Security("Admin,BookingManagement")]
		public EntityTransport<ProcessResponse> UpdateBookingTimes(long BookingID, DateTime start, DateTime end) {

			Utility.WCFCheckSecurityAttribute();

			var processResponse = new ProcessResponse();
			try {
				Utility.CheckSession();
				VenueBooking dbBooking;
				//Get Database VenueBooking
				dbBooking = context.VenueBooking.Where(w => w.VenueBookingID == BookingID).FirstOrDefault();

				//check validity
				int conflicts = (from b in context.VenueBooking
								 where b.VenueBookingID != BookingID											// Only include other bookings
								 && b.CancelledOn == null 
								 && b.VenueID == dbBooking.VenueID
								 && b.VenueBookingStatusID == (long) Enumerators.VenueBookingStatus.Pending 																// that are not cancelled
								 && ((b.StartDateTime <= start && b.EndDateTime >= end)		//that surrounds the time
									|| (b.EndDateTime > start && b.EndDateTime <= end)		//starts before the booking
									|| (b.StartDateTime >= start && b.StartDateTime < end)) // starts after booking
								 select b).Count();

				if(conflicts > 0) {
					processResponse.success = false;
					processResponse.message = "The dates chosen for this booking would cause a conflict.";
				} else {
					
					dbBooking.StartDateTime = start;
					dbBooking.EndDateTime = end;
					Venue venue = context.Venue.Where(w => w.VenueID == dbBooking.VenueID).FirstOrDefault();
					if(venue.IsCostTimeBased) {
						//Calculate cost based on the number of increments in the timespan.
						TimeSpan bookingTime = dbBooking.EndDateTime - dbBooking.StartDateTime;
						Decimal increments = (Decimal)bookingTime.TotalMinutes / venue.TimeIncrement;
						dbBooking.TotalBookingCost = increments * venue.Cost;
					} else {
						//flat booking rate.
						dbBooking.TotalBookingCost = venue.Cost;
					}
					context.SaveChanges();
					processResponse.success = true;
					processResponse.message = "Booking successfully updated!";
				}

				return Utility.ServiceResponse(ref processResponse);

			} catch(Exception ex) {
				return Utility.ServiceResponse(ref processResponse, ex);
			}
		}

		/// <summary>
		/// Retrieves the list of bookings for a particular date and venue
		/// </summary>
		/// <param name="VenueID"></param>
		/// <param name="StartDate"></param>
		/// <returns></returns>
		[OperationContract]
		[Security("Admin,BookingManagement")]
		public EntityTransport<CustomCalendarEvent> GetBookingList(long VenueID, DateTime StartDate) {

			Utility.WCFCheckSecurityAttribute();
			List<CustomCalendarEvent> bookings = new List<CustomCalendarEvent>();

			try {
				DateTime beginning = new DateTime(StartDate.Year, StartDate.Month, StartDate.Day);
				DateTime end = new DateTime(StartDate.Year, StartDate.Month, StartDate.Day).AddDays(1);
				//overriding above logic, the start date will be the first of the previous month and the end date will be the last day of the next month
				//calendar cannot send us a start and end date

				DateTime now = DateTime.Now.AddMonths(-1);
				beginning = new DateTime(now.Year, now.Month, 1, 0, 0, 0);

				//add 2 months to now to go 1 month head of current
				now = now.AddMonths(2);
				end = new DateTime(now.Year, now.Month, DateTime.DaysInMonth(now.Year, now.Month), 23, 59, 59);

				bookings = (from book in context.VenueBooking
						   where book.VenueID == VenueID
						   && book.CancelledOn == null													//Not Cancelled
						   && book.VenueBookingStatusID == (long)Enumerators.VenueBookingStatus.Pending		//Set to pending
						   && book.StartDateTime >= beginning && book.EndDateTime < end					//Falls within the day specified
							select new CustomCalendarEvent() {
							OccupantID = book.OccupantID,
							ID = book.VenueBookingID,
							VenueID = book.VenueID,
							TotalBookingCost = book.TotalBookingCost,
							InsertedByID = book.InsertedByID,
							InsertedBy = book.InsertedBy,
							InsertedOn = book.InsertedOn,
							title = book.Name,
							Name = book.Name,
							Email = book.Email,
							Phone = book.PhoneNumber,
							VenueBookingStatusID = book.VenueBookingStatusID,
							Requirements = book.Requirements,
							allDay = false,
							start = book.StartDateTime,
							end = book.EndDateTime,
							className = book.VenueBookingStatus.Name,
							editable = true
						   }).ToList();
				return Utility.ServiceResponse(ref bookings);
			} catch(Exception ex) {
				return Utility.ServiceResponse(ref bookings, ex);
			}
		}

		/// <summary>
		/// gets bookings for the specified duration
		/// </summary>
		/// <param name="VenueID"></param>
		/// <param name="StartDate"></param>
		/// <param name="EndDate"></param>
		/// <returns></returns>
		[OperationContract]
		[Security("Admin,BookingManagement")]
		public EntityTransport<CustomCalendarEvent> GetBookingListByDates(long VenueID, DateTime StartDate, DateTime EndDate) {

			Utility.WCFCheckSecurityAttribute();
			List<CustomCalendarEvent> bookings = new List<CustomCalendarEvent>();

			try {
				DateTime beginning = new DateTime(StartDate.Year, StartDate.Month, StartDate.Day, 0, 0, 0);
				DateTime end = new DateTime(EndDate.Year, EndDate.Month, EndDate.Day, 23, 59, 59);

				bookings = (from book in context.VenueBooking
						   where book.VenueID == VenueID
						   && book.CancelledOn == null													//Not Cancelled
						   && book.VenueBookingStatusID == (long)Enumerators.VenueBookingStatus.Pending		//Set to pending
						   && book.StartDateTime >= beginning && book.EndDateTime < end					//Falls within the day specified
							select new CustomCalendarEvent() {
							OccupantID = book.OccupantID,
							ID = book.VenueBookingID,
							VenueID = book.VenueID,
							TotalBookingCost = book.TotalBookingCost,
							InsertedByID = book.InsertedByID,
							InsertedBy = book.InsertedBy,
							InsertedOn = book.InsertedOn,
							title = book.Name,
							Name = book.Name,
							Email = book.Email,
							Phone = book.PhoneNumber,
							VenueBookingStatusID = book.VenueBookingStatusID,
							Requirements = book.Requirements,
							allDay = false,
							start = book.StartDateTime,
							end = book.EndDateTime,
							className = book.VenueBookingStatus.Name,
							editable = true
						   }).OrderBy(b=> b.start).ToList();
				return Utility.ServiceResponse(ref bookings);
			} catch(Exception ex) {
				return Utility.ServiceResponse(ref bookings, ex);
			}
		}

		[OperationContract]
		[Security("Admin,BookingManage")]
		public EntityTransport<CustomIntanetBooking> GetUpcomingBookings(long OccupantID) {
			List<CustomIntanetBooking> bookings = new List<CustomIntanetBooking>();

			try {

				bookings = context.VenueBooking.Where(v => v.VenueBookingStatusID == (long)Enumerators.VenueBookingStatus.Pending && v.CancelledOn == null && v.OccupantID == OccupantID && v.StartDateTime > DateTime.Now).Select(v => new CustomIntanetBooking() {
					ID = v.VenueBookingID,
					title = v.Name,
					VenueName = v.Venue.Name,
					start = v.StartDateTime,
					end = v.EndDateTime,
					OccupantID = v.OccupantID,
					TotalBookingCost = v.TotalBookingCost,
					VenueBookingStatusID = v.VenueBookingStatusID

				}).OrderBy(b=> b.start).ToList();
				return Utility.ServiceResponse(ref bookings);
			} catch(Exception ex) {
				return Utility.ServiceResponse(ref bookings, ex);
			}
		}

		/// <summary>
		/// Retrieves a booking by ID
		/// </summary>
		/// <param name="BookingID"></param>
		/// <returns>VenueBooking</returns>
		[OperationContract]
		[Security("Admin,BookingManage")]
		public EntityTransport<VenueBooking> GetBooking(long BookingID) {
			VenueBooking booking = null;
			try {
				booking = context.VenueBooking.Where(w => w.VenueBookingID == BookingID).FirstOrDefault();
				return Utility.ServiceResponse(ref booking);
			} catch(Exception ex) {
				return Utility.ServiceResponse(ref booking, ex);
			}
		}

		/// <summary>
		/// Returns future bookings by occupant ID
		/// </summary>
		/// <param name="OccupantID"></param>
		/// <returns></returns>
		[OperationContract]
		[Security("Admin,BookingManage")]
		public EntityTransport<VenueBooking> GetBookingsByOccupantID(long OccupantID) {
			List<VenueBooking> bookings = new List<VenueBooking>();
			try {
				bookings = (from books in context.VenueBooking
							where books.CancelledOn == null
							&& books.VenueBookingStatusID == (long)Enumerators.VenueBookingStatus.Pending
							&& books.StartDateTime >= DateTime.Now
							select books).ToList();
				return Utility.ServiceResponse(ref bookings);
			} catch(Exception ex) {
				return Utility.ServiceResponse(ref bookings, ex);
			}
		}
		#endregion
		
	}
}
